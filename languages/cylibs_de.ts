<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>About</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>AcquisitionSettings</name>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>0</source>
        <translation type="obsolete">00</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation type="obsolete">Es gibt ungespeicherte Anderungen in der laufende Ansicht.Mochten Sie diese Änderungen anwenden oder ablenken ?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation type="obsolete">Ungespeicherte Anderungen</translation>
    </message>
</context>
<context>
    <name>BenchEmptying</name>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
</context>
<context>
    <name>BenchFilling</name>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
</context>
<context>
    <name>BenchSettings</name>
    <message>
        <source>Type</source>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <source>0</source>
        <translation type="obsolete">00</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>11</source>
        <translation type="obsolete">11</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>13</source>
        <translation type="obsolete">13</translation>
    </message>
    <message>
        <source>20</source>
        <translation type="obsolete">20</translation>
    </message>
    <message>
        <source>21</source>
        <translation type="obsolete">21</translation>
    </message>
    <message>
        <source>22</source>
        <translation type="obsolete">22</translation>
    </message>
    <message>
        <source>25</source>
        <translation type="obsolete">25</translation>
    </message>
    <message>
        <source>26</source>
        <translation type="obsolete">26</translation>
    </message>
    <message>
        <source>27</source>
        <translation type="obsolete">27</translation>
    </message>
    <message>
        <source>28</source>
        <translation type="obsolete">28</translation>
    </message>
    <message>
        <source>32</source>
        <translation type="obsolete">32</translation>
    </message>
    <message>
        <source>33</source>
        <translation type="obsolete">33</translation>
    </message>
    <message>
        <source>34</source>
        <translation type="obsolete">34</translation>
    </message>
    <message>
        <source>37</source>
        <translation type="obsolete">37</translation>
    </message>
    <message>
        <source>38</source>
        <translation type="obsolete">38</translation>
    </message>
    <message>
        <source>39</source>
        <translation type="obsolete">39</translation>
    </message>
    <message>
        <source>42</source>
        <translation type="obsolete">42</translation>
    </message>
    <message>
        <source>43</source>
        <translation type="obsolete">43</translation>
    </message>
    <message>
        <source>44</source>
        <translation type="obsolete">44</translation>
    </message>
    <message>
        <source>47</source>
        <translation type="obsolete">47</translation>
    </message>
    <message>
        <source>48</source>
        <translation type="obsolete">48</translation>
    </message>
    <message>
        <source>46</source>
        <translation type="obsolete">46</translation>
    </message>
    <message>
        <source>45</source>
        <translation type="obsolete">45</translation>
    </message>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">Raumlufttemperatur</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation type="obsolete">Es gibt ungespeicherte Anderungen in der laufende Ansicht.Mochten Sie diese Änderungen anwenden oder ablenken ?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation type="obsolete">Ungespeicherte Anderungen</translation>
    </message>
</context>
<context>
    <name>BurstingEdit</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="obsolete">Temperatur</translation>
    </message>
    <message>
        <source>Control</source>
        <translation type="obsolete">Kontrolle</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="obsolete">Druck</translation>
    </message>
</context>
<context>
    <name>BurstingTestMonitoring</name>
    <message>
        <source>Process</source>
        <translation type="obsolete">Prozess</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>BurstingTestParts</name>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
</context>
<context>
    <name>CYAI</name>
    <message>
        <source>Analog input</source>
        <translation>Analogeingang</translation>
    </message>
</context>
<context>
    <name>CYAO</name>
    <message>
        <source>Analog output</source>
        <translation>Analogausgang</translation>
    </message>
</context>
<context>
    <name>CYAbout</name>
    <message>
        <source>=</source>
        <translation type="vanished">=</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>Up&amp;date</source>
        <translation>Sp&amp;ace</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Select archive to send to the regulator.</source>
        <translation>Kann das Verzeichnis %1 nicht erstellen</translation>
    </message>
    <message>
        <source>(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> )</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>( </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Real Time information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAboutApplication</name>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A&amp;uthors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Droid Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;License Agreement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please report bugs to: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisition</name>
    <message>
        <source>Post-mortem mode</source>
        <translation type="vanished">Post-mortem Modus</translation>
    </message>
    <message>
        <source>Acquisition started</source>
        <translation>Erfassung startet</translation>
    </message>
    <message>
        <source>Acquisition pausing</source>
        <translation>Erfassung Pause</translation>
    </message>
    <message>
        <source>Acquisition window in progress</source>
        <translation>Erfassungsfenster in progress</translation>
    </message>
    <message>
        <source>Acquisition saving</source>
        <translation>Erfassung Speicherung</translation>
    </message>
    <message>
        <source>Save acquisition file timer</source>
        <translation>Speicherung der Erfassungsdatei timer</translation>
    </message>
    <message>
        <source>Post-mortem periodical backup timer</source>
        <translation>Post-mortem periodical backup timer</translation>
    </message>
    <message>
        <source>Periodical backup</source>
        <translation>Periodical backup</translation>
    </message>
    <message>
        <source>Acquisition files directory</source>
        <translation>Erfassungsdatei Verzeichnis</translation>
    </message>
    <message>
        <source>Use test directory</source>
        <translation>Benutzen Sie das Testverzeichnis </translation>
    </message>
    <message>
        <source>Directory path</source>
        <translation>Verzeichnisbahn</translation>
    </message>
    <message>
        <source>This path is used to save acquisition files somewhere else than in the test directory.</source>
        <translation>Diese Bahn wird zur Speicherung von Erfassungsdateien anderswo als im Testverzeichnis benutzt.</translation>
    </message>
    <message>
        <source>acquisition</source>
        <translation type="vanished">Erfassung</translation>
    </message>
    <message>
        <source>Acquisition files prefix</source>
        <translation>Erfassungsdatei  Prafix</translation>
    </message>
    <message>
        <source>Acquisition files extension</source>
        <translation>Erfassungsdatei Erweiterung</translation>
    </message>
    <message>
        <source>Maximum of lines per acquisition file</source>
        <translation>Maximale Linienanzahl pro Erfassungsdatei</translation>
    </message>
    <message>
        <source>Maximum number of acquisition files</source>
        <translation type="vanished">Maximale Anzahl der Erfassungsdateien</translation>
    </message>
    <message>
        <source>The acquisition saving will restart from the first file if this acquisition file number is reached.</source>
        <translation>Die Erfassungspeicherung wird neu starten von der erste Datei ab, wenn diese Erfassungsdateinummer erreicht ist.</translation>
    </message>
    <message>
        <source>Choose sensibly this number if you don&apos;t want to overwrite the oldest acquisition files.</source>
        <translation>Andern Sie etwa diese Niummer wenn Sie die letzte Erfassungsdatei nicht uberschreiben mochten.</translation>
    </message>
    <message>
        <source>Number of acquisition files</source>
        <translation>Nummer der Erfassungsdatei</translation>
    </message>
    <message>
        <source>Acquisition time to pretend</source>
        <translation>Erfassungszeit</translation>
    </message>
    <message>
        <source>Maximum file size</source>
        <translation>Maximale Dateigrosse</translation>
    </message>
    <message>
        <source>Maximum size acquisition</source>
        <translation>Maximale Erfassungsgrosse</translation>
    </message>
    <message>
        <source>Start acquisition date time</source>
        <translation>Datum und Zeit des Erfassungsstarts</translation>
    </message>
    <message>
        <source>Acquisition file column separator</source>
        <translation>Erfassungsdatei Kolonne Trenner</translation>
    </message>
    <message>
        <source>Other acquisition file column separator</source>
        <translation>Erfassungsdatei anderer Kolonne Trenner</translation>
    </message>
    <message>
        <source>Acquisition backup</source>
        <translation>Erfassung Backup</translation>
    </message>
    <message>
        <source>Backup file</source>
        <translation>Backup Data</translation>
    </message>
    <message>
        <source>Acquisition since:</source>
        <translation>Erfassung seit:</translation>
    </message>
    <message>
        <source>TIME (ms)%1</source>
        <translation type="vanished">TIME (ms)%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML !
%2: %3 %4</source>
        <translation>Die Datei %1 enthalt keine gultige XML !
%2: %3 %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a valid definition, which must have a document type</source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">Kann die Datei nicht speichern %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Kann die Datei nicht speichern %1</translation>
    </message>
    <message>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
    <message>
        <source>Kb</source>
        <translation>Kb</translation>
    </message>
    <message>
        <source>Mb</source>
        <translation>Mb</translation>
    </message>
    <message>
        <source>Gb</source>
        <translation>Gb</translation>
    </message>
    <message>
        <source>TIME</source>
        <translation>TIME</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="obsolete">Format</translation>
    </message>
    <message>
        <source>TIME (ms)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timestamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>us</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SoliaGraph compatibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Continuous backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Storage size is limited by the maximum number of acquisition files configurable in the  &apos;Files&apos; tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Post-mortem continuous acquisition archive error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Continuous acquisition archive file by post-mortem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generates a time-stamped acquisition file at the end of each acquisition time in sub-directories following the &apos;year/month/day/hour/minute&apos; tree structure.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SoliaGraph allows multiple selection of files or sub-directories to easily perform a temporal analysis of the acquisition by scrolling through the resulting curves.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please note that this mode of operation can consume a lot of storage space, and should therefore be used sparingly. You can simulate the space required in the &apos;Files&apos; tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generates an acquisition files based only on trigger events configured in the &apos;Events&apos; tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>In addition to saving post-mortem recordings by configurable events, you can also save all post-mortem recordings to multiple files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To simulate only the memory space required for continuous storage with simulation time, you can set this value to 0.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can also enter 0 to disable backup on event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The integration of SoliaGraph in Cylix provides direct access and this generation of compatible CSV files. If you are interested in this feature, please contact us to available it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Makes Cylix CSV acquisition files compatibled with SoliaGraph Web software for easy visualization them in the form of time curves. For this, generate files has a defined format and a timestamp (in µs).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preserves import compatibility with existing post-processing files (e.g. .ods or .xls spreadsheets) by using the old Cylix acquisition file format. These include a time counter (in ms), initialized to 0 at the start of acquisition.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The maximum is set at 1,048,576 for use with acquisition files from Excel 2007 and LibreOffice Calc 4.2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Acquisition</source>
        <translation type="unfinished">Erfassung </translation>
    </message>
    <message>
        <source>In continuous storage, this maximum is not used, as the number of files per hour is naturally low.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum of acquisition files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum of acquisition files on events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>CSV file size optimization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Optimizes CSV file size by using empty fields for unchanged values.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>With this optimization, storage space simulation can give a much higher value than reality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compatible with Solia Graph.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thanks to this function, you can, without greatly increasing the size of CSV files, add to the acquisition of data with highly variable values other, much less variable values, such as states.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionBackupFlag</name>
    <message>
        <source>Prefix used in the backup file name</source>
        <translation>Benutztes Prafix in der Backup Dateiname</translation>
    </message>
    <message>
        <source>Header used in the backup file</source>
        <translation>Benutzter Header in der Backup Datei</translation>
    </message>
    <message>
        <source>Enable backup</source>
        <translation>Erlaubt Backup</translation>
    </message>
    <message>
        <source>No backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <source>Rising</source>
        <translation>Display</translation>
    </message>
    <message>
        <source>Falling</source>
        <translation>Scaling</translation>
    </message>
    <message>
        <source>Backup on the falling edge of event.</source>
        <translation>Maximale Zeitdauer vor Neigung des Vorwartssensors.</translation>
    </message>
    <message>
        <source>Changing</source>
        <translation>Stift andern</translation>
    </message>
    <message>
        <source>Recording</source>
        <translation>Speicherung</translation>
    </message>
    <message>
        <source>Overwrite</source>
        <translation>Uberschreiben</translation>
    </message>
    <message>
        <source>Increment</source>
        <translation>Inkrement</translation>
    </message>
    <message>
        <source>If the prefix doesn&apos;t change the new archive overwrites the last one.</source>
        <translation>Wenn das Prafix andert sich nicht, das neue Archiv uberschreibt das letzte.</translation>
    </message>
    <message>
        <source>The number of backup is appended to the name of backup file. Thus, old backup files are note removed.</source>
        <translation>Die Backup-Anzahl ist dem Name von Backup-Datei beigefugt. Auf diese Weise alte Backup-Dateien sind nicht geloscht.</translation>
    </message>
    <message>
        <source>Backup delay on event</source>
        <translation>Backup date</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Backup on the rising edge of event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Backup on the rising and falling edges of event.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionDataView</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>Stift</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Anschluss</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>Designer Name</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>Integralkoefficient</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Data acquisition label</source>
        <translation>Datenerfassung Label</translation>
    </message>
    <message>
        <source>Enter the data acquisition label: </source>
        <translation>Geben Sie den Datenerfassungslabel ein:</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionFast</name>
    <message>
        <source>Acquisition period</source>
        <translation>Erfassungsperiode</translation>
    </message>
    <message>
        <source>Acquisition length time</source>
        <translation type="vanished">Erfassungszeitdauer</translation>
    </message>
    <message>
        <source>Acquisition window time</source>
        <translation>Erfassungsfenster Zeit</translation>
    </message>
    <message>
        <source>No acquisition window time</source>
        <translation>Keine Erfassungsfenster Zeit</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>Bursts acquisition period</source>
        <translation>Berstenerfassungsperiode</translation>
    </message>
    <message>
        <source>Acquisition length time of a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This time combined with the acquisition period automatically generates a number of lines per acquisition file. To reduce the time required to import multiple acquisition files into SoliaGraph, it is advisable to limit the number of small files by maximizing this time.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionSetup</name>
    <message>
        <source>&amp;General</source>
        <translation>&amp;Allgemein</translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation>Parameters</translation>
    </message>
    <message>
        <source>Acquisition window</source>
        <translation type="vanished">Erfassungsfenster</translation>
    </message>
    <message>
        <source>Datas</source>
        <translation>Daten</translation>
    </message>
    <message>
        <source>&amp;Header</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>Fo&amp;rm</source>
        <translation type="vanished">Form</translation>
    </message>
    <message>
        <source>Separation options</source>
        <translation>Trennung Optionen</translation>
    </message>
    <message>
        <source>&amp;Tabulation</source>
        <translation>&amp;Tabulation</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>Sem&amp;i-colon</source>
        <translation>Kolon</translation>
    </message>
    <message>
        <source>C&amp;omma</source>
        <translation>K&amp;omma</translation>
    </message>
    <message>
        <source>Sp&amp;ace</source>
        <translation>Sp&amp;ace</translation>
    </message>
    <message>
        <source>Ot&amp;her</source>
        <translation>Ot&amp;her</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Fi&amp;les</source>
        <translation>Da&amp;teien</translation>
    </message>
    <message>
        <source>Memory space</source>
        <translation type="vanished">Speicherplatz</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="vanished">=</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>E&amp;vents</source>
        <translation>Erei&amp;gnisse</translation>
    </message>
    <message>
        <source>Event</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Freigeben</translation>
    </message>
    <message>
        <source>Prefix</source>
        <translation>Prafix</translation>
    </message>
    <message>
        <source>Recording</source>
        <translation>Speicherung</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <source>&amp;States</source>
        <translation>&amp;Status</translation>
    </message>
    <message>
        <source>Applying this change will restart this acquisition with a new time reference and a new file will be created !</source>
        <translation>Diese Anderung anwenden wird diese Erfassung neu starten mit einer neue Zeitreferenz und eine neue Datei wird erzeugt !</translation>
    </message>
    <message>
        <source>Acquisition setup applying</source>
        <translation>Erfassung Setup Anwendung</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The number of lines calculated %1 is higher than maximum allowed (%2)!
Please reduce the acquisition period or its length time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Storage space simulation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionSlow</name>
    <message>
        <source>Acquisition period</source>
        <translation>Erfassungsperiode</translation>
    </message>
    <message>
        <source>Acquisition length time</source>
        <translation>Erfassungszeitdauer</translation>
    </message>
    <message>
        <source>Acquisition window time</source>
        <translation>Erfassungsfenster Zeit</translation>
    </message>
    <message>
        <source>No acquisition window time</source>
        <translation>Keine Erfassungsfenster Zeit</translation>
    </message>
</context>
<context>
    <name>CYAcquisitionWin</name>
    <message>
        <source>CYLIX: Acquisitions tool</source>
        <translation>CYLIX: Erfassungswerkzeuge</translation>
    </message>
</context>
<context>
    <name>CYAction</name>
    <message>
        <source>Users administration</source>
        <translation type="unfinished">Benutzerverwaltung</translation>
    </message>
    <message>
        <source>Function usage flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The current user is authorized to access this function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The current user is not authorized to access this function or Cylix is in protected access.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYActionCollection</name>
    <message>
        <source>Cannot add action %1 in menu of window %2!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot add action %1 in tool bar %2 of window %3!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAdc</name>
    <message>
        <source>ADC</source>
        <translation type="vanished">ADC</translation>
    </message>
</context>
<context>
    <name>CYAdc16</name>
    <message>
        <source>ADC</source>
        <translation type="obsolete">ADC</translation>
    </message>
</context>
<context>
    <name>CYAdc32</name>
    <message>
        <source>ADC</source>
        <translation type="obsolete">ADC</translation>
    </message>
</context>
<context>
    <name>CYAna</name>
    <message>
        <source>Number of points used for calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>New point</source>
        <translation>DAtei %1 bei Linie %2</translation>
    </message>
    <message>
        <source>Bench sensor</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Reference sensor</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>ADC bench values</source>
        <translation>Warnungswert</translation>
    </message>
    <message>
        <source>Point %1</source>
        <translation>Edit: %1</translation>
    </message>
    <message>
        <source>Bench values</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Reference values</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Analogic</source>
        <translation>Analog</translation>
    </message>
    <message>
        <source>High value</source>
        <translation type="vanished">Hoher Wert</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation type="vanished">Niedrieger Wert</translation>
    </message>
    <message>
        <source>Sensor defect</source>
        <translation>Sensor Fehler</translation>
    </message>
    <message>
        <source>Ana. Sensor</source>
        <translation>Ana. Messung</translation>
    </message>
    <message>
        <source>Defect of the sensor or the conditioner (Ex: signal lower than 4mA or higher than 20mA).  This may indicate a failure or a measure out of scale. Make sure of the quality of connections and the good performance of the acquisition board.</source>
        <translation>Sensor oder Umwandlerfehler (z.B : Signal niedriger als 4mA oder höher als 20mA). Dies kann einen Fehler oder eine unverhaltnismassig grosse Messung anzeigen. Uberprufen Sie die Anschlusse und die richtige Leistung der Erfassungskarte</translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bench sensor in ADC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Averaging times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>High value (not calibrated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum value of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low value (not calibrated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum value of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum value ADC of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum value ADC of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gain offset of %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ADC value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DAC value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseCell</name>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>Select a display type</source>
        <translation>Wahlen Sie einen Ansichttyp</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;Simple</translation>
    </message>
    <message>
        <source>&amp;Multimeter</source>
        <translation>&amp;Multimeter</translation>
    </message>
    <message>
        <source>&amp;Analog</source>
        <translation>&amp;Analog</translation>
    </message>
    <message>
        <source>&amp;Digital</source>
        <translation>Digitaleingang</translation>
    </message>
    <message>
        <source>&amp;Classical oscilloscope</source>
        <translation>&amp;Oszilloskop</translation>
    </message>
    <message>
        <source>Oscilloscope multiple formats</source>
        <translation>Oszilloskop Sollwerts</translation>
    </message>
    <message>
        <source>&amp;BarGraph</source>
        <translation>&amp;BarGraph</translation>
    </message>
    <message>
        <source>Datas &amp;table</source>
        <translation>Daten&amp;Tabelle</translation>
    </message>
    <message>
        <source>No display for this type of data</source>
        <translation>Keine Ansicht fur diesen Datentyp</translation>
    </message>
    <message>
        <source>The clipboard does not contain a valid display description.</source>
        <translation>Der Clipboard enthalt keine gultige Ansichtsbeschreibung</translation>
    </message>
    <message>
        <source>Analyse cell</source>
        <translation>Analysezelle</translation>
    </message>
    <message>
        <source>Add a cell on the right</source>
        <translation>Geben Sie rechts eine Zelle zu  </translation>
    </message>
    <message>
        <source>Add a cell below</source>
        <translation>Geben Sie darunter eine Zelle zu  </translation>
    </message>
    <message>
        <source>Remove this cell</source>
        <translation>Entfernen Sie diese Zelle</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The parent of cell %1 has more than 2 children!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseSheet</name>
    <message>
        <source>Analyse sheet</source>
        <translation>Analysebogen</translation>
    </message>
    <message>
        <source>Refresh time</source>
        <translation>Erfrischungszeit</translation>
    </message>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Die Datei %1 enthalt keine gultige XML
%2: Linie:%3 Kolonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>The file %1 has an invalid refresh time.</source>
        <translation>Die Datei %1 hat eine ungultige Refresh Zeit.</translation>
    </message>
    <message>
        <source>The file %1 has an invalid work sheet size.</source>
        <translation>Die Datei %1 hat eine ungultige Arbeitsbogen Grosse.</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">Kann die Datei nicht speichern %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Kann die Datei nicht speichern %1</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation>&amp;Properties</translation>
    </message>
    <message>
        <source>Row or Column out of range (%1, %2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseSheetSettings</name>
    <message>
        <source>Analyses Sheet Properties</source>
        <translation>Analysebogen Eigenschaften</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>Enter the title of the work sheet here.</source>
        <translation>Bitte geben Sie den Titel des Arbeitsbogens hier ein.</translation>
    </message>
    <message>
        <source>Update Interval</source>
        <translation>Update Interval</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation>Kolonnen</translation>
    </message>
    <message>
        <source>Rows</source>
        <translation>Linien</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseSpace</name>
    <message>
        <source>This is your analyse space. It holds your analyse sheets. You need to create a new analyse sheet (Menu File-&gt;New) before you can drag datas here.</source>
        <translation>Das ist Ihre Analyseraum. Er enthalt Ihren Analysebogen. Sie mussen einen neuen Analysebogen erzeugen (Menu Datei-&gt; Neu) bevor Sie Daten hier verschieben konnen.</translation>
    </message>
    <message>
        <source>Sheet %1</source>
        <translation>Bogen %1</translation>
    </message>
    <message>
        <source>The analyse sheet &apos;%1&apos; has been modified.
Do you want to save this analyse sheet?</source>
        <translation>Der Analysebogen %1 ist verandert worden.Mochten Sie diesen Analysebogen speichern?</translation>
    </message>
    <message>
        <source>Select a work sheet to load</source>
        <translation>Wahlen Sie einen Arbeitsbogen zum aufladen</translation>
    </message>
    <message>
        <source>You don&apos;t have an analyse sheet that could be saved!</source>
        <translation>Sie haben keinen Analysebogen zu speichern!</translation>
    </message>
    <message>
        <source>Save current work sheet as</source>
        <translation>Speichern Sie laufenden Arbeitsbogen unter</translation>
    </message>
    <message>
        <source>You don&apos;t have an analyse sheet that could be captured!</source>
        <translation>Sie haben keinen Analysebogen zu erfassen!</translation>
    </message>
    <message>
        <source>There are no analyse sheets that could be deleted!</source>
        <translation>Es gibt keine Analysebogen zu loschen !</translation>
    </message>
    <message>
        <source>Graphic analyse</source>
        <translation>Graphic analyse</translation>
    </message>
    <message>
        <source>%1/Graphic_sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have to enter a title for the analyse sheet !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseWin</name>
    <message>
        <source>&amp;Analyse sheet properties...</source>
        <translation>&amp;Eigenschaften des Analysebogens...</translation>
    </message>
    <message>
        <source>Configure &amp;Style...</source>
        <translation>Configure &amp;Style...</translation>
    </message>
    <message>
        <source>Template...</source>
        <translation>Muster...</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>&amp;Capture</source>
        <translation>&amp;Erfassung</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Erfrischungszeit</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYApplication</name>
    <message>
        <source>modified</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAquisitionWidget</name>
    <message>
        <source>Setup</source>
        <translation>Setup</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAutoCali</name>
    <message>
        <source>&amp;High value</source>
        <translation>&amp;Hoher Wert</translation>
    </message>
    <message>
        <source>&amp;Designer value</source>
        <translation>&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="vanished">Analogeingang</translation>
    </message>
    <message>
        <source>&amp;Low value</source>
        <translation>&amp;Niedrieger Wert</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Anmerkung</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Hoch</translation>
    </message>
    <message>
        <source>High Value</source>
        <translation>Hoher Wert</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Nieder</translation>
    </message>
    <message>
        <source>Low Value</source>
        <translation>Niedrieger Wert</translation>
    </message>
    <message>
        <source>Do you really want to load the designer&apos;s values ?</source>
        <translation>Mochten Sie wirklich die Hersteller Werte aufladen ?</translation>
    </message>
    <message>
        <source>Currrent values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAutoCaliInput</name>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
</context>
<context>
    <name>CYBargraph</name>
    <message>
        <source>This display cannot treat this type of data !</source>
        <translation>Diese Ansicht kann diesen Datentyp nicht verarbeiten !</translation>
    </message>
    <message>
        <source>This data is already displayed in this bargraph.</source>
        <translation>Diese Data ist schon angegeben in diesem Bargraph.</translation>
    </message>
    <message>
        <source>Bargraph</source>
        <translation>Bargraph</translation>
    </message>
</context>
<context>
    <name>CYBargraphPlotter</name>
    <message>
        <source>For a good display the maximum number of bars in the bargraph is %1!</source>
        <translation>Fur eine gute Ansicht, liegt die maximale Baranzahl im Bargraph bei %1!</translation>
    </message>
    <message>
        <source>Bargraph</source>
        <translation>Bargraph</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation>Skala</translation>
    </message>
    <message>
        <source>Minimum value</source>
        <translation>Minimaler Wert</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation>Maximaler Wert</translation>
    </message>
    <message>
        <source>Sense</source>
        <translation>Sense</translation>
    </message>
    <message>
        <source>Increasing</source>
        <translation>Steigend</translation>
    </message>
    <message>
        <source>Decreasing</source>
        <translation>Absteigend</translation>
    </message>
    <message>
        <source>Alarm</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>Upper bound value</source>
        <translation>Oberer abhangiger Wert</translation>
    </message>
    <message>
        <source>Upper alarm enable</source>
        <translation>Oberwarnung erlaubt</translation>
    </message>
    <message>
        <source>Lower bound value</source>
        <translation>Niedrieger Wert</translation>
    </message>
    <message>
        <source>Lower alarm enable</source>
        <translation>Niederwarnung erlaubt</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <source>Normal Bar Color</source>
        <translation>Normale Balkenfarbe</translation>
    </message>
    <message>
        <source>Out-of-range Color</source>
        <translation>Out-of-range Farbe</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation>Hintergrund Farbe</translation>
    </message>
</context>
<context>
    <name>CYBargraphSetup</name>
    <message>
        <source>BarGraph Settings</source>
        <translation>BarGraph Sollwerts</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <source>Display Range</source>
        <translation>Anzeigebereich</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYBool</name>
    <message>
        <source>Designer value: %1.</source>
        <translation>Hersteller Wert : %1</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>Binary</translation>
    </message>
    <message>
        <source>true</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>false</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCC</name>
    <message>
        <source>Maintenance</source>
        <translation>Wartung</translation>
    </message>
    <message>
        <source>Cycles/switching counter</source>
        <translation>Zyklen-/Umschaltungszahler</translation>
    </message>
    <message>
        <source>Counter used for maintenance</source>
        <translation>Zur Wartung benutzte Zahler </translation>
    </message>
    <message>
        <source>Total</source>
        <translation>Komplet</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>Teil</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Anmerkung</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation> Reset</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>Alert threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An alert is generated if the partial counter reaches this value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A null value disable the control.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold. This threshold is settable in the maintenance counters window.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCS</name>
    <message>
        <source>Maintenance</source>
        <translation>Wartung</translation>
    </message>
    <message>
        <source>Time counter</source>
        <translation>Zeit Zahler</translation>
    </message>
    <message>
        <source>Counter used for maintenance</source>
        <translation>Zur Wartung benutzte Zahler </translation>
    </message>
    <message>
        <source>Total</source>
        <translation>Komplet</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>Teil</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Anmerkung</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation> Reset</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>Alert threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An alert is generated if the partial counter reaches this value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A null value disable the control.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold. This threshold is settable in the maintenance counters window.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCT</name>
    <message>
        <source>Alert value</source>
        <translation>Warnungswert</translation>
    </message>
    <message>
        <source>Fault value</source>
        <translation>Fehlerwert</translation>
    </message>
    <message>
        <source> &gt; MAX (Max. alert)</source>
        <translation> &gt; MAX (Max. Alarm)</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>Prozess</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert. Also The maximum time (or number of cycles) of alert has been reached and thus produced a fault.</source>
        <translation>Die Messung war hoher als den maximalen tolerierten Wert, was einen Alarm auslost. Uberdies ist die maximale Alarm-Zeitdauer (oder Zyklen Anzahl) erreicht worden ist, deshalb lost es einen Fehler aus. </translation>
    </message>
    <message>
        <source> &lt; MIN (Max. alert)</source>
        <translation> &lt; MIN (Max. Alarm)</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert. Also The maximum time (or number of cycles) of alert has been reached and thus produced a fault.</source>
        <translation>Die Messung war niedriger als den minimalen tolerierten Wert, was einen Alarm auslost. Uberdies ist die maximale Alarm-Zeitdauer (oder Zyklen Anzahl) erreicht worden ist , deshalb lost es einen Fehler aus. </translation>
    </message>
    <message>
        <source> &gt; MAX: DATA1</source>
        <translation> &gt; MAX: DATA1</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum allowed value, which produced a fault.</source>
        <translation>Die Messung war hoher als den maximalen tolerierten Wert, was einen Fehler auslost. </translation>
    </message>
    <message>
        <source> &lt; MIN: DATA1</source>
        <translation> &lt; MIN: DATA1</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum allowed value, which produced a fault.</source>
        <translation>Die Messung war niedriger als den minimalen tolerierten Wert, was einen Fehler auslost. </translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert.</source>
        <translation>Die Messung war hoher als den maximalen tolerierten Wert, was eine Alarm auslost. </translation>
    </message>
    <message>
        <source>The value at which the alert occured is written in the message.</source>
        <translation>Der Wert, bei dem der Alarm ausgelost wird, wird in der Meldung geschrieben.</translation>
    </message>
    <message>
        <source>The maximum measured value is written in the message of end of alert.</source>
        <translation>Der maximalen gemessenen Wert wird in der Meldung von Alarm-Ende geschrieben.</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert.</source>
        <translation>Messung war niedriger als den minimalen tolerierten Wert, was einen Alarm auslöst.</translation>
    </message>
    <message>
        <source>The minimum measured value is written in the message of end of alert.</source>
        <translation>Der minimalen gemessenen Wert wird in der Meldung von Alarm-Ende geschrieben.</translation>
    </message>
</context>
<context>
    <name>CYCTCyc</name>
    <message>
        <source>Number of inhibition cycles before control (T0)</source>
        <translation>Anzahl der Inhibitionszyklen vor Kontrolle (T0)</translation>
    </message>
    <message>
        <source>The minimum number of cycles before control is T0</source>
        <translation>Die Minimalanzahl der Inhibitionszyklen vor Kontrolle ist T0</translation>
    </message>
    <message>
        <source>The maximum number of cycles before control is T0+T1</source>
        <translation>Die Maximalanzahl der Inhibitionszyklen vor Kontrolle ist T0+T1</translation>
    </message>
    <message>
        <source>Supplementary cycles before control (T1)</source>
        <translation>Zusatzliche Zyklen vor Kontrolle (T1)</translation>
    </message>
    <message>
        <source>The control start when the measure in inside the tolerances, or when this optional inhibition is overreached</source>
        <translation>Die Kontrolle beginnt wenn die Messung innerhalb der Toleranze liegt, oder wenn die optionnelle Inhibition uberschritten ist</translation>
    </message>
    <message>
        <source>Stop test if maximum alert ?</source>
        <translation>Test Stop wenn maximal Warnunganzahl ?</translation>
    </message>
    <message>
        <source>The bench will be stopped if the max number of cycles alert is reached.</source>
        <translation>Der Prufstand wird gestoppt wenn die maximale Anzahl der Warnugszyklen erreicht ist.</translation>
    </message>
    <message>
        <source>Maximum number of cycles out of alert tolerances</source>
        <translation>Maximale Anzahl der Zyklen ausser der Warnungstoleranzen</translation>
    </message>
    <message>
        <source>Maximum outside tolerances number of cycles for control.</source>
        <translation>Maximale ausser Toleranz Anzahl der Zyklen zur Kontrolle.</translation>
    </message>
    <message>
        <source>If reached =&gt; fault!</source>
        <translation>Wenn erreicht =&gt; Fehler!</translation>
    </message>
    <message>
        <source>Stop test if out of fault tolerances ?</source>
        <translation>Test Stop wenn ausser Fehlertoleranzen ?</translation>
    </message>
    <message>
        <source>The bench will be stopped if fault tolerance is reached.</source>
        <translation>Der Prufstand wird gestoppt wenn die Fehlertoleranz erreicht ist.</translation>
    </message>
    <message>
        <source> &gt; MAX (Max. alert)</source>
        <translation> &gt; MAX (Max. Alarm)</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>Prozess</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert. Also the maximum number of cycles of alert has been reached and thus produced a fault.</source>
        <translation>Die Messung war hoher als den maximalen tolerierten Wert, was einen Alarm auslost. Uberdies ist die maximale Anzahl von Alarm-Zyklen erreicht worden, deshalb lost es einen Fehler aus. </translation>
    </message>
    <message>
        <source> &lt; MIN (Max. alert)</source>
        <translation> &lt; MIN (Max. Alarm)</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert. Also the maximum number of cycles of alert has been reached and thus produced a fault.</source>
        <translation>Die Messung war niedriger als den minimalen tolerierten Wert, was einen Alarm auslost. Uberdies ist die maximale Anzahl von Alarm-Zyklen erreicht worden, deshalb lost es einen Fehler aus. </translation>
    </message>
</context>
<context>
    <name>CYCTInput</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCTTime</name>
    <message>
        <source>Minimum measure control delay (T0)</source>
        <translation>Minimale Zeitdauer der Messungskontrolle (T0)</translation>
    </message>
    <message>
        <source>The minimum control delay is T0</source>
        <translation>Die minimale Zeitdauer der Kontrolle ist T0</translation>
    </message>
    <message>
        <source>The maximum control delay is T0+T1</source>
        <translation>Die maximale Zeitdauer der Kontrolle ist T0+T1</translation>
    </message>
    <message>
        <source>Supplementary measure control delay (T1)</source>
        <translation>Zusatzliche Zeitdauer der Messugskontrolle (T1)</translation>
    </message>
    <message>
        <source>The control start when the measure in inside the tolerances, or when this optional inhibition is overreached</source>
        <translation>Die Kontrolle beginnt wenn die Messung innerhalb der Toleranze liegt, oder wenn die optionnelle Inhibition uberschritten ist</translation>
    </message>
    <message>
        <source>Stop test if maximum alert ?</source>
        <translation>Test Stop wenn maximal Warnunganzahl ?</translation>
    </message>
    <message>
        <source>The bench will be stopped if the max time alert is reached.</source>
        <translation>Der Prufstand wird gestoppt wenn die maximale Dauerwarnung erreicht ist.</translation>
    </message>
    <message>
        <source>Maximum time out of alert tolerances</source>
        <translation>Maximale Timeout der Warnungstoleranzen</translation>
    </message>
    <message>
        <source>Maximum outside alert tolerances time before process fault.</source>
        <translation>Maximale ausser Warnungstoleranzen Zeitdauer vor Prozessfehler.</translation>
    </message>
    <message>
        <source>Stop test if out of fault tolerances ?</source>
        <translation>Test Stop wenn ausser Fehlertoleranzen ?</translation>
    </message>
    <message>
        <source>The bench will be stopped if fault tolerance is reached.</source>
        <translation>Der Prufstand wird gestoppt wenn die Fehlertoleranz erreicht ist.</translation>
    </message>
    <message>
        <source> &gt; MAX (Max. alert)</source>
        <translation> &gt; MAX (Max. Alarm)</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>Prozess</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert. Also the maximum time of alert has been reached and thus produced a fault.</source>
        <translation>Die Messung war hoher als den maximalen tolerierten Wert, was einen Alarm auslost. Uberdies ist die maximale Alarm-Zeitdauer erreicht worden, deshalb lost es einen Fehler aus. </translation>
    </message>
    <message>
        <source> &lt; MIN (Max. alert)</source>
        <translation> &lt; MIN (Max. Alarm)</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert. Also the maximum time of alert has been reached and thus produced a fault.</source>
        <translation>Die Messung war niedriger als den minimalen tolerierten Wert, was einen Alarm auslost. Uberdies ist die maximale Alarm-Zeitdauer erreicht worden, deshalb lost es einen Fehler aus. </translation>
    </message>
</context>
<context>
    <name>CYChangeAccess</name>
    <message>
        <source>Choose access: </source>
        <translation>Zugriff wahlen:</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Change Accesss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCnt</name>
    <message>
        <source>Total counter</source>
        <translation>Gesamtzahler</translation>
    </message>
    <message>
        <source>Counter used for maintenance</source>
        <translation>Zur Wartung benutzte Zahler </translation>
    </message>
    <message>
        <source>Partial counter</source>
        <translation>Teilzahler</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Anmerkung</translation>
    </message>
</context>
<context>
    <name>CYComboBoxDialog</name>
    <message>
        <source>Appl&amp;y</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Dialog comboBox data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCommand</name>
    <message>
        <source>Command</source>
        <translation>Steuerung</translation>
    </message>
</context>
<context>
    <name>CYCore</name>
    <message>
        <source>Bench</source>
        <translation>Prufstand</translation>
    </message>
    <message>
        <source>Undef</source>
        <translation>Undef</translation>
    </message>
    <message>
        <source>Simulation</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>Designer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <source>Protected</source>
        <translation>Geschutzt</translation>
    </message>
    <message>
        <source>Administrator</source>
        <translation>Verwalter</translation>
    </message>
    <message>
        <source>Cannot start the &apos;%1&apos; with the %2 on COM%3 !</source>
        <translation>Kann die &apos;%1&apos; nicht starten mit der %2 auf COM%3 !</translation>
    </message>
    <message>
        <source>Can&apos;t remove the user %1 because it is the current user!</source>
        <translation>Benutzer %1 kann nicht entnommen werden da es laufender Benutzer ist!</translation>
    </message>
    <message>
        <source>Removing user</source>
        <translation>Benutzer wechseln</translation>
    </message>
    <message>
        <source>Can&apos;t remove the group %1 because it is used by the user %2!</source>
        <translation>Gruppe %1 kann nicht entnommen werden da sie vom Benutzer %2 gebraucht wird!</translation>
    </message>
    <message>
        <source>Removing users group</source>
        <translation>Benutzergruppe wechseln</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <source>This access has the administrator rights and authorizes all the menus!</source>
        <translation>Dieser Zugang hat die Verwalter-Rechte und ermachtigt alle Menus!</translation>
    </message>
    <message>
        <source>Do you want to create a new access more protected?</source>
        <translation>Mochten Sie einen neuen Zugang mehr geschutzt erstellen?</translation>
    </message>
    <message>
        <source>Administrator rights!</source>
        <translation>Verwalter-Rechte!</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Yes</translation>
    </message>
    <message>
        <source>&amp;Continue</source>
        <translation>Weitermachen</translation>
    </message>
    <message>
        <source>Initializing metrology window...</source>
        <translation>Initialisierung Supervisor-Ereignisse</translation>
    </message>
    <message>
        <source>Times of tests</source>
        <translation>Zeit Zahler</translation>
    </message>
    <message>
        <source>Times of access</source>
        <translation>Zeit Zahler</translation>
    </message>
    <message>
        <source>Cannot create the directory %1</source>
        <translation>Kann das Verzeichnis %1 nicht erstellen</translation>
    </message>
    <message>
        <source>Cannot find backup directoy</source>
        <translation>Kein Backupverzeichnis vorhanden</translation>
    </message>
    <message>
        <source>Cannot find the directory to backup</source>
        <translation>Kann das Verzeichnis zum Backup nicht finden</translation>
    </message>
    <message>
        <source>Cannot find the file to backup</source>
        <translation>Kann die Datei zum Backup nicht finden</translation>
    </message>
    <message>
        <source>See the users managment section of the computer installation procedure.</source>
        <translation>Siehe Benutzerverwaltungsektion im Computer Installationsvorgang.</translation>
    </message>
    <message>
        <source>The CYLIX base directrory %1 doesn&apos;t exist!</source>
        <translation>Das CYLIX Hauptverzeichnis %1 ist nicht vorhanden!</translation>
    </message>
    <message>
        <source>%1 is not a directory!</source>
        <translation>%1 is kein Verzeichnis!</translation>
    </message>
    <message>
        <source>%1 is not a readable!</source>
        <translation>%1 ist nicht lesbar!</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <source>Do you want to include the sub-groups ?</source>
        <translation>Mochten Sie die Untergruppe einschliessen ?</translation>
    </message>
    <message>
        <source>Loading groug</source>
        <translation>Aufladung Gruppe</translation>
    </message>
    <message>
        <source>class: %1 not supported (%2)</source>
        <translation>class: %1 not supported (%2)</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Metrology window</source>
        <translation>Ereignisse Einstellungen</translation>
    </message>
    <message>
        <source>GID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>UID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Evolution of the project settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add alphabetically the datas of this group in a new column ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If you answer &quot;No&quot;, the datas will be added in news rows !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1:%2 : designer value checking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose a filename to save under for this export file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HOST(S)	LINK(S)	GROUP	NAME	LABEL	VALUE	DEF	MIN	MAX	HELP
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot find the manual for the current language !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot remove the application&apos;s temporary directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You do not have access rights to the requested documentation ! Check your access rights with your Cylix administrator. These can be configured in the Cylix user administration tool.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not find the access action to open the document %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not find document %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error %1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initialization SoliaGraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SoliaGraph is not activated on this Cylix. If you are interested in this tool, please contact SOLIA Concept.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting SoliaGraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Doesn&apos;t exist &apos;%1&apos; !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initializing SoliaGraph...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to unzip file SoliaGraph!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to start SoliaGraph !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SoliaGraph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDB</name>
    <message>
        <source>Starting refresh timer datas base %1 at %2 ms</source>
        <translation>Startet Wiederholungstimer Datenbank %1 bei %2 ms</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">Kann die Datei nicht speichern %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Kann die Datei nicht speichern %1</translation>
    </message>
    <message>
        <source>The clipboard does not contain a valid datas base description.</source>
        <translation>Das Clipboard enthalt keine gultige Datenbank Beschreibung</translation>
    </message>
    <message>
        <source>Cannot remove %1 from %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDBLibs</name>
    <message>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <source>Setpoints protection</source>
        <translation>Sollwerts Sicherung</translation>
    </message>
    <message>
        <source>Avoid to erase project setpoints. If a project, which has ever been used by the machine, is modified in the project editor then its saving is done in a different project directory.</source>
        <translation>Vermeiden Sie das Loschen jeder Projektsollwerts. Falls ein Projekt, das irgendeinmal im Test war, im Projekteditor geandert wird, so wird seine Sicherung in einem anderen Projektverzeichnis gespeichert.</translation>
    </message>
    <message>
        <source>The name of this directory is automatically created with the date and hour of the modification.</source>
        <translation>Der Name dieses Verzeichnis ist automatisch erstellt mit Datum und Stunde der Anderung</translation>
    </message>
    <message>
        <source>Test</source>
        <translation>Test</translation>
    </message>
    <message>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <source>Enable backup</source>
        <translation>Erlaubt Backup</translation>
    </message>
    <message>
        <source>Double safeguard in another partition all the files of parameters and useful results in the event of loss of the partition of datas /home.</source>
        <translation>Verdoppeln Sie die Speicherungen aller Parameter und  wichtiger Ergebnisse in einer andere Partition im Falle eines Verlusts der Daten/Home Partition.</translation>
    </message>
    <message>
        <source>Backup path</source>
        <translation>Backup path</translation>
    </message>
    <message>
        <source>Partition backup path.</source>
        <translation>Partition backup path.</translation>
    </message>
    <message>
        <source>Events</source>
        <translation>Ereignisse</translation>
    </message>
    <message>
        <source>Supervisor starting</source>
        <translation>Supervisor Start</translation>
    </message>
    <message>
        <source>Supervisor stoping</source>
        <translation>Supervisor Stop</translation>
    </message>
    <message>
        <source>New calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>Last channel calibrated</source>
        <translation>kalibrierter Sensor</translation>
    </message>
    <message>
        <source>Viewer</source>
        <translation>Display</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Date and hour</source>
        <translation>Datum und Zeit</translation>
    </message>
    <message>
        <source>Since</source>
        <translation>Skala</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>From</source>
        <translation>Vom</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Help of the selected event</source>
        <translation>Hilfe des ausgewählten Ereignisses</translation>
    </message>
    <message>
        <source>Mail</source>
        <translation>Mail</translation>
    </message>
    <message>
        <source>Enable the send of fault messages by email</source>
        <translation>Erlaubt die Sendung von Fehlermeldungen via Email</translation>
    </message>
    <message>
        <source>SMTP servor name</source>
        <translation>SMTP servor name</translation>
    </message>
    <message>
        <source>SMTP port</source>
        <translation>SMTP port</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">Bereich</translation>
    </message>
    <message>
        <source>Sender</source>
        <translation>Sender</translation>
    </message>
    <message>
        <source>Receiver (s)</source>
        <translation>Empfanger</translation>
    </message>
    <message>
        <source>If there is more than one address, separate them with commas.</source>
        <translation>Gibt es mehr als eine Adresse, trennen Sie sie mit Komma.</translation>
    </message>
    <message>
        <source>Subject</source>
        <translation>Subject</translation>
    </message>
    <message>
        <source>Body</source>
        <translation>Body</translation>
    </message>
    <message>
        <source>Users</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <source>Simulation access</source>
        <translation>Simulation Zugriff</translation>
    </message>
    <message>
        <source>Current user</source>
        <translation>&amp;Current</translation>
    </message>
    <message>
        <source>Acquisition</source>
        <translation>Erfassung </translation>
    </message>
    <message>
        <source>Inhibits calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>Inhibits verification</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>Inhibits designer values</source>
        <translation>&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Full display</source>
        <translation>Dummy display</translation>
    </message>
    <message>
        <source>Curve color of deviation FS</source>
        <translation>Kurvenstyl</translation>
    </message>
    <message>
        <source>Curve style of deviation FS</source>
        <translation>Kurvenstyl</translation>
    </message>
    <message>
        <source>No Curve</source>
        <translation>Keine Kurve</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation>Linien</translation>
    </message>
    <message>
        <source>Sticks</source>
        <translation>Sticks</translation>
    </message>
    <message>
        <source>Steps</source>
        <translation>Steps</translation>
    </message>
    <message>
        <source>Dots</source>
        <translation>Dots</translation>
    </message>
    <message>
        <source>%1/calibrating</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>SMTP user name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SMTP password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This information is also shown in the Cylix window title bar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date and time of access change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inhibits reset points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inhibits addition point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inhibits suppression point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Full display with all values in table and all curves.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simplified display of table and curves without some values.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Curve color of linearity error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Curve style of linearity error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Directory of PDF recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Directory containing the report file of verification or calibration. These are automatically classified into sensor subdirectories.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The help of an event is a real diagnostic aid to understand its cause.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>One or more hyperlinks allow to open the Cylix manual directly at a precise place, as for example the part relating to the setting which can influence the event triggering.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>One or more data (measurements, setpoints, parameters...) allow to visualize directly values which can influence the event triggering. These values, in bold, are refreshed when the help is displayed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Project sent to the numerical regulator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDI</name>
    <message>
        <source>Digital input</source>
        <translation>Digitaleingang</translation>
    </message>
    <message>
        <source>Unplugged</source>
        <translation>Ausgeschaltet</translation>
    </message>
</context>
<context>
    <name>CYDO</name>
    <message>
        <source>Digital output</source>
        <translation>Analogausgang</translation>
    </message>
    <message>
        <source>Unplugged</source>
        <translation>Ausgeschaltet</translation>
    </message>
</context>
<context>
    <name>CYData</name>
    <message>
        <source>Control</source>
        <translation>Kontrolle</translation>
    </message>
    <message>
        <source>Integer</source>
        <translation>Integer</translation>
    </message>
    <message>
        <source>Boolean</source>
        <translation>Boolean</translation>
    </message>
    <message>
        <source>Floating</source>
        <translation>Floating</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>Wort</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <source>Note :</source>
        <translation>Anmerkung :</translation>
    </message>
    <message>
        <source>Notes :</source>
        <translation>Anmerkungen :</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 cannot be wrote because not in write mode !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error of help data markup %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The increase in value is too abrupt. The value of this data can only be increased by a maximum of %1% of its current value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The decrease in value is too abrupt. The value of this data can only be decreased by a maximum of %1% of its current value.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasBrowser</name>
    <message>
        <source>User datas</source>
        <translation>Benutzer Daten</translation>
    </message>
    <message>
        <source>System datas</source>
        <translation>System Daten</translation>
    </message>
    <message>
        <source>All datas</source>
        <translation>Alle Daten</translation>
    </message>
    <message>
        <source>The Seach bar makes it possible to search by words or characters among the filtered results. &lt;br/&gt;&lt;br/&gt; Only matches (data and/or categories) are then displayed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasBrowserList</name>
    <message>
        <source>Data Browser</source>
        <translation>Data Browser</translation>
    </message>
    <message>
        <source>Physical Type</source>
        <translation>Physischer Typ</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>Drag datas to empty cells of a analyse sheet.</source>
        <translation>Drag Daten zur Entleerung der Zellen eines Analysebogens.</translation>
    </message>
    <message>
        <source>The data browser lists the connected hosts and the datas that they provide. Click and drag datas into drop zones of a analyse sheet. A display will appear that visualizes the values provided by the data. Some data displays can display values of multiple datas. Simply drag other datas on to the display to add more datas.</source>
        <translation>Der Databrowser listet die angeschlossene Hosts und die von denen versorgten Daten. Klick und drag Daten in die Dropzellen eines Analysebogens. Eine Ansicht wird erscheinen mit den von den Daten versorgte Werte. Brigen Sie andere Daten in die Ansicht um Daten dazu zu bringen.</translation>
    </message>
    <message>
        <source>Drag datas to empty fields in a work sheet</source>
        <translation>Entnehmen Sie Daten um Felder zu leeren in einem ARbeitsbogen</translation>
    </message>
</context>
<context>
    <name>CYDatasEditList</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>Stift</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Anschluss</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>Designer Name</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>Integralkoefficient</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>Connexion</source>
        <translation>Anschluss</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Push this button to configure the color of the data in the oscilloscope.</source>
        <translation>Benutzen Sie diesen Knopf um die Datenfarbe im Oszilloskop zu konfigurieren.</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>&amp;Set Color</source>
        <translation>&amp;Set Color</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Push this button to configure the pen of the data in the oscilloscope.</source>
        <translation>Benutzen Sie diesen Knopf, um den Datenstift im Oszilloskop zu konfigurieren</translation>
    </message>
    <message>
        <source>Set Pen</source>
        <translation>Set Pen</translation>
    </message>
    <message>
        <source>Push this button to configure the display coefficient of the data in the oscilloscope.</source>
        <translation>Benutzen Sie diesen Knopf, um den Datenstift im Oszilloskop zu konfigurieren</translation>
    </message>
    <message>
        <source>Set Coefficient</source>
        <translation>Integralkoefficient</translation>
    </message>
    <message>
        <source>Push this button to delete the data.</source>
        <translation>Benutzen Sie diesen Knopf um die Daten zu loschen</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Hôte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasListView</name>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>Stift</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Anschluss</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>Designer Name</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>Integralkoefficient</translation>
    </message>
    <message>
        <source>Display coefficient</source>
        <translation>Integralkoefficient</translation>
    </message>
    <message>
        <source>Enter the new display coefficient: </source>
        <translation>Geben Sie den neuen Label ein:</translation>
    </message>
    <message>
        <source>Data label</source>
        <translation>Data label</translation>
    </message>
    <message>
        <source>Enter the new label: </source>
        <translation>Geben Sie den neuen Label ein:</translation>
    </message>
    <message>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You must enter a coefficient display different of 0 !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter the new display offset: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can enter a maximun of %1 data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Acquisition list of datas full</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasTable</name>
    <message>
        <source>Datas table</source>
        <translation>Daten Tabelle</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>Forcing</source>
        <translation>Forcing</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>Connection(s)</source>
        <translation>Anschluss(e)</translation>
    </message>
    <message>
        <source>Host(s)</source>
        <translation>Host(s)</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Phys</source>
        <translation>Phys</translation>
    </message>
    <message>
        <source>Elec</source>
        <translation>Elec</translation>
    </message>
    <message>
        <source>ADC</source>
        <translation>ADC</translation>
    </message>
    <message>
        <source>Display</source>
        <translation>Display</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation> Reset</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>Teil</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Anmerkung</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>Kontrolle</translation>
    </message>
    <message>
        <source>Addr</source>
        <translation>&amp;Add</translation>
    </message>
    <message>
        <source>Column label</source>
        <translation>Kolonne Titel</translation>
    </message>
    <message>
        <source>Enter the label of this column:</source>
        <translation>Geben Sie den Titel dieser Kolonne ein</translation>
    </message>
    <message>
        <source>&amp;Forcing mode</source>
        <translation>&amp;Forcing Modus</translation>
    </message>
    <message>
        <source>Reset mode</source>
        <translation> Reset Modus</translation>
    </message>
    <message>
        <source>Partial mode</source>
        <translation>Teilmodus</translation>
    </message>
    <message>
        <source>Note mode</source>
        <translation>Anmerkung Modus</translation>
    </message>
    <message>
        <source>Hide column</source>
        <translation>Kolonne verstecken</translation>
    </message>
    <message>
        <source>Show column</source>
        <translation>Kolonne anzeigen</translation>
    </message>
    <message>
        <source>Add column</source>
        <translation>Kolonne beifugen</translation>
    </message>
    <message>
        <source>Change column label</source>
        <translation>Kolonnetitel andern</translation>
    </message>
    <message>
        <source>Change column width</source>
        <translation>Kolonnebreite andern</translation>
    </message>
    <message>
        <source>Remove row</source>
        <translation>Zeile löschen</translation>
    </message>
    <message>
        <source>Change row height</source>
        <translation>Kolonnehohe andern</translation>
    </message>
    <message>
        <source>Export PDF</source>
        <translation>&amp;Export</translation>
    </message>
    <message>
        <source>Input the new width of selected columns</source>
        <translation>Geben Sie die neue Breite der gewahlte Kolonne ein</translation>
    </message>
    <message>
        <source>No row selected !</source>
        <translation>Keine ausgewählte Zeile !</translation>
    </message>
    <message>
        <source>Are you sure to remove the setected row(s) ?</source>
        <translation>Wollen sie wirklich die ausgewählte Zeile(n) löschen ?</translation>
    </message>
    <message>
        <source>Input the new height of selected rows</source>
        <translation>Geben Sie die neue Hohe der gewahlte Kolonne ein</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Anschluss</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Page %1</source>
        <translation>Seite %1</translation>
    </message>
    <message>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alert threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1/table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1/table.pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t print %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasTableDisplayCell</name>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>No display for datas group</source>
        <translation>Keine Ansicht fur Datengruppe</translation>
    </message>
    <message>
        <source>Select a display type</source>
        <translation>Wahlen Sie einen Ansichttyp</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;Simple</translation>
    </message>
    <message>
        <source>&amp;Multimeter</source>
        <translation>&amp;Multimeter</translation>
    </message>
    <message>
        <source>&amp;Analog</source>
        <translation>&amp;Analog</translation>
    </message>
    <message>
        <source>&amp;Digital</source>
        <translation>Digitaleingang</translation>
    </message>
    <message>
        <source>&amp;Classical oscilloscope</source>
        <translation>&amp;Oszilloskop</translation>
    </message>
    <message>
        <source>Oscilloscope multiple formats</source>
        <translation>Oszilloskop Sollwerts</translation>
    </message>
    <message>
        <source>&amp;BarGraph</source>
        <translation>&amp;BarGraph</translation>
    </message>
    <message>
        <source>Datas &amp;table</source>
        <translation>Daten&amp;Tabelle</translation>
    </message>
    <message>
        <source>No display for this type of data</source>
        <translation>Keine Ansicht fur diesen Datentyp</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasTableSetup</name>
    <message>
        <source>Datas Table Settings</source>
        <translation>Daten Tabelle Sollwerts</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDial</name>
    <message>
        <source>Minimum value</source>
        <translation>Minimaler Wert</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation>Maximaler Wert</translation>
    </message>
    <message>
        <source>Scale step</source>
        <translation>Skala Schritt</translation>
    </message>
</context>
<context>
    <name>CYDialScaleDialog</name>
    <message>
        <source>Scale setup</source>
        <translation>Scale setup</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplay</name>
    <message>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>Automatic title</source>
        <translation>Automatischer Titel</translation>
    </message>
    <message>
        <source>Edit the data&apos;s label to change the title.</source>
        <translation>Schaut den Datenlabel um den Titel zu andern.</translation>
    </message>
    <message>
        <source>Enter the title in the box below.</source>
        <translation>Name des Titels in dem Fenster unten eingeben.</translation>
    </message>
    <message>
        <source>Add unit to the title</source>
        <translation>Einheit zum Titel beifugen</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <source>Font size</source>
        <translation>Zeichensatz Grosse</translation>
    </message>
    <message>
        <source>&lt;qt&gt;&lt;p&gt;This is a data display. To customize a sensor display click and hold the right mouse button on either the frame or the display box and select the &lt;i&gt;Properties&lt;/i&gt; entry from the popup menu. Select &lt;i&gt;Remove&lt;/i&gt; to delete the display from the work sheet.&lt;/p&gt;%1&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;&lt;p&gt;Das ist eine Data Ansiht. Um eine Fuhler Ansicht zu personalisieren klick und behalten Sie die rechte Taste der Maus gedruckt entweder auf dem Rahmen oder Ansicht Box und wahlen Sie die &lt;i&gt;Eigenschaften&lt;/i&gt; Eingabe vom Popup Menu. Wahlen Sie&lt;i&gt;Entfernen&lt;/i&gt; um die Ansicht vom Arbeitsbogen zu loschen.&lt;/p&gt;%1&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>Create a new file %1</source>
        <translation>Neue Datei erzugen %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML ! %2: %3 %4</source>
        <translation>Die Datei %1 enthalt keine gultige XML ! %2: %3 %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">Kann die Datei nicht speichern %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Kann die Datei nicht speichern %1</translation>
    </message>
    <message>
        <source>Display</source>
        <translation>Display</translation>
    </message>
    <message>
        <source>&amp;Setup update interval</source>
        <translation>&amp;Setup Update Interval</translation>
    </message>
    <message>
        <source>&amp;Continue update</source>
        <translation>&amp;Continue update</translation>
    </message>
    <message>
        <source>P&amp;ause update</source>
        <translation>P&amp;ause update</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayCell</name>
    <message>
        <source>The clipboard does not contain a valid display description.</source>
        <translation>Der Clipboard enthalt keine gultige Ansichtsbeschreibung</translation>
    </message>
    <message>
        <source>Do you really want to delete the display?</source>
        <translation>Mochten Sie wirklich die Ansicht loschen?</translation>
    </message>
</context>
<context>
    <name>CYDisplayDummy</name>
    <message>
        <source>Empty cell</source>
        <translation>Leere Zelle</translation>
    </message>
    <message>
        <source>analyse sheet</source>
        <translation>Analysebogen</translation>
    </message>
    <message>
        <source>datas table</source>
        <translation>Daten Tabelle</translation>
    </message>
    <message>
        <source>This is an empty cell in an %1. Drag a data or group of datas from the data browser and drop it here. A data(s) display will appear that allows you to monitor the values of the data(s) over time.</source>
        <translation>Das ist eine leere Zelle in einem %1. Machen Sie eine Drag and Drop an diesem Ort mit einer Data oder Dategruppe vom Databrowser. Eine Daten Ansicht erscheint wo Sie die Datenwerte uber Zeit kontrollieren konnen. </translation>
    </message>
    <message>
        <source>Dummy display</source>
        <translation>Dummy display</translation>
    </message>
    <message>
        <source>&amp;Select a display type</source>
        <translation>Wahlen Sie einen Ansichttyp</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;Simple</translation>
    </message>
    <message>
        <source>&amp;Multimeter</source>
        <translation>&amp;Multimeter</translation>
    </message>
    <message>
        <source>&amp;Analog</source>
        <translation>&amp;Analog</translation>
    </message>
    <message>
        <source>&amp;Digital</source>
        <translation>Digitaleingang</translation>
    </message>
    <message>
        <source>&amp;Classical oscilloscope</source>
        <translation>&amp;Oszilloskop</translation>
    </message>
    <message>
        <source>Oscilloscope multiple formats</source>
        <translation>Oszilloskop Sollwerts</translation>
    </message>
    <message>
        <source>&amp;BarGraph</source>
        <translation>&amp;BarGraph</translation>
    </message>
    <message>
        <source>Datas &amp;table</source>
        <translation>Daten&amp;Tabelle</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayItem</name>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayListView</name>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Anschluss</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>Designer Name</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplaySatusIndicator</name>
    <message>
        <source>Connection is OK.</source>
        <translation>Anschluss ist OK.</translation>
    </message>
    <message>
        <source>Connection has been lost.</source>
        <translation>Anschluss verloren</translation>
    </message>
    <message>
        <source>Update is pausing.</source>
        <translation>Update macht Pause.</translation>
    </message>
    <message>
        <source>The refresh of all graphic analyzers is pausing.</source>
        <translation>Die Wiederholung aller Graphikanalyser ist in Pause.</translation>
    </message>
</context>
<context>
    <name>CYDisplaySimple</name>
    <message>
        <source>Simple display</source>
        <translation>Einfache Ansicht</translation>
    </message>
    <message>
        <source>Numerical base</source>
        <translation>Nummerische Basis</translation>
    </message>
    <message>
        <source>The numerical base is used only if the data is only an unsigned integer type!</source>
        <translation>Die nummerische Basis wird nur benutzt wenn die Angabe vom ungezeichneten ganzen Zahl Typ ist!</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>Binary</translation>
    </message>
    <message>
        <source>Decimal</source>
        <translation>Dezimal</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation>Hexadezimal</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <source>Bicolor</source>
        <translation>Zweifarbig</translation>
    </message>
    <message>
        <source>Color On</source>
        <translation>Farbe On</translation>
    </message>
    <message>
        <source>Color Off</source>
        <translation>Farbe Off</translation>
    </message>
    <message>
        <source>Shape</source>
        <translation>Form</translation>
    </message>
    <message>
        <source>Rectangular</source>
        <translation>Viereckig</translation>
    </message>
    <message>
        <source>Circular</source>
        <translation>Rund</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Look</translation>
    </message>
    <message>
        <source>Flat</source>
        <translation>Flat</translation>
    </message>
    <message>
        <source>Raised</source>
        <translation>Im Relief</translation>
    </message>
    <message>
        <source>Sunken</source>
        <translation>Tief</translation>
    </message>
    <message>
        <source>Customized colors</source>
        <translation>kundenspezifische Farbe</translation>
    </message>
    <message>
        <source>The colors will be those of data</source>
        <translation>Die Farben werden diejenige der Daten sein</translation>
    </message>
    <message>
        <source>Text color of valid value</source>
        <translation>Textfarbe der gultigen Werts</translation>
    </message>
    <message>
        <source>Text color of not valid value</source>
        <translation>Textfarbe der ungultiger Werts</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Hintergrund Farbe</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show unit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplaySimpleSetup</name>
    <message>
        <source>Simple Display Settings</source>
        <translation>Einfach Display Einstellungen</translation>
    </message>
    <message>
        <source>&amp;General</source>
        <translation>&amp;Allgemein</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>&amp;Style</source>
        <translation>&amp;Stil</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation>Farben</translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation>&amp;Data</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayStyle</name>
    <message>
        <source>Color %1</source>
        <translation>Farbe %1</translation>
    </message>
</context>
<context>
    <name>CYDisplayStyleSetup</name>
    <message>
        <source>Global Style Settings</source>
        <translation>Global Style Sollwerts</translation>
    </message>
    <message>
        <source>&amp;Apply to current analyse sheet</source>
        <translation>&amp;Anwenden bei laufendem Analysebogen</translation>
    </message>
    <message>
        <source>Display Style</source>
        <translation>Stil anzeigen</translation>
    </message>
    <message>
        <source>&amp;Font Size</source>
        <translation>&amp;Zeichensatz Grosse</translation>
    </message>
    <message>
        <source>&amp;Grid Color</source>
        <translation>&amp;Gitter Farbe</translation>
    </message>
    <message>
        <source>A&amp;larm Color</source>
        <translation>A&amp;larm Farbe</translation>
    </message>
    <message>
        <source>Foreground Color &amp;1</source>
        <translation>Vordergrund Farbe &amp;1</translation>
    </message>
    <message>
        <source>Foreground Color &amp;2</source>
        <translation>Vordergrund Farbe &amp;2</translation>
    </message>
    <message>
        <source>Datas Colors</source>
        <translation>Daten Farben</translation>
    </message>
    <message>
        <source>Change Color</source>
        <translation>Farbe wechseln</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Background Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayTimerSetup</name>
    <message>
        <source>Timer Settings</source>
        <translation>Timer Sollwerts</translation>
    </message>
    <message>
        <source>Update Interval</source>
        <translation>Update Interval</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEvent</name>
    <message>
        <source>Event</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <source>Acknow.</source>
        <translation>Quittierung</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
</context>
<context>
    <name>CYEventsBackup</name>
    <message>
        <source>Backup date</source>
        <translation>Backup date</translation>
    </message>
    <message>
        <source>Select a date lower than today&apos;s.</source>
        <translation>Wahlen Sie eine Data kleiner als die heutige.</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsGenerator</name>
    <message>
        <source>Events</source>
        <translation>Ereignisse</translation>
    </message>
</context>
<context>
    <name>CYEventsGeneratorSPV</name>
    <message>
        <source>Supervisor Events</source>
        <translation>Supervisor Ereignisse</translation>
    </message>
    <message>
        <source>Initializing supervisor events...</source>
        <translation>Initialisierung Supervisor-Ereignisse</translation>
    </message>
    <message>
        <source>Supervisor starting</source>
        <translation>Supervisor Start</translation>
    </message>
    <message>
        <source>Supervisor stoping</source>
        <translation>Supervisor Stop</translation>
    </message>
    <message>
        <source>New calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>New archive: DATA_REC1</source>
        <translation>Neue Archiv: DATA_REC1</translation>
    </message>
    <message>
        <source>RS422 communication module not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The RS422 communication module has not been correctly installed. Reinstall Cylix.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ethernet interprocess communication not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Ethernet interprocess communication software was not detected at startup Cylix. Reinstall Cylix.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ethernet interprocess communication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cylix cannot communicate with the Ethernet interprocess communication software. Restart PC.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time stamp of starting the cylix software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time stamp of stopping the cylix software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sensor calibration by the metrology tool.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time stamp of new archive generated by the acquisition tool.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please note that under Windows you have to install Cylix as administrator even if the current Windows session is already in administrator mode. To do this, right-click on the installation file to access this command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notifies the sending of the project to the numerical regulator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This sending is also done automatically when the communication is detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot create file : DATA_REC1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check access rights to the acquisition directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="unfinished">Warnung</translation>
    </message>
</context>
<context>
    <name>CYEventsList</name>
    <message>
        <source>One or more data (measurements, setpoints, parameters...) allow to visualize directly values which can influence the event triggering. These values, in bold, are refreshed when the help is displayed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>One or more hyperlinks allow to open the Cylix manual directly at a precise place, as for example the part relating to the setting which can influence the event triggering.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>These links, which are only visible in the tooltip, can be activated at the bottom of the window of events logs.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsMailingWidget</name>
    <message>
        <source>&amp;Test</source>
        <translation>&amp;Test</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>You must be in administrator access to modifie this values</source>
        <translation>Sie mussen im Administrator Zugriff sein um diese Werte zu andern</translation>
    </message>
    <message>
        <source>SMTP servor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsManager</name>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML</source>
        <translation>Die Datei %1 enthalt keine gultige XML</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">Kann die Datei nicht speichern %1!</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Kann die Datei nicht speichern %1</translation>
    </message>
    <message>
        <source>Cannot find the events generator %1</source>
        <translation>Kann nicht den Ereignisses Generator %1 finden</translation>
    </message>
    <message>
        <source>Events list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name, message and help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsPreferences</name>
    <message>
        <source>Events settings</source>
        <translation>Ereignisse Einstellungen</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>Mail</source>
        <translation>Mail</translation>
    </message>
    <message>
        <source>&amp;Designer Value</source>
        <translation>&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsReport</name>
    <message>
        <source> COLOR	TIME	SINCE	TYPE	FROM	DESCRIPTION	NAME
</source>
        <translation>FARBE	DATUM ZEIT	TYP	VON	BESCHREIBUNG	NAME
</translation>
    </message>
    <message>
        <source>Events report (%1) of %2 since %3

</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsView</name>
    <message>
        <source>Date and hour</source>
        <translation>Datum und Zeit</translation>
    </message>
    <message>
        <source>Events file</source>
        <translation>Ereignisse-Datei</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Since</source>
        <translation>Skala</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>From</source>
        <translation>Vom</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>The test directory of this events file has been moved or removed.</source>
        <translation>Das Testverzeichnis dieser Ereignisdatei ist versetzt oder geloscht worden.</translation>
    </message>
    <message>
        <source>This events file has been moved or removed.</source>
        <translation>Diese Ereignisse-Datei ist versetzt oder geloscht worden.</translation>
    </message>
    <message>
        <source>Select a events report to load</source>
        <translation>Wahlen Sie einen Ereignisbericht zum aufladen</translation>
    </message>
    <message>
        <source>The current events file changed!
Do you want to view this one?</source>
        <translation>Die laufende Ereignisdatei ist geandert worden!
Mochten Sie diesen ansehen ?</translation>
    </message>
    <message>
        <source>No help provided !</source>
        <translation>Keine Hilfeseite vorhanden !</translation>
    </message>
</context>
<context>
    <name>CYEventsWindow</name>
    <message>
        <source>Events reports</source>
        <translation>Ereignisprotokolle</translation>
    </message>
    <message>
        <source>Settings events...</source>
        <translation>Settings events...</translation>
    </message>
    <message>
        <source>&amp;Current</source>
        <translation>&amp;Current</translation>
    </message>
    <message>
        <source>&amp;Backup</source>
        <translation>&amp;Backup</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <source>&amp;None</source>
        <translation>&amp;Keine</translation>
    </message>
    <message>
        <source>Their end</source>
        <translation>Ihr Ende</translation>
    </message>
    <message>
        <source>Their acknowledgement</source>
        <translation>Ihre Quittierung</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open &amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;From</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYExtMeasure</name>
    <message>
        <source>Unit</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Freigeben</translation>
    </message>
    <message>
        <source>Scaling</source>
        <translation>Scaling</translation>
    </message>
    <message>
        <source>High value</source>
        <translation>Hoher Wert</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation>Niedrieger Wert</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation>Kontrolle</translation>
    </message>
    <message>
        <source>Level</source>
        <translation>Level</translation>
    </message>
    <message>
        <source>Alert tolerance interval</source>
        <translation>Warnung Toleranzintervall</translation>
    </message>
    <message>
        <source>Fault tolerance interval</source>
        <translation>Fehler Toleranzintervall</translation>
    </message>
    <message>
        <source>Calibration type</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
</context>
<context>
    <name>CYExtMeasureEdit</name>
    <message>
        <source>#</source>
        <translation type="vanished">#</translation>
    </message>
    <message>
        <source>Scale adjust points</source>
        <translation>Skala Einstellungspunkte</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Nieder</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Hoch</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation>Kontrolle</translation>
    </message>
    <message>
        <source>± </source>
        <translation>± </translation>
    </message>
    <message>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYFileCSV</name>
    <message>
        <source>Maximum of lines per file</source>
        <translation>Maximale Linienanzahl pro Erfassungsdatei</translation>
    </message>
    <message>
        <source>Maximum file size</source>
        <translation>Maximale Dateigrosse</translation>
    </message>
</context>
<context>
    <name>CYFlag</name>
    <message>
        <source>If it is:</source>
        <translation>Wenn es so ist:</translation>
    </message>
    <message>
        <source>If you select:</source>
        <translation>Wenn sie eingeben:</translation>
    </message>
    <message>
        <source>Designer value:</source>
        <translation>Herstellerwert:</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>Binary</translation>
    </message>
</context>
<context>
    <name>CYFlagDialog</name>
    <message>
        <source>Dialog input of flag data</source>
        <translation>Dialogeingabe für Flag-Daten</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYFlagInput</name>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYG7Cnt</name>
    <message>
        <source>Counter %1 of grafcet %2</source>
        <translation>Zahler %1 vom Grafcet %2</translation>
    </message>
    <message>
        <source>Grafcet counter</source>
        <translation>Grafcet Zahler</translation>
    </message>
</context>
<context>
    <name>CYG7Tim</name>
    <message>
        <source>Timer %1 of grafcet %2</source>
        <translation>Timer %1 vom Grafcet %2</translation>
    </message>
    <message>
        <source>Grafcet timer</source>
        <translation>Grafcet timer</translation>
    </message>
</context>
<context>
    <name>CYImageList</name>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Die Datei %1 enthalt keine gultige XML
%2: Linie:%3 Kolonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">Kann die Datei nicht speichern %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Kann die Datei nicht speichern %1</translation>
    </message>
</context>
<context>
    <name>CYImageListEdit</name>
    <message>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <source>Fichier</source>
        <translation>Filter</translation>
    </message>
    <message>
        <source>Dele&amp;te</source>
        <translation>Skala Einstellungspunkte</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Canc&amp;el</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>N°</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose an image to add into this list.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYListCali</name>
    <message>
        <source>Inputs / Outputs Calibration</source>
        <translation>Eingange/Ausgange Kalibrierung</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Ana.</source>
        <translation>Ana.</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMV</name>
    <message>
        <source>Forward</source>
        <translation>Vorwarts</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation>Ruckwarts</translation>
    </message>
    <message>
        <source>Sensor loss timer %1</source>
        <translation>Sensorverlust Timer %1</translation>
    </message>
    <message>
        <source>Maximum delay before the falling edge of the backward sensor.</source>
        <translation>Maximale Zeitdauer vor Neigung des Vorwartssensors.</translation>
    </message>
    <message>
        <source>If this time is reached, a failure is signalled.</source>
        <translation>Ist diese Zeit erreicht, so wird  eine Storung gemeldet.</translation>
    </message>
    <message>
        <source>Sensor appearence timer %1</source>
        <translation>Sensoranwesenheit Timer %1</translation>
    </message>
    <message>
        <source>Maximum delay before the rising edge of the forward sensor.</source>
        <translation>Maximale Zeitdauer vor Steigung des Vorwartssensors.</translation>
    </message>
    <message>
        <source>Maximum delay before the falling edge of the forward sensor.</source>
        <translation>Maximale Zeitdauer vor Neigung des Vorwartssensors.</translation>
    </message>
    <message>
        <source>Maximum delay before the rising edge of the backward sensor.</source>
        <translation>Maximale Zeitdauer vor Steigung des Ruckwartssensors.</translation>
    </message>
    <message>
        <source>Sensor failure signalisation %1</source>
        <translation>Sensorfehler Meldung %1</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>A faillure generates a fault.</source>
        <translation>Eine Storung erzeugt einen Fehler.</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>A faillure generates an alert and then the timers simulate the sensors.</source>
        <translation>Eine Storung erzeugt eine Warnung und der Timer simuliert dann den Sensor.</translation>
    </message>
    <message>
        <source>Initialized movement</source>
        <translation>Initialisierte Bewegung</translation>
    </message>
    <message>
        <source>Positioning</source>
        <translation>Positionierung</translation>
    </message>
    <message>
        <source>Grafcet current state</source>
        <translation>Grafcet laufender Zustand</translation>
    </message>
    <message>
        <source>No sensor presence %1</source>
        <translation>Keine Sensor-Anwesenheit %1</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>Prozess</translation>
    </message>
    <message>
        <source>The maximum waiting time of rising edge of the sensor of end of forward stroke has been exceeded by the movement forward.</source>
        <translation>Die maximale Wartezeit der steigenden Kante des Sensors zum Vorlaufende ist während der Vorwärts-Bewegung überschritten worden</translation>
    </message>
    <message>
        <source>This may indicate a sensor failure or movement.</source>
        <translation>Das kann einen Fehler des Sensors oder der Bewegung anzeigen.</translation>
    </message>
    <message>
        <source>No sensor loss %1</source>
        <translation>Kein Sensor-Verlust %1</translation>
    </message>
    <message>
        <source>The maximum waiting time of falling edge of the sensor of end of forward stroke has been exceeded by the movement backward.</source>
        <translation>Die maximale Wartezeit der fallenden Kante des Sensors zum Vorlaufende ist während der Vorwärts-Bewegung überschritten worden</translation>
    </message>
    <message>
        <source>Sensor loss %1</source>
        <translation>Sensor-Verlust %1</translation>
    </message>
    <message>
        <source>The forward sensor was lost in forward static.</source>
        <translation>Die vorwärts-bewegenden Sensor ist in der Vorwärts-Statik verloren worden.</translation>
    </message>
    <message>
        <source>This may indicate an adjustment defect of the sensor.</source>
        <translation>Das kann eine schlechte Einstellung des Sensors anzeigen.</translation>
    </message>
    <message>
        <source>The maximum waiting time of rising edge of the sensor of end of backward stroke has been exceeded by the movement backward.</source>
        <translation>Die maximale Wartezeit der steigenden Kante des Sensors zum Rückwärtsende ist während des Rückwärts-Bewegung überschritten worden</translation>
    </message>
    <message>
        <source>The maximum waiting time of falling edge of the sensor of end of backward stroke has been exceeded by the movement forward.</source>
        <translation>Die maximale Wartezeit der fallenden Kante des Sensors zum Rückwärtsende ist während des Rückwärts-Bewegung überschritten worden</translation>
    </message>
    <message>
        <source>The backward sensor was lost in backward static.</source>
        <translation>Die rückwärts-bewegenden Sensor ist in derRückwärts-Statik verloren worden.</translation>
    </message>
    <message>
        <source>Sensor coherence %1/%2</source>
        <translation>Sensor Konsistenz %1</translation>
    </message>
    <message>
        <source>The sensors forward and backward were actived at the same time.</source>
        <translation>Die Vorwärts- und Rückwärtssensoren sind gleichzeitig aktiviert.</translation>
    </message>
    <message>
        <source>This may indicate an adjustment defect of the sensors.</source>
        <translation>This may indicate an adjustment defect of the sensor.</translation>
    </message>
</context>
<context>
    <name>CYMVInput</name>
    <message>
        <source>Forward motion</source>
        <translation>Vorwartsbewegung</translation>
    </message>
    <message>
        <source>Backward motion</source>
        <translation>Ruckwartsbewegung</translation>
    </message>
</context>
<context>
    <name>CYMainWin</name>
    <message>
        <source>Test: %1 (Access: %2)</source>
        <translation>Test: %1 (Access: %2)</translation>
    </message>
    <message>
        <source>Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Test: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The new language will be taken into account the next time the application will be started.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYManuCali</name>
    <message>
        <source>&amp;Designer value</source>
        <translation>&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>&amp;Low value</source>
        <translation>&amp;Niedrieger Wert</translation>
    </message>
    <message>
        <source>&amp;High value</source>
        <translation>&amp;Hoher Wert</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Anmerkung</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Hoch</translation>
    </message>
    <message>
        <source>High Value</source>
        <translation>Hoher Wert</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Nieder</translation>
    </message>
    <message>
        <source>Low Value</source>
        <translation>Niedrieger Wert</translation>
    </message>
    <message>
        <source>Do you really want to load the designer&apos;s values ?</source>
        <translation>Mochten Sie wirklich die Hersteller Werte aufladen ?</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYManuCaliInput</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMeasure</name>
    <message>
        <source>Flag</source>
        <translation>Flag</translation>
    </message>
    <message>
        <source>Calibration</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>Sensor to calibrate</source>
        <translation>Sensor, der kalibriert werden muss</translation>
    </message>
    <message>
        <source>Sensor calibrated</source>
        <translation>kalibrierter Sensor</translation>
    </message>
    <message>
        <source>Flag of validity of the calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>High value</source>
        <translation>Hoher Wert</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation>Niedrieger Wert</translation>
    </message>
    <message>
        <source>Date of last calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>Date of last verification</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>Date of next calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>If this date is expired an alert will be generated to remind to calibrate the sensor %1.</source>
        <translation>Wenn dieses Datum abgelaufen ist, wird ein Alarm erzeugen, um zu errinern der Sensor %1 zu kalibrieren</translation>
    </message>
    <message>
        <source>Date of next verification</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>Calibration control</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>Alert for next verification</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Anmerkung</translation>
    </message>
    <message>
        <source>Number of points used for calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>Direct bench sensor</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>New point</source>
        <translation>DAtei %1 bei Linie %2</translation>
    </message>
    <message>
        <source>Bench sensor</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Reference sensor</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Direct bench values</source>
        <translation>Warnungswert</translation>
    </message>
    <message>
        <source>Point %1</source>
        <translation>Edit: %1</translation>
    </message>
    <message>
        <source>Bench values</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Reference values</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Calibrating mode</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>Manual calibrating</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>Automatic calibrating</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>Environment</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <source>month(s)</source>
        <translation>Host(s)</translation>
    </message>
    <message>
        <source>Periodicity of control</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation>Operator</translation>
    </message>
    <message>
        <source>Designer values</source>
        <translation>&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Procedure</source>
        <translation>Druck</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Steuerung</translation>
    </message>
    <message>
        <source>Report file</source>
        <translation>Ereignisse-Datei</translation>
    </message>
    <message>
        <source>Event</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>Numeric</source>
        <translation>Nummerische Basis</translation>
    </message>
    <message>
        <source>Sensor sheet</source>
        <translation>Sensor Fehler</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>High value (FS)</source>
        <translation>Hoher Wert</translation>
    </message>
    <message>
        <source>Low value (FS)</source>
        <translation>Niedrieger Wert</translation>
    </message>
    <message>
        <source>Verification in accordance</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>Last calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>Last verification</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>Next verification</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>Calibrating</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>Viewing</source>
        <translation>Display</translation>
    </message>
    <message>
        <source>No calibration</source>
        <translation>Datum der nachsten Kalibrierung</translation>
    </message>
    <message>
        <source>Sensor defect</source>
        <translation type="unfinished">Sensor Fehler</translation>
    </message>
    <message>
        <source>Indicates that the date of calibration of the sensor is exceeded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Indicates that the sensor was calibrated to the onset of this event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>High value (not calibrated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum value of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low value (not calibrated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum value of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generates an alert in case of overtaking of the date of next verification.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Averaging times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Adding a calibration point by reading the average of the direct value of the sensor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Adding a calibration point by entering the direct value of the sensor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Manufacter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>P/N (Type)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>S/N (Serial N)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uncertainty type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uncertainty - % FS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The total measurement uncertainty of the sensor is expressed as a percentage of the Full Scale of the sensor. It is then fixed for each measurement and can also be expressed in sensor unit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uncertainty - fixed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The total measurement uncertainty of the sensor is expressed in sensor unit. It is then fixed for each measurement and can also be expressed as a percentage of the Full Scale of the sensor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uncertainty - % of reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The total measurement uncertainty of the sensor is expressed as a percentage of the read measure.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Equipment reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Verification not in accordance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Editing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum number of ADC points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum number of ADC points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Measure</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMeasureSetting</name>
    <message>
        <source>Measurement setting</source>
        <translation>Ereignisse Einstellungen</translation>
    </message>
    <message>
        <source>Calibration type</source>
        <translation type="vanished">Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>%1: Sensor changed</source>
        <translation>kalibrierter Sensor</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Label</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Freigeben</translation>
    </message>
    <message>
        <source>Scaling</source>
        <translation>Scaling</translation>
    </message>
    <message>
        <source>High value</source>
        <translation>Hoher Wert</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation>Niedrieger Wert</translation>
    </message>
    <message>
        <source>Signal type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Indicates that the type or scale of the sensor have been changed. So, it&apos;s recommended to make a new calibrate of sensor.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMessageBox</name>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Yes</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C&amp;ontinue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMetroCaliInput</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>Direct</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <source>Reference sensor</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Bench sensor</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Average of bench sensor</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Calibrating point &apos;%1&apos;</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>%1 : New point</source>
        <translation>DAtei %1 bei Linie %2</translation>
    </message>
    <message>
        <source>ADC</source>
        <translation>ADC</translation>
    </message>
</context>
<context>
    <name>CYMetroCalibrate</name>
    <message>
        <source>#</source>
        <translation type="vanished">#</translation>
    </message>
    <message>
        <source>Curves</source>
        <translation>Kurven</translation>
    </message>
    <message>
        <source>Points</source>
        <translation>Edit: %1</translation>
    </message>
    <message>
        <source>Mini
(%1)</source>
        <translation>Prufstand</translation>
    </message>
    <message>
        <source>Bench
(%1)</source>
        <translation>Prufstand</translation>
    </message>
    <message>
        <source>Error
(%1)</source>
        <translation>Gruppen</translation>
    </message>
    <message>
        <source>Bench
(ADC)</source>
        <translation>Prufstand</translation>
    </message>
    <message>
        <source>(sec)</source>
        <translation>sec</translation>
    </message>
    <message>
        <source>Gross
(%1)</source>
        <translation>Gruppen</translation>
    </message>
    <message>
        <source>Calibrating sheet &apos;%1&apos; of %2 (Modified)</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>Do you want to validate this calibration for %1 ?</source>
        <translation>Mochten Sie die Untergruppe einschliessen ?</translation>
    </message>
    <message>
        <source>End of verification de %1 !
In accordance ?</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>%1_%2_%3</source>
        <translation>%1s%2</translation>
    </message>
    <message>
        <source>_calibrating</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>_verification</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>%1/%2</source>
        <translation>%1s%2</translation>
    </message>
    <message>
        <source>%1/%2.pdf</source>
        <translation>%1/Kurve_%2.ps</translation>
    </message>
    <message>
        <source>Page %1/%2</source>
        <translation>Seite %1</translation>
    </message>
    <message>
        <source>Page %1/2</source>
        <translation>Seite %1</translation>
    </message>
    <message>
        <source>Infos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maxi uncertainty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mini uncertainty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Linearity error (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error (% FS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Standard
(%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maxi
(%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error
(% FS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Straight
Line
 (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Linearity
error
(% FS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Averaging
time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do you want to close the calibration sheet of %1 without saving the latest changes ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Linearity error (% FS)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMetroSensorSheet</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>#</source>
        <translation type="vanished">#</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Edit sensor sheet &apos;%1&apos;</source>
        <translation>Keine Sensor-Anwesenheit %1</translation>
    </message>
    <message>
        <source>Change sensor sheet &apos;%1&apos;</source>
        <translation>Keine Sensor-Anwesenheit %1</translation>
    </message>
    <message>
        <source>Are you sure to validate this sensor sheet ?
If yes, it is recommended to redo the calibration !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMetroSensorSheetView</name>
    <message>
        <source>Sensor sheet</source>
        <translation>Sensor Fehler</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>High point</source>
        <translation>Kolonne beifugen</translation>
    </message>
    <message>
        <source>Low point</source>
        <translation>Kolonne beifugen</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Anmerkung</translation>
    </message>
</context>
<context>
    <name>CYMetroSettings</name>
    <message>
        <source>Metrology settings</source>
        <translation>Ereignisse Einstellungen</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>Designer &amp;Value</source>
        <translation>&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Clo&amp;se</source>
        <translation>Farben</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>0</source>
        <translation>00</translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMetroWin</name>
    <message>
        <source>With alert</source>
        <translation>Hoher Wert</translation>
    </message>
    <message>
        <source>Without alert</source>
        <translation>Hoher Wert</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <source>Sensors</source>
        <translation>Sensor Fehler</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Sensor sheet</source>
        <translation>Sensor Fehler</translation>
    </message>
    <message>
        <source>Clear table</source>
        <translation>Warnungswert</translation>
    </message>
    <message>
        <source>Add point</source>
        <translation>Kolonne beifugen</translation>
    </message>
    <message>
        <source>Delete point</source>
        <translation>Skala Einstellungspunkte</translation>
    </message>
    <message>
        <source>Calibrate</source>
        <translation>Kalibrierung Kontrolle</translation>
    </message>
    <message>
        <source>Verification</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>Designer values</source>
        <translation>&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Metrology settings</source>
        <translation>Ereignisse Einstellungen</translation>
    </message>
    <message>
        <source>Metrology tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMultimeter</name>
    <message>
        <source>Multimeter</source>
        <translation>Multimeter</translation>
    </message>
    <message>
        <source>Edit the data&apos;s label to change the title</source>
        <translation>Schaut den Datenlabel um den Titel zu andern</translation>
    </message>
    <message>
        <source>Analog display</source>
        <translation>Analog Ansicht</translation>
    </message>
    <message>
        <source>Digital display</source>
        <translation>Digitaleingang</translation>
    </message>
    <message>
        <source>Numerical base</source>
        <translation>Nummerische Basis</translation>
    </message>
    <message>
        <source>The numerical base is used only if the data is only an unsigned integer type!</source>
        <translation>Die nummerische Basis wird nur benutzt wenn die Angabe vom ungezeichneten ganzen Zahl Typ ist!</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>Binary</translation>
    </message>
    <message>
        <source>Decimal</source>
        <translation>Dezimal</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation>Hexadezimal</translation>
    </message>
    <message>
        <source>Alarm</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>Upper bound value</source>
        <translation>Oberer abhangiger Wert</translation>
    </message>
    <message>
        <source>Upper alarm enable</source>
        <translation>Oberwarnung erlaubt</translation>
    </message>
    <message>
        <source>Lower bound value</source>
        <translation>Niedrieger Wert</translation>
    </message>
    <message>
        <source>Lower alarm enable</source>
        <translation>Niederwarnung erlaubt</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <source>Analog</source>
        <translation>Analog</translation>
    </message>
    <message>
        <source>Normal color</source>
        <translation>Normale Farbe</translation>
    </message>
    <message>
        <source>Alarm color</source>
        <translation>Warnung Farbe</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Hintergrund Farbe</translation>
    </message>
    <message>
        <source>Text color</source>
        <translation>Text Farbe</translation>
    </message>
    <message>
        <source>Digital</source>
        <translation>Digitaleingang</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMultimeterSetup</name>
    <message>
        <source>Multimeter Settings</source>
        <translation>Multimeter Sollwerts</translation>
    </message>
    <message>
        <source>&amp;General</source>
        <translation>&amp;Allgemein</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>&amp;Style</source>
        <translation>&amp;Stil</translation>
    </message>
    <message>
        <source>Analog</source>
        <translation>Analog</translation>
    </message>
    <message>
        <source>Digital</source>
        <translation>Digitaleingang</translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation>&amp;Data</translation>
    </message>
    <message>
        <source>Analog scale</source>
        <translation>Analogskala</translation>
    </message>
    <message>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYNetConnect</name>
    <message>
        <source>Connect Host</source>
        <translation>Connect Host</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <source>Select the name of the host you want to connect to.</source>
        <translation>Bitte wahlen Sie den Namen des Hosts den Sie anschliessen mochten</translation>
    </message>
    <message>
        <source>Co&amp;nnect</source>
        <translation>Anschliessen</translation>
    </message>
    <message>
        <source>Configure connections</source>
        <translation>Anschlusse konfigurieren</translation>
    </message>
    <message>
        <source>COM</source>
        <translation>COM</translation>
    </message>
    <message>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYNetLink</name>
    <message>
        <source>Connected</source>
        <translation>Connected</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Disconnected</translation>
    </message>
    <message>
        <source>%1 kHz</source>
        <translation>%1 kHz</translation>
    </message>
    <message>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
</context>
<context>
    <name>CYNetR422F</name>
    <message>
        <source>O&amp;k</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Are you sure to update this regulator !</source>
        <translation>Kann das Verzeichnis %1 nicht erstellen</translation>
    </message>
    <message>
        <source>Install the content of this archive on this regulator !</source>
        <translation>Kann das Verzeichnis %1 nicht erstellen</translation>
    </message>
    <message>
        <source>Cannot open the init file !</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>Cannot open the file &apos;%1&apos; !</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>Install %1 in %2</source>
        <translation>DAtei %1 bei Linie %2</translation>
    </message>
    <message>
        <source>Save %1 in %2</source>
        <translation>DAtei %1 bei Linie %2</translation>
    </message>
    <message>
        <source>Regulator ok !</source>
        <translation>Regler</translation>
    </message>
    <message>
        <source>Regulator disconnected.</source>
        <translation>Disconnected</translation>
    </message>
    <message>
        <source>Cannot reboot the regulator !</source>
        <translation>Kann das Verzeichnis %1 nicht erstellen</translation>
    </message>
    <message>
        <source>Cannot read info datas from regulator !</source>
        <translation>Kann das Verzeichnis %1 nicht erstellen</translation>
    </message>
    <message>
        <source>Cannot reconnect to the regulator !</source>
        <translation>Kann das Verzeichnis %1 nicht erstellen</translation>
    </message>
    <message>
        <source>Regulator rebooting...</source>
        <translation>Regler</translation>
    </message>
    <message>
        <source>No file name from the regulator !</source>
        <translation>Kann das Verzeichnis %1 nicht erstellen</translation>
    </message>
    <message>
        <source>Cannot open file &apos;%1&apos; !</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>Cannot find directory &apos;%1&apos; !</source>
        <translation>Kann das Verzeichnis zum Backup nicht finden</translation>
    </message>
    <message>
        <source>Cannot find file &apos;%1&apos; !</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>File transfert</source>
        <translation>Filter</translation>
    </message>
    <message>
        <source>File exchange with regulator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Doesn&apos;t exist &apos;%1&apos; !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updating of regulator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Its serial number must be %1, while the software you want install has a as serial number %2 !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update of regulator from the version %1 to the version %2. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure to install on the regulator the version %1 of the software %2 !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot remove the old directory %1 !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot start uncompressing file of update (%1)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot finish uncompressing file of update (%1)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Init file doesn&apos;t exist !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section %1 unknown in the init file !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The destination directory path &apos;%1&apos; is too long !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The source directory path &apos;%1&apos; is too long !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The file name &apos;%1&apos; is not correct for transfert file !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The file name &apos;%1&apos; is too long !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&apos;%1&apos; doesn&apos;t exist !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File %1 is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regulator is blocked in file transfert mode !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>YOU MUST REBOOT MANUALLY THE REGULATOR !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop of communication driver in normal mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop of communication driver in file transfert mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Waiting for disconnection (Maximum time %1 sec)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restart of communication driver in file transfert mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restart of communication driver in normal mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regulator reconnected in normal mode instead of file transfert mode !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regulator reconnected in file transfert mode (R422F %1).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Waiting for reconnection (Maximum time %1 sec)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regulator reconnected in file transfert mode (R422F %1) instead of normal mode !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regulator reconnected in normal mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting file transfert...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No file to transfert !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Install : %1 (%2 %)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No answer from the regulator !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot remove file &apos;%1\%2&apos; on the regulator !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot make file &apos;%1\%2&apos; on the regulator !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Block number error &apos;%1&apos; !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File size error &apos;%1\%2&apos; !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Regulator state unknown : &apos;%1&apos; !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bad size block to read !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bad num block to read !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save : %1 (%2 %)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>End of file transfert.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can&apos;t close this box before the end of the transfert !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYNumDialog</name>
    <message>
        <source>Dialog input of numerical data</source>
        <translation>Dialogeingabe für numerische Daten</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYNumDisplay</name>
    <message>
        <source>The input text must be a numeric value !</source>
        <translation>Der Eingabetext muss ein DIgitalwert sein !</translation>
    </message>
    <message>
        <source>Cannot find the data %1</source>
        <translation>Kann die Data %1 nicht finden</translation>
    </message>
</context>
<context>
    <name>CYNumInput</name>
    <message>
        <source>Can&apos;t represent value %1 in terms of fixed-point numbers with precision %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t represent value %1 in terms of fixed-point numbers with precision %2 %3/%4 = %5</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPID</name>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>Proportional</source>
        <translation>Proportional</translation>
    </message>
    <message>
        <source>Proportional Integral</source>
        <translation>Proportionale Integral</translation>
    </message>
    <message>
        <source>Proportional Derivative</source>
        <translation>Proportionale Ableitung</translation>
    </message>
    <message>
        <source>Proportional Integral Derivative</source>
        <translation>Proportionale Integralableitung</translation>
    </message>
    <message>
        <source>Integral</source>
        <translation>Integral</translation>
    </message>
    <message>
        <source>Proportional band coefficient</source>
        <translation>Proportionaler Bandkoeffizient</translation>
    </message>
    <message>
        <source>Proportional coefficient</source>
        <translation>Proportionaler Koeffizient</translation>
    </message>
    <message>
        <source>If 0, the proportional control won&apos;t be active (It can work in integral mode)</source>
        <translation>Falls 0, wird die proportionale Kontrolle nicht aktiv (es kann im Integralmodus arbeiten)</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>Diese Kontrolle ist eine  Misch- P.I.D.</translation>
    </message>
    <message>
        <source>Proportional band</source>
        <translation>Proportionaler Band</translation>
    </message>
    <message>
        <source>Integral coefficient</source>
        <translation>Integralkoefficient</translation>
    </message>
    <message>
        <source>If 0, the integral control won&apos;t be in operation.</source>
        <translation>Falls 0, wird keine Integralkontrolle wirken.</translation>
    </message>
    <message>
        <source>Integral/derivative ratio</source>
        <translation>Integral/Ableitung Verhaltnis</translation>
    </message>
    <message>
        <source>According to Ziegler and Nichols the best ratio is 4.0.</source>
        <translation>Nach Ziegler und Nichols liegt das beste Verhaltnis bei 4.0.</translation>
    </message>
    <message>
        <source>If 0, the derivative control won&apos;t be in operation.</source>
        <translation>Falls 0, wird keine Ableitungkontrolle wirken.</translation>
    </message>
    <message>
        <source>Negative output limit</source>
        <translation>Negativer Ausgangsgrenzwert</translation>
    </message>
    <message>
        <source>Output limit.</source>
        <translation>Ausgangsgrenzwert</translation>
    </message>
    <message>
        <source>Positive output limit</source>
        <translation>Positiver Ausgangsgrenzwert</translation>
    </message>
    <message>
        <source>Derivative type</source>
        <translation>Ableitungtyp</translation>
    </message>
    <message>
        <source>On the gap</source>
        <translation>On the gap</translation>
    </message>
    <message>
        <source>On the measure.</source>
        <translation>Bei der Messung.</translation>
    </message>
    <message>
        <source>On the setpoint.</source>
        <translation>Bei dem Sollwert</translation>
    </message>
    <message>
        <source>If you select:</source>
        <translation>Wenn sie eingeben:</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The derivative calculation will be done on the gap (setpoint - measure).</source>
        <translation>Das kann nutzlich sein um eine Uberschreitung am Ende der Steigung zu vermeiden. Die Ableitungkalkulation wird beim Gap (Einstellung - Messung) durchgefuhrt.</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure.</source>
        <translation>Die Ableitungkalkulation wird auf dem Mass durchgefuhrt.</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the setpoint.</source>
        <translation>Die Ableitungkalkulation wird auf dem Sollwert durchgefuhrt.</translation>
    </message>
    <message>
        <source>Measure derivative</source>
        <translation>Ableitungmass</translation>
    </message>
    <message>
        <source>Ramp on PID setpoint</source>
        <translation>Steigung bei PID Sollwert</translation>
    </message>
    <message>
        <source>The PID setpoint will be a ramp form one.</source>
        <translation>Die PID Sollwert ist einer Steigungform ahnlich.</translation>
    </message>
    <message>
        <source>It will be the final value (No ramp).</source>
        <translation>Es wird der Schlusswert sein (keine Steigung).</translation>
    </message>
    <message>
        <source>Integral frozen in ramp</source>
        <translation>Integral erstarren in Steigung</translation>
    </message>
    <message>
        <source>The integral value will frozen in ramp.</source>
        <translation>Das Integral wird in der Steigung erstarren.</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The integral value will be modified normally by the PID control.</source>
        <translation>Das kann nutzlich sein um eine Uberschreitung am Ende der Steigung zu vermeiden. Der Integralwert wird normalerweise verandert bei der PID Kontrolle.</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure. The derivative effect is based on the error variation : setpoint – measurement. This may be useful to avoid overshoot at the ramp end.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The derivative effect is based on the setpoint variation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPIDCalculD</name>
    <message>
        <source>Measure derivative</source>
        <translation>Ableitungmass</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure.</source>
        <translation>Die Ableitungkalkulation wird auf dem Mass durchgefuhrt.</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The derivative calculation will be done on the gap (setpoint - measure).</source>
        <translation>Das kann nutzlich sein um eine Uberschreitung am Ende der Steigung zu vermeiden. Die Ableitungkalkulation wird beim Gap (Einstellung - Messung) durchgefuhrt.</translation>
    </message>
</context>
<context>
    <name>CYPIDI</name>
    <message>
        <source>Integral coefficient</source>
        <translation>Integralkoefficient</translation>
    </message>
    <message>
        <source>If 0, the integral control won&apos;t be in operation.</source>
        <translation>Falls 0, wird keine Integralkontrolle wirken.</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>Diese Kontrolle ist eine  Misch- P.I.D.</translation>
    </message>
</context>
<context>
    <name>CYPIDInput</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>- </source>
        <translation>- </translation>
    </message>
    <message>
        <source>+ </source>
        <translation>+ </translation>
    </message>
    <message>
        <source>PID</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPIDK</name>
    <message>
        <source>Integral/derivative ratio</source>
        <translation>Integral/Ableitung Verhaltnis</translation>
    </message>
    <message>
        <source>According to Ziegler and Nichols the best ratio is 4.0.</source>
        <translation>Nach Ziegler und Nichols liegt das beste Verhaltnis bei 4.0.</translation>
    </message>
    <message>
        <source>If 0, the derivative control won&apos;t be in operation.</source>
        <translation>Falls 0, wird keine Ableitungkontrolle wirken.</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>Diese Kontrolle ist eine  Misch- P.I.D.</translation>
    </message>
</context>
<context>
    <name>CYPIDMax</name>
    <message>
        <source>Positive output limit</source>
        <translation>Positiver Ausgangsgrenzwert</translation>
    </message>
    <message>
        <source>Output limit.</source>
        <translation>Ausgangsgrenzwert</translation>
    </message>
</context>
<context>
    <name>CYPIDMin</name>
    <message>
        <source>Negative output limit</source>
        <translation>Negativer Ausgangsgrenzwert</translation>
    </message>
    <message>
        <source>Output limit.</source>
        <translation>Ausgangsgrenzwert</translation>
    </message>
</context>
<context>
    <name>CYPIDNoI</name>
    <message>
        <source>Integral frozen in ramp</source>
        <translation>Integral erstarren in Steigung</translation>
    </message>
    <message>
        <source>The integral value will frozen in ramp.</source>
        <translation>Das Integral wird in der Steigung erstarren.</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The integral value will be modified normally by the PID control.</source>
        <translation>Das kann nutzlich sein um eine Uberschreitung am Ende der Steigung zu vermeiden. Der Integralwert wird normalerweise verandert bei der PID Kontrolle.</translation>
    </message>
</context>
<context>
    <name>CYPIDP</name>
    <message>
        <source>Proportional coefficient</source>
        <translation>Proportionaler Koeffizient</translation>
    </message>
    <message>
        <source>If 0, the proportional control won&apos;t be active (It can work in integral mode)</source>
        <translation>Falls 0, wird die proportionale Kontrolle nicht aktiv (es kann im Integralmodus arbeiten)</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>Diese Kontrolle ist eine  Misch- P.I.D.</translation>
    </message>
</context>
<context>
    <name>CYPIDRamp</name>
    <message>
        <source>Ramp on PID setpoint</source>
        <translation>Steigung bei PID Sollwert</translation>
    </message>
    <message>
        <source>The PID setpoint will be a ramp form one.</source>
        <translation>Die PID Sollwert ist einer Steigungform ahnlich.</translation>
    </message>
    <message>
        <source>It will be the final value (No ramp).</source>
        <translation>Es wird der Schlusswert sein (keine Steigung).</translation>
    </message>
</context>
<context>
    <name>CYPasswordDialog</name>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Keep password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Verify:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password strength meter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The password strength meter gives an indication of the security of the password you have entered. To improve the strength of the password, try:
 - using a longer password;
 - using a mixture of upper- and lower-case letters;
 - using numbers or symbols, such as #, as well as letters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Passwords do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You entered two different passwords. Please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The password you have entered has a low strength. To improve the strength of the password, try:
 - using a longer password;
 - using a mixture of upper- and lower-case letters;
 - using numbers or symbols as well as letters.

Would you like to use this password anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low Password Strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password must be at least 1 character long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password must be at least %1 characters long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Passwords match</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPen</name>
    <message>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <source>No line at all</source>
        <translation>Gar keine Linie</translation>
    </message>
    <message>
        <source>A simple line</source>
        <translation>Eine einfache Linie</translation>
    </message>
    <message>
        <source>Dashes separated by a few pixels</source>
        <translation>Trennstriche mittels einige Pixels getrennt</translation>
    </message>
    <message>
        <source>Dots separated by a few pixels</source>
        <translation>Punkte mittels einige Pixels getrennt</translation>
    </message>
    <message>
        <source>Alternate dots and dashes</source>
        <translation>Alterniert Punkte und Trennstriche</translation>
    </message>
    <message>
        <source>One dash, two dots, one dash, two dots</source>
        <translation>Ein Punkt, zwei Trennstriche, ein Punkt, zwei Trennstriche</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPenDialog</name>
    <message>
        <source>Pen edit dialog</source>
        <translation>Pen edit dialog</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>Stift</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYProject</name>
    <message>
        <source>The file %1 does not contain valid XML !
%2: %3 %4</source>
        <translation>Die Datei %1 enthalt keine gultige XML !
%2: %3 %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">Kann die Datei nicht speichern %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Kann die Datei nicht speichern %1</translation>
    </message>
</context>
<context>
    <name>CYProjectEditor</name>
    <message>
        <source>The selected object is not a file !</source>
        <translation>Das gewahle Objekt ist keine Datei !</translation>
    </message>
    <message>
        <source>Select a project file ends with &quot;.cyprj&quot;.</source>
        <translation>Wahlen Sie eine Projektdatei, die mit &quot;.cyprj&quot; endet.</translation>
    </message>
    <message>
        <source>Cannot find the project !</source>
        <translation>Kann dieses Projekt nicht finden !</translation>
    </message>
    <message>
        <source>The selected file is not a project file !</source>
        <translation>Die gewahle Datei ist keine Projektdatei !</translation>
    </message>
    <message>
        <source>Do you want to load an existing project or create a new project ?</source>
        <translation>Mochten Sie ein vorhandenes Projekt aufladen, oder ein neues Projekt erzeugen ?</translation>
    </message>
    <message>
        <source>Cannot find the current project !</source>
        <translation>Kann das laufende Projekt nicht finden !</translation>
    </message>
    <message>
        <source>&amp;Create</source>
        <translation>&amp;Erzeugen</translation>
    </message>
    <message>
        <source>&amp;Load</source>
        <translation>&amp;Aufladen</translation>
    </message>
    <message>
        <source>If you save this project the restart test will be forbidden !</source>
        <translation>Wenn Sie dieses Projekt speichern wird der Restart Test verboten sein !</translation>
    </message>
    <message>
        <source>The project was modified.
Do you want to save your changes?</source>
        <translation>Das Projekt ist verandert worden.Mochten Sie Ihre Änderungen speichern?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Ungespeicherte Anderungen</translation>
    </message>
    <message>
        <source>View: %1</source>
        <translation>Ansicht: %1</translation>
    </message>
    <message>
        <source> (Execution following ON)</source>
        <translation> (Execution following ON)</translation>
    </message>
    <message>
        <source>Edit: %1</source>
        <translation>Edit: %1</translation>
    </message>
    <message>
        <source>Edit: %1 (modified)</source>
        <translation>Edit: %1 (verandert)</translation>
    </message>
    <message>
        <source>Project error</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <source>Select project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The project cannot be saved because the editor has been open in read only mode!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYProjectTestDirDialog</name>
    <message>
        <source>Test directory</source>
        <translation>Testverzeichnis</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="vanished">textLabel</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Name of the new test directory</source>
        <translation>Name des neuen Testverzeichnisses</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <source>Input the test name directory</source>
        <translation>Name des Testverzeichnisses eingeben</translation>
    </message>
    <message>
        <source>Add &amp;test number</source>
        <translation>Testnummer eingeben</translation>
    </message>
    <message>
        <source>Choose another directory</source>
        <translation>Benutzen Sie das Testverzeichnis </translation>
    </message>
    <message>
        <source>Create a ne&amp;w test directory</source>
        <translation>Neues Testverzeichnis erzeugen</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>If you want to change the test directory, you must change its name !</source>
        <translation>Wenn Sie das Testverzeichnis andern mochten, mussen Sie dessen Namen andern !</translation>
    </message>
    <message>
        <source>This directory already exists !
It may contain results of an other test.
Do you really want to use this directory ?</source>
        <translation>Dieses Verzeichnis ist schon vorhanden !
Es kann Ergebnisse eines enderen Tests enthalten.
Mochten Sie wirklich diese Verzeichnis benutzen ?</translation>
    </message>
</context>
<context>
    <name>CYSMTP</name>
    <message>
        <source>Connected to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simple SMTP client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unexpected reply from SMTP server:

</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScope</name>
    <message>
        <source>On which axis do you want this data must to be ?</source>
        <translation>Auf welcher Achse wunschen Sie diese Daten ?</translation>
    </message>
    <message>
        <source>Axis selection</source>
        <translation>Achse Selektion</translation>
    </message>
    <message>
        <source>&amp;Y</source>
        <translation>&amp;Y</translation>
    </message>
    <message>
        <source>&amp;X</source>
        <translation>&amp;X</translation>
    </message>
    <message>
        <source>This display cannot treat this type of data !</source>
        <translation>Diese Ansicht kann diesen Datentyp nicht verarbeiten !</translation>
    </message>
    <message>
        <source>This data is already displayed in this oscilloscope.</source>
        <translation>Diese Data ist in diesem Oszilloskok schon angegeben</translation>
    </message>
    <message>
        <source>CYLIX: Scope Capture Window</source>
        <translation>CYLIX: Scope Capture Window</translation>
    </message>
    <message>
        <source>Axis</source>
        <translation>Axis</translation>
    </message>
    <message>
        <source>Scope</source>
        <translation>Scope</translation>
    </message>
    <message>
        <source>&amp;Analyse curves</source>
        <translation>&amp;Analyse Kurven</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation> Reset</translation>
    </message>
    <message>
        <source>Pr&amp;int curves</source>
        <translation>Pr&amp;int curves</translation>
    </message>
    <message>
        <source>Export PDF</source>
        <translation>&amp;Export</translation>
    </message>
    <message>
        <source>Export CSV</source>
        <translation>&amp;Export</translation>
    </message>
    <message>
        <source>Reset trigger</source>
        <translation> Reset Modus</translation>
    </message>
    <message>
        <source>This data can not be put into an oscilloscope because it is not buffered!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Setup trigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The previous export not finished!
Renew the export.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeAcquisition</name>
    <message>
        <source>curves</source>
        <translation>Kurven</translation>
    </message>
    <message>
        <source>Acquisition period</source>
        <translation type="unfinished">Erfassungsperiode</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="unfinished">Titel</translation>
    </message>
    <message>
        <source>Exported:</source>
        <translation type="vanished">Exported:</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <source>Saving under</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X decimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y decimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeAnalyser</name>
    <message>
        <source>&amp;Export PDF</source>
        <translation>&amp;Export</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>E&amp;xport CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto zoom Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom &amp;In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom &amp;Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Permut cursors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Main Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeAxis</name>
    <message>
        <source>Left axis</source>
        <translation>Linke Achse</translation>
    </message>
    <message>
        <source>Right axis</source>
        <translation>Rechte Achse</translation>
    </message>
    <message>
        <source>Bottom axis</source>
        <translation>Achsefuss</translation>
    </message>
    <message>
        <source>Top axis</source>
        <translation>Hohe Achse</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation>Maximaler Wert</translation>
    </message>
    <message>
        <source>Minimum value</source>
        <translation>Minimaler Wert</translation>
    </message>
    <message>
        <source>Auto scale</source>
        <translation>Analogskala</translation>
    </message>
    <message>
        <source>Scale settable</source>
        <translation>Skala Schritt</translation>
    </message>
    <message>
        <source>Left value</source>
        <translation>Linker Wert</translation>
    </message>
    <message>
        <source>Right value</source>
        <translation>Rechter Wert</translation>
    </message>
    <message>
        <source>Shown</source>
        <translation>Anzeigen</translation>
    </message>
    <message>
        <source>Show axis</source>
        <translation>Achse anzeigen</translation>
    </message>
    <message>
        <source>Hide axis</source>
        <translation>Achse verstecken</translation>
    </message>
    <message>
        <source>Logarithmic scale</source>
        <translation>Logarithmic Skala</translation>
    </message>
    <message>
        <source>Enable logarithmic scale</source>
        <translation>Enable logarithmic scale</translation>
    </message>
    <message>
        <source>Disable logarithmic scale</source>
        <translation>Disable logarithmic scale</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Ermoglicht</translation>
    </message>
    <message>
        <source>Axis enabled</source>
        <translation>Achse ermoglicht</translation>
    </message>
    <message>
        <source>Axis disabled</source>
        <translation>Achse unermoglicht</translation>
    </message>
    <message>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>hour</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <source>Hide the auto-scale option</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeAxisSetup</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
</context>
<context>
    <name>CYScopeCapture</name>
    <message>
        <source>Cursors</source>
        <translation>Cursors</translation>
    </message>
</context>
<context>
    <name>CYScopeCaptureLegend</name>
    <message>
        <source>Cursors</source>
        <translation>Cursors</translation>
    </message>
</context>
<context>
    <name>CYScopePlotter</name>
    <message>
        <source>If you click on the scale a dialog box to change it will be created.</source>
        <translation type="vanished">Wenn Sie auf diese Skala klicken, erscheint einen Dialogbox fur jede Anderung.</translation>
    </message>
    <message>
        <source>For a good display the maximum number of curves is %1!</source>
        <translation>Fur eine gute Ansicht liegt die Maximale Anzahl der Kurven bei %1!</translation>
    </message>
    <message>
        <source>You must put data with the same acquisition time of the data(s) already displayed by this oscilloscope</source>
        <translation>Sie mussen Daten von selber Erfassungszeit eingeben, die schon bei diesem Oszilloskop angegeben sind</translation>
    </message>
    <message>
        <source>You must put data with the same buffer size of the data(s) already displayed by this oscilloscope</source>
        <translation>Sie mussen Daten von selber Buffergrosse eingeben, die schon bei diesem Oszilloskop angegeben sind</translation>
    </message>
    <message>
        <source>You must put data with the same burst buffer size of the data(s) already displayed by this oscilloscope</source>
        <translation>Sie mussen Daten vom selben Berstenbuffer eingeben, die schon bei diesem Oszilloskop angegeben sind</translation>
    </message>
    <message>
        <source>You must put data from the same connection of the data(s)already displayed by this oscilloscope</source>
        <translation>Sie mussen Daten vom selben Anschluss eingeben, die schon bei diesem Oszilloskop angegeben sind</translation>
    </message>
    <message>
        <source>You must put the same physical type of the data(s) already displayed by this oscilloscope</source>
        <translation>Sie mussen den selben Datentyp eingeben, den schon bei diesem Oszilloskop angegeben ist</translation>
    </message>
    <message>
        <source>No title</source>
        <translation>Kein Titel</translation>
    </message>
    <message>
        <source>Change label</source>
        <translation>Label andern</translation>
    </message>
    <message>
        <source>Change pen</source>
        <translation>Stift andern</translation>
    </message>
    <message>
        <source>Change scale coefficient</source>
        <translation>Integralkoefficient</translation>
    </message>
    <message>
        <source>If you double click on the scope a window to analyse curves will be created.</source>
        <translation>Wenn Sie auf dem Scope doppelt klicken, erscheint einen Box zur Kurvenanalyse.</translation>
    </message>
    <message>
        <source>Scope</source>
        <translation>Scope</translation>
    </message>
    <message>
        <source>Use mode</source>
        <translation>Verwendungsmodus</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <source>XY</source>
        <translation>XY</translation>
    </message>
    <message>
        <source>Sampling mode</source>
        <translation>Sampling Modus</translation>
    </message>
    <message>
        <source>Continuous</source>
        <translation>Continuous</translation>
    </message>
    <message>
        <source>In bursts</source>
        <translation>In bursts</translation>
    </message>
    <message>
        <source>Trigger</source>
        <translation>Trigonometrie</translation>
    </message>
    <message>
        <source>Level</source>
        <translation>Level</translation>
    </message>
    <message>
        <source>Rising</source>
        <translation>Display</translation>
    </message>
    <message>
        <source>Falling</source>
        <translation>Scaling</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <source>Curves style</source>
        <translation>Kurvenstyl</translation>
    </message>
    <message>
        <source>No Curve</source>
        <translation>Keine Kurve</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation>Linien</translation>
    </message>
    <message>
        <source>Sticks</source>
        <translation>Sticks</translation>
    </message>
    <message>
        <source>Steps</source>
        <translation>Steps</translation>
    </message>
    <message>
        <source>Dots</source>
        <translation>Dots</translation>
    </message>
    <message>
        <source>Curves width</source>
        <translation>Kurvenbreite</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Hintergrund Farbe</translation>
    </message>
    <message>
        <source>Major gridlines</source>
        <translation>Haupt Gitternetzlinien</translation>
    </message>
    <message>
        <source>Minor gridlines</source>
        <translation>Sekundare Gitternetzlinien</translation>
    </message>
    <message>
        <source>Major gridlines color</source>
        <translation>Haupt Gitternetzlinien Farbe</translation>
    </message>
    <message>
        <source>Minor gridlines color</source>
        <translation>Sekundare Gitternetzlinien Farbe</translation>
    </message>
    <message>
        <source>Legends</source>
        <translation type="vanished">Legende</translation>
    </message>
    <message>
        <source>Acquisition</source>
        <translation>Erfassung </translation>
    </message>
    <message>
        <source>Enable acquisition</source>
        <translation>Erlaubt Erfassung</translation>
    </message>
    <message>
        <source>Data label</source>
        <translation>Data label</translation>
    </message>
    <message>
        <source>Enter the new label: </source>
        <translation>Geben Sie den neuen Label ein:</translation>
    </message>
    <message>
        <source>Display coefficient</source>
        <translation>Integralkoefficient</translation>
    </message>
    <message>
        <source>Enter the new display coefficient: </source>
        <translation>Geben Sie den neuen Label ein:</translation>
    </message>
    <message>
        <source>&lt;p&gt;If you click on the scale, a dialog box will appear to change it.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable trigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Post-Trigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trigger over the level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trigger below the level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bottom legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Legend position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You must enter a coefficient display different of 0 !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No trigger signal!
Do you want to configure it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activation of the trigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The source signal of the trigger was not found!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopePrinter</name>
    <message>
        <source>%1/curves</source>
        <translation>%1/Kurven</translation>
    </message>
    <message>
        <source>%1/curve_%2.ps</source>
        <translation>%1/Kurve_%2.ps</translation>
    </message>
    <message>
        <source>%1/curve</source>
        <translation>%1/Kurve</translation>
    </message>
    <message>
        <source>%1/curve.pdf</source>
        <translation>%1/Kurve</translation>
    </message>
    <message>
        <source>Page %1</source>
        <translation>Seite %1</translation>
    </message>
    <message>
        <source>Saving under</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t print %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeScaleDialog</name>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeSetup</name>
    <message>
        <source>Oscilloscope Settings</source>
        <translation>Oszilloskop Sollwerts</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Stil</translation>
    </message>
    <message>
        <source>Grid</source>
        <translation>Gitter</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <source>Curves</source>
        <translation>Kurven</translation>
    </message>
    <message>
        <source>Background</source>
        <translation>Hintergrund</translation>
    </message>
    <message>
        <source>Data &amp;X</source>
        <translation>Data &amp;X</translation>
    </message>
    <message>
        <source>Datas &amp;Y</source>
        <translation>Daten &amp;Y</translation>
    </message>
    <message>
        <source>Axis</source>
        <translation>Axis</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeTrigger</name>
    <message>
        <source>Trigger setting</source>
        <translation>Timer Sollwerts</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeTriggerSetup</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
</context>
<context>
    <name>CYSettingsDialog</name>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation>Es gibt ungespeicherte Anderungen in der laufende Ansicht.Mochten Sie diese Änderungen anwenden oder ablenken ?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Ungespeicherte Anderungen</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYSplashScreen</name>
    <message>
        <source>Starting CYLIX...</source>
        <translation>CYLIX startet...</translation>
    </message>
    <message>
        <source>Stopping CYLIX...</source>
        <translation>CYLIX stoppt...</translation>
    </message>
</context>
<context>
    <name>CYString</name>
    <message>
        <source>Designer value: %1.</source>
        <translation>Hersteller Wert : %1</translation>
    </message>
    <message>
        <source>The maximum number of characters is %1.</source>
        <translation>Die Maximalanzahl der Zeichen ist %1.</translation>
    </message>
    <message>
        <source>If the string is too long it will be truncated!</source>
        <translation>Wenn die Folge zu lang ist, wird es abgeschnitten!</translation>
    </message>
</context>
<context>
    <name>CYTSec</name>
    <message>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Time</translation>
    </message>
</context>
<context>
    <name>CYTabWidget</name>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation>Es gibt ungespeicherte Anderungen in der laufende Ansicht.Mochten Sie diese Änderungen anwenden oder ablenken ?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Ungespeicherte Anderungen</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYTextDialog</name>
    <message>
        <source>Appl&amp;y</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYTextEdit</name>
    <message>
        <source>&amp;Scroll view</source>
        <translation>Roll Ansicht</translation>
    </message>
    <message>
        <source>Showing last line</source>
        <translation>letzte Linie anzeigen</translation>
    </message>
    <message>
        <source>Vertical Auto</source>
        <translation>Vertikale Auto</translation>
    </message>
    <message>
        <source>Vertical Off</source>
        <translation>Vertikale Aus</translation>
    </message>
    <message>
        <source>Vertical On</source>
        <translation>Vertikale Ein</translation>
    </message>
    <message>
        <source>Horizontal Auto</source>
        <translation>Horizontale Auto</translation>
    </message>
    <message>
        <source>Horizontal Off</source>
        <translation>Horizontale Aus</translation>
    </message>
    <message>
        <source>Horizontal On</source>
        <translation>Horizontale Ein</translation>
    </message>
    <message>
        <source>Maximum number of lines : %1 !</source>
        <translation>Maximale Anzahl der Erfassungsdateien</translation>
    </message>
</context>
<context>
    <name>CYTim</name>
    <message>
        <source>Maintenance</source>
        <translation>Wartung</translation>
    </message>
    <message>
        <source>Timer</source>
        <translation>Timer</translation>
    </message>
    <message>
        <source>Timer used for maintenance</source>
        <translation>Zur Wartung benutzter Timer</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>Teil</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Anmerkung</translation>
    </message>
    <message>
        <source>Total</source>
        <translation>Komplet</translation>
    </message>
</context>
<context>
    <name>CYTime</name>
    <message>
        <source>Time</source>
        <translation>Time</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>hour</translation>
    </message>
    <message>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <source>msec</source>
        <translation>msec</translation>
    </message>
    <message>
        <source>%1h%2m</source>
        <translation>%1h%2m</translation>
    </message>
    <message>
        <source>%2 min</source>
        <translation>%2 min</translation>
    </message>
    <message>
        <source>%1m%2s</source>
        <translation>%1m%2s</translation>
    </message>
    <message>
        <source>%1 sec</source>
        <translation>%1 sec</translation>
    </message>
    <message>
        <source>%1s%2ms</source>
        <translation>%1s%2ms</translation>
    </message>
    <message>
        <source>%3 msec</source>
        <translation>%3 msec</translation>
    </message>
    <message>
        <source>%1h%2m%3s</source>
        <translation>%1h%2m%3s</translation>
    </message>
    <message>
        <source>%1h%2m%3s%4</source>
        <translation>%1h%2m%3s%4</translation>
    </message>
    <message>
        <source>%1m%2s%3</source>
        <translation>%1m%2s%3</translation>
    </message>
    <message>
        <source>%1s%2</source>
        <translation>%1s%2</translation>
    </message>
    <message>
        <source>%1 msec</source>
        <translation>%1 msec</translation>
    </message>
    <message>
        <source>%1h</source>
        <translation>%1h</translation>
    </message>
    <message>
        <source>%1m</source>
        <translation>%1m</translation>
    </message>
    <message>
        <source>%1s</source>
        <translation>%1s</translation>
    </message>
    <message>
        <source> hour</source>
        <translation> hour</translation>
    </message>
    <message>
        <source> min</source>
        <translation> min</translation>
    </message>
    <message>
        <source> sec</source>
        <translation> sec</translation>
    </message>
    <message>
        <source> msec</source>
        <translation> msec</translation>
    </message>
    <message>
        <source>h%1m</source>
        <translation>h%1m</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <source>m%1s</source>
        <translation>m%1s</translation>
    </message>
    <message>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <source>s%1ms</source>
        <translation>s%1ms</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>h%1m%2s</source>
        <translation>h%1m%2s</translation>
    </message>
    <message>
        <source>h%1m%2s%3ms</source>
        <translation>h%1m%2s%3ms</translation>
    </message>
    <message>
        <source>m%1s%2ms</source>
        <translation>m%1s%2ms</translation>
    </message>
    <message>
        <source>Value from %1 to %2.</source>
        <translation>Wert von %1 bis %2.</translation>
    </message>
    <message>
        <source>Precision is %1 %2.</source>
        <translation>Genauigkeit ist %1 %2</translation>
    </message>
    <message>
        <source>Designer value: %1.</source>
        <translation>Hersteller Wert : %1</translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is higher than maximum value (%3&gt;%4) !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is smaller than minimum value (%3&lt;%4) !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>0ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>00ms</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUser</name>
    <message>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>LINUX user</source>
        <translation>LINUX Benutzer</translation>
    </message>
    <message>
        <source>The LINUX user takes the user name of the current LINUX session.</source>
        <translation>Der LINUX Benutzer nimmt den Name der aktuellen LINUX Session.</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Kennwort</translation>
    </message>
    <message>
        <source>Has a password</source>
        <translation>Braucht ein Kennwort</translation>
    </message>
    <message>
        <source>Password of &apos;%1&apos;</source>
        <translation>Kennwort auf &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <translation>Falsches Kennwort</translation>
    </message>
    <message>
        <source>Access &apos;%1&apos; has no password.</source>
        <translation>Zugriff &apos;%1&apos; notigt kein Kennwort.</translation>
    </message>
    <message>
        <source>Access password</source>
        <translation>Zugriff Kennwort</translation>
    </message>
    <message>
        <source>New password for &apos;%1&apos;</source>
        <translation>Neues Kennwort fur &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t find group with index %1</source>
        <translation>Kann die Gruppe mit Index  %1 nicht finden</translation>
    </message>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Die Datei %1 enthalt keine gultige XML
%2: Linie:%3 Kolonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">Kann die Datei nicht speichern %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Kann die Datei nicht speichern %1</translation>
    </message>
    <message>
        <source>Password checking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1
Password of &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Access &apos;%1&apos; cannot change its password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot get system user name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserAction</name>
    <message>
        <source>User action</source>
        <translation>Benutzer Aktion</translation>
    </message>
</context>
<context>
    <name>CYUserAdminEdit</name>
    <message>
        <source>Users administration</source>
        <translation>Benutzerverwaltung</translation>
    </message>
    <message>
        <source>Users</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>&amp;Add</source>
        <translation>&amp;Add</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>The LINUX user takes the user name of the current LINUX session.</source>
        <translation>Der LINUX Benutzer nimmt den Name der aktuellen LINUX Session.</translation>
    </message>
    <message>
        <source>Add &amp;Linux user</source>
        <translation>Add &amp;Linux Benutzer</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>Gruppen</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Administrator</source>
        <translation>Verwalter</translation>
    </message>
    <message>
        <source>Are you sure to remove the user %1 ?</source>
        <translation>Wollen sie wirklich die ausgewählte Zeile(n) löschen ?</translation>
    </message>
    <message>
        <source>Are you sure to remove the users group %1 ?</source>
        <translation>Wollen sie wirklich die ausgewählte Zeile(n) löschen ?</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserAdminEdit2</name>
    <message>
        <source>Users administration</source>
        <translation>Benutzerverwaltung</translation>
    </message>
    <message>
        <source>Add &amp;user</source>
        <translation>Add &amp;Linux Benutzer</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>The LINUX user takes the user name of the current LINUX session.</source>
        <translation>Der LINUX Benutzer nimmt den Name der aktuellen LINUX Session.</translation>
    </message>
    <message>
        <source>Add &amp;Linux user</source>
        <translation>Add &amp;Linux Benutzer</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>Add &amp;group</source>
        <translation>Kolonne beifugen</translation>
    </message>
    <message>
        <source>Alt+G</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Remo&amp;ve</source>
        <translation>Zeile löschen</translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <source>Michel</source>
        <translation>Maschine</translation>
    </message>
    <message>
        <source>Vincent</source>
        <translation>Skala</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation>Wartung</translation>
    </message>
    <message>
        <source>Julien</source>
        <translation>Multimeter</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Are you sure to remove the user %1 ?</source>
        <translation>Wollen sie wirklich die ausgewählte Zeile(n) löschen ?</translation>
    </message>
    <message>
        <source>Are you sure to remove the users group %1 ?</source>
        <translation>Wollen sie wirklich die ausgewählte Zeile(n) löschen ?</translation>
    </message>
    <message>
        <source>The WINDOWS user takes the user name of the current WINDOWS session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add &amp;Windows user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Robert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chefs d&apos;équipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Equipe de nuit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Xavier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>David</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Equipe de jour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thomas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Jean</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserEdit</name>
    <message>
        <source>Edit user</source>
        <translation>Ausdruck Benutzer</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>App&amp;ly</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserGroup</name>
    <message>
        <source>Users group</source>
        <translation>Benutzergruppe</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>CYLIX starting protected</source>
        <translation>CYLIX Starten ist geschuzt</translation>
    </message>
    <message>
        <source>CYLIX can start automatically with the current user at the time of the preceding stop if the group of this one allows it.</source>
        <translation>CYLIX kann automatisch starten mit dem laufenden Benutzer bei der Zeit des vorrigen Stops wenn die Gruppe dieses Benutzers es erlaubt.</translation>
    </message>
    <message>
        <source>CYLIX starts in protected access.</source>
        <translation>CYLIX startet in geschutzter Zugriff.</translation>
    </message>
    <message>
        <source>Screensaver protection</source>
        <translation>Screensaver Sicherung</translation>
    </message>
    <message>
        <source>Screensaver protection action</source>
        <translation>Screensaver Sicherungaktion</translation>
    </message>
    <message>
        <source>Force CYLIX in protected access</source>
        <translation>Zwingen sie CYLIX in geschutzter Zugriff.</translation>
    </message>
    <message>
        <source>Stop the LINUX session</source>
        <translation>Stoppt die LINUX Session</translation>
    </message>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Die Datei %1 enthalt keine gultige XML
%2: Linie:%3 Kolonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">Kann die Datei nicht speichern %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Kann die Datei nicht speichern %1</translation>
    </message>
    <message>
        <source>Protect access to desktop environment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restricts access to the desktop environment by disabling its panel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configuration is not possible if one of the parents has enabled this protection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This does not work and will be corrected soon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to find the collection of action %1 in user group %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to find action %1 in user group %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserGroupEdit</name>
    <message>
        <source>Edit user group</source>
        <translation>Ausdruck der Benutzergruppe</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Menus</source>
        <translation>Menus</translation>
    </message>
    <message>
        <source>Au&amp;thorized actions:</source>
        <translation>Er&amp;laubte Aktionen:</translation>
    </message>
    <message>
        <source>Proh&amp;ibited actions:</source>
        <translation>Uner&amp;laubte Aktionen:</translation>
    </message>
    <message>
        <source>Protection</source>
        <translation>Sicherung</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYWin</name>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Kann die Datei nicht offnen %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Die Datei %1 enthalt keine gultige XML
%2: Linie:%3 Kolonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Die Datei %1 enthalt keine gultige Definition, die einen Dokumenttyp haben muss</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Qt Application</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYWord</name>
    <message>
        <source>Word</source>
        <translation>Wort</translation>
    </message>
</context>
<context>
    <name>CalCHP</name>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>CalCylinder</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
</context>
<context>
    <name>CalServo</name>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>Conditions</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <source>SUPERVISOR</source>
        <translation type="unfinished">SUPERVISOR</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation type="unfinished">Supervisor</translation>
    </message>
    <message>
        <source>Regulator</source>
        <translation type="unfinished">Regler</translation>
    </message>
    <message>
        <source>Local datas</source>
        <translation type="unfinished">Lokale Dateien</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="unfinished">Druck</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="unfinished">Temperatur</translation>
    </message>
    <message>
        <source>Percentage</source>
        <translation type="unfinished">Prozentsatz</translation>
    </message>
    <message>
        <source>Trigonometry</source>
        <translation type="unfinished">Trigonometrie</translation>
    </message>
    <message>
        <source>Integer</source>
        <translation type="obsolete">Integer</translation>
    </message>
    <message>
        <source>Operator&apos;s manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cylix manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Release notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Machine</source>
        <translation type="unfinished">Maschine</translation>
    </message>
    <message>
        <source>Non-synchronous connection</source>
        <translation type="unfinished">Nicht synchron Anschluss</translation>
    </message>
    <message>
        <source>Synchronous connection</source>
        <translation type="unfinished">Synchronanschluss</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="unfinished">Typ</translation>
    </message>
    <message>
        <source>Leak rate</source>
        <translation type="unfinished">Leckverhaltnis</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="unfinished">Analogeingang</translation>
    </message>
    <message>
        <source>Stations inlet pressure</source>
        <translation type="unfinished">Station Eingangsdruck</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="unfinished">Zylinderposition</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation type="unfinished">Temperatur der Eingangsflussigkeit bei der Station</translation>
    </message>
    <message>
        <source>Stations outlet fluid temperature</source>
        <translation type="unfinished">Temperatur der Ausgangsflussigkeit bei der Station</translation>
    </message>
    <message>
        <source>Ramp 1 heater temperature</source>
        <translation type="unfinished">Warmeleistungtemperatur Rampe 1</translation>
    </message>
    <message>
        <source>Ramp 2 heater temperature</source>
        <translation type="unfinished">Warmeleistungtemperatur Rampe 2</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation type="unfinished">Station Ausgangsdruck</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="unfinished">Berstendruck</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="unfinished">Raumlufttemperatur</translation>
    </message>
    <message>
        <source>Water inlet temperature</source>
        <translation type="unfinished">Wassertemperatur beim Eingang</translation>
    </message>
    <message>
        <source>Water outlet temperature</source>
        <translation type="unfinished">Wassertemperatur beim Ausgang</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation type="unfinished">Analogausgang</translation>
    </message>
    <message>
        <source>Frequency converter VA1 (circulation pump)</source>
        <translation type="unfinished">Frequenzkonverter VA1 (Kreislaufpumpe)</translation>
    </message>
    <message>
        <source>Servo cylinder control</source>
        <translation type="unfinished">Kontrol des Servozylinders</translation>
    </message>
    <message>
        <source>Slow continuous acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fast continuous acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Slow acquisition on event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fast acquisition on event</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CylibsTest</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>Analys&amp;e</source>
        <translation>&amp;Analyse</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Page 1</source>
        <translation>Seite 1</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>New Item</source>
        <translation>Neues Stuck</translation>
    </message>
    <message>
        <source>New Item 2</source>
        <translation>Neues Stuck 2</translation>
    </message>
    <message>
        <source>a </source>
        <translation>a</translation>
    </message>
    <message>
        <source> v</source>
        <translation> v</translation>
    </message>
    <message>
        <source>textLabel1</source>
        <translation>textLabel1</translation>
    </message>
    <message>
        <source>Page 2</source>
        <translation>Seite 2</translation>
    </message>
    <message>
        <source>00</source>
        <translation>00</translation>
    </message>
    <message>
        <source>titre</source>
        <translation>Titel</translation>
    </message>
    <message>
        <source>pushButton&amp;4</source>
        <translation>pushButton&amp;4</translation>
    </message>
    <message>
        <source>pushBu&amp;tton6</source>
        <translation>pushButton&amp;6</translation>
    </message>
    <message>
        <source>pushButton&amp;5</source>
        <translation>pushButton&amp;5</translation>
    </message>
    <message>
        <source>pushButton&amp;3</source>
        <translation>pushButton&amp;3</translation>
    </message>
    <message>
        <source>1hh</source>
        <translation>1hh</translation>
    </message>
    <message>
        <source>&amp;0</source>
        <translation>&amp;0</translation>
    </message>
    <message>
        <source>AnalyseSheet</source>
        <translation>Analysebogen</translation>
    </message>
    <message>
        <source>Loa&amp;d</source>
        <translation>&amp;Aufladen</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Sa&amp;ve</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <source>Au&amp;toCali</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <source>Man&amp;uCali</source>
        <translation>Manuell</translation>
    </message>
    <message>
        <source>Scope</source>
        <translation>Scope</translation>
    </message>
    <message>
        <source>SUPERVISOR</source>
        <translation type="vanished">SUPERVISOR</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation type="vanished">Supervisor</translation>
    </message>
    <message>
        <source>Machine</source>
        <translation type="vanished">Maschine</translation>
    </message>
    <message>
        <source>Regulator</source>
        <translation type="vanished">Regler</translation>
    </message>
    <message>
        <source>Local datas</source>
        <translation type="vanished">Lokale Dateien</translation>
    </message>
    <message>
        <source>Non-synchronous connection</source>
        <translation type="vanished">Nicht synchron Anschluss</translation>
    </message>
    <message>
        <source>Synchronous connection</source>
        <translation type="vanished">Synchronanschluss</translation>
    </message>
    <message>
        <source>Simulation</source>
        <translation type="vanished">Simulation</translation>
    </message>
    <message>
        <source>Designer</source>
        <translation type="vanished">Hersteller</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Typ</translation>
    </message>
    <message>
        <source>Digital inputs</source>
        <translation>Digitaleingange</translation>
    </message>
    <message>
        <source>Analog inputs</source>
        <translation>Analogeingange</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="vanished">Temperatur</translation>
    </message>
    <message>
        <source>Trigonometry</source>
        <translation type="vanished">Trigonometrie</translation>
    </message>
    <message>
        <source>Leak rate</source>
        <translation type="vanished">Leckverhaltnis</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="vanished">Druck</translation>
    </message>
    <message>
        <source>Percentage</source>
        <translation type="vanished">Prozentsatz</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="vanished">Analogeingang</translation>
    </message>
    <message>
        <source>Stations inlet pressure</source>
        <translation type="vanished">Station Eingangsdruck</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="vanished">Zylinderposition</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation type="vanished">Temperatur der Eingangsflussigkeit bei der Station</translation>
    </message>
    <message>
        <source>Stations outlet fluid temperature</source>
        <translation type="vanished">Temperatur der Ausgangsflussigkeit bei der Station</translation>
    </message>
    <message>
        <source>Ramp 1 heater temperature</source>
        <translation type="vanished">Warmeleistungtemperatur Rampe 1</translation>
    </message>
    <message>
        <source>Ramp 2 heater temperature</source>
        <translation type="vanished">Warmeleistungtemperatur Rampe 2</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation type="vanished">Station Ausgangsdruck</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="vanished">Berstendruck</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="vanished">Raumlufttemperatur</translation>
    </message>
    <message>
        <source>Water inlet temperature</source>
        <translation type="vanished">Wassertemperatur beim Eingang</translation>
    </message>
    <message>
        <source>Water outlet temperature</source>
        <translation type="vanished">Wassertemperatur beim Ausgang</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation type="vanished">Analogausgang</translation>
    </message>
    <message>
        <source>Frequency converter VA1 (circulation pump)</source>
        <translation type="vanished">Frequenzkonverter VA1 (Kreislaufpumpe)</translation>
    </message>
    <message>
        <source>Servo cylinder control</source>
        <translation type="vanished">Kontrol des Servozylinders</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>YX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>CYEditText</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graphic analyse window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C&amp;ylix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Protected access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Change access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change &amp;password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Machine status backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Graphic analyse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;User administration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DBCal1</name>
    <message>
        <source>Calibration</source>
        <translation type="obsolete">Kalibrierung Kontrolle</translation>
    </message>
</context>
<context>
    <name>DBCyc</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
</context>
<context>
    <name>DBEcl1</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
</context>
<context>
    <name>DBFonct1</name>
    <message>
        <source>General</source>
        <translation type="obsolete">Allgemein</translation>
    </message>
</context>
<context>
    <name>DBForce1</name>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>Direct</source>
        <translation type="obsolete">Projekt</translation>
    </message>
</context>
<context>
    <name>DBGraf1</name>
    <message>
        <source>Acquisition</source>
        <translation type="obsolete">Erfassung </translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">Freigeben</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation type="obsolete">Kontrolle</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">Raumlufttemperatur</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>Process</source>
        <translation type="obsolete">Prozess</translation>
    </message>
    <message>
        <source>Test</source>
        <translation type="obsolete">Test</translation>
    </message>
</context>
<context>
    <name>DBInfoPC</name>
    <message>
        <source>Description</source>
        <translation type="obsolete">Beschreibung</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
</context>
<context>
    <name>DBPIDP1</name>
    <message>
        <source>Minimum value</source>
        <translation type="obsolete">Minimaler Wert</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation type="obsolete">Maximaler Wert</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation type="obsolete">Kontrolle</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">Berstendruck</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">Raumlufttemperatur</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation type="obsolete">Niedrieger Wert</translation>
    </message>
    <message>
        <source>High value</source>
        <translation type="obsolete">Hoher Wert</translation>
    </message>
</context>
<context>
    <name>DBPrg</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
</context>
<context>
    <name>DBPrj1</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
    <message>
        <source>Test</source>
        <translation type="obsolete">Test</translation>
    </message>
</context>
<context>
    <name>DBRN1</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
    <message>
        <source>Bench</source>
        <translation type="obsolete">Prufstand</translation>
    </message>
    <message>
        <source>Digital input</source>
        <translation type="obsolete">Digitaleingang</translation>
    </message>
    <message>
        <source>Digital output</source>
        <translation type="obsolete">Analogausgang</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="obsolete">Analogeingang</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation type="obsolete">Temperatur der Eingangsflussigkeit bei der Station</translation>
    </message>
    <message>
        <source>Stations inlet pressure</source>
        <translation type="obsolete">Station Eingangsdruck</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation type="obsolete">Station Ausgangsdruck</translation>
    </message>
    <message>
        <source>Ramp 1 heater temperature</source>
        <translation type="obsolete">Warmeleistungtemperatur Rampe 1</translation>
    </message>
    <message>
        <source>Ramp 2 heater temperature</source>
        <translation type="obsolete">Warmeleistungtemperatur Rampe 2</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation type="obsolete">Analogausgang</translation>
    </message>
    <message>
        <source>Frequency converter VA1 (circulation pump)</source>
        <translation type="obsolete">Frequenzkonverter VA1 (Kreislaufpumpe)</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">Berstendruck</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">Raumlufttemperatur</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">Wartung</translation>
    </message>
    <message>
        <source>Level</source>
        <translation type="obsolete">Level</translation>
    </message>
</context>
<context>
    <name>DBRNL1</name>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">Wartung</translation>
    </message>
    <message>
        <source>Flag</source>
        <translation type="obsolete">Flag</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">Status</translation>
    </message>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
</context>
<context>
    <name>DBRNR1</name>
    <message>
        <source>Stations inlet pressure</source>
        <translation type="obsolete">Station Eingangsdruck</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation type="obsolete">Station Ausgangsdruck</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation type="obsolete">Temperatur der Eingangsflussigkeit bei der Station</translation>
    </message>
    <message>
        <source>Stations outlet fluid temperature</source>
        <translation type="obsolete">Temperatur der Ausgangsflussigkeit bei der Station</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">Raumlufttemperatur</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">Berstendruck</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation type="obsolete">Analogausgang</translation>
    </message>
    <message>
        <source>Event</source>
        <translation type="obsolete">Ereignis</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation type="obsolete">Fehler</translation>
    </message>
</context>
<context>
    <name>DBSPV</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
</context>
<context>
    <name>DBSta1</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">Projekt</translation>
    </message>
</context>
<context>
    <name>DBTest</name>
    <message>
        <source>Test</source>
        <translation type="obsolete">Test</translation>
    </message>
</context>
<context>
    <name>DangerousTemperature</name>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
</context>
<context>
    <name>DoorsOpening</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>EventsGeneratorRN1</name>
    <message>
        <source>Bench</source>
        <translation type="obsolete">Prufstand</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Process</source>
        <translation type="obsolete">Prozess</translation>
    </message>
    <message>
        <source>Regulator</source>
        <translation type="obsolete">Regler</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation type="obsolete">Supervisor</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation type="obsolete">Operator</translation>
    </message>
    <message>
        <source>Command</source>
        <translation type="obsolete">Steuerung</translation>
    </message>
    <message>
        <source>End</source>
        <translation type="obsolete">Ende</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">Wartung</translation>
    </message>
</context>
<context>
    <name>EventsGeneratorSPV</name>
    <message>
        <source>Operator</source>
        <translation type="obsolete">Operator</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation type="obsolete">Supervisor</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">Warnung</translation>
    </message>
</context>
<context>
    <name>EventsPanel</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>IOForcing</name>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>IOFrame</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>IOView</name>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
</context>
<context>
    <name>ListCali</name>
    <message>
        <source>Group</source>
        <translation type="obsolete">Gruppe</translation>
    </message>
    <message>
        <source>Label</source>
        <translation type="obsolete">Label</translation>
    </message>
    <message>
        <source>Phys</source>
        <translation type="obsolete">Phys</translation>
    </message>
    <message>
        <source>Elec</source>
        <translation type="obsolete">Elec</translation>
    </message>
</context>
<context>
    <name>MainWin</name>
    <message>
        <source>&amp;Test</source>
        <translation type="obsolete">&amp;Test</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">Beschreibung</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">Raumlufttemperatur</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">Berstendruck</translation>
    </message>
    <message>
        <source>Initializing metrology window...</source>
        <translation type="obsolete">Initialisierung Supervisor-Ereignisse</translation>
    </message>
    <message>
        <source>Metrology window</source>
        <translation type="obsolete">Ereignisse Einstellungen</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation type="obsolete">Operator</translation>
    </message>
    <message>
        <source>Test: %1 (Access: %2)</source>
        <translation type="obsolete">Test: %1 (Access: %2)</translation>
    </message>
</context>
<context>
    <name>MaintenanceCounters</name>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
</context>
<context>
    <name>NetInfo</name>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
</context>
<context>
    <name>Network</name>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>PEXDoorsOpening</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>PFBPID</name>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">Berstendruck</translation>
    </message>
</context>
<context>
    <name>PFBSet</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>PFEPID</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>POSAccel</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>POSCtrl</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
</context>
<context>
    <name>POSCtrl_2SV</name>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
</context>
<context>
    <name>POSPID</name>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
</context>
<context>
    <name>POSPID_2SV</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>- </source>
        <translation type="obsolete">- </translation>
    </message>
    <message>
        <source>+ </source>
        <translation type="obsolete">+ </translation>
    </message>
</context>
<context>
    <name>POSSet</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>Page</name>
    <message>
        <source>Dynamic page example</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProcessCooling</name>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>ProcessDegassing</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
</context>
<context>
    <name>ProcessDrain</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
</context>
<context>
    <name>ProcessEmptying</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>ProcessLeak</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>ProcessSecurity</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
</context>
<context>
    <name>ProfileEdit</name>
    <message>
        <source>Pressure</source>
        <translation type="obsolete">Druck</translation>
    </message>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
    <message>
        <source>Fault</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="obsolete">Temperatur</translation>
    </message>
    <message>
        <source>Control</source>
        <translation type="obsolete">Kontrolle</translation>
    </message>
</context>
<context>
    <name>ProjectDescription</name>
    <message>
        <source>Description</source>
        <translation type="obsolete">Beschreibung</translation>
    </message>
</context>
<context>
    <name>ProjectView</name>
    <message>
        <source>Type</source>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <source>0</source>
        <translation type="obsolete">00</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
</context>
<context>
    <name>ProjectWizard</name>
    <message>
        <source>&amp;Create</source>
        <translation type="obsolete">&amp;Erzeugen</translation>
    </message>
</context>
<context>
    <name>PulsatingTestMonitoring</name>
    <message>
        <source>Process</source>
        <translation type="obsolete">Prozess</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>0</source>
        <translation type="obsolete">00</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <source>0,00</source>
        <translation type="obsolete">0,00</translation>
    </message>
</context>
<context>
    <name>PulsatingTestParts</name>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>Pv</name>
    <message>
        <source>Test directory</source>
        <translation type="obsolete">Testverzeichnis</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>File %1 at line %2</source>
        <translation>DAtei %1 bei Linie %2</translation>
    </message>
    <message>
        <source>File %1 at line %2: </source>
        <translation>Datei %1 bei Linie %2: </translation>
    </message>
    <message>
        <source>File %1 at line %2 !</source>
        <translation>Datei %1 bei Linie %2 !</translation>
    </message>
    <message>
        <source>CYLIX DEBUG</source>
        <translation>CYLIX DEBUG</translation>
    </message>
    <message>
        <source>File %1 at line %2 on object %3 !</source>
        <translation>Datei %1 bei Linie %2 auf Objekt %3 !</translation>
    </message>
    <message>
        <source>CYLIX OBJECT DEBUG</source>
        <translation>CYLIX OBJECT DEBUG</translation>
    </message>
    <message>
        <source>File %1 at line %2 for data %3 !</source>
        <translation>Datei %1 bei Linie %2 fur Data %3 !</translation>
    </message>
    <message>
        <source>CYLIX DATA DEBUG</source>
        <translation>CYLIX DATA DEBUG</translation>
    </message>
    <message>
        <source>File %1 at line %2 on object %3 for data %4 !</source>
        <translation>Datei %1 bei Linie %2 auf Objekt %3 fur Data %4 !</translation>
    </message>
    <message>
        <source>CYLIX OBJECT DATA DEBUG</source>
        <translation>CYLIX OBJECT DATA DEBUG</translation>
    </message>
    <message>
        <source>CYLIX ERROR</source>
        <translation>CYLIX ERROR</translation>
    </message>
    <message>
        <source>CYLIX OBJECT ERROR</source>
        <translation>CYLIX OBJECT ERROR</translation>
    </message>
    <message>
        <source>CYLIX DATA ERROR</source>
        <translation>CYLIX DATA ERROR</translation>
    </message>
    <message>
        <source>CYLIX OBJECT DATA ERROR</source>
        <translation>CYLIX OBJECT DATA ERROR</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 !</source>
        <translation>Fehler in Quelldatei %1 bei Linie %2 !</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 on object %3 !</source>
        <translation>Fehler in Quelldatei  %1 bei Linie %2 auf Objekt %3 !</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 for data %3 !</source>
        <translation>Fehler in Quelldatei %1 bei Linie %2 fur Data %3 !</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 on object %3 for data %4 !</source>
        <translation>Fehler in Quelldatei %1 bei Linie %2 auf Objekt %3 fur Data %4 !</translation>
    </message>
    <message>
        <source>Value from %1 to %2 %3.</source>
        <translation>Wert von %1 bis %2 %3.</translation>
    </message>
    <message>
        <source>Precision is %1 %2.</source>
        <translation>Genauigkeit ist %1 %2</translation>
    </message>
    <message>
        <source>Designer value:</source>
        <translation>Herstellerwert:</translation>
    </message>
    <message>
        <source>Designer value: %1 %2.</source>
        <translation>Hersteller Wert : %1 %2.</translation>
    </message>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation>Es gibt ungespeicherte Anderungen in der laufende Ansicht.Mochten Sie diese Änderungen anwenden oder ablenken ?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Ungespeicherte Anderungen</translation>
    </message>
    <message>
        <source>replace this with information about your translation team</source>
        <comment>&lt;p&gt;KDE is translated into many languages thanks to the work of the translation teams all over the world.&lt;/p&gt;&lt;p&gt;For more information on KDE internationalization visit &lt;a href=&quot;http://l10n.kde.org&quot;&gt;http://l10n.kde.org&lt;/a&gt;&lt;/p&gt;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No licensing terms for this program have been specified.
Please check the documentation or the source for any
licensing terms.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This program is distributed under the terms of the %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is higher than maximum value (%3&gt;%4) !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is smaller than minimum value (%3&lt;%4) !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choice:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 at line %2 : error in sheet %3 for one or multiple datas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 !
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QwtPlotRenderer</name>
    <message>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export File Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SealingTest</name>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>SealingTestView</name>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Type</source>
        <translation type="obsolete">Typ</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;Herstellerwert</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">Anwenden</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation type="obsolete">Es gibt ungespeicherte Anderungen in der laufende Ansicht.Mochten Sie diese Änderungen anwenden oder ablenken ?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation type="obsolete">Ungespeicherte Anderungen</translation>
    </message>
</context>
<context>
    <name>SpvCurvesDisplay</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>SpvCustomerData</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>SpvPV</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>StartBursting</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>StartCooling</name>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>StartDegassing</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>StartEmptying</name>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>StartPulsating</name>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
</context>
<context>
    <name>StartStatic</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>11</source>
        <translation type="obsolete">11</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
</context>
<context>
    <name>StaticEdit</name>
    <message>
        <source>Fault</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="obsolete">Temperatur</translation>
    </message>
    <message>
        <source>Control</source>
        <translation type="obsolete">Kontrolle</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="obsolete">Druck</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Time</translation>
    </message>
</context>
<context>
    <name>StaticTestMonitoring</name>
    <message>
        <source>Process</source>
        <translation type="obsolete">Prozess</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">Zylinderposition</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="obsolete">Druck</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Time</translation>
    </message>
    <message>
        <source>0,00</source>
        <translation type="obsolete">0,00</translation>
    </message>
</context>
<context>
    <name>StaticTestParts</name>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>11</source>
        <translation type="obsolete">11</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>Synoptic</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
</context>
<context>
    <name>SynopticAC</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
</context>
<context>
    <name>TAAPID</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>TAASet</name>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
</context>
<context>
    <name>TFECtrl</name>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
</context>
<context>
    <name>TFEPID</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">textLabel</translation>
    </message>
</context>
<context>
    <name>TFESet</name>
    <message>
        <source>± </source>
        <translation type="obsolete">± </translation>
    </message>
</context>
<context>
    <name>TargetWidget</name>
    <message>
        <source>Please specify the folder where Cylix will be installed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TestDescription</name>
    <message>
        <source>Operator</source>
        <translation type="obsolete">Operator</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation type="obsolete">Ok</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">Loschen</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="obsolete">Steuerung</translation>
    </message>
</context>
<context>
    <name>WaitStartBursting</name>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
</context>
</TS>
