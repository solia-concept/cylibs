<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>CYAI</name>
    <message>
        <source>Analog input</source>
        <translation>Entrée analogique</translation>
    </message>
</context>
<context>
    <name>CYAO</name>
    <message>
        <source>Analog output</source>
        <translation>Sortie analogique</translation>
    </message>
</context>
<context>
    <name>CYAbout</name>
    <message>
        <source>=</source>
        <translation type="vanished">=</translation>
    </message>
    <message>
        <source>(</source>
        <translation>(</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <source>)</source>
        <translation>)</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source> )</source>
        <translation> )</translation>
    </message>
    <message>
        <source>( </source>
        <translation>( </translation>
    </message>
    <message>
        <source>Up&amp;date</source>
        <translation>&amp;Mise à jour</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Select archive to send to the regulator.</source>
        <translation>Sélectionner l&apos;archive à envoyer au régulateur.</translation>
    </message>
    <message>
        <source>Real Time information</source>
        <translation>Informations temps réel</translation>
    </message>
</context>
<context>
    <name>CYAboutApplication</name>
    <message>
        <source>&amp;About</source>
        <translation>À &amp;propos</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <source>A&amp;uthors</source>
        <translation>A&amp;uteurs</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Droid Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Droid Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&amp;License Agreement</source>
        <translation>&amp;Accord de licence</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Please report bugs to: </source>
        <translation>Veuillez signaler les bugs à: </translation>
    </message>
</context>
<context>
    <name>CYAcquisition</name>
    <message>
        <source>Post-mortem mode</source>
        <translation type="vanished">Mode post-mortem</translation>
    </message>
    <message>
        <source>Acquisition started</source>
        <translation>Acquisition démarrée</translation>
    </message>
    <message>
        <source>Acquisition pausing</source>
        <translation>Acquisition en pause</translation>
    </message>
    <message>
        <source>Acquisition window in progress</source>
        <translation>Fenêtre d&apos;acquisition en cours</translation>
    </message>
    <message>
        <source>Acquisition saving</source>
        <translation>Enregistrement de l&apos;acquisition</translation>
    </message>
    <message>
        <source>Save acquisition file timer</source>
        <translation>Période de sauvegarde du fichier d&apos;acquisition</translation>
    </message>
    <message>
        <source>Post-mortem periodical backup timer</source>
        <translation>Temps d&apos;archivage périodique du post-mortem</translation>
    </message>
    <message>
        <source>Periodical backup</source>
        <translation>Archivage périodique</translation>
    </message>
    <message>
        <source>Acquisition files directory</source>
        <translation>Répertoire des fichiers d&apos;acquisition</translation>
    </message>
    <message>
        <source>Use test directory</source>
        <translation>Utilisation du répertoire d&apos;essai</translation>
    </message>
    <message>
        <source>Directory path</source>
        <translation>Chemin du répertoire</translation>
    </message>
    <message>
        <source>This path is used to save acquisition files somewhere else than in the test directory.</source>
        <translation>Ce chemin est utilisé pour l&apos;enregistrement des fichiers d&apos;acquisition ailleurs que dans le répertoire d&apos;essai.</translation>
    </message>
    <message>
        <source>acquisition</source>
        <translation type="vanished">acquisition</translation>
    </message>
    <message>
        <source>Acquisition files prefix</source>
        <translation>Préfixe des fichiers d&apos;acquisition</translation>
    </message>
    <message>
        <source>Acquisition files extension</source>
        <translation>Extension des fichiers d&apos;acquisition</translation>
    </message>
    <message>
        <source>Maximum of lines per acquisition file</source>
        <translation>Nombre de lignes par fichier d&apos;acquisition</translation>
    </message>
    <message>
        <source>Maximum number of acquisition files</source>
        <translation type="vanished">Nombre maximum de fichiers d&apos;acquisition</translation>
    </message>
    <message>
        <source>The acquisition saving will restart from the first file if this acquisition file number is reached.</source>
        <translation>L&apos;enregistrement de l&apos;acquisition recommencera à partir du premier fichier si ce nombre de fichiers d&apos;acquisition est atteint.</translation>
    </message>
    <message>
        <source>Choose sensibly this number if you don&apos;t want to overwrite the oldest acquisition files.</source>
        <translation>Choisir judicieusement ce chiffre si vous ne voulez pas écraser les fichiers d&apos;acquisition les plus anciens .</translation>
    </message>
    <message>
        <source>Number of acquisition files</source>
        <translation>Nombre de fichiers d&apos;acquisition</translation>
    </message>
    <message>
        <source>Acquisition time to pretend</source>
        <translation>Durée d&apos;acquisition à simuler</translation>
    </message>
    <message>
        <source>Maximum file size</source>
        <translation>Taille maximale d&apos;un fichier</translation>
    </message>
    <message>
        <source>Maximum size acquisition</source>
        <translation>Taille maximale de l&apos;acquisition</translation>
    </message>
    <message>
        <source>Start acquisition date time</source>
        <translation>Date et heure de début d&apos;acquisition</translation>
    </message>
    <message>
        <source>Acquisition file column separator</source>
        <translation>Séparateur de colonne du fichier d&apos;acquisition</translation>
    </message>
    <message>
        <source>Other acquisition file column separator</source>
        <translation>Autre séparateur de colonnes du fichier d&apos;acquisition</translation>
    </message>
    <message>
        <source>Acquisition backup</source>
        <translation>Archive de l&apos;acquisition</translation>
    </message>
    <message>
        <source>Backup file</source>
        <translation>Fichier d&apos;archivage</translation>
    </message>
    <message>
        <source>Acquisition since:</source>
        <translation>Acquisition depuis:</translation>
    </message>
    <message>
        <source>TIME (ms)%1</source>
        <translation type="vanished">TEMPS (ms)%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML !
%2: %3 %4</source>
        <translation>Le fichier %1 ne contient pas de code XML valide!
%2: %3 %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a valid definition, which must have a document type</source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation>Impossible d&apos;enregistrer le fichier %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Impossible d&apos;enregistrer le fichier %1</translation>
    </message>
    <message>
        <source>bytes</source>
        <translation>octets</translation>
    </message>
    <message>
        <source>Kb</source>
        <translation>Ko</translation>
    </message>
    <message>
        <source>Mb</source>
        <translation>Mo</translation>
    </message>
    <message>
        <source>Gb</source>
        <translation>Go</translation>
    </message>
    <message>
        <source>TIME</source>
        <translation>TEMPS</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">Format</translation>
    </message>
    <message>
        <source>Old Cylix acquisition</source>
        <translation type="vanished">Ancienne acquisition de Cylix</translation>
    </message>
    <message>
        <source>This choice maintains import compatibility with existing formatting files (e.g. .ods or .xls spreadsheets).</source>
        <translation type="vanished">Ce choix permet de maintenir la compatibilité des importations avec les fichiers de mise en forme existants (par exemple, les feuilles de calcul .ods ou .xls).</translation>
    </message>
    <message>
        <source>SoliaGraph</source>
        <translation type="vanished">SoliaGraph</translation>
    </message>
    <message>
        <source>Compatible with SoliaGraph Web software for graphical display of CSV acquisition files.</source>
        <translation type="vanished">Compatible avec le logiciel Web SoliaGraph de visualisation en courbes graphiques de fichiers d&apos;acquisition CSV.</translation>
    </message>
    <message>
        <source>TIME (ms)</source>
        <translation>TEMPS (ms)</translation>
    </message>
    <message>
        <source>Timestamp</source>
        <translation>Timestamp</translation>
    </message>
    <message>
        <source>us</source>
        <translation>us</translation>
    </message>
    <message>
        <source>Customized</source>
        <translation type="vanished">Personnalisé</translation>
    </message>
    <message>
        <source>Customizable acquisition format by adding header data and choosing a separator.</source>
        <translation type="obsolete">Format d&apos;acquisition personnalisable par l&apos;ajout de données d&apos;en-tête et le choix d&apos;un séparateur.</translation>
    </message>
    <message>
        <source>This Cylix original format preserves import compatibility with existing formatting files (e.g. .ods or .xls spreadsheets).</source>
        <translation type="vanished">Ce format original Cylix préserve la compatibilité d&apos;importation avec les fichiers de traitement existants (par exemple, les feuilles de calcul .ods ou .xls).</translation>
    </message>
    <message>
        <source>Compatible with SoliaGraph Web software for easy visualization of CSV acquisition files in the form of time curves.</source>
        <translation type="vanished">Compatible avec le logiciel SoliaGraph Web pour une visualisation aisée des fichiers d&apos;acquisition CSV sous forme de courbes temporelles.</translation>
    </message>
    <message>
        <source>Customizable acquisition format by choosing a separator.</source>
        <translation type="vanished">Format d&apos;acquisition personnalisable par le choix d&apos;un séparateur.</translation>
    </message>
    <message>
        <source>SoliaGraph compatibility</source>
        <translation>Compatibilité SoliaGraph</translation>
    </message>
    <message>
        <source>Continuous backup</source>
        <translation>Sauvegarde continue</translation>
    </message>
    <message>
        <source>Storage size is limited by the maximum number of acquisition files configurable in the  &apos;Files&apos; tab.</source>
        <translation>La taille de stockage est limitée en fonction du nombre maximum de fichiers d&apos;acquistion configurable dans l&apos;onglet &quot;Fichiers&quot;.</translation>
    </message>
    <message>
        <source>Post-mortem continuous acquisition archive error</source>
        <translation>Erreur d&apos;archivage de l&apos;acquisition continue post-mortem</translation>
    </message>
    <message>
        <source>Continuous acquisition archive file by post-mortem</source>
        <translation>Fichier d&apos;archives d&apos;acquisition continue par post-mortem</translation>
    </message>
    <message>
        <source>Preserves import compatibility with existing formatting files (e.g. .ods or .xls spreadsheets). It incorporates a time counter (in ms), initialized to 0 at the start of acquisition.</source>
        <translation type="vanished">Préserve la compatibilité d&apos;importation avec les fichiers de formatage existants (par exemple les feuilles de calcul .ods ou .xls). Il intègre un compteur de temps (en ms), initialisé à 0 au début de l&apos;acquisition.</translation>
    </message>
    <message>
        <source>Makes Cylix compatible with SoliaGraph Web software for easy visualization of CSV acquisition files in the form of time curves. For this, it incorporates a timestamp (in µs).</source>
        <translation type="vanished">Rend Cylix compatible avec le logiciel SoliaGraph Web pour une visualisation aisée des fichiers d&apos;acquisition CSV sous forme de courbes temporelles. Pour cela, il intègre un timestamp (en µs).</translation>
    </message>
    <message>
        <source>Generates a time-stamped acquisition file at the end of each acquisition time in sub-directories following the &apos;year/month/day/hour/minute&apos; tree structure.</source>
        <translation>Génère un fichier d&apos;acquisition horodaté à chaque fin de temps d&apos;acquisition dans des sous-répertoires suivant l&apos;arborescence &apos;année/mois/jour/heure/minute&apos;.</translation>
    </message>
    <message>
        <source>SoliaGraph allows multiple selection of files or sub-directories to easily perform a temporal analysis of the acquisition by scrolling through the resulting curves.</source>
        <translation>SoliaGraph permet la sélection multiple de fichiers ou de sous-répertoires afin d&apos;effectuer facilement une analyse temporelle de l&apos;acquisition en faisant défiler les courbes résultantes.</translation>
    </message>
    <message>
        <source>Please note that this mode of operation can consume a lot of storage space, and should therefore be used sparingly. You can simulate the space required in the &apos;Files&apos; tab.</source>
        <translation>Attention, ce fonctionnement pouvant être gourmand en stockage, il doit être utilisé à bon escient. Vous pouvez simulez l&apos;espace nécessaire dans l&apos;onglet &apos;Fichiers&apos;.</translation>
    </message>
    <message>
        <source>Generates an acquisition files based only on trigger events configured in the &apos;Events&apos; tab.</source>
        <translation>Génère des fichiers d&apos;acquisition uniquement en fonction des événements déclencheurs configurés dans l&apos;onglet &apos;Événements&apos;.</translation>
    </message>
    <message>
        <source>In addition to saving post-mortem recordings by configurable events, you can also save all post-mortem recordings to multiple files.</source>
        <translation>En plus de sauvegarder les enregistrements post-mortem par événements configurables, vous pouvez également sauvegarder tous les enregistrements post-mortem dans plusieurs fichiers.</translation>
    </message>
    <message>
        <source>Maximum number of acquisition files on events</source>
        <translation type="vanished">Nombre maximum de fichiers d&apos;acquisition sur événements</translation>
    </message>
    <message>
        <source>To simulate only the memory space required for continuous storage with simulation time, you can set this value to 0.</source>
        <translation>Pour simuler uniquement l&apos;espace mémoire requis pour le stockage continu avec le temps de simulation, vous pouvez fixer cette valeur à 0.</translation>
    </message>
    <message>
        <source>You can also enter 0 to disable backup on event.</source>
        <translation>Vous pouvez également saisir 0 pour interdire l&apos;enregistrement sur événement.</translation>
    </message>
    <message>
        <source>The integration of this tool into Cylix enables direct access and generation of SoliaGraph-compatible CSV files. If you are interested in this feature, but it is not available in your Cylix supervision, please contact us.</source>
        <translation type="vanished">L&apos;intégration de cet outil dans Cylix y permet un accès direct et une génération de fichiers CSV compatibles SoliaGraph. Si cette fonctionnalité vous intéresse, mais n&apos;est pas accessible dans votre supervision Cylix, nous vous invitons à nous contacter.</translation>
    </message>
    <message>
        <source>The integration of this SoliaGraph in Cylix provides direct access and this generation of compatible CSV files. If you are interested in this feature, but it is not available in your Cylix supervision, please contact us.</source>
        <translation type="obsolete">L&apos;intégration de SoliaGraph dans Cylix y permet un accès direct et cette génération de fichiers CSV compatibles. Si cette fonctionnalité vous intéresse, mais n&apos;est pas accessible dans votre supervision Cylix, nous vous invitons à nous contacter.</translation>
    </message>
    <message>
        <source>The integration of SoliaGraph in Cylix provides direct access and this generation of compatible CSV files. If you are interested in this feature, but it is not available in your Cylix supervision, please contact us.</source>
        <translation type="vanished">L&apos;intégration de SoliaGraph dans Cylix y permet un accès direct et cette génération de fichiers CSV compatibles. Si cette fonctionnalité vous intéresse, mais n&apos;est pas accessible dans votre supervision Cylix, nous vous invitons à nous contacter.</translation>
    </message>
    <message>
        <source>The integration of SoliaGraph in Cylix provides direct access and this generation of compatible CSV files. If you are interested in this feature, please contact us to available it.</source>
        <translation>L&apos;intégration de SoliaGraph dans Cylix permet un accès direct et la génération de fichiers CSV compatibles. Si vous êtes intéressé par cette fonctionnalité, veuillez nous contacter pour la rendre opérationnelle.</translation>
    </message>
    <message>
        <source>Preserves import compatibility with existing post-treatment files (e.g. .ods or .xls spreadsheets). It incorporates a time counter (in ms), initialized to 0 at the start of acquisition.</source>
        <translation type="vanished">Préserve la compatibilité d&apos;import avec les fichiers de post-traitement existants (par exemple les feuilles de calcul .ods ou .xls). Il intègre un compteur de temps (en ms), initialisé à 0 au début de l&apos;acquisition.</translation>
    </message>
    <message>
        <source>Makes Cylix CSV acquisition files compatibled with SoliaGraph Web software for easy visualization them in the form of time curves. For this, it imposes a defined format and incorporates a timestamp (in µs).</source>
        <translation type="vanished">Rend les fichiers d&apos;acquisition Cylix CSV compatibles avec le logiciel SoliaGraph Web pour les visualiser facilement sous forme de courbes temporelles. Pour cela, il impose un format défini et intègre un horodatage (en µs).</translation>
    </message>
    <message>
        <source>Preserves import compatibility with existing post-treatment files (e.g. .ods or .xls spreadsheets). Generated files incorporate a time counter (in ms), initialized to 0 at the start of acquisition.</source>
        <translation type="vanished">Préserve la compatibilité d&apos;import avec les fichiers de post-traitement existants (par exemple les feuilles de calcul .ods ou .xls). Les fichiers générés intègrent un compteur de temps (en ms), initialisé à 0 au début de l&apos;acquisition.</translation>
    </message>
    <message>
        <source>Makes Cylix CSV acquisition files compatibled with SoliaGraph Web software for easy visualization them in the form of time curves. For this, generate files has a defined format and a timestamp (in µs).</source>
        <translation>Rend les fichiers d&apos;acquisition Cylix CSV compatibles avec le logiciel SoliaGraph Web pour les visualiser facilement sous forme de courbes temporelles. Pour cela, les fichiers générés ont un format défini et un horodatage (en µs).</translation>
    </message>
    <message>
        <source>Preserves import compatibility with existing post-processing files (e.g. .ods or .xls spreadsheets) by using the old Cylix acquisition file format. These include a time counter (in ms), initialized to 0 at the start of acquisition.</source>
        <translation>Préserve la compatibilité d&apos;importation avec les fichiers de post-traitement existants (par exemple les feuilles de calcul .ods ou .xls) en utilisant l&apos;ancien format des fichiers d&apos;acquition de Cylix. Ceux-ci intègrent un compteur de temps (en ms), initialisé à 0 au début de l&apos;acquisition.</translation>
    </message>
    <message>
        <source>The maximum is set at 1,048,576 for use with acquisition files from Excel 2007 and LibreOffice Calc 4.2.</source>
        <translation>Le maximum est ici fixé 1 048 576 pour exploiter les fichiers d&apos;acquisition à partir de la version 2007 de Excel 2007 et la version 4.2 de LibreOffice Calc.</translation>
    </message>
    <message>
        <source>Acquisition</source>
        <translation>Acquisition</translation>
    </message>
    <message>
        <source>In continuous storage, this maximum is not used, as the number of files per hour is naturally low.</source>
        <translation>En stockage continu ce maximum n&apos;est pas utilisé car le nombre de fichiers par sous-répertoire d&apos;une heure est naturellement faible.</translation>
    </message>
    <message>
        <source>Maximum of acquisition files</source>
        <translation>Nombre de fichiers d&apos;acquisition</translation>
    </message>
    <message>
        <source>Maximum of acquisition files on events</source>
        <translation>Maximum de fichiers d&apos;acquisition sur événements</translation>
    </message>
    <message>
        <source>CSV file size optimization</source>
        <translation>Optimisation taille des fichiers CSV</translation>
    </message>
    <message>
        <source>Optimizes CSV file size by using empty fields for unchanged values.</source>
        <translation>Optimise la taille des fichiers CSV par l&apos;utilisation de champs vides en cas de valeurs inchangées.</translation>
    </message>
    <message>
        <source>Compatible with Solia Graph</source>
        <translation type="vanished">Compatible avec Solia Graph</translation>
    </message>
    <message>
        <source>With this optimization, storage space simulation can give a much higher value than reality.</source>
        <translation>Avec cette optimisation, la simulation de l&apos;espace de stockage peut donner une valeur bien plus grande que la réalité.</translation>
    </message>
    <message>
        <source>Compatible with Solia Graph.</source>
        <translation>Compatible avec Solia Graph.</translation>
    </message>
    <message>
        <source>Thanks to this function, you can, without greatly increasing the size of CSV files, add to the acquisition of data with highly variable values other, much less variable values, such as states.</source>
        <translation>Grâce à cette fonction, vous pouvez ainsi, sans augmenter fortement la taille des fichiers CSV, ajouter dans l&apos;acquisition de données à valeurs très changeantes d&apos;autres beaucoup moins, comme par exemples des états.</translation>
    </message>
</context>
<context>
    <name>CYAcquisitionBackupFlag</name>
    <message>
        <source>Prefix used in the backup file name</source>
        <translation>Préfixe utilisé dans le nom du fichier d&apos;archive</translation>
    </message>
    <message>
        <source>Header used in the backup file</source>
        <translation>En-tête utilisé dans le fichier d&apos;archive</translation>
    </message>
    <message>
        <source>Enable backup</source>
        <translation>Active le backup</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <source>No backup</source>
        <translation>Pas d&apos;enregistrement</translation>
    </message>
    <message>
        <source>Rising</source>
        <translation>Montant</translation>
    </message>
    <message>
        <source>Backup on the rising edge of event.</source>
        <translation>Sauvegarde sur front montant de l&apos;événement.</translation>
    </message>
    <message>
        <source>Falling</source>
        <translation>Descendant</translation>
    </message>
    <message>
        <source>Backup on the falling edge of event.</source>
        <translation>Sauvegarde sur front descendant de l&apos;événement.</translation>
    </message>
    <message>
        <source>Changing</source>
        <translation>Changement</translation>
    </message>
    <message>
        <source>Backup on the rising and falling edges of event.</source>
        <translation>Sauvegarde sur front montant et descendant de l&apos;événement.</translation>
    </message>
    <message>
        <source>Recording</source>
        <translation>Enregistrement</translation>
    </message>
    <message>
        <source>Overwrite</source>
        <translation>Écraser</translation>
    </message>
    <message>
        <source>Increment</source>
        <translation>Incrémenter</translation>
    </message>
    <message>
        <source>If the prefix doesn&apos;t change the new archive overwrites the last one.</source>
        <translation>Si le préfixe ne change pas la nouvelle archive écrase la précédente.</translation>
    </message>
    <message>
        <source>The number of backup is appended to the name of backup file. Thus, old backup files are note removed.</source>
        <translation>Un numéro d&apos;archivage est ajouté à la fin du nom du fichier d&apos;archive. Ainsi, les anciens fichiers d&apos;archive ne sont pas supprimés.</translation>
    </message>
    <message>
        <source>Backup delay on event</source>
        <translation>Retard de sauvegarde sur l&apos;événement</translation>
    </message>
</context>
<context>
    <name>CYAcquisitionDataView</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>#</source>
        <translation>N°</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>Crayon</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>État</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>Nom Designer</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>Coefficient</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Data acquisition label</source>
        <translation>Libellé de la donnée d&apos;acquisition</translation>
    </message>
    <message>
        <source>Enter the data acquisition label: </source>
        <translation>Entrer le libellé de la donnée d&apos;acquisition: </translation>
    </message>
</context>
<context>
    <name>CYAcquisitionFast</name>
    <message>
        <source>Acquisition period</source>
        <translation>Période d&apos;acquisition</translation>
    </message>
    <message>
        <source>Acquisition length time</source>
        <translation type="vanished">Durée d&apos;acquisition</translation>
    </message>
    <message>
        <source>Acquisition window time</source>
        <translation>Durée de la fenêtre d&apos;acquisition</translation>
    </message>
    <message>
        <source>No acquisition window time</source>
        <translation>Intervalle de temps entre 2 fenêtres d&apos;acquisition</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>Bursts acquisition period</source>
        <translation>Période d&apos;acquisition par rafales</translation>
    </message>
    <message>
        <source>Acquisition length time of a file</source>
        <translation>Durée d&apos;acquisition d&apos;un fichier</translation>
    </message>
    <message>
        <source>This time combined with the acquisition period automatically generates a number of lines per acquisition file. To reduce the time required to import multiple acquisition files into SoliaGraph, it is advisable to limit the number of small files by maximizing this time.</source>
        <translation>Ce temps combiné à la période d&apos;acquisition génère automatiquement un nombre de lignes par fichier d&apos;acquisition. Afin de réduire le temps nécessaire à l&apos;importation de plusieurs fichiers d&apos;acquisition dans SoliaGraph, il est conseillé de limiter le nombre de petits fichiers en maximisant ce temps.</translation>
    </message>
</context>
<context>
    <name>CYAcquisitionSetup</name>
    <message>
        <source>&amp;General</source>
        <translation>&amp;Général</translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Acquisition window</source>
        <translation type="vanished">Fenêtre d&apos;acquisition</translation>
    </message>
    <message>
        <source>Datas</source>
        <translation>Données</translation>
    </message>
    <message>
        <source>&amp;Header</source>
        <translation>&amp;En-tête</translation>
    </message>
    <message>
        <source>Fo&amp;rm</source>
        <translation type="vanished">Fo&amp;rme</translation>
    </message>
    <message>
        <source>&amp;Form</source>
        <translation type="vanished">Fo&amp;rmat</translation>
    </message>
    <message>
        <source>Separation options</source>
        <translation>Options de séparation</translation>
    </message>
    <message>
        <source>&amp;Tabulation</source>
        <translation>&amp;Tabulation</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>Sem&amp;i-colon</source>
        <translation>Point-v&amp;irgule</translation>
    </message>
    <message>
        <source>C&amp;omma</source>
        <translation>Vir&amp;gule</translation>
    </message>
    <message>
        <source>Sp&amp;ace</source>
        <translation>Esp&amp;ace</translation>
    </message>
    <message>
        <source>Ot&amp;her</source>
        <translation>A&amp;utre</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Fi&amp;les</source>
        <translation>Fich&amp;iers</translation>
    </message>
    <message>
        <source>Memory space</source>
        <translation type="vanished">Place mémoire</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="vanished">=</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation>Enregistrement</translation>
    </message>
    <message>
        <source>E&amp;vents</source>
        <translation>E&amp;vénements</translation>
    </message>
    <message>
        <source>Event</source>
        <translation>Evénement</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Actif</translation>
    </message>
    <message>
        <source>Prefix</source>
        <translation>Préfixe</translation>
    </message>
    <message>
        <source>Recording</source>
        <translation>Enregistrement</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <source>&amp;States</source>
        <translation>État&amp;s</translation>
    </message>
    <message>
        <source>Applying this change will restart this acquisition with a new time reference and a new file will be created !</source>
        <translation>Si vous appliquez cette modification, l&apos;acquisition redémarrera avec une nouvelle référence temporelle et un nouveau fichier sera créé !</translation>
    </message>
    <message>
        <source>Acquisition setup applying</source>
        <translation>Prise en compte de la configuration d&apos;acquisition</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation>&amp;Abandonner</translation>
    </message>
    <message>
        <source>The number of lines calculated %1 is higher than maximum allowed (%2)!
Please reduce the acquisition period or its length time.</source>
        <translation>Le nombre de lignes calculées %1 est supérieur au maximum autorisé (%2)!
Réduisez la période d&apos;acquisition ou sa durée.</translation>
    </message>
    <message>
        <source>Simulating stockage space</source>
        <translation type="vanished">Simulation de l&apos;espace de stockage</translation>
    </message>
    <message>
        <source>Storage space simulation</source>
        <translation>Simulation de l&apos;espace de stockage</translation>
    </message>
</context>
<context>
    <name>CYAcquisitionSlow</name>
    <message>
        <source>Acquisition period</source>
        <translation>Période d&apos;acquisition</translation>
    </message>
    <message>
        <source>Acquisition length time</source>
        <translation>Durée d&apos;acquisition</translation>
    </message>
    <message>
        <source>Acquisition window time</source>
        <translation>Durée de la fenêtre d&apos;acquisition</translation>
    </message>
    <message>
        <source>No acquisition window time</source>
        <translation>Intervalle de temps entre 2 fenêtres d&apos;acquisition</translation>
    </message>
</context>
<context>
    <name>CYAcquisitionWin</name>
    <message>
        <source>CYLIX: Acquisitions tool</source>
        <translation>CYLIX: Outil d&apos;acquisitions</translation>
    </message>
</context>
<context>
    <name>CYAction</name>
    <message>
        <source>Function usage flag</source>
        <translation>Flag d&apos;autorisation de l&apos;action</translation>
    </message>
    <message>
        <source>The current user is authorized to access this function.</source>
        <translation>L&apos;utilisateur actuel est autorisé à accéder à cette fonction.</translation>
    </message>
    <message>
        <source>The current user is not authorized to access this function or Cylix is in protected access.</source>
        <translation>L&apos;utilisateur actuel n&apos;est pas autorisé à accéder à cette fonction ou Cylix est en accès protégé.</translation>
    </message>
    <message>
        <source>Users administration</source>
        <translation>Administration utilisateurs</translation>
    </message>
    <message>
        <source>User administration</source>
        <translation type="vanished">Administration utilisateurs</translation>
    </message>
</context>
<context>
    <name>CYActionCollection</name>
    <message>
        <source>Cannot add action %1 in menu of window %2!</source>
        <translation>Impossible d&apos;ajouter l&apos;action %1 dans le menu de la fenêtre %2!</translation>
    </message>
    <message>
        <source>Cannot add action %1 in tool bar %2 of window %3!</source>
        <translation>Impossible d&apos;ajouter l&apos;action %1 dans la barre d&apos;outils %2 de la fenêtre%3!</translation>
    </message>
</context>
<context>
    <name>CYAdc</name>
    <message>
        <source>ADC</source>
        <translation type="vanished">ADC</translation>
    </message>
</context>
<context>
    <name>CYAdc16</name>
    <message>
        <source>ADC</source>
        <translation type="vanished">ADC</translation>
    </message>
    <message>
        <source>Calibration</source>
        <translation type="vanished">Calibrage</translation>
    </message>
</context>
<context>
    <name>CYAdc32</name>
    <message>
        <source>ADC</source>
        <translation type="vanished">ADC</translation>
    </message>
    <message>
        <source>Calibration</source>
        <translation type="vanished">Calibrage</translation>
    </message>
</context>
<context>
    <name>CYAna</name>
    <message>
        <source>Metrology</source>
        <translation>Métrologie</translation>
    </message>
    <message>
        <source>Number of points used for calibration</source>
        <translation>Nombre de point utiliser pour l&apos;étalonnage</translation>
    </message>
    <message>
        <source>Bench sensor in ADC</source>
        <translation>Capteur banc en ADC</translation>
    </message>
    <message>
        <source>New point</source>
        <translation>Nouveau point</translation>
    </message>
    <message>
        <source>Bench sensor</source>
        <translation>Capteur banc</translation>
    </message>
    <message>
        <source>Reference sensor</source>
        <translation>Capteur étalon</translation>
    </message>
    <message>
        <source>ADC bench values</source>
        <translation>Valeurs ADC du banc</translation>
    </message>
    <message>
        <source>Point %1</source>
        <translation>Point: %1</translation>
    </message>
    <message>
        <source>Bench values</source>
        <translation>Valeurs du banc</translation>
    </message>
    <message>
        <source>Reference values</source>
        <translation>Valeurs étalon</translation>
    </message>
    <message>
        <source>Averaging times</source>
        <translation>Temps de moyennage</translation>
    </message>
    <message>
        <source>Analogic</source>
        <translation>Analogique</translation>
    </message>
    <message>
        <source>High value (not calibrated)</source>
        <translation>Point haut (non calibré)</translation>
    </message>
    <message>
        <source>Maximum value of device usage range.</source>
        <translation>Valeur maximale de la plage d&apos;utilisation du matériel.</translation>
    </message>
    <message>
        <source>Low value (not calibrated)</source>
        <translation>Point bas (non calibré)</translation>
    </message>
    <message>
        <source>Minimum value of device usage range.</source>
        <translation>Valeur minimale de la plage d&apos;utilisation du matériel.</translation>
    </message>
    <message>
        <source>Maximum value ADC of device usage range.</source>
        <translation>Valeur maximale ADC de la plage d&apos;utilisation du matériel.</translation>
    </message>
    <message>
        <source>Minimum value ADC of device usage range.</source>
        <translation>Valeur minimale ADC de la plage d&apos;utilisation du matériel.</translation>
    </message>
    <message>
        <source>High value</source>
        <translation type="vanished">Point haut</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation type="vanished">Point bas</translation>
    </message>
    <message>
        <source>Gain offset of %1</source>
        <translation>Gain offset de %1</translation>
    </message>
    <message>
        <source>Sensor defect</source>
        <translation>Défaut capteur</translation>
    </message>
    <message>
        <source>Ana. Sensor</source>
        <translation>Capteur Ana</translation>
    </message>
    <message>
        <source>Defect of the sensor or the conditioner (Ex: signal lower than 4mA or higher than 20mA).  This may indicate a failure or a measure out of scale. Make sure of the quality of connections and the good performance of the acquisition board.</source>
        <translation>Défaut du capteur ou du conditionneur (Ex: signal inférieur à 4mA ou supérieur à 20mA). Peut indiquer une défaillance ou une sortie d&apos;échelle du capteur. S&apos;assurer de la qualité des connexions et du bon fonctionnement de la carte d&apos;acquisition.</translation>
    </message>
    <message>
        <source>ADC value</source>
        <translation>Valeur ADC</translation>
    </message>
    <message>
        <source>DAC value</source>
        <translation>Valeur DAC</translation>
    </message>
</context>
<context>
    <name>CYAnalyseCell</name>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Select a display type</source>
        <translation>Sélectionner un type d&apos;afficheur</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;Simple</translation>
    </message>
    <message>
        <source>&amp;Multimeter</source>
        <translation>&amp;Multimètre</translation>
    </message>
    <message>
        <source>&amp;Analog</source>
        <translation>&amp;Analogique</translation>
    </message>
    <message>
        <source>&amp;Digital</source>
        <translation>&amp;Numérique</translation>
    </message>
    <message>
        <source>&amp;Classical oscilloscope</source>
        <translation>&amp;Oscilloscope classique</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.)</source>
        <translation type="vanished">Oscilloscope pouvant avoir deux axes d&apos;ordonnées, chacun avec son propre format.</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.</source>
        <translation>L&apos;oscilloscope peut avoir deux axes d&apos;ordonnées, chacun avec son propre format.</translation>
    </message>
    <message>
        <source>Oscilloscope multiple formats</source>
        <translation>Oscilloscope multiples formats</translation>
    </message>
    <message>
        <source>Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets.</source>
        <translation>Oscilloscope pouvant avoir plusieurs formats de données différents sur un seul axe d&apos;ordonnées. Le libellé de chaque mesure est alors affiché avec son unité entre crochets . Le coefficient d&apos;affichage paramétrable, si celui-ci diffère de 1, s&apos;affiche également entre crochets .</translation>
    </message>
    <message>
        <source>&amp;BarGraph</source>
        <translation>&amp;Bargraphe</translation>
    </message>
    <message>
        <source>Datas &amp;table</source>
        <translation>&amp;Tableau de données</translation>
    </message>
    <message>
        <source>No display for this type of data</source>
        <translation>Aucun afficheur pour ce type de donnée</translation>
    </message>
    <message>
        <source>The parent of cell %1 has more than 2 children!</source>
        <translation>Le parent de la cellule %1 a plus de 2 enfants!</translation>
    </message>
    <message>
        <source>The clipboard does not contain a valid display description.</source>
        <translation>Le presse-papiers ne contient aucune description d&apos;affichage valable.</translation>
    </message>
    <message>
        <source>Analyse cell</source>
        <translation>Cellule d&apos;analyse</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <source>Add a cell on the right</source>
        <translation>Ajoute une cellule à droite</translation>
    </message>
    <message>
        <source>Add a cell below</source>
        <translation>Ajoute une cellule en dessous</translation>
    </message>
    <message>
        <source>Remove this cell</source>
        <translation>Supprime cette cellule</translation>
    </message>
</context>
<context>
    <name>CYAnalyseSheet</name>
    <message>
        <source>Analyse sheet</source>
        <translation>Feuille d&apos;analyse</translation>
    </message>
    <message>
        <source>Refresh time</source>
        <translation>Période de rafraîchissement</translation>
    </message>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Impossible d&apos;ouvrir le fichier %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Le fichier %1 ne contient pas de code XML valide
%2: ligne:%3 colonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>The file %1 has an invalid refresh time.</source>
        <translation>L&apos;intervalle de rafraîchissement du fichier %1 est incorrect.</translation>
    </message>
    <message>
        <source>The file %1 has an invalid work sheet size.</source>
        <translation>La taille de la feuille d&apos;analyse du fichier %1 est incorrecte.</translation>
    </message>
    <message>
        <source>Row or Column out of range (%1, %2)</source>
        <translation>Ligne ou Colonne en dehors des limites (%1, %2)</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation>Impossible d&apos;enregistrer le fichier %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Impossible d&apos;enregistrer le fichier %1.</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation>&amp;Propriétés</translation>
    </message>
</context>
<context>
    <name>CYAnalyseSheetSettings</name>
    <message>
        <source>Analyses Sheet Properties</source>
        <translation>Propriétés de la feuille d&apos;analyse</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>Enter the title of the work sheet here.</source>
        <translation>Entrer ici le titre de la feuille d&apos;analyse.</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <source>Update Interval</source>
        <translation>Intervalle de rafraîchissement</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation>Colonnes</translation>
    </message>
    <message>
        <source>Rows</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>CYAnalyseSpace</name>
    <message>
        <source>%1/Graphic_sheet</source>
        <translation>%1/Feuille_graphique</translation>
    </message>
    <message>
        <source>This is your analyse space. It holds your analyse sheets. You need to create a new analyse sheet (Menu File-&gt;New) before you can drag datas here.</source>
        <translation>Ceci est votre espace d&apos;analyse. Il contient vos feuilles d&apos;analyse. Vous devez créer une nouvelle feuille d&apos;analyse (menu Fichier-&gt;Nouveau) avant de pouvoir glisser-déposer des données ici.</translation>
    </message>
    <message>
        <source>Sheet %1</source>
        <translation>Feuille %1</translation>
    </message>
    <message>
        <source>The analyse sheet &apos;%1&apos; has been modified.
Do you want to save this analyse sheet?</source>
        <translation>La feuille d&apos;analyse « %1 » contient des informations non enregistrées. 
Voulez-vous l&apos;enregistrer ?</translation>
    </message>
    <message>
        <source>Select a work sheet to load</source>
        <translation>Sélectionnez une feuille d&apos;analyse à ouvrir</translation>
    </message>
    <message>
        <source>You don&apos;t have an analyse sheet that could be saved!</source>
        <translation>Aucune feuille d&apos;analyse ne peut être enregistrée !</translation>
    </message>
    <message>
        <source>Save current work sheet as</source>
        <translation>Enregistrer la feuille d&apos;analyse actuelle sous</translation>
    </message>
    <message>
        <source>This sheet is a model !</source>
        <translation type="vanished">Cette feuille est un modèle !</translation>
    </message>
    <message>
        <source>Would you save it in %1 ?</source>
        <translation type="vanished">Voulez-vous l&apos;enregistrer dans %1 ?</translation>
    </message>
    <message>
        <source>You have to enter a title for the analyse sheet !</source>
        <translation>Vous devez entrer un titre pour la feuille d&apos;analyse!</translation>
    </message>
    <message>
        <source>You don&apos;t have an analyse sheet that could be captured!</source>
        <translation>Aucune feuille d&apos;analyse ne peut être capturée!</translation>
    </message>
    <message>
        <source>There are no analyse sheets that could be deleted!</source>
        <translation>Aucune feuille d&apos;analyse ne peut être supprimée !</translation>
    </message>
    <message>
        <source>Graphic analyse</source>
        <translation>Analyse graphique</translation>
    </message>
</context>
<context>
    <name>CYAnalyseWin</name>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nouveau</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <source>Open &amp;Recent</source>
        <translation type="vanished">&amp;Récemment ouvert(s)</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation>Enregistrer &amp;sous...</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>Co&amp;uper</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>Co&amp;pier</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>C&amp;oller</translation>
    </message>
    <message>
        <source>&amp;Analyse sheet properties...</source>
        <translation>Propriétés de la feuille d&apos;&amp;analyse...</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>Confi&amp;guration</translation>
    </message>
    <message>
        <source>Configure &amp;Style...</source>
        <translation>Configurer le &amp;style...</translation>
    </message>
    <message>
        <source>Template...</source>
        <translation>Modèle...</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <source>&amp;Capture</source>
        <translation>&amp;Capture</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Rafraîchir</translation>
    </message>
</context>
<context>
    <name>CYApplication</name>
    <message>
        <source>modified</source>
        <translation>modifié</translation>
    </message>
</context>
<context>
    <name>CYAquisitionWidget</name>
    <message>
        <source>View</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation>Configuration</translation>
    </message>
</context>
<context>
    <name>CYAutoCali</name>
    <message>
        <source>&amp;High value</source>
        <translation>Point &amp;haut</translation>
    </message>
    <message>
        <source>&amp;Designer value</source>
        <translation>Valeur &amp;constructeur</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="vanished">Entrée analogique</translation>
    </message>
    <message>
        <source>&amp;Low value</source>
        <translation>Point &amp;bas</translation>
    </message>
    <message>
        <source>Currrent values</source>
        <translation>Valeurs courantes</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Haut</translation>
    </message>
    <message>
        <source>High Value</source>
        <translation>Point haut</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Bas</translation>
    </message>
    <message>
        <source>Low Value</source>
        <translation>Point bas</translation>
    </message>
    <message>
        <source>Do you really want to load the designer&apos;s values ?</source>
        <translation>Voulez-vous vraiment charger les valeurs constructeurs ?</translation>
    </message>
</context>
<context>
    <name>CYAutoCaliInput</name>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
</context>
<context>
    <name>CYBargraph</name>
    <message>
        <source>This display cannot treat this type of data !</source>
        <translation>Cet afficheur ne peut traiter ce type de donnée !</translation>
    </message>
    <message>
        <source>This data is already displayed in this bargraph.</source>
        <translation>Cette donnée est déjà affichée dans ce bargraph.</translation>
    </message>
    <message>
        <source>Bargraph</source>
        <translation>Graphe en barres</translation>
    </message>
</context>
<context>
    <name>CYBargraphPlotter</name>
    <message>
        <source>For a good display the maximum number of bars in the bargraph is %1!</source>
        <translation>Pour un bon affichage le nombre maximum de barres du graphe est %1!</translation>
    </message>
    <message>
        <source>Bargraph</source>
        <translation>Graphe en barres</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation>Échelle</translation>
    </message>
    <message>
        <source>Minimum value</source>
        <translation>Valeur minimum</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation>Valeur maximum</translation>
    </message>
    <message>
        <source>Sense</source>
        <translation>Sens</translation>
    </message>
    <message>
        <source>Increasing</source>
        <translation>Croissant</translation>
    </message>
    <message>
        <source>Increasing1</source>
        <translation type="vanished">Croissant</translation>
    </message>
    <message>
        <source>Decreasing</source>
        <translation>Décroissant</translation>
    </message>
    <message>
        <source>Alarm</source>
        <translation>Alerte</translation>
    </message>
    <message>
        <source>Upper bound value</source>
        <translation>Valeur limite haute</translation>
    </message>
    <message>
        <source>Upper alarm enable</source>
        <translation>Alarme haute active</translation>
    </message>
    <message>
        <source>Lower bound value</source>
        <translation>Valeur limite basse</translation>
    </message>
    <message>
        <source>Lower alarm enable</source>
        <translation>Alarme active basse</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>Normal Bar Color</source>
        <translation>Couleur normales des barres</translation>
    </message>
    <message>
        <source>Out-of-range Color</source>
        <translation>Couleur en cas de sortie de la plage</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation>Couleur de fond</translation>
    </message>
</context>
<context>
    <name>CYBargraphSetup</name>
    <message>
        <source>BarGraph Settings</source>
        <translation>Configuration du graphe en barres</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>Donnée(s)</translation>
    </message>
    <message>
        <source>Display Range</source>
        <translation>Plage d&apos;affichage</translation>
    </message>
</context>
<context>
    <name>CYBool</name>
    <message>
        <source>Designer value: %1.</source>
        <translation>Valeur constructeur: %1.</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>Binaire</translation>
    </message>
    <message>
        <source>true</source>
        <translation>vrai</translation>
    </message>
    <message>
        <source>false</source>
        <translation>faux</translation>
    </message>
</context>
<context>
    <name>CYCC</name>
    <message>
        <source>Maintenance</source>
        <translation>Maintenance</translation>
    </message>
    <message>
        <source>Cycles/switching counter</source>
        <translation>Compteur cycles/commutations</translation>
    </message>
    <message>
        <source>Counter used for maintenance</source>
        <translation>Compteur de maintenance</translation>
    </message>
    <message>
        <source>Total</source>
        <translation>Total</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>Partiel</translation>
    </message>
    <message>
        <source>Alert threshold</source>
        <translation>Seuil d&apos;alerte</translation>
    </message>
    <message>
        <source>An alert is generated if the partial counter reaches this value.</source>
        <translation>Une alerte est générée si le compteur partiel atteint cette valeur.</translation>
    </message>
    <message>
        <source>A null value disable the control.</source>
        <translation>Une valeur nulle désactive le contrôle.</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold. This threshold is settable in the maintenance counters window.</source>
        <translation>Indique que le compteur partiel a atteint le seuil d&apos;alerte. Ce seuil est réglable dans la fenêtre des compteurs de maintenance.</translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold.</source>
        <translation type="vanished">Indique que le compteur partiel a atteint le niveau d&apos;alerte.</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>RAZ</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Alerte</translation>
    </message>
</context>
<context>
    <name>CYCS</name>
    <message>
        <source>Maintenance</source>
        <translation>Maintenance</translation>
    </message>
    <message>
        <source>Time counter</source>
        <translation>Compteur horaire</translation>
    </message>
    <message>
        <source>Counter used for maintenance</source>
        <translation>Compteur de maintenance</translation>
    </message>
    <message>
        <source>Total</source>
        <translation>Total</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>Partiel</translation>
    </message>
    <message>
        <source>Alert threshold</source>
        <translation>Seuil d&apos;alerte</translation>
    </message>
    <message>
        <source>An alert is generated if the partial counter reaches this value.</source>
        <translation>Une alerte est générée si le compteur partiel atteint cette valeur.</translation>
    </message>
    <message>
        <source>A null value disable the control.</source>
        <translation>Une valeur nulle désactive le contrôle.</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold. This threshold is settable in the maintenance counters window.</source>
        <translation>Indique que le compteur partiel a atteint le seuil d&apos;alerte. Ce seuil est réglable dans la fenêtre des compteurs de maintenance.</translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold.</source>
        <translation type="vanished">Indique que le compteur partiel a atteint le niveau d&apos;alerte.</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>RAZ</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Alerte</translation>
    </message>
</context>
<context>
    <name>CYCT</name>
    <message>
        <source>Alert value</source>
        <translation>Valeur d&apos;alerte</translation>
    </message>
    <message>
        <source>Fault value</source>
        <translation>Valeur de défaut</translation>
    </message>
    <message>
        <source> &gt; MAX (Max. alert)</source>
        <translation> &gt; MAX (Alerte Max.)</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>Process</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert. Also The maximum time (or number of cycles) of alert has been reached and thus produced a fault.</source>
        <translation>La mesure était supérieure à la valeur maximale tolérée, ce qui a produit une alerte. Le temps maximum (ou le nombre de cycles) d&apos;alerte a été atteint produisant ainsi un défaut.</translation>
    </message>
    <message>
        <source> &lt; MIN (Max. alert)</source>
        <translation> &lt; MIN (Alerte Max.)</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert. Also The maximum time (or number of cycles) of alert has been reached and thus produced a fault.</source>
        <translation>La mesure était inférieure à la valeur minimale tolérée, ce qui a produit une alerte. Le temps maximum (ou le nombre de cycles) d&apos;alerte a été atteint produisant ainsi un défaut.</translation>
    </message>
    <message>
        <source> &gt; MAX: DATA1</source>
        <translation> &gt; MAX: DATA1</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum allowed value, which produced a fault.</source>
        <translation>La mesure était supérieure à la valeur maximale autorisée, ce qui a produit un défaut.</translation>
    </message>
    <message>
        <source> &lt; MIN: DATA1</source>
        <translation> &lt; MIN: DATA1</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum allowed value, which produced a fault.</source>
        <translation>La mesure était inférieure à la valeur minimale autorisée, ce qui a produit un défaut.</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert.</source>
        <translation>La mesure était supérieure à la valeur maximale tolérée, ce qui a produit une alerte.</translation>
    </message>
    <message>
        <source>The value at which the alert occured is written in the message.</source>
        <translation>La valeur à laquelle l&apos;alerte est apparue est écrite dans le message.</translation>
    </message>
    <message>
        <source>The maximum measured value is written in the message of end of alert.</source>
        <translation>La valeur maximale mesurée est écrite dans le message de fin d&apos;alerte.</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert.</source>
        <translation>La mesure était inférieure à la valeur minimale tolérée, ce qui a produit une alerte.</translation>
    </message>
    <message>
        <source>The minimum measured value is written in the message of end of alert.</source>
        <translation>La valeur minimale mesurée est écrite dans le message de fin d&apos;alerte.</translation>
    </message>
</context>
<context>
    <name>CYCTCyc</name>
    <message>
        <source>Number of inhibition cycles before control (T0)</source>
        <translation>Nombre de cycles d&apos;inhibition avant le contrôle (T0)</translation>
    </message>
    <message>
        <source>The minimum number of cycles before control is T0</source>
        <translation>Le nombre de cycles minimum avant le contrôle est T0</translation>
    </message>
    <message>
        <source>The maximum number of cycles before control is T0+T1</source>
        <translation>Le nombre de cycles maximum avant le contrôle est T0+T1</translation>
    </message>
    <message>
        <source>Supplementary cycles before control (T1)</source>
        <translation>Nombre de cycles supplémentaires avant contrôle (T1)</translation>
    </message>
    <message>
        <source>The control start when the measure in inside the tolerances, or when this optional inhibition is overreached</source>
        <translation>Le contrôle est effectif dès que la mesure est dans les tolérances ou quand cette inhibition optionnelle est dépassée</translation>
    </message>
    <message>
        <source>Stop test if maximum alert ?</source>
        <translation>Arrêt de l&apos;essai si alerte maximum ?</translation>
    </message>
    <message>
        <source>The bench will be stopped if the max number of cycles alert is reached.</source>
        <translation>Le banc sera arrêté si le nombre maximum de cycles d&apos;alertes est atteint.</translation>
    </message>
    <message>
        <source>Maximum number of cycles out of alert tolerances</source>
        <translation>Nombre maximum de cycles hors tolérances d&apos;alerte</translation>
    </message>
    <message>
        <source>Maximum outside tolerances number of cycles for control.</source>
        <translation>Nombre maximum de cycles hors tolérances.</translation>
    </message>
    <message>
        <source>If reached =&gt; fault!</source>
        <translation>Si dépassé =&gt; Génération d&apos;un défaut!</translation>
    </message>
    <message>
        <source>Stop test if out of fault tolerances ?</source>
        <translation>Arrêt de l&apos;essai si hors tolérance de défaut ?</translation>
    </message>
    <message>
        <source>The bench will be stopped if fault tolerance is reached.</source>
        <translation>Le banc sera arrêté si les tolérances de défaut sont atteintes.</translation>
    </message>
    <message>
        <source> &gt; MAX (Max. alert)</source>
        <translation> &gt; MAX (Alerte Max.)</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>Process</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert. Also the maximum number of cycles of alert has been reached and thus produced a fault.</source>
        <translation>La mesure était supérieure à la valeur maximale tolérée, ce qui a produit une alerte. Le nombre de cycles maximum d&apos;alerte a été atteint produisant ainsi un défaut.</translation>
    </message>
    <message>
        <source> &lt; MIN (Max. alert)</source>
        <translation> &lt; MIN (Alerte Max.)</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert. Also the maximum number of cycles of alert has been reached and thus produced a fault.</source>
        <translation>La mesure était inférieure à la valeur minimale tolérée, ce qui a produit une alerte. Le nombre de cycles maximum d&apos;alerte a été atteint produisant ainsi un défaut.</translation>
    </message>
</context>
<context>
    <name>CYCTInput</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation>Attention: ces paramètres sont liés au projet actuel</translation>
    </message>
</context>
<context>
    <name>CYCTTime</name>
    <message>
        <source>Minimum measure control delay (T0)</source>
        <translation>Délai minimum avant contrôle de la mesure (T0)</translation>
    </message>
    <message>
        <source>The minimum control delay is T0</source>
        <translation>Le temps minimum avant le contrôle est T0</translation>
    </message>
    <message>
        <source>The maximum control delay is T0+T1</source>
        <translation>Le temps maximum avant le contrôle est T0+T1</translation>
    </message>
    <message>
        <source>Supplementary measure control delay (T1)</source>
        <translation>Temps supplémentaire avant contrôle (T1)</translation>
    </message>
    <message>
        <source>The control start when the measure in inside the tolerances, or when this optional inhibition is overreached</source>
        <translation>Le contrôle est effectif dès que la mesure est dans les tolérances ou quand cette inhibition optionnelle est dépassée</translation>
    </message>
    <message>
        <source>Stop test if maximum alert ?</source>
        <translation>Arrêt de l&apos;essai si alerte maximum ?</translation>
    </message>
    <message>
        <source>The bench will be stopped if the max time alert is reached.</source>
        <translation>Le banc sera arrêté si le temps maximum d&apos;alertes est atteint.</translation>
    </message>
    <message>
        <source>Maximum time out of alert tolerances</source>
        <translation>Temps maximum hors tolérances d&apos;alerte</translation>
    </message>
    <message>
        <source>Maximum outside alert tolerances time before process fault.</source>
        <translation>Temps maximum hors tolérances avant défaut process.</translation>
    </message>
    <message>
        <source>Stop test if out of fault tolerances ?</source>
        <translation>Arrêt de l&apos;essai si hors tolérance de défaut ?</translation>
    </message>
    <message>
        <source>The bench will be stopped if fault tolerance is reached.</source>
        <translation>Le banc sera arrêté si les tolérances de défaut sont atteintes.</translation>
    </message>
    <message>
        <source> &gt; MAX (Max. alert)</source>
        <translation> &gt; MAX (Alerte Max.)</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>Process</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert. Also the maximum time of alert has been reached and thus produced a fault.</source>
        <translation>La mesure était supérieure à la valeur maximale tolérée, ce qui a produit une alerte. Le temps maximum d&apos;alerte a été atteint produisant ainsi un défaut.</translation>
    </message>
    <message>
        <source> &lt; MIN (Max. alert)</source>
        <translation> &lt; MIN (Alerte Max.)</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert. Also the maximum time of alert has been reached and thus produced a fault.</source>
        <translation>La mesure était inférieure à la valeur minimale tolérée, ce qui a produit une alerte. Le temps maximum d&apos;alerte a été atteint produisant ainsi un défaut.</translation>
    </message>
</context>
<context>
    <name>CYChangeAccess</name>
    <message>
        <source>Change Accesss</source>
        <translation>Changement d&apos;accès</translation>
    </message>
    <message>
        <source>Choose access: </source>
        <translation>Choix de l&apos;accès: </translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
</context>
<context>
    <name>CYCnt</name>
    <message>
        <source>Total counter</source>
        <translation>Compteur total</translation>
    </message>
    <message>
        <source>Counter used for maintenance</source>
        <translation>Compteur de maintenance</translation>
    </message>
    <message>
        <source>Partial counter</source>
        <translation>Compteur partiel</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
</context>
<context>
    <name>CYComboBoxDialog</name>
    <message>
        <source>Dialog comboBox data</source>
        <translation>Boîte de saisie liste</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYCommand</name>
    <message>
        <source>Command</source>
        <translation>Commande</translation>
    </message>
</context>
<context>
    <name>CYCore</name>
    <message>
        <source>Bench</source>
        <translation>Banc</translation>
    </message>
    <message>
        <source>Besnch</source>
        <translation type="vanished">Besnch</translation>
    </message>
    <message>
        <source>Undef</source>
        <translation>Pas défini</translation>
    </message>
    <message>
        <source>Electrical cabinet temperature2</source>
        <translation type="vanished">Température de l&apos;armoire électrique3</translation>
    </message>
    <message>
        <source>Simulation</source>
        <translation>Simulation</translation>
    </message>
    <message>
        <source>Designer</source>
        <translation>Constructeur</translation>
    </message>
    <message>
        <source>Protected</source>
        <translation>Protégé</translation>
    </message>
    <message>
        <source>Administrator</source>
        <translation>Administrateur</translation>
    </message>
    <message>
        <source>Cannot start the &apos;%1&apos; with the %2 on COM%3 !</source>
        <translation>Impossible de démarrer la &apos;%1&apos; avec le %2 sur la COM%3 !</translation>
    </message>
    <message>
        <source>Can&apos;t remove the user %1 because it is the current user!</source>
        <translation>Impossible de supprimer l&apos;utilisateur %1 parce qu&apos;il est l&apos;utilisateur courant!</translation>
    </message>
    <message>
        <source>Removing user</source>
        <translation>Suppression d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Can&apos;t remove the group %1 because it is used by the user %2!</source>
        <translation>Impossible de supprimer le groupe %1 parce qu&apos;il est utilisé par l&apos;utilisateur %2!</translation>
    </message>
    <message>
        <source>Removing users group</source>
        <translation>Suppression de groupe utilisateurs</translation>
    </message>
    <message>
        <source>GID</source>
        <translation>GID</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Début</translation>
    </message>
    <message>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <source>This access has the administrator rights and authorizes all the menus!</source>
        <translation>Cet accès a les droits administrateur et autorise tous les menus !</translation>
    </message>
    <message>
        <source>Do you want to create a new access more protected?</source>
        <translation>Voulez-vous créer un nouvel accès plus sécurisé ?</translation>
    </message>
    <message>
        <source>Administrator rights!</source>
        <translation>Droits administrateur !</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Oui</translation>
    </message>
    <message>
        <source>&amp;Continue</source>
        <translation>&amp;Continuer</translation>
    </message>
    <message>
        <source>Initializing metrology window...</source>
        <translation>Initialisation la fenêtre de métrologie...</translation>
    </message>
    <message>
        <source>Times of tests</source>
        <translation>Temps de tests</translation>
    </message>
    <message>
        <source>Times of access</source>
        <translation>Temps d&apos;accès</translation>
    </message>
    <message>
        <source>Evolution of the project settings</source>
        <translation>Evolution des paramètres projet</translation>
    </message>
    <message>
        <source>Cannot create the directory %1</source>
        <translation>Impossible de créer le répertoire %1</translation>
    </message>
    <message>
        <source>Cannot find backup directoy</source>
        <translation>Impossible de trouver répertoire de backup</translation>
    </message>
    <message>
        <source>Cannot find the directory to backup</source>
        <translation>Impossible de trouver le répertoire à mettre en backup</translation>
    </message>
    <message>
        <source>Cannot find the file to backup</source>
        <translation>Impossible de trouver le fichier à mettre en backup</translation>
    </message>
    <message>
        <source>See the users managment section of the computer installation procedure.</source>
        <translation>Voir la partie gestion des utilisateurs de la procédure d&apos;installation de l&apos;ordinateur.</translation>
    </message>
    <message>
        <source>The CYLIX base directrory %1 doesn&apos;t exist!</source>
        <translation>Le répertoire de base de CYLIX %1 n&apos;existe pas!</translation>
    </message>
    <message>
        <source>%1 is not a directory!</source>
        <translation>%1 n&apos;est pas un répertoire!</translation>
    </message>
    <message>
        <source>%1 is not a readable!</source>
        <translation>%1 n&apos;a pas d&apos;accès en lecture!</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Do you want to include the sub-groups ?</source>
        <translation>Voulez-vous inclure les sous-groupes ?</translation>
    </message>
    <message>
        <source>Loading groug</source>
        <translation>Chargement du groupe</translation>
    </message>
    <message>
        <source>Add alphabetically the datas of this group in a new column ?</source>
        <translation>Ajouter par ordre alphabétique les données de ce groupe dans une nouvelle colonne ?</translation>
    </message>
    <message>
        <source>If you answer &quot;No&quot;, the datas will be added in news rows !</source>
        <translation>Si vous répondez &quot;Non&quot;, les données seront ajoutées dans de nouvelles lignes !</translation>
    </message>
    <message>
        <source>class: %1 not supported (%2)</source>
        <translation>classe: %1 non supportée (%2)</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Alerte</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>%1:%2 : designer value checking</source>
        <translation>%1:%2 : vérification valeur constructeur</translation>
    </message>
    <message>
        <source>Choose a filename to save under for this export file.</source>
        <translation>Choisir un nom de fichier à enregistrer sous pour ce fichier d&apos;export.</translation>
    </message>
    <message>
        <source>HOST(S)	LINK(S)	GROUP	NAME	LABEL	VALUE	DEF	MIN	MAX	HELP
</source>
        <translation>HÔTE(S)	CONNEXION(S)	GROUPE	NOM	LIBELLÉ	VALEUR	DEF	MIN	MAX	AIDE
</translation>
    </message>
    <message>
        <source>Cannot find the manual for the current language !</source>
        <translation>Impossible de trouver le manuel de la langue en cours!</translation>
    </message>
    <message>
        <source>Metrology window</source>
        <translation>Fenêtre de métrologie</translation>
    </message>
    <message>
        <source>Could not find the access action to open the document %1.</source>
        <translation>Impossible de trouver l&apos;action d&apos;accès pour ouvrir le document %1.</translation>
    </message>
    <message>
        <source>Could not find document %1.</source>
        <translation>Impossible de trouver le document %1.</translation>
    </message>
    <message>
        <source>Cannot remove the application&apos;s temporary directory.</source>
        <translation>Impossible de supprimer le répertoire temporaire de l&apos;application.</translation>
    </message>
    <message>
        <source>You do not have access rights to the requested documentation! Check your access rights with your Cylix administrator. These can be configured in the Cylix user administration tool.</source>
        <translation type="obsolete">Vous n&apos;avez pas les droits d&apos;accès à la documentation demandée! Vérifiez vos droits au près de votre administrateur de Cylix. Ceux-ci son configurables dans l&apos;outil d&apos;administration utilisateurs de Cylix. </translation>
    </message>
    <message>
        <source>You do not have access rights to the requested documentation ! Check your access rights with your Cylix administrator. These can be configured in the Cylix user administration tool.</source>
        <translation>Vous n&apos;avez pas les droits d&apos;accès à la documentation demandée ! Vérifiez vos droits d&apos;accès au près de votre administrateur de Cylix. Ceux-ci sont configurables dans l&apos;outil d&apos;administration utilisateurs de Cylix.</translation>
    </message>
    <message>
        <source>Unable to copy directory %1 to %2!</source>
        <translation type="vanished">Impossible de copier le répertoire %1 vers %2 !</translation>
    </message>
    <message>
        <source>Error %1!</source>
        <translation>Erreur %1 !</translation>
    </message>
    <message>
        <source>Initialization SoliaGraph</source>
        <translation>Initialisation SoliaGraph</translation>
    </message>
    <message>
        <source>SoliaGraph is not activated on this Cylix. If you are interested in this tool, please contact SOLIA Concept.</source>
        <translation>SoliaGraph n&apos;est pas activé sur ce Cylix. Si vous êtes intéressé par cet outil, veuillez contacter SOLIA Concept.</translation>
    </message>
    <message>
        <source>Starting SoliaGraph</source>
        <translation>Démarrage de SoliaGraph</translation>
    </message>
    <message>
        <source>Doesn&apos;t exist &apos;%1&apos; !</source>
        <translation>&apos;%1&apos; innexistant !</translation>
    </message>
    <message>
        <source>Unable to load file %1!</source>
        <translation type="vanished">Impossible de charger le fichier %1 !</translation>
    </message>
    <message>
        <source>Unable to start Chrome!</source>
        <translation type="vanished">Impossible de démarrer Chrome !</translation>
    </message>
    <message>
        <source>Unable to start Chrome !</source>
        <translation type="vanished">Impossible de démarrer Chrome !</translation>
    </message>
    <message>
        <source>Please install it to use SoliaGraph.</source>
        <translation type="vanished">Veuillez l&apos;installer pour utiliser SoliaGraph.</translation>
    </message>
    <message>
        <source>Initializing SoliaGraph...</source>
        <translation>Initialisation de SoliaGraph...</translation>
    </message>
    <message>
        <source>Unable to unzip file SoliaGraph!</source>
        <translation>Impossible de décompresser le fichier SoliaGraph !</translation>
    </message>
    <message>
        <source>Unable to start SoliaGraph !</source>
        <translation>Impossible de démarrer SoliaGraph !</translation>
    </message>
    <message>
        <source>SoliaGraph</source>
        <translation>SoliaGraph</translation>
    </message>
</context>
<context>
    <name>CYDB</name>
    <message>
        <source>Starting refresh timer datas base %1 at %2 ms</source>
        <translation>Démarrage du rafraîchissement de la base de données %1 à %2 ms</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation>Impossible d&apos;enregistrer le fichier %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Impossible d&apos;enregistrer le fichier %1.</translation>
    </message>
    <message>
        <source>The clipboard does not contain a valid datas base description.</source>
        <translation>Le presse-papiers ne contient aucune description de base de données valable.</translation>
    </message>
    <message>
        <source>Cannot remove %1 from %2</source>
        <translation>Impossible de supprimer %1 de %2</translation>
    </message>
</context>
<context>
    <name>CYDBLibs</name>
    <message>
        <source>Project</source>
        <translation>Projet</translation>
    </message>
    <message>
        <source>Setpoints protection</source>
        <translation>Protection des consignes</translation>
    </message>
    <message>
        <source>Avoid to erase project setpoints. If a project, which has ever been used by the machine, is modified in the project editor then its saving is done in a different project directory.</source>
        <translation>Empêche l&apos;écrasement des consignes du projet. Si un projet, qui a déjà été utilisé par la machine, est modifié dans l&apos;éditeur de projet, alors son enregistrement se fait dans un répertoire de projet différent.</translation>
    </message>
    <message>
        <source>The name of this directory is automatically created with the date and hour of the modification.</source>
        <translation>Le nom de ce répertoire est créé automatiquement avec la date et l&apos;heure de la modification.</translation>
    </message>
    <message>
        <source>Test</source>
        <translation>Essai</translation>
    </message>
    <message>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <source>Enable backup</source>
        <translation>Active le backup</translation>
    </message>
    <message>
        <source>Double safeguard in another partition all the files of parameters and useful results in the event of loss of the partition of datas /home.</source>
        <translation>Sauvegarde en doublon dans une autre partition tous les fichiers de paramètres et de résultats utiles en cas de perte de la partition de données /home.</translation>
    </message>
    <message>
        <source>Backup path</source>
        <translation>Chemin du backup</translation>
    </message>
    <message>
        <source>Partition backup path.</source>
        <translation>Chemin de la partition backup.</translation>
    </message>
    <message>
        <source>Events</source>
        <translation>Evénements</translation>
    </message>
    <message>
        <source>Supervisor starting</source>
        <translation>Démarrage du superviseur</translation>
    </message>
    <message>
        <source>Supervisor stoping</source>
        <translation>Arrêt du superviseur</translation>
    </message>
    <message>
        <source>New calibration</source>
        <translation>Nouvel étalonnage</translation>
    </message>
    <message>
        <source>Last channel calibrated</source>
        <translation>Dernière voie calibrée</translation>
    </message>
    <message>
        <source>Viewer</source>
        <translation>Visualisateur</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Date and hour</source>
        <translation>Date et heure</translation>
    </message>
    <message>
        <source>Since</source>
        <translation>Depuis</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>From</source>
        <translation>Provenance</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Help of the selected event</source>
        <translation>Aide de l&apos;événement sélectionné</translation>
    </message>
    <message>
        <source>Mail</source>
        <translation>Mail</translation>
    </message>
    <message>
        <source>Enable the send of fault messages by email</source>
        <translation>Activation des mails sur événements</translation>
    </message>
    <message>
        <source>SMTP servor name</source>
        <translation>Serveur SMTP</translation>
    </message>
    <message>
        <source>SMTP port</source>
        <translation>Port SMTP</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">Domaine</translation>
    </message>
    <message>
        <source>SMTP user name</source>
        <translation>Nom utilisateur SMTP</translation>
    </message>
    <message>
        <source>SMTP password</source>
        <translation>Mote de passe SMTP</translation>
    </message>
    <message>
        <source>Sender</source>
        <translation>Émetteur</translation>
    </message>
    <message>
        <source>Receiver (s)</source>
        <translation>Destinataire(s)</translation>
    </message>
    <message>
        <source>If there is more than one address, separate them with commas.</source>
        <translation>S&apos;il y a plus qu&apos;une adresse, les séparer avec des virgules.</translation>
    </message>
    <message>
        <source>Subject</source>
        <translation>Sujet</translation>
    </message>
    <message>
        <source>Body</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Users</source>
        <translation>Utilisateurs</translation>
    </message>
    <message>
        <source>Simulation access</source>
        <translation>Accès de simulation</translation>
    </message>
    <message>
        <source>Current user</source>
        <translation>Utilisateur actuel</translation>
    </message>
    <message>
        <source>This information is also shown in the Cylix window title bar.</source>
        <translation>Cette information est également affichée dans la barre de titre de la fenêtre cylix.</translation>
    </message>
    <message>
        <source>Date and time of access change</source>
        <translation>Date et heure de changement d&apos;accès</translation>
    </message>
    <message>
        <source>Acquisition</source>
        <translation>Acquisition</translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation>Métrologie</translation>
    </message>
    <message>
        <source>Inhibits reset points</source>
        <translation>Inhibe RAZ points</translation>
    </message>
    <message>
        <source>Inhibits addition point</source>
        <translation>Inhibe ajout point</translation>
    </message>
    <message>
        <source>Inhibits suppression point</source>
        <translation>Inhibe suppression point</translation>
    </message>
    <message>
        <source>Inhibits calibration</source>
        <translation>Inhibe étalonnage</translation>
    </message>
    <message>
        <source>Inhibits verification</source>
        <translation>Inhibe vérification</translation>
    </message>
    <message>
        <source>Inhibits designer values</source>
        <translation>Inhibe valeurs constructeur</translation>
    </message>
    <message>
        <source>Full display</source>
        <translation>Affichage complet</translation>
    </message>
    <message>
        <source>Full display with all values in table and all curves.</source>
        <translation>Affichage complet avec toutes les valeurs dans le tableau et toutes les courbes.</translation>
    </message>
    <message>
        <source>Simplified display of table and curves without some values.</source>
        <translation>Affichage de la table et les courbes simplifiée sans certaines valeurs.</translation>
    </message>
    <message>
        <source>Curve color of deviation FS</source>
        <translation>Couleur courbe écart PE</translation>
    </message>
    <message>
        <source>Curve style of deviation FS</source>
        <translation>Style courbe écart PE</translation>
    </message>
    <message>
        <source>No Curve</source>
        <translation>Pas de courbe</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <source>Sticks</source>
        <translation>Bâtons</translation>
    </message>
    <message>
        <source>Steps</source>
        <translation>Échelons</translation>
    </message>
    <message>
        <source>Dots</source>
        <translation>Points</translation>
    </message>
    <message>
        <source>Curve color of linearity error</source>
        <translation>Couleur courbe erreur de linéarité</translation>
    </message>
    <message>
        <source>Curve style of linearity error</source>
        <translation>Style courbe erreur de linéarité</translation>
    </message>
    <message>
        <source>%1/calibrating</source>
        <translation>%1/etalonnage</translation>
    </message>
    <message>
        <source>Directory of PDF recording</source>
        <translation>Répertoire d&apos;enregistrement PDF</translation>
    </message>
    <message>
        <source>Directory containing the report file of verification or calibration. These are automatically classified into sensor subdirectories.</source>
        <translation>Répertoire contenant les PV de vérification ou d&apos;étalonnage. Ceux-ci y sont automatiquement classés dans des sous-répertoires par capteur.</translation>
    </message>
    <message>
        <source>The help of an event can contain one or more hyperlinks which allow to open the Cylix manual directly at a precise place, as for example the part relating to the setting which can influence its release.</source>
        <translation type="vanished">L&apos;aide d&apos;un événement peut contenir un ou plusieurs liens hypertextes qui permettent d&apos;ouvrir le manuel de Cylix directement à un endroit précis comme par exemple la partie relative au paramétrage pouvant influancer son déclenchement.</translation>
    </message>
    <message>
        <source>Opening the Cylix manual directly in the right place may not work if the default PDF viewer of the OS does not support opening URL links with given bookmarks at the end after the &apos;#&apos; character.</source>
        <translation type="vanished">L&apos;ouverture du manuel de Cylix directement au bon endroit peut ne pas fonctionner si le visualiseur PDF par défaut de l&apos;OS ne gère pas l&apos;ouverture de liens URL avec des marques pages données à la fin de ceux-ci après le caractère &apos;#&apos;.</translation>
    </message>
    <message>
        <source>Opening the Cylix manual directly in the right place may not work if the default PDF viewer of the OS does not support opening URL links with bookmarks (second par of URL after &apos;#&apos; character).</source>
        <translation type="obsolete">L&apos;ouverture du manuel Cylix directement au bon endroit peut ne pas fonctionner si le visualiseur PDF par défaut du système d&apos;exploitation ne prend pas en charge l&apos;ouverture des liens URL avec des signets (deuxième partie de l&apos;URL après le caractère &apos;#&apos;).</translation>
    </message>
    <message>
        <source>The help of an event is a real diagnostic aid to understand its cause.</source>
        <translation>L&apos;aide d&apos;un événement est une véritable aide au diagnostic pour en comprendre la cause.</translation>
    </message>
    <message>
        <source>One or more hyperlinks allow to open the Cylix manual directly at a precise place, as for example the part relating to the setting which can influence the event triggering.</source>
        <translation>Un ou plusieurs hyperliens permettent d&apos;ouvrir le manuel Cylix directement à un endroit précis, comme par exemple la partie relative au paramétrage qui peut influencer le déclenchement de l&apos;événement.</translation>
    </message>
    <message>
        <source>One or more data (measurements, setpoints, parameters...) allow to visualize directly values which can influence the event triggering. These values, in bold, are refreshed when the help is displayed, either when the event is selected in the event report.</source>
        <translation type="vanished">Une ou plusieurs données (mesures, consignes, paramètres...) permettent de visualiser directement les valeurs qui peuvent influencer le déclenchement de l&apos;événement. Ces valeurs, en gras, sont rafraîchies lors de l&apos;affichage de l&apos;aide, soit lorsque l&apos;événement est sélectionné dans le rapport d&apos;événement.</translation>
    </message>
    <message>
        <source>One or more data (measurements, setpoints, parameters...) allow to visualize directly values which can influence the event triggering. These values, in bold, are refreshed when the help is displayed.</source>
        <translation>Une ou plusieurs données (mesures, consignes, paramètres...) permettent de visualiser directement les valeurs qui peuvent influencer le déclenchement de l&apos;événement. Ces valeurs, en gras, sont rafraîchies lors de l&apos;affichage de l&apos;aide.</translation>
    </message>
    <message>
        <source>Project sent to the numerical regulator</source>
        <translation>Projet envoyé au régulateur numérique</translation>
    </message>
    <message>
        <source>Send project</source>
        <translation>Envoi du projet</translation>
    </message>
</context>
<context>
    <name>CYDI</name>
    <message>
        <source>Digital input</source>
        <translation>Entrée TOR</translation>
    </message>
    <message>
        <source>Unplugged</source>
        <translation>Non-connecté</translation>
    </message>
</context>
<context>
    <name>CYDO</name>
    <message>
        <source>Digital output</source>
        <translation>Sortie TOR</translation>
    </message>
    <message>
        <source>Unplugged</source>
        <translation>Non-connecté</translation>
    </message>
</context>
<context>
    <name>CYData</name>
    <message>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>Contrôle</translation>
    </message>
    <message>
        <source>Integer</source>
        <translation>Entier</translation>
    </message>
    <message>
        <source>Boolean</source>
        <translation>Booléen</translation>
    </message>
    <message>
        <source>Floating</source>
        <translation>Flottant</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>Mot 16 bits</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <source>%1 cannot be wrote because not in write mode !</source>
        <translation>%1 ne peut pas être écrit parce que pas en mode écriture!</translation>
    </message>
    <message>
        <source>Note :</source>
        <translation>Note: </translation>
    </message>
    <message>
        <source>Notes :</source>
        <translation>Notes: </translation>
    </message>
    <message>
        <source>Error of help data markup %1</source>
        <translation>Erreur de balisage de donnée d&apos;aide %1</translation>
    </message>
    <message>
        <source>The increase in value is too abrupt. The value of this data can only be increased by a maximum of %1% of its current value.</source>
        <translation>L&apos;augmentation de la valeur est trop brutale. La valeur de cette donnée ne peut être augmentée que de %1% maximum de sa valeur actuelle.</translation>
    </message>
    <message>
        <source>The decrease in value is too abrupt. The value of this data can only be decreased by a maximum of %1% of its current value.</source>
        <translation>La diminution de la valeur est trop brutale. La valeur de cette donnée ne peut être diminuée que de %1% maximum de sa valeur actuelle.</translation>
    </message>
</context>
<context>
    <name>CYDatasBrowser</name>
    <message>
        <source>User datas</source>
        <translation>Données utilisateur</translation>
    </message>
    <message>
        <source>System datas</source>
        <translation>Données système</translation>
    </message>
    <message>
        <source>All datas</source>
        <translation>Toutes données</translation>
    </message>
    <message>
        <source>The Seach bar makes it possible to search by words or characters among the filtered results. &lt;br/&gt;&lt;br/&gt; Only matches (data and/or categories) are then displayed.</source>
        <translation>La barre de recherche permet la recherche par mots ou caractères parmis les résultats filtrés. &lt;br/&gt;&lt;br/&gt; Seules les correspondances sont affichées.</translation>
    </message>
</context>
<context>
    <name>CYDatasBrowserList</name>
    <message>
        <source>Data Browser</source>
        <translation>Explorateur de données</translation>
    </message>
    <message>
        <source>Physical Type</source>
        <translation>Type physique</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Drag datas to empty cells of a analyse sheet.</source>
        <translation>Glisser des données vers les cellules vides de la feuille d&apos;analyse.</translation>
    </message>
    <message>
        <source>The data browser lists the connected hosts and the datas that they provide. Click and drag datas into drop zones of a analyse sheet. A display will appear that visualizes the values provided by the data. Some data displays can display values of multiple datas. Simply drag other datas on to the display to add more datas.</source>
        <translation>L&apos;explorateur de données liste les hôtes connectés et les données qu&apos;ils fournissent. Glissez les données à la souris dans les cellules de la feuille d&apos;analyse. Un affichage apparaîtra afin de consulter les valeurs fournies par la donnée. Certains affichages de données peuvent afficher des valeurs de plusieurs données. Glissez-déposez simplement d&apos;autres données vers l&apos;affichage pour les y ajouter.</translation>
    </message>
    <message>
        <source>Drag datas to empty fields in a work sheet</source>
        <translation>Déposez les données à mesurer dans les emplacements vides des feuilles d&apos;analyse</translation>
    </message>
</context>
<context>
    <name>CYDatasEditList</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>#</source>
        <translation>N°</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>Crayon</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>État</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>Nom Designer</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>Coefficient</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Connexion</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Hôte</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>State</source>
        <translation>État</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <source>Push this button to configure the color of the data in the oscilloscope.</source>
        <translation>Cliquer sur ce bouton pour changer la couleur de la donnée dans l&apos;oscilloscope.</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>&amp;Set Color</source>
        <translation>&amp;Changer de couleur</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Push this button to configure the pen of the data in the oscilloscope.</source>
        <translation>Cliquer sur ce bouton pour changer le crayon de la donnée dans l&apos;oscilloscope.</translation>
    </message>
    <message>
        <source>Set Pen</source>
        <translation>Changer de crayon</translation>
    </message>
    <message>
        <source>Push this button to configure the display coefficient of the data in the oscilloscope.</source>
        <translation>Cliquer sur ce bouton pour changer le coefficient d&apos;affichage de la donnée dans l&apos;oscilloscope.</translation>
    </message>
    <message>
        <source>Set Coefficient</source>
        <translation>Changer de coefficient</translation>
    </message>
    <message>
        <source>Set Offset</source>
        <translation>Changer l&apos;offset</translation>
    </message>
    <message>
        <source>Push this button to delete the data.</source>
        <translation>Cliquer sur ce bouton pour enlever la donnée de l&apos;oscilloscope.</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>Supp&amp;rimer</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
</context>
<context>
    <name>CYDatasListView</name>
    <message>
        <source>#</source>
        <translation>N°</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>Crayon</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>État</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>Nom Designer</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>Coefficient</translation>
    </message>
    <message>
        <source>Display coefficient</source>
        <translation>Coefficient d&apos;affichage</translation>
    </message>
    <message>
        <source>Enter the new display coefficient: </source>
        <translation>Entrer le nouveau coefficient d&apos;affichage: </translation>
    </message>
    <message>
        <source>You must enter a coefficient display different of 0 !</source>
        <translation>Vous devez entrer un coefficient d&apos;affichage différent de 0 !</translation>
    </message>
    <message>
        <source>Display offset</source>
        <translation>Offset d&apos;affichage</translation>
    </message>
    <message>
        <source>Enter the new display offset: </source>
        <translation>Entrer le nouvel offset d&apos;affichage: </translation>
    </message>
    <message>
        <source>Data label</source>
        <translation>Libellé de donnée</translation>
    </message>
    <message>
        <source>Enter the new label: </source>
        <translation>Entrer le nouveau libellé: </translation>
    </message>
    <message>
        <source>You can enter a maximun of %1 data!</source>
        <translation>Vous pouvez entrer un maximum de %1 données!</translation>
    </message>
    <message>
        <source>Acquisition list of datas full</source>
        <translation>Liste d&apos;acquisition de données pleine</translation>
    </message>
    <message>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
</context>
<context>
    <name>CYDatasTable</name>
    <message>
        <source>Datas table</source>
        <translation>Tableau de données</translation>
    </message>
    <message>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <source>Forcing</source>
        <translation>Forçage</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Connection(s)</source>
        <translation>Connexion(s)</translation>
    </message>
    <message>
        <source>Host(s)</source>
        <translation>Hôte(s)</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <source>Phys</source>
        <translation>Phys</translation>
    </message>
    <message>
        <source>Elec</source>
        <translation>Elec</translation>
    </message>
    <message>
        <source>ADC</source>
        <translation>ADC</translation>
    </message>
    <message>
        <source>Display</source>
        <translation>Afficheur</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>RAZ</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>Partiel</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>Contrôle</translation>
    </message>
    <message>
        <source>Addr</source>
        <translation>Addr</translation>
    </message>
    <message>
        <source>Threshold</source>
        <translation>Seuil</translation>
    </message>
    <message>
        <source>Column label</source>
        <translation>Libellé de colonne</translation>
    </message>
    <message>
        <source>Enter the label of this column:</source>
        <translation>Entrer le libellé de la colonne:</translation>
    </message>
    <message>
        <source>&amp;Forcing mode</source>
        <translation>Mode de &amp;forçage</translation>
    </message>
    <message>
        <source>Reset mode</source>
        <translation>Mode RAZ</translation>
    </message>
    <message>
        <source>Partial mode</source>
        <translation>Mode partiel</translation>
    </message>
    <message>
        <source>Note mode</source>
        <translation>Mode note</translation>
    </message>
    <message>
        <source>Alert threshold</source>
        <translation>Seuil d&apos;alerte</translation>
    </message>
    <message>
        <source>Hide column</source>
        <translation>Masquer la colonne</translation>
    </message>
    <message>
        <source>Show column</source>
        <translation>Afficher la colonne</translation>
    </message>
    <message>
        <source>Add column</source>
        <translation>Ajouter une colonne</translation>
    </message>
    <message>
        <source>Change column label</source>
        <translation>Changer le libellé de la colonne</translation>
    </message>
    <message>
        <source>Change column width</source>
        <translation>Changer la largeur de colonne</translation>
    </message>
    <message>
        <source>Remove row</source>
        <translation>Supprimer ligne</translation>
    </message>
    <message>
        <source>Change row height</source>
        <translation>Changer la hauteur de ligne</translation>
    </message>
    <message>
        <source>Export PDF</source>
        <translation>Export PDF</translation>
    </message>
    <message>
        <source>Input the new width of selected columns</source>
        <translation>Entrez la nouvelle largeur des colonnes sélectionnées</translation>
    </message>
    <message>
        <source>No row selected !</source>
        <translation>Pas de ligne sélectionnée !</translation>
    </message>
    <message>
        <source>Are you sure to remove the setected row(s) ?</source>
        <translation>Êtes-vous sûr de supprimer la (les) ligne(s) sélectionnée(s) ?</translation>
    </message>
    <message>
        <source>Input the new height of selected rows</source>
        <translation>Entrez la nouvelle hauteur des lignes sélectionnées</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>%1/table</source>
        <translation>%1/table</translation>
    </message>
    <message>
        <source>%1/table.pdf</source>
        <translation>%1/table.pdf</translation>
    </message>
    <message>
        <source>Can&apos;t print %1</source>
        <translation>Impossible d&apos;imprimer %1</translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation>Impossible d&apos;ouvrir %1</translation>
    </message>
    <message>
        <source>Page %1</source>
        <translation>Page %1</translation>
    </message>
</context>
<context>
    <name>CYDatasTableDisplayCell</name>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>No display for datas group</source>
        <translation>Aucun afficheur pour un groupe de données</translation>
    </message>
    <message>
        <source>Select a display type</source>
        <translation>Sélectionner un type d&apos;afficheur</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;Simple</translation>
    </message>
    <message>
        <source>&amp;Multimeter</source>
        <translation>&amp;Multimètre</translation>
    </message>
    <message>
        <source>&amp;Analog</source>
        <translation>&amp;Analogique</translation>
    </message>
    <message>
        <source>&amp;Digital</source>
        <translation>&amp;Numérique</translation>
    </message>
    <message>
        <source>&amp;Classical oscilloscope</source>
        <translation>&amp;Oscilloscope classique</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.</source>
        <translation>L&apos;oscilloscope peut avoir deux axes d&apos;ordonnées, chacun avec son propre format.</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.)</source>
        <translation type="vanished">Oscilloscope pouvant avoir deux axes d&apos;ordonnées, chacun avec son propre format.</translation>
    </message>
    <message>
        <source>Oscilloscope multiple formats</source>
        <translation>Oscilloscope multiples formats</translation>
    </message>
    <message>
        <source>Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets.</source>
        <translation>Oscilloscope pouvant avoir plusieurs formats de données différents sur un seul axe d&apos;ordonnées. Le libellé de chaque mesure est alors affiché avec son unité entre crochets . Le coefficient d&apos;affichage paramétrable, si celui-ci diffère de 1, s&apos;affiche également entre crochets .</translation>
    </message>
    <message>
        <source>&amp;BarGraph</source>
        <translation>Graphe en &amp;barres</translation>
    </message>
    <message>
        <source>Datas &amp;table</source>
        <translation>&amp;Tableau de données</translation>
    </message>
    <message>
        <source>No display for this type of data</source>
        <translation>Aucun afficheur pour ce type de donnée</translation>
    </message>
</context>
<context>
    <name>CYDatasTableSetup</name>
    <message>
        <source>Datas Table Settings</source>
        <translation>Configuration de la table de données</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>Donnée(s)</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
</context>
<context>
    <name>CYDial</name>
    <message>
        <source>Minimum value</source>
        <translation>Valeur minimum</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation>Valeur maximum</translation>
    </message>
    <message>
        <source>Scale step</source>
        <translation>Pas de l&apos;échelle</translation>
    </message>
</context>
<context>
    <name>CYDialScaleDialog</name>
    <message>
        <source>Scale setup</source>
        <translation>Configuration de l&apos;échelle</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>=</source>
        <translation>=</translation>
    </message>
</context>
<context>
    <name>CYDisplay</name>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>Automatic title</source>
        <translation>Titre automatique</translation>
    </message>
    <message>
        <source>Edit the data&apos;s label to change the title.</source>
        <translation>Éditer le libellé de la donnée pour changer le titre.</translation>
    </message>
    <message>
        <source>Enter the title in the box below.</source>
        <translation>Entrer le titre dans la boîte ci-dessous.</translation>
    </message>
    <message>
        <source>Add unit to the title</source>
        <translation>Ajouter l&apos;unité au titre</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>Font size</source>
        <translation>Taille de la police</translation>
    </message>
    <message>
        <source>&lt;qt&gt;&lt;p&gt;This is a data display. To customize a sensor display click and hold the right mouse button on either the frame or the display box and select the &lt;i&gt;Properties&lt;/i&gt; entry from the popup menu. Select &lt;i&gt;Remove&lt;/i&gt; to delete the display from the work sheet.&lt;/p&gt;%1&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;&lt;p&gt;Ceci est l&apos;afficheur d&apos;un donnée. Pour le personnaliser, cliquez avec le bouton droit de la souris soit sur le cadre, soit dans l&apos;affichage et maintenez le bouton enfoncé, puis cliquez sur &lt;i&gt;Propriétés&lt;/i&gt; dans le menu contextuel. Sélectionnez &lt;i&gt;Supprimer&lt;/i&gt; pour supprimer cet afficheur de la feuille de données.&lt;/p&gt;%1&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>Create a new file %1</source>
        <translation>Création d&apos;un nouveau fichier %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML ! %2: %3 %4</source>
        <translation>Le fichier %1 ne contient pas de code XML valide ! %2: %3 %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation>Impossible d&apos;enregistrer le fichier %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Impossible d&apos;enregistrer le fichier %1.</translation>
    </message>
    <message>
        <source>Display</source>
        <translation>Afficheur</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>Confi&amp;guration</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <source>&amp;Setup update interval</source>
        <translation>Inter&amp;valle de rafraîchissement</translation>
    </message>
    <message>
        <source>&amp;Continue update</source>
        <translation>&amp;Poursuivre le rafraîchissement</translation>
    </message>
    <message>
        <source>P&amp;ause update</source>
        <translation>In&amp;terrompre le rafraîchissement</translation>
    </message>
</context>
<context>
    <name>CYDisplayCell</name>
    <message>
        <source>The clipboard does not contain a valid display description.</source>
        <translation>Le presse-papiers ne contient aucune description d&apos;affichage valable.</translation>
    </message>
    <message>
        <source>Do you really want to delete the display?</source>
        <translation>Voulez-vous vraiment supprimer l&apos;affichage ?</translation>
    </message>
</context>
<context>
    <name>CYDisplayDummy</name>
    <message>
        <source>Empty cell</source>
        <translation>Cellule vide</translation>
    </message>
    <message>
        <source>analyse sheet</source>
        <translation>Feuille d&apos;analyse</translation>
    </message>
    <message>
        <source>datas table</source>
        <translation>Table de données</translation>
    </message>
    <message>
        <source>This is an empty cell in an %1. Drag a data or group of datas from the data browser and drop it here. A data(s) display will appear that allows you to monitor the values of the data(s) over time.</source>
        <translation>Ceci est une cellule vide dans %1. Glisser une donnée ou un groupe de données de l&apos;explorateur de données et la ou le déposer ici. Un afficheur de donnée apparaîtra et vous permettra de suivre dans le temps les valeurs de cette donnée ou de ce goupe de données.</translation>
    </message>
    <message>
        <source>Dummy display</source>
        <translation>Afficheur vide</translation>
    </message>
    <message>
        <source>&amp;Select a display type</source>
        <translation>&amp;Sélectionner un type d&apos;afficheur</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;Simple</translation>
    </message>
    <message>
        <source>&amp;Multimeter</source>
        <translation>&amp;Multimètre</translation>
    </message>
    <message>
        <source>&amp;Analog</source>
        <translation>&amp;Analogique</translation>
    </message>
    <message>
        <source>&amp;Digital</source>
        <translation>&amp;Numérique</translation>
    </message>
    <message>
        <source>&amp;Classical oscilloscope</source>
        <translation>&amp;Oscilloscope classique</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.</source>
        <translation>L&apos;oscilloscope peut avoir deux axes d&apos;ordonnées, chacun avec son propre format.</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.)</source>
        <translation type="vanished">Oscilloscope pouvant avoir deux axes d&apos;ordonnées, chacun avec son propre format.</translation>
    </message>
    <message>
        <source>Oscilloscope multiple formats</source>
        <translation>Oscilloscope multiples formats</translation>
    </message>
    <message>
        <source>Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets.</source>
        <translation>Oscilloscope pouvant avoir plusieurs formats de données différents sur un seul axe d&apos;ordonnées. Le libellé de chaque mesure est alors affiché avec son unité entre crochets . Le coefficient d&apos;affichage paramétrable, si celui-ci diffère de 1, s&apos;affiche également entre crochets .</translation>
    </message>
    <message>
        <source>&amp;BarGraph</source>
        <translation>Graphe en &amp;barres</translation>
    </message>
    <message>
        <source>Datas &amp;table</source>
        <translation>&amp;Tableau de données</translation>
    </message>
</context>
<context>
    <name>CYDisplayItem</name>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
</context>
<context>
    <name>CYDisplayListView</name>
    <message>
        <source>#</source>
        <translation>N°</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>État</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>Nom Designer</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>Connexion</translation>
    </message>
</context>
<context>
    <name>CYDisplaySatusIndicator</name>
    <message>
        <source>Connection is OK.</source>
        <translation>La connexion est bonne.</translation>
    </message>
    <message>
        <source>Connection has been lost.</source>
        <translation>La connexion a été perdue.</translation>
    </message>
    <message>
        <source>Update is pausing.</source>
        <translation>Rafraîchissement en pause.</translation>
    </message>
    <message>
        <source>The refresh of all graphic analyzers is pausing.</source>
        <translation>Le rafraîchissement de tous les outils d&apos;analyse graphique sont en pause.</translation>
    </message>
</context>
<context>
    <name>CYDisplaySimple</name>
    <message>
        <source>Simple display</source>
        <translation>Afficheur simple</translation>
    </message>
    <message>
        <source>Numerical base</source>
        <translation>Base numérique</translation>
    </message>
    <message>
        <source>The numerical base is used only if the data is only an unsigned integer type!</source>
        <translation>La base numérique n&apos;est utilisée uniquement si la données est de type entier non signé!</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>Binaire</translation>
    </message>
    <message>
        <source>Decimal</source>
        <translation>Décimale</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation>Hexadécimale</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation>Octal</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>Bicolor</source>
        <translation>Bicolore</translation>
    </message>
    <message>
        <source>Color On</source>
        <translation>Couleur LED allumée</translation>
    </message>
    <message>
        <source>Color Off</source>
        <translation>Couleur LED éteinte</translation>
    </message>
    <message>
        <source>Shape</source>
        <translation>Forme</translation>
    </message>
    <message>
        <source>Rectangular</source>
        <translation>Rectangulaire</translation>
    </message>
    <message>
        <source>Circular</source>
        <translation>Circulaire</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Aspect</translation>
    </message>
    <message>
        <source>Flat</source>
        <translation>Plat</translation>
    </message>
    <message>
        <source>Raised</source>
        <translation>Surélevé</translation>
    </message>
    <message>
        <source>Sunken</source>
        <translation>Encaissé</translation>
    </message>
    <message>
        <source>Show unit</source>
        <translation>Afficher l&apos;unité</translation>
    </message>
    <message>
        <source>Customized colors</source>
        <translation>Couleurs personnalisées</translation>
    </message>
    <message>
        <source>The colors will be those of data</source>
        <translation>Les couleurs seront celles de la donnée</translation>
    </message>
    <message>
        <source>Text color of valid value</source>
        <translation>Couleur de texte de valeur valide</translation>
    </message>
    <message>
        <source>Text color of not valid value</source>
        <translation>Couleur de texte de valeur non valide</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Couleur de fond</translation>
    </message>
</context>
<context>
    <name>CYDisplaySimpleSetup</name>
    <message>
        <source>Simple Display Settings</source>
        <translation>Configuration d&apos;un afficheur simple</translation>
    </message>
    <message>
        <source>&amp;General</source>
        <translation>&amp;Général</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>&amp;Style</source>
        <translation>&amp;Style</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation>Couleurs</translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation>&amp;Donnée</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYDisplayStyle</name>
    <message>
        <source>Color %1</source>
        <translation>Couleur %1</translation>
    </message>
</context>
<context>
    <name>CYDisplayStyleSetup</name>
    <message>
        <source>Global Style Settings</source>
        <translation>Configuration du style global</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>&amp;Apply to current analyse sheet</source>
        <translation>&amp;Appliquer à la feuille d&apos;analyse actuelle</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>Display Style</source>
        <translation>Style d&apos;affichage</translation>
    </message>
    <message>
        <source>&amp;Font Size</source>
        <translation>Taille de la &amp;Police</translation>
    </message>
    <message>
        <source>&amp;Grid Color</source>
        <translation>Couleur de la &amp;grille</translation>
    </message>
    <message>
        <source>A&amp;larm Color</source>
        <translation>Couleur de l&apos;&amp;alarme</translation>
    </message>
    <message>
        <source>Foreground Color &amp;1</source>
        <translation>Couleur du &amp;fond</translation>
    </message>
    <message>
        <source>Foreground Color &amp;2</source>
        <translation>Couleur du texte &amp;2</translation>
    </message>
    <message>
        <source>&amp;Background Color</source>
        <translation>Couleur de &amp;fond</translation>
    </message>
    <message>
        <source>Datas Colors</source>
        <translation>Couleurs des données</translation>
    </message>
    <message>
        <source>Change Color</source>
        <translation>Changer de couleur</translation>
    </message>
</context>
<context>
    <name>CYDisplayTimerSetup</name>
    <message>
        <source>Timer Settings</source>
        <translation>Configuration du temps</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Update Interval</source>
        <translation>Intervalle de rafraîchissement</translation>
    </message>
</context>
<context>
    <name>CYEvent</name>
    <message>
        <source>Event</source>
        <translation>Evénement</translation>
    </message>
    <message>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <source>Acknow.</source>
        <translation>Acquittement</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation>Opérateur</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
</context>
<context>
    <name>CYEventsBackup</name>
    <message>
        <source>Backup date</source>
        <translation>Date d&apos;archivage</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Select a date lower than today&apos;s.</source>
        <translation>Sélectionner une date inférieure à celle d&apos;aujourd&apos;hui.</translation>
    </message>
</context>
<context>
    <name>CYEventsGenerator</name>
    <message>
        <source>Events</source>
        <translation>Evénements</translation>
    </message>
</context>
<context>
    <name>CYEventsGeneratorSPV</name>
    <message>
        <source>Supervisor Events</source>
        <translation>Événements superviseur</translation>
    </message>
    <message>
        <source>Initializing supervisor events...</source>
        <translation>Initialisation des événements superviseur...</translation>
    </message>
    <message>
        <source>RS422 communication module not installed</source>
        <translation>Module de communication RS422 non installé</translation>
    </message>
    <message>
        <source>The RS422 communication module has not been correctly installed. Reinstall Cylix.</source>
        <translation>Le module de communication RS422 n&apos;a pas été installé correctement. Réinstaller Cylix.</translation>
    </message>
    <message>
        <source>Ethernet interprocess communication not installed</source>
        <translation>Interprocessus communication Ethernet non installé</translation>
    </message>
    <message>
        <source>Interprocess Ethernet communication</source>
        <translation type="vanished">Communication interprocessus Ethernet</translation>
    </message>
    <message>
        <source>The Ethernet interprocess communication software was not detected at startup Cylix. Reinstall Cylix.</source>
        <translation>Le logiciel interprocessus de communication Ethernet n&apos;a pas été détecté au démarrage Cylix. Réinstaller Cylix.</translation>
    </message>
    <message>
        <source>Ethernet interprocess communication</source>
        <translation>Interprocessus communication Ethernet</translation>
    </message>
    <message>
        <source>Cylix cannot communicate with the Ethernet interprocess communication software. Restart PC.</source>
        <translation>Cylix ne peut pas communiquer avec le logiciel interprocessus de communication Ethernet. Redémarrer le PC.</translation>
    </message>
    <message>
        <source>Interprocess communication for Ethernet communication</source>
        <translation type="vanished">Communication interprocessus pour communication Ethernet</translation>
    </message>
    <message>
        <source>The interprocess communication software for Ethernet communication was not detected at startup Cylix. Reinstall Cylix.</source>
        <translation type="vanished">Le logiciel de communication interprocessus pour la communication Ethernet n&apos;a pas été détecté au démarrage Cylix. Réinstallez Cylix.</translation>
    </message>
    <message>
        <source>Supervisor starting</source>
        <translation>Démarrage du superviseur</translation>
    </message>
    <message>
        <source>Time stamp of starting the cylix software.</source>
        <translation>Horodatage du démarrage du logiciel cylix.</translation>
    </message>
    <message>
        <source>Time stamp of stopping the cylix software.</source>
        <translation>Horodatage de l&apos;arrêt du logiciel cylix.</translation>
    </message>
    <message>
        <source>Time stamp of new archive generated by the acquisition tool.</source>
        <translation>Horodatage d&apos;une nouvelle archive générée par l&apos;outil d&apos;acquisition.</translation>
    </message>
    <message>
        <source>Starting the cylix software.</source>
        <translation type="vanished">Démarrage du logiciel cylix.</translation>
    </message>
    <message>
        <source>Supervisor stoping</source>
        <translation>Arrêt du superviseur</translation>
    </message>
    <message>
        <source>Stopping the cylix software.</source>
        <translation type="vanished">Démarrage du logiciel cylix.</translation>
    </message>
    <message>
        <source>New calibration</source>
        <translation>Nouvel étalonnage</translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation>Métrologie</translation>
    </message>
    <message>
        <source>Sensor calibration by the metrology tool.</source>
        <translation>Etalonnage cateur par l&apos;outil métrologie.</translation>
    </message>
    <message>
        <source>New archive: DATA_REC1</source>
        <translation>Nouvelle archive: DATA_REC1</translation>
    </message>
    <message>
        <source>New archive generated by the acquisition tool.</source>
        <translation type="vanished">Nouvelle archive générée par l&apos;outil d&apos;acquisition.</translation>
    </message>
    <message>
        <source>Please note that under Windows you have to install Cylix as administrator even if the current Windows session is already in administrator mode. To do this, right-click on the installation file to access this command. </source>
        <translation type="vanished">Attention, sous Windows il faut installer Cylix en tant qu&apos;administrateur même si la session Windows en cours est déjà en administrateur. Pour ce faire, faire un clic droit sur le fichier d&apos;installation afin d&apos;avoir accès à cette commande.</translation>
    </message>
    <message>
        <source>Please note that under Windows you have to install Cylix as administrator even if the current Windows session is already in administrator mode. To do this, right-click on the installation file to access this command.</source>
        <translation>Attention, sous Windows il faut installer Cylix en tant qu&apos;administrateur même si la session Windows en cours est déjà en administrateur. Pour ceci, faire un clic droit sur le fichier d&apos;installation afin d&apos;avoir accès à cette commande.</translation>
    </message>
    <message>
        <source>Send project</source>
        <translation>Envoi du projet</translation>
    </message>
    <message>
        <source>Notifies the sending of the project to the numerical regulator.</source>
        <translation>Notifie l&apos;envoi du projet au régulateur numérique.</translation>
    </message>
    <message>
        <source>This sending is also done automatically when the communication is detected.</source>
        <translation>Cet envoi se fait également automatiquement lorsque la communication est détectée.</translation>
    </message>
    <message>
        <source>Cannot create file : DATA_REC1</source>
        <translation>Impossible de créer le fichier : DATA_REC1</translation>
    </message>
    <message>
        <source>Check access rights to the acquisition directory.</source>
        <translation>Vérifier les droits d&apos;accès au répertoire d&apos;acquisition.</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Alerte</translation>
    </message>
</context>
<context>
    <name>CYEventsList</name>
    <message>
        <source>One or more data (measurements, setpoints, parameters...) allow to visualize directly values which can influence the event triggering. These values, in bold, are refreshed when the help is displayed.</source>
        <translation>Une ou plusieurs données (mesures, consignes, paramètres...) permettent de visualiser directement les valeurs qui peuvent influencer le déclenchement de l&apos;événement. Ces valeurs, en gras, sont rafraîchies lors de l&apos;affichage de l&apos;aide.</translation>
    </message>
    <message>
        <source>One or more hyperlinks allow to open the Cylix manual directly at a precise place, as for example the part relating to the setting which can influence the event triggering.</source>
        <translation>Un ou plusieurs hyperliens permettent d&apos;ouvrir le manuel Cylix directement à un endroit précis, comme par exemple la partie relative au paramétrage qui peut influencer le déclenchement de l&apos;événement.</translation>
    </message>
    <message>
        <source>These links, which are only visible in the tooltip, can be activated at the bottom of the window of events logs.</source>
        <translation>Ces liens, qui ne sont visibles que dans l&apos;infobulle, peuvent être activés en bas de la fenêtre des journaux d&apos;événements.</translation>
    </message>
</context>
<context>
    <name>CYEventsMailingWidget</name>
    <message>
        <source>SMTP servor</source>
        <translation>Serveur SMTP</translation>
    </message>
    <message>
        <source>&amp;Test</source>
        <translation>&amp;Essai</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>You must be in administrator access to modifie this values</source>
        <translation>Vous devez être en accès administrateur pour modifier ces valeurs</translation>
    </message>
</context>
<context>
    <name>CYEventsManager</name>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Impossible d&apos;ouvrir le fichier %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML</source>
        <translation>Le fichier %1 ne contient pas un code XML valide</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation>Impossible d&apos;enregistrer le fichier %1!</translation>
    </message>
    <message>
        <source>Events list</source>
        <translation>Liste des événements</translation>
    </message>
    <message>
        <source>Name, message and help</source>
        <translation>Nom, message et assistance</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nom</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">Message</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Aide</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Impossible d&apos;enregistrer le fichier %1.</translation>
    </message>
    <message>
        <source>Cannot find the events generator %1</source>
        <translation>Impossible de trouver le générateur d&apos;événements %1</translation>
    </message>
</context>
<context>
    <name>CYEventsPreferences</name>
    <message>
        <source>Events settings</source>
        <translation>Configuration des événements</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Page</source>
        <translation>Page</translation>
    </message>
    <message>
        <source>Mail</source>
        <translation>Mail</translation>
    </message>
    <message>
        <source>&amp;Designer Value</source>
        <translation>Valeur &amp;constructeur</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYEventsReport</name>
    <message>
        <source>Events report (%1) of %2 since %3

</source>
        <translation>Journal d&apos;événements (%1) de %2 depuis %3

</translation>
    </message>
    <message>
        <source> COLOR	TIME	SINCE	TYPE	FROM	DESCRIPTION	NAME
</source>
        <translation> COULEUR	TEMPS	DEBUT	TYPE	PROVENANCE	DESCRIPTION	NOM
</translation>
    </message>
</context>
<context>
    <name>CYEventsView</name>
    <message>
        <source>Date and hour</source>
        <translation>Date et heure</translation>
    </message>
    <message>
        <source>Events file</source>
        <translation>Fichier d&apos;événements</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Since</source>
        <translation>Depuis</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>From</source>
        <translation>Provenance</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>The test directory of this events file has been moved or removed.</source>
        <translation>Le répertoire d&apos;essai de ce journal d&apos;événements a été déplacé ou supprimé.</translation>
    </message>
    <message>
        <source>This events file has been moved or removed.</source>
        <translation>Ce fichier d&apos;événements a été déplacé ou supprimé.</translation>
    </message>
    <message>
        <source>Select a events report to load</source>
        <translation>Sélectionner le journal d&apos;événements à charger</translation>
    </message>
    <message>
        <source>The current events file changed!
Do you want to view this one?</source>
        <translation>Le fichier d&apos;événements courant à changé!
Voulez-vous visualiser celui-ci?</translation>
    </message>
    <message>
        <source>No help provided !</source>
        <translation>Pas d&apos;aide disponible !</translation>
    </message>
</context>
<context>
    <name>CYEventsWindow</name>
    <message>
        <source>Events reports</source>
        <translation>Journaux d&apos;événements</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <source>Open &amp;Recent</source>
        <translation>&amp;Récemment ouvert(s)</translation>
    </message>
    <message>
        <source>Settings events...</source>
        <translation>Configuration des événements...</translation>
    </message>
    <message>
        <source>&amp;Current</source>
        <translation>&amp;Courant</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Backup</source>
        <translation>&amp;Archivage</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>&amp;Configuration</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Affichage</translation>
    </message>
    <message>
        <source>&amp;Columns</source>
        <translation>&amp;Colonnes</translation>
    </message>
    <message>
        <source>&amp;Types</source>
        <translation>&amp;Types</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <source>&amp;None</source>
        <translation>&amp;Aucun</translation>
    </message>
    <message>
        <source>Their end</source>
        <translation>Leur fin</translation>
    </message>
    <message>
        <source>Their acknowledgement</source>
        <translation>Leur acquittement</translation>
    </message>
    <message>
        <source>&amp;From</source>
        <translation>&amp;Provenance</translation>
    </message>
</context>
<context>
    <name>CYExtMeasure</name>
    <message>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Actif</translation>
    </message>
    <message>
        <source>Scaling</source>
        <translation>Mise à l&apos;échelle</translation>
    </message>
    <message>
        <source>High value</source>
        <translation>Point haut</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation>Point bas</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation>Surveillance</translation>
    </message>
    <message>
        <source>Level</source>
        <translation>Seuil</translation>
    </message>
    <message>
        <source>Alert tolerance interval</source>
        <translation>Intervalle de tolérance d&apos;alerte</translation>
    </message>
    <message>
        <source>Fault tolerance interval</source>
        <translation>Intervalle de tolérance de défaut</translation>
    </message>
    <message>
        <source>Calibration type</source>
        <translation>Type de calibrage</translation>
    </message>
</context>
<context>
    <name>CYExtMeasureEdit</name>
    <message>
        <source>#</source>
        <translation type="vanished">N°</translation>
    </message>
    <message>
        <source>Scale adjust points</source>
        <translation>Mise à l&apos;échelle des points</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Bas</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Haut</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation>Surveillance</translation>
    </message>
    <message>
        <source>± </source>
        <translation>± </translation>
    </message>
    <message>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Configuration</translation>
    </message>
</context>
<context>
    <name>CYFileCSV</name>
    <message>
        <source>Maximum of lines per file</source>
        <translation>Nombre de lignes par fichier</translation>
    </message>
    <message>
        <source>Maximum file size</source>
        <translation>Taille maximale d&apos;un fichier</translation>
    </message>
</context>
<context>
    <name>CYFlag</name>
    <message>
        <source>If it is:</source>
        <translation>Si c&apos;est:</translation>
    </message>
    <message>
        <source>If you select:</source>
        <translation>Si vous sélectionnez:</translation>
    </message>
    <message>
        <source>Designer value:</source>
        <translation>Valeur constructeur:</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>Binaire</translation>
    </message>
</context>
<context>
    <name>CYFlagDialog</name>
    <message>
        <source>Dialog input of flag data</source>
        <translation>Boîte de saisie de flag</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYFlagInput</name>
    <message>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
</context>
<context>
    <name>CYG7Cnt</name>
    <message>
        <source>Counter %1 of grafcet %2</source>
        <translation>Compteur %1 du grafcet %2</translation>
    </message>
    <message>
        <source>Grafcet counter</source>
        <translation>Compteur grafcet</translation>
    </message>
</context>
<context>
    <name>CYG7Tim</name>
    <message>
        <source>Timer %1 of grafcet %2</source>
        <translation>Temporisation %1 du grafcet %2</translation>
    </message>
    <message>
        <source>Grafcet timer</source>
        <translation>Temporisation grafcet</translation>
    </message>
</context>
<context>
    <name>CYImageList</name>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Impossible d&apos;ouvrir le fichier %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Le fichier %1 ne contient pas de code XML valide
%2: ligne:%3 colonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation>Impossible d&apos;enregistrer le fichier %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Impossible d&apos;enregistrer le fichier %1.</translation>
    </message>
</context>
<context>
    <name>CYImageListEdit</name>
    <message>
        <source>N°</source>
        <translation>N°</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <source>Fichier</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <source>Dele&amp;te</source>
        <translation>Supp&amp;rimer</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation>O&amp;k</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Canc&amp;el</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Choose an image to add into this list.</source>
        <translation>Choisir une image à ajouter dans cette liste.</translation>
    </message>
</context>
<context>
    <name>CYListCali</name>
    <message>
        <source>Inputs / Outputs Calibration</source>
        <translation>Calibrage des Entrées / Sorties</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Ana.</source>
        <translation>Ana.</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
</context>
<context>
    <name>CYMV</name>
    <message>
        <source>Forward</source>
        <translation>Aller</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation>Retour</translation>
    </message>
    <message>
        <source>Sensor loss timer %1</source>
        <translation>Temporisation perte capteur %1</translation>
    </message>
    <message>
        <source>Maximum delay before the falling edge of the backward sensor.</source>
        <translation>Temps maximum avant le front descendant du capteur fin de course retour.</translation>
    </message>
    <message>
        <source>If this time is reached, a failure is signalled.</source>
        <translation>Si ce temps est atteint, une défaillance est signalée.</translation>
    </message>
    <message>
        <source>Sensor appearence timer %1</source>
        <translation>Temporisation apparition capteur %1</translation>
    </message>
    <message>
        <source>Maximum delay before the rising edge of the forward sensor.</source>
        <translation>Temps maximum avant le front montant du capteur fin de course aller.</translation>
    </message>
    <message>
        <source>Maximum delay before the falling edge of the forward sensor.</source>
        <translation>Temps maximum avant le front descendant du capteur fin de course aller.</translation>
    </message>
    <message>
        <source>Maximum delay before the rising edge of the backward sensor.</source>
        <translation>Temps maximum avant le front montant du capteur fin de course retour.</translation>
    </message>
    <message>
        <source>Sensor failure signalisation %1</source>
        <translation>Signalisation défaillance capteur %1</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <source>A faillure generates a fault.</source>
        <translation>Une défaillance capteur génère un défaut.</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Alerte</translation>
    </message>
    <message>
        <source>A faillure generates an alert and then the timers simulate the sensors.</source>
        <translation>Une défaillance capteur génère une alerte et les temporisations simulent alors les capteurs.</translation>
    </message>
    <message>
        <source>Initialized movement</source>
        <translation>Mouvement initialisé</translation>
    </message>
    <message>
        <source>Positioning</source>
        <translation>Positionnement</translation>
    </message>
    <message>
        <source>Grafcet current state</source>
        <translation>Étape courante du grafcet</translation>
    </message>
    <message>
        <source>No sensor presence %1</source>
        <translation>Non apparition capteur %1</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>Process</translation>
    </message>
    <message>
        <source>The maximum waiting time of rising edge of the sensor of end of forward stroke has been exceeded by the movement forward.</source>
        <translation>Le temps maximum d&apos;attente du front montant du capteur fin de course aller à été dépassé lors du mouvement aller.</translation>
    </message>
    <message>
        <source>This may indicate a sensor failure or movement.</source>
        <translation>Cela peut indiquer un défaut du capteur ou du mouvement.</translation>
    </message>
    <message>
        <source>No sensor loss %1</source>
        <translation>Non perte capteur %1</translation>
    </message>
    <message>
        <source>The maximum waiting time of falling edge of the sensor of end of forward stroke has been exceeded by the movement backward.</source>
        <translation>Le temps maximum d&apos;attente du front descendant du capteur fin de course aller à été dépassé lors du mouvement retour.</translation>
    </message>
    <message>
        <source>Sensor loss %1</source>
        <translation>Perte capteur %1</translation>
    </message>
    <message>
        <source>The forward sensor was lost in forward static.</source>
        <translation>Le capteur aller à été perdu en statique aller.</translation>
    </message>
    <message>
        <source>This may indicate an adjustment defect of the sensor.</source>
        <translation>Cela peut indiquer un défaut de réglage du capteur.</translation>
    </message>
    <message>
        <source>The maximum waiting time of rising edge of the sensor of end of backward stroke has been exceeded by the movement backward.</source>
        <translation>Le temps maximum d&apos;attente du front montant du capteur fin de course retour à été dépassé lors du mouvement retour.</translation>
    </message>
    <message>
        <source>The maximum waiting time of falling edge of the sensor of end of backward stroke has been exceeded by the movement forward.</source>
        <translation>Le temps maximum d&apos;attente du front descendant du capteur fin de course retour à été dépassé lors du mouvement aller.</translation>
    </message>
    <message>
        <source>The backward sensor was lost in backward static.</source>
        <translation>Le capteur retour à été perdu en statique retour.</translation>
    </message>
    <message>
        <source>Sensor coherence %1/%2</source>
        <translation>Cohérence capteur %1/%2</translation>
    </message>
    <message>
        <source>The sensors forward and backward were actived at the same time.</source>
        <translation>Les capteurs aller et retour ont été actifs en même temps.</translation>
    </message>
    <message>
        <source>This may indicate an adjustment defect of the sensors.</source>
        <translation>Cela peut indiquer un défaut de réglage des capteurs.</translation>
    </message>
</context>
<context>
    <name>CYMVInput</name>
    <message>
        <source>Forward motion</source>
        <translation>Mouvement aller</translation>
    </message>
    <message>
        <source>Backward motion</source>
        <translation>Mouvement retour</translation>
    </message>
</context>
<context>
    <name>CYMainWin</name>
    <message>
        <source>Languages</source>
        <translation>Langues</translation>
    </message>
    <message>
        <source>Test: %1 (Access: %2)</source>
        <translation>Test: %1 (Accès: %2)</translation>
    </message>
    <message>
        <source>Test: %1</source>
        <translation>Test: %1</translation>
    </message>
    <message>
        <source>The new language will be taken into account the next time the application will be started.</source>
        <translation>La nouvelle langue sera prise en compte au redémarrage de l&apos;application.</translation>
    </message>
</context>
<context>
    <name>CYManuCali</name>
    <message>
        <source>&amp;Designer value</source>
        <translation>Valeur &amp;constructeur</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>&amp;Low value</source>
        <translation>Point &amp;bas</translation>
    </message>
    <message>
        <source>&amp;High value</source>
        <translation>Point &amp;haut</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Haut</translation>
    </message>
    <message>
        <source>High Value</source>
        <translation>Point haut</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Bas</translation>
    </message>
    <message>
        <source>Low Value</source>
        <translation>Point bas</translation>
    </message>
    <message>
        <source>Do you really want to load the designer&apos;s values ?</source>
        <translation>Voulez-vous vraiment charger les valeurs constructeurs ?</translation>
    </message>
</context>
<context>
    <name>CYManuCaliInput</name>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
</context>
<context>
    <name>CYMeasure</name>
    <message>
        <source>Flag</source>
        <translation>Flag</translation>
    </message>
    <message>
        <source>Calibration</source>
        <translation>Étalonnage</translation>
    </message>
    <message>
        <source>Indicates that the date of calibration of the sensor is exceeded.</source>
        <translation>Indique que la date de calibrage du capteur est dépassé.</translation>
    </message>
    <message>
        <source>Sensor to calibrate</source>
        <translation>Capteur à calibrer</translation>
    </message>
    <message>
        <source>Indicates that the sensor was calibrated to the onset of this event.</source>
        <translation>Indique que le capteur a été calibré à l&apos;apparition de cet événement.</translation>
    </message>
    <message>
        <source>Sensor calibrated</source>
        <translation>Capteur calibré</translation>
    </message>
    <message>
        <source>Flag of validity of the calibration</source>
        <translation>Flag de validité de l&apos;étalonnage</translation>
    </message>
    <message>
        <source>High value</source>
        <translation>Point haut</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation>Point bas</translation>
    </message>
    <message>
        <source>High value (not calibrated)</source>
        <translation>Point haut (non calibré)</translation>
    </message>
    <message>
        <source>Maximum value of device usage range.</source>
        <translation>Valeur maximale de la plage d&apos;utilisation du matériel.</translation>
    </message>
    <message>
        <source>Low value (not calibrated)</source>
        <translation>Point bas (non calibré)</translation>
    </message>
    <message>
        <source>Minimum value of device usage range.</source>
        <translation>Valeur minimale de la plage d&apos;utilisation du matériel.</translation>
    </message>
    <message>
        <source>Date of last calibration</source>
        <translation>Date du dernier étalonnage</translation>
    </message>
    <message>
        <source>Date of last verification</source>
        <translation>Date de la dernière vérification</translation>
    </message>
    <message>
        <source>Date of next calibration</source>
        <translation>Date du prochain étalonnage</translation>
    </message>
    <message>
        <source>If this date is expired an alert will be generated to remind to calibrate the sensor %1.</source>
        <translation>Si cette date est expirée une alerte sera générée pour rappeler de calibrer le capteur %1.</translation>
    </message>
    <message>
        <source>Date of next verification</source>
        <translation>Date de la prochaine vérification</translation>
    </message>
    <message>
        <source>Calibration control</source>
        <translation>Contrôle calibrage</translation>
    </message>
    <message>
        <source>Alert for next verification</source>
        <translation>Alerte pour la prochaine vérification</translation>
    </message>
    <message>
        <source>Generates an alert in case of overtaking of the date of next verification.</source>
        <translation>Génère une alerte en cas de dépassement de la date de la prochaine vérification.</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation>Métrologie</translation>
    </message>
    <message>
        <source>Number of points used for calibration</source>
        <translation>Nombre de point utiliser pour l&apos;étalonnage</translation>
    </message>
    <message>
        <source>Direct bench sensor</source>
        <translation>Capteur banc direct</translation>
    </message>
    <message>
        <source>New point</source>
        <translation>Nouveau point</translation>
    </message>
    <message>
        <source>Bench sensor</source>
        <translation>Capteur banc</translation>
    </message>
    <message>
        <source>Reference sensor</source>
        <translation>Capteur étalon</translation>
    </message>
    <message>
        <source>Direct bench values</source>
        <translation>Valeurs banc directes</translation>
    </message>
    <message>
        <source>Point %1</source>
        <translation>Point: %1</translation>
    </message>
    <message>
        <source>Bench values</source>
        <translation>Valeurs du banc</translation>
    </message>
    <message>
        <source>Reference values</source>
        <translation>Valeurs étalon</translation>
    </message>
    <message>
        <source>Averaging times</source>
        <translation>Temps de moyennage</translation>
    </message>
    <message>
        <source>Calibrating mode</source>
        <translation>Mode de calibrage</translation>
    </message>
    <message>
        <source>Manual calibrating</source>
        <translation>Calibrage manuel</translation>
    </message>
    <message>
        <source>Automatic calibrating</source>
        <translation>Calibrage automatique</translation>
    </message>
    <message>
        <source>Adding a calibration point by reading the average of the direct value of the sensor.</source>
        <translation>Ajout d&apos;un point de calibrage par lecture de la moyenne de la valeur directe du capteur.</translation>
    </message>
    <message>
        <source>Adding a calibration point by entering the direct value of the sensor.</source>
        <translation>Ajout d&apos;un point de calibrage par saisie de la valeur directe du capteur.</translation>
    </message>
    <message>
        <source>Manufacter</source>
        <translation>Constructeur</translation>
    </message>
    <message>
        <source>P/N (Type)</source>
        <translation>P/N (Type)</translation>
    </message>
    <message>
        <source>S/N (Serial N)</source>
        <translation>S/N (N Série)</translation>
    </message>
    <message>
        <source>Uncertainty type</source>
        <translation>Type d&apos;incertitude</translation>
    </message>
    <message>
        <source>Uncertainty - % FS</source>
        <translation>Incertitude - % PE</translation>
    </message>
    <message>
        <source>The total measurement uncertainty of the sensor is expressed as a percentage of the Full Scale of the sensor. It is then fixed for each measurement and can also be expressed in sensor unit.</source>
        <translation>L&apos;incertitude de mesure totale du capteur est exprimée en pourcentage de la pleine échelle du capteur. Elle est donc figée pour chaque mesurage et peut aussi être exprimée en unitée capteur.</translation>
    </message>
    <message>
        <source>Uncertainty - fixed value</source>
        <translation>Incertitude - valeur fixe</translation>
    </message>
    <message>
        <source>The total measurement uncertainty of the sensor is expressed in sensor unit. It is then fixed for each measurement and can also be expressed as a percentage of the Full Scale of the sensor.</source>
        <translation>L&apos;incertitude de mesure totale du capteur est exprimée en unité capteur. Elle est donc figée pour chaque mesurage et peut aussi être exprimée en pourcentage de la pleine échelle du capteur.</translation>
    </message>
    <message>
        <source>Uncertainty - % of reading</source>
        <translation>Incertitude - % lecture</translation>
    </message>
    <message>
        <source>The total measurement uncertainty of the sensor is expressed as a percentage of the read measure.</source>
        <translation>L&apos;incertitude de mesure totale du capteur est exprimée en pourcentage de la mesure lue.</translation>
    </message>
    <message>
        <source>Environment</source>
        <translation>Environnement</translation>
    </message>
    <message>
        <source>month(s)</source>
        <translation>Mois</translation>
    </message>
    <message>
        <source>Periodicity of control</source>
        <translation>Périodicité du contrôle</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation>Opérateur</translation>
    </message>
    <message>
        <source>Equipment reference</source>
        <translation>Référence équipement</translation>
    </message>
    <message>
        <source>Designer values</source>
        <translation>Valeurs constructeur</translation>
    </message>
    <message>
        <source>Procedure</source>
        <translation>Procédure</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <source>Report file</source>
        <translation>PV</translation>
    </message>
    <message>
        <source>Event</source>
        <translation>Evénement</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Alerte</translation>
    </message>
    <message>
        <source>Numeric</source>
        <translation>Numérique</translation>
    </message>
    <message>
        <source>Sensor sheet</source>
        <translation>Fiche capteur</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>High value (FS)</source>
        <translation>Point haut (PE)</translation>
    </message>
    <message>
        <source>Low value (FS)</source>
        <translation>Point bas (PE)</translation>
    </message>
    <message>
        <source>Verification in accordance</source>
        <translation>Vérification conforme</translation>
    </message>
    <message>
        <source>Verification not in accordance</source>
        <translation>Vérification non conforme</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>Résumé</translation>
    </message>
    <message>
        <source>Last calibration</source>
        <translation>Dernier étalonnage</translation>
    </message>
    <message>
        <source>Last verification</source>
        <translation>Dernière vérification</translation>
    </message>
    <message>
        <source>Next verification</source>
        <translation>Prochaine vérification</translation>
    </message>
    <message>
        <source>Calibrating</source>
        <translation>Étalonnage</translation>
    </message>
    <message>
        <source>Editing</source>
        <translation>Édition</translation>
    </message>
    <message>
        <source>Viewing</source>
        <translation>Visualisation</translation>
    </message>
    <message>
        <source>No calibration</source>
        <translation>Non étalonné</translation>
    </message>
    <message>
        <source>Maximum number of ADC points</source>
        <translation>Nombre maxi de points ADC</translation>
    </message>
    <message>
        <source>Minimum number of ADC points</source>
        <translation>Nombre mini de points ADC</translation>
    </message>
    <message>
        <source>Sensor defect</source>
        <translation>Défaut capteur</translation>
    </message>
    <message>
        <source>Measure</source>
        <translation>Mesure</translation>
    </message>
</context>
<context>
    <name>CYMeasureSetting</name>
    <message>
        <source>Measurement setting</source>
        <translation>Configuration mesure</translation>
    </message>
    <message>
        <source>Calibration type</source>
        <translation type="vanished">Type de calibrage</translation>
    </message>
    <message>
        <source>Signal type</source>
        <translation>Type de signal</translation>
    </message>
    <message>
        <source>Indicates that the type or scale of the sensor have been changed. So, it&apos;s recommended to make a new calibrate of sensor.</source>
        <translation>Indique que le type ou l&apos;échelle du capteur a été modifié. Il est donc recommandé de faire un nouveau calibrage du capteur.</translation>
    </message>
    <message>
        <source>%1: Sensor changed</source>
        <translation>%1: Capteur modifié</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>Alerte</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>Libellé</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>Actif</translation>
    </message>
    <message>
        <source>Scaling</source>
        <translation>Mise à l&apos;échelle</translation>
    </message>
    <message>
        <source>High value</source>
        <translation>Point haut</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation>Point bas</translation>
    </message>
</context>
<context>
    <name>CYMessageBox</name>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Oui</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>C&amp;ontinue</source>
        <translation>&amp;Continuer</translation>
    </message>
</context>
<context>
    <name>CYMetroCaliInput</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>Direct</source>
        <translation>Direct</translation>
    </message>
    <message>
        <source>Reference sensor</source>
        <translation>Capteur étalon</translation>
    </message>
    <message>
        <source>Bench sensor</source>
        <translation>Capteur banc</translation>
    </message>
    <message>
        <source>Average of bench sensor</source>
        <translation>Moyenne du capteur banc</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Calibrating point &apos;%1&apos;</source>
        <translation>Point d&apos;étalonnage &apos;%1&apos;</translation>
    </message>
    <message>
        <source>%1 : New point</source>
        <translation>%1 : Nouveau point</translation>
    </message>
    <message>
        <source>ADC</source>
        <translation>ADC</translation>
    </message>
</context>
<context>
    <name>CYMetroCalibrate</name>
    <message>
        <source>Infos</source>
        <translation>Infos</translation>
    </message>
    <message>
        <source>#</source>
        <translation type="vanished">N°</translation>
    </message>
    <message>
        <source>Curves</source>
        <translation>Courbes</translation>
    </message>
    <message>
        <source>Points</source>
        <translation>Points</translation>
    </message>
    <message>
        <source>Mini
(%1)</source>
        <translation>Mini
(%1)</translation>
    </message>
    <message>
        <source>Standard
(%1)</source>
        <translation>Étalon
(%1)</translation>
    </message>
    <message>
        <source>Maxi
(%1)</source>
        <translation>Maxi
(%1)</translation>
    </message>
    <message>
        <source>Bench
(%1)</source>
        <translation>Banc
(%1)</translation>
    </message>
    <message>
        <source>Error
(%1)</source>
        <translation>Erreur
(%1)</translation>
    </message>
    <message>
        <source>Error
(% FS)</source>
        <translation>Erreur
(% PE)</translation>
    </message>
    <message>
        <source>Bench
(ADC)</source>
        <translation>Banc
(ADC)</translation>
    </message>
    <message>
        <source>Straight
Line
 (%1)</source>
        <translation>Ligne
Droite
 (%1)</translation>
    </message>
    <message>
        <source>Linearity
error
(% FS)</source>
        <translation>Erreur de
linéarité
(% PE)</translation>
    </message>
    <message>
        <source>Averaging
time</source>
        <translation>Temps de
moyennage</translation>
    </message>
    <message>
        <source>(sec)</source>
        <translation>(sec)</translation>
    </message>
    <message>
        <source>Gross
(%1)</source>
        <translation>Brute
(%1)</translation>
    </message>
    <message>
        <source>Calibrating sheet &apos;%1&apos; of %2 (Modified)</source>
        <translation>Fiche d&apos;étalonnage &apos;%1&apos; of %2 (Modifiée)</translation>
    </message>
    <message>
        <source>Do you want to validate this calibration for %1 ?</source>
        <translation>Voulez-vous valider cet étalonnage pour %1 ?</translation>
    </message>
    <message>
        <source>End of verification de %1 !
In accordance ?</source>
        <translation>Fin de vérification de %1 !
Conforme ?</translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation>Impossible d&apos;ouvrir %1</translation>
    </message>
    <message>
        <source>Do you want to close the calibration sheet of %1 without saving the latest changes ?</source>
        <translation>Voulez-vous fermer la feuille d&apos;étalonnage de %1 sans enregistrer les dernière modifications ?</translation>
    </message>
    <message>
        <source>Mini uncertainty</source>
        <translation>Incertitude mini</translation>
    </message>
    <message>
        <source>Maxi uncertainty</source>
        <translation>Incertitude maxi</translation>
    </message>
    <message>
        <source>Linearity error (%)</source>
        <translation>Erreur de linéarité (%)</translation>
    </message>
    <message>
        <source>Error (% FS)</source>
        <translation>Erreur (% PE)</translation>
    </message>
    <message>
        <source>Linearity error (% FS)</source>
        <translation>Erreur de linéarité (% PE)</translation>
    </message>
    <message>
        <source>%1_%2_%3</source>
        <translation>%1_%2_%3</translation>
    </message>
    <message>
        <source>_calibrating</source>
        <translation>_etalonnage</translation>
    </message>
    <message>
        <source>_verification</source>
        <translation>_verification</translation>
    </message>
    <message>
        <source>%1/%2</source>
        <translation>%1/%2</translation>
    </message>
    <message>
        <source>%1/%2.pdf</source>
        <translation>%1/%2.pdf</translation>
    </message>
    <message>
        <source>Page %1/%2</source>
        <translation>Page %1/%2</translation>
    </message>
    <message>
        <source>Page %1/2</source>
        <translation>Page %1/2</translation>
    </message>
</context>
<context>
    <name>CYMetroSensorSheet</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>#</source>
        <translation type="vanished">N°</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Edit sensor sheet &apos;%1&apos;</source>
        <translation>Édition fiche capteur &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Are you sure to validate this sensor sheet ?
If yes, it is recommended to redo the calibration !</source>
        <translation>Etes-vous sûr de valider cette fiche capteur ?
Si oui, il est recommandé de refaire son étalonnage !</translation>
    </message>
    <message>
        <source>Change sensor sheet &apos;%1&apos;</source>
        <translation>Changer fiche capteur &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>CYMetroSensorSheetView</name>
    <message>
        <source>Sensor sheet</source>
        <translation>Fiche capteur</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>High point</source>
        <translation>Point haut</translation>
    </message>
    <message>
        <source>Low point</source>
        <translation>Point bas</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
</context>
<context>
    <name>CYMetroSettings</name>
    <message>
        <source>Metrology settings</source>
        <translation>Configuration métrologie</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>Designer &amp;Value</source>
        <translation>Valeur &amp;constructeur</translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Clo&amp;se</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Page</source>
        <translation>Page</translation>
    </message>
    <message>
        <source>View</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <source>0</source>
        <translation>0</translation>
    </message>
</context>
<context>
    <name>CYMetroWin</name>
    <message>
        <source>Metrology tool</source>
        <translation>Outil de métrologie</translation>
    </message>
    <message>
        <source>With alert</source>
        <translation>Avec alerte</translation>
    </message>
    <message>
        <source>Without alert</source>
        <translation>Sans alerte</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <source>Sensors</source>
        <translation>Capteurs</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>&amp;Configuration</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>Sensor sheet</source>
        <translation>Fiche capteur</translation>
    </message>
    <message>
        <source>Clear table</source>
        <translation>Effacer tableau</translation>
    </message>
    <message>
        <source>Add point</source>
        <translation>Ajouter point</translation>
    </message>
    <message>
        <source>Delete point</source>
        <translation>Supprimer point</translation>
    </message>
    <message>
        <source>Calibrate</source>
        <translation>Étalonner</translation>
    </message>
    <message>
        <source>Verification</source>
        <translation>Vérification</translation>
    </message>
    <message>
        <source>Designer values</source>
        <translation>Valeurs constructeur</translation>
    </message>
    <message>
        <source>Metrology settings</source>
        <translation>Configuration métrologie</translation>
    </message>
</context>
<context>
    <name>CYMultimeter</name>
    <message>
        <source>Multimeter</source>
        <translation>Multimètre</translation>
    </message>
    <message>
        <source>Edit the data&apos;s label to change the title</source>
        <translation>Éditer le libellé de la donnée pour changer le titre</translation>
    </message>
    <message>
        <source>Analog display</source>
        <translation>Afficheur analogique</translation>
    </message>
    <message>
        <source>Digital display</source>
        <translation>Afficheur numérique</translation>
    </message>
    <message>
        <source>Numerical base</source>
        <translation>Base numérique</translation>
    </message>
    <message>
        <source>The numerical base is used only if the data is only an unsigned integer type!</source>
        <translation>La base numérique n&apos;est utilisée uniquement si la données est de type entier non signé!</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>Binaire</translation>
    </message>
    <message>
        <source>Decimal</source>
        <translation>Décimale</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation>Hexadécimale</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation>Octal</translation>
    </message>
    <message>
        <source>Alarm</source>
        <translation>Alerte</translation>
    </message>
    <message>
        <source>Upper bound value</source>
        <translation>Valeur limite haute</translation>
    </message>
    <message>
        <source>Upper alarm enable</source>
        <translation>Alarme haute active</translation>
    </message>
    <message>
        <source>Lower bound value</source>
        <translation>Valeur limite basse</translation>
    </message>
    <message>
        <source>Lower alarm enable</source>
        <translation>Alarme active basse</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>Analog</source>
        <translation>Analogique</translation>
    </message>
    <message>
        <source>Normal color</source>
        <translation>Couleur normale</translation>
    </message>
    <message>
        <source>Alarm color</source>
        <translation>Couleur d&apos;alarme</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Couleur de fond</translation>
    </message>
    <message>
        <source>Text color</source>
        <translation>Couleur de texte</translation>
    </message>
    <message>
        <source>Digital</source>
        <translation>Numérique</translation>
    </message>
</context>
<context>
    <name>CYMultimeterSetup</name>
    <message>
        <source>Multimeter Settings</source>
        <translation>Configuration du multimètre</translation>
    </message>
    <message>
        <source>&amp;General</source>
        <translation>&amp;Général</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>&amp;Style</source>
        <translation>&amp;Style</translation>
    </message>
    <message>
        <source>Analog</source>
        <translation>Analogique</translation>
    </message>
    <message>
        <source>Digital</source>
        <translation>Numérique</translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation>&amp;Donnée</translation>
    </message>
    <message>
        <source>Analog scale</source>
        <translation>Échelle analogique</translation>
    </message>
    <message>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYNetConnect</name>
    <message>
        <source>Connect Host</source>
        <translation>Connexion d&apos;un hôte</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Select the name of the host you want to connect to.</source>
        <translation>Sélection du nom de l&apos;hôte à connecter.</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Co&amp;nnect</source>
        <translation>Co&amp;nnecter</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Configure connections</source>
        <translation>Configuration des connexions</translation>
    </message>
    <message>
        <source>COM</source>
        <translation>COM</translation>
    </message>
    <message>
        <source>State</source>
        <translation>État</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
</context>
<context>
    <name>CYNetLink</name>
    <message>
        <source>Connected</source>
        <translation>Connecté</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>Déconnecté</translation>
    </message>
    <message>
        <source>%1 kHz</source>
        <translation>%1 kHz</translation>
    </message>
    <message>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
</context>
<context>
    <name>CYNetR422F</name>
    <message>
        <source>File exchange with regulator</source>
        <translation>Échange de fichier avec le régulateur</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation>O&amp;k</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Doesn&apos;t exist &apos;%1&apos; !</source>
        <translation>&apos;%1&apos; innexistant !</translation>
    </message>
    <message>
        <source>Updating of regulator</source>
        <translation>Mise à jour régulateur</translation>
    </message>
    <message>
        <source>Are you sure to update this regulator !</source>
        <translation>Êtes vous sûr de mettre à jour le régulateur !</translation>
    </message>
    <message>
        <source>Its serial number must be %1, while the software you want install has a as serial number %2 !</source>
        <translation>Son numéro de série doit être %1, alors que le logiciel que vous voulez installer a comme numéro de série %2 !</translation>
    </message>
    <message>
        <source>Update of regulator from the version %1 to the version %2. </source>
        <translation>Mise à jour du régulateur de la version %1 à la version %2. </translation>
    </message>
    <message>
        <source>Are you sure to install on the regulator the version %1 of the software %2 !</source>
        <translation>Êtes vous sûr d&apos;installer sur le régulateur la version %1 du logiciel %2 !</translation>
    </message>
    <message>
        <source>Install the content of this archive on this regulator !</source>
        <translation>Installer le contenu de l&apos;archive sur le régulateur !</translation>
    </message>
    <message>
        <source>Cannot remove the old directory %1 !</source>
        <translation>Impossible de supprimer l&apos;ancien répertoire %1!</translation>
    </message>
    <message>
        <source>Cannot start uncompressing file of update (%1)!</source>
        <translation>Impossible de lancer le décompression du fichier de mise à jour (%1)!</translation>
    </message>
    <message>
        <source>Cannot finish uncompressing file of update (%1)!</source>
        <translation>Impossible de terminer le décompression du fichier de mise à jour (%1)!</translation>
    </message>
    <message>
        <source>Init file doesn&apos;t exist !</source>
        <translation>Fichier d&apos;init innexixtant !</translation>
    </message>
    <message>
        <source>Section %1 unknown in the init file !</source>
        <translation>Section %1 inconnue dans le fichier d&apos;init !</translation>
    </message>
    <message>
        <source>Cannot open the init file !</source>
        <translation>Impossible d&apos;ouvrir le fichier d&apos;init !</translation>
    </message>
    <message>
        <source>The destination directory path &apos;%1&apos; is too long !</source>
        <translation>Le chemin du répertoire de destination &apos;%1&apos; est trop long !</translation>
    </message>
    <message>
        <source>The source directory path &apos;%1&apos; is too long !</source>
        <translation>Le chemin du répertoire source &apos;%1&apos; est trop long !</translation>
    </message>
    <message>
        <source>The file name &apos;%1&apos; is not correct for transfert file !</source>
        <translation>Le nom de fichier &apos;%1&apos; n&apos;est pas correct pour le transfert de fichier !</translation>
    </message>
    <message>
        <source>The file name &apos;%1&apos; is too long !</source>
        <translation>Le nom de fichier &apos;%1&apos; est trop long !</translation>
    </message>
    <message>
        <source>&apos;%1&apos; doesn&apos;t exist !</source>
        <translation>&apos;%1&apos; innexistant !</translation>
    </message>
    <message>
        <source>Cannot open the file &apos;%1&apos; !</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos; !</translation>
    </message>
    <message>
        <source>File %1 is empty!</source>
        <translation>Le fichier %1 est vide!</translation>
    </message>
    <message>
        <source>Install %1 in %2</source>
        <translation>Installer %1 dans %2</translation>
    </message>
    <message>
        <source>Save %1 in %2</source>
        <translation>Sauvegarder %1 dans %2</translation>
    </message>
    <message>
        <source>Regulator is blocked in file transfert mode !</source>
        <translation>Régulateur bloqué en mode transfert de fichier !</translation>
    </message>
    <message>
        <source>YOU MUST REBOOT MANUALLY THE REGULATOR !</source>
        <translation>VOUS DEVEZ REDÉMARRER MANUELLEMENT LE RÉGULATEUR !</translation>
    </message>
    <message>
        <source>Regulator ok !</source>
        <translation>Régulateur ok!</translation>
    </message>
    <message>
        <source>Stop of communication driver in normal mode.</source>
        <translation>Arrêt du driver de communication en mode normal.</translation>
    </message>
    <message>
        <source>Stop of communication driver in file transfert mode.</source>
        <translation>Arrêt du driver de communication en mode transfert de fichier.</translation>
    </message>
    <message>
        <source>Regulator disconnected.</source>
        <translation>Régulateur déconnecté.</translation>
    </message>
    <message>
        <source>Cannot reboot the regulator !</source>
        <translation>Impossible de redémarrer le régulateur !</translation>
    </message>
    <message>
        <source>Waiting for disconnection (Maximum time %1 sec)...</source>
        <translation>Attente déconnexion (Temps maximum %1 sec)...</translation>
    </message>
    <message>
        <source>Restart of communication driver in file transfert mode.</source>
        <translation>Redémarrage du driver de communication en mode transfert de fichier.</translation>
    </message>
    <message>
        <source>Restart of communication driver in normal mode.</source>
        <translation>Redémarrage du driver de communication en mode normal.</translation>
    </message>
    <message>
        <source>Cannot read info datas from regulator !</source>
        <translation>Impossible de lire les données d&apos;info du régulateur !</translation>
    </message>
    <message>
        <source>Regulator reconnected in normal mode instead of file transfert mode !</source>
        <translation>Régulateur reconnecté en mode normal au lieu du mode transfert de fichier !</translation>
    </message>
    <message>
        <source>Regulator reconnected in file transfert mode (R422F %1).</source>
        <translation>Régulateur reconnecté en mode transfert de fichier (R422F %1).</translation>
    </message>
    <message>
        <source>Waiting for reconnection (Maximum time %1 sec)...</source>
        <translation>Attente reconnexion (Temps maximum %1 sec)...</translation>
    </message>
    <message>
        <source>Cannot reconnect to the regulator !</source>
        <translation>Impossible se reconnecter au régulateur !</translation>
    </message>
    <message>
        <source>Regulator reconnected in file transfert mode (R422F %1) instead of normal mode !</source>
        <translation>Régulateur reconnecté en mode transfert de fichier (R422F %1) au lieu du mode normal !</translation>
    </message>
    <message>
        <source>Regulator reconnected in normal mode.</source>
        <translation>Régulateur reconnecté en mode normal.</translation>
    </message>
    <message>
        <source>Regulator rebooting...</source>
        <translation>Redémarrage du régulateur...</translation>
    </message>
    <message>
        <source>Starting file transfert...</source>
        <translation>Démarrage transfert de fichier...</translation>
    </message>
    <message>
        <source>No file to transfert !</source>
        <translation>Pas de fichier à transférer !</translation>
    </message>
    <message>
        <source>Install : %1 (%2 %)</source>
        <translation>Installer : %1 (%2 %)</translation>
    </message>
    <message>
        <source>No answer from the regulator !</source>
        <translation>Pas de réponse du régulateur !</translation>
    </message>
    <message>
        <source>Cannot remove file &apos;%1\%2&apos; on the regulator !</source>
        <translation>Impossible de supprimer le fichier &apos;%1\%2&apos; sur le régulateur !</translation>
    </message>
    <message>
        <source>Cannot make file &apos;%1\%2&apos; on the regulator !</source>
        <translation>Impossible de créer le fichier &apos;%1\%2&apos; sur le régulateur !</translation>
    </message>
    <message>
        <source>Block number error &apos;%1&apos; !</source>
        <translation>Erreur numéro block &apos;%1&apos; !</translation>
    </message>
    <message>
        <source>File size error &apos;%1\%2&apos; !</source>
        <translation>Erreur de taille de fichier &apos;%1\%2&apos; !</translation>
    </message>
    <message>
        <source>Regulator state unknown : &apos;%1&apos; !</source>
        <translation>État du régulateur inconnu : &apos;%1&apos; !</translation>
    </message>
    <message>
        <source>Bad size block to read !</source>
        <translation>Mauvaise taille de block à lire !</translation>
    </message>
    <message>
        <source>Bad num block to read !</source>
        <translation>Mauvais numéro de block à lire !</translation>
    </message>
    <message>
        <source>Save : %1 (%2 %)</source>
        <translation>Sauver : %1 (%2 %)</translation>
    </message>
    <message>
        <source>No file name from the regulator !</source>
        <translation>Pas de nom de fichier en provenance du régulateur !</translation>
    </message>
    <message>
        <source>Cannot open file &apos;%1&apos; !</source>
        <translation>Impossible d&apos;ouvrir le fichier &apos;%1&apos; !</translation>
    </message>
    <message>
        <source>Cannot find directory &apos;%1&apos; !</source>
        <translation>Impossible de trouver le répertoire &apos;%1&apos; !</translation>
    </message>
    <message>
        <source>Cannot find file &apos;%1&apos; !</source>
        <translation>Impossible de trouver le fichier &apos;%1&apos; !</translation>
    </message>
    <message>
        <source>End of file transfert.</source>
        <translation>Fin du transfert de fichier.</translation>
    </message>
    <message>
        <source>You can&apos;t close this box before the end of the transfert !</source>
        <translation>Vous ne pouvez fermer cette boîte avant la fin du transfert de fichier !</translation>
    </message>
    <message>
        <source>File transfert</source>
        <translation>Transfert de fichier</translation>
    </message>
</context>
<context>
    <name>CYNumDialog</name>
    <message>
        <source>Dialog input of numerical data</source>
        <translation>Boîte de saisie de donnée numérique</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYNumDisplay</name>
    <message>
        <source>The input text must be a numeric value !</source>
        <translation>Le texte saisie doit être une valeur numérique!</translation>
    </message>
    <message>
        <source>Cannot find the data %1</source>
        <translation>Impossible de trouver la donnée %1</translation>
    </message>
</context>
<context>
    <name>CYNumInput</name>
    <message>
        <source>Can&apos;t represent value %1 in terms of fixed-point numbers with precision %2</source>
        <translation>Impossible de représenter la valeur %1 en termes de nombres à virgule avec précision %2</translation>
    </message>
    <message>
        <source>Can&apos;t represent value %1 in terms of fixed-point numbers with precision %2 %3/%4 = %5</source>
        <translation>Impossible de représenter la valeur %1 en termes de nombres à virgule avec précision %2 %3/%4 =%5</translation>
    </message>
</context>
<context>
    <name>CYPID</name>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Proportional</source>
        <translation>Proportionnelle</translation>
    </message>
    <message>
        <source>Proportional Integral</source>
        <translation>Proportionnelle Intégrale</translation>
    </message>
    <message>
        <source>Proportional Derivative</source>
        <translation>Proportionnelle Dérivée</translation>
    </message>
    <message>
        <source>Proportional Integral Derivative</source>
        <translation>Proportionnelle Intégrale Dérivée</translation>
    </message>
    <message>
        <source>Integral</source>
        <translation>Intégrale</translation>
    </message>
    <message>
        <source>Proportional band coefficient</source>
        <translation>Bande proportionnelle</translation>
    </message>
    <message>
        <source>Proportional coefficient</source>
        <translation>Coefficient de proportionnalité</translation>
    </message>
    <message>
        <source>If 0, the proportional control won&apos;t be active (It can work in integral mode)</source>
        <translation>Si vous saisissez la valeur 0 le correcteur proportionnel est désactivée (La régulation peut fonctionner en mode intégrateur pur).</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>Cette régulation est une P.I.D. mixte.</translation>
    </message>
    <message>
        <source>Proportional band</source>
        <translation>Bande proportionnelle</translation>
    </message>
    <message>
        <source>Integral coefficient</source>
        <translation>Coefficient d&apos;intégrale</translation>
    </message>
    <message>
        <source>If 0, the integral control won&apos;t be in operation.</source>
        <translation>Si vous saisissez la valeur 0 la correction intégrale est désactivée.</translation>
    </message>
    <message>
        <source>Integral/derivative ratio</source>
        <translation>Rapport Intégrale/Dérivée</translation>
    </message>
    <message>
        <source>According to Ziegler and Nichols the best ratio is 4.0.</source>
        <translation>D&apos;après Ziegler et Nichols le meilleur rapport est de 4.0.</translation>
    </message>
    <message>
        <source>If 0, the derivative control won&apos;t be in operation.</source>
        <translation>Si vous saisissez la valeur 0 la correction dérivée est désactivée.</translation>
    </message>
    <message>
        <source>Negative output limit</source>
        <translation>Limitation de la sortie (Négative)</translation>
    </message>
    <message>
        <source>Output limit.</source>
        <translation>Limitation de la sortie.</translation>
    </message>
    <message>
        <source>Positive output limit</source>
        <translation>Limitation de la sortie (Positive)</translation>
    </message>
    <message>
        <source>Derivative type</source>
        <translation>Type de dérivée</translation>
    </message>
    <message>
        <source>On the gap</source>
        <translation>Sur l&apos;écart</translation>
    </message>
    <message>
        <source>On the measure.</source>
        <translation>Sur la mesure.</translation>
    </message>
    <message>
        <source>On the setpoint.</source>
        <translation>Sur la consigne.</translation>
    </message>
    <message>
        <source>If you select:</source>
        <translation>Si vous sélectionnez:</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The derivative calculation will be done on the gap (setpoint - measure).</source>
        <translation>Ceci peut être utile pour éviter un dépassement de mesure à la fin de l&apos;essai. Le calcul de la dérivée se fera sur l&apos;écart (consigne - mesure).</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure.</source>
        <translation>Le calcul de la dérivée se fera sur la mesure.</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the setpoint.</source>
        <translation>Le calcul de la dérivée se fera sur la consigne.</translation>
    </message>
    <message>
        <source>Measure derivative</source>
        <translation>Dérivée de la mesure</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure. The derivative effect is based on the error variation : setpoint – measurement. This may be useful to avoid overshoot at the ramp end.</source>
        <translation>Le calcul de la dérivée se fera sur la mesure. L&apos;effet de dérivée est basé sur la variation d&apos;erreur : consigne - mesure. Ceci peut être utile pour éviter un dépassement en fin de rampe.</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure.The derivative effect is based on the error variation : setpoint – measurement.This may be useful to avoid overshoot at the ramp end.</source>
        <translation type="vanished">Le calcul de la dérivée se fait sur la mesure. L&apos;effet de dérivée est basé sur la variation de l&apos;erreur : consigne - mesure. Ceci peut être utile pour éviter un dépassement en fin de rampe.</translation>
    </message>
    <message>
        <source>The derivative effect is based on the setpoint variation.</source>
        <translation>L&apos;effet de dérivée est basé sur la variation de la consigne.</translation>
    </message>
    <message>
        <source>Ramp on PID setpoint</source>
        <translation>Rampe sur la consigne du PID</translation>
    </message>
    <message>
        <source>The PID setpoint will be a ramp form one.</source>
        <translation>La consigne de la PID sera celle de la rampe.</translation>
    </message>
    <message>
        <source>It will be the final value (No ramp).</source>
        <translation>Cela sera la valeur finale (pas de rampe).</translation>
    </message>
    <message>
        <source>Integral frozen in ramp</source>
        <translation>Intégrale bloquée en rampe</translation>
    </message>
    <message>
        <source>The integral value will frozen in ramp.</source>
        <translation>La valeur de l&apos;intégrale sera bloquée en rampe.</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The integral value will be modified normally by the PID control.</source>
        <translation>Ceci peut être utile pour éviter des dépassements de mesure (overshoot) à la fin de la rampe. La valeur intégrale sera modifiée normalement par la PID de régulation.</translation>
    </message>
</context>
<context>
    <name>CYPIDCalculD</name>
    <message>
        <source>Measure derivative</source>
        <translation>Dérivée de la mesure</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure.</source>
        <translation>Le calcul de la dérivée se fera sur la mesure.</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The derivative calculation will be done on the gap (setpoint - measure).</source>
        <translation>Ceci peut être utile pour éviter un dépassement de mesure à la fin de l&apos;essai. Le calcul de la dérivée se fera sur l&apos;écart (consigne - mesure).</translation>
    </message>
</context>
<context>
    <name>CYPIDI</name>
    <message>
        <source>Integral coefficient</source>
        <translation>Coefficient d&apos;intégrale</translation>
    </message>
    <message>
        <source>If 0, the integral control won&apos;t be in operation.</source>
        <translation>Si vous saisissez la valeur 0 la correction intégrale est désactivée.</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>Cette régulation est une P.I.D. mixte.</translation>
    </message>
</context>
<context>
    <name>CYPIDInput</name>
    <message>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>- </source>
        <translation>- </translation>
    </message>
    <message>
        <source>+ </source>
        <translation>+ </translation>
    </message>
</context>
<context>
    <name>CYPIDK</name>
    <message>
        <source>Integral/derivative ratio</source>
        <translation>Rapport Intégrale/Dérivée</translation>
    </message>
    <message>
        <source>According to Ziegler and Nichols the best ratio is 4.0.</source>
        <translation>D&apos;après Ziegler et Nichols le meilleur rapport est de 4.0.</translation>
    </message>
    <message>
        <source>If 0, the derivative control won&apos;t be in operation.</source>
        <translation>Si vous saisissez la valeur 0 la correction dérivée est désactivée.</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>Cette régulation est une P.I.D. mixte.</translation>
    </message>
</context>
<context>
    <name>CYPIDMax</name>
    <message>
        <source>Positive output limit</source>
        <translation>Limitation de la sortie (Positive)</translation>
    </message>
    <message>
        <source>Output limit.</source>
        <translation>Limitation de la sortie.</translation>
    </message>
</context>
<context>
    <name>CYPIDMin</name>
    <message>
        <source>Negative output limit</source>
        <translation>Limitation de la sortie (Négative)</translation>
    </message>
    <message>
        <source>Output limit.</source>
        <translation>Limitation de la sortie.</translation>
    </message>
</context>
<context>
    <name>CYPIDNoI</name>
    <message>
        <source>Integral frozen in ramp</source>
        <translation>Intégrale bloquée en rampe</translation>
    </message>
    <message>
        <source>The integral value will frozen in ramp.</source>
        <translation>La valeur de l&apos;intégrale sera bloquée en rampe.</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The integral value will be modified normally by the PID control.</source>
        <translation>Ceci peut être utile pour éviter des dépassements de mesure (overshoot) à la fin de la rampe. La valeur intégrale sera modifiée normalement par la PID de régulation.</translation>
    </message>
</context>
<context>
    <name>CYPIDP</name>
    <message>
        <source>Proportional coefficient</source>
        <translation>Coefficient de proportionnalité</translation>
    </message>
    <message>
        <source>If 0, the proportional control won&apos;t be active (It can work in integral mode)</source>
        <translation>Si vous saisissez la valeur 0 le correcteur proportionnel est désactivée (La régulation peut fonctionner en mode intégrateur pur).</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>Cette régulation est une P.I.D. mixte.</translation>
    </message>
</context>
<context>
    <name>CYPIDRamp</name>
    <message>
        <source>Ramp on PID setpoint</source>
        <translation>Rampe sur la consigne du PID</translation>
    </message>
    <message>
        <source>The PID setpoint will be a ramp form one.</source>
        <translation>La consigne de la PID sera celle de la rampe.</translation>
    </message>
    <message>
        <source>It will be the final value (No ramp).</source>
        <translation>Cela sera la valeur finale (pas de rampe).</translation>
    </message>
</context>
<context>
    <name>CYPasswordDialog</name>
    <message>
        <source>&amp;Password:</source>
        <translation>Mot de &amp;passe:</translation>
    </message>
    <message>
        <source>&amp;Keep password</source>
        <translation>&amp;Conserver le mot de passe</translation>
    </message>
    <message>
        <source>&amp;Verify:</source>
        <translation>&amp;Vérifier:</translation>
    </message>
    <message>
        <source>Password strength meter:</source>
        <translation>Analyseur de mot de passe:</translation>
    </message>
    <message>
        <source>The password strength meter gives an indication of the security of the password you have entered. To improve the strength of the password, try:
 - using a longer password;
 - using a mixture of upper- and lower-case letters;
 - using numbers or symbols, such as #, as well as letters.</source>
        <translation>Le vérificateur du niveau de sécurité du mot de passe donne une indication de la sécurité du mot de passe que vous avez saisi. Pour améliorer la force du mot de passe, essayez :
- en utilisant un mot de passe plus long;
- en utilisant un mélange de lettres majuscules et minuscules;
- en utilisant des nombres ou des symboles tels que #, ainsi que des lettres.</translation>
    </message>
    <message>
        <source>Passwords do not match</source>
        <translation>Les mots de passe ne correspondent pas</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>You entered two different passwords. Please try again.</source>
        <translation>Vous avez saisi deux mots de passe différents. Veuillez réessayer.</translation>
    </message>
    <message>
        <source>The password you have entered has a low strength. To improve the strength of the password, try:
 - using a longer password;
 - using a mixture of upper- and lower-case letters;
 - using numbers or symbols as well as letters.

Would you like to use this password anyway?</source>
        <translation>Le mot de passe saisi est faible. Pour améliorer la force du mot de passe, essayez:
- en utilisant un mot de passe plus long;
- en utilisant un mélange de lettres majuscules et minuscules;
- en utilisant des nombres ou des symboles ainsi que des lettres.

Voulez-vous utiliser ce mot de passe de toute façon?</translation>
    </message>
    <message>
        <source>Low Password Strength</source>
        <translation>Faible force de mot de passe</translation>
    </message>
    <message>
        <source>Password change</source>
        <translation>Changement de mot de passe</translation>
    </message>
    <message>
        <source>Password is empty</source>
        <translation>Le mot de passe est vide</translation>
    </message>
    <message>
        <source>Password must be at least 1 character long</source>
        <translation>Le mot de passe doit comporter au moins 1 caractère</translation>
    </message>
    <message>
        <source>Password must be at least %1 characters long</source>
        <translation>Le mot de passe doit comporter au moins %1 caractères</translation>
    </message>
    <message>
        <source>Passwords match</source>
        <translation>Les mots de passe correspondent</translation>
    </message>
</context>
<context>
    <name>CYPen</name>
    <message>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>No line at all</source>
        <translation>Pas de ligne</translation>
    </message>
    <message>
        <source>A simple line</source>
        <translation>Une simple ligne</translation>
    </message>
    <message>
        <source>Dashes separated by a few pixels</source>
        <translation>Tirets séparés par quelques pixels</translation>
    </message>
    <message>
        <source>Dots separated by a few pixels</source>
        <translation>Points séparés par quelques pixels</translation>
    </message>
    <message>
        <source>Alternate dots and dashes</source>
        <translation>Points et tirets alternés</translation>
    </message>
    <message>
        <source>One dash, two dots, one dash, two dots</source>
        <translation>Un tiret, deux points, un tiret, deux points</translation>
    </message>
    <message>
        <source>Width</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
</context>
<context>
    <name>CYPenDialog</name>
    <message>
        <source>Pen edit dialog</source>
        <translation>Boîte d&apos;édition d&apos;un crayon</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>Crayon</translation>
    </message>
</context>
<context>
    <name>CYProject</name>
    <message>
        <source>The file %1 does not contain valid XML !
%2: %3 %4</source>
        <translation>Le fichier %1 ne contient pas de code XML valide!
%2: %3 %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation>Impossible d&apos;enregistrer le fichier %1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Impossible d&apos;enregistrer le fichier %1.</translation>
    </message>
</context>
<context>
    <name>CYProjectEditor</name>
    <message>
        <source>Select project file</source>
        <translation>Sélectionner le fichier projet</translation>
    </message>
    <message>
        <source>The selected object is not a file !</source>
        <translation>L&apos;objet sélectionné n&apos;est pas un fichier !</translation>
    </message>
    <message>
        <source>Select a project file ends with &quot;.cyprj&quot;.</source>
        <translation>Sélectionner un fichier projet qui finit par &quot;.cyprj&quot;.</translation>
    </message>
    <message>
        <source>Cannot find the project !</source>
        <translation>Impossible de trouver le projet !</translation>
    </message>
    <message>
        <source>The selected file is not a project file !</source>
        <translation>Le fichier sélectionné n&apos;est pas un fichier projet !</translation>
    </message>
    <message>
        <source>Do you want to load an existing project or create a new project ?</source>
        <translation>Voulez-vous charger un projet existant ou en créer un nouveau ?</translation>
    </message>
    <message>
        <source>Cannot find the current project !</source>
        <translation>Impossible de trouver le projet courant !</translation>
    </message>
    <message>
        <source>&amp;Create</source>
        <translation>&amp;Créer</translation>
    </message>
    <message>
        <source>&amp;Load</source>
        <translation>Char&amp;ger</translation>
    </message>
    <message>
        <source>The project cannot be saved because the editor has been open in read only mode!</source>
        <translation>Le projet ne peut pas être enregistré car l&apos;éditeur a été ouvert en mode lecture seule!</translation>
    </message>
    <message>
        <source>If you save this project the restart test will be forbidden !</source>
        <translation>Si vous enregistrez ce projet le redémarrage d&apos;essai sera interdit !</translation>
    </message>
    <message>
        <source>The project was modified.
Do you want to save your changes?</source>
        <translation>Le projet a été modifié.
Voulez-vous enregistrer les changements?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Changements non enregistrés</translation>
    </message>
    <message>
        <source>View: %1</source>
        <translation>Visualisation: %1</translation>
    </message>
    <message>
        <source> (Execution following ON)</source>
        <translation> (Suivi d&apos;exécution actif)</translation>
    </message>
    <message>
        <source>Edit: %1</source>
        <translation>Edition: %1</translation>
    </message>
    <message>
        <source>Edit: %1 (modified)</source>
        <translation>Edition: %1 (modifié)</translation>
    </message>
    <message>
        <source>Project error</source>
        <translation>Erreur projet</translation>
    </message>
</context>
<context>
    <name>CYProjectTestDirDialog</name>
    <message>
        <source>Test directory</source>
        <translation>Répertoire d&apos;essai</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="vanished">textLabel</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Name of the new test directory</source>
        <translation>Nom du nouveau répertoire d&apos;essai</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <source>Input the test name directory</source>
        <translation>Entrez le nom du répertoire d&apos;essai</translation>
    </message>
    <message>
        <source>Add &amp;test number</source>
        <translation>Ajou&amp;ter le numéro d&apos;essai</translation>
    </message>
    <message>
        <source>Choose another directory</source>
        <translation>Choisir un autre répertoire</translation>
    </message>
    <message>
        <source>Create a ne&amp;w test directory</source>
        <translation>Créer un nouvea&amp;u répertoire d&apos;essai</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <source>If you want to change the test directory, you must change its name !</source>
        <translation>Si vous voulez changer de répertoire d&apos;essai, vous devez changer son nom !</translation>
    </message>
    <message>
        <source>This directory already exists !
It may contain results of an other test.
Do you really want to use this directory ?</source>
        <translation>Ce répertoire existe déjà !
Il se peut qu&apos;il contienne des résultats d&apos;un autre essai.
Voulez-vous vraiment utiliser ce répertoire ?</translation>
    </message>
</context>
<context>
    <name>CYSMTP</name>
    <message>
        <source>Connected to %1</source>
        <translation>Connecté à %1</translation>
    </message>
    <message>
        <source>Simple SMTP client</source>
        <translation>Simple client SMTP</translation>
    </message>
    <message>
        <source>Message sent</source>
        <translation type="vanished">Message envoyé</translation>
    </message>
    <message>
        <source>Qt Mail Example</source>
        <translation type="vanished">Exemple Qt Mail</translation>
    </message>
    <message>
        <source>Unexpected reply from SMTP server:

</source>
        <translation>Réponse inattendue du serveur SMTP:

</translation>
    </message>
</context>
<context>
    <name>CYScope</name>
    <message>
        <source>This data can not be put into an oscilloscope because it is not buffered!</source>
        <translation>Ces données ne peuvent pas être placées dans un oscilloscope car elles ne sont pas mises en bufferisées !</translation>
    </message>
    <message>
        <source>On which axis do you want this data must to be ?</source>
        <translation>Sur quel axe voulez-vous que cette donnée soit ?</translation>
    </message>
    <message>
        <source>Axis selection</source>
        <translation>Sélection d&apos;axe</translation>
    </message>
    <message>
        <source>&amp;Y</source>
        <translation>&amp;Y</translation>
    </message>
    <message>
        <source>&amp;X</source>
        <translation>&amp;X</translation>
    </message>
    <message>
        <source>This display cannot treat this type of data !</source>
        <translation>Cet afficheur ne peut traiter ce type de donnée !</translation>
    </message>
    <message>
        <source>This data is already displayed in this oscilloscope.</source>
        <translation>Cette donnée est déjà affichée sur cet oscilloscope.</translation>
    </message>
    <message>
        <source>CYLIX: Scope Capture Window</source>
        <translation>CYLIX: Fenêtre de capture de l&apos;oscilloscope</translation>
    </message>
    <message>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Droit</translation>
    </message>
    <message>
        <source>Scope</source>
        <translation>Oscilloscope</translation>
    </message>
    <message>
        <source>&amp;Analyse curves</source>
        <translation>&amp;Analyse des courbes</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>RAZ</translation>
    </message>
    <message>
        <source>Pr&amp;int curves</source>
        <translation>&amp;Impression des courbes</translation>
    </message>
    <message>
        <source>Export PDF</source>
        <translation>Export PDF</translation>
    </message>
    <message>
        <source>Export CSV</source>
        <translation>Export CSV</translation>
    </message>
    <message>
        <source>Setup trigger</source>
        <translation>Configuration trigger</translation>
    </message>
    <message>
        <source>Reset trigger</source>
        <translation>Réarmement trigger</translation>
    </message>
    <message>
        <source>The previous export not finished!
Renew the export.</source>
        <translation>L&apos;exportation précédente n&apos;a pas fini!
Renouveler l&apos;exportation.</translation>
    </message>
</context>
<context>
    <name>CYScopeAcquisition</name>
    <message>
        <source>curves</source>
        <translation>courbes</translation>
    </message>
    <message>
        <source>Saving under</source>
        <translation>Enregistrement sous</translation>
    </message>
    <message>
        <source>Acquisition period</source>
        <translation>Période d&apos;acquisition</translation>
    </message>
    <message>
        <source>Date:</source>
        <translation>Date:</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>X axis</source>
        <translation>Axe X</translation>
    </message>
    <message>
        <source>X min</source>
        <translation>Min X</translation>
    </message>
    <message>
        <source>X max</source>
        <translation>Max X</translation>
    </message>
    <message>
        <source>X unit</source>
        <translation>Unité X</translation>
    </message>
    <message>
        <source>X decimal</source>
        <translation>Décimale X</translation>
    </message>
    <message>
        <source>Y axis</source>
        <translation>Axe Y</translation>
    </message>
    <message>
        <source>Y min</source>
        <translation>Min Y</translation>
    </message>
    <message>
        <source>Y max</source>
        <translation>Max Y</translation>
    </message>
    <message>
        <source>Y unit</source>
        <translation>Unité Y</translation>
    </message>
    <message>
        <source>Y decimal</source>
        <translation>Décimale Y</translation>
    </message>
    <message>
        <source>Y color</source>
        <translation>Couleur Y</translation>
    </message>
    <message>
        <source>Exported:</source>
        <translation type="vanished">Exporté:</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
</context>
<context>
    <name>CYScopeAnalyser</name>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation>&amp;Imprimer...</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <source>&amp;Export PDF</source>
        <translation>&amp;Export PDF</translation>
    </message>
    <message>
        <source>E&amp;xport CSV</source>
        <translation>E&amp;xporter CSV</translation>
    </message>
    <message>
        <source>Auto zoom Y</source>
        <translation>Zoom automatique Y</translation>
    </message>
    <message>
        <source>&amp;Zoom</source>
        <translation>&amp;Zoom</translation>
    </message>
    <message>
        <source>Zoom &amp;In</source>
        <translation>&amp;Agrandir</translation>
    </message>
    <message>
        <source>Zoom &amp;Out</source>
        <translation>&amp;Dézoomer</translation>
    </message>
    <message>
        <source>&amp;Permut cursors</source>
        <translation>&amp;Permuter curseurs</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Edition</translation>
    </message>
    <message>
        <source>Main Toolbar</source>
        <translation>Barre d&apos;outils principale</translation>
    </message>
</context>
<context>
    <name>CYScopeAxis</name>
    <message>
        <source>Left axis</source>
        <translation>Axe gauche</translation>
    </message>
    <message>
        <source>Right axis</source>
        <translation>Axe droit</translation>
    </message>
    <message>
        <source>Bottom axis</source>
        <translation>Axe bas</translation>
    </message>
    <message>
        <source>Top axis</source>
        <translation>Axe haut</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation>Valeur maximum</translation>
    </message>
    <message>
        <source>Minimum value</source>
        <translation>Valeur minimum</translation>
    </message>
    <message>
        <source>Auto scale</source>
        <translation>Échelle automatique</translation>
    </message>
    <message>
        <source>Scale settable</source>
        <translation>Échelle paramétrable</translation>
    </message>
    <message>
        <source>Left value</source>
        <translation>Valeur gauche</translation>
    </message>
    <message>
        <source>Right value</source>
        <translation>Valeur droite</translation>
    </message>
    <message>
        <source>Shown</source>
        <translation>Affiché</translation>
    </message>
    <message>
        <source>Show axis</source>
        <translation>Afficher l&apos;axe</translation>
    </message>
    <message>
        <source>Hide axis</source>
        <translation>Cacher l&apos;axe</translation>
    </message>
    <message>
        <source>Logarithmic scale</source>
        <translation>Echelle logarithmique</translation>
    </message>
    <message>
        <source>Enable logarithmic scale</source>
        <translation>Activation de l&apos;échelle logarithmique</translation>
    </message>
    <message>
        <source>Disable logarithmic scale</source>
        <translation>Désactivation de l&apos;échelle logarithmique</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>Actif</translation>
    </message>
    <message>
        <source>Axis enabled</source>
        <translation>Axe activé</translation>
    </message>
    <message>
        <source>Axis disabled</source>
        <translation>Axe désactivé</translation>
    </message>
    <message>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>heure</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <source>Hide the auto-scale option</source>
        <translation>Masquer l&apos;option d&apos;échelle automatique</translation>
    </message>
</context>
<context>
    <name>CYScopeAxisSetup</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
</context>
<context>
    <name>CYScopeCapture</name>
    <message>
        <source>Cursors</source>
        <translation>Curseurs</translation>
    </message>
</context>
<context>
    <name>CYScopeCaptureLegend</name>
    <message>
        <source>Cursors</source>
        <translation>Curseurs</translation>
    </message>
</context>
<context>
    <name>CYScopePlotter</name>
    <message>
        <source>If you click on the scale a dialog box to change it will be created.</source>
        <translation type="vanished">Si vous cliquez sur l&apos;échelle une boîte de dialogue pour la modifier sera alors créée.</translation>
    </message>
    <message>
        <source>&lt;p&gt;If you click on the scale, a dialog box will appear to change it.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si vous cliquez sur l&apos;échelle une boîte de dialogue s&apos;ouvrira pour la modifier.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>For a good display the maximum number of curves is %1!</source>
        <translation>Pour un bon affichage le nombre maximum de courbes est %1!</translation>
    </message>
    <message>
        <source>You must put data with the same acquisition time of the data(s) already displayed by this oscilloscope</source>
        <translation>Vous devez y mettre des données de même temps d&apos;acquisition que celle de la (des) donnée(s) déjà affichée(s) par cet oscilloscope</translation>
    </message>
    <message>
        <source>You must put data with the same buffer size of the data(s) already displayed by this oscilloscope</source>
        <translation>Vous devez y mettre des données de même taille mémoire que celle de la (des) donnée(s) déjà affichée(s) par cet oscilloscope</translation>
    </message>
    <message>
        <source>You must put data with the same burst buffer size of the data(s) already displayed by this oscilloscope</source>
        <translation>Vous devez y mettre des données de même taille mémoire en rafale que celle de la (des) donnée(s) déjà affichée(s) par cet oscilloscope</translation>
    </message>
    <message>
        <source>You must put data from the same connection of the data(s)already displayed by this oscilloscope</source>
        <translation>Vous devez y mettre des données de même connexion que celle de la (des) donnée(s) déjà affichée(s) par cet oscilloscope</translation>
    </message>
    <message>
        <source>You must put the same physical type of the data(s) already displayed by this oscilloscope</source>
        <translation>Vous devez mettre le même type physique que celui de la (des) donnée(s) déjà affichée(s) par cet oscilloscope</translation>
    </message>
    <message>
        <source>No title</source>
        <translation>Aucun titre</translation>
    </message>
    <message>
        <source>No legend</source>
        <translation>Aucune légende</translation>
    </message>
    <message>
        <source>Change label</source>
        <translation>Changer le libellé</translation>
    </message>
    <message>
        <source>Change pen</source>
        <translation>Changer le crayon</translation>
    </message>
    <message>
        <source>Change scale coefficient</source>
        <translation>Changer coefficient d&apos;échelle</translation>
    </message>
    <message>
        <source>If you double click on the scope a window to analyse curves will be created.</source>
        <translation>Si vous double cliquez sur l&apos;oscilloscope une fenêtre d&apos;analyse des courbes sera alors créée.</translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation>Impossible d&apos;ouvrir %1</translation>
    </message>
    <message>
        <source>Scope</source>
        <translation>Oscilloscope</translation>
    </message>
    <message>
        <source>Use mode</source>
        <translation>Mode d&apos;utilisation</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <source>XY</source>
        <translation>XY</translation>
    </message>
    <message>
        <source>Sampling mode</source>
        <translation>Mode d&apos;échantillonnage</translation>
    </message>
    <message>
        <source>Continuous</source>
        <translation>Continu</translation>
    </message>
    <message>
        <source>In bursts</source>
        <translation>Par rafales</translation>
    </message>
    <message>
        <source>Trigger</source>
        <translation>Trigger</translation>
    </message>
    <message>
        <source>Signal</source>
        <translation>Signal</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <source>Disable trigger</source>
        <translation>Désactive le trigger</translation>
    </message>
    <message>
        <source>Level</source>
        <translation>Seuil</translation>
    </message>
    <message>
        <source>Post-Trigger</source>
        <translation>Post-Trigger</translation>
    </message>
    <message>
        <source>Slope</source>
        <translation>Pente</translation>
    </message>
    <message>
        <source>Rising</source>
        <translation>Montant</translation>
    </message>
    <message>
        <source>Trigger over the level.</source>
        <translation>Déclenchement au dessus du seuil.</translation>
    </message>
    <message>
        <source>Falling</source>
        <translation>Descendant</translation>
    </message>
    <message>
        <source>Trigger below the level.</source>
        <translation>Déclenchement en dessous du seuil.</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>Curves style</source>
        <translation>Style des courbes</translation>
    </message>
    <message>
        <source>No Curve</source>
        <translation>Pas de courbe</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <source>Sticks</source>
        <translation>Bâtons</translation>
    </message>
    <message>
        <source>Steps</source>
        <translation>Échelons</translation>
    </message>
    <message>
        <source>Dots</source>
        <translation>Points</translation>
    </message>
    <message>
        <source>Curves width</source>
        <translation>Épaisseur des courbes</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Couleur de fond</translation>
    </message>
    <message>
        <source>Major gridlines</source>
        <translation>Grille principale</translation>
    </message>
    <message>
        <source>Minor gridlines</source>
        <translation>Grille secondaire</translation>
    </message>
    <message>
        <source>Major gridlines color</source>
        <translation>Couleur de la grille principale</translation>
    </message>
    <message>
        <source>Minor gridlines color</source>
        <translation>Couleur de la grille secondaire</translation>
    </message>
    <message>
        <source>Hide legend</source>
        <translation>Masquer la légende</translation>
    </message>
    <message>
        <source>Left legend</source>
        <translation>Légende à gauche</translation>
    </message>
    <message>
        <source>Right legend</source>
        <translation>Légende à droite</translation>
    </message>
    <message>
        <source>Bottom legend</source>
        <translation>Légende en bas</translation>
    </message>
    <message>
        <source>Top legend</source>
        <translation>Légende en haut</translation>
    </message>
    <message>
        <source>Legend position</source>
        <translation>Position légende</translation>
    </message>
    <message>
        <source>Legends</source>
        <translation type="vanished">Légendes</translation>
    </message>
    <message>
        <source>Acquisition</source>
        <translation>Acquisition</translation>
    </message>
    <message>
        <source>Enable acquisition</source>
        <translation>Active l&apos;acquisition</translation>
    </message>
    <message>
        <source>Data label</source>
        <translation>Libellé de donnée</translation>
    </message>
    <message>
        <source>Enter the new label: </source>
        <translation>Entrer le nouveau libellé: </translation>
    </message>
    <message>
        <source>Display coefficient</source>
        <translation>Coefficient d&apos;affichage</translation>
    </message>
    <message>
        <source>Enter the new display coefficient: </source>
        <translation>Entrer le nouveau coefficient d&apos;affichage: </translation>
    </message>
    <message>
        <source>You must enter a coefficient display different of 0 !</source>
        <translation>Vous devez entrer un coefficient d&apos;affichage différent de 0 !</translation>
    </message>
    <message>
        <source>No trigger signal!
Do you want to configure it?</source>
        <translation>Aucun signal sur le trigger!\nVoulez-vous le configurer?</translation>
    </message>
    <message>
        <source>Activation of the trigger</source>
        <translation>Activation du trigger</translation>
    </message>
    <message>
        <source>The source signal of the trigger was not found!</source>
        <translation>Le signal source du trigger n&apos;a pas été trouvé!</translation>
    </message>
</context>
<context>
    <name>CYScopePrinter</name>
    <message>
        <source>%1/curves</source>
        <translation>%1/courbes</translation>
    </message>
    <message>
        <source>%1/curve_%2.ps</source>
        <translation>%1/courbe_%2.ps</translation>
    </message>
    <message>
        <source>%1/curve</source>
        <translation>%1/courbe</translation>
    </message>
    <message>
        <source>Saving under</source>
        <translation>Enregistrement sous</translation>
    </message>
    <message>
        <source>%1/curve.pdf</source>
        <translation>%1/courbe.pdf</translation>
    </message>
    <message>
        <source>Can&apos;t print %1</source>
        <translation>Impossible d&apos;imprimer %1</translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation>Impossible d&apos;ouvrir %1</translation>
    </message>
    <message>
        <source>Page %1</source>
        <translation>Page %1</translation>
    </message>
</context>
<context>
    <name>CYScopeScaleDialog</name>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
</context>
<context>
    <name>CYScopeSetup</name>
    <message>
        <source>Oscilloscope Settings</source>
        <translation>Configuration de l&apos;oscilloscope</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <source>Grid</source>
        <translation>Grille</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <source>Curves</source>
        <translation>Courbes</translation>
    </message>
    <message>
        <source>Background</source>
        <translation>Fond</translation>
    </message>
    <message>
        <source>Data &amp;X</source>
        <translation>Donnée &amp;X</translation>
    </message>
    <message>
        <source>Datas &amp;Y</source>
        <translation>Données &amp;Y</translation>
    </message>
    <message>
        <source>Axis</source>
        <translation>Axe</translation>
    </message>
</context>
<context>
    <name>CYScopeTrigger</name>
    <message>
        <source>Trigger setting</source>
        <translation>Configuration trigger</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
</context>
<context>
    <name>CYScopeTriggerSetup</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
</context>
<context>
    <name>CYSettingsDialog</name>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation>Il y a des changements non enregistrés dans la vue active.
Voulez-vous appliquer ou abandonner ces modifications ?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Changements non enregistrés</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation>&amp;Abandonner</translation>
    </message>
</context>
<context>
    <name>CYSplashScreen</name>
    <message>
        <source>Starting CYLIX...</source>
        <translation>Démarrage de CYLIX en cours...</translation>
    </message>
    <message>
        <source>Stopping CYLIX...</source>
        <translation>Arrêt de CYLIX en cours...</translation>
    </message>
</context>
<context>
    <name>CYString</name>
    <message>
        <source>Designer value: %1.</source>
        <translation>Valeur constructeur: %1.</translation>
    </message>
    <message>
        <source>The maximum number of characters is %1.</source>
        <translation>Le nombre maximum de caractères est %1.</translation>
    </message>
    <message>
        <source>If the string is too long it will be truncated!</source>
        <translation>Si le texte est trop long il sera tronqué!</translation>
    </message>
</context>
<context>
    <name>CYTSec</name>
    <message>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
</context>
<context>
    <name>CYTabWidget</name>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation>Il y a des changements non enregistrés dans la vue active.
Voulez-vous appliquer ou abandonner ces modifications ?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Changements non enregistrés</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation>&amp;Abandonner</translation>
    </message>
</context>
<context>
    <name>CYTextDialog</name>
    <message>
        <source>Appl&amp;y</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYTextEdit</name>
    <message>
        <source>&amp;Scroll view</source>
        <translation>&amp;Défilement vue</translation>
    </message>
    <message>
        <source>Showing last line</source>
        <translation>Affichage dernière ligne</translation>
    </message>
    <message>
        <source>Vertical Auto</source>
        <translation>Vertical Auto</translation>
    </message>
    <message>
        <source>Vertical Off</source>
        <translation>Vertical Off</translation>
    </message>
    <message>
        <source>Vertical On</source>
        <translation>Vertical On</translation>
    </message>
    <message>
        <source>Horizontal Auto</source>
        <translation>Horizontal Auto</translation>
    </message>
    <message>
        <source>Horizontal Off</source>
        <translation>Horizontal Off</translation>
    </message>
    <message>
        <source>Horizontal On</source>
        <translation>Horizontal On</translation>
    </message>
    <message>
        <source>Maximum number of lines : %1 !</source>
        <translation>Nombre maxi de lignes : %1 !</translation>
    </message>
</context>
<context>
    <name>CYTim</name>
    <message>
        <source>Maintenance</source>
        <translation>Maintenance</translation>
    </message>
    <message>
        <source>Timer</source>
        <translation>Compteur horaire</translation>
    </message>
    <message>
        <source>Timer used for maintenance</source>
        <translation>Compteur horaire de maintenance</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>Partiel</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <source>Total</source>
        <translation>Total</translation>
    </message>
</context>
<context>
    <name>CYTime</name>
    <message>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>heure</translation>
    </message>
    <message>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <source>msec</source>
        <translation>msec</translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is higher than maximum value (%3&gt;%4) !</source>
        <translation>Valeur constructeur de %1:%2 est supérieure à la valeur maximale (%3&gt;%4) !</translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is smaller than minimum value (%3&lt;%4) !</source>
        <translation>Valeur constructeur de %1:%2 est inférieure à la valeur minimale (%3&lt;%4) !</translation>
    </message>
    <message>
        <source>%1h%2m</source>
        <translation>%1h%2m</translation>
    </message>
    <message>
        <source>%2 min</source>
        <translation>%2 min</translation>
    </message>
    <message>
        <source>%1m%2s</source>
        <translation>%1m%2s</translation>
    </message>
    <message>
        <source>%1 sec</source>
        <translation>%1 sec</translation>
    </message>
    <message>
        <source>%1s%2ms</source>
        <translation>%1s%2ms</translation>
    </message>
    <message>
        <source>%3 msec</source>
        <translation>%3 msec</translation>
    </message>
    <message>
        <source>%1h%2m%3s</source>
        <translation>%1h%2m%3s</translation>
    </message>
    <message>
        <source>%1h%2m%3s%4</source>
        <translation>%1h%2m%3s%4</translation>
    </message>
    <message>
        <source>%1m%2s%3</source>
        <translation>%1m%2s%3</translation>
    </message>
    <message>
        <source>%1s%2</source>
        <translation>%1s%2</translation>
    </message>
    <message>
        <source>%1 msec</source>
        <translation>%1 msec</translation>
    </message>
    <message>
        <source>%1h</source>
        <translation>%1h</translation>
    </message>
    <message>
        <source>%1m</source>
        <translation>%1m</translation>
    </message>
    <message>
        <source>%1s</source>
        <translation>%1s</translation>
    </message>
    <message>
        <source> hour</source>
        <translation> heure</translation>
    </message>
    <message>
        <source> min</source>
        <translation> min</translation>
    </message>
    <message>
        <source> sec</source>
        <translation> sec</translation>
    </message>
    <message>
        <source> msec</source>
        <translation> msec</translation>
    </message>
    <message>
        <source>h%1m</source>
        <translation>h%1m</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <source>m%1s</source>
        <translation>m%1s</translation>
    </message>
    <message>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <source>s%1ms</source>
        <translation>s%1ms</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>h%1m%2s</source>
        <translation>h%1m%2s</translation>
    </message>
    <message>
        <source>h%1m%2s%3ms</source>
        <translation>h%1m%2s%3ms</translation>
    </message>
    <message>
        <source>m%1s%2ms</source>
        <translation>m%1s%2ms</translation>
    </message>
    <message>
        <source>Value from %1 to %2.</source>
        <translation>Valeur de %1 à %2.</translation>
    </message>
    <message>
        <source>Precision is %1 %2.</source>
        <translation>Précision de %1 %2.</translation>
    </message>
    <message>
        <source>Designer value: %1.</source>
        <translation>Valeur constructeur: %1.</translation>
    </message>
    <message>
        <source>0ms</source>
        <translation>0ms</translation>
    </message>
    <message>
        <source>00ms</source>
        <translation>00ms</translation>
    </message>
</context>
<context>
    <name>CYUser</name>
    <message>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>LINUX user</source>
        <translation>Utilisateur LINUX</translation>
    </message>
    <message>
        <source>The LINUX user takes the user name of the current LINUX session.</source>
        <translation>L&apos;utilisateur de LINUX prend le nom d&apos;utilisateur de la session courante de LINUX</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Has a password</source>
        <translation>Possède un mot de passe</translation>
    </message>
    <message>
        <source>Password checking</source>
        <translation>Vérification de mot de passe</translation>
    </message>
    <message>
        <source>%1
Password of &apos;%2&apos;</source>
        <translation>%1
Mot de passe de &apos;%2&apos;</translation>
    </message>
    <message>
        <source>Password of &apos;%1&apos;</source>
        <translation>Mot de passe de &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <translation>Mauvais mot de passe</translation>
    </message>
    <message>
        <source>Access &apos;%1&apos; has no password.</source>
        <translation>L&apos;accès &apos;%1&apos; n&apos;a pas de mot de passe.</translation>
    </message>
    <message>
        <source>Access password</source>
        <translation>Mot de passe d&apos;accès</translation>
    </message>
    <message>
        <source>Access &apos;%1&apos; cannot change its password.</source>
        <translation>L&apos;accès &apos;%1&apos; ne peut pas changer son mot de passe.</translation>
    </message>
    <message>
        <source>New password for &apos;%1&apos;</source>
        <translation>Nouveau mot de passe &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Can&apos;t find group with index %1</source>
        <translation>Impossible de trouver le groupe avec l&apos;index %1</translation>
    </message>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Impossible d&apos;ouvrir le fichier %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Le fichier %1 ne contient pas de code XML valide
%2: ligne:%3 colonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation>Impossible d&apos;enregistrer le fichier %1!</translation>
    </message>
    <message>
        <source>Cannot get system user name</source>
        <translation>Impossible de récupérer le nom de l&apos;utilisateur système.</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Impossible d&apos;enregistrer le fichier %1.</translation>
    </message>
</context>
<context>
    <name>CYUserAction</name>
    <message>
        <source>User action</source>
        <translation>Action utilisateur</translation>
    </message>
</context>
<context>
    <name>CYUserAdminEdit</name>
    <message>
        <source>Users administration</source>
        <translation>Administration utilisateurs</translation>
    </message>
    <message>
        <source>Users</source>
        <translation>Utilisateurs</translation>
    </message>
    <message>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>&amp;Add</source>
        <translation>&amp;Ajouter</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>The LINUX user takes the user name of the current LINUX session.</source>
        <translation>L&apos;utilisateur de LINUX prend le nom d&apos;utilisateur de la session courante de LINUX</translation>
    </message>
    <message>
        <source>Add &amp;Linux user</source>
        <translation>Ajouter l&apos;utilisateur &amp;Linux</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>Groupes</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Administrator</source>
        <translation>Administrateur</translation>
    </message>
    <message>
        <source>Are you sure to remove the user %1 ?</source>
        <translation>Êtes-vous sûr de supprimer l&apos;utilisateur %1 ?</translation>
    </message>
    <message>
        <source>Are you sure to remove the users group %1 ?</source>
        <translation>Êtes-vous sûr de supprimer le groupe d&apos;utilisateurs %1 ?</translation>
    </message>
</context>
<context>
    <name>CYUserAdminEdit2</name>
    <message>
        <source>Users administration</source>
        <translation>Administration utilisateurs</translation>
    </message>
    <message>
        <source>Add &amp;user</source>
        <translation>Ajouter &amp;utilisateur</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <source>The LINUX user takes the user name of the current LINUX session.</source>
        <translation>L&apos;utilisateur de LINUX prend le nom d&apos;utilisateur de la session courante de LINUX</translation>
    </message>
    <message>
        <source>Add &amp;Linux user</source>
        <translation>Ajouter l&apos;utilisateur &amp;Linux</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>The WINDOWS user takes the user name of the current WINDOWS session.</source>
        <translation>L&apos;utilisateur de WINDOWS prend le nom d&apos;utilisateur de la session courante de WINDOWS.</translation>
    </message>
    <message>
        <source>Add &amp;Windows user</source>
        <translation>Ajouter l&apos;utilisateur &amp;Windows</translation>
    </message>
    <message>
        <source>Add &amp;group</source>
        <translation>Ajouter &amp;groupe</translation>
    </message>
    <message>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <source>Remo&amp;ve</source>
        <translation>Supprime&amp;r</translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Editer</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Utilisateur</translation>
    </message>
    <message>
        <source>Chefs d&apos;équipe</source>
        <translation>Chefs d&apos;équipe</translation>
    </message>
    <message>
        <source>Michel</source>
        <translation>Michel</translation>
    </message>
    <message>
        <source>Equipe de jour</source>
        <translation>Equipe de jour</translation>
    </message>
    <message>
        <source>Jean</source>
        <translation>Jean</translation>
    </message>
    <message>
        <source>Thomas</source>
        <translation>Thomas</translation>
    </message>
    <message>
        <source>Vincent</source>
        <translation>Vincent</translation>
    </message>
    <message>
        <source>Equipe de nuit</source>
        <translation>Equipe de nuit</translation>
    </message>
    <message>
        <source>David</source>
        <translation>David</translation>
    </message>
    <message>
        <source>Xavier</source>
        <translation>Xavier</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation>Maintenance</translation>
    </message>
    <message>
        <source>Julien</source>
        <translation>Julien</translation>
    </message>
    <message>
        <source>Robert</source>
        <translation>Robert</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation>O&amp;k</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Are you sure to remove the user %1 ?</source>
        <translation>Êtes-vous sûr de supprimer l&apos;utilisateur %1 ?</translation>
    </message>
    <message>
        <source>Are you sure to remove the users group %1 ?</source>
        <translation>Êtes-vous sûr de supprimer le groupe d&apos;utilisateurs %1 ?</translation>
    </message>
</context>
<context>
    <name>CYUserEdit</name>
    <message>
        <source>Edit user</source>
        <translation>Édition utilisateur</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>App&amp;ly</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
</context>
<context>
    <name>CYUserGroup</name>
    <message>
        <source>Users group</source>
        <translation>Groupe utilisateurs</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>CYLIX starting protected</source>
        <translation>Démarrage de CYLIX protégé</translation>
    </message>
    <message>
        <source>CYLIX can start automatically with the current user at the time of the preceding stop if the group of this one allows it.</source>
        <translation>CYLIX peut démarrer automatiquement avec l&apos;utilisateur courant lors du précédent arrêt si le groupe de celui-ci le permet.</translation>
    </message>
    <message>
        <source>CYLIX starts in protected access.</source>
        <translation>CYLIX démarre en accès protégé.</translation>
    </message>
    <message>
        <source>Screensaver protection</source>
        <translation>Protection de l&apos;économiseur d&apos;écran</translation>
    </message>
    <message>
        <source>Screensaver protection action</source>
        <translation>Action de protection de l&apos;économiseur d&apos;écran</translation>
    </message>
    <message>
        <source>Force CYLIX in protected access</source>
        <translation>Force CYLIX en accès protégé</translation>
    </message>
    <message>
        <source>Stop the LINUX session</source>
        <translation>Arrête la session LINUX</translation>
    </message>
    <message>
        <source>Protect access to desktop environment</source>
        <translation>Protéger l&apos;accès à l&apos;environnement de bureau</translation>
    </message>
    <message>
        <source>Restricts access to the desktop environment by disabling its panel.</source>
        <translation>Restreint l&apos;accès à l&apos;environnement de bureau en désactivant son panneau.</translation>
    </message>
    <message>
        <source>Configuration is not possible if one of the parents has enabled this protection.</source>
        <translation>La configuration n&apos;est pas possible si un des parents a activé cette protection.</translation>
    </message>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Impossible d&apos;ouvrir le fichier %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Le fichier %1 ne contient pas de code XML valide
%2: ligne:%3 colonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation>Impossible d&apos;enregistrer le fichier %1!</translation>
    </message>
    <message>
        <source>Unable to find the collection of action %1 in user group %2</source>
        <translation>Impossible de trouver la collection de l&apos;action %1 dans le groupe d&apos;utilisateurs %2</translation>
    </message>
    <message>
        <source>Unable to find action %1 in user group %2</source>
        <translation>Impossible de trouver l&apos;action %1 dans le groupe d&apos;utilisateurs %2</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">Impossible d&apos;enregistrer le fichier %1.</translation>
    </message>
    <message>
        <source>This does not work and will be corrected soon.</source>
        <translation>Cela ne fonctionne pas et sera corrigé bientôt.</translation>
    </message>
</context>
<context>
    <name>CYUserGroupEdit</name>
    <message>
        <source>Edit user group</source>
        <translation>Édition groupe utilisateurs</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Menus</source>
        <translation>Menus</translation>
    </message>
    <message>
        <source>Au&amp;thorized actions:</source>
        <translation>Actions au&amp;torisées:</translation>
    </message>
    <message>
        <source>Proh&amp;ibited actions:</source>
        <translation>Actions &amp;interdites:</translation>
    </message>
    <message>
        <source>Protection</source>
        <translation>Protection</translation>
    </message>
</context>
<context>
    <name>CYWin</name>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>Impossible d&apos;ouvrir le fichier %1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>Le fichier %1 ne contient pas de code XML valide
%2: ligne:%3 colonne:%4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>Le fichier %1 ne contient pas une définition valide, celle-ci devrait avoir un type de document </translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>&amp;About %1</source>
        <translation>À &amp;propos %1</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>À propos &amp;Qt</translation>
    </message>
    <message>
        <source>What&apos;s &amp;This</source>
        <translation type="vanished">&amp;Qu&apos;est-ce que c&apos;est</translation>
    </message>
    <message>
        <source>Qt Application</source>
        <translation>Application Qt</translation>
    </message>
</context>
<context>
    <name>CYWord</name>
    <message>
        <source>Word</source>
        <translation>Mot 16 bits</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <source>Operator&apos;s manual</source>
        <translation>Manuel opérateur</translation>
    </message>
    <message>
        <source>Cylix manual</source>
        <translation>Manuel de Cylix</translation>
    </message>
    <message>
        <source>Release notes</source>
        <translation>Notes de mise à jour</translation>
    </message>
    <message>
        <source>SUPERVISOR</source>
        <translation>SUPERVISEUR</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation>Superviseur</translation>
    </message>
    <message>
        <source>Machine</source>
        <translation>Machine</translation>
    </message>
    <message>
        <source>Regulator</source>
        <translation>Régulateur</translation>
    </message>
    <message>
        <source>Local datas</source>
        <translation>Données locales</translation>
    </message>
    <message>
        <source>Non-synchronous connection</source>
        <translation>Connexion assynchrone</translation>
    </message>
    <message>
        <source>Synchronous connection</source>
        <translation>Connexion synchrone</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <source>Trigonometry</source>
        <translation>Trigonométrie</translation>
    </message>
    <message>
        <source>Leak rate</source>
        <translation>Taux de fuite</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation>Pression</translation>
    </message>
    <message>
        <source>Percentage</source>
        <translation>Pourcentage</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation>Entrée analogique</translation>
    </message>
    <message>
        <source>Stations inlet pressure</source>
        <translation>Pression entrée postes</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation>Position vérin</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation>Température fluide d&apos;entrée postes</translation>
    </message>
    <message>
        <source>Stations outlet fluid temperature</source>
        <translation>Température fluide de sortie postes</translation>
    </message>
    <message>
        <source>Ramp 1 heater temperature</source>
        <translation>Température rampe de chauffe 1</translation>
    </message>
    <message>
        <source>Ramp 2 heater temperature</source>
        <translation>Température rampe de chauffe 2</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation>Pression sortie postes</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation>Pression éclatement</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation>Température air ambiant</translation>
    </message>
    <message>
        <source>Water inlet temperature</source>
        <translation>Température entrée eau</translation>
    </message>
    <message>
        <source>Water outlet temperature</source>
        <translation>Température sortie eau</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation>Sortie analogique</translation>
    </message>
    <message>
        <source>Frequency converter VA1 (circulation pump)</source>
        <translation>Variateur VA1 (pompe de circulation)</translation>
    </message>
    <message>
        <source>Servo cylinder control</source>
        <translation>Commande servo-vérin</translation>
    </message>
    <message>
        <source>Slow continuous acquisition</source>
        <translation>Acquisition continue lente</translation>
    </message>
    <message>
        <source>Fast continuous acquisition</source>
        <translation>Acquisition continue rapide</translation>
    </message>
    <message>
        <source>Slow acquisition on event</source>
        <translation>Acquisition lente sur événement</translation>
    </message>
    <message>
        <source>Fast acquisition on event</source>
        <translation>Acquisition rapide sur événement</translation>
    </message>
</context>
<context>
    <name>CylibsTest</name>
    <message>
        <source>textLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>Analys&amp;e</source>
        <translation>Analys&amp;e</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Page 1</source>
        <translation>Page 1</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>Appliq&amp;uer</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>New Item</source>
        <translation>Nouvel élément</translation>
    </message>
    <message>
        <source>New Item 2</source>
        <translation>New Item 2</translation>
    </message>
    <message>
        <source>a </source>
        <translation>a </translation>
    </message>
    <message>
        <source> v</source>
        <translation> v</translation>
    </message>
    <message>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <source>textLabel1</source>
        <translation>textLabel1</translation>
    </message>
    <message>
        <source>Page 2</source>
        <translation>Page 2</translation>
    </message>
    <message>
        <source>YX</source>
        <translation>YX</translation>
    </message>
    <message>
        <source>Page</source>
        <translation>Page</translation>
    </message>
    <message>
        <source>00</source>
        <translation>00</translation>
    </message>
    <message>
        <source>titre</source>
        <translation>titre</translation>
    </message>
    <message>
        <source>pushButton&amp;4</source>
        <translation>pushButton&amp;4</translation>
    </message>
    <message>
        <source>pushBu&amp;tton6</source>
        <translation>pushBu&amp;tton6</translation>
    </message>
    <message>
        <source>pushButton&amp;5</source>
        <translation>pushButton&amp;5</translation>
    </message>
    <message>
        <source>pushButton&amp;3</source>
        <translation>pushButton&amp;3</translation>
    </message>
    <message>
        <source>1hh</source>
        <translation>1hh</translation>
    </message>
    <message>
        <source>&amp;0</source>
        <translation>&amp;0</translation>
    </message>
    <message>
        <source>AnalyseSheet</source>
        <translation>Feuille d&apos;analyse</translation>
    </message>
    <message>
        <source>Loa&amp;d</source>
        <translation>Char&amp;ger</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Sa&amp;ve</source>
        <translation>Enre&amp;gister</translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <source>Au&amp;toCali</source>
        <translation>Au&amp;toCali</translation>
    </message>
    <message>
        <source>Man&amp;uCali</source>
        <translation>Man&amp;uCali</translation>
    </message>
    <message>
        <source>Scope</source>
        <translation>Oscilloscope</translation>
    </message>
    <message>
        <source>CYEditText</source>
        <translation>CYEditText</translation>
    </message>
    <message>
        <source>SUPERVISOR</source>
        <translation type="vanished">SUPERVISEUR</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation type="vanished">Superviseur</translation>
    </message>
    <message>
        <source>Machine</source>
        <translation type="vanished">Machine</translation>
    </message>
    <message>
        <source>Regulator</source>
        <translation type="vanished">Régulateur</translation>
    </message>
    <message>
        <source>Local datas</source>
        <translation type="vanished">Données locales</translation>
    </message>
    <message>
        <source>Non-synchronous connection</source>
        <translation type="vanished">Connexion assynchrone</translation>
    </message>
    <message>
        <source>Synchronous connection</source>
        <translation type="vanished">Connexion synchrone</translation>
    </message>
    <message>
        <source>Simulation</source>
        <translation type="vanished">Simulation</translation>
    </message>
    <message>
        <source>Designer</source>
        <translation type="vanished">Constructeur</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">Type</translation>
    </message>
    <message>
        <source>Digital inputs</source>
        <translation>Entrées TOR</translation>
    </message>
    <message>
        <source>Analog inputs</source>
        <translation>Entrées analogiques</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="vanished">Température</translation>
    </message>
    <message>
        <source>Trigonometry</source>
        <translation type="vanished">Trigonométrie</translation>
    </message>
    <message>
        <source>Leak rate</source>
        <translation type="vanished">Taux de fuite</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="vanished">Pression</translation>
    </message>
    <message>
        <source>Percentage</source>
        <translation type="vanished">Pourcentage</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="vanished">Entrée analogique</translation>
    </message>
    <message>
        <source>Stations inlet pressure</source>
        <translation type="vanished">Pression entrée postes</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="vanished">Position vérin</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation type="vanished">Température fluide d&apos;entrée postes</translation>
    </message>
    <message>
        <source>Stations outlet fluid temperature</source>
        <translation type="vanished">Température fluide de sortie postes</translation>
    </message>
    <message>
        <source>Ramp 1 heater temperature</source>
        <translation type="vanished">Température rampe de chauffe 1</translation>
    </message>
    <message>
        <source>Ramp 2 heater temperature</source>
        <translation type="vanished">Température rampe de chauffe 2</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation type="vanished">Pression sortie postes</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="vanished">Pression éclatement</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="vanished">Température air ambiant</translation>
    </message>
    <message>
        <source>Water inlet temperature</source>
        <translation type="vanished">Température entrée eau</translation>
    </message>
    <message>
        <source>Water outlet temperature</source>
        <translation type="vanished">Température sortie eau</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation type="vanished">Sortie analogique</translation>
    </message>
    <message>
        <source>Frequency converter VA1 (circulation pump)</source>
        <translation type="vanished">Variateur VA1 (pompe de circulation)</translation>
    </message>
    <message>
        <source>Servo cylinder control</source>
        <translation type="vanished">Commande servo-vérin</translation>
    </message>
    <message>
        <source>Graphic analyse window</source>
        <translation>Fenêtre d&apos;analyse graphique</translation>
    </message>
    <message>
        <source>C&amp;ylix</source>
        <translation>C&amp;ylix</translation>
    </message>
    <message>
        <source>&amp;Protected access</source>
        <translation>Accès &amp;protégé</translation>
    </message>
    <message>
        <source>&amp;Change access</source>
        <translation>&amp;Changement d&apos;accès</translation>
    </message>
    <message>
        <source>Change &amp;password</source>
        <translation>Changement de mot de &amp;passe</translation>
    </message>
    <message>
        <source>Machine status backup</source>
        <translation>Sauvegarde état machine</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation>O&amp;utils</translation>
    </message>
    <message>
        <source>&amp;Graphic analyse</source>
        <translation>Analyse &amp;graphique</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>Confi&amp;guration</translation>
    </message>
    <message>
        <source>&amp;User administration</source>
        <translation>Administration &amp;utilisateur</translation>
    </message>
</context>
<context>
    <name>Page</name>
    <message>
        <source>Dynamic page example</source>
        <translation>Exemple de page dynamique</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>File %1 at line %2</source>
        <translation>Fichier source %1 à la ligne %2 !</translation>
    </message>
    <message>
        <source>File %1 at line %2: </source>
        <translation>Fichier %1 à la ligne %2: </translation>
    </message>
    <message>
        <source>File %1 at line %2 !</source>
        <translation>Fichier %1 à la ligne %2 ! </translation>
    </message>
    <message>
        <source>CYLIX DEBUG</source>
        <translation>DEBUG CYLIX</translation>
    </message>
    <message>
        <source>File %1 at line %2 on object %3 !</source>
        <translation>Fichier %1 à la ligne %2 sur l&apos;objet %3 !</translation>
    </message>
    <message>
        <source>CYLIX OBJECT DEBUG</source>
        <translation>DEBUG OBJET CYLIX</translation>
    </message>
    <message>
        <source>File %1 at line %2 for data %3 !</source>
        <translation>Fichier %1 à la ligne %2 pour la donnée %3 !</translation>
    </message>
    <message>
        <source>CYLIX DATA DEBUG</source>
        <translation>DEBUG DONNEE CYLIX</translation>
    </message>
    <message>
        <source>File %1 at line %2 on object %3 for data %4 !</source>
        <translation>Fichier %1 à la ligne %2 sur l&apos;objet %3 pour la donnée %4 !</translation>
    </message>
    <message>
        <source>CYLIX OBJECT DATA DEBUG</source>
        <translation>DEBUG OBJET DONNEE CYLIX</translation>
    </message>
    <message>
        <source>CYLIX ERROR</source>
        <translation>ERREUR CYLIX</translation>
    </message>
    <message>
        <source>CYLIX OBJECT ERROR</source>
        <translation>ERREUR OBJET CYLIX</translation>
    </message>
    <message>
        <source>CYLIX DATA ERROR</source>
        <translation>ERREUR DONNEE CYLIX</translation>
    </message>
    <message>
        <source>CYLIX OBJECT DATA ERROR</source>
        <translation>ERREUR OBJET DONNEE CYLIX</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 !</source>
        <translation>Erreur dans le fichier source %1 à la ligne %2 !</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 on object %3 !</source>
        <translation>Erreur dans le fichier source %1 à la ligne %2 sur l&apos;objet %3 !</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 for data %3 !</source>
        <translation>Erreur dans le fichier source %1 à la ligne %2 pour la donnée %3 !</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 on object %3 for data %4 !</source>
        <translation>Erreur dans le fichier source %1 à la ligne %2 sur l&apos;objet %3 pour la donnée %4 !</translation>
    </message>
    <message>
        <source>replace this with information about your translation team</source>
        <comment>&lt;p&gt;KDE is translated into many languages thanks to the work of the translation teams all over the world.&lt;/p&gt;&lt;p&gt;For more information on KDE internationalization visit &lt;a href=&quot;http://l10n.kde.org&quot;&gt;http://l10n.kde.org&lt;/a&gt;&lt;/p&gt;</comment>
        <translation>Remplacer ceci par des informations sur votre équipe de traduction</translation>
    </message>
    <message>
        <source>No licensing terms for this program have been specified.
Please check the documentation or the source for any
licensing terms.
</source>
        <translation>Aucune condition de licence pour ce programme n&apos;a été spécifiée.
Veuillez consulter la documentation ou la source pour
les conditions de licence.</translation>
    </message>
    <message>
        <source>This program is distributed under the terms of the %1.</source>
        <translation>Ce programme est distribué sous les termes du %1.</translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is higher than maximum value (%3&gt;%4) !</source>
        <translation>Valeur constructeur de %1:%2 est supérieure à la valeur maximale (%3&gt;%4) !</translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is smaller than minimum value (%3&lt;%4) !</source>
        <translation>Valeur constructeur de %1:%2 est inférieure à la valeur minimale (%3&lt;%4) !</translation>
    </message>
    <message>
        <source>Choice:</source>
        <translation>Choix:</translation>
    </message>
    <message>
        <source>Value from %1 to %2 %3.</source>
        <translation>Valeur de %1 à %2 %3.</translation>
    </message>
    <message>
        <source>Precision is %1 %2.</source>
        <translation>Précision de %1 %2.</translation>
    </message>
    <message>
        <source>Designer value:</source>
        <translation>Valeur constructeur:</translation>
    </message>
    <message>
        <source>Designer value: %1 %2.</source>
        <translation>Valeur constructeur: %1 %2.</translation>
    </message>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation>Il y a des changements non enregistrés dans la vue active.
Voulez-vous appliquer ou abandonner ces modifications ?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>Changements non enregistrés</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>&amp;Appliquer</translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation>&amp;Abandonner</translation>
    </message>
    <message>
        <source>%1 at line %2 : error in sheet %3 for one or multiple datas.</source>
        <translation>%1 à la ligne %2 : erreur dans la feuille %3 pour une ou plusieurs données.</translation>
    </message>
    <message>
        <source>%1 !
</source>
        <translation>%1 !
</translation>
    </message>
    <message>
        <source>CYLIX Object DEBUG</source>
        <translation type="vanished">DEBUG Objet CYLIX</translation>
    </message>
</context>
<context>
    <name>QwtPlotRenderer</name>
    <message>
        <source>Documents</source>
        <translation>Documents</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Images</translation>
    </message>
    <message>
        <source>Export File Name</source>
        <translation>Nom du fichier d&apos;exportation</translation>
    </message>
</context>
<context>
    <name>TargetWidget</name>
    <message>
        <source>Please specify the folder where Cylix will be installed.</source>
        <translation>Veuillez spécifier le dossier où Cylix sera installé.</translation>
    </message>
</context>
</TS>
