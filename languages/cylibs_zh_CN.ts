<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>About</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
    <message>
        <source>(</source>
        <translation type="obsolete">(</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="obsolete">&lt;</translation>
    </message>
    <message>
        <source>)</source>
        <translation type="obsolete">)</translation>
    </message>
</context>
<context>
    <name>AcquisitionSettings</name>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;设计者评价</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">类型</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>0</source>
        <translation type="obsolete">0</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation type="obsolete">注意：这些设置属于当前项目</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation type="obsolete">活动视图中存在未保存的更改。
是否应用或放弃此更改?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation type="obsolete">未保存更改</translation>
    </message>
</context>
<context>
    <name>BenchEmptying</name>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
</context>
<context>
    <name>BenchFilling</name>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
</context>
<context>
    <name>BenchSettings</name>
    <message>
        <source>Type</source>
        <translation type="obsolete">类型</translation>
    </message>
    <message>
        <source>0</source>
        <translation type="obsolete">0</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>11</source>
        <translation type="obsolete">11</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>13</source>
        <translation type="obsolete">13</translation>
    </message>
    <message>
        <source>20</source>
        <translation type="obsolete">20</translation>
    </message>
    <message>
        <source>21</source>
        <translation type="obsolete">21</translation>
    </message>
    <message>
        <source>22</source>
        <translation type="obsolete">22</translation>
    </message>
    <message>
        <source>25</source>
        <translation type="obsolete">25</translation>
    </message>
    <message>
        <source>26</source>
        <translation type="obsolete">26</translation>
    </message>
    <message>
        <source>27</source>
        <translation type="obsolete">27</translation>
    </message>
    <message>
        <source>28</source>
        <translation type="obsolete">28</translation>
    </message>
    <message>
        <source>32</source>
        <translation type="obsolete">32</translation>
    </message>
    <message>
        <source>33</source>
        <translation type="obsolete">33</translation>
    </message>
    <message>
        <source>34</source>
        <translation type="obsolete">34</translation>
    </message>
    <message>
        <source>37</source>
        <translation type="obsolete">37</translation>
    </message>
    <message>
        <source>38</source>
        <translation type="obsolete">38</translation>
    </message>
    <message>
        <source>39</source>
        <translation type="obsolete">39</translation>
    </message>
    <message>
        <source>42</source>
        <translation type="obsolete">42</translation>
    </message>
    <message>
        <source>43</source>
        <translation type="obsolete">43</translation>
    </message>
    <message>
        <source>44</source>
        <translation type="obsolete">44</translation>
    </message>
    <message>
        <source>47</source>
        <translation type="obsolete">47</translation>
    </message>
    <message>
        <source>48</source>
        <translation type="obsolete">48</translation>
    </message>
    <message>
        <source>46</source>
        <translation type="obsolete">46</translation>
    </message>
    <message>
        <source>45</source>
        <translation type="obsolete">45</translation>
    </message>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;设计者评价</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">环境温度</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation type="obsolete">活动视图中存在未保存的更改。
是否应用或放弃此更改?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation type="obsolete">未保存更改</translation>
    </message>
</context>
<context>
    <name>BurstingEdit</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation type="obsolete">故障</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">警报</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="obsolete">温度</translation>
    </message>
    <message>
        <source>Control</source>
        <translation type="obsolete">控制</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="obsolete">压力</translation>
    </message>
</context>
<context>
    <name>BurstingTestMonitoring</name>
    <message>
        <source>Process</source>
        <translation type="obsolete">程序</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>BurstingTestParts</name>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
</context>
<context>
    <name>CYAI</name>
    <message>
        <source>Analog input</source>
        <translation>模拟输入</translation>
    </message>
</context>
<context>
    <name>CYAO</name>
    <message>
        <source>Analog output</source>
        <translation>模拟输出</translation>
    </message>
</context>
<context>
    <name>CYAbout</name>
    <message>
        <source>=</source>
        <translation type="vanished">=</translation>
    </message>
    <message>
        <source>(</source>
        <translation>(</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <source>)</source>
        <translation>)</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source> )</source>
        <translation> )</translation>
    </message>
    <message>
        <source>( </source>
        <translation>( </translation>
    </message>
    <message>
        <source>Up&amp;date</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Select archive to send to the regulator.</source>
        <translation>选择存档发给调整器</translation>
    </message>
    <message>
        <source>Real Time information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAboutApplication</name>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A&amp;uthors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Droid Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;License Agreement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please report bugs to: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisition</name>
    <message>
        <source>Post-mortem mode</source>
        <translation type="vanished">事后模式</translation>
    </message>
    <message>
        <source>Acquisition started</source>
        <translation>采集开始</translation>
    </message>
    <message>
        <source>Acquisition pausing</source>
        <translation>采集暂停</translation>
    </message>
    <message>
        <source>Acquisition window in progress</source>
        <translation>采集窗口进行中</translation>
    </message>
    <message>
        <source>Acquisition saving</source>
        <translation>采集保存</translation>
    </message>
    <message>
        <source>Save acquisition file timer</source>
        <translation>保存采集文件定时器</translation>
    </message>
    <message>
        <source>Post-mortem periodical backup timer</source>
        <translation>事后分析定期备份定时器</translation>
    </message>
    <message>
        <source>Periodical backup</source>
        <translation>定期备份</translation>
    </message>
    <message>
        <source>Acquisition files directory</source>
        <translation>采集文件目录</translation>
    </message>
    <message>
        <source>Use test directory</source>
        <translation>使用测试目录</translation>
    </message>
    <message>
        <source>Directory path</source>
        <translation>目录路径</translation>
    </message>
    <message>
        <source>This path is used to save acquisition files somewhere else than in the test directory.</source>
        <translation>该路径用于保存采集文件到测试目录以外的地方</translation>
    </message>
    <message>
        <source>acquisition</source>
        <translation type="vanished">采集</translation>
    </message>
    <message>
        <source>Acquisition files prefix</source>
        <translation>采集文件前缀</translation>
    </message>
    <message>
        <source>Acquisition files extension</source>
        <translation>采集文件扩展名</translation>
    </message>
    <message>
        <source>Maximum of lines per acquisition file</source>
        <translation>每个采集文件最多行数</translation>
    </message>
    <message>
        <source>Maximum number of acquisition files</source>
        <translation type="vanished">采集文件最大数目</translation>
    </message>
    <message>
        <source>The acquisition saving will restart from the first file if this acquisition file number is reached.</source>
        <translation>达到该最大数目后采集保存将自第一个文件重新开始</translation>
    </message>
    <message>
        <source>Choose sensibly this number if you don&apos;t want to overwrite the oldest acquisition files.</source>
        <translation>谨慎选择该数目如果您不想覆盖最老采集数据</translation>
    </message>
    <message>
        <source>Number of acquisition files</source>
        <translation>采集文件数目</translation>
    </message>
    <message>
        <source>Acquisition time to pretend</source>
        <translation>模拟采集时间</translation>
    </message>
    <message>
        <source>Maximum file size</source>
        <translation>最大文件规格</translation>
    </message>
    <message>
        <source>Maximum size acquisition</source>
        <translation>最大采集规格</translation>
    </message>
    <message>
        <source>Start acquisition date time</source>
        <translation>开始采集日期时间</translation>
    </message>
    <message>
        <source>Acquisition file column separator</source>
        <translation>采集文件行分隔符</translation>
    </message>
    <message>
        <source>Other acquisition file column separator</source>
        <translation>其他采集文件行分隔符</translation>
    </message>
    <message>
        <source>Acquisition backup</source>
        <translation>采集备份</translation>
    </message>
    <message>
        <source>Backup file</source>
        <translation>备份文件</translation>
    </message>
    <message>
        <source>Acquisition since:</source>
        <translation>采集开始自：</translation>
    </message>
    <message>
        <source>TIME (ms)%1</source>
        <translation type="vanished">时间 (ms)%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML !
%2: %3 %4</source>
        <translation>文件 %1 不含有效 XML !
%2: %3 %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a valid definition, which must have a document type</source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">不能保存文件%1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">不能保存文件%1</translation>
    </message>
    <message>
        <source>bytes</source>
        <translation>字节</translation>
    </message>
    <message>
        <source>Kb</source>
        <translation>Kb</translation>
    </message>
    <message>
        <source>Mb</source>
        <translation>Mb</translation>
    </message>
    <message>
        <source>Gb</source>
        <translation>Gb</translation>
    </message>
    <message>
        <source>TIME</source>
        <translation>时间</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="obsolete">格式</translation>
    </message>
    <message>
        <source>TIME (ms)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timestamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>us</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SoliaGraph compatibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Continuous backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Storage size is limited by the maximum number of acquisition files configurable in the  &apos;Files&apos; tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Post-mortem continuous acquisition archive error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Continuous acquisition archive file by post-mortem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generates a time-stamped acquisition file at the end of each acquisition time in sub-directories following the &apos;year/month/day/hour/minute&apos; tree structure.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SoliaGraph allows multiple selection of files or sub-directories to easily perform a temporal analysis of the acquisition by scrolling through the resulting curves.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please note that this mode of operation can consume a lot of storage space, and should therefore be used sparingly. You can simulate the space required in the &apos;Files&apos; tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generates an acquisition files based only on trigger events configured in the &apos;Events&apos; tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>In addition to saving post-mortem recordings by configurable events, you can also save all post-mortem recordings to multiple files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To simulate only the memory space required for continuous storage with simulation time, you can set this value to 0.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can also enter 0 to disable backup on event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The integration of SoliaGraph in Cylix provides direct access and this generation of compatible CSV files. If you are interested in this feature, please contact us to available it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Makes Cylix CSV acquisition files compatibled with SoliaGraph Web software for easy visualization them in the form of time curves. For this, generate files has a defined format and a timestamp (in µs).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preserves import compatibility with existing post-processing files (e.g. .ods or .xls spreadsheets) by using the old Cylix acquisition file format. These include a time counter (in ms), initialized to 0 at the start of acquisition.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The maximum is set at 1,048,576 for use with acquisition files from Excel 2007 and LibreOffice Calc 4.2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Acquisition</source>
        <translation type="unfinished">采集</translation>
    </message>
    <message>
        <source>In continuous storage, this maximum is not used, as the number of files per hour is naturally low.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum of acquisition files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum of acquisition files on events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>CSV file size optimization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Optimizes CSV file size by using empty fields for unchanged values.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>With this optimization, storage space simulation can give a much higher value than reality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compatible with Solia Graph.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thanks to this function, you can, without greatly increasing the size of CSV files, add to the acquisition of data with highly variable values other, much less variable values, such as states.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionBackupFlag</name>
    <message>
        <source>Prefix used in the backup file name</source>
        <translation>备份文件已用前缀</translation>
    </message>
    <message>
        <source>Header used in the backup file</source>
        <translation>备份文件已用页头</translation>
    </message>
    <message>
        <source>Enable backup</source>
        <translation>启用备份</translation>
    </message>
    <message>
        <source>No backup</source>
        <translation>无备份</translation>
    </message>
    <message>
        <source>Rising</source>
        <translation>上升</translation>
    </message>
    <message>
        <source>Backup on the rising edge of event.</source>
        <translation>项目上升沿备份</translation>
    </message>
    <message>
        <source>Falling</source>
        <translation>下降</translation>
    </message>
    <message>
        <source>Backup on the falling edge of event.</source>
        <translation>项目下降沿备份</translation>
    </message>
    <message>
        <source>Changing</source>
        <translation>改变</translation>
    </message>
    <message>
        <source>Backup on the rising and falling edges of event.</source>
        <translation>项目上升沿和下降沿备份</translation>
    </message>
    <message>
        <source>Recording</source>
        <translation>记录</translation>
    </message>
    <message>
        <source>Overwrite</source>
        <translation>覆盖</translation>
    </message>
    <message>
        <source>Increment</source>
        <translation>增加</translation>
    </message>
    <message>
        <source>If the prefix doesn&apos;t change the new archive overwrites the last one.</source>
        <translation>如不改变前缀，新存档覆盖老存档</translation>
    </message>
    <message>
        <source>The number of backup is appended to the name of backup file. Thus, old backup files are note removed.</source>
        <translation>备份编号附加在备份文件上。因此，老备份文件不被移除</translation>
    </message>
    <message>
        <source>Backup delay on event</source>
        <translation>项目延迟备份</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionDataView</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>笔</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>单元</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>CLEMESSY 名称</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>系数</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Data acquisition label</source>
        <translation>采集数据标签</translation>
    </message>
    <message>
        <source>Enter the data acquisition label: </source>
        <translation>输入采集数据标签：</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionFast</name>
    <message>
        <source>Acquisition period</source>
        <translation>采集周期</translation>
    </message>
    <message>
        <source>Acquisition length time</source>
        <translation type="vanished">采集时间长度</translation>
    </message>
    <message>
        <source>Acquisition window time</source>
        <translation>采集窗口时间</translation>
    </message>
    <message>
        <source>No acquisition window time</source>
        <translation>无采集窗口时间</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>Bursts acquisition period</source>
        <translation>爆破采集阶段</translation>
    </message>
    <message>
        <source>Acquisition length time of a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This time combined with the acquisition period automatically generates a number of lines per acquisition file. To reduce the time required to import multiple acquisition files into SoliaGraph, it is advisable to limit the number of small files by maximizing this time.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionSetup</name>
    <message>
        <source>&amp;General</source>
        <translation>&amp;常规</translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation>参数</translation>
    </message>
    <message>
        <source>Acquisition window</source>
        <translation type="vanished">采集窗口</translation>
    </message>
    <message>
        <source>Datas</source>
        <translation>数据</translation>
    </message>
    <message>
        <source>&amp;Header</source>
        <translation>&amp;页眉</translation>
    </message>
    <message>
        <source>Fo&amp;rm</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Separation options</source>
        <translation>分割选项</translation>
    </message>
    <message>
        <source>&amp;Tabulation</source>
        <translation>&amp;制表</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>Sem&amp;i-colon</source>
        <translation>分号</translation>
    </message>
    <message>
        <source>C&amp;omma</source>
        <translation>逗号</translation>
    </message>
    <message>
        <source>Sp&amp;ace</source>
        <translation>空格</translation>
    </message>
    <message>
        <source>Ot&amp;her</source>
        <translation>其他</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Fi&amp;les</source>
        <translation>文件</translation>
    </message>
    <message>
        <source>Memory space</source>
        <translation type="vanished">内存空间</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="vanished">=</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation>保存中</translation>
    </message>
    <message>
        <source>E&amp;vents</source>
        <translation>项目</translation>
    </message>
    <message>
        <source>Event</source>
        <translation>项目</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <source>Prefix</source>
        <translation>前缀</translation>
    </message>
    <message>
        <source>Recording</source>
        <translation>记录</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <source>&amp;States</source>
        <translation>&amp;状态</translation>
    </message>
    <message>
        <source>Applying this change will restart this acquisition with a new time reference and a new file will be created !</source>
        <translation>应用该更改会重启采集，新的时间和新的文件将被创建！</translation>
    </message>
    <message>
        <source>Acquisition setup applying</source>
        <translation>采集步骤应用</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The number of lines calculated %1 is higher than maximum allowed (%2)!
Please reduce the acquisition period or its length time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Storage space simulation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAcquisitionSlow</name>
    <message>
        <source>Acquisition period</source>
        <translation>采集周期</translation>
    </message>
    <message>
        <source>Acquisition length time</source>
        <translation>采集时间长度</translation>
    </message>
    <message>
        <source>Acquisition window time</source>
        <translation>采集窗口时间</translation>
    </message>
    <message>
        <source>No acquisition window time</source>
        <translation>无采集窗口时间</translation>
    </message>
</context>
<context>
    <name>CYAcquisitionWin</name>
    <message>
        <source>CYLIX: Acquisitions tool</source>
        <translation>CYLIX: 采集工具</translation>
    </message>
</context>
<context>
    <name>CYAction</name>
    <message>
        <source>Users administration</source>
        <translation type="unfinished">用户管理</translation>
    </message>
    <message>
        <source>Function usage flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The current user is authorized to access this function.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The current user is not authorized to access this function or Cylix is in protected access.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYActionCollection</name>
    <message>
        <source>Cannot add action %1 in menu of window %2!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot add action %1 in tool bar %2 of window %3!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAdc</name>
    <message>
        <source>ADC</source>
        <translation type="vanished">ADC</translation>
    </message>
</context>
<context>
    <name>CYAdc16</name>
    <message>
        <source>ADC</source>
        <translation type="obsolete">ADC</translation>
    </message>
</context>
<context>
    <name>CYAdc32</name>
    <message>
        <source>ADC</source>
        <translation type="obsolete">ADC</translation>
    </message>
</context>
<context>
    <name>CYAna</name>
    <message>
        <source>Metrology</source>
        <translation>度量</translation>
    </message>
    <message>
        <source>Number of points used for calibration</source>
        <translation>校准使用点数</translation>
    </message>
    <message>
        <source>Bench sensor in ADC</source>
        <translation>ADC内试验台传感器</translation>
    </message>
    <message>
        <source>New point</source>
        <translation>新 点</translation>
    </message>
    <message>
        <source>Bench sensor</source>
        <translation>试验台传感器</translation>
    </message>
    <message>
        <source>Reference sensor</source>
        <translation>参考传感器</translation>
    </message>
    <message>
        <source>ADC bench values</source>
        <translation>ADC试验台数值</translation>
    </message>
    <message>
        <source>Point %1</source>
        <translation>点%1</translation>
    </message>
    <message>
        <source>Bench values</source>
        <translation>试验台数值</translation>
    </message>
    <message>
        <source>Reference values</source>
        <translation>参考数值</translation>
    </message>
    <message>
        <source>Averaging times</source>
        <translation>平均时间</translation>
    </message>
    <message>
        <source>Analogic</source>
        <translation>模拟的</translation>
    </message>
    <message>
        <source>High value</source>
        <translation type="vanished">高值</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation type="vanished">低值</translation>
    </message>
    <message>
        <source>Gain offset of %1</source>
        <translation>增益补偿%1</translation>
    </message>
    <message>
        <source>Sensor defect</source>
        <translation>传感器损坏</translation>
    </message>
    <message>
        <source>Ana. Sensor</source>
        <translation>模拟传感器</translation>
    </message>
    <message>
        <source>Defect of the sensor or the conditioner (Ex: signal lower than 4mA or higher than 20mA).  This may indicate a failure or a measure out of scale. Make sure of the quality of connections and the good performance of the acquisition board.</source>
        <translation>调节器或传感器损坏（例如：信号电流低于4mA或高于20mA）。可能表现为故障或不成比例的测量结果。确保连接器质量以及采集板的良好性能。</translation>
    </message>
    <message>
        <source>High value (not calibrated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum value of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low value (not calibrated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum value of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum value ADC of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum value ADC of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ADC value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DAC value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseCell</name>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Select a display type</source>
        <translation>选择一个显示类型</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;简单</translation>
    </message>
    <message>
        <source>&amp;Multimeter</source>
        <translation>&amp;万用表</translation>
    </message>
    <message>
        <source>&amp;Analog</source>
        <translation>&amp;模拟</translation>
    </message>
    <message>
        <source>&amp;Digital</source>
        <translation>&amp;数字</translation>
    </message>
    <message>
        <source>&amp;Classical oscilloscope</source>
        <translation>&amp;经典示波器</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.)</source>
        <translation type="vanished">示波器可能有其自带格式的两个坐标轴.)</translation>
    </message>
    <message>
        <source>Oscilloscope multiple formats</source>
        <translation>多格式示波器</translation>
    </message>
    <message>
        <source>Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets.</source>
        <translation>示波器的单个坐标轴可能含有集中不同格式，每个测量标签和单位都会显示在方括号中。可配置显示系数，如果不是1，也会显示在方括号中。</translation>
    </message>
    <message>
        <source>&amp;BarGraph</source>
        <translation>&amp;柱状图表</translation>
    </message>
    <message>
        <source>Datas &amp;table</source>
        <translation>数据 &amp;表格</translation>
    </message>
    <message>
        <source>No display for this type of data</source>
        <translation>该类型数据无显示</translation>
    </message>
    <message>
        <source>The clipboard does not contain a valid display description.</source>
        <translation>键盘不含有效显示描述</translation>
    </message>
    <message>
        <source>Analyse cell</source>
        <translation>分析单元</translation>
    </message>
    <message>
        <source>Add a cell on the right</source>
        <translation>在右边添加一个单元</translation>
    </message>
    <message>
        <source>Add a cell below</source>
        <translation>在下方添加一个单元</translation>
    </message>
    <message>
        <source>Remove this cell</source>
        <translation>删除该单元</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The parent of cell %1 has more than 2 children!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseSheet</name>
    <message>
        <source>Analyse sheet</source>
        <translation>分析表</translation>
    </message>
    <message>
        <source>Refresh time</source>
        <translation>更新时间</translation>
    </message>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>无法打开文件%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>文件 %1 不含有效 XML 
%2: 行:%3 列: %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>The file %1 has an invalid refresh time.</source>
        <translation>%1有一个有效更新时间</translation>
    </message>
    <message>
        <source>The file %1 has an invalid work sheet size.</source>
        <translation>%1有一个有效的工作表规格</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">不能保存文件%1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">不能保存文件%1</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation>&amp;属性</translation>
    </message>
    <message>
        <source>Row or Column out of range (%1, %2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseSheetSettings</name>
    <message>
        <source>Analyses Sheet Properties</source>
        <translation>分析表属性</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>Enter the title of the work sheet here.</source>
        <translation>在此输入工作表标题</translation>
    </message>
    <message>
        <source>Update Interval</source>
        <translation>更新间距</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation>列</translation>
    </message>
    <message>
        <source>Rows</source>
        <translation>行</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseSpace</name>
    <message>
        <source>%1/Graphic_sheet</source>
        <translation>%1/图形表</translation>
    </message>
    <message>
        <source>This is your analyse space. It holds your analyse sheets. You need to create a new analyse sheet (Menu File-&gt;New) before you can drag datas here.</source>
        <translation>这是你的分析空间。它拥有你的分析表。在拖入数据之前你需要创建一个新的分析表（菜单 文件-&gt;新）。</translation>
    </message>
    <message>
        <source>Sheet %1</source>
        <translation>表格%1</translation>
    </message>
    <message>
        <source>The analyse sheet &apos;%1&apos; has been modified.
Do you want to save this analyse sheet?</source>
        <translation>&apos;%1&apos;分析表已经被修改.
你需要保存该分析表格吗？</translation>
    </message>
    <message>
        <source>Select a work sheet to load</source>
        <translation>选择一个工作表加载</translation>
    </message>
    <message>
        <source>You don&apos;t have an analyse sheet that could be saved!</source>
        <translation>你没有可保存的分析表！</translation>
    </message>
    <message>
        <source>Save current work sheet as</source>
        <translation>保存当前工作表为</translation>
    </message>
    <message>
        <source>This sheet is a model !</source>
        <translation type="vanished">该表格是一个模板！</translation>
    </message>
    <message>
        <source>Would you save it in %1 ?</source>
        <translation type="vanished">你可以把它保存到 %1吗？</translation>
    </message>
    <message>
        <source>You don&apos;t have an analyse sheet that could be captured!</source>
        <translation>你没有可以捕捉的分析表格！</translation>
    </message>
    <message>
        <source>There are no analyse sheets that could be deleted!</source>
        <translation>没有可删除的分析表！</translation>
    </message>
    <message>
        <source>Graphic analyse</source>
        <translation>图形分析</translation>
    </message>
    <message>
        <source>You have to enter a title for the analyse sheet !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAnalyseWin</name>
    <message>
        <source>&amp;Analyse sheet properties...</source>
        <translation>&amp;分析表属性……</translation>
    </message>
    <message>
        <source>Configure &amp;Style...</source>
        <translation>设置&amp;风格……</translation>
    </message>
    <message>
        <source>Template...</source>
        <translation>模板……</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <source>&amp;Capture</source>
        <translation>&amp;捕捉</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYApplication</name>
    <message>
        <source>modified</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAquisitionWidget</name>
    <message>
        <source>Setup</source>
        <translation>启用</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAutoCali</name>
    <message>
        <source>&amp;High value</source>
        <translation>&amp;高值</translation>
    </message>
    <message>
        <source>&amp;Designer value</source>
        <translation>&amp;设计着值</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="vanished">模拟输入</translation>
    </message>
    <message>
        <source>&amp;Low value</source>
        <translation>&amp;低值</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>High</source>
        <translation>高</translation>
    </message>
    <message>
        <source>High Value</source>
        <translation>高值</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>低</translation>
    </message>
    <message>
        <source>Low Value</source>
        <translation>低值</translation>
    </message>
    <message>
        <source>Do you really want to load the designer&apos;s values ?</source>
        <translation>你确定想要加载该设计者的数值？</translation>
    </message>
    <message>
        <source>Currrent values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYAutoCaliInput</name>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
</context>
<context>
    <name>CYBargraph</name>
    <message>
        <source>This display cannot treat this type of data !</source>
        <translation>该显示器无法处理此类数据！</translation>
    </message>
    <message>
        <source>This data is already displayed in this bargraph.</source>
        <translation>该数据已显示在此柱状图表中。</translation>
    </message>
    <message>
        <source>Bargraph</source>
        <translation>柱状图表</translation>
    </message>
</context>
<context>
    <name>CYBargraphPlotter</name>
    <message>
        <source>For a good display the maximum number of bars in the bargraph is %1!</source>
        <translation>为了更好的显示柱状图形中的条形图最大数量为%1!</translation>
    </message>
    <message>
        <source>Bargraph</source>
        <translation>柱状图表</translation>
    </message>
    <message>
        <source>Scale</source>
        <translation>标度</translation>
    </message>
    <message>
        <source>Minimum value</source>
        <translation>最小值</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation>最大值</translation>
    </message>
    <message>
        <source>Sense</source>
        <translation>检测</translation>
    </message>
    <message>
        <source>Increasing</source>
        <translation>增加</translation>
    </message>
    <message>
        <source>Decreasing</source>
        <translation>降低</translation>
    </message>
    <message>
        <source>Alarm</source>
        <translation>警报</translation>
    </message>
    <message>
        <source>Upper bound value</source>
        <translation>上界值</translation>
    </message>
    <message>
        <source>Upper alarm enable</source>
        <translation>启用上部报警</translation>
    </message>
    <message>
        <source>Lower bound value</source>
        <translation>下界值</translation>
    </message>
    <message>
        <source>Lower alarm enable</source>
        <translation>启用下端报警</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>风格</translation>
    </message>
    <message>
        <source>Normal Bar Color</source>
        <translation>标准导条颜色</translation>
    </message>
    <message>
        <source>Out-of-range Color</source>
        <translation>超出范围的颜色</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation>背景颜色</translation>
    </message>
</context>
<context>
    <name>CYBargraphSetup</name>
    <message>
        <source>BarGraph Settings</source>
        <translation>柱状图表设置</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>风格</translation>
    </message>
    <message>
        <source>Display Range</source>
        <translation>显示范围</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYBool</name>
    <message>
        <source>Designer value: %1.</source>
        <translation>设计者值：%1.</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>二进制</translation>
    </message>
    <message>
        <source>true</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>false</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCC</name>
    <message>
        <source>Maintenance</source>
        <translation>维修</translation>
    </message>
    <message>
        <source>Cycles/switching counter</source>
        <translation>循环/计数器开关</translation>
    </message>
    <message>
        <source>Counter used for maintenance</source>
        <translation>维修使用计数器</translation>
    </message>
    <message>
        <source>Total</source>
        <translation>总</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>分</translation>
    </message>
    <message>
        <source>Alert threshold</source>
        <translation>警报极限值</translation>
    </message>
    <message>
        <source>An alert is generated if the partial counter reaches this value.</source>
        <translation>如果分计数器达到该数值，将会引起警报</translation>
    </message>
    <message>
        <source>A null value disable the control.</source>
        <translation>零值将停用该控制</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold.</source>
        <translation type="vanished">表面分计数器达到报警极限值</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>警报</translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold. This threshold is settable in the maintenance counters window.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCS</name>
    <message>
        <source>Maintenance</source>
        <translation>维修</translation>
    </message>
    <message>
        <source>Time counter</source>
        <translation>计时器</translation>
    </message>
    <message>
        <source>Counter used for maintenance</source>
        <translation>维修使用计数器</translation>
    </message>
    <message>
        <source>Total</source>
        <translation>总</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>分</translation>
    </message>
    <message>
        <source>Alert threshold</source>
        <translation>警报极限值</translation>
    </message>
    <message>
        <source>An alert is generated if the partial counter reaches this value.</source>
        <translation>如果分计数器达到该数值，将会引起警报</translation>
    </message>
    <message>
        <source>A null value disable the control.</source>
        <translation>零值将停用该控制</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold.</source>
        <translation type="vanished">表面分计数器达到报警极限值</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>警报</translation>
    </message>
    <message>
        <source>Indicates that the partial counter reached the alert threshold. This threshold is settable in the maintenance counters window.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCT</name>
    <message>
        <source>Alert value</source>
        <translation>警报值</translation>
    </message>
    <message>
        <source>Fault value</source>
        <translation>错误值</translation>
    </message>
    <message>
        <source> &gt; MAX (Max. alert)</source>
        <translation>&gt; MAX （最高警报）</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>程序</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert. Also The maximum time (or number of cycles) of alert has been reached and thus produced a fault.</source>
        <translation>测量结果高于最大许可值时产生一个警报。同时达到警报的最大期限(或循环次数)，因而产生故障。</translation>
    </message>
    <message>
        <source> &lt; MIN (Max. alert)</source>
        <translation> &lt; MIN (最高警报)</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert. Also The maximum time (or number of cycles) of alert has been reached and thus produced a fault.</source>
        <translation>测量结果低于最小许可值时产生一个警报。同时达到警报的最大期限(或循环次数)，因而产生故障。</translation>
    </message>
    <message>
        <source> &gt; MAX: DATA1</source>
        <translation> &gt; MAX: DATA1</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum allowed value, which produced a fault.</source>
        <translation>测量结果高于最大许可值时产生一个故障。</translation>
    </message>
    <message>
        <source> &lt; MIN: DATA1</source>
        <translation>&gt; MIN: DATA1</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum allowed value, which produced a fault.</source>
        <translation>测量结果低于最小许可值时产生一个故障。</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert.</source>
        <translation>测量结果高于最大许可值时产生一个警报。</translation>
    </message>
    <message>
        <source>The value at which the alert occured is written in the message.</source>
        <translation>发生警报的值应写入信息中。</translation>
    </message>
    <message>
        <source>The maximum measured value is written in the message of end of alert.</source>
        <translation>最大测量值写在报警信息结束。</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert.</source>
        <translation>测量结果低于最小许可值时产生一个警报。</translation>
    </message>
    <message>
        <source>The minimum measured value is written in the message of end of alert.</source>
        <translation>最小测量值写在报警信息结束。</translation>
    </message>
</context>
<context>
    <name>CYCTCyc</name>
    <message>
        <source>Number of inhibition cycles before control (T0)</source>
        <translation>控制前抑制循环次数（T0）</translation>
    </message>
    <message>
        <source>The minimum number of cycles before control is T0</source>
        <translation>控制前最低循环次数为T0</translation>
    </message>
    <message>
        <source>The maximum number of cycles before control is T0+T1</source>
        <translation>控制前最大循环次数为T0+T1</translation>
    </message>
    <message>
        <source>Supplementary cycles before control (T1)</source>
        <translation>控制前补充循环次数（T1）</translation>
    </message>
    <message>
        <source>The control start when the measure in inside the tolerances, or when this optional inhibition is overreached</source>
        <translation>当测量在误差之内或者改抑制超出的时候控制启动</translation>
    </message>
    <message>
        <source>Stop test if maximum alert ?</source>
        <translation>最多报警时停止设备？</translation>
    </message>
    <message>
        <source>The bench will be stopped if the max number of cycles alert is reached.</source>
        <translation>当设备报警达到最大次数时设备停止</translation>
    </message>
    <message>
        <source>Maximum number of cycles out of alert tolerances</source>
        <translation>警报误差外最大循环次数</translation>
    </message>
    <message>
        <source>Maximum outside tolerances number of cycles for control.</source>
        <translation>控制用最大外部允许循环次数。</translation>
    </message>
    <message>
        <source>If reached =&gt; fault!</source>
        <translation>达到 =&gt; 错误!</translation>
    </message>
    <message>
        <source>Stop test if out of fault tolerances ?</source>
        <translation>如错误误差超出即停止测试</translation>
    </message>
    <message>
        <source>The bench will be stopped if fault tolerance is reached.</source>
        <translation>如错误误差临界值试验台将停止</translation>
    </message>
    <message>
        <source> &gt; MAX (Max. alert)</source>
        <translation>&gt; MAX （最高警报）</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>程序</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert. Also the maximum number of cycles of alert has been reached and thus produced a fault.</source>
        <translation>测量结果高于最大许可值时产生一个警报。同时达到警报的最大循环次数，因而产生故障。</translation>
    </message>
    <message>
        <source> &lt; MIN (Max. alert)</source>
        <translation> &lt; MIN (最高警报)</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert. Also the maximum number of cycles of alert has been reached and thus produced a fault.</source>
        <translation>测量结果低于最小许可值时产生一个警报。同时达到警报的最大循环次数，因而产生故障。</translation>
    </message>
</context>
<context>
    <name>CYCTInput</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation>注意：这些设置属于当前项目</translation>
    </message>
</context>
<context>
    <name>CYCTTime</name>
    <message>
        <source>Minimum measure control delay (T0)</source>
        <translation>最低测量控制延迟（T0）</translation>
    </message>
    <message>
        <source>The minimum control delay is T0</source>
        <translation>最低测量控制延迟是T0</translation>
    </message>
    <message>
        <source>The maximum control delay is T0+T1</source>
        <translation>最大测量控制延迟是T0+T1</translation>
    </message>
    <message>
        <source>Supplementary measure control delay (T1)</source>
        <translation>补充测量控制延迟（T1）</translation>
    </message>
    <message>
        <source>The control start when the measure in inside the tolerances, or when this optional inhibition is overreached</source>
        <translation>当测量在误差之内或者改抑制超出的时候控制启动</translation>
    </message>
    <message>
        <source>Stop test if maximum alert ?</source>
        <translation>最多报警时停止设备？</translation>
    </message>
    <message>
        <source>The bench will be stopped if the max time alert is reached.</source>
        <translation>如最大警报时间达到即停止设备</translation>
    </message>
    <message>
        <source>Maximum time out of alert tolerances</source>
        <translation>警报误差外最大时间</translation>
    </message>
    <message>
        <source>Maximum outside alert tolerances time before process fault.</source>
        <translation>程序错误前最大时间误差值</translation>
    </message>
    <message>
        <source>Stop test if out of fault tolerances ?</source>
        <translation>如错误误差超出即停止测试</translation>
    </message>
    <message>
        <source>The bench will be stopped if fault tolerance is reached.</source>
        <translation>如错误误差临界值试验台将停止</translation>
    </message>
    <message>
        <source> &gt; MAX (Max. alert)</source>
        <translation>&gt; MAX （最高警报）</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>程序</translation>
    </message>
    <message>
        <source>Measurement was higher than the maximum tolerated value, which produced an alert. Also the maximum time of alert has been reached and thus produced a fault.</source>
        <translation>测量结果高于最大许可值时产生一个警报。同时达到警报的最大期限，因而产生故障。</translation>
    </message>
    <message>
        <source> &lt; MIN (Max. alert)</source>
        <translation> &lt; MIN (最高警报)</translation>
    </message>
    <message>
        <source>Measurement was lower than the minimum tolerated value, which produced an alert. Also the maximum time of alert has been reached and thus produced a fault.</source>
        <translation>测量结果低于最小许可值时产生一个警报。同时达到警报的最大期限，因而产生故障。</translation>
    </message>
</context>
<context>
    <name>CYChangeAccess</name>
    <message>
        <source>Choose access: </source>
        <translation>选择通道</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Change Accesss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYCnt</name>
    <message>
        <source>Total counter</source>
        <translation>总计数器</translation>
    </message>
    <message>
        <source>Counter used for maintenance</source>
        <translation>维修使用计数器</translation>
    </message>
    <message>
        <source>Partial counter</source>
        <translation>分计数器</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
</context>
<context>
    <name>CYComboBoxDialog</name>
    <message>
        <source>Dialog comboBox data</source>
        <translation>对话框下拉列表数据</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYCommand</name>
    <message>
        <source>Command</source>
        <translation>指令</translation>
    </message>
</context>
<context>
    <name>CYCore</name>
    <message>
        <source>Bench</source>
        <translation>试验台</translation>
    </message>
    <message>
        <source>Undef</source>
        <translation>Undef</translation>
    </message>
    <message>
        <source>Simulation</source>
        <translation>模拟</translation>
    </message>
    <message>
        <source>Designer</source>
        <translation>设计员</translation>
    </message>
    <message>
        <source>Protected</source>
        <translation>受保护的</translation>
    </message>
    <message>
        <source>Administrator</source>
        <translation>管理员</translation>
    </message>
    <message>
        <source>Cannot start the &apos;%1&apos; with the %2 on COM%3 !</source>
        <translation>不能在 COM%3启动 &apos;%1&apos; 和 %2  !</translation>
    </message>
    <message>
        <source>Can&apos;t remove the user %1 because it is the current user!</source>
        <translation>不能移除%1用户，因为它是当前用户！</translation>
    </message>
    <message>
        <source>Removing user</source>
        <translation>正在移除用户</translation>
    </message>
    <message>
        <source>Can&apos;t remove the group %1 because it is used by the user %2!</source>
        <translation>不能移除%1组，因为%2正在使用!</translation>
    </message>
    <message>
        <source>Removing users group</source>
        <translation>正在移除用户组</translation>
    </message>
    <message>
        <source>GID</source>
        <translation>GID</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <source>User</source>
        <translation>用户</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <source>End</source>
        <translation>结束</translation>
    </message>
    <message>
        <source>This access has the administrator rights and authorizes all the menus!</source>
        <translation>该通道有管理员所有权限可以授权所有菜单！</translation>
    </message>
    <message>
        <source>Do you want to create a new access more protected?</source>
        <translation>你确定要创建一个更受保护的新通道？</translation>
    </message>
    <message>
        <source>Administrator rights!</source>
        <translation>管理员权限！</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;是</translation>
    </message>
    <message>
        <source>&amp;Continue</source>
        <translation>&amp;继续</translation>
    </message>
    <message>
        <source>Initializing metrology window...</source>
        <translation>正在初始化度量窗口……</translation>
    </message>
    <message>
        <source>Times of tests</source>
        <translation>测试时间</translation>
    </message>
    <message>
        <source>Times of access</source>
        <translation>通道时间</translation>
    </message>
    <message>
        <source>Evolution of the project settings</source>
        <translation>项目设置进展</translation>
    </message>
    <message>
        <source>Cannot create the directory %1</source>
        <translation>无法创建路径%1</translation>
    </message>
    <message>
        <source>Cannot find backup directoy</source>
        <translation>无法找到备份路径</translation>
    </message>
    <message>
        <source>Cannot find the directory to backup</source>
        <translation>无法找到至备份路径</translation>
    </message>
    <message>
        <source>Cannot find the file to backup</source>
        <translation>无法找到至备份文件</translation>
    </message>
    <message>
        <source>See the users managment section of the computer installation procedure.</source>
        <translation>见电脑安装步骤中用户管区</translation>
    </message>
    <message>
        <source>The CYLIX base directrory %1 doesn&apos;t exist!</source>
        <translation>CYLIX基本目录%1不存在！</translation>
    </message>
    <message>
        <source>%1 is not a directory!</source>
        <translation>%1不是目录！</translation>
    </message>
    <message>
        <source>%1 is not a readable!</source>
        <translation>%1不可读！</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>滤波器</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>链接</translation>
    </message>
    <message>
        <source>Do you want to include the sub-groups ?</source>
        <translation>你确定包含子组合吗？</translation>
    </message>
    <message>
        <source>Loading groug</source>
        <translation>加载组合</translation>
    </message>
    <message>
        <source>Add alphabetically the datas of this group in a new column ?</source>
        <translation>按字母顺序在新一列中添加该组合的数据？</translation>
    </message>
    <message>
        <source>If you answer &quot;No&quot;, the datas will be added in news rows !</source>
        <translation>如果你回答&quot;否&quot;,数据将被添加进新一行！</translation>
    </message>
    <message>
        <source>class: %1 not supported (%2)</source>
        <translation>等级：%1不被支持(%2)</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>警报</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation>故障</translation>
    </message>
    <message>
        <source>%1:%2 : designer value checking</source>
        <translation>%1:%2 :设计者值检查</translation>
    </message>
    <message>
        <source>Choose a filename to save under for this export file.</source>
        <translation>为该导出文件选择保存文件名</translation>
    </message>
    <message>
        <source>HOST(S)	LINK(S)	GROUP	NAME	LABEL	VALUE	DEF	MIN	MAX	HELP
</source>
        <translation>HOST(S)	LINK(S)	GROUP	NAME	LABEL	VALUE	DEF	MIN	MAX	HELP
</translation>
    </message>
    <message>
        <source>Metrology window</source>
        <translation>度量窗口</translation>
    </message>
    <message>
        <source>Cannot find the manual for the current language !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot remove the application&apos;s temporary directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You do not have access rights to the requested documentation ! Check your access rights with your Cylix administrator. These can be configured in the Cylix user administration tool.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not find the access action to open the document %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not find document %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error %1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initialization SoliaGraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SoliaGraph is not activated on this Cylix. If you are interested in this tool, please contact SOLIA Concept.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting SoliaGraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Doesn&apos;t exist &apos;%1&apos; !</source>
        <translation type="unfinished">不存在 &apos;%1&apos; ！</translation>
    </message>
    <message>
        <source>Initializing SoliaGraph...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to unzip file SoliaGraph!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to start SoliaGraph !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SoliaGraph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDB</name>
    <message>
        <source>Starting refresh timer datas base %1 at %2 ms</source>
        <translation>开始刷新%2毫秒时的计时器数据库%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">不能保存文件%1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">不能保存文件%1</translation>
    </message>
    <message>
        <source>The clipboard does not contain a valid datas base description.</source>
        <translation>剪贴板中不包含有效的数据库说明。</translation>
    </message>
    <message>
        <source>Cannot remove %1 from %2</source>
        <translation>无法从%2中移除%1</translation>
    </message>
</context>
<context>
    <name>CYDBLibs</name>
    <message>
        <source>Project</source>
        <translation>项目</translation>
    </message>
    <message>
        <source>Setpoints protection</source>
        <translation>设定值保护</translation>
    </message>
    <message>
        <source>Avoid to erase project setpoints. If a project, which has ever been used by the machine, is modified in the project editor then its saving is done in a different project directory.</source>
        <translation>避免删除项目的设定值。如果是该机器未经使用过的项目，应在项目编辑器中修改该项目，然后将其保存在不同的项目目录中。</translation>
    </message>
    <message>
        <source>The name of this directory is automatically created with the date and hour of the modification.</source>
        <translation>该目录名称为自动创建，其中包括修改日期和时间。</translation>
    </message>
    <message>
        <source>Test</source>
        <translation>测试</translation>
    </message>
    <message>
        <source>Backup</source>
        <translation>备份</translation>
    </message>
    <message>
        <source>Enable backup</source>
        <translation>启用备份</translation>
    </message>
    <message>
        <source>Double safeguard in another partition all the files of parameters and useful results in the event of loss of the partition of datas /home.</source>
        <translation>在另一个分区中的双重保障，所有与数据/个人资料夹丢失分区相关的参数文件和有用结果。</translation>
    </message>
    <message>
        <source>Backup path</source>
        <translation>备份路径</translation>
    </message>
    <message>
        <source>Partition backup path.</source>
        <translation>分区备份路径。</translation>
    </message>
    <message>
        <source>Events</source>
        <translation>活动</translation>
    </message>
    <message>
        <source>Supervisor starting</source>
        <translation>管理器启动</translation>
    </message>
    <message>
        <source>Supervisor stoping</source>
        <translation>管理器停止</translation>
    </message>
    <message>
        <source>New calibration</source>
        <translation>新校准</translation>
    </message>
    <message>
        <source>Last channel calibrated</source>
        <translation>最后一个校准通道</translation>
    </message>
    <message>
        <source>Viewer</source>
        <translation>浏览器</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>Date and hour</source>
        <translation>日期和时间</translation>
    </message>
    <message>
        <source>Since</source>
        <translation>自</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>From</source>
        <translation>从</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <source>Help of the selected event</source>
        <translation>选定事件的帮助</translation>
    </message>
    <message>
        <source>Mail</source>
        <translation>邮件</translation>
    </message>
    <message>
        <source>Enable the send of fault messages by email</source>
        <translation>可通过邮件发送故障信息</translation>
    </message>
    <message>
        <source>SMTP servor name</source>
        <translation>SMTP服务器名称</translation>
    </message>
    <message>
        <source>SMTP port</source>
        <translation>SMTP 端口</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">域</translation>
    </message>
    <message>
        <source>Sender</source>
        <translation>发送人</translation>
    </message>
    <message>
        <source>Receiver (s)</source>
        <translation>接收人</translation>
    </message>
    <message>
        <source>If there is more than one address, separate them with commas.</source>
        <translation>如果有多个地址，用逗号将其分开。</translation>
    </message>
    <message>
        <source>Subject</source>
        <translation>主题</translation>
    </message>
    <message>
        <source>Body</source>
        <translation>正文</translation>
    </message>
    <message>
        <source>Users</source>
        <translation>用户</translation>
    </message>
    <message>
        <source>Simulation access</source>
        <translation>模拟接入</translation>
    </message>
    <message>
        <source>Current user</source>
        <translation>当前用户</translation>
    </message>
    <message>
        <source>This information is also shown in the Cylix window title bar.</source>
        <translation>这个信息也被显示在Cylix窗口标题栏。</translation>
    </message>
    <message>
        <source>Date and time of access change</source>
        <translation>访问日期和时间变更</translation>
    </message>
    <message>
        <source>Acquisition</source>
        <translation>采集</translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation>度量</translation>
    </message>
    <message>
        <source>Inhibits reset points</source>
        <translation>禁止复位点</translation>
    </message>
    <message>
        <source>Inhibits addition point</source>
        <translation>禁止加入点</translation>
    </message>
    <message>
        <source>Inhibits suppression point</source>
        <translation>禁止抑制点</translation>
    </message>
    <message>
        <source>Inhibits calibration</source>
        <translation>禁止校准</translation>
    </message>
    <message>
        <source>Inhibits verification</source>
        <translation>禁止验证</translation>
    </message>
    <message>
        <source>Inhibits designer values</source>
        <translation>禁止设计师价值</translation>
    </message>
    <message>
        <source>Full display</source>
        <translation>全显</translation>
    </message>
    <message>
        <source>Full display with all values in table and all curves.</source>
        <translation>全显表格中的所有值和所有曲线。</translation>
    </message>
    <message>
        <source>Simplified display of table and curves without some values.</source>
        <translation>精显不含部分值的表格和曲线。</translation>
    </message>
    <message>
        <source>Curve color of deviation FS</source>
        <translation>FS偏差曲线的颜色</translation>
    </message>
    <message>
        <source>Curve style of deviation FS</source>
        <translation>FS偏差曲线类型</translation>
    </message>
    <message>
        <source>No Curve</source>
        <translation>无曲线</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation>明细</translation>
    </message>
    <message>
        <source>Sticks</source>
        <translation>换档杆</translation>
    </message>
    <message>
        <source>Steps</source>
        <translation>步骤</translation>
    </message>
    <message>
        <source>Dots</source>
        <translation>点</translation>
    </message>
    <message>
        <source>Curve color of linearity error</source>
        <translation>线性错误曲线颜色 (%)</translation>
    </message>
    <message>
        <source>Curve style of linearity error</source>
        <translation>线性错误曲线风格</translation>
    </message>
    <message>
        <source>%1/calibrating</source>
        <translation>%1 /校准</translation>
    </message>
    <message>
        <source>Directory of PDF recording</source>
        <translation>PDF记录目录</translation>
    </message>
    <message>
        <source>Directory containing the report file of verification or calibration. These are automatically classified into sensor subdirectories.</source>
        <translation>目录包含验证或校准的报告文件。此类目录被自动归为传感器子目录。</translation>
    </message>
    <message>
        <source>SMTP user name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SMTP password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The help of an event is a real diagnostic aid to understand its cause.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>One or more hyperlinks allow to open the Cylix manual directly at a precise place, as for example the part relating to the setting which can influence the event triggering.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>One or more data (measurements, setpoints, parameters...) allow to visualize directly values which can influence the event triggering. These values, in bold, are refreshed when the help is displayed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Project sent to the numerical regulator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDI</name>
    <message>
        <source>Digital input</source>
        <translation>数字输入</translation>
    </message>
    <message>
        <source>Unplugged</source>
        <translation>不插电</translation>
    </message>
</context>
<context>
    <name>CYDO</name>
    <message>
        <source>Digital output</source>
        <translation>数字输出</translation>
    </message>
    <message>
        <source>Unplugged</source>
        <translation>不插电</translation>
    </message>
</context>
<context>
    <name>CYData</name>
    <message>
        <source>Control</source>
        <translation>控制</translation>
    </message>
    <message>
        <source>Integer</source>
        <translation>整数</translation>
    </message>
    <message>
        <source>Boolean</source>
        <translation>布尔</translation>
    </message>
    <message>
        <source>Floating</source>
        <translation>浮动工具条</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <source>Word</source>
        <translation>关键词</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>文字</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <source>%1 cannot be wrote because it is in write mode !</source>
        <translation type="vanished">%1 不能写入因为在编辑模式</translation>
    </message>
    <message>
        <source>Note :</source>
        <translation>备注：</translation>
    </message>
    <message>
        <source>Notes :</source>
        <translation>备注:</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 cannot be wrote because not in write mode !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error of help data markup %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The increase in value is too abrupt. The value of this data can only be increased by a maximum of %1% of its current value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The decrease in value is too abrupt. The value of this data can only be decreased by a maximum of %1% of its current value.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasBrowser</name>
    <message>
        <source>User datas</source>
        <translation>用户数据</translation>
    </message>
    <message>
        <source>System datas</source>
        <translation>系统数据</translation>
    </message>
    <message>
        <source>All datas</source>
        <translation>所有数据</translation>
    </message>
    <message>
        <source>The Seach bar makes it possible to search by words or characters among the filtered results. &lt;br/&gt;&lt;br/&gt; Only matches (data and/or categories) are then displayed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasBrowserList</name>
    <message>
        <source>Data Browser</source>
        <translation>数据浏览器</translation>
    </message>
    <message>
        <source>Physical Type</source>
        <translation>物理类型</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>滤波器</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>链接</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Drag datas to empty cells of a analyse sheet.</source>
        <translation>将数据拖动到分析表的空白单元格。</translation>
    </message>
    <message>
        <source>The data browser lists the connected hosts and the datas that they provide. Click and drag datas into drop zones of a analyse sheet. A display will appear that visualizes the values provided by the data. Some data displays can display values of multiple datas. Simply drag other datas on to the display to add more datas.</source>
        <translation>数据浏览器中列出了连接主机及主机提供的数据。单击并拖动数据到分析表中的区域。将显示数据的可视化值。部分数据显示可显示多个数据值。只需拖动显示添加更多数据的其他数据。</translation>
    </message>
    <message>
        <source>Drag datas to empty fields in a work sheet</source>
        <translation>将数据拖动到工作表中的空白域。</translation>
    </message>
</context>
<context>
    <name>CYDatasEditList</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>笔</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>单元</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>CLEMESSY 名称</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>系数</translation>
    </message>
    <message>
        <source>Groupe</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Connexion</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Hôte</source>
        <translation>主人</translation>
    </message>
    <message>
        <source>State</source>
        <translation>状态</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <source>Push this button to configure the color of the data in the oscilloscope.</source>
        <translation>按下该按钮以设置示波器内数据颜色</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>&amp;Set Color</source>
        <translation>&amp;设置颜色</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Push this button to configure the pen of the data in the oscilloscope.</source>
        <translation>按下该按钮以设置示波器内数据笔</translation>
    </message>
    <message>
        <source>Set Pen</source>
        <translation>设置笔</translation>
    </message>
    <message>
        <source>Push this button to configure the display coefficient of the data in the oscilloscope.</source>
        <translation>按下该按钮以设置示波器内数据显示系数</translation>
    </message>
    <message>
        <source>Set Coefficient</source>
        <translation>设置系数</translation>
    </message>
    <message>
        <source>Push this button to delete the data.</source>
        <translation>按下该按钮删除数据</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasListView</name>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>笔</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>单元</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>CLEMESSY 名称</translation>
    </message>
    <message>
        <source>Coefficient</source>
        <translation>系数</translation>
    </message>
    <message>
        <source>Display coefficient</source>
        <translation>显示系数</translation>
    </message>
    <message>
        <source>Enter the new display coefficient: </source>
        <translation>输入新的显示系数:</translation>
    </message>
    <message>
        <source>You must enter a coefficient display different of 0 !</source>
        <translation>您必须输入一个显示不同0的系数!</translation>
    </message>
    <message>
        <source>Data label</source>
        <translation>数据标签</translation>
    </message>
    <message>
        <source>Enter the new label: </source>
        <translation>输入新标签:</translation>
    </message>
    <message>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter the new display offset: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can enter a maximun of %1 data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Acquisition list of datas full</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasTable</name>
    <message>
        <source>Datas table</source>
        <translation>数据表</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <source>Forcing</source>
        <translation>加压</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Connection(s)</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Host(s)</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>Phys</source>
        <translation>物理</translation>
    </message>
    <message>
        <source>Elec</source>
        <translation>电气</translation>
    </message>
    <message>
        <source>ADC</source>
        <translation>ADC</translation>
    </message>
    <message>
        <source>Display</source>
        <translation>显示</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>分</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>控制</translation>
    </message>
    <message>
        <source>Addr</source>
        <translation>地址</translation>
    </message>
    <message>
        <source>Threshold</source>
        <translation>阈值</translation>
    </message>
    <message>
        <source>Column label</source>
        <translation>列标签</translation>
    </message>
    <message>
        <source>Enter the label of this column:</source>
        <translation>在该列中输入标签:</translation>
    </message>
    <message>
        <source>&amp;Forcing mode</source>
        <translation>&amp;加压模式</translation>
    </message>
    <message>
        <source>Reset mode</source>
        <translation>复位模式</translation>
    </message>
    <message>
        <source>Partial mode</source>
        <translation>部分模式</translation>
    </message>
    <message>
        <source>Note mode</source>
        <translation>注意模式</translation>
    </message>
    <message>
        <source>Alert threshold</source>
        <translation>警报极限值</translation>
    </message>
    <message>
        <source>Hide column</source>
        <translation>隐藏列</translation>
    </message>
    <message>
        <source>Show column</source>
        <translation>显示列</translation>
    </message>
    <message>
        <source>Add column</source>
        <translation>增加列</translation>
    </message>
    <message>
        <source>Change column label</source>
        <translation>更改列标签</translation>
    </message>
    <message>
        <source>Change column width</source>
        <translation>更改列宽</translation>
    </message>
    <message>
        <source>Remove row</source>
        <translation>删除行</translation>
    </message>
    <message>
        <source>Change row height</source>
        <translation>更改行高</translation>
    </message>
    <message>
        <source>Export PDF</source>
        <translation>导出PDF</translation>
    </message>
    <message>
        <source>Input the new width of selected columns</source>
        <translation>输入选定列的新宽度</translation>
    </message>
    <message>
        <source>No row selected !</source>
        <translation>没有选定的行！</translation>
    </message>
    <message>
        <source>Are you sure to remove the setected row(s) ?</source>
        <translation>您确定删除选定行吗？</translation>
    </message>
    <message>
        <source>Input the new height of selected rows</source>
        <translation>输入选定行的新高度</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Page %1</source>
        <translation>页 %1</translation>
    </message>
    <message>
        <source>Id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1/table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1/table.pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t print %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasTableDisplayCell</name>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>No display for datas group</source>
        <translation>没有数据组显示</translation>
    </message>
    <message>
        <source>Select a display type</source>
        <translation>选择一个显示类型</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;简单</translation>
    </message>
    <message>
        <source>&amp;Multimeter</source>
        <translation>&amp;万用表</translation>
    </message>
    <message>
        <source>&amp;Analog</source>
        <translation>&amp;模拟</translation>
    </message>
    <message>
        <source>&amp;Digital</source>
        <translation>&amp;数字</translation>
    </message>
    <message>
        <source>&amp;Classical oscilloscope</source>
        <translation>&amp;经典示波器</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.)</source>
        <translation type="vanished">示波器可能有其自带格式的两个坐标轴.)</translation>
    </message>
    <message>
        <source>Oscilloscope multiple formats</source>
        <translation>多格式示波器</translation>
    </message>
    <message>
        <source>Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets.</source>
        <translation>示波器的单个坐标轴可能含有集中不同格式，每个测量标签和单位都会显示在方括号中。可配置显示系数，如果不是1，也会显示在方括号中。</translation>
    </message>
    <message>
        <source>&amp;BarGraph</source>
        <translation>&amp;柱状图表</translation>
    </message>
    <message>
        <source>Datas &amp;table</source>
        <translation>数据 &amp;表格</translation>
    </message>
    <message>
        <source>No display for this type of data</source>
        <translation>该类型数据无显示</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDatasTableSetup</name>
    <message>
        <source>Datas Table Settings</source>
        <translation>数据表设置</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDial</name>
    <message>
        <source>Minimum value</source>
        <translation>最小值</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation>最大值</translation>
    </message>
    <message>
        <source>Scale step</source>
        <translation>缩放步骤</translation>
    </message>
</context>
<context>
    <name>CYDialScaleDialog</name>
    <message>
        <source>Scale setup</source>
        <translation>比例设置</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplay</name>
    <message>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>Automatic title</source>
        <translation>自动标题</translation>
    </message>
    <message>
        <source>Edit the data&apos;s label to change the title.</source>
        <translation>编辑数据标签，更改标题。</translation>
    </message>
    <message>
        <source>Enter the title in the box below.</source>
        <translation>在下列文本框中输入标题。</translation>
    </message>
    <message>
        <source>Add unit to the title</source>
        <translation>添加单元标题</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>风格</translation>
    </message>
    <message>
        <source>Font size</source>
        <translation>字体大小</translation>
    </message>
    <message>
        <source>&lt;qt&gt;&lt;p&gt;This is a data display. To customize a sensor display click and hold the right mouse button on either the frame or the display box and select the &lt;i&gt;Properties&lt;/i&gt; entry from the popup menu. Select &lt;i&gt;Remove&lt;/i&gt; to delete the display from the work sheet.&lt;/p&gt;%1&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;&lt;p&gt;这是一个数据显示。设置传感器显示，将光标移动到框架或显示箱，点击鼠标右键，选择&lt;i&gt;属性&lt;/i&gt;，在弹出菜单中输入设置信息。 选定&lt;i&gt;删除&lt;/i&gt;，从工作图表中删除显示项。&lt;/p&gt;%1&lt;/qt&gt;</translation>
    </message>
    <message>
        <source>Create a new file %1</source>
        <translation>创建一个新文件%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML ! %2: %3 %4</source>
        <translation>该文件%1不包含有效的XML！%2: %3 %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">不能保存文件%1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">不能保存文件%1</translation>
    </message>
    <message>
        <source>Display</source>
        <translation>显示</translation>
    </message>
    <message>
        <source>&amp;Setup update interval</source>
        <translation>&amp;设置更新区域</translation>
    </message>
    <message>
        <source>&amp;Continue update</source>
        <translation>&amp;继续更新</translation>
    </message>
    <message>
        <source>P&amp;ause update</source>
        <translation>&amp;停止更新</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayCell</name>
    <message>
        <source>The clipboard does not contain a valid display description.</source>
        <translation>键盘不含有效显示描述</translation>
    </message>
    <message>
        <source>Do you really want to delete the display?</source>
        <translation>您确定删除该显示吗？</translation>
    </message>
</context>
<context>
    <name>CYDisplayDummy</name>
    <message>
        <source>Empty cell</source>
        <translation>空单元</translation>
    </message>
    <message>
        <source>analyse sheet</source>
        <translation>分析表</translation>
    </message>
    <message>
        <source>datas table</source>
        <translation>数据表</translation>
    </message>
    <message>
        <source>This is an empty cell in an %1. Drag a data or group of datas from the data browser and drop it here. A data(s) display will appear that allows you to monitor the values of the data(s) over time.</source>
        <translation>这是%1中的空单元。从数据浏览器中拖动一个数据或一组数据到此处。将出现一个数据显示，方便您监测超时数据值。</translation>
    </message>
    <message>
        <source>Dummy display</source>
        <translation>虚拟显示</translation>
    </message>
    <message>
        <source>&amp;Select a display type</source>
        <translation>&amp;选择显示类型</translation>
    </message>
    <message>
        <source>&amp;Simple</source>
        <translation>&amp;简单</translation>
    </message>
    <message>
        <source>&amp;Multimeter</source>
        <translation>&amp;万用表</translation>
    </message>
    <message>
        <source>&amp;Analog</source>
        <translation>&amp;模拟</translation>
    </message>
    <message>
        <source>&amp;Digital</source>
        <translation>&amp;数字</translation>
    </message>
    <message>
        <source>&amp;Classical oscilloscope</source>
        <translation>&amp;经典示波器</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.)</source>
        <translation type="vanished">示波器可能有其自带格式的两个坐标轴.)</translation>
    </message>
    <message>
        <source>Oscilloscope multiple formats</source>
        <translation>多格式示波器</translation>
    </message>
    <message>
        <source>Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets.</source>
        <translation>示波器的单个坐标轴可能含有集中不同格式，每个测量标签和单位都会显示在方括号中。可配置显示系数，如果不是1，也会显示在方括号中。</translation>
    </message>
    <message>
        <source>&amp;BarGraph</source>
        <translation>&amp;柱状图表</translation>
    </message>
    <message>
        <source>Datas &amp;table</source>
        <translation>数据 &amp;表格</translation>
    </message>
    <message>
        <source>Oscilloscope may have two ordinate axes, each with its own format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayItem</name>
    <message>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayListView</name>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>单元</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <source>Designer Name</source>
        <translation>CLEMESSY 名称</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>滤波器</translation>
    </message>
    <message>
        <source>Link</source>
        <translation>链接</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplaySatusIndicator</name>
    <message>
        <source>Connection is OK.</source>
        <translation>连接OK。</translation>
    </message>
    <message>
        <source>Connection has been lost.</source>
        <translation>连接丢失！</translation>
    </message>
    <message>
        <source>Update is pausing.</source>
        <translation>更新暂停。</translation>
    </message>
    <message>
        <source>The refresh of all graphic analyzers is pausing.</source>
        <translation>所有图形分析器均暂停刷新。</translation>
    </message>
</context>
<context>
    <name>CYDisplaySimple</name>
    <message>
        <source>Simple display</source>
        <translation>简单显示</translation>
    </message>
    <message>
        <source>Numerical base</source>
        <translation>数值基础</translation>
    </message>
    <message>
        <source>The numerical base is used only if the data is only an unsigned integer type!</source>
        <translation>当数据属于无符号的整数类型时才使用数值基础。</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>自动</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>二进制</translation>
    </message>
    <message>
        <source>Decimal</source>
        <translation>十进制</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation>十六进制</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>风格</translation>
    </message>
    <message>
        <source>Bicolor</source>
        <translation>双色</translation>
    </message>
    <message>
        <source>Color On</source>
        <translation>开启颜色</translation>
    </message>
    <message>
        <source>Color Off</source>
        <translation>关闭颜色</translation>
    </message>
    <message>
        <source>Shape</source>
        <translation>形状</translation>
    </message>
    <message>
        <source>Rectangular</source>
        <translation>矩形</translation>
    </message>
    <message>
        <source>Circular</source>
        <translation>圆形</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>查看</translation>
    </message>
    <message>
        <source>Flat</source>
        <translation>平面</translation>
    </message>
    <message>
        <source>Raised</source>
        <translation>凸起</translation>
    </message>
    <message>
        <source>Sunken</source>
        <translation>凹陷</translation>
    </message>
    <message>
        <source>Customized colors</source>
        <translation>定制颜色</translation>
    </message>
    <message>
        <source>The colors will be those of data</source>
        <translation>颜色从这些数据表示</translation>
    </message>
    <message>
        <source>Text color of valid value</source>
        <translation>有效值的文本颜色</translation>
    </message>
    <message>
        <source>Text color of not valid value</source>
        <translation>无效值的文本颜色</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>背景色</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show unit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplaySimpleSetup</name>
    <message>
        <source>Simple Display Settings</source>
        <translation>简单显示设置</translation>
    </message>
    <message>
        <source>&amp;General</source>
        <translation>&amp;常规</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>&amp;Style</source>
        <translation>&amp;风格</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation>颜色</translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation>&amp;数据</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayStyle</name>
    <message>
        <source>Color %1</source>
        <translation>颜色%1</translation>
    </message>
</context>
<context>
    <name>CYDisplayStyleSetup</name>
    <message>
        <source>Global Style Settings</source>
        <translation>整体风格设置</translation>
    </message>
    <message>
        <source>&amp;Apply to current analyse sheet</source>
        <translation>&amp;应用到当前分析表格</translation>
    </message>
    <message>
        <source>Display Style</source>
        <translation>显示风格</translation>
    </message>
    <message>
        <source>&amp;Font Size</source>
        <translation>&amp;字体大小</translation>
    </message>
    <message>
        <source>&amp;Grid Color</source>
        <translation>&amp;网格颜色</translation>
    </message>
    <message>
        <source>A&amp;larm Color</source>
        <translation>警报颜色</translation>
    </message>
    <message>
        <source>Foreground Color &amp;1</source>
        <translation>前景颜色 &amp;1</translation>
    </message>
    <message>
        <source>Foreground Color &amp;2</source>
        <translation>Foreground Color &amp;2</translation>
    </message>
    <message>
        <source>Datas Colors</source>
        <translation>数据颜色</translation>
    </message>
    <message>
        <source>Change Color</source>
        <translation>更换颜色</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Background Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYDisplayTimerSetup</name>
    <message>
        <source>Timer Settings</source>
        <translation>计时器设置</translation>
    </message>
    <message>
        <source>Update Interval</source>
        <translation>更新间距</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEvent</name>
    <message>
        <source>Event</source>
        <translation>项目</translation>
    </message>
    <message>
        <source>End</source>
        <translation>结束</translation>
    </message>
    <message>
        <source>Acknow.</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation>操作员</translation>
    </message>
    <message>
        <source>User</source>
        <translation>用户</translation>
    </message>
</context>
<context>
    <name>CYEventsBackup</name>
    <message>
        <source>Backup date</source>
        <translation>备份资料</translation>
    </message>
    <message>
        <source>Select a date lower than today&apos;s.</source>
        <translation>选择一个迟于今日的日期。</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsGenerator</name>
    <message>
        <source>Events</source>
        <translation>活动</translation>
    </message>
</context>
<context>
    <name>CYEventsGeneratorSPV</name>
    <message>
        <source>Supervisor Events</source>
        <translation>监督程序事件</translation>
    </message>
    <message>
        <source>Initializing supervisor events...</source>
        <translation>正在初始化监督程序事件...</translation>
    </message>
    <message>
        <source>Supervisor starting</source>
        <translation>管理器启动</translation>
    </message>
    <message>
        <source>Starting the cylix software.</source>
        <translation type="vanished">正在启动 cylix软件。</translation>
    </message>
    <message>
        <source>Supervisor stoping</source>
        <translation>管理器停止</translation>
    </message>
    <message>
        <source>Stopping the cylix software.</source>
        <translation type="vanished">正在关闭cylix软件。</translation>
    </message>
    <message>
        <source>New calibration</source>
        <translation>新校准</translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation>度量</translation>
    </message>
    <message>
        <source>Sensor calibration by the metrology tool.</source>
        <translation>通过计量工具进行传感器校准。</translation>
    </message>
    <message>
        <source>New archive: DATA_REC1</source>
        <translation>新的归档文件： DATA_REC1</translation>
    </message>
    <message>
        <source>New archive generated by the acquisition tool.</source>
        <translation type="vanished">通过采集工具生产新的归档文件。</translation>
    </message>
    <message>
        <source>RS422 communication module not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The RS422 communication module has not been correctly installed. Reinstall Cylix.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ethernet interprocess communication not installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Ethernet interprocess communication software was not detected at startup Cylix. Reinstall Cylix.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ethernet interprocess communication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cylix cannot communicate with the Ethernet interprocess communication software. Restart PC.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time stamp of starting the cylix software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time stamp of stopping the cylix software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time stamp of new archive generated by the acquisition tool.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please note that under Windows you have to install Cylix as administrator even if the current Windows session is already in administrator mode. To do this, right-click on the installation file to access this command.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notifies the sending of the project to the numerical regulator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This sending is also done automatically when the communication is detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot create file : DATA_REC1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check access rights to the acquisition directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="unfinished">警报</translation>
    </message>
</context>
<context>
    <name>CYEventsList</name>
    <message>
        <source>One or more data (measurements, setpoints, parameters...) allow to visualize directly values which can influence the event triggering. These values, in bold, are refreshed when the help is displayed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>One or more hyperlinks allow to open the Cylix manual directly at a precise place, as for example the part relating to the setting which can influence the event triggering.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>These links, which are only visible in the tooltip, can be activated at the bottom of the window of events logs.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsMailingWidget</name>
    <message>
        <source>&amp;Test</source>
        <translation>&amp;测试</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>You must be in administrator access to modifie this values</source>
        <translation>您必须以管理员身份访问并修改该值。</translation>
    </message>
    <message>
        <source>SMTP servor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsManager</name>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>无法打开文件%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML</source>
        <translation>该文件%1不包含有效的XML</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">不能保存文件%1!</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">名字</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">不能保存文件%1</translation>
    </message>
    <message>
        <source>Cannot find the events generator %1</source>
        <translation>找不到事件生成器%1</translation>
    </message>
    <message>
        <source>Events list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name, message and help</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsPreferences</name>
    <message>
        <source>Events settings</source>
        <translation>时间设置</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>Mail</source>
        <translation>邮件</translation>
    </message>
    <message>
        <source>&amp;Designer Value</source>
        <translation>&amp;设计者评价</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsReport</name>
    <message>
        <source> COLOR	TIME	SINCE	TYPE	FROM	DESCRIPTION	NAME
</source>
        <translation> COLOR	TIME	SINCE	TYPE	FROM	DESCRIPTION	NAME
</translation>
    </message>
    <message>
        <source>Events report (%1) of %2 since %3

</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYEventsView</name>
    <message>
        <source>Date and hour</source>
        <translation>日期和时间</translation>
    </message>
    <message>
        <source>Events file</source>
        <translation>事件文件</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>Since</source>
        <translation>自</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>From</source>
        <translation>从</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <source>The test directory of this events file has been moved or removed.</source>
        <translation>该事件文件实验目录已被移动或移除</translation>
    </message>
    <message>
        <source>This events file has been moved or removed.</source>
        <translation>该事件文件已被移动或移除</translation>
    </message>
    <message>
        <source>Select a events report to load</source>
        <translation>选择要加载的事件报告</translation>
    </message>
    <message>
        <source>The current events file changed!
Do you want to view this one?</source>
        <translation>当前事件文件发送更改！ 
您想要查看此更改项吗？</translation>
    </message>
    <message>
        <source>No help provided !</source>
        <translation>不提供帮助</translation>
    </message>
</context>
<context>
    <name>CYEventsWindow</name>
    <message>
        <source>Events reports</source>
        <translation>事件报告</translation>
    </message>
    <message>
        <source>Settings events...</source>
        <translation>设置事件......</translation>
    </message>
    <message>
        <source>&amp;Current</source>
        <translation>&amp;当前</translation>
    </message>
    <message>
        <source>&amp;Backup</source>
        <translation>&amp;备份</translation>
    </message>
    <message>
        <source>All</source>
        <translation>所有</translation>
    </message>
    <message>
        <source>&amp;None</source>
        <translation>&amp;无</translation>
    </message>
    <message>
        <source>Their end</source>
        <translation>结果</translation>
    </message>
    <message>
        <source>Their acknowledgement</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open &amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;From</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYExtMeasure</name>
    <message>
        <source>Unit</source>
        <translation>单元</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <source>Scaling</source>
        <translation>缩放</translation>
    </message>
    <message>
        <source>High value</source>
        <translation>高值</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation>低值</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation>监督</translation>
    </message>
    <message>
        <source>Level</source>
        <translation>等级</translation>
    </message>
    <message>
        <source>Alert tolerance interval</source>
        <translation>警报容差区间</translation>
    </message>
    <message>
        <source>Fault tolerance interval</source>
        <translation>故障容差区间</translation>
    </message>
    <message>
        <source>Calibration type</source>
        <translation>校准类型</translation>
    </message>
</context>
<context>
    <name>CYExtMeasureEdit</name>
    <message>
        <source>#</source>
        <translation type="vanished">#</translation>
    </message>
    <message>
        <source>Scale adjust points</source>
        <translation>比例调整点</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>低</translation>
    </message>
    <message>
        <source>High</source>
        <translation>高</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation>监督</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>格式</translation>
    </message>
    <message>
        <source>± </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYFileCSV</name>
    <message>
        <source>Maximum of lines per file</source>
        <translation>单文件最大行数</translation>
    </message>
    <message>
        <source>Maximum file size</source>
        <translation>最大文件规格</translation>
    </message>
</context>
<context>
    <name>CYFlag</name>
    <message>
        <source>If it is:</source>
        <translation>如果它是：</translation>
    </message>
    <message>
        <source>If you select:</source>
        <translation>如果您选择：</translation>
    </message>
    <message>
        <source>Designer value:</source>
        <translation>设计者价值</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>二进制</translation>
    </message>
</context>
<context>
    <name>CYFlagDialog</name>
    <message>
        <source>Dialog input of flag data</source>
        <translation>对话框中输入标记数据</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYFlagInput</name>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYG7Cnt</name>
    <message>
        <source>Counter %1 of grafcet %2</source>
        <translation>grafcet %2计算器 %1</translation>
    </message>
    <message>
        <source>Grafcet counter</source>
        <translation>Grafcet 计数器</translation>
    </message>
</context>
<context>
    <name>CYG7Tim</name>
    <message>
        <source>Timer %1 of grafcet %2</source>
        <translation>grafcet %2计时器 %1</translation>
    </message>
    <message>
        <source>Grafcet timer</source>
        <translation>Grafcet计时器</translation>
    </message>
</context>
<context>
    <name>CYImageList</name>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>无法打开文件%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>文件 %1 不含有效 XML 
%2: 行:%3 列: %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">不能保存文件%1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">不能保存文件%1</translation>
    </message>
</context>
<context>
    <name>CYImageListEdit</name>
    <message>
        <source>N°</source>
        <translation>N°</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>图像</translation>
    </message>
    <message>
        <source>Fichier</source>
        <translation>文件</translation>
    </message>
    <message>
        <source>Dele&amp;te</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Canc&amp;el</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Choose an image to add into this list.</source>
        <translation>选择一个图像并添加到此表中。</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYListCali</name>
    <message>
        <source>Inputs / Outputs Calibration</source>
        <translation>输入/输出校准</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>Ana.</source>
        <translation>Ana.</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMV</name>
    <message>
        <source>Forward</source>
        <translation>前进</translation>
    </message>
    <message>
        <source>Backward</source>
        <translation>后退</translation>
    </message>
    <message>
        <source>Sensor loss timer %1</source>
        <translation>传感器损失计时器 %1</translation>
    </message>
    <message>
        <source>Maximum delay before the falling edge of the backward sensor.</source>
        <translation>后退传感器下降沿之前的最大延迟。</translation>
    </message>
    <message>
        <source>If this time is reached, a failure is signalled.</source>
        <translation>如果达到该时间，则用信用表示故障。</translation>
    </message>
    <message>
        <source>Sensor appearence timer %1</source>
        <translation>传感器显现计时器%1</translation>
    </message>
    <message>
        <source>Maximum delay before the rising edge of the forward sensor.</source>
        <translation>前进传感器上升沿之前的最大延迟。</translation>
    </message>
    <message>
        <source>Maximum delay before the falling edge of the forward sensor.</source>
        <translation>前进传感器下降沿之前的最大延迟。</translation>
    </message>
    <message>
        <source>Maximum delay before the rising edge of the backward sensor.</source>
        <translation>后退传感器上升沿之前的最大延迟。</translation>
    </message>
    <message>
        <source>Sensor failure signalisation %1</source>
        <translation>传感器故障信号通知 %1</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation>故障</translation>
    </message>
    <message>
        <source>A faillure generates a fault.</source>
        <translation>故障导致错误。</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>警报</translation>
    </message>
    <message>
        <source>A faillure generates an alert and then the timers simulate the sensors.</source>
        <translation>故障引发警报，然后计时器模拟传感器。</translation>
    </message>
    <message>
        <source>Initialized movement</source>
        <translation>初始化移动</translation>
    </message>
    <message>
        <source>Positioning</source>
        <translation>定位</translation>
    </message>
    <message>
        <source>Grafcet current state</source>
        <translation>Grafcet 当前状态</translation>
    </message>
    <message>
        <source>No sensor presence %1</source>
        <translation>不存在传感器 %1</translation>
    </message>
    <message>
        <source>Process</source>
        <translation>程序</translation>
    </message>
    <message>
        <source>The maximum waiting time of rising edge of the sensor of end of forward stroke has been exceeded by the movement forward.</source>
        <translation>在返回冲程的最后阶段，由于向前移动，导致传感器上升边的最大等待时间已经超出。</translation>
    </message>
    <message>
        <source>This may indicate a sensor failure or movement.</source>
        <translation>可能表示为传感器故障或移动。</translation>
    </message>
    <message>
        <source>No sensor loss %1</source>
        <translation>无传感器损失 %1</translation>
    </message>
    <message>
        <source>The maximum waiting time of falling edge of the sensor of end of forward stroke has been exceeded by the movement backward.</source>
        <translation>在前进冲程的最后阶段，由于向后移动，导致传感器下降沿的最大等待时间已经超出。</translation>
    </message>
    <message>
        <source>Sensor loss %1</source>
        <translation>传感器损失 %1</translation>
    </message>
    <message>
        <source>The forward sensor was lost in forward static.</source>
        <translation>前进传感器在静态前进中丢失。</translation>
    </message>
    <message>
        <source>This may indicate an adjustment defect of the sensor.</source>
        <translation>可表示为传感器的调节缺陷。</translation>
    </message>
    <message>
        <source>The maximum waiting time of rising edge of the sensor of end of backward stroke has been exceeded by the movement backward.</source>
        <translation>在返回冲程的最后阶段，由于向后移动，导致传感器上升边的最大等待时间已经超出。</translation>
    </message>
    <message>
        <source>The maximum waiting time of falling edge of the sensor of end of backward stroke has been exceeded by the movement forward.</source>
        <translation>在返回冲程的最后阶段，由于向前移动，导致传感器上升边的最大等待时间已经超出。</translation>
    </message>
    <message>
        <source>The backward sensor was lost in backward static.</source>
        <translation>后退传感器在静态后退中丢失。</translation>
    </message>
    <message>
        <source>Sensor coherence %1/%2</source>
        <translation>传感器相关性 %1/ %2</translation>
    </message>
    <message>
        <source>The sensors forward and backward were actived at the same time.</source>
        <translation>前进和后退传感器同时激活。</translation>
    </message>
    <message>
        <source>This may indicate an adjustment defect of the sensors.</source>
        <translation>可表示为传感器的调节缺陷。</translation>
    </message>
</context>
<context>
    <name>CYMVInput</name>
    <message>
        <source>Forward motion</source>
        <translation>前进</translation>
    </message>
    <message>
        <source>Backward motion</source>
        <translation>倒退</translation>
    </message>
</context>
<context>
    <name>CYMainWin</name>
    <message>
        <source>Test: %1 (Access: %2)</source>
        <translation>测试: %1 (通道: %2)</translation>
    </message>
    <message>
        <source>Languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Test: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The new language will be taken into account the next time the application will be started.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYManuCali</name>
    <message>
        <source>&amp;Designer value</source>
        <translation>&amp;设计着值</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>&amp;Low value</source>
        <translation>&amp;低值</translation>
    </message>
    <message>
        <source>&amp;High value</source>
        <translation>&amp;高值</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>High</source>
        <translation>高</translation>
    </message>
    <message>
        <source>High Value</source>
        <translation>高值</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>低</translation>
    </message>
    <message>
        <source>Low Value</source>
        <translation>低值</translation>
    </message>
    <message>
        <source>Do you really want to load the designer&apos;s values ?</source>
        <translation>你确定想要加载该设计者的数值？</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYManuCaliInput</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMeasure</name>
    <message>
        <source>Flag</source>
        <translation>标记</translation>
    </message>
    <message>
        <source>Calibration</source>
        <translation>校准</translation>
    </message>
    <message>
        <source>Indicates that the date of calibration of the sensor is exceeded.</source>
        <translation>表示已超过传感器的校准日期。</translation>
    </message>
    <message>
        <source>Sensor to calibrate</source>
        <translation>传感器校准</translation>
    </message>
    <message>
        <source>Indicates that the sensor was calibrated to the onset of this event.</source>
        <translation>表示在该时间发生时已经校准传感器。</translation>
    </message>
    <message>
        <source>Sensor calibrated</source>
        <translation>校准的传感器</translation>
    </message>
    <message>
        <source>Flag of validity of the calibration</source>
        <translation>校准有效期标记</translation>
    </message>
    <message>
        <source>High value</source>
        <translation>高值</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation>低值</translation>
    </message>
    <message>
        <source>Date of last calibration</source>
        <translation>最后校准日期</translation>
    </message>
    <message>
        <source>Date of last verification</source>
        <translation>最后验证日期</translation>
    </message>
    <message>
        <source>Date of next calibration</source>
        <translation>下次校准日期</translation>
    </message>
    <message>
        <source>If this date is expired an alert will be generated to remind to calibrate the sensor %1.</source>
        <translation>该日期到期时，将生成警报，提醒校准传感器%1。</translation>
    </message>
    <message>
        <source>Date of next verification</source>
        <translation>下次验证日期</translation>
    </message>
    <message>
        <source>Calibration control</source>
        <translation>校准控制</translation>
    </message>
    <message>
        <source>Alert for next verification</source>
        <translation>下次验证警报</translation>
    </message>
    <message>
        <source>Generates an alert in case of overtaking of the date of next verification.</source>
        <translation>超过下次验证日期时将生成警报。</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation>度量</translation>
    </message>
    <message>
        <source>Number of points used for calibration</source>
        <translation>校准使用点数</translation>
    </message>
    <message>
        <source>Direct bench sensor</source>
        <translation>直接试验台传感器</translation>
    </message>
    <message>
        <source>New point</source>
        <translation>新 点</translation>
    </message>
    <message>
        <source>Bench sensor</source>
        <translation>试验台传感器</translation>
    </message>
    <message>
        <source>Reference sensor</source>
        <translation>参考传感器</translation>
    </message>
    <message>
        <source>Direct bench values</source>
        <translation>直接试验台数值</translation>
    </message>
    <message>
        <source>Point %1</source>
        <translation>点%1</translation>
    </message>
    <message>
        <source>Bench values</source>
        <translation>试验台数值</translation>
    </message>
    <message>
        <source>Reference values</source>
        <translation>参考数值</translation>
    </message>
    <message>
        <source>Averaging times</source>
        <translation>平均时间</translation>
    </message>
    <message>
        <source>Calibrating mode</source>
        <translation>校准模式</translation>
    </message>
    <message>
        <source>Manual calibrating</source>
        <translation>手动校准</translation>
    </message>
    <message>
        <source>Automatic calibrating</source>
        <translation>自动校准</translation>
    </message>
    <message>
        <source>Adding a calibration point by reading the average of the direct value of the sensor.</source>
        <translation>通过读取传感器直接数值的平均值，添加一个校准点。</translation>
    </message>
    <message>
        <source>Adding a calibration point by entering the direct value of the sensor.</source>
        <translation>通过输入传感器直接数值增加一个校准点。</translation>
    </message>
    <message>
        <source>Manufacter</source>
        <translation>Manufacter</translation>
    </message>
    <message>
        <source>P/N (Type)</source>
        <translation>P/N（类型）</translation>
    </message>
    <message>
        <source>S/N (Serial N)</source>
        <translation>S/N (序号)</translation>
    </message>
    <message>
        <source>Uncertainty type</source>
        <translation>不确定性类型</translation>
    </message>
    <message>
        <source>Uncertainty - % FS</source>
        <translation>不确定性 - % FS</translation>
    </message>
    <message>
        <source>The total measurement uncertainty of the sensor is expressed as a percentage of the Full Scale of the sensor. It is then fixed for each measurement and can also be expressed in sensor unit.</source>
        <translation>传感器总测量不确定度表示为传感器的满刻度百分比。然后将其固定为每个测量，亦可表示在传感器单元。</translation>
    </message>
    <message>
        <source>Uncertainty - fixed value</source>
        <translation>不确定性-固定值</translation>
    </message>
    <message>
        <source>The total measurement uncertainty of the sensor is expressed in sensor unit. It is then fixed for each measurement and can also be expressed as a percentage of the Full Scale of the sensor.</source>
        <translation>传感器总测量不确定度表示在传感器单元。然后将其固定为每个测量，亦可表示为传感器的满刻度百分比。</translation>
    </message>
    <message>
        <source>Uncertainty - % of reading</source>
        <translation>不确定性 - % of 读取</translation>
    </message>
    <message>
        <source>The total measurement uncertainty of the sensor is expressed as a percentage of the read measure.</source>
        <translation>传感器总测量不确定度以测量读取值的百分比表示。</translation>
    </message>
    <message>
        <source>Environment</source>
        <translation>环境</translation>
    </message>
    <message>
        <source>month(s)</source>
        <translation>月份</translation>
    </message>
    <message>
        <source>Periodicity of control</source>
        <translation>定期控制</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation>操作员</translation>
    </message>
    <message>
        <source>Equipment reference</source>
        <translation>环境参考</translation>
    </message>
    <message>
        <source>Designer values</source>
        <translation>设计者数值</translation>
    </message>
    <message>
        <source>Procedure</source>
        <translation>程序</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>评论</translation>
    </message>
    <message>
        <source>Report file</source>
        <translation>报告文件</translation>
    </message>
    <message>
        <source>Event</source>
        <translation>项目</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>警报</translation>
    </message>
    <message>
        <source>Numeric</source>
        <translation>数字</translation>
    </message>
    <message>
        <source>Sensor sheet</source>
        <translation>传感器表</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>High value (FS)</source>
        <translation>高值 (FS)</translation>
    </message>
    <message>
        <source>Low value (FS)</source>
        <translation>低值（FS）</translation>
    </message>
    <message>
        <source>Verification in accordance</source>
        <translation>一致性验证</translation>
    </message>
    <message>
        <source>Verification not in accordance</source>
        <translation>不一致性验证</translation>
    </message>
    <message>
        <source>Summary</source>
        <translation>概况</translation>
    </message>
    <message>
        <source>Last calibration</source>
        <translation>最后校准</translation>
    </message>
    <message>
        <source>Last verification</source>
        <translation>上次校准</translation>
    </message>
    <message>
        <source>Next verification</source>
        <translation>下次校准</translation>
    </message>
    <message>
        <source>Calibrating</source>
        <translation>校准</translation>
    </message>
    <message>
        <source>Editing</source>
        <translation>编辑</translation>
    </message>
    <message>
        <source>Viewing</source>
        <translation>查看</translation>
    </message>
    <message>
        <source>No calibration</source>
        <translation>不校准</translation>
    </message>
    <message>
        <source>Sensor defect</source>
        <translation type="unfinished">传感器损坏</translation>
    </message>
    <message>
        <source>High value (not calibrated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum value of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low value (not calibrated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum value of device usage range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum number of ADC points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum number of ADC points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Measure</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMeasureSetting</name>
    <message>
        <source>Measurement setting</source>
        <translation>测量设置</translation>
    </message>
    <message>
        <source>Calibration type</source>
        <translation type="vanished">校准类型</translation>
    </message>
    <message>
        <source>Indicates that the type or scale of the sensor have been changed. So, it&apos;s recommended to make a new calibrate of sensor.</source>
        <translation>表明传感器的范围或者类型被改变，所以，建议重新校准传感器</translation>
    </message>
    <message>
        <source>%1: Sensor changed</source>
        <translation>%1: 传感器变更</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation>警报</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation>单元</translation>
    </message>
    <message>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <source>Scaling</source>
        <translation>缩放</translation>
    </message>
    <message>
        <source>High value</source>
        <translation>高值</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation>低值</translation>
    </message>
    <message>
        <source>Signal type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMessageBox</name>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;是</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C&amp;ontinue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMetroCaliInput</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source>Direct</source>
        <translation>直接</translation>
    </message>
    <message>
        <source>Reference sensor</source>
        <translation>参考传感器</translation>
    </message>
    <message>
        <source>Bench sensor</source>
        <translation>试验台传感器</translation>
    </message>
    <message>
        <source>Average of bench sensor</source>
        <translation>试验台传感器平均</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Calibrating point &apos;%1&apos;</source>
        <translation>校准点 &apos;%1&apos;</translation>
    </message>
    <message>
        <source>%1 : New point</source>
        <translation>%1 : 新 点</translation>
    </message>
    <message>
        <source>ADC</source>
        <translation>ADC</translation>
    </message>
</context>
<context>
    <name>CYMetroCalibrate</name>
    <message>
        <source>Infos</source>
        <translation>信息</translation>
    </message>
    <message>
        <source>#</source>
        <translation type="vanished">#</translation>
    </message>
    <message>
        <source>Curves</source>
        <translation>曲线</translation>
    </message>
    <message>
        <source>Points</source>
        <translation>点</translation>
    </message>
    <message>
        <source>Mini
(%1)</source>
        <translation>最小
(%1)</translation>
    </message>
    <message>
        <source>Standard
(%1)</source>
        <translation>标准
(%1)</translation>
    </message>
    <message>
        <source>Maxi
(%1)</source>
        <translation>最大
(%1)</translation>
    </message>
    <message>
        <source>Bench
(%1)</source>
        <translation>试验台
(%1)</translation>
    </message>
    <message>
        <source>Error
(%1)</source>
        <translation>错误
(%1)</translation>
    </message>
    <message>
        <source>Error
(% FS)</source>
        <translation>错误
(% FS)</translation>
    </message>
    <message>
        <source>Bench
(ADC)</source>
        <translation>试验台
(ADC)</translation>
    </message>
    <message>
        <source>Straight
Line
 (%1)</source>
        <translation>直
线
 (%1)</translation>
    </message>
    <message>
        <source>Linearity
error
(% FS)</source>
        <translation>线性
错误
(% FS)</translation>
    </message>
    <message>
        <source>Averaging
time</source>
        <translation>平均
时间</translation>
    </message>
    <message>
        <source>(sec)</source>
        <translation>(秒)</translation>
    </message>
    <message>
        <source>Gross
(%1)</source>
        <translation>粗
(%1)</translation>
    </message>
    <message>
        <source>Calibrating sheet &apos;%1&apos; of %2 (Modified)</source>
        <translation>校准表 &apos;%1&apos; of %2 (已修改)</translation>
    </message>
    <message>
        <source>Do you want to validate this calibration for %1 ?</source>
        <translation>你确认确定 %1 本次校准？</translation>
    </message>
    <message>
        <source>End of verification de %1 !
In accordance ?</source>
        <translation>验证完毕 %1 !
一致?</translation>
    </message>
    <message>
        <source>Do you want to close the calibration sheet of %1 without saving the latest changes ?</source>
        <translation>你确认在不保存最新更改的情况下关闭%1的校准表？</translation>
    </message>
    <message>
        <source>Mini uncertainty</source>
        <translation>最小不确定性</translation>
    </message>
    <message>
        <source>Maxi uncertainty</source>
        <translation>最大不确定性</translation>
    </message>
    <message>
        <source>Linearity error (%)</source>
        <translation>线性错误 (%)</translation>
    </message>
    <message>
        <source>Error (% FS)</source>
        <translation>错误 (% FS)</translation>
    </message>
    <message>
        <source>Linearity error (% FS)</source>
        <translation>线性错误 (% FS)</translation>
    </message>
    <message>
        <source>%1_%2_%3</source>
        <translation>%1_%2_%3</translation>
    </message>
    <message>
        <source>_calibrating</source>
        <translation>_校准</translation>
    </message>
    <message>
        <source>_verification</source>
        <translation>_验证</translation>
    </message>
    <message>
        <source>%1/%2</source>
        <translation>%1/%2</translation>
    </message>
    <message>
        <source>%1/%2.pdf</source>
        <translation>%1/%2.pdf</translation>
    </message>
    <message>
        <source>Page %1/%2</source>
        <translation>页 %1/%2</translation>
    </message>
    <message>
        <source>Page %1/2</source>
        <translation>页 %1/2</translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMetroSensorSheet</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source>#</source>
        <translation type="vanished">#</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Edit sensor sheet &apos;%1&apos;</source>
        <translation>编辑传感器表格 &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Are you sure to validate this sensor sheet ?
If yes, it is recommended to redo the calibration !</source>
        <translation>你确认要确定该传感器表格?
如是，推荐重做校准！</translation>
    </message>
    <message>
        <source>Change sensor sheet &apos;%1&apos;</source>
        <translation>更换传感器表 &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>CYMetroSensorSheetView</name>
    <message>
        <source>Sensor sheet</source>
        <translation>传感器表</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>High point</source>
        <translation>高点</translation>
    </message>
    <message>
        <source>Low point</source>
        <translation>低点</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
</context>
<context>
    <name>CYMetroSettings</name>
    <message>
        <source>Metrology settings</source>
        <translation>度量设置</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source>Designer &amp;Value</source>
        <translation>设计&amp;值</translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Clo&amp;se</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMetroWin</name>
    <message>
        <source>Metrology tool</source>
        <translation>度量工具</translation>
    </message>
    <message>
        <source>With alert</source>
        <translation>带警报</translation>
    </message>
    <message>
        <source>Without alert</source>
        <translation>不带警报</translation>
    </message>
    <message>
        <source>All</source>
        <translation>所有</translation>
    </message>
    <message>
        <source>Sensors</source>
        <translation>传感器</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>Sensor sheet</source>
        <translation>传感器表</translation>
    </message>
    <message>
        <source>Clear table</source>
        <translation>清理表格</translation>
    </message>
    <message>
        <source>Add point</source>
        <translation>添加点</translation>
    </message>
    <message>
        <source>Delete point</source>
        <translation>删除点</translation>
    </message>
    <message>
        <source>Calibrate</source>
        <translation>校准</translation>
    </message>
    <message>
        <source>Verification</source>
        <translation>验证</translation>
    </message>
    <message>
        <source>Designer values</source>
        <translation>设计者数值</translation>
    </message>
    <message>
        <source>Metrology settings</source>
        <translation>度量设置</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMultimeter</name>
    <message>
        <source>Multimeter</source>
        <translation>万用表</translation>
    </message>
    <message>
        <source>Edit the data&apos;s label to change the title</source>
        <translation>编辑数据标签，更改标题</translation>
    </message>
    <message>
        <source>Analog display</source>
        <translation>模拟显示</translation>
    </message>
    <message>
        <source>Digital display</source>
        <translation>数字显示</translation>
    </message>
    <message>
        <source>Numerical base</source>
        <translation>数值基础</translation>
    </message>
    <message>
        <source>The numerical base is used only if the data is only an unsigned integer type!</source>
        <translation>当数据属于无符号的整数类型时才使用数值基础。</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>自动</translation>
    </message>
    <message>
        <source>Binary</source>
        <translation>二进制</translation>
    </message>
    <message>
        <source>Decimal</source>
        <translation>十进制</translation>
    </message>
    <message>
        <source>Hexadecimal</source>
        <translation>十六进制</translation>
    </message>
    <message>
        <source>Alarm</source>
        <translation>警报</translation>
    </message>
    <message>
        <source>Upper bound value</source>
        <translation>上界值</translation>
    </message>
    <message>
        <source>Upper alarm enable</source>
        <translation>启用上部报警</translation>
    </message>
    <message>
        <source>Lower bound value</source>
        <translation>下界值</translation>
    </message>
    <message>
        <source>Lower alarm enable</source>
        <translation>启用下端报警</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>风格</translation>
    </message>
    <message>
        <source>Analog</source>
        <translation>模拟</translation>
    </message>
    <message>
        <source>Normal color</source>
        <translation>标准颜色</translation>
    </message>
    <message>
        <source>Alarm color</source>
        <translation>报警颜色</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>背景色</translation>
    </message>
    <message>
        <source>Text color</source>
        <translation>文本颜色</translation>
    </message>
    <message>
        <source>Digital</source>
        <translation>数字化</translation>
    </message>
    <message>
        <source>Octal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYMultimeterSetup</name>
    <message>
        <source>Multimeter Settings</source>
        <translation>万用表设置</translation>
    </message>
    <message>
        <source>&amp;General</source>
        <translation>&amp;常规</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>&amp;Style</source>
        <translation>&amp;风格</translation>
    </message>
    <message>
        <source>Analog</source>
        <translation>模拟</translation>
    </message>
    <message>
        <source>Digital</source>
        <translation>数字化</translation>
    </message>
    <message>
        <source>&amp;Data</source>
        <translation>&amp;数据</translation>
    </message>
    <message>
        <source>Analog scale</source>
        <translation>类比标尺</translation>
    </message>
    <message>
        <source>=</source>
        <translation>=</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYNetConnect</name>
    <message>
        <source>Connect Host</source>
        <translation>主机连接</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <source>Select the name of the host you want to connect to.</source>
        <translation>选择您需要连接的主机名称</translation>
    </message>
    <message>
        <source>Co&amp;nnect</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Configure connections</source>
        <translation>配置连接</translation>
    </message>
    <message>
        <source>COM</source>
        <translation>COM</translation>
    </message>
    <message>
        <source>State</source>
        <translation>状态</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYNetLink</name>
    <message>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation>已断开连接</translation>
    </message>
    <message>
        <source>%1 kHz</source>
        <translation>%1 kHz</translation>
    </message>
    <message>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
</context>
<context>
    <name>CYNetR422F</name>
    <message>
        <source>File exchange with regulator</source>
        <translation>与校准器文件交换</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Doesn&apos;t exist &apos;%1&apos; !</source>
        <translation>不存在 &apos;%1&apos; ！</translation>
    </message>
    <message>
        <source>Updating of regulator</source>
        <translation>调节器更新</translation>
    </message>
    <message>
        <source>Are you sure to update this regulator !</source>
        <translation>您确实要更新该调节器吗！</translation>
    </message>
    <message>
        <source>Its serial number must be %1, while the software you want install has a as serial number %2 !</source>
        <translation>其序列号必须为 %1，其中，待安装软件的序列号为 %2 ！</translation>
    </message>
    <message>
        <source>Update of regulator from the version %1 to the version %2. </source>
        <translation>将调节器从%1版本升级到%2版。</translation>
    </message>
    <message>
        <source>Are you sure to install on the regulator the version %1 of the software %2 !</source>
        <translation>您确定将调节器从%1版本升级到%2版吗？</translation>
    </message>
    <message>
        <source>Install the content of this archive on this regulator !</source>
        <translation>安装调节器上的归档文件内容！</translation>
    </message>
    <message>
        <source>Init file doesn&apos;t exist !</source>
        <translation>初始文件不存在！</translation>
    </message>
    <message>
        <source>Section %1 unknown in the init file !</source>
        <translation>初始文件中的%1节未知！</translation>
    </message>
    <message>
        <source>Cannot open the init file !</source>
        <translation>无法打开该初始文件 ！</translation>
    </message>
    <message>
        <source>The destination directory path &apos;%1&apos; is too long !</source>
        <translation>目标目录路径 &apos;%1&apos; 过长!</translation>
    </message>
    <message>
        <source>The source directory path &apos;%1&apos; is too long !</source>
        <translation>源目录路径 &apos;%1&apos; 过长!</translation>
    </message>
    <message>
        <source>The file name &apos;%1&apos; is not correct for transfert file !</source>
        <translation>传输文件的文件名 &apos;%1&apos; 不正确！</translation>
    </message>
    <message>
        <source>The file name &apos;%1&apos; is too long !</source>
        <translation>文件名 &apos;%1&apos; 过长!</translation>
    </message>
    <message>
        <source>&apos;%1&apos; doesn&apos;t exist !</source>
        <translation>&apos;%1&apos; 不存在 ！</translation>
    </message>
    <message>
        <source>Cannot open the file &apos;%1&apos; !</source>
        <translation>无法打开文件 &apos;%1&apos; ！</translation>
    </message>
    <message>
        <source>Install %1 in %2</source>
        <translation>在 %2 中安装 %1</translation>
    </message>
    <message>
        <source>Save %1 in %2</source>
        <translation>将%1保存在%2</translation>
    </message>
    <message>
        <source>Regulator is blocked in file transfert mode !</source>
        <translation>在文件传输模式下，调节器出现阻碍！</translation>
    </message>
    <message>
        <source>YOU MUST REBOOT MANUALLY THE REGULATOR !</source>
        <translation>您必须手动重新启动调节器 ！</translation>
    </message>
    <message>
        <source>Regulator ok !</source>
        <translation>调节器OK！</translation>
    </message>
    <message>
        <source>Stop of communication driver in normal mode.</source>
        <translation>在常规模式下，停止通讯驱动程序。</translation>
    </message>
    <message>
        <source>Stop of communication driver in file transfert mode.</source>
        <translation>在文件传输模式下，停止通讯驱动程序！</translation>
    </message>
    <message>
        <source>Regulator disconnected.</source>
        <translation>调节器已断开连接。</translation>
    </message>
    <message>
        <source>Cannot reboot the regulator !</source>
        <translation>无法重启调节器！</translation>
    </message>
    <message>
        <source>Waiting for disconnection (Maximum time %1 sec)...</source>
        <translation>正在等待断开连接（最长时间 %1秒）......</translation>
    </message>
    <message>
        <source>Restart of communication driver in file transfert mode.</source>
        <translation>在文件传输模式下，停止通讯驱动程序。</translation>
    </message>
    <message>
        <source>Restart of communication driver in normal mode.</source>
        <translation>在常规模式下，重启通讯驱动程序。</translation>
    </message>
    <message>
        <source>Cannot read info datas from regulator !</source>
        <translation>无法从调节器中读取信息数据 ！</translation>
    </message>
    <message>
        <source>Regulator reconnected in normal mode instead of file transfert mode !</source>
        <translation>调节器在常规模式，不是文件传输模式下重新连接！</translation>
    </message>
    <message>
        <source>Regulator reconnected in file transfert mode (R422F %1).</source>
        <translation>调节器在文件传输模式下重新连接 （R422F %1)。</translation>
    </message>
    <message>
        <source>Waiting for reconnection (Maximum time %1 sec)...</source>
        <translation>正在等待重新连接（最长时间 %1秒）......</translation>
    </message>
    <message>
        <source>Cannot reconnect to the regulator !</source>
        <translation>无法重新连接到调节器！</translation>
    </message>
    <message>
        <source>Regulator reconnected in file transfert mode (R422F %1) instead of normal mode !</source>
        <translation>调节器在文件传输模式(R422F %1)，不是常规模式下重新连接！</translation>
    </message>
    <message>
        <source>Regulator reconnected in normal mode.</source>
        <translation>调节器在常规模式下重新连接。</translation>
    </message>
    <message>
        <source>Regulator rebooting...</source>
        <translation>调节器正在重启......</translation>
    </message>
    <message>
        <source>Starting file transfert...</source>
        <translation>正在启动文件传输......</translation>
    </message>
    <message>
        <source>No file to transfert !</source>
        <translation>没有传输文件!</translation>
    </message>
    <message>
        <source>Install : %1 (%2 %)</source>
        <translation>安装: %1 (%2 %)</translation>
    </message>
    <message>
        <source>No answer from the regulator !</source>
        <translation>调节器无回应！</translation>
    </message>
    <message>
        <source>Cannot remove file &apos;%1\%2&apos; on the regulator !</source>
        <translation>无法删除调节器上的文件&apos;%1\%2&apos; ！</translation>
    </message>
    <message>
        <source>Cannot make file &apos;%1\%2&apos; on the regulator !</source>
        <translation>无法在调节器上创建文件&apos;%1\%2&apos; ！</translation>
    </message>
    <message>
        <source>Block number error &apos;%1&apos; !</source>
        <translation>分程序编号错误&apos;%1&apos; !</translation>
    </message>
    <message>
        <source>File size error &apos;%1\%2&apos; !</source>
        <translation>文件大小错误 &apos;%1\ %2&apos;!</translation>
    </message>
    <message>
        <source>Regulator state unknown : &apos;%1&apos; !</source>
        <translation>调节器状态未知： &apos;%1&apos; ！</translation>
    </message>
    <message>
        <source>Bad size block to read !</source>
        <translation>读取规块损坏！</translation>
    </message>
    <message>
        <source>Bad num block to read !</source>
        <translation>读取数字块损坏</translation>
    </message>
    <message>
        <source>Save : %1 (%2 %)</source>
        <translation>保持 : %1 (%2 %)</translation>
    </message>
    <message>
        <source>No file name from the regulator !</source>
        <translation>调节器中没有文件名！</translation>
    </message>
    <message>
        <source>Cannot open file &apos;%1&apos; !</source>
        <translation>无法打开文件 &apos;%1&apos; ！</translation>
    </message>
    <message>
        <source>Cannot find directory &apos;%1&apos; !</source>
        <translation>无法找到目录 &apos;%1&apos;!</translation>
    </message>
    <message>
        <source>Cannot find file &apos;%1&apos; !</source>
        <translation>无法找到文件 &apos;%1&apos; !</translation>
    </message>
    <message>
        <source>End of file transfert.</source>
        <translation>文件传输结束。</translation>
    </message>
    <message>
        <source>You can&apos;t close this box before the end of the transfert !</source>
        <translation>文件传输结束前您无法关闭此框！</translation>
    </message>
    <message>
        <source>File transfert</source>
        <translation>文件传输</translation>
    </message>
    <message>
        <source>Cannot remove the old directory %1 !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot start uncompressing file of update (%1)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot finish uncompressing file of update (%1)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File %1 is empty!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYNumDialog</name>
    <message>
        <source>Dialog input of numerical data</source>
        <translation>对话框中输入数字数据</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYNumDisplay</name>
    <message>
        <source>The input text must be a numeric value !</source>
        <translation>输入文本必须是一个数字值！</translation>
    </message>
    <message>
        <source>Cannot find the data %1</source>
        <translation>无法找到数据%1</translation>
    </message>
</context>
<context>
    <name>CYNumInput</name>
    <message>
        <source>Can&apos;t represent value %1 in terms of fixed-point numbers with precision %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t represent value %1 in terms of fixed-point numbers with precision %2 %3/%4 = %5</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPID</name>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>Proportional</source>
        <translation>按比例</translation>
    </message>
    <message>
        <source>Proportional Integral</source>
        <translation>比例积分</translation>
    </message>
    <message>
        <source>Proportional Derivative</source>
        <translation>比例微分</translation>
    </message>
    <message>
        <source>Proportional Integral Derivative</source>
        <translation>比例积分微分</translation>
    </message>
    <message>
        <source>Integral</source>
        <translation>积分</translation>
    </message>
    <message>
        <source>Proportional band coefficient</source>
        <translation>比例带系统</translation>
    </message>
    <message>
        <source>Proportional coefficient</source>
        <translation>比例系统</translation>
    </message>
    <message>
        <source>If 0, the proportional control won&apos;t be active (It can work in integral mode)</source>
        <translation>如果为0，则不会激活比例控制（可在积分模式下工作）</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>该控制属于一种混合型 P.I.D。</translation>
    </message>
    <message>
        <source>Proportional band</source>
        <translation>比例带</translation>
    </message>
    <message>
        <source>Integral coefficient</source>
        <translation>积分系数</translation>
    </message>
    <message>
        <source>If 0, the integral control won&apos;t be in operation.</source>
        <translation>如果为0，比例控制则不会处于运行状态。</translation>
    </message>
    <message>
        <source>Integral/derivative ratio</source>
        <translation>积分/微分比</translation>
    </message>
    <message>
        <source>According to Ziegler and Nichols the best ratio is 4.0.</source>
        <translation>根据Ziegler和Nichols，最佳比例为4.0。</translation>
    </message>
    <message>
        <source>If 0, the derivative control won&apos;t be in operation.</source>
        <translation>如果为0，微分控制则不会处于运行状态。</translation>
    </message>
    <message>
        <source>Negative output limit</source>
        <translation>负输出限制</translation>
    </message>
    <message>
        <source>Output limit.</source>
        <translation>输出限制。</translation>
    </message>
    <message>
        <source>Positive output limit</source>
        <translation>正输出限制</translation>
    </message>
    <message>
        <source>Derivative type</source>
        <translation>微分类型</translation>
    </message>
    <message>
        <source>On the gap</source>
        <translation>关于间隙</translation>
    </message>
    <message>
        <source>On the measure.</source>
        <translation>关于测量。</translation>
    </message>
    <message>
        <source>On the setpoint.</source>
        <translation>关于设置点。</translation>
    </message>
    <message>
        <source>If you select:</source>
        <translation>如果您选择：</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The derivative calculation will be done on the gap (setpoint - measure).</source>
        <translation>有助于避免匝道尽头的超调量。将在间隙（设置点-测量）上进行微分计算。</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure.</source>
        <translation>将在测量上进行微分计算。</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the setpoint.</source>
        <translation>将在设置点上进行微分计算。</translation>
    </message>
    <message>
        <source>Measure derivative</source>
        <translation>测量微分</translation>
    </message>
    <message>
        <source>Ramp on PID setpoint</source>
        <translation>PID设置点上的匝道</translation>
    </message>
    <message>
        <source>The PID setpoint will be a ramp form one.</source>
        <translation>PID设置点将构成一个匝道。</translation>
    </message>
    <message>
        <source>It will be the final value (No ramp).</source>
        <translation>它将是最终值(无匝道)。</translation>
    </message>
    <message>
        <source>Integral frozen in ramp</source>
        <translation>匝道的积分冻结</translation>
    </message>
    <message>
        <source>The integral value will frozen in ramp.</source>
        <translation>整数值在匝道冻结。</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The integral value will be modified normally by the PID control.</source>
        <translation>有助于避免匝道尽头的超调量。通常使用比例积分微分（PID）控制修改整数值。</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure. The derivative effect is based on the error variation : setpoint – measurement. This may be useful to avoid overshoot at the ramp end.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The derivative effect is based on the setpoint variation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPIDCalculD</name>
    <message>
        <source>Measure derivative</source>
        <translation>测量微分</translation>
    </message>
    <message>
        <source>The derivative calculation will be done on the measure.</source>
        <translation>将在测量上进行微分计算。</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The derivative calculation will be done on the gap (setpoint - measure).</source>
        <translation>有助于避免匝道尽头的超调量。将在间隙（设置点-测量）上进行微分计算。</translation>
    </message>
</context>
<context>
    <name>CYPIDI</name>
    <message>
        <source>Integral coefficient</source>
        <translation>积分系数</translation>
    </message>
    <message>
        <source>If 0, the integral control won&apos;t be in operation.</source>
        <translation>如果为0，比例控制则不会处于运行状态。</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>该控制属于一种混合型 P.I.D。</translation>
    </message>
</context>
<context>
    <name>CYPIDInput</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source>- </source>
        <translation>- </translation>
    </message>
    <message>
        <source>+ </source>
        <translation>+ </translation>
    </message>
    <message>
        <source>PID</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPIDK</name>
    <message>
        <source>Integral/derivative ratio</source>
        <translation>积分/微分比</translation>
    </message>
    <message>
        <source>According to Ziegler and Nichols the best ratio is 4.0.</source>
        <translation>根据Ziegler和Nichols，最佳比例为4.0。</translation>
    </message>
    <message>
        <source>If 0, the derivative control won&apos;t be in operation.</source>
        <translation>如果为0，微分控制则不会处于运行状态。</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>该控制属于一种混合型 P.I.D。</translation>
    </message>
</context>
<context>
    <name>CYPIDMax</name>
    <message>
        <source>Positive output limit</source>
        <translation>正输出限制</translation>
    </message>
    <message>
        <source>Output limit.</source>
        <translation>输出限制。</translation>
    </message>
</context>
<context>
    <name>CYPIDMin</name>
    <message>
        <source>Negative output limit</source>
        <translation>负输出限制</translation>
    </message>
    <message>
        <source>Output limit.</source>
        <translation>输出限制。</translation>
    </message>
</context>
<context>
    <name>CYPIDNoI</name>
    <message>
        <source>Integral frozen in ramp</source>
        <translation>匝道的积分冻结</translation>
    </message>
    <message>
        <source>The integral value will frozen in ramp.</source>
        <translation>整数值在匝道冻结。</translation>
    </message>
    <message>
        <source>This may be useful to avoid overshoot at the ramp end.The integral value will be modified normally by the PID control.</source>
        <translation>有助于避免匝道尽头的超调量。通常使用比例积分微分（PID）控制修改整数值。</translation>
    </message>
</context>
<context>
    <name>CYPIDP</name>
    <message>
        <source>Proportional coefficient</source>
        <translation>比例系统</translation>
    </message>
    <message>
        <source>If 0, the proportional control won&apos;t be active (It can work in integral mode)</source>
        <translation>如果为0，则不会激活比例控制（可在积分模式下工作）</translation>
    </message>
    <message>
        <source>This control is a mixt P.I.D.</source>
        <translation>该控制属于一种混合型 P.I.D。</translation>
    </message>
</context>
<context>
    <name>CYPIDRamp</name>
    <message>
        <source>Ramp on PID setpoint</source>
        <translation>PID设置点上的匝道</translation>
    </message>
    <message>
        <source>The PID setpoint will be a ramp form one.</source>
        <translation>PID设置点将构成一个匝道。</translation>
    </message>
    <message>
        <source>It will be the final value (No ramp).</source>
        <translation>它将是最终值(无匝道)。</translation>
    </message>
</context>
<context>
    <name>CYPasswordDialog</name>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Keep password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Verify:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password strength meter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The password strength meter gives an indication of the security of the password you have entered. To improve the strength of the password, try:
 - using a longer password;
 - using a mixture of upper- and lower-case letters;
 - using numbers or symbols, such as #, as well as letters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Passwords do not match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You entered two different passwords. Please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The password you have entered has a low strength. To improve the strength of the password, try:
 - using a longer password;
 - using a mixture of upper- and lower-case letters;
 - using numbers or symbols as well as letters.

Would you like to use this password anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low Password Strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password must be at least 1 character long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password must be at least %1 characters long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Passwords match</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPen</name>
    <message>
        <source>Style</source>
        <translation>风格</translation>
    </message>
    <message>
        <source>No line at all</source>
        <translation>完全无空线</translation>
    </message>
    <message>
        <source>A simple line</source>
        <translation>简单线</translation>
    </message>
    <message>
        <source>Dashes separated by a few pixels</source>
        <translation>破折号隔开几个像素</translation>
    </message>
    <message>
        <source>Dots separated by a few pixels</source>
        <translation>点隔开几个像素</translation>
    </message>
    <message>
        <source>Alternate dots and dashes</source>
        <translation>备用点和破折号</translation>
    </message>
    <message>
        <source>One dash, two dots, one dash, two dots</source>
        <translation>1个破折号，2个点，1个破折号，2个点，</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYPenDialog</name>
    <message>
        <source>Pen edit dialog</source>
        <translation>笔编辑对话框</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Pen</source>
        <translation>笔</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYProject</name>
    <message>
        <source>The file %1 does not contain valid XML !
%2: %3 %4</source>
        <translation>文件 %1 不含有效 XML !
%2: %3 %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">不能保存文件%1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">不能保存文件%1</translation>
    </message>
</context>
<context>
    <name>CYProjectEditor</name>
    <message>
        <source>The selected object is not a file !</source>
        <translation>选择对象不是一个文件！</translation>
    </message>
    <message>
        <source>Select a project file ends with &quot;.cyprj&quot;.</source>
        <translation>选择以&quot;.cyprj&quot; 结尾的一个项目文件。</translation>
    </message>
    <message>
        <source>Cannot find the project !</source>
        <translation>无法找到此项目 ！</translation>
    </message>
    <message>
        <source>The selected file is not a project file !</source>
        <translation>选定文件不是一个项目文件！</translation>
    </message>
    <message>
        <source>Do you want to load an existing project or create a new project ?</source>
        <translation>您要加载一个现有项目或创建一个新的项目吗？</translation>
    </message>
    <message>
        <source>Cannot find the current project !</source>
        <translation>无法找到当前项目！</translation>
    </message>
    <message>
        <source>&amp;Create</source>
        <translation>&amp;创建</translation>
    </message>
    <message>
        <source>&amp;Load</source>
        <translation>&amp;加载</translation>
    </message>
    <message>
        <source>If you save this project the restart test will be forbidden !</source>
        <translation>如果您保存此项目，将禁止重新启动测试 ！</translation>
    </message>
    <message>
        <source>The project was modified.
Do you want to save your changes?</source>
        <translation>此项目已更改。 
是否将更改保存？</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>未保存更改</translation>
    </message>
    <message>
        <source>View: %1</source>
        <translation>查看: %1</translation>
    </message>
    <message>
        <source> (Execution following ON)</source>
        <translation>(执行下列ON)</translation>
    </message>
    <message>
        <source>Edit: %1</source>
        <translation>编辑: %1</translation>
    </message>
    <message>
        <source>Edit: %1 (modified)</source>
        <translation>编辑： %1 （更改）</translation>
    </message>
    <message>
        <source>Project error</source>
        <translation>项目错误</translation>
    </message>
    <message>
        <source>Select project file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The project cannot be saved because the editor has been open in read only mode!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYProjectTestDirDialog</name>
    <message>
        <source>Test directory</source>
        <translation>测试目录</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="vanished">文字标签</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Name of the new test directory</source>
        <translation>新测试目录名称</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>自动</translation>
    </message>
    <message>
        <source>Input the test name directory</source>
        <translation>输入目录名称</translation>
    </message>
    <message>
        <source>Add &amp;test number</source>
        <translation>添加测试编号</translation>
    </message>
    <message>
        <source>Choose another directory</source>
        <translation>选择另一个目录</translation>
    </message>
    <message>
        <source>Create a ne&amp;w test directory</source>
        <translation>创建一个新的测试目录</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <source>If you want to change the test directory, you must change its name !</source>
        <translation>如果你想改变测试目录，必须修改目录名字！</translation>
    </message>
    <message>
        <source>This directory already exists !
It may contain results of an other test.
Do you really want to use this directory ?</source>
        <translation>此目录已经存在！
 其中可能包括其它测试结果。
 你确定需要使用该目录吗？</translation>
    </message>
</context>
<context>
    <name>CYSMTP</name>
    <message>
        <source>Connected to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simple SMTP client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unexpected reply from SMTP server:

</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScope</name>
    <message>
        <source>On which axis do you want this data must to be ?</source>
        <translation>您想要在哪个轴上显示这个数据？</translation>
    </message>
    <message>
        <source>Axis selection</source>
        <translation>选择轴</translation>
    </message>
    <message>
        <source>&amp;Y</source>
        <translation>&amp;Y</translation>
    </message>
    <message>
        <source>&amp;X</source>
        <translation>&amp;X</translation>
    </message>
    <message>
        <source>This display cannot treat this type of data !</source>
        <translation>该显示器无法处理此类数据！</translation>
    </message>
    <message>
        <source>This data is already displayed in this oscilloscope.</source>
        <translation>这个数据已经显示在示波器中。</translation>
    </message>
    <message>
        <source>CYLIX: Scope Capture Window</source>
        <translation>CYLIX: 视窗撷取范围</translation>
    </message>
    <message>
        <source>Axis</source>
        <translation>轴</translation>
    </message>
    <message>
        <source>Scope</source>
        <translation>范围</translation>
    </message>
    <message>
        <source>&amp;Analyse curves</source>
        <translation>&amp;分析曲线</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <source>Pr&amp;int curves</source>
        <translation>Pr&amp;int曲线</translation>
    </message>
    <message>
        <source>Export PDF</source>
        <translation>导出PDF</translation>
    </message>
    <message>
        <source>Export CSV</source>
        <translation>导出CSV</translation>
    </message>
    <message>
        <source>Setup trigger</source>
        <translation>启动触发</translation>
    </message>
    <message>
        <source>Reset trigger</source>
        <translation>重置触发</translation>
    </message>
    <message>
        <source>This data can not be put into an oscilloscope because it is not buffered!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The previous export not finished!
Renew the export.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeAcquisition</name>
    <message>
        <source>curves</source>
        <translation>曲线</translation>
    </message>
    <message>
        <source>Acquisition period</source>
        <translation type="unfinished">采集周期</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="unfinished">标题</translation>
    </message>
    <message>
        <source>Exported:</source>
        <translation type="vanished">导出∶</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <source>Saving under</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X decimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y decimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeAnalyser</name>
    <message>
        <source>&amp;Export PDF</source>
        <translation>&amp;导出PDF</translation>
    </message>
    <message>
        <source>&amp;Permut cursors</source>
        <translation>&amp;Permut光标</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>E&amp;xport CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto zoom Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom &amp;In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom &amp;Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Main Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeAxis</name>
    <message>
        <source>Left axis</source>
        <translation>左轴</translation>
    </message>
    <message>
        <source>Right axis</source>
        <translation>右轴</translation>
    </message>
    <message>
        <source>Bottom axis</source>
        <translation>底部轴</translation>
    </message>
    <message>
        <source>Top axis</source>
        <translation>顶部轴</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation>最大值</translation>
    </message>
    <message>
        <source>Minimum value</source>
        <translation>最小值</translation>
    </message>
    <message>
        <source>Auto scale</source>
        <translation>自动缩放</translation>
    </message>
    <message>
        <source>Scale settable</source>
        <translation>可定位缩放</translation>
    </message>
    <message>
        <source>Left value</source>
        <translation>左值</translation>
    </message>
    <message>
        <source>Right value</source>
        <translation>右值</translation>
    </message>
    <message>
        <source>Shown</source>
        <translation>显示</translation>
    </message>
    <message>
        <source>Show axis</source>
        <translation>显示轴</translation>
    </message>
    <message>
        <source>Hide axis</source>
        <translation>隐藏轴</translation>
    </message>
    <message>
        <source>Logarithmic scale</source>
        <translation>对数刻度</translation>
    </message>
    <message>
        <source>Enable logarithmic scale</source>
        <translation>启动对数刻度</translation>
    </message>
    <message>
        <source>Disable logarithmic scale</source>
        <translation>禁止对数刻度</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation>启用</translation>
    </message>
    <message>
        <source>Axis enabled</source>
        <translation>启用轴</translation>
    </message>
    <message>
        <source>Axis disabled</source>
        <translation>禁用轴</translation>
    </message>
    <message>
        <source>Hz</source>
        <translation>赫兹</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>sec</source>
        <translation>秒</translation>
    </message>
    <message>
        <source>min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>小时</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <source>Hide the auto-scale option</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeAxisSetup</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
</context>
<context>
    <name>CYScopeCapture</name>
    <message>
        <source>Cursors</source>
        <translation>光标</translation>
    </message>
</context>
<context>
    <name>CYScopeCaptureLegend</name>
    <message>
        <source>Cursors</source>
        <translation>光标</translation>
    </message>
</context>
<context>
    <name>CYScopePlotter</name>
    <message>
        <source>If you click on the scale a dialog box to change it will be created.</source>
        <translation type="vanished">如果点击缩放，将创建一个对话框来改变它。</translation>
    </message>
    <message>
        <source>For a good display the maximum number of curves is %1!</source>
        <translation>为了更好的显示曲线的最大数量为%1!</translation>
    </message>
    <message>
        <source>You must put data with the same acquisition time of the data(s) already displayed by this oscilloscope</source>
        <translation>您必须设置该示波器上已经显示的，具有相同采集时间的数据。</translation>
    </message>
    <message>
        <source>You must put data with the same buffer size of the data(s) already displayed by this oscilloscope</source>
        <translation>您必须设置该示波器上已经显示的，具有相同缓冲量的数据。</translation>
    </message>
    <message>
        <source>You must put data with the same burst buffer size of the data(s) already displayed by this oscilloscope</source>
        <translation>您必须设置该示波器上已经显示的，具有相同突发缓冲量的数据。</translation>
    </message>
    <message>
        <source>You must put data from the same connection of the data(s)already displayed by this oscilloscope</source>
        <translation>您必须设置该示波器上已经显示的，具有相同连接的数据。</translation>
    </message>
    <message>
        <source>You must put the same physical type of the data(s) already displayed by this oscilloscope</source>
        <translation>您必须设置该示波器上已经显示的，具相同物理类型的数据。</translation>
    </message>
    <message>
        <source>No title</source>
        <translation>无标题</translation>
    </message>
    <message>
        <source>Change label</source>
        <translation>更改标签</translation>
    </message>
    <message>
        <source>Change pen</source>
        <translation>更改笔</translation>
    </message>
    <message>
        <source>Change scale coefficient</source>
        <translation>更改缩放系数</translation>
    </message>
    <message>
        <source>If you double click on the scope a window to analyse curves will be created.</source>
        <translation>如果您在窗口范围内双击，将创建处分析曲线。</translation>
    </message>
    <message>
        <source>Scope</source>
        <translation>范围</translation>
    </message>
    <message>
        <source>Use mode</source>
        <translation>使用模式</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <source>XY</source>
        <translation>XY</translation>
    </message>
    <message>
        <source>Sampling mode</source>
        <translation>采样模式</translation>
    </message>
    <message>
        <source>Continuous</source>
        <translation>连续</translation>
    </message>
    <message>
        <source>In bursts</source>
        <translation>突发传送中</translation>
    </message>
    <message>
        <source>Trigger</source>
        <translation>触发</translation>
    </message>
    <message>
        <source>Signal</source>
        <translation>信号</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>无</translation>
    </message>
    <message>
        <source>Level</source>
        <translation>等级</translation>
    </message>
    <message>
        <source>Post-Trigger</source>
        <translation>触发后</translation>
    </message>
    <message>
        <source>Slope</source>
        <translation>斜率</translation>
    </message>
    <message>
        <source>Rising</source>
        <translation>上升</translation>
    </message>
    <message>
        <source>Trigger over the level.</source>
        <translation>触发超过水平</translation>
    </message>
    <message>
        <source>Falling</source>
        <translation>下降</translation>
    </message>
    <message>
        <source>Trigger below the level.</source>
        <translation>触发在水平之下</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>风格</translation>
    </message>
    <message>
        <source>Curves style</source>
        <translation>曲线风格</translation>
    </message>
    <message>
        <source>No Curve</source>
        <translation>无曲线</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation>明细</translation>
    </message>
    <message>
        <source>Sticks</source>
        <translation>换档杆</translation>
    </message>
    <message>
        <source>Steps</source>
        <translation>步骤</translation>
    </message>
    <message>
        <source>Dots</source>
        <translation>点</translation>
    </message>
    <message>
        <source>Curves width</source>
        <translation>曲线宽度</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>背景色</translation>
    </message>
    <message>
        <source>Major gridlines</source>
        <translation>主要网格线</translation>
    </message>
    <message>
        <source>Minor gridlines</source>
        <translation>次要网格线</translation>
    </message>
    <message>
        <source>Major gridlines color</source>
        <translation>主要网格线颜色</translation>
    </message>
    <message>
        <source>Minor gridlines color</source>
        <translation>次要网格线颜色</translation>
    </message>
    <message>
        <source>Legends</source>
        <translation type="vanished">图例</translation>
    </message>
    <message>
        <source>Acquisition</source>
        <translation>采集</translation>
    </message>
    <message>
        <source>Enable acquisition</source>
        <translation>启动采集</translation>
    </message>
    <message>
        <source>Data label</source>
        <translation>数据标签</translation>
    </message>
    <message>
        <source>Enter the new label: </source>
        <translation>输入新标签:</translation>
    </message>
    <message>
        <source>Display coefficient</source>
        <translation>显示系数</translation>
    </message>
    <message>
        <source>Enter the new display coefficient: </source>
        <translation>输入新的显示系数:</translation>
    </message>
    <message>
        <source>You must enter a coefficient display different of 0 !</source>
        <translation>您必须输入一个显示不同0的系数!</translation>
    </message>
    <message>
        <source>Activation of the trigger</source>
        <translation>触发反应</translation>
    </message>
    <message>
        <source>&lt;p&gt;If you click on the scale, a dialog box will appear to change it.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable trigger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bottom legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Legend position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No trigger signal!
Do you want to configure it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The source signal of the trigger was not found!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopePrinter</name>
    <message>
        <source>%1/curves</source>
        <translation>%1/曲线</translation>
    </message>
    <message>
        <source>%1/curve_%2.ps</source>
        <translation>%1/曲线_%2.ps</translation>
    </message>
    <message>
        <source>%1/curve</source>
        <translation>%1/曲线</translation>
    </message>
    <message>
        <source>%1/curve.pdf</source>
        <translation>%1/曲线.pdf</translation>
    </message>
    <message>
        <source>Page %1</source>
        <translation>页 %1</translation>
    </message>
    <message>
        <source>Saving under</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t print %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t open %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeScaleDialog</name>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeSetup</name>
    <message>
        <source>Oscilloscope Settings</source>
        <translation>示波器设置</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>Style</source>
        <translation>风格</translation>
    </message>
    <message>
        <source>Grid</source>
        <translation>表格</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>文字</translation>
    </message>
    <message>
        <source>Curves</source>
        <translation>曲线</translation>
    </message>
    <message>
        <source>Background</source>
        <translation>背景</translation>
    </message>
    <message>
        <source>Data &amp;X</source>
        <translation>数据 &amp;X</translation>
    </message>
    <message>
        <source>Datas &amp;Y</source>
        <translation>数据 &amp;Y</translation>
    </message>
    <message>
        <source>Axis</source>
        <translation>轴</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeTrigger</name>
    <message>
        <source>Trigger setting</source>
        <translation>触发设置</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYScopeTriggerSetup</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
</context>
<context>
    <name>CYSettingsDialog</name>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation>活动视图中存在未保存的更改。
是否应用或放弃此更改?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>未保存更改</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYSplashScreen</name>
    <message>
        <source>Starting CYLIX...</source>
        <translation>正在启动 CYLIX...</translation>
    </message>
    <message>
        <source>Stopping CYLIX...</source>
        <translation>正在停止 CYLIX...</translation>
    </message>
</context>
<context>
    <name>CYString</name>
    <message>
        <source>Designer value: %1.</source>
        <translation>设计者值：%1.</translation>
    </message>
    <message>
        <source>The maximum number of characters is %1.</source>
        <translation>最大字符数为%1。</translation>
    </message>
    <message>
        <source>If the string is too long it will be truncated!</source>
        <translation>如果字符串过长,将其缩短!</translation>
    </message>
</context>
<context>
    <name>CYTSec</name>
    <message>
        <source>sec</source>
        <translation>秒</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>时间</translation>
    </message>
</context>
<context>
    <name>CYTabWidget</name>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation>活动视图中存在未保存的更改。
是否应用或放弃此更改?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>未保存更改</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYTextDialog</name>
    <message>
        <source>Appl&amp;y</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
</context>
<context>
    <name>CYTextEdit</name>
    <message>
        <source>&amp;Scroll view</source>
        <translation>&amp;滚动视图</translation>
    </message>
    <message>
        <source>Showing last line</source>
        <translation>显示最后一行</translation>
    </message>
    <message>
        <source>Vertical Auto</source>
        <translation>自动垂直</translation>
    </message>
    <message>
        <source>Vertical Off</source>
        <translation>垂直关闭</translation>
    </message>
    <message>
        <source>Vertical On</source>
        <translation>垂直打开</translation>
    </message>
    <message>
        <source>Horizontal Auto</source>
        <translation>自动水平</translation>
    </message>
    <message>
        <source>Horizontal Off</source>
        <translation>关闭水平</translation>
    </message>
    <message>
        <source>Horizontal On</source>
        <translation>打开水平</translation>
    </message>
    <message>
        <source>Maximum number of lines : %1 !</source>
        <translation>最大行数： %1 ！</translation>
    </message>
</context>
<context>
    <name>CYTim</name>
    <message>
        <source>Maintenance</source>
        <translation>维修</translation>
    </message>
    <message>
        <source>Timer</source>
        <translation>计时器</translation>
    </message>
    <message>
        <source>Timer used for maintenance</source>
        <translation>用于维护的计时器</translation>
    </message>
    <message>
        <source>Partial</source>
        <translation>分</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>笔记</translation>
    </message>
    <message>
        <source>Total</source>
        <translation>总</translation>
    </message>
</context>
<context>
    <name>CYTime</name>
    <message>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <source>hour</source>
        <translation>小时</translation>
    </message>
    <message>
        <source>min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <source>sec</source>
        <translation>秒</translation>
    </message>
    <message>
        <source>msec</source>
        <translation>毫秒</translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is higher than maximum value (%3&gt;%4) !</source>
        <translation>设计值%1:%2高于最大值(%3&gt;%4)!</translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is smaller than minimum value (%3&lt;%4) !</source>
        <translation>设计值%1:%2小于最小值(%3︿%4) !</translation>
    </message>
    <message>
        <source>%1h%2m</source>
        <translation>%1小时%2分钟</translation>
    </message>
    <message>
        <source>%2 min</source>
        <translation>%2分钟</translation>
    </message>
    <message>
        <source>%1m%2s</source>
        <translation>%1m%2s</translation>
    </message>
    <message>
        <source>%1 sec</source>
        <translation>%1秒</translation>
    </message>
    <message>
        <source>%1s%2ms</source>
        <translation>%1s%2ms</translation>
    </message>
    <message>
        <source>%3 msec</source>
        <translation>%3毫秒</translation>
    </message>
    <message>
        <source>%1h%2m%3s</source>
        <translation>%1小时%2分钟%3秒</translation>
    </message>
    <message>
        <source>%1h%2m%3s%4</source>
        <translation>%1小时%2分钟%3秒%4</translation>
    </message>
    <message>
        <source>%1m%2s%3</source>
        <translation>%1分钟%2秒%3</translation>
    </message>
    <message>
        <source>%1s%2</source>
        <translation>%1秒%2</translation>
    </message>
    <message>
        <source>%1 msec</source>
        <translation>%1毫秒</translation>
    </message>
    <message>
        <source>%1h</source>
        <translation>%1小时</translation>
    </message>
    <message>
        <source>%1m</source>
        <translation>%1m</translation>
    </message>
    <message>
        <source>%1s</source>
        <translation>%1s</translation>
    </message>
    <message>
        <source> hour</source>
        <translation>小时</translation>
    </message>
    <message>
        <source> min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <source> sec</source>
        <translation>秒</translation>
    </message>
    <message>
        <source> msec</source>
        <translation>毫秒</translation>
    </message>
    <message>
        <source>h%1m</source>
        <translation>h%1m</translation>
    </message>
    <message>
        <source>m</source>
        <translation>分钟</translation>
    </message>
    <message>
        <source>m%1s</source>
        <translation>m%1s</translation>
    </message>
    <message>
        <source>s</source>
        <translation>秒</translation>
    </message>
    <message>
        <source>s%1ms</source>
        <translation>s%1ms</translation>
    </message>
    <message>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <source>h%1m%2s</source>
        <translation>h%1m%2s</translation>
    </message>
    <message>
        <source>h%1m%2s%3ms</source>
        <translation>h%1m%2s%3ms</translation>
    </message>
    <message>
        <source>m%1s%2ms</source>
        <translation>m%1s%2ms</translation>
    </message>
    <message>
        <source>Value from %1 to %2.</source>
        <translation>%1到%2的值。</translation>
    </message>
    <message>
        <source>Precision is %1 %2.</source>
        <translation>精度是%1 %2。</translation>
    </message>
    <message>
        <source>Designer value: %1.</source>
        <translation>设计者值：%1.</translation>
    </message>
    <message>
        <source>0ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>00ms</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUser</name>
    <message>
        <source>User</source>
        <translation>用户</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>LINUX user</source>
        <translation>LINUX 用户</translation>
    </message>
    <message>
        <source>The LINUX user takes the user name of the current LINUX session.</source>
        <translation>LINUX 用户使用当前LINUX 集内现有用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <source>Has a password</source>
        <translation>已有密码</translation>
    </message>
    <message>
        <source>Password of &apos;%1&apos;</source>
        <translation>&apos;%1&apos; 的密码</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <translation>密码错误</translation>
    </message>
    <message>
        <source>Access &apos;%1&apos; has no password.</source>
        <translation>访问&apos;%1&apos; 没有密码。</translation>
    </message>
    <message>
        <source>Access password</source>
        <translation>登陆密码</translation>
    </message>
    <message>
        <source>New password for &apos;%1&apos;</source>
        <translation> &apos;%1&apos;的新密码</translation>
    </message>
    <message>
        <source>Can&apos;t find group with index %1</source>
        <translation>无法找到含索引%1的分组。</translation>
    </message>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>无法打开文件%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>文件 %1 不含有效 XML 
%2: 行:%3 列: %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">不能保存文件%1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">不能保存文件%1</translation>
    </message>
    <message>
        <source>Password checking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1
Password of &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Access &apos;%1&apos; cannot change its password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot get system user name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserAction</name>
    <message>
        <source>User action</source>
        <translation>用户操作</translation>
    </message>
</context>
<context>
    <name>CYUserAdminEdit</name>
    <message>
        <source>Users administration</source>
        <translation>用户管理</translation>
    </message>
    <message>
        <source>Users</source>
        <translation>用户</translation>
    </message>
    <message>
        <source>Index</source>
        <translation>索引</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>&amp;Add</source>
        <translation>&amp;加</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>The LINUX user takes the user name of the current LINUX session.</source>
        <translation>LINUX 用户使用当前LINUX 集内现有用户名</translation>
    </message>
    <message>
        <source>Add &amp;Linux user</source>
        <translation>Add &amp;Linux用户</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>组</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Administrator</source>
        <translation>管理员</translation>
    </message>
    <message>
        <source>Are you sure to remove the user %1 ?</source>
        <translation>您确定删除用户%1吗？</translation>
    </message>
    <message>
        <source>Are you sure to remove the users group %1 ?</source>
        <translation>您确定删除用户组%1吗？</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserAdminEdit2</name>
    <message>
        <source>Users administration</source>
        <translation>用户管理</translation>
    </message>
    <message>
        <source>Add &amp;user</source>
        <translation>增加 &amp;用户</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <source>The LINUX user takes the user name of the current LINUX session.</source>
        <translation>LINUX 用户使用当前LINUX 集内现有用户名</translation>
    </message>
    <message>
        <source>Add &amp;Linux user</source>
        <translation>Add &amp;Linux用户</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>Add &amp;group</source>
        <translation>增加 &amp;组</translation>
    </message>
    <message>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <source>Remo&amp;ve</source>
        <translation>移除</translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>User</source>
        <translation>用户</translation>
    </message>
    <message>
        <source>Chefs d&apos;équipe</source>
        <translation>组长</translation>
    </message>
    <message>
        <source>Michel</source>
        <translation>Michel</translation>
    </message>
    <message>
        <source>Equipe de jour</source>
        <translation>白班</translation>
    </message>
    <message>
        <source>Jean</source>
        <translation>Jean</translation>
    </message>
    <message>
        <source>Thomas</source>
        <translation>Thomas</translation>
    </message>
    <message>
        <source>Vincent</source>
        <translation>Vincent</translation>
    </message>
    <message>
        <source>Equipe de nuit</source>
        <translation>夜班</translation>
    </message>
    <message>
        <source>David</source>
        <translation>David</translation>
    </message>
    <message>
        <source>Xavier</source>
        <translation>Xavier</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation>维修</translation>
    </message>
    <message>
        <source>Julien</source>
        <translation>Julien</translation>
    </message>
    <message>
        <source>Robert</source>
        <translation>Robert</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Are you sure to remove the user %1 ?</source>
        <translation>您确定删除用户%1吗？</translation>
    </message>
    <message>
        <source>Are you sure to remove the users group %1 ?</source>
        <translation>您确定删除用户组%1吗？</translation>
    </message>
    <message>
        <source>The WINDOWS user takes the user name of the current WINDOWS session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add &amp;Windows user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserEdit</name>
    <message>
        <source>Edit user</source>
        <translation>编辑用户</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>App&amp;ly</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>组</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserGroup</name>
    <message>
        <source>Users group</source>
        <translation>用户组</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>CYLIX starting protected</source>
        <translation>CYLIX 正在启动保护</translation>
    </message>
    <message>
        <source>CYLIX can start automatically with the current user at the time of the preceding stop if the group of this one allows it.</source>
        <translation>只要用户组允许，当前用户可在前次停止时自动启动CYLIX。</translation>
    </message>
    <message>
        <source>CYLIX starts in protected access.</source>
        <translation>CYLIX在访问保护模式下启动。</translation>
    </message>
    <message>
        <source>Screensaver protection</source>
        <translation>屏幕保护程序保护</translation>
    </message>
    <message>
        <source>Screensaver protection action</source>
        <translation>屏幕保护程序的保护动作</translation>
    </message>
    <message>
        <source>Force CYLIX in protected access</source>
        <translation>在访问保护模式下，推动CYLIX。</translation>
    </message>
    <message>
        <source>Stop the LINUX session</source>
        <translation>停止 LINUX会话</translation>
    </message>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>无法打开文件%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>文件 %1 不含有效 XML 
%2: 行:%3 列: %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1!</source>
        <translation type="unfinished">不能保存文件%1!</translation>
    </message>
    <message>
        <source>Can&apos;t save file %1</source>
        <translation type="vanished">不能保存文件%1</translation>
    </message>
    <message>
        <source>Protect access to desktop environment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restricts access to the desktop environment by disabling its panel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configuration is not possible if one of the parents has enabled this protection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This does not work and will be corrected soon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to find the collection of action %1 in user group %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to find action %1 in user group %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYUserGroupEdit</name>
    <message>
        <source>Edit user group</source>
        <translation>编辑用户组</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Menus</source>
        <translation>菜单</translation>
    </message>
    <message>
        <source>Au&amp;thorized actions:</source>
        <translation>已授权操作：</translation>
    </message>
    <message>
        <source>Proh&amp;ibited actions:</source>
        <translation>已禁止操作：</translation>
    </message>
    <message>
        <source>Protection</source>
        <translation>保护</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYWin</name>
    <message>
        <source>Can&apos;t open the file %1</source>
        <translation>无法打开文件%1</translation>
    </message>
    <message>
        <source>The file %1 does not contain valid XML
%2: line:%3 colomn:%4</source>
        <translation>文件 %1 不含有效 XML 
%2: 行:%3 列: %4</translation>
    </message>
    <message>
        <source>The file %1 does not contain a validdefinition, which must have a document type </source>
        <translation>文件%1不含有效定义，该文件必须有文件类型</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Qt Application</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CYWord</name>
    <message>
        <source>Word</source>
        <translation>关键词</translation>
    </message>
</context>
<context>
    <name>CalCHP</name>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;设计者评价</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>CalCylinder</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;设计者评价</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
</context>
<context>
    <name>CalServo</name>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;设计者评价</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>Conditions</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <source>SUPERVISOR</source>
        <translation type="unfinished">检察员</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation type="unfinished">检查员</translation>
    </message>
    <message>
        <source>Regulator</source>
        <translation type="unfinished">校准</translation>
    </message>
    <message>
        <source>Local datas</source>
        <translation type="unfinished">本地数据</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">时间</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="unfinished">压力</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="unfinished">温度</translation>
    </message>
    <message>
        <source>Percentage</source>
        <translation type="unfinished">百分比</translation>
    </message>
    <message>
        <source>Trigonometry</source>
        <translation type="unfinished">三角法</translation>
    </message>
    <message>
        <source>Integer</source>
        <translation type="obsolete">整数</translation>
    </message>
    <message>
        <source>Operator&apos;s manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cylix manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Release notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Machine</source>
        <translation type="unfinished">设备</translation>
    </message>
    <message>
        <source>Non-synchronous connection</source>
        <translation type="unfinished">不同步连接</translation>
    </message>
    <message>
        <source>Synchronous connection</source>
        <translation type="unfinished">同步连接</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="unfinished">类型</translation>
    </message>
    <message>
        <source>Leak rate</source>
        <translation type="unfinished">泄漏率</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="unfinished">模拟输入</translation>
    </message>
    <message>
        <source>Stations inlet pressure</source>
        <translation type="unfinished">站入口压力</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="unfinished">港位置</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation type="unfinished">站入口流体温度</translation>
    </message>
    <message>
        <source>Stations outlet fluid temperature</source>
        <translation type="unfinished">站出口流体温度</translation>
    </message>
    <message>
        <source>Ramp 1 heater temperature</source>
        <translation type="unfinished">斜升 1 加热器温度</translation>
    </message>
    <message>
        <source>Ramp 2 heater temperature</source>
        <translation type="unfinished">斜升 2 加热器温度</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation type="unfinished">站出口压力</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="unfinished">爆裂压力</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="unfinished">环境温度</translation>
    </message>
    <message>
        <source>Water inlet temperature</source>
        <translation type="unfinished">进水温度</translation>
    </message>
    <message>
        <source>Water outlet temperature</source>
        <translation type="unfinished">出水温度</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation type="unfinished">模拟输出</translation>
    </message>
    <message>
        <source>Frequency converter VA1 (circulation pump)</source>
        <translation type="unfinished">频率转换器 VA1（循环泵）</translation>
    </message>
    <message>
        <source>Servo cylinder control</source>
        <translation type="unfinished">伺服缸控制</translation>
    </message>
    <message>
        <source>Slow continuous acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fast continuous acquisition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Slow acquisition on event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fast acquisition on event</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CylibsTest</name>
    <message>
        <source>textLabel</source>
        <translation>文字标签</translation>
    </message>
    <message>
        <source>Analys&amp;e</source>
        <translation>分析</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <source>Page 1</source>
        <translation>第一页</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation>应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>New Item</source>
        <translation>新项目</translation>
    </message>
    <message>
        <source>New Item 2</source>
        <translation>项目二</translation>
    </message>
    <message>
        <source>a </source>
        <translation>a</translation>
    </message>
    <message>
        <source> v</source>
        <translation>v</translation>
    </message>
    <message>
        <source>textLabel1</source>
        <translation>文本标签1</translation>
    </message>
    <message>
        <source>Page 2</source>
        <translation>第二页</translation>
    </message>
    <message>
        <source>YX</source>
        <translation>YX</translation>
    </message>
    <message>
        <source>00</source>
        <translation>00</translation>
    </message>
    <message>
        <source>titre</source>
        <translation>标题</translation>
    </message>
    <message>
        <source>pushButton&amp;4</source>
        <translation>按钮&amp;4</translation>
    </message>
    <message>
        <source>pushBu&amp;tton6</source>
        <translation>按钮&amp;6</translation>
    </message>
    <message>
        <source>pushButton&amp;5</source>
        <translation>按钮&amp;5</translation>
    </message>
    <message>
        <source>pushButton&amp;3</source>
        <translation>按钮&amp;3</translation>
    </message>
    <message>
        <source>1hh</source>
        <translation>1hh</translation>
    </message>
    <message>
        <source>&amp;0</source>
        <translation>&amp;0</translation>
    </message>
    <message>
        <source>AnalyseSheet</source>
        <translation>分析单</translation>
    </message>
    <message>
        <source>Loa&amp;d</source>
        <translation>装载</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Sa&amp;ve</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>Alt+V</source>
        <translation>Alt+V</translation>
    </message>
    <message>
        <source>Au&amp;toCali</source>
        <translation>Au&amp;toCali</translation>
    </message>
    <message>
        <source>Man&amp;uCali</source>
        <translation>Man&amp;uCali</translation>
    </message>
    <message>
        <source>Scope</source>
        <translation>范围</translation>
    </message>
    <message>
        <source>CYEditText</source>
        <translation>CY编辑文本</translation>
    </message>
    <message>
        <source>SUPERVISOR</source>
        <translation type="vanished">检察员</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation type="vanished">检查员</translation>
    </message>
    <message>
        <source>Machine</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>Regulator</source>
        <translation type="vanished">校准</translation>
    </message>
    <message>
        <source>Local datas</source>
        <translation type="vanished">本地数据</translation>
    </message>
    <message>
        <source>Non-synchronous connection</source>
        <translation type="vanished">不同步连接</translation>
    </message>
    <message>
        <source>Synchronous connection</source>
        <translation type="vanished">同步连接</translation>
    </message>
    <message>
        <source>Simulation</source>
        <translation type="vanished">模拟</translation>
    </message>
    <message>
        <source>Designer</source>
        <translation type="vanished">设计员</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Digital inputs</source>
        <translation>数据输入</translation>
    </message>
    <message>
        <source>Analog inputs</source>
        <translation>模拟输入</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="vanished">温度</translation>
    </message>
    <message>
        <source>Trigonometry</source>
        <translation type="vanished">三角法</translation>
    </message>
    <message>
        <source>Leak rate</source>
        <translation type="vanished">泄漏率</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="vanished">压力</translation>
    </message>
    <message>
        <source>Percentage</source>
        <translation type="vanished">百分比</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="vanished">模拟输入</translation>
    </message>
    <message>
        <source>Stations inlet pressure</source>
        <translation type="vanished">站入口压力</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="vanished">港位置</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation type="vanished">站入口流体温度</translation>
    </message>
    <message>
        <source>Stations outlet fluid temperature</source>
        <translation type="vanished">站出口流体温度</translation>
    </message>
    <message>
        <source>Ramp 1 heater temperature</source>
        <translation type="vanished">斜升 1 加热器温度</translation>
    </message>
    <message>
        <source>Ramp 2 heater temperature</source>
        <translation type="vanished">斜升 2 加热器温度</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation type="vanished">站出口压力</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="vanished">爆裂压力</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="vanished">环境温度</translation>
    </message>
    <message>
        <source>Water inlet temperature</source>
        <translation type="vanished">进水温度</translation>
    </message>
    <message>
        <source>Water outlet temperature</source>
        <translation type="vanished">出水温度</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation type="vanished">模拟输出</translation>
    </message>
    <message>
        <source>Frequency converter VA1 (circulation pump)</source>
        <translation type="vanished">频率转换器 VA1（循环泵）</translation>
    </message>
    <message>
        <source>Servo cylinder control</source>
        <translation type="vanished">伺服缸控制</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Graphic analyse window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C&amp;ylix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Protected access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Change access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change &amp;password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Machine status backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Graphic analyse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;User administration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DBCal1</name>
    <message>
        <source>Calibration</source>
        <translation type="obsolete">校准</translation>
    </message>
</context>
<context>
    <name>DBCalM1</name>
    <message>
        <source>Metrology</source>
        <translation type="obsolete">度量</translation>
    </message>
</context>
<context>
    <name>DBCyc</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">项目</translation>
    </message>
</context>
<context>
    <name>DBEcl1</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">项目</translation>
    </message>
</context>
<context>
    <name>DBFonct1</name>
    <message>
        <source>Metrology</source>
        <translation type="obsolete">度量</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="obsolete">常规</translation>
    </message>
</context>
<context>
    <name>DBForce1</name>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
    <message>
        <source>Direct</source>
        <translation type="obsolete">直接</translation>
    </message>
</context>
<context>
    <name>DBGraf1</name>
    <message>
        <source>Acquisition</source>
        <translation type="obsolete">采集</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">启用</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation type="obsolete">监督</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">环境温度</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
    <message>
        <source>Process</source>
        <translation type="obsolete">程序</translation>
    </message>
    <message>
        <source>Test</source>
        <translation type="obsolete">测试</translation>
    </message>
</context>
<context>
    <name>DBInfoPC</name>
    <message>
        <source>Description</source>
        <translation type="obsolete">描述</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">名字</translation>
    </message>
</context>
<context>
    <name>DBPIDP1</name>
    <message>
        <source>Minimum value</source>
        <translation type="obsolete">最小值</translation>
    </message>
    <message>
        <source>Maximum value</source>
        <translation type="obsolete">最大值</translation>
    </message>
    <message>
        <source>Supervision</source>
        <translation type="obsolete">监督</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">爆裂压力</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">环境温度</translation>
    </message>
    <message>
        <source>Low value</source>
        <translation type="obsolete">低值</translation>
    </message>
    <message>
        <source>High value</source>
        <translation type="obsolete">高值</translation>
    </message>
</context>
<context>
    <name>DBPPID</name>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation type="obsolete">注意：这些设置属于当前项目</translation>
    </message>
</context>
<context>
    <name>DBPSet</name>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation type="obsolete">注意：这些设置属于当前项目</translation>
    </message>
</context>
<context>
    <name>DBPrg</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">项目</translation>
    </message>
</context>
<context>
    <name>DBPrj1</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">项目</translation>
    </message>
    <message>
        <source>Test</source>
        <translation type="obsolete">测试</translation>
    </message>
</context>
<context>
    <name>DBRN1</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">项目</translation>
    </message>
    <message>
        <source>Bench</source>
        <translation type="obsolete">试验台</translation>
    </message>
    <message>
        <source>Digital input</source>
        <translation type="obsolete">数字输入</translation>
    </message>
    <message>
        <source>Digital output</source>
        <translation type="obsolete">数字输出</translation>
    </message>
    <message>
        <source>Analog input</source>
        <translation type="obsolete">模拟输入</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation type="obsolete">站入口流体温度</translation>
    </message>
    <message>
        <source>Stations inlet pressure</source>
        <translation type="obsolete">站入口压力</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation type="obsolete">站出口压力</translation>
    </message>
    <message>
        <source>Ramp 1 heater temperature</source>
        <translation type="obsolete">斜升 1 加热器温度</translation>
    </message>
    <message>
        <source>Ramp 2 heater temperature</source>
        <translation type="obsolete">斜升 2 加热器温度</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation type="obsolete">模拟输出</translation>
    </message>
    <message>
        <source>Frequency converter VA1 (circulation pump)</source>
        <translation type="obsolete">频率转换器 VA1（循环泵）</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">爆裂压力</translation>
    </message>
    <message>
        <source>Metrology</source>
        <translation type="obsolete">度量</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">环境温度</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">维修</translation>
    </message>
    <message>
        <source>Level</source>
        <translation type="obsolete">等级</translation>
    </message>
</context>
<context>
    <name>DBRNL1</name>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">维修</translation>
    </message>
    <message>
        <source>Flag</source>
        <translation type="obsolete">标记</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">状态</translation>
    </message>
    <message>
        <source>Project</source>
        <translation type="obsolete">项目</translation>
    </message>
</context>
<context>
    <name>DBRNR1</name>
    <message>
        <source>Stations inlet pressure</source>
        <translation type="obsolete">站入口压力</translation>
    </message>
    <message>
        <source>Stations outlet pressure</source>
        <translation type="obsolete">站出口压力</translation>
    </message>
    <message>
        <source>Stations inlet fluid temperature</source>
        <translation type="obsolete">站入口流体温度</translation>
    </message>
    <message>
        <source>Stations outlet fluid temperature</source>
        <translation type="obsolete">站出口流体温度</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">环境温度</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">爆裂压力</translation>
    </message>
    <message>
        <source>Analog output</source>
        <translation type="obsolete">模拟输出</translation>
    </message>
    <message>
        <source>Event</source>
        <translation type="obsolete">项目</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation type="obsolete">故障</translation>
    </message>
</context>
<context>
    <name>DBSPV</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">项目</translation>
    </message>
</context>
<context>
    <name>DBSta1</name>
    <message>
        <source>Project</source>
        <translation type="obsolete">项目</translation>
    </message>
</context>
<context>
    <name>DBTest</name>
    <message>
        <source>Test</source>
        <translation type="obsolete">测试</translation>
    </message>
</context>
<context>
    <name>DangerousTemperature</name>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
</context>
<context>
    <name>DoorsOpening</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>EventsGeneratorRN1</name>
    <message>
        <source>Bench</source>
        <translation type="obsolete">试验台</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation type="obsolete">故障</translation>
    </message>
    <message>
        <source>Process</source>
        <translation type="obsolete">程序</translation>
    </message>
    <message>
        <source>Regulator</source>
        <translation type="obsolete">校准</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">警报</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation type="obsolete">检查员</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation type="obsolete">操作员</translation>
    </message>
    <message>
        <source>Command</source>
        <translation type="obsolete">指令</translation>
    </message>
    <message>
        <source>End</source>
        <translation type="obsolete">结束</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">维修</translation>
    </message>
</context>
<context>
    <name>EventsGeneratorSPV</name>
    <message>
        <source>Operator</source>
        <translation type="obsolete">操作员</translation>
    </message>
    <message>
        <source>Supervisor</source>
        <translation type="obsolete">检查员</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">警报</translation>
    </message>
</context>
<context>
    <name>EventsPanel</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>IOForcing</name>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>IOFrame</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>IOView</name>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
</context>
<context>
    <name>ListCali</name>
    <message>
        <source>Group</source>
        <translation type="obsolete">组</translation>
    </message>
    <message>
        <source>Label</source>
        <translation type="obsolete">标签</translation>
    </message>
    <message>
        <source>Phys</source>
        <translation type="obsolete">物理</translation>
    </message>
    <message>
        <source>Elec</source>
        <translation type="obsolete">电气</translation>
    </message>
</context>
<context>
    <name>MainWin</name>
    <message>
        <source>&amp;Test</source>
        <translation type="obsolete">&amp;测试</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">描述</translation>
    </message>
    <message>
        <source>Ambient air temperature</source>
        <translation type="obsolete">环境温度</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">爆裂压力</translation>
    </message>
    <message>
        <source>Initializing metrology window...</source>
        <translation type="obsolete">正在初始化度量窗口……</translation>
    </message>
    <message>
        <source>Metrology window</source>
        <translation type="obsolete">度量窗口</translation>
    </message>
    <message>
        <source>Operator</source>
        <translation type="obsolete">操作员</translation>
    </message>
    <message>
        <source>Test: %1 (Access: %2)</source>
        <translation type="obsolete">测试: %1 (通道: %2)</translation>
    </message>
</context>
<context>
    <name>MaintenanceCounters</name>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>NetInfo</name>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
</context>
<context>
    <name>Network</name>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;设计者评价</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>PEXDoorsOpening</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>PFBPID</name>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation type="obsolete">注意：这些设置属于当前项目</translation>
    </message>
    <message>
        <source>Bursting pressure</source>
        <translation type="obsolete">爆裂压力</translation>
    </message>
</context>
<context>
    <name>PFBSet</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>PFEPID</name>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation type="obsolete">注意：这些设置属于当前项目</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>POSAccel</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>POSCtrl</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
</context>
<context>
    <name>POSPID</name>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
</context>
<context>
    <name>POSPID_2SV</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>- </source>
        <translation type="obsolete">- </translation>
    </message>
    <message>
        <source>+ </source>
        <translation type="obsolete">+ </translation>
    </message>
</context>
<context>
    <name>POSSet</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>Page</name>
    <message>
        <source>Dynamic page example</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProcessCooling</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>ProcessDegassing</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>ProcessDrain</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>ProcessEmptying</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>ProcessLeak</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>ProcessSecurity</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>ProfileEdit</name>
    <message>
        <source>Pressure</source>
        <translation type="obsolete">压力</translation>
    </message>
    <message>
        <source>Fault</source>
        <translation type="obsolete">故障</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">警报</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="obsolete">温度</translation>
    </message>
    <message>
        <source>Control</source>
        <translation type="obsolete">控制</translation>
    </message>
</context>
<context>
    <name>ProjectDescription</name>
    <message>
        <source>Description</source>
        <translation type="obsolete">描述</translation>
    </message>
</context>
<context>
    <name>ProjectView</name>
    <message>
        <source>Type</source>
        <translation type="obsolete">类型</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">时间</translation>
    </message>
    <message>
        <source>0</source>
        <translation type="obsolete">0</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
</context>
<context>
    <name>ProjectWizard</name>
    <message>
        <source>&amp;Create</source>
        <translation type="obsolete">&amp;创建</translation>
    </message>
</context>
<context>
    <name>PulsatingTestMonitoring</name>
    <message>
        <source>Process</source>
        <translation type="obsolete">程序</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>0</source>
        <translation type="obsolete">0</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">时间</translation>
    </message>
    <message>
        <source>0,00</source>
        <translation type="obsolete">0,00</translation>
    </message>
</context>
<context>
    <name>PulsatingTestParts</name>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>Pv</name>
    <message>
        <source>Test directory</source>
        <translation type="obsolete">测试目录</translation>
    </message>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>File %1 at line %2</source>
        <translation>行 %2中的文件 %1</translation>
    </message>
    <message>
        <source>File %1 at line %2: </source>
        <translation>行 %2中文件 %1：</translation>
    </message>
    <message>
        <source>File %1 at line %2 !</source>
        <translation>文件 %1在行 %2中！</translation>
    </message>
    <message>
        <source>CYLIX DEBUG</source>
        <translation>CYLIX调试</translation>
    </message>
    <message>
        <source>File %1 at line %2 on object %3 !</source>
        <translation>对象%3在行%2的文件%1！</translation>
    </message>
    <message>
        <source>CYLIX OBJECT DEBUG</source>
        <translation>CYLIX 对象调试</translation>
    </message>
    <message>
        <source>File %1 at line %2 for data %3 !</source>
        <translation>数据%3在行%2的文件%1！</translation>
    </message>
    <message>
        <source>CYLIX DATA DEBUG</source>
        <translation>CYLIX 数据调试</translation>
    </message>
    <message>
        <source>File %1 at line %2 on object %3 for data %4 !</source>
        <translation>关于数据%4的对象%3在行%2的文件%1！</translation>
    </message>
    <message>
        <source>CYLIX OBJECT DATA DEBUG</source>
        <translation>CYLIX 对象数据调试</translation>
    </message>
    <message>
        <source>CYLIX ERROR</source>
        <translation>CYLIX 错误</translation>
    </message>
    <message>
        <source>CYLIX OBJECT ERROR</source>
        <translation>CYLIX 对象错误</translation>
    </message>
    <message>
        <source>CYLIX DATA ERROR</source>
        <translation>CYLIX 数据错误</translation>
    </message>
    <message>
        <source>CYLIX OBJECT DATA ERROR</source>
        <translation>CYLIX 对象数据错误</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 !</source>
        <translation>行%2中的源代码文件%1错误！</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 on object %3 !</source>
        <translation>对象%3在行%2的源代码文件%1错误！</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 for data %3 !</source>
        <translation>数据%3在行%2的源代码文件%1错误！</translation>
    </message>
    <message>
        <source>Error in source file %1 at line %2 on object %3 for data %4 !</source>
        <translation>关于数据%4的对象%3在行%2的源代码文件%1错误！</translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is higher than maximum value (%3&gt;%4) !</source>
        <translation>设计值%1:%2高于最大值(%3&gt;%4)!</translation>
    </message>
    <message>
        <source>Designer value of %1:%2 is smaller than minimum value (%3&lt;%4) !</source>
        <translation>设计值%1:%2小于最小值(%3︿%4) !</translation>
    </message>
    <message>
        <source>Choice:</source>
        <translation>选择：</translation>
    </message>
    <message>
        <source>Value from %1 to %2 %3.</source>
        <translation>%1至%2%3的值。</translation>
    </message>
    <message>
        <source>Precision is %1 %2.</source>
        <translation>精度是%1 %2。</translation>
    </message>
    <message>
        <source>Designer value:</source>
        <translation>设计者价值</translation>
    </message>
    <message>
        <source>Designer value: %1 %2.</source>
        <translation>设计值：%1 %2。</translation>
    </message>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation>活动视图中存在未保存的更改。
是否应用或放弃此更改?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation>未保存更改</translation>
    </message>
    <message>
        <source>replace this with information about your translation team</source>
        <comment>&lt;p&gt;KDE is translated into many languages thanks to the work of the translation teams all over the world.&lt;/p&gt;&lt;p&gt;For more information on KDE internationalization visit &lt;a href=&quot;http://l10n.kde.org&quot;&gt;http://l10n.kde.org&lt;/a&gt;&lt;/p&gt;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No licensing terms for this program have been specified.
Please check the documentation or the source for any
licensing terms.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This program is distributed under the terms of the %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 at line %2 : error in sheet %3 for one or multiple datas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 !
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QwtPlotRenderer</name>
    <message>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export File Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SealingTest</name>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>SealingTestView</name>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Type</source>
        <translation type="obsolete">类型</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>&amp;Designer Value</source>
        <translation type="obsolete">&amp;设计者评价</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation type="obsolete">Alt+D</translation>
    </message>
    <message>
        <source>Appl&amp;y</source>
        <translation type="obsolete">应用</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation type="obsolete">Alt+Y</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>There are unsaved changes in the active view.
Do you want to apply or discard this changes?</source>
        <translation type="obsolete">活动视图中存在未保存的更改。
是否应用或放弃此更改?</translation>
    </message>
    <message>
        <source>Unsaved changes</source>
        <translation type="obsolete">未保存更改</translation>
    </message>
</context>
<context>
    <name>SpvCurvesDisplay</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>SpvCustomerData</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
</context>
<context>
    <name>SpvPV</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>StartBursting</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>StartCooling</name>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>StartDegassing</name>
    <message>
        <source>=</source>
        <translation type="obsolete">=</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>StartEmptying</name>
    <message>
        <source>Alt+A</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>StartPulsating</name>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
</context>
<context>
    <name>StartStatic</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>&amp;4</source>
        <translation type="obsolete">&amp;4</translation>
    </message>
    <message>
        <source>&amp;5</source>
        <translation type="obsolete">&amp;5</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>&amp;3</source>
        <translation type="obsolete">&amp;3</translation>
    </message>
    <message>
        <source>&amp;1</source>
        <translation type="obsolete">&amp;1</translation>
    </message>
    <message>
        <source>&amp;6</source>
        <translation type="obsolete">&amp;6</translation>
    </message>
    <message>
        <source>&amp;7</source>
        <translation type="obsolete">&amp;7</translation>
    </message>
    <message>
        <source>&amp;9</source>
        <translation type="obsolete">&amp;9</translation>
    </message>
    <message>
        <source>11</source>
        <translation type="obsolete">11</translation>
    </message>
    <message>
        <source>&amp;2</source>
        <translation type="obsolete">&amp;2</translation>
    </message>
    <message>
        <source>&amp;8</source>
        <translation type="obsolete">&amp;8</translation>
    </message>
    <message>
        <source>1&amp;0</source>
        <translation type="obsolete">1&amp;0</translation>
    </message>
</context>
<context>
    <name>StaticEdit</name>
    <message>
        <source>Fault</source>
        <translation type="obsolete">故障</translation>
    </message>
    <message>
        <source>Alert</source>
        <translation type="obsolete">警报</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="obsolete">温度</translation>
    </message>
    <message>
        <source>Control</source>
        <translation type="obsolete">控制</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="obsolete">压力</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">时间</translation>
    </message>
</context>
<context>
    <name>StaticTestMonitoring</name>
    <message>
        <source>Process</source>
        <translation type="obsolete">程序</translation>
    </message>
    <message>
        <source>Cylinder position</source>
        <translation type="obsolete">港位置</translation>
    </message>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>Pressure</source>
        <translation type="obsolete">压力</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">时间</translation>
    </message>
    <message>
        <source>0,00</source>
        <translation type="obsolete">0,00</translation>
    </message>
</context>
<context>
    <name>StaticTestParts</name>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>11</source>
        <translation type="obsolete">11</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
</context>
<context>
    <name>Synoptic</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
</context>
<context>
    <name>SynopticAC</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
</context>
<context>
    <name>TAAPID</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
</context>
<context>
    <name>TFEPID</name>
    <message>
        <source>textLabel</source>
        <translation type="obsolete">文字标签</translation>
    </message>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation type="obsolete">注意：这些设置属于当前项目</translation>
    </message>
</context>
<context>
    <name>TFESet</name>
    <message>
        <source>Beware: These settings are attached to the current project</source>
        <translation type="obsolete">注意：这些设置属于当前项目</translation>
    </message>
</context>
<context>
    <name>TargetWidget</name>
    <message>
        <source>Please specify the folder where Cylix will be installed.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TestDescription</name>
    <message>
        <source>Operator</source>
        <translation type="obsolete">操作员</translation>
    </message>
    <message>
        <source>8</source>
        <translation type="obsolete">8</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="obsolete">1</translation>
    </message>
    <message>
        <source>3</source>
        <translation type="obsolete">3</translation>
    </message>
    <message>
        <source>7</source>
        <translation type="obsolete">7</translation>
    </message>
    <message>
        <source>4</source>
        <translation type="obsolete">4</translation>
    </message>
    <message>
        <source>2</source>
        <translation type="obsolete">2</translation>
    </message>
    <message>
        <source>5</source>
        <translation type="obsolete">5</translation>
    </message>
    <message>
        <source>6</source>
        <translation type="obsolete">6</translation>
    </message>
    <message>
        <source>10</source>
        <translation type="obsolete">10</translation>
    </message>
    <message>
        <source>9</source>
        <translation type="obsolete">9</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="obsolete">12</translation>
    </message>
    <message>
        <source>O&amp;k</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Ca&amp;ncel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="obsolete">评论</translation>
    </message>
</context>
<context>
    <name>WaitStartBursting</name>
    <message>
        <source>&amp;Ok</source>
        <translation type="obsolete">&amp;Ok</translation>
    </message>
</context>
</TS>
