################################################################
# Cylix Library
# Copyright (C) 2015-2018   Gérald Le Cléach
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the Cy License, Version 1.0
################################################################

CY_VERSION = 5.63
DEFINES += DEF_CY_VERSION='"\\\"$${CY_VERSION}\\\""'

######################################################################
# Install paths
######################################################################
defined(CY_INSTALL_PREFIX, var){
  #défini dans l'application (ex. cylix.pri)
}
else{
#sinon définition pour installation
unix {
    CY_INSTALL_PREFIX = /usr/local/Cylix/Cylibs/$${CY_VERSION}-qt-$$QT_VERSION
}
windows {
    CY_INSTALL_PREFIX = C:/Cylix/Cylibs/$${CY_VERSION}-qt-$$QT_VERSION
}
}

CY_INSTALL_DOC     = $${CY_INSTALL_PREFIX}/doc
CY_INSTALL_CYDOC   = $${CY_INSTALL_DOC}/cydoc
CY_INSTALL_HEADERS = $${CY_INSTALL_PREFIX}/include
CY_INSTALL_LIBS    = $${CY_INSTALL_PREFIX}/lib
CY_INSTALL_BIN     = $${CY_INSTALL_PREFIX}/bin
CY_INSTALL_LANG    = $${CY_INSTALL_PREFIX}/languages
CY_INSTALL_LINUX   = $${CY_INSTALL_PREFIX}/linux
CY_INSTALL_WINDOWS = $${CY_INSTALL_PREFIX}/windows

DEFINES += CY_STR_INSTALL=\\\"$$CY_INSTALL_PREFIX\\\"
DEFINES += CY_STR_INSTALL_DOC=\\\"$$CY_INSTALL_DOC\\\"
DEFINES += CY_STR_INSTALL_CYDOC=\\\"$$CY_INSTALL_CYDOC\\\"
DEFINES += CY_STR_INSTALL_BIN=\\\"$$CY_INSTALL_BIN\\\"
DEFINES += CY_STR_INSTALL_LANG=\\\"$$CY_INSTALL_LANG\\\"
DEFINES += CY_STR_INSTALL_LINUX=\\\"$$CY_INSTALL_LINUX\\\"
DEFINES += CY_STR_INSTALL_WINDOWS=\\\"$$CY_INSTALL_WINDOWS\\\"

DEFINES += DEF_QT_INSTALL_PREFIX=\\\"$$[QT_INSTALL_PREFIX]\\\"

######################################################################
# Designer plugin
# creator/designer load designer plugins from certain default
# directories ( f.e the path below QT_INSTALL_PREFIX ) and the
# directories listed in the QT_PLUGIN_PATH environment variable.
# When using the path below CY_INSTALL_PREFIX you need to
# add $${CY_INSTALL_PREFIX}/plugins to QT_PLUGIN_PATH in the
# runtime environment of designer/creator.
######################################################################

CY_INSTALL_PLUGINS   = $${CY_INSTALL_PREFIX}/plugins/designer

# linux distributors often organize the Qt installation
# their way and QT_INSTALL_PREFIX doesn't offer a good
# path. Also QT_INSTALL_PREFIX is only one of the default
# search paths of the designer - not the Qt creator

#CY_INSTALL_PLUGINS   = $$[QT_INSTALL_PREFIX]/plugins/designer

######################################################################
# Features
# When building a Cy application with qmake you might want to load
# the compiler/linker flags, that are required to build a Cy application
# from cy.prf. Therefore all you need to do is to add "CONFIG += cy"
# to your project file and take care, that cy.prf can be found by qmake.
# ( see http://doc.trolltech.com/4.7/qmake-advanced-usage.html#adding-new-configuration-features )
# I recommend not to install the Cy features together with the
# Qt features, because you will have to reinstall the Cy features,
# with every Qt upgrade.
######################################################################

CY_INSTALL_FEATURES  = $${CY_INSTALL_PREFIX}/features
# CY_INSTALL_FEATURES  = $$[QT_INSTALL_PREFIX]/features

######################################################################
# Build the static/shared libraries.
# If CyDll is enabled, a shared library is built, otherwise
# it will be a static library.
######################################################################

CY_CONFIG           += CyDll

######################################################################
# CyPlot enables all classes, that are needed to use the CyPlot
# widget.
######################################################################

CY_CONFIG       += CyPlot

######################################################################
# CyWidgets enables all classes, that are needed to use the all other
# widgets (sliders, dials, ...), beside CyPlot.
######################################################################

CY_CONFIG     += CyWidgets

######################################################################
# If you want to display svg images on the plot canvas, or
# export a plot to a SVG document
######################################################################

CY_CONFIG     += CySvg

######################################################################
# If you want to use a OpenGL plot canvas
######################################################################

CY_CONFIG     += CyOpenGL

######################################################################
# You can use the MathML renderer of the Qt solutions package to
# enable MathML support in Cy. Because of license implications
# the ( modified ) code of the MML Widget solution is included and
# linked together with the CyMathMLTextEngine into an own library.
# To use it you will have to add "CONFIG += Cymathml"
# to your qmake project file.
######################################################################

#CY_CONFIG     += CyMathML

######################################################################
# If you want to build the Cy designer plugin,
# enable the line below.
# Otherwise you have to build it from the designer directory.
######################################################################

CY_CONFIG     += CyDesigner

######################################################################
# Compile all Cy classes into the designer plugin instead
# of linking it against the shared Cy library. Has no effect
# when CyDesigner or CyDll are not both enabled.
#
# On systems where rpath is supported ( all Unixoids ) the
# location of the installed Cy library is compiled into the plugin,
# but on Windows it might be easier to have a self contained
# plugin to avoid any hassle with configuring the runtime
# environment of the designer/creator.
######################################################################

windows {
#    CY_CONFIG     += CyDesignerSelfContained
}

######################################################################
# If you want to auto build the examples, enable the line below
# Otherwise you have to build them from the examples directory.
######################################################################

CY_CONFIG     += CyExamples

######################################################################
# The playground is primarily intended for the Cy development
# to explore and test new features. Nevertheless you might find
# ideas or code snippets that help for application development
# If you want to auto build the applications in playground, enable
# the line below.
# Otherwise you have to build them from the playground directory.
######################################################################

#CY_CONFIG     += CyPlayground

######################################################################
# When Qt has been built as framework qmake wants
# to link frameworks instead of regular libs
######################################################################

macx:!static:CONFIG(qt_framework, qt_framework|qt_no_framework) {

    CY_CONFIG += CyFramework
}

######################################################################
# Create and install pc files for pkg-config
# See http://www.freedesktop.org/wiki/Software/pkg-config/
######################################################################

unix {

    #CY_CONFIG     += CyPkgConfig
}
