<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>Page</name>
    <message>
        <source>Dynamic page example</source>
        <translation type="obsolete">Exemple de page dynamique</translation>
    </message>
</context>
<context>
    <name>TargetWidget</name>
    <message>
        <source>Please specify the folder where Cylix will be installed.</source>
        <translation>Veuillez spécifier le dossier où Cylix sera installé.</translation>
    </message>
</context>
<context>
    <name>installscript</name>
    <message>
        <source>A previous installation exists in this folder. If you wish to continue, everything will be overwritten.</source>
        <translation type="obsolete">Une installation précédente existe dans ce dossier. Si vous souhaitez continuer, tout sera écrasé.</translation>
    </message>
</context>
</TS>
