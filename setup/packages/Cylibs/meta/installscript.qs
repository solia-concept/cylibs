// Constructor
function Component()
{
    component.loaded.connect(this, Component.prototype.installerLoaded);
    installer.setDefaultPageVisible(QInstaller.TargetDirectory, false);
    installer.installationFinished.connect(this,Component.prototype.installationFinished);
}

// Utility function like QString QDir::toNativeSeparators(const QString & pathName) [static]
var Dir = new function () {
    this.toNativeSparator = function (path) {
        if (installer.value("os") == "win")
            return path.replace(/\//g, '\\');
        return path;
    }
};

// Called as soon as the component was loaded
Component.prototype.installerLoaded = function()
{
    if (installer.addWizardPage(component, "TargetWidget", QInstaller.TargetDirectory)) {
        var widget = gui.pageWidgetByObjectName("DynamicTargetWidget");
        if (widget != null) {
            widget.targetDirectory.textChanged.connect(this, Component.prototype.targetChanged);
            widget.targetChooser.clicked.connect(this, Component.prototype.chooseTarget);

            widget.windowTitle = "Installation Folder";
            widget.targetDirectory.text = Dir.toNativeSparator(installer.value("TargetDir"));
        }
    }
}

// Callback when one is clicking on the button to select where to install your application
Component.prototype.chooseTarget = function () {
    var widget = gui.pageWidgetByObjectName("DynamicTargetWidget");
    if (widget != null) {
        var newTarget = QFileDialog.getExistingDirectory("Choose your target directory.", widget.targetDirectory.text);
        if (newTarget != "") {
            widget.targetDirectory.text = Dir.toNativeSparator(newTarget);
        }
    }
}

Component.prototype.targetChanged = function (text) {
    var widget = gui.pageWidgetByObjectName("DynamicTargetWidget");
    if (widget != null) {
        if (text != "") {
            widget.complete = true;
            installer.setValue("TargetDir", text);
            if (installer.fileExists(text + "/components.xml")) {
                var warning = "<font color='red'>" + qsTr("A previous installation exists in this folder. If you wish to continue, everything will be overwritten.") + "</font>";
                widget.labelOverwrite.text = warning;
            } else {
                widget.labelOverwrite.text = "";
            }
            return;
        }
        widget.complete = false;
    }
}

Component.prototype.targetChanged = function (text)
{
    var widget = gui.currentPageWidget(); // get the current wizard page
    var install = false;

    if (widget != null)
    {
        if (text != "")
        {
            if (installer.fileExists(text + "/components.xml"))
            {
                var result = QMessageBox.question("quit.question", "Installer", "Do you want to overwrite previous installation?",
                    QMessageBox.Yes | QMessageBox.No);
                if (result == QMessageBox.Yes)
                {
                   install = true;
                }
            }
            else
                install = true;
        }
        else
            install = false;
    }

    widget.complete = install;

    if(install)
        installer.setValue("TargetDir", text);
}
/*
Controller.prototype.TargetDirectoryPageCallback = function()
{
    var widget = gui.currentPageWidget();
    widget.TargetDirectoryLineEdit.textChanged.connect(this, Controller.prototype.targetChanged);
    Controller.prototype.targetChanged(widget.TargetDirectoryLineEdit.text);
}

Controller.prototype.targetChanged = function (text)
{
    installer.setValue("RemoveTargetDir", true);
    if (text != "" && installer.fileExists(text + "/components.xml")) {
        if(QMessageBox.question("OverwriteTargetDirectory", "Overwrite target directory", "Do you want to overwrite previous installation?", QMessageBox.Yes | QMessageBox.No) == QMessageBox.Yes)
        {
            installer.setValue("RemoveTargetDir", false);
            }
    }
}*/

Component.prototype.createOperations = function()
{
try {
    // call the base create operations function
    component.createOperations();
    if (installer.value("os") == "win")
    {
        try
        {
component.addOperation("CreateShortcut", "@TargetDir@/bin/cylibstest.exe", "@StartMenuDir@/Cylibs.lnk");
        } catch (e)
        {
            // Do nothing if key doesn't exist
        }
    }
    } catch (e) {
    print(e);
    }

    var editionName = "Cylibs"
/*    if (!(installer.value("QT_EDITION_NAME") === ""))
        editionName = installer.value("QT_EDITION_NAME");*/

    // Create uninstall link only for windows
    if (installer.value("os") == "win")
    {
        // shortcut to uninstaller
        component.addOperation( "CreateShortcut",
                                "@TargetDir@/MaintenanceTool.exe",
                                "@StartMenuDir@/Uninstall " + editionName + ".lnk",
                                " --uninstall");
    }
    // only for windows online installer
    if ( installer.value("os") == "win" && !installer.isOfflineOnly() )
    {
        // create shortcut
        component.addOperation( "CreateShortcut",
                                "@TargetDir@/MaintenanceTool.exe",
                                "@StartMenuDir@/" + editionName + " Maintenance Tool.lnk",
                                "workingDirectory=@TargetDir@" );
    }

    if ( installer.value("os") == "x11" )
    {
        // only for online installer
        if (!installer.isOfflineOnly())
  {
            component.addOperation( "CreateDesktopEntry",
                                    editionName + "-MaintenanceTool.desktop",
                                    "Type=Application\nExec=@TargetDir@/MaintenanceTool\nPath=@TargetDir@\nName=" + editionName + " Maintenance Tool\nGenericName=Install or uninstall Qt components.\nIcon=QtIcon\nTerminal=false\nCategories=Development;Qt;"
                                   );
        }

     }
/*    component.createOperations();

    if (systemInfo.productType === "windows") {
        component.addOperation("CreateShortcut",
                "@TargetDir@/bin/cylibstest.exe",
                "@AllUsersStartMenuProgramsPath@/@StartMenuGroup@/Cylix/Cylibs/cylibstest.lnk",
                "workingDirectory=@TargetDir@",
                "iconPath=@TargetDir@/cylibstest.ico");
        }*/
     if (installer.value("os") == "win")
     {
       component.addOperation("Execute", "/C", "@TargetDir@/windows/post_install.bat", "@TargetDir@", "C:/Qt/5.15.2/mingw81_64")
     }
     if ( installer.value("os") == "x11" )
     {
        component.addOperation("Execute", "@TargetDir@/linux/post_install.sh", "@TargetDir@", "/opt/Qt/5.15.2/gcc_64")
     }
}

Component.prototype.installationFinished = function()
{
    try {

    if (installer.isInstaller() && installer.status == QInstaller.Success)
    {
    }
    } catch(e) {
    console.log(e);
    }

}

