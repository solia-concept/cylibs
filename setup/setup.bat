@echo on

REM Script de deploiement Windows pour Qt utilisant windeployqt qui prend
REM une les librairies Cylibs comme entree et la rend autonome en copiant
REM les  bibliotheques Qt et les plugins utilises.

set TMP=REPERTOIRE APPLICATION ACTUELLEMENT INSTALLEE
IF [%1]==[] (echo ERREUR: MANQUE %TMP% exit /b 1)
set APPDIR=%1
echo %APPDIR%

set TMP=SOUS-REPERTOIRE LOCAL PACKAGES/CYLIBS
IF [%2]==[] (echo ERREUR: MANQUE %TMP% exit /b 2)
set PACKDIR=%2
echo %PACKDIR%

echo Remplacement des "/" par "\"
set "APPDIR=%APPDIR:/=\%"
echo %APPDIR%

echo Remplacement des "/" par "\"
set "PACKDIR=%PACKDIR:/=\%"
echo %PACKDIR%

set DATA=%PACKDIR%\data
for /f "tokens=*" %%i in ('dir %DATA%\*.* /AD /B') do (RD "%DATA%\%%i" /S /Q)
echo o | del %DATA%\*.*

xcopy "%APPDIR%\*" /Y /K /S "%DATA%"
