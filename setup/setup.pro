#-------------------------------------------------------------------
# Fichier setup de Cylibs pour QtCreator
# Copyright (C) 2016-2017   Gérald Le Cléach
#
# Cylibs is free software; you can redistribute it and/or
# modify it under the terms of the LGPL License, Version 3.0
#
# Le répertoire setup permet de réaliser des fichiers d'installation
# ou de mise à jour logicielle via QtInstaller sous Linux ou sous
# Windows (voir Qt_Installer.txt)
#-------------------------------------------------------------------

TEMPLATE = aux

DISTFILES = \
    $$PWD/Qt_Installer.txt \
    $$PWD/config/config.xml \
    $$PWD/packages/Cylibs/linuxdeployqt.sh \
    $$PWD/packages/Cylibs/meta/package.xml \
    $$PWD/packages/Cylibs/meta/installscript.qs \
    $$PWD/packages/Cylibs/meta/lgpl-3.0.txt \
    $$PWD/packages/Cylibs/linuxdeployqt-continuous-x86_64.AppImage \
    setup.sh \
    setup.bat

FORMS += \
    $$PWD/packages/Cylibs/meta/page.ui \
    $$PWD/packages/Cylibs/meta/targetwidget.ui \
    packages/Cylibs/meta/page.ui \
    packages/Cylibs/meta/targetwidget.ui
