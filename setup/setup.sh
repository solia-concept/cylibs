#!/bin/bash
 
########################################################################
# Script de déploiement Linux pour Qt utilisant linuxdeployqt qui prend
# les librairies Cylibs comme entrée et la rend autonome en copiant
# les  bibliothèques Qt et les plugins utilisés.
#
# Avant de l'utiliser il faut installe patchelf :
# sudo apt-get install patchelf
# et ajuster le chemin Qt en modifiant:
# /usr/lib/x86_64-linux-gnu/qt-default/qtchooser/default.conf
########################################################################
 
set -e
set -x
 
APPNAME=$1 # nom de l'exécutable Cylix
APPDIR=$2  # répertoire système de l'application installée par make install
PACKDIR=$3 # répertoire packages/Cylix

if [ "$APPNAME" = "" ] ; then
  echo "ERREUR MANQUE APPNAME: nom de l'exécutable Cylix"
  exit $?
fi

if [ "$APPDIR" = "" ] ; then
    echo "ERREUR MANQUE APPDIR: répertoire système de l'application installée par make install"
    exit $?
fi

if [ "$PACKDIR" = "" ] ; then
  echo "ERREUR MANQUE APPDIR: répertoire système de l'application installée par make install"
  exit $?
fi

rm -fr --verbose ${PACKDIR}/data/*
cp -r ${APPDIR}/* ${PACKDIR}/data

#nom de l'exécutable avec son chemin absolue
BINARY=${PACKDIR}/data/bin/${APPNAME}

./linuxdeployqt-5-x86_64.AppImage "${BINARY}" -qmldir=${PACKDIR}/data -bundle-non-qt-libs || true
./linuxdeployqt-5-x86_64.AppImage "${BINARY}" -qmldir=${PACKDIR}/data -appimage

ls ${PACKDIR}
