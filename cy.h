/***************************************************************************
                          cy.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CY_H
#define CY_H

// ANSI
#include <stdlib.h>
// QT
#include <QtGlobal>
#include <QMessageBox>
#include <QString>
// CY
#include "cytypes.h"
#include "scmdb_t.h"

#if defined(Q_OS_WIN)
  #include <winsock2.h>
  #include <sys/timeb.h>
  #include <process.h>
#elif defined(Q_OS_LINUX)
  #include <sys/socket.h>
  #include <sys/resource.h>
  #include <netinet/in.h>
  #include <netinet/ip.h>
  #include <netinet/tcp.h>
  #include <arpa/inet.h>
  #include <netdb.h>
#endif

/** CY regroupe plusieurs groupes de définitions propres à CY.
  * @author Gérald LE CLEACH
  */


/** Marquage de texte en français.. */
#define TRFR QString

// BASE TEMPORELLES
/** Heures. */
#define BASE_HOUR   3600000.0
/** Minutes. */
#define BASE_MIN      60000.0
/** Secondes. */
#define BASE_SEC       1000.0
/** Millisecondes. */
#define BASE_MSEC         1.0

// EVENEMENTS CYLIBS
/** Suppression d'une cellule d'analyse CYAnalyseCell. */
#define RemoveAnalyseCell   2000

//   TRAITEMENT ET AFFICHAGE DES EVENEMENTS
#define BST    M00  // =>  Message bistable (Acquittements dans journal)
#define SCR    M01  // =>  Affichage à l'écran (Implicite pour process)
#define REC    M02  // =>  Enregistrement dans le journal d'évènements
#define RAZ    M03  // =>  Remise à zéro dès prise en compte par evnt

#define SI_0   false    // actif si à 0
#define SI_1   true     // actif si à 1


/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro.
	* Cette macro ne doit être que temporaire. Si elle doit rester dans les sources il vaut mieux utiliser CYMESSAGE. */
#define CYDEBUG \
{ \
	QString msg = QObject::tr("File %1 at line %2").arg(__FILE__).arg(__LINE__); \
	qDebug("%s", msg.toUtf8().constData());\
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi qu'un text explicatif.
  * Cette macro ne doit être que temporaire. Si elle doit rester dans les sources il vaut mieux utiliser CYMESSAGETEXT. */
#define CYDEBUGTEXT(text) \
{ \
	QString msg = QObject::tr("File %1 at line %2: ").arg(__FILE__).arg(__LINE__)+text; \
	qDebug("%s", msg.toUtf8().constData());\
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro. */
#define CYDEBUGBOX \
{ \
  QString msg = QObject::tr("File %1 at line %2 !").arg(__FILE__).arg(__LINE__); \
  QMessageBox::information(0, QObject::tr("CYLIX DEBUG"), msg); \
  qWarning("%s", msg.toUtf8().constData());\
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi qu'un text explicatif. */
#define CYDEBUGBOXTEXT(text) \
{ \
  QString msg = QObject::tr("File %1 at line %2 !").arg(__FILE__).arg(__LINE__)+"\n"+text; \
  QMessageBox::information(0, QObject::tr("CYLIX DEBUG"), msg); \
  qWarning("%s", msg.toUtf8().constData()); \
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet qui a généré cette erreur. */
#define CYDEBUGBOXOBJECT(object) \
{ \
  QString msg = QObject::tr("File %1 at line %2 on object %3 !").arg(__FILE__).arg(__LINE__).arg(object); \
  QMessageBox::information(0, QObject::tr("CYLIX OBJECT DEBUG"), msg); \
  qWarning("%s", msg.toUtf8().constData());\
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur. */
#define CYDEBUGBOXDATA(data) \
{ \
  QString msg = QObject::tr("File %1 at line %2 for data %3 !").arg(__FILE__).arg(__LINE__).arg(data); \
  QMessageBox::information(0, QObject::tr("CYLIX DATA DEBUG"), msg); \
  qWarning("%s", msg.toUtf8().constData()); \
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur. */
#define CYDEBUGBOXOBJECTDATA(object, data) \
{ \
  QString msg = QObject::tr("File %1 at line %2 on object %3 for data %4 !").arg(__FILE__).arg(__LINE__).arg(object).arg(data); \
  QMessageBox::information(0, QObject::tr("CYLIX OBJECT DATA DEBUG"), msg); \
  qWarning("%s", msg.toUtf8().constData()); \
}


/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro. */
#define CYMESSAGE \
{ \
	QString msg = QObject::tr("File %1 at line %2").arg(__FILE__).arg(__LINE__); \
	qDebug("%s", msg.toUtf8().constData());\
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
	* ainsi qu'un text explicatif. */
#define CYMESSAGETEXT(text) \
{ \
	QString msg = QObject::tr("File %1 at line %2: ").arg(__FILE__).arg(__LINE__)+text; \
	qDebug("%s", msg.toUtf8().constData());\
}


/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro.
  * Cette macro ne doit pas être temporaire. */
#define CYWARNING \
{ \
  QString msg = QObject::tr("File %1 at line %2 !").arg(__FILE__).arg(__LINE__); \
  QMessageBox::warning(0, QObject::tr("CYLIX ERROR"), msg); \
  qWarning("%s", msg.toUtf8().constData());\
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi qu'un text explicatif.
  * Cette macro ne doit pas être temporaire. */
#define CYWARNINGTEXT(text) \
{ \
  QString msg = QObject::tr("File %1 at line %2 !").arg(__FILE__).arg(__LINE__)+"\n"+text; \
  QMessageBox::warning(0, QObject::tr("CYLIX ERROR"), msg); \
  qWarning("%s", msg.toUtf8().constData()); \
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet qui a généré cette erreur.
  * Cette macro ne doit pas être temporaire. */
#define CYWARNINGOBJECT(object) \
{ \
  QString msg = QObject::tr("File %1 at line %2 on object %3 !").arg(__FILE__).arg(__LINE__).arg(object); \
  QMessageBox::warning(0, QObject::tr("CYLIX OBJECT ERROR"), msg); \
  qWarning("%s", msg.toUtf8().constData());\
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur.
  * Cette macro ne doit pas être temporaire. */
#define CYWARNINGDATA(data) \
{ \
  QString msg = QObject::tr("File %1 at line %2 for data %3 !").arg(__FILE__).arg(__LINE__).arg(data); \
  QMessageBox::warning(0, QObject::tr("CYLIX DATA ERROR"), msg); \
  qWarning("%s", msg.toUtf8().constData()); \
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur.
  * Cette macro ne doit pas être temporaire. */
#define CYWARNINGOBJECTDATA(object, data) \
{ \
  QString msg = QObject::tr("File %1 at line %2 on object %3 for data %4 !").arg(__FILE__).arg(__LINE__).arg(object).arg(data); \
  QMessageBox::warning(0, QObject::tr("CYLIX OBJECT DATA ERROR"), msg); \
  qWarning("%s", msg.toUtf8().constData()); \
}


/** Lance un QMessageBox::critical contenant le nom du fichier et le numéro de ligne où est placée cette macro. */
#define CYFATAL \
{ \
  QString msg = QObject::tr("Error in source file %1 at line %2 !").arg(__FILE__).arg(__LINE__); \
  QMessageBox::critical(0, QObject::tr("CYLIX ERROR"), msg); \
  qFatal("%s", msg.toUtf8().constData()); \
}

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi qu'un text explicatif.
  * Cette macro ne doit être que temporaire. Si elle doit rester dans les sources il vaut mieux utiliser CYFATALOBJECT. */
#define CYFATALTEXT(text) \
{ \
  QString msg = QObject::tr("Error in source file %1 at line %2 !").arg(__FILE__).arg(__LINE__)+"\n"+text; \
  QMessageBox::critical(0, QObject::tr("CYLIX ERROR"), msg); \
  qFatal("%s", msg.toUtf8().constData()); \
}

/** Lance un QMessageBox::critical contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet qui a généré cette erreur. */
#define CYFATALOBJECT(object) \
{ \
  QString msg = QObject::tr("Error in source file %1 at line %2 on object %3 !").arg(__FILE__).arg(__LINE__).arg(object); \
  QMessageBox::critical(0, QObject::tr("CYLIX OBJECT ERROR"), msg); \
  qFatal("%s", msg.toUtf8().constData()); \
}

/** Lance un QMessageBox::critical contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet qui a généré cette erreur. */
#define CYFATALDATA(data) \
{ \
  QString msg = QObject::tr("Error in source file %1 at line %2 for data %3 !").arg(__FILE__).arg(__LINE__).arg(data); \
  QMessageBox::critical(0, QObject::tr("CYLIX DATA ERROR"), msg); \
  qFatal("%s", msg.toUtf8().constData()); \
}

/** Lance un QMessageBox::critical contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur. */
#define CYFATALOBJECTDATA(object, data) \
{ \
  QString msg = QObject::tr("Error in source file %1 at line %2 on object %3 for data %4 !").arg(__FILE__).arg(__LINE__).arg(object).arg(data); \
  QMessageBox::critical(0, QObject::tr("CYLIX OBJECT DATA ERROR"), msg); \
  qFatal("%s", msg.toUtf8().constData()); \
}

#ifdef __cplusplus

#if !defined(QT_NAMESPACE) || defined(Q_MOC_RUN) /* user namespace */
# define CY_BEGIN_NAMESPACE
# define CY_END_NAMESPACE
#else /* user namespace */
# define CY_BEGIN_NAMESPACE namespace QT_NAMESPACE {
# define CY_END_NAMESPACE }
#endif /* user namespace */

#else /* __cplusplus */

# define CY_BEGIN_NAMESPACE
# define CY_END_NAMESPACE

#endif /* __cplusplus */


//#if !defined(QT_NAMESPACE) || defined(Q_MOC_RUN) /* user namespace */
//# define QT_BEGIN_NAMESPACE
//# define QT_END_NAMESPACE
//#else /* user namespace */

CY_BEGIN_NAMESPACE
//# define QT_BEGIN_NAMESPACE namespace QT_NAMESPACE {
//# define QT_END_NAMESPACE }

#if !defined(Q_QDOC) && !defined(Q_MOC_RUN)
struct QMetaObject;
const QMetaObject *qt_getQtMetaObject() Q_DECL_NOEXCEPT; // defined in qobject.h (which can't be included here)
#define CY_Q_ENUM(ENUM) \
    inline const QMetaObject *cy_getEnumMetaObject(ENUM) Q_DECL_NOEXCEPT { return qt_getQtMetaObject(); } \
    inline Q_DECL_CONSTEXPR const char *cy_getEnumName(ENUM) Q_DECL_NOEXCEPT { return #ENUM; }
#define CY_Q_FLAG(ENUM) CY_Q_ENUM(ENUM)
#else
#define QT_Q_ENUM Q_ENUM
#define QT_Q_FLAG Q_FLAG
#endif

#ifndef Q_MOC_RUN
namespace
#else
class Q_CORE_EXPORT
#endif
Cy
{
#if defined(Q_MOC_RUN)
    Q_OBJECT
public:
#endif
  /** Types de valeur d'une donnée CY. */
  enum ValueType
  {
    /** Non défini. */
    Undef = 0,
    /** Entier signé de 8 bits. */
    VS8,
    /** Entier signé de 16 bits. */
    VS16,
    /** Entier signé de 32 bits. */
    VS32,
    /** Entier entier non-signé de 8 bits. */
    VU8,
    /** Entier non-signé de 16 bits. */
    VU16,
    /** Entier non-signé de 32 bits. */
    VU32,
    /** Flottant de 32 bits. */
    VF32,
    /** Flottant de 64 bits. */
    VF64,
    /** Chaîne de caractères. */
    String,
    /** Temps représenté par un u32. */
    Time,
    /** Temps représenté par un f32 en secondes. */
    Sec,
    /** Mot de 16 bits. */
    Word,
    /** Booléen. */
    Bool,
    /** Couleur. */
    Color,
  };

  /** Types de conteneur possibles. */
  enum TemplateType
  {
    /** Boîte de dialogue. */
    Dialog,
    /** Widget. */
    Widget,
    /** Frame. */
    Frame,
    /** Groupe de bouttons. */
    ButtonGroup,
  };

  /** Modes d'utilisation d'une donnée CY. */
  enum Mode
  {
    /** Non visible pour l'utilisateur final. */
    Sys = 0,
    /** Visible pour l'utilisateur final. */
    User,
    /** Visible uniquement pour le constructeur. */
    Designer,
    /** Pour les E/S non câblées. */
    Free,
    /** Pour les commandes et les groupes de commandes.
      * @see CYCommand, CYWCommand. */
    Bistable,
    /** Pour les commandes et les groupes de commandes.
      * @see CYCommand, CYWCommand. */
    Monostable,
    /** Les commandes peuvent avoir un évènement associé.
      * @see CYCommand. */
    Event,
  };

  /** Format d'affichage temporel. */
  enum TimeFormat
  {
    /** Temps sans format défini. */
    TIME_NO_FORMAT,
    /** Temps en heures (ex : 5 hour). */
    HOUR,
    /** Temps en minutes (ex : 6 min). */
    MIN,
    /** Temps en secondes (ex : 4 sec). */
    SEC,
    /** Temps en millisecondes (ex : 81 msec). */
    MSEC,
    /** Temps en heures, minutes (ex : 5h06m). */
    HOUR_MIN,
    /** Temps en heures, minutes, secondes (ex : 5h06m04s). */
    HOUR_MIN_SEC,
    /** Temps en minutes et secondes (ex : 6m04s). */
    MIN_SEC,
    /** Temps en secondes et millisecondes sans 0 devant (ex : 4s081). */
    SEC_MSEC,
    /** Temps en heures, minutes, secondes et millisecondes (ex : 5h06m04s081). */
    HOUR_MIN_SEC_MSEC,
    /** Temps en minutes secondes et millisecondes (ex : 6m04s081). */
    MIN_SEC_MSEC,
  };

  /** Base numérique en cas de donnée entière positive */
  enum NumBase
  {
    /** Base numérique définie par la donnée.  */
    BaseData = 0,
    /** A base 2.  */
    Binary = 2,
    /** A base 10.  */
    Decimal = 10,
    /** A base 16.  */
    Hexadecimal = 16,
  };

  /** Section de groupe de données. */
  enum GroupSection
  {
    /** Pas de demande relative au groupe. */
    No,
    /** Demande relative au dernier sous-groupe.. */
    lastGroup,
    /** Demande relative aux sous-groupes. */
    underGroup,
    /** Demande relative au groupe entier. */
    wholeGroup
  };

  /** Énumération des types d'incertitude possibles pour les mesures @see CYMeasure */
  enum Uncertain
  {
     /** Incertitude maxi de l'étendue de mesure en %. */
    UEM = 0,
    /** Incertitude en unité capteur. */
    UUC = 1,
    /** Incertitude en % par point. */
    UPC = 2,
  };

  /** Énumération des types de détection de changement d'état ou de valeur. */
  enum Change{ None=0, Rising=1, Falling=2, Changing=3 };

#ifndef Q_QDOC
  // NOTE: Generally, do not add QT_Q_ENUM if a corresponding Q_Q_FLAG exists.
  CY_Q_ENUM(ValueType)
  CY_Q_ENUM(TemplateType)
  CY_Q_ENUM(Mode)
  CY_Q_ENUM(TimeFormat)
  CY_Q_ENUM(NumBase)
  CY_Q_ENUM(GroupSection)
  CY_Q_ENUM(Uncertain)
  CY_Q_ENUM(Change)
#endif // Q_DOC
}
#ifdef Q_MOC_RUN
 ;
#endif

#undef CY_Q_ENUM
#undef CY_Q_FLAG

CY_END_NAMESPACE

/** @return le chemin complet du fichier.
@param file 	Chemin du fichier pouvant être complet ou relatif.
		Dans le premier cas celui-ci commence par un '/'.
		S'il ne commence ni par un '/' ni par '.' il est considéré
		comme un fichier système et est alors recherché comme tel.
*/
QString findResource(QString file, QString type="data");

/** Copie un répertoire. */
bool copyPath(QString sourceDir, QString destinationDir, bool overWriteDirectory);

/** Copie les fichiers d'un répertoire dans au autre. */
bool copyDirectoryFiles(const QString &fromDir, const QString &toDir, bool coverFileIfExist);

//======================================
// GESTION DE TUBES INTER-PROCESSUS
//======================================

int open_tube(dcltb_t *ptb);
int close_tube(dcltb_t *ptb);
int read_tube(dcltb_t *ptube, void *data, size_t size);
int write_tube(dcltb_t *ptube, void *data, size_t size);

#endif
