/* -*- mode: C++ ; c-file-style: "stroustrup" -*- *****************************
 * Cy Widget Library
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Qwt License, Version 1.0
 *****************************************************************************/

#ifndef CY_DESIGNER_PLOTDIALOG_H
#define CY_DESIGNER_PLOTDIALOG_H

#include <QDialog>

namespace CyDesignerPlugin
{

    class PlotDialog: public QDialog
    {
        Q_OBJECT

    public:
        PlotDialog( const QString &properties, QWidget *parent = NULL );

    Q_SIGNALS:
        void edited( const QString& );
    };

}

#endif
