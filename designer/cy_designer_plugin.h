#ifndef CY_DESIGNER_PLUGIN_H
#define CY_DESIGNER_PLUGIN_H

#include <QtUiPlugin/QDesignerCustomWidgetInterface>
#include <QDesignerTaskMenuExtension>
#include <QExtensionFactory>

namespace CYDesignerPlugin
{
    class CustomWidgetInterface: public QObject,
        public QDesignerCustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        CustomWidgetInterface( QObject *parent );

        virtual bool isContainer() const;
        virtual bool isInitialized() const;
        virtual QIcon icon() const;
        virtual QString codeTemplate() const;
        virtual QString domXml() const;
        virtual QString group() const;
        virtual QString includeFile() const;
        virtual QString name() const;
        virtual QString toolTip() const;
        virtual QString whatsThis() const;
        virtual void initialize( QDesignerFormEditorInterface * );

    protected:
        QString d_name;
        QString d_group;
        QString d_include;
        QString d_toolTip;
        QString d_whatsThis;
        QString d_domXml;
        QString d_codeTemplate;
        QIcon d_icon;
        bool d_isContainer;

    private:
        bool d_isInitialized;
    };

    class CustomWidgetCollectionInterface: public QObject,
        public QDesignerCustomWidgetCollectionInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetCollectionInterface )

#if QT_VERSION >= 0x050000
        Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QDesignerCustomWidgetCollectionInterface" )
#endif


    public:
        CustomWidgetCollectionInterface( QObject *parent = NULL );

        virtual QList<QDesignerCustomWidgetInterface*> customWidgets() const;

    private:
        QList<QDesignerCustomWidgetInterface*> d_plugins;
    };

    class CardDIInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        CardDIInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class CardDOInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        CardDOInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class CheckBoxInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        CheckBoxInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class ComboBoxInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        ComboBoxInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class PushButtonInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        PushButtonInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class NumDisplayInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        NumDisplayInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class NumInputInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        NumInputInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class LineEditInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        LineEditInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class TextInputInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        TextInputInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class TextEditInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        TextEditInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class DateWidgetInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        DateWidgetInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class FlagInputInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        FlagInputInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class TextLabelInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        TextLabelInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class LedInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        LedInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class ValveInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        ValveInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class Valve3Interface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        Valve3Interface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class PumpInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        PumpInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class VentilatorInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        VentilatorInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class HeaterInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        HeaterInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class CoolerInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        CoolerInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class WidgetInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        WidgetInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class FrameInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        FrameInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class ButtonGroupInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        ButtonGroupInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class ScrollViewInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        ScrollViewInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class LabelInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        LabelInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class PixmapViewInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        PixmapViewInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class MovieViewInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        MovieViewInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class LevelViewInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        LevelViewInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class MovingPixmapInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        MovingPixmapInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class DisplaySimpleInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        DisplaySimpleInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class MultimeterInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        MultimeterInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class BargraphInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        BargraphInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class PlotInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        PlotInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class ScopeInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        ScopeInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class ScopeMultipleInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        ScopeMultipleInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class DialInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        DialInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class RobotTopViewInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        RobotTopViewInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class SliderInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        SliderInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class WheelInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        WheelInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class ColorButtonInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        ColorButtonInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class PenInputInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        PenInputInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class TabWidgetInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        TabWidgetInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class CTInputInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        CTInputInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class MVInputInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        MVInputInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class PIDInputInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        PIDInputInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class AcquisitionSetupInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        AcquisitionSetupInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class DatasBrowserInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        DatasBrowserInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class DatasTableInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        DatasTableInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class AnalyseSheetInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        AnalyseSheetInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class ListViewInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        ListViewInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class DatasListViewInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        DatasListViewInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class DatasEditListInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        DatasEditListInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class ExtMeasureEditInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        ExtMeasureEditInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class TableInterface: public CustomWidgetInterface
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerCustomWidgetInterface )

    public:
        TableInterface( QObject *parent );
        virtual QWidget *createWidget( QWidget *parent );
    };

    class TaskMenuFactory: public QExtensionFactory
    {
        Q_OBJECT

    public:
        TaskMenuFactory( QExtensionManager *parent = 0 );

    protected:
        QObject *createExtension( QObject *object,
            const QString &iid, QObject *parent ) const;
    };

    class TaskMenuExtension: public QObject,
        public QDesignerTaskMenuExtension
    {
        Q_OBJECT
        Q_INTERFACES( QDesignerTaskMenuExtension )

    public:
        TaskMenuExtension( QWidget *widget, QObject *parent );

        QAction *preferredEditAction() const;
        QList<QAction *> taskActions() const;

    private Q_SLOTS:
        void editProperties();
        void applyProperties( const QString & );

    private:
        QAction *d_editAction;
        QWidget *d_widget;
    };

};

#endif
