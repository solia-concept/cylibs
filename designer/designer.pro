################################################################
# Cy Widget Library
# Copyright (C) 2015   Gérald Le Cléach
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the Qwt License, Version 1.0
################################################################

CY_ROOT = $${PWD}/..
CY_OUT_ROOT = $${OUT_PWD}/..

include ( $${CY_ROOT}/cyconfig.pri )
include ( $${CY_ROOT}/cyfunctions.pri )


CONFIG( debug_and_release ) {

    # When building debug_and_release the designer plugin is built
    # for release only. If you want to have a debug version it has to be
    # done with "CONFIG += debug" only.

    message("debug_and_release: building the Cy designer plugin in release mode only")

    CONFIG -= debug_and_release
    CONFIG += release
}

contains(CY_CONFIG, CyDesigner) {

    CONFIG    += qt plugin 

    greaterThan(QT_MAJOR_VERSION, 4) {

        QT += designer
    }
    else {

        CONFIG    += designer
    }


    TEMPLATE        = lib
    TARGET          = cy_designer_plugin

    DESTDIR         = plugins/designer

    DEPENDPATH += $${CY_ROOT}/src \
              $${CY_ROOT}/src/cyaccess \
              $${CY_ROOT}/src/cyacquisition \
              $${CY_ROOT}/src/cyanalyse \
              $${CY_ROOT}/src/cycali \
              $${CY_ROOT}/src/cycore \
              $${CY_ROOT}/src/cydata \
              $${CY_ROOT}/src/cydisplay \
              $${CY_ROOT}/src/cyevent \
              $${CY_ROOT}/src/cynet \
              $${CY_ROOT}/src/cyproject \
              $${CY_ROOT}/src/cyscript \
              $${CY_ROOT}/src/cyui \
              $${CY_ROOT}/src/cyuser \
              $${CY_ROOT}/src/cywidgets \
              $${CY_ROOT}/src/qwt \

    INCLUDEPATH += $${CY_ROOT}/src \
               $${CY_ROOT}/src/cyui \
               $${CY_ROOT}/src/cycore \
               $${CY_ROOT}/src/cydisplay \
               $${CY_ROOT}/src/qwt \
               $${CY_ROOT}/src/cydata \
               $${CY_ROOT}/src/cyacquisition \
               $${CY_ROOT}/src/cyevent \
               $${CY_ROOT}/src/cynet \
               $${CY_ROOT}/src/cyuser \
               $${CY_ROOT}/src/cyanalyse \
               $${CY_ROOT}/src/cycali \
               $${CY_ROOT}/src/cyaccess \
               $${CY_ROOT}/src/cyproject \
               $${CY_ROOT}/src/cywidgets

    contains(CY_CONFIG, CyDll) {

        contains(CY_CONFIG, CyDesignerSelfContained) {
            CY_CONFIG += include_src
        }

    } else {

        # for linking against a static library the 
        # plugin will be self contained anyway 
    }

    contains(CY_CONFIG, include_src) {

        # compile all cy classes into the plugin

        include ( $${CY_ROOT}/src/src.pri )

        for( header, HEADERS) {
            CY_HEADERS += $${CY_ROOT}/src/$${header}
        }

        for( source, SOURCES ) {
            CY_SOURCES += $${CY_ROOT}/src/$${source}
        }

        HEADERS = $${CY_HEADERS}
        SOURCES = $${CY_SOURCES}

    } else {

        # compile the path for finding the Cy library
        # into the plugin. Not supported on Windows !

        QMAKE_RPATHDIR *= $${CY_INSTALL_LIBS}
        cyAddLibrary($${CY_OUT_ROOT}/lib, cy)

        contains(CY_CONFIG, CyDll) {

            windows {
                DEFINES += QT_DLL CY_DLL
            }
        }
    }

    !contains(CY_CONFIG, CyPlot) {
        DEFINES += NO_CY_PLOT
    }

    !contains(CY_CONFIG, CyWidgets) {
        DEFINES += NO_CY_WIDGETS
    }

    HEADERS += cy_designer_plugin.h
    SOURCES += cy_designer_plugin.cpp

    contains(CY_CONFIG, CyPlot) {

        HEADERS += cy_designer_plotdialog.h
        SOURCES += cy_designer_plotdialog.cpp
    }

    RESOURCES += cy_designer_plugin.qrc

    target.path = $${CY_INSTALL_PLUGINS}
    INSTALLS += target
}
else {
    TEMPLATE        = subdirs # do nothing
}

