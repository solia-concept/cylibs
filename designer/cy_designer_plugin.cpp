#include <qglobal.h>
#include <qaction.h>
#include <QtPlugin>
#include <QDesignerFormEditorInterface>
#include <QDesignerFormWindowInterface>
#include <QDesignerFormWindowCursorInterface>
#include <QExtensionManager>
#include <QErrorMessage>

#include "cy_designer_plugin.h"
#include "cy_designer_plotdialog.h"

#include "cycarddi.h"
#include "cycarddo.h"
#include "cycheckbox.h"
#include "cycombobox.h"
#include "cypushbutton.h"
#include "cynumdisplay.h"
#include "cynuminput.h"
#include "cylineedit.h"
#include "cytextinput.h"
#include "cytextedit.h"
#include "cydatewidget.h"
#include "cyflaginput.h"
#include "cytextlabel.h"
#include "cyled.h"
#include "cyvalve.h"
#include "cyvalve3.h"
#include "cypump.h"
#include "cyventilator.h"
#include "cyheater.h"
#include "cycooler.h"
#include "cyframe.h"
#include "cybuttongroup.h"
#include "cyscrollview.h"
#include "cylabel.h"
#include "cypixmapview.h"
#include "cymovieview.h"
#include "cylevelview.h"
#include "cydisplaysimple.h"
#include "cymultimeter.h"
#include "cybargraph.h"
#include "cyscope.h"
#include "cyscopemultiple.h"
#include "cyplot.h"
#include "cydial.h"
#include "cyrobottopview.h"
#include "cyslider.h"
#include "cywheel.h"
#include "cycolorbutton.h"
#include "cypeninput.h"
#include "cytabwidget.h"
#include "cyctinput.h"
#include "cymvinput.h"
#include "cypidinput.h"
#include "cyacquisitionsetup.h"
#include "cydatasbrowser.h"
#include "cydatastable.h"
#include "cyanalysesheet.h"
#include "cydataseditlist.h"
#include "cytreewidget.h"
#include "cydataslistview.h"
#include "cyextmeasureedit.h"
#include "cymovingpixmap.h"
#include "cytable.h"
#include "cyplot.h"

using namespace CYDesignerPlugin;

CustomWidgetInterface::CustomWidgetInterface( QObject *parent ):
    QObject( parent ),
    d_isContainer( false ),
    d_isInitialized( false )
{
}

bool CustomWidgetInterface::isContainer() const
{
    return d_isContainer;
}

bool CustomWidgetInterface::isInitialized() const
{
    return d_isInitialized;
}

QIcon CustomWidgetInterface::icon() const
{
    return d_icon;
}

QString CustomWidgetInterface::codeTemplate() const
{
    return d_codeTemplate;
}

QString CustomWidgetInterface::domXml() const
{
    return d_domXml;
}

QString CustomWidgetInterface::group() const
{
    return d_group;
}

QString CustomWidgetInterface::includeFile() const
{
    return d_include;
}

QString CustomWidgetInterface::name() const
{
    return d_name;
}

QString CustomWidgetInterface::toolTip() const
{
    return d_toolTip;
}

QString CustomWidgetInterface::whatsThis() const
{
    return d_whatsThis;
}

void CustomWidgetInterface::initialize(
    QDesignerFormEditorInterface *formEditor )
{
    if ( d_isInitialized )
        return;

    QExtensionManager *manager = formEditor->extensionManager();
    if ( manager )
    {
        manager->registerExtensions( new TaskMenuFactory( manager ),
            Q_TYPEID( QDesignerTaskMenuExtension ) );
    }

    d_isInitialized = true;
}



CardDIInterface::CardDIInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYCardDI";
    d_include = "cycardDI.h";
    d_icon = QPixmap( ":/pixmaps/cycarddi.png" );
    d_group = "CY: "+tr("Maintenance");
    d_domXml =
        "<widget class=\"CYCardDI\" name=\"cardDI\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>150</width>\n"
        "   <height>150</height>\n"
        "  </rect>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *CardDIInterface::createWidget( QWidget *parent )
{
    return new CYCardDI( parent );
}

CardDOInterface::CardDOInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYCardDO";
    d_include = "cycarddo.h";
    d_icon = QPixmap( ":/pixmaps/cycarddo.png" );
    d_group = "CY: "+tr("Maintenance");
    d_domXml =
        "<widget class=\"CYCardDO\" name=\"cardDO\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>150</width>\n"
        "   <height>150</height>\n"
        "  </rect>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *CardDOInterface::createWidget( QWidget *parent )
{
    return new CYCardDO( parent );
}

CheckBoxInterface::CheckBoxInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYCheckBox";
    d_include = "cycheckbox.h";
    d_icon = QPixmap( ":/pixmaps/cycheckbox.png" );
    d_group = "CY: "+tr("Buttons");
    d_domXml =
        "<widget class=\"CYCheckBox\" name=\"checkBox\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *CheckBoxInterface::createWidget( QWidget *parent )
{
    return new CYCheckBox( parent );
}

ComboBoxInterface::ComboBoxInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYComboBox";
    d_include = "cycombobox.h";
    d_icon = QPixmap( ":/pixmaps/cycombobox.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYComboBox\" name=\"comboBox\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *ComboBoxInterface::createWidget( QWidget *parent )
{
    return new CYComboBox( parent );
}

PushButtonInterface::PushButtonInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYPushButton";
    d_include = "cypushbutton.h";
    d_icon = QPixmap( ":/pixmaps/cypushbutton.png" );
    d_group = "CY: "+tr("Buttons");
    d_isContainer = true;
    d_domXml =
        "<widget class=\"CYPushButton\" name=\"pushButton\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *PushButtonInterface::createWidget( QWidget *parent )
{
    return new CYPushButton( parent );
}

NumDisplayInterface::NumDisplayInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYNumDisplay";
    d_include = "cynumdisplay.h";
    d_icon = QPixmap( ":/pixmaps/cynumdisplay.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYNumDisplay\" name=\"numDisplay\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *NumDisplayInterface::createWidget( QWidget *parent )
{
    return new CYNumDisplay( parent );
}

NumInputInterface::NumInputInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYNumInput";
    d_include = "cynuminput.h";
    d_icon = QPixmap( ":/pixmaps/cynuminput.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYNumInput\" name=\"numInput\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *NumInputInterface::createWidget( QWidget *parent )
{
    return new CYNumInput( parent );
}

LineEditInterface::LineEditInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYLineEdit";
    d_include = "cylineedit.h";
    d_icon = QPixmap( ":/pixmaps/cylineedit.png" );
    d_group = "CY: "+tr("Line edit Widgets");
    d_domXml =
        "<widget class=\"CYLineEdit\" name=\"cylineEdit\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *LineEditInterface::createWidget( QWidget *parent )
{
    return new CYLineEdit( parent );
}

TextInputInterface::TextInputInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYTextInput";
    d_include = "cytextinput.h";
    d_icon = QPixmap( ":/pixmaps/cytextinput.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYTextInput\" name=\"textInput\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *TextInputInterface::createWidget( QWidget *parent )
{
    return new CYTextInput( parent );
}

TextEditInterface::TextEditInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYTextEdit";
    d_include = "cytextedit.h";
    d_icon = QPixmap( ":/pixmaps/cytextedit.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYTextEdit\" name=\"textEdit\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>150</width>\n"
        "   <height>150</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *TextEditInterface::createWidget( QWidget *parent )
{
    return new CYTextEdit( parent );
}

DateWidgetInterface::DateWidgetInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYDateWidget";
    d_include = "cydatewidget.h";
    d_icon = QPixmap( ":/pixmaps/cydatewidget.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYDateWidget\" name=\"dateWidget\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *DateWidgetInterface::createWidget( QWidget *parent )
{
    return new CYDateWidget( parent );
}

FlagInputInterface::FlagInputInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYFlagInput";
    d_include = "cyflaginput.h";
    d_icon = QPixmap( ":/pixmaps/cyflaginput.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYFlagInput\" name=\"flagInput\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *FlagInputInterface::createWidget( QWidget *parent )
{
    return new CYFlagInput( parent );
}

TextLabelInterface::TextLabelInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYTextLabel";
    d_include = "cytextlabel.h";
    d_icon = QPixmap( ":/pixmaps/cytextlabel.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYTextLabel\" name=\"textLabel\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *TextLabelInterface::createWidget( QWidget *parent )
{
    return new CYTextLabel( parent );
}

LedInterface::LedInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYLed";
    d_include = "cyled.h";
    d_icon = QPixmap( ":/pixmaps/cyled.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYLed\" name=\"led\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *LedInterface::createWidget( QWidget *parent )
{
    return new CYLed( parent );
}

ValveInterface::ValveInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYValve";
    d_include = "cyvalve.h";
    d_icon = QPixmap( ":/pixmaps/cyvalve_nf_true.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYValve\" name=\"valve\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *ValveInterface::createWidget( QWidget *parent )
{
    return new CYValve( parent );
}

Valve3Interface::Valve3Interface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYValve3";
    d_include = "cyvalve3.h";
    d_icon = QPixmap( ":/pixmaps/cyvalve3_ews_true.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYValve3\" name=\"valve3\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *Valve3Interface::createWidget( QWidget *parent )
{
    return new CYValve3( parent );
}

PumpInterface::PumpInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYPump";
    d_include = "cypump.h";
    d_icon = QPixmap( ":/pixmaps/cypump_on_lr.gif" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYPump\" name=\"Pump\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *PumpInterface::createWidget( QWidget *parent )
{
    return new CYPump( parent );
}

VentilatorInterface::VentilatorInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYVentilator";
    d_include = "cyventilator.h";
    d_icon = QPixmap( ":/pixmaps/cyventilator_on.gif" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYVentilator\" name=\"ventilator\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *VentilatorInterface::createWidget( QWidget *parent )
{
    return new CYVentilator( parent );
}

HeaterInterface::HeaterInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYHeater";
    d_include = "cyheater.h";
    d_icon = QPixmap( ":/pixmaps/cyheater_on.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYHeater\" name=\"heater\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *HeaterInterface::createWidget( QWidget *parent )
{
    return new CYHeater( parent );
}

CoolerInterface::CoolerInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYCooler";
    d_include = "cycooler.h";
    d_icon = QPixmap( ":/pixmaps/cycooler_on.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYCooler\" name=\"cooler\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *CoolerInterface::createWidget( QWidget *parent )
{
    return new CYCooler( parent );
}

WidgetInterface::WidgetInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYWidget";
    d_include = "cywidget.h";
    d_icon = QPixmap( ":/pixmaps/cywidget.png" );
    d_group = "CY: "+tr("Containers");
    d_isContainer = true;
    d_domXml =
        "<widget class=\"CYWidget\" name=\"widget\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *WidgetInterface::createWidget( QWidget *parent )
{
    return new CYWidget( parent );
}

FrameInterface::FrameInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYFrame";
    d_include = "cyframe.h";
    d_icon = QPixmap( ":/pixmaps/cyframe.png" );
    d_group = "CY: "+tr("Containers");
    d_isContainer = true;
    d_domXml =
        "<widget class=\"CYFrame\" name=\"frame\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *FrameInterface::createWidget( QWidget *parent )
{
    return new CYFrame( parent );
}

ButtonGroupInterface::ButtonGroupInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYButtonGroup";
    d_include = "cybuttongroup.h";
    d_icon = QPixmap( ":/pixmaps/cybuttongroup.png" );
    d_group = "CY: "+tr("Containers");
    d_isContainer = true;
    d_domXml =
        "<widget class=\"CYButtonGroup\" name=\"buttonGroup\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *ButtonGroupInterface::createWidget( QWidget *parent )
{
    return new CYButtonGroup( parent );
}

ScrollViewInterface::ScrollViewInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYScrollView";
    d_include = "cyscrollview.h";
    d_icon = QPixmap( ":/pixmaps/cyscrollview.png" );
    d_group = "CY: "+tr("Containers");
    d_isContainer = true;
    d_domXml =
        "<widget class=\"CYScrollView\" name=\"scrollView\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *ScrollViewInterface::createWidget( QWidget *parent )
{
    return new CYScrollView( parent );
}

LabelInterface::LabelInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYLabel";
    d_include = "cylabel.h";
    d_icon = QPixmap( ":/pixmaps/cylabel.png" );
    d_group = "CY: "+tr("Containers");
    d_isContainer = true;
    d_domXml =
        "<widget class=\"CYLabel\" name=\"label\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *LabelInterface::createWidget( QWidget *parent )
{
    return new CYLabel( parent );
}

PixmapViewInterface::PixmapViewInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYPixmapView";
    d_include = "cypixmapview.h";
    d_icon = QPixmap( ":/pixmaps/cypixmapview.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYPixmapView\" name=\"pixmapView\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *PixmapViewInterface::createWidget( QWidget *parent )
{
    return new CYPixmapView( parent );
}

MovieViewInterface::MovieViewInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYMovieView";
    d_include = "cymovieview.h";
    d_icon = QPixmap( ":/pixmaps/cyMovieView.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYMovieView\" name=\"movieView\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *MovieViewInterface::createWidget( QWidget *parent )
{
    return new CYMovieView( parent );
}

LevelViewInterface::LevelViewInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYLevelView";
    d_include = "cylevelview.h";
    d_icon = QPixmap( ":/pixmaps/cylevelview.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYLevelView\" name=\"levelView\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *LevelViewInterface::createWidget( QWidget *parent )
{
    return new CYLevelView( parent );
}

MovingPixmapInterface::MovingPixmapInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYMovingPixmap";
    d_include = "cymovingpixmap.h";
    d_icon = QPixmap( ":/pixmaps/cymovingpixmap.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYMovingPixmap\" name=\"movingPixmap\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *MovingPixmapInterface::createWidget( QWidget *parent )
{
    return new CYMovingPixmap( parent );
}

DisplaySimpleInterface::DisplaySimpleInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYDisplaySimple";
    d_include = "cydisplaysimple.h";
    d_icon = QPixmap( ":/pixmaps/cydisplaysimple.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYDisplaySimple\" name=\"displaySimple\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *DisplaySimpleInterface::createWidget( QWidget *parent )
{
    return new CYDisplaySimple( parent );
}

MultimeterInterface::MultimeterInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYMultimeter";
    d_include = "cymultimeter.h";
    d_icon = QPixmap( ":/pixmaps/cymultimeter.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYMultimeter\" name=\"multimeter\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *MultimeterInterface::createWidget( QWidget *parent )
{
    return new CYMultimeter( parent );
}

BargraphInterface::BargraphInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYBargraph";
    d_include = "cybargraph.h";
    d_icon = QPixmap( ":/pixmaps/cybargraph.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYBargraph\" name=\"bargraph\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *BargraphInterface::createWidget( QWidget *parent )
{
    return new CYBargraph( parent );
}

PlotInterface::PlotInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYPlot";
    d_include = "cyplot.h";
    d_icon = QPixmap( ":/pixmaps/cyplot.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYPlot\" name=\"Plot\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>150</width>\n"
        "   <height>150</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *PlotInterface::createWidget( QWidget *parent )
{
    return new CYPlot( parent );
}

ScopeInterface::ScopeInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYScope";
    d_include = "cyscope.h";
    d_icon = QPixmap( ":/pixmaps/cyscope.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYScope\" name=\"scope\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>150</width>\n"
        "   <height>150</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *ScopeInterface::createWidget( QWidget *parent )
{
    return new CYScope( parent );
}

ScopeMultipleInterface::ScopeMultipleInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYScopeMultiple";
    d_include = "cyscopemultiple.h";
    d_icon = QPixmap( ":/pixmaps/cyscopemultiple.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYScopeMultiple\" name=\"scopeMultiple\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *ScopeMultipleInterface::createWidget( QWidget *parent )
{
    return new CYScopeMultiple( parent );
}

DialInterface::DialInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYDial";
    d_include = "cydial.h";
    d_icon = QPixmap( ":/pixmaps/cydial.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYDial\" name=\"dial\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *DialInterface::createWidget( QWidget *parent )
{
    return new CYDial( parent );
}

RobotTopViewInterface::RobotTopViewInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYRobotTopView";
    d_include = "cyrobottopview.h";
    d_icon = QPixmap( ":/pixmaps/cyrobottopview.png" );
    d_group = "CY: "+tr("Display Widgets");
    d_domXml =
        "<widget class=\"CYRobotTopView\" name=\"robotTopView\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *RobotTopViewInterface::createWidget( QWidget *parent )
{
    return new CYRobotTopView( parent );
}

SliderInterface::SliderInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYSlider";
    d_include = "cyslider.h";
    d_icon = QPixmap( ":/pixmaps/cyslider.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYSlider\" name=\"slider\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *SliderInterface::createWidget( QWidget *parent )
{
    return new CYSlider( parent );
}

WheelInterface::WheelInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYWheel";
    d_include = "cywheel.h";
    d_icon = QPixmap( ":/pixmaps/cywheel.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYWheel\" name=\"wheel\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *WheelInterface::createWidget( QWidget *parent )
{
    return new CYWheel( parent );
}

ColorButtonInterface::ColorButtonInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYColorButton";
    d_include = "cycolorbutton.h";
    d_icon = QPixmap( ":/pixmaps/cycolorbutton.png" );
    d_group = "CY: "+tr("Buttons");
    d_domXml =
        "<widget class=\"CYColorButton\" name=\"colorButton\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *ColorButtonInterface::createWidget( QWidget *parent )
{
    return new CYColorButton( parent );
}

PenInputInterface::PenInputInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYPenInput";
    d_include = "cypeninput.h";
    d_icon = QPixmap( ":/pixmaps/cypeninput.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYPenInput\" name=\"penInput\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *PenInputInterface::createWidget( QWidget *parent )
{
    return new CYPenInput( parent );
}

TabWidgetInterface::TabWidgetInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYTabWidget";
    d_include = "cytabwidget.h";
    d_icon = QPixmap( ":/pixmaps/cytabwidget.png" );
    d_group = "CY: "+tr("Containers");
    d_isContainer = true;
    d_domXml =
        "<widget class=\"CYTabWidget\" name=\"tabWidget\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *TabWidgetInterface::createWidget( QWidget *parent )
{
    return new CYTabWidget( parent );
}

CTInputInterface::CTInputInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYCTInput";
    d_include = "cyctinput.h";
    d_icon = QPixmap( ":/pixmaps/cyctinput.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYCTInput\" name=\"CTInput\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *CTInputInterface::createWidget( QWidget *parent )
{
    return new CYCTInput( parent );
}

MVInputInterface::MVInputInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYMVInput";
    d_include = "cymvinput.h";
    d_icon = QPixmap( ":/pixmaps/cymvinput.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYMVInput\" name=\"MVInput\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *MVInputInterface::createWidget( QWidget *parent )
{
    return new CYMVInput( parent );
}

PIDInputInterface::PIDInputInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYPIDInput";
    d_include = "cypidinput.h";
    d_icon = QPixmap( ":/pixmaps/cypidinput.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYPIDInput\" name=\"PIDInput\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *PIDInputInterface::createWidget( QWidget *parent )
{
    return new CYPIDInput( parent );
}

AcquisitionSetupInterface::AcquisitionSetupInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYAcquisitionSetup";
    d_include = "cyacquisitionsetup.h";
    d_icon = QPixmap( ":/pixmaps/cyacquisitionsetup.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYAcquisitionSetup\" name=\"acquisitionSetup\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *AcquisitionSetupInterface::createWidget( QWidget *parent )
{
    return new CYAcquisitionSetup( parent );
}

DatasBrowserInterface::DatasBrowserInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYDatasBrowser";
    d_include = "cydatasbrowser.h";
    d_icon = QPixmap( ":/pixmaps/cydatasbrowser.png" );
    d_group = "CY: "+tr("Item Views");
    d_domXml =
        "<widget class=\"CYDatasBrowser\" name=\"datasBrowser\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *DatasBrowserInterface::createWidget( QWidget *parent )
{
    return new CYDatasBrowser( parent );
}

DatasTableInterface::DatasTableInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYDatasTable";
    d_include = "cydatastable.h";
    d_icon = QPixmap( ":/pixmaps/cydatastable.png" );
    d_group = "CY: "+tr("Item Views");
    d_domXml =
        "<widget class=\"CYDatasTable\" name=\"datasTable\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *DatasTableInterface::createWidget( QWidget *parent )
{
    return new CYDatasTable( parent );
}

AnalyseSheetInterface::AnalyseSheetInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYAnalyseSheet";
    d_include = "cyanalysesheet.h";
    d_icon = QPixmap( ":/pixmaps/cyanalysesheet.png" );
    d_group = "CY: "+tr("Item Views");
    d_domXml =
        "<widget class=\"CYAnalyseSheet\" name=\"analyseSheet\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *AnalyseSheetInterface::createWidget( QWidget *parent )
{
    return new CYAnalyseSheet( parent );
}

ListViewInterface::ListViewInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYTreeWidget";
    d_include = "cytreewidget.h";
    d_icon = QPixmap( ":/pixmaps/cytreewidget.png" );
    d_group = "CY: "+tr("Item Views");
    d_domXml =
        "<widget class=\"CYTreeWidget\" name=\"treeWidget\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *ListViewInterface::createWidget( QWidget *parent )
{
    return new CYTreeWidget( parent );
}

DatasListViewInterface::DatasListViewInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYDatasListView";
    d_include = "cydataslistview.h";
    d_icon = QPixmap( ":/pixmaps/cydataslistview.png" );
    d_group = "CY: "+tr("Item Views");
    d_domXml =
        "<widget class=\"CYDatasListView\" name=\"datasListView\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *DatasListViewInterface::createWidget( QWidget *parent )
{
    return new CYDatasListView( parent );
}

DatasEditListInterface::DatasEditListInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYDatasEditList";
    d_include = "cydataseditlist.h";
    d_icon = QPixmap( ":/pixmaps/cydataseditlist.png" );
    d_group = "CY: "+tr("Item Views");
    d_domXml =
        "<widget class=\"CYDatasEditList\" name=\"datasEditList\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *DatasEditListInterface::createWidget( QWidget *parent )
{
    return new CYDatasEditList( parent );
}

ExtMeasureEditInterface::ExtMeasureEditInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYExtMeasureEdit";
    d_include = "cyextmeasureedit.h";
    d_icon = QPixmap( ":/pixmaps/cyextmeasureedit.png" );
    d_group = "CY: "+tr("Input Widgets");
    d_domXml =
        "<widget class=\"CYExtMeasureEdit\" name=\"extMeasureEdit\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *ExtMeasureEditInterface::createWidget( QWidget *parent )
{
    return new CYExtMeasureEdit( parent );
}

TableInterface::TableInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYTable";
    d_include = "cytable.h";
    d_icon = QPixmap( ":/pixmaps/cytable.png" );
    d_group = "CY: "+tr("Item Views");
    d_domXml =
        "<widget class=\"CYTable\" name=\"table\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *TableInterface::createWidget( QWidget *parent )
{
    return new CYTable( parent );
}

CustomWidgetCollectionInterface::CustomWidgetCollectionInterface(
        QObject *parent ):
    QObject( parent )
{
    d_plugins.append(new CardDIInterface(this));
    d_plugins.append(new CardDOInterface(this));
    d_plugins.append(new CheckBoxInterface(this));
    d_plugins.append(new ComboBoxInterface(this));
    d_plugins.append(new PushButtonInterface(this));
    d_plugins.append(new NumDisplayInterface(this));
    d_plugins.append(new NumInputInterface(this));
    d_plugins.append(new LineEditInterface(this));
    d_plugins.append(new TextInputInterface(this));
    d_plugins.append(new TextEditInterface(this));
    d_plugins.append(new DateWidgetInterface(this));
    d_plugins.append(new FlagInputInterface(this));
    d_plugins.append(new TextLabelInterface(this));
    d_plugins.append(new LedInterface(this));
    d_plugins.append(new ValveInterface(this));
    d_plugins.append(new Valve3Interface(this));
    d_plugins.append(new PumpInterface(this));
    d_plugins.append(new VentilatorInterface(this));
    d_plugins.append(new HeaterInterface(this));
    d_plugins.append(new CoolerInterface(this));
    d_plugins.append(new WidgetInterface(this));
    d_plugins.append(new FrameInterface(this));
    d_plugins.append(new ButtonGroupInterface(this));
    d_plugins.append(new ScrollViewInterface(this));
    d_plugins.append(new LabelInterface(this));
    d_plugins.append(new PixmapViewInterface(this));
    d_plugins.append(new MovieViewInterface(this));
    d_plugins.append(new LevelViewInterface(this));
    d_plugins.append(new MovingPixmapInterface(this));
    d_plugins.append(new MultimeterInterface(this));
    d_plugins.append(new BargraphInterface(this));
    d_plugins.append(new PlotInterface(this));
    d_plugins.append(new ScopeInterface(this));
    d_plugins.append(new ScopeMultipleInterface(this));
    d_plugins.append(new DialInterface(this));
    d_plugins.append(new RobotTopViewInterface(this));
    d_plugins.append(new SliderInterface(this));
    d_plugins.append(new WheelInterface(this));
    d_plugins.append(new ColorButtonInterface(this));
    d_plugins.append(new PenInputInterface(this));
    d_plugins.append(new TabWidgetInterface(this));
    d_plugins.append(new CTInputInterface(this));
    d_plugins.append(new MVInputInterface(this));
    d_plugins.append(new PIDInputInterface(this));
    d_plugins.append(new AcquisitionSetupInterface(this));
    d_plugins.append(new DatasBrowserInterface(this));
    d_plugins.append(new DatasTableInterface(this));
    d_plugins.append(new AnalyseSheetInterface(this));
    d_plugins.append(new ListViewInterface(this));
    d_plugins.append(new DatasListViewInterface(this));
    d_plugins.append(new DatasEditListInterface(this));
    d_plugins.append(new ExtMeasureEditInterface(this));
    d_plugins.append(new TableInterface(this));
}

QList<QDesignerCustomWidgetInterface*>
CustomWidgetCollectionInterface::customWidgets( void ) const
{
    return d_plugins;
}

TaskMenuFactory::TaskMenuFactory( QExtensionManager *parent ):
    QExtensionFactory( parent )
{
}

QObject *TaskMenuFactory::createExtension(
    QObject *object, const QString &iid, QObject *parent ) const
{
    if ( iid == Q_TYPEID( QDesignerTaskMenuExtension ) )
    {
//#ifndef NO_QWT_PLOT
//        if ( CyPlot *plot = qobject_cast<CyPlot*>( object ) )
//            return new TaskMenuExtension( plot, parent );
//#endif
//#ifndef NO_QWT_WIDGETS
//        if ( CyDial *dial = qobject_cast<CyDial*>( object ) )
//            return new TaskMenuExtension( dial, parent );
//#endif
    }

    return QExtensionFactory::createExtension( object, iid, parent );
}


TaskMenuExtension::TaskMenuExtension( QWidget *widget, QObject *parent ):
    QObject( parent ),
    d_widget( widget )
{
    d_editAction = new QAction( tr( "Edit Cy Attributes ..." ), this );
    connect( d_editAction, SIGNAL( triggered() ),
        this, SLOT( editProperties() ) );
}

QList<QAction *> TaskMenuExtension::taskActions() const
{
    QList<QAction *> list;
    list.append( d_editAction );
    return list;
}

QAction *TaskMenuExtension::preferredEditAction() const
{
    return d_editAction;
}

void TaskMenuExtension::editProperties()
{
    const QVariant v = d_widget->property( "propertiesDocument" );
    if ( v.type() != QVariant::String )
        return;

////#ifndef NO_CY_PLOT
//    QString properties = v.toString();

//    if ( qobject_cast<CYPlot*>( d_widget ) )
//    {
//        PlotDialog dialog( properties );
//        connect( &dialog, SIGNAL( edited( const QString& ) ),
//            SLOT( applyProperties( const QString & ) ) );
//        ( void )dialog.exec();
//        return;
//    }
////#endif

    static QErrorMessage *errorMessage = NULL;
    if ( errorMessage == NULL )
        errorMessage = new QErrorMessage();
    errorMessage->showMessage( "Not implemented yet." );
}

void TaskMenuExtension::applyProperties( const QString &properties )
{
    QDesignerFormWindowInterface *formWindow =
        QDesignerFormWindowInterface::findFormWindow( d_widget );
    if ( formWindow && formWindow->cursor() )
        formWindow->cursor()->setProperty( "propertiesDocument", properties );
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2( CYDesignerPlugin, CustomWidgetCollectionInterface )
#endif

