#!/bin/bash
# cykde3qt5: Script de migration d'une application cylibs de kde3 en Qt5.

# Copyright (C) 2015 Gérald LE CLÉACH

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

VERSION="1.0"
version="$VERSION
Written by Gérald LE CLEACH"

usage="
Script de correction de migration d'une application cylibs de kde3 en Qt5.

Usage: $0 [OPTION] DIR [ARG]
Options:
 -h         Affiche cette aide.
 -v         Affiche la version.
 -all       Nettoie, copie les fichiers et répertoires du répertoire de projet
            kde3 DIR et les met à jour.
 -clean     Nettoie le projet pour en faire un nouveau modèle vierge.
 -update    Mise à jour des fichiers du répertoire DIR ou tout si
            aucun n'est précisé.
 -uiqt3qt4  '-update' avec regénération des fichiers UI en version 4
 -tp        Test les propriétés au lieu de remplacer les propriétés Qt3
            par d'autres définies dans cylibs.

Report bugs to <gerald.lecleach@solia-concept.fr>."

if test $# = 0; then
  echo "$usage"
  exit 1
fi

all=0
res=0
clean=0
update=0
uiqt3qt4=0
tp=0
updatedir=0
updatefile=0
dir=0
NEWLINE=0
PRECLINE=0

#cmd="tr [A-Z] [a-z]"
for i do
  case "$i" in
  -h*) exec echo "$usage";;
  -v*) exec echo "$version";;
  -all*) all=1;;
  -clean*) clean=1;;
  -update*) update=1;;
  -tp*) tp=1;;
  /*)  continue;;
  *)  continue;;
  esac
done

if [ $uiqt3qt4 = 1 ]; then
  update=1
fi

if [ $update = 1 ]; then
  ok=0

  if [ "$2" = "" ]; then
    echo "répertoire total"
    ok=1
  else
    if test -d $2; then
      updatedir=$2
      echo $2 "répertoire existant"
      ok=1
    else
      if test -e $2; then
        echo $2 "fichier existant"
        updatefile=$2
        ok=1
      fi
    fi
  fi
  if [ $ok = 0 ]; then
    "Fichier ou répertoire à mettre à jour innexistant" $2
    exit 0
  fi
fi


backup()
{
  dstpath=$(basename "$PWD")
  echo $dstpath
  NOW=$(date +"%Y-%m-%d_%H-%M-%S")
  backup=$dstpath"_$NOW.tar.gz"
  echo "BACKUP PROJET: "$backup
  cd ..
  tar -cvf $backup $dstpath
  cd $dstpath
}

clean_project()
{
  echo "SUPPRESSION DES ANCIENS FICHIERS"
  rm -f ./$dstpath.pro
  rm -f ./ChangeLog
  rm -f *~
  rm -fr ./src
  if [ $clean = 1 ]; then
    exit 1
  fi
}


add_include()
{
  if (grep -q $CLASS $file2update); then
    if ! (grep -q "$NEWLINE" $file2update); then
        sed -i -e "/$PRECLINE/ a$NEWLINE" $file2update;
    fi
  fi
}

#remplace une ligne
replace()
{
  sed -i -e "s/$OLD/$NEW/g" $file2update
}

#commente une ligne
comment_line()
{
  COMMENT="\/\/CYQT5"
  sed -i -e "/\/\//! s/.*$MOTIF/$COMMENT &/" $file2update
}

#commente une fonction
comment_bloc()
{
  COMMENT="\/\/CYQT5"
  sed -i -e "/$BEGIN/,/$END/{ /$COMMENT/b; s/.*/$COMMENT &/; }" $file2update
}

#commente une fonction
comment_function()
{
  BEGIN="^$FUNCTION";
  END="^\}";
  COMMENT="\/\/CYQT5"
  sed -i -e "/$BEGIN/,/$END/{ /$COMMENT/b; s/.*/$COMMENT &/; }" $file2update
}


#commente le contenu d'une fonction
comment_in_function()
{
  BEGIN=$FUNCTION;
  END="^\}";
  OPEN="^{";
  COMMENT="\/\/CYQT5"
  sed -i -e "/$BEGIN/,/$END/{ /$BEGIN/b;/$END/b;/$OPEN/b;/$COMMENT/b; s/.*/$COMMENT &/; }" $file2update
}

#récupère le code d'une fonction
code_function()
{
  BEGIN=$FUNCTION;
  END="^\}";
  OPEN="^{";
  MOTIF="";
 # COMMAND=$MOTIF$(sed))
 # sed -i -e "/$BEGIN/,/$END/{ /$BEGIN/b;/$OPEN/b;/$END/b;$COMMAND; }" $file2update
}

#ajoute à la fin d'une fonction
add_end_function()
{
  MOTIF="$MOTIF\n\}"
  BEGIN=$FUNCTION;
  END="^\}";
  OPEN="^{";
  sed -i -e "/$BEGIN/,/$END/{ /$BEGIN/b;s/$END/$MOTIF/; }" $file2update
}

#ajoute à la fin d'une fonction
add_begin_function()
{
  MOTIF="{\n$MOTIF"
  BEGIN=$FUNCTION;
  END="^\}";
  OPEN="^{";
  sed -i -e "/$BEGIN/,/$END/{ /$BEGIN/b;s/$OPEN/$MOTIF/; }" $file2update
}

#ajoute avant $TARGET dans une fonction
add_in_function()
{
  MOTIF="$MOTIF\n$TARGET"
  BEGIN=$FUNCTION;
  echo -e "$LINENO $BEGIN\n$MOTIF"
  END="^\}";
  OPEN="^{";
  sed -i -e "/$BEGIN/,/$END/{ /$BEGIN/b;s/$TARGET/$MOTIF/; }" $file2update
}

#ajoute avant $TARGET dans une fonction
delete_in_function()
{
  BEGIN=$FUNCTION;
  END="^\}";
  OPEN="^{";
  sed -i -e "/$BEGIN/,/$END/{ /$BEGIN/b;/$MOTIF/d; }" $file2update
}

#ajoute $ADD après $MOTIF
add_after()
{
  if ! (grep -q "$MOTIF$ADD" $file2update); then
      sed -i -e "s/$MOTIF/$MOTIF$ADD/g" $file2update
  fi
}

#ajoute $ADD avant $MOTIF
add_before()
{
  if ! (grep -q "$ADD$MOTIF" $file2update); then
      sed -i -e "s/$ADD$MOTIF/$MOTIF/g" $file2update
  fi
}

#ajoute dans les sources de l'enfant d'un .ui l'appel à ce parent
prepend_ui_parent()
{
  if ! (grep -q "ui->$WIDGET" $file2update); then
      sed -i -e "s/$WIDGET/ui->$WIDGET/g" $file2update
  fi
}

move_line_before()
{
  sed -i -e "/$LINE/d" $file2update;
  sed -i -e "/$TARGET/ i$LINE" $file2update;
}

move_line_after()
{
  LINE=$(sed -n "/$MOTIF/p" $file2update)
  sed -i -e "/$MOTIF/d" $file2update
  sed -i -e "/$TARGET/ a$LINE" $file2update
}

append_line()
{
  grep -q "$NEWLINE" $file2update || { sed -i -e "/$PRECLINE/ a$NEWLINE" $file2update; }
}

prepend_line()
{
  grep -q "$NEWLINE" $file2update || { sed -i -e "s/\(\s*\)$NEXTLINE/\1$NEWLINE\n&/" $file2update; }
}

change_line()
{
  grep -q "$NEWLINE" $file2update || { sed -i -e "s/$OLDLINE/$NEWLINE/I" $file2update; }
}

change_begin_line()
{
  sed -i -e "s/^\(\s*\)$OLD/\1$NEW/g" $file2update
}

return_scope()
{
  if ! (grep -q "scope()" $file2update); then
  {
  echo "
CYScope *$CLASS::scope()
{
  return ui->$SCOPE;
}" >> $file2update
  }
  fi
}

#transfert déclarations des fonctions d'un fichier *.ui.h
access_header_ui()
{
  ACCESS_BLOC=""

  #récupération uniquement des lignes de l'accès en cours
  ACCESS_BLOC=$(echo "$BLOC" | sed "/access=\"$ACCESS\"/!d" )

  if [ "$ACCESS_BLOC" = "" ]; then
    return
  fi

  if [ $TYPE = "variable" ]; then
  {
    OLD="<$TYPE access=\"$ACCESS\".*>\(.*\)<\/$TYPE>"
    NEW="\1"
    ACCESS_BLOC=$(echo "$ACCESS_BLOC" | sed "s/$OLD/$NEW/g")
  }
  else
  {
    #déclarations de fonctions
    LINES=$ACCESS_BLOC
    LINE=$(echo "$LINES" | sed -n "1p")
    while [[ $LINES != "" ]]; do
    {
      FUNCTION=$(echo "$LINE" | sed "s/.*>\(.*\)<.*/\1/g")

      #FUNCTION=$(echo "$FUNCTION" | sed "s/(*.)//")
      if [ "$FUNCTION" != "" ]; then
      {
        if (test -e $hfile2update) &&
           (grep -q "$FUNCTION" $hfile2update); then
        {
          #déclaration déja existante
          ACCESS_BLOC=$(echo "$ACCESS_BLOC" | sed "/$FUNCTION/d")
        }
        fi
      }
      fi
      LINES=$(echo "$LINES" | sed "1d")
      LINE=$(echo "$LINES" | sed -n "1p")
    }
    done

    #ajoute returnType="void" si aucun type de retour de précisé dans .ui
    OLD="<$TYPE\(.*\)>\(.*\)<\/$TYPE>"
    NEW="<$TYPE\1 returnType=\"void\">\2<\/$TYPE>"
    ACCESS_BLOC=$(echo "$ACCESS_BLOC" | sed "/returnType=/! s/$OLD/$NEW/g")

    #si fonction statique
    OLD="<$TYPE access=\"$ACCESS\" specifier="static" returnType=\"\(.*\)\">\(.*\)<\/$TYPE>"
    NEW="static \1 \2;"
    ACCESS_BLOC=$(echo "$ACCESS_BLOC" | sed "s/$OLD/$NEW/g")
    #sinon
    OLD="<$TYPE access=\"$ACCESS\".*returnType=\"\(.*\)\">\(.*\)<\/$TYPE>"
    NEW="\1 \2;"
    ACCESS_BLOC=$(echo "$ACCESS_BLOC" | sed "s/$OLD/$NEW/g")
  }
  fi

  if [ "$ACCESS_BLOC" = "" ]; then
    return
  fi

  # passage de la première lettre de l'accès en majuscule
  Access=$(echo "$ACCESS" | sed -r "s/(^.| .)/\U&/g")

  LABEL=""
  if [ $TYPE = "signal" ]; then
    LABEL="signals: ## Signals"
  fi
  if [ $TYPE = "slot" ]; then
    LABEL="$ACCESS slots: ## $Access slots"
  fi
  if [ $TYPE = "function" ]; then
    LABEL="$ACCESS: ## $Access methods"
  fi
  if [ $TYPE = "variable" ]; then
    LABEL="$ACCESS: ## $Access attributes"
  fi

  if [ "$LABEL" = "" ]; then
    echo "Type de membre à importer d'un fichier .ui Qt3" $TYPE
    exit 0
  else
    HEADER_UI=$(echo -e "$HEADER_UI\n$LABEL\n$ACCESS_BLOC\n")
  fi
}

type_header_ui()
{
  BEGIN1="<"$TYPE"s>"
  END1="<\/"$TYPE"s>"
  BLOC=$(cat $uifile2update | sed -ne "/$BEGIN1/,/$END1/p")

  OLD="<$TYPEs>\(.*\)"
  NEW="\1"
  BLOC=$(echo "$BLOC" | sed "s/$OLD/$NEW/g")

  OLD="\(.*\)<\/$TYPEs>"
  NEW="\1"
  BLOC=$(echo "$BLOC" | sed "s/$OLD/$NEW/g")

  #ajout access="public" si aucun accès de précisé dans .ui
  OLD="<$TYPE\(.*\)<\/$TYPE>"
  NEW="<$TYPE access=\"public\" \1<\/$TYPE>"
  BLOC=$(echo "$BLOC" | sed "/access=/! s/$OLD/$NEW/g")

  #echo -e "$TYPE:\n$BLOC"

  ACCESS="public"     access_header_ui
  ACCESS="protected"  access_header_ui
  ACCESS="private"    access_header_ui
}

update_header_ui()
{
  if [ HEADER_UI = "" ]; then
    return
  fi
  cat $file2update
}

header_ui()
{
  HEADER_UI=""
  TYPE="signal"       type_header_ui
  TYPE="slot"         type_header_ui
  TYPE="function"     type_header_ui
  TYPE="variable"     type_header_ui
  HEADER_UI=$(echo "$HEADER_UI" | sed "s/void init()/void initUi()/")
}

import_uih()
{
  TMP=""
  FUNCTIONS=""
  uihfile=$(echo "$uifile2update" | sed "s/$class.ui/$ui_class.ui.h/")
  if ! test -e $uihfile; then
    return
  fi

  TMP=$(cat "$uihfile")
  OPEN="^{"
  CLOSE="^}"
  END="ENDBLOC"
  update_function=0

  #traîtement particulier de l'ancienne fonction init() appelée automatiquement
  #dans les constructeurs des classes générées par les fichiers .ui sous Qt3
  if (grep -q "::init()" $uihfile); then
  {
    #changement de nom pour éviter conflit avec une autre fonction init()
    TMP=$(echo "$TMP" | sed "s/::init()/::initUi()/")
    if !(grep -q "initUi()" $cppfile2update); then
    {
      OLD="ui->setupUi(this);"
      NEW="initUi();"
      sed -i -e "s/\(\s*\)$OLD/\1$OLD\n\1$NEW/" $cppfile2update;
    }
    fi
  }
  fi

  FUNCTIONS=$(echo "$TMP" | sed -n "/$OPEN/{g;1!p;};h" )
  FUNCTIONUIH=$(echo "$FUNCTIONS" | sed -n "1p")
  while [[ $FUNCTIONS != "" ]]; do
  {
    #récupération de la définition d'une fonction dans le fichier .ui.h
    TMP=$(echo "$TMP" | sed "0,/$CLOSE/s//$END/")
    CODEUIH=$(echo "$TMP" | sed -n "/$FUNCTIONUIH/,/$END/p")
    CODEUIH=$(echo "$CODEUIH" | sed "s/$END/}/")
    CODEUIH="\n$CODEUIH"
    TMP=$(echo "$TMP" | sed "/$END/d")

    #fonction dans cpp
    FUNCTION=$(echo "$FUNCTIONUIH" | sed "s/$UI_CLASS::/$CLASS::/")
    #appel dans .cpp de la fonction définie dans .ui.h
    TARGET=$(echo "$FUNCTIONUIH" | sed "s/.* \(.*\)(/\1/")

    LINES=$CODEUIH

    LINES=$(echo "$LINES" | sed "s/$FUNCTIONUIH/$FUNCTION/")
    if (grep -q "$FUNCTION" $cppfile2update); then
    {
      #Fonction existante dans cpp
      update_function=1
      #récupération du code de la fonction mère définie dans .ui.h
      #pour le remplacer dans l'éventuel appel de celle-ci dans
      #la fonction fille existante du cpp
      LINES=$(echo "$LINES" | sed "/$FUNCTION/d")
      LINES=$(echo "$LINES" | sed "/$OPEN/d")
      LINES=$(echo "$LINES" | sed "/$CLOSE/d")
    }
    else
      update_function=0
    fi

    LINE=$(echo "$LINES" | sed -n "1p")
    while [[ $LINES != "" ]]; do
    {
      if [ $update_function = 0 ]; then
      {
        echo -e "$LINE" >> $cppfile2update
      }
#      else
#      {
#        #ajoute ligne de code si appel la fonction définie dans .ui.h
#        echo -e "$LINENO $LINE"
#        # NE MARCHE PAS MOTIF=$LINE add_in_function
#      }
      fi
      LINES=$(echo "$LINES" | sed "1d")
      LINE=$(echo "$LINES" | sed -n "1p")
    }
    done
#    if [ $update_function = 1 ]; then
#    {
#      #suppression dans la fonction fille de l'appel
#      #de la fonction mère définie dans .ui.h
#      MOTIF=$TARGET
#      ## NE MARCHE PAS delete_in_function
#    }
#    fi

    FUNCTIONS=$(echo "$FUNCTIONS" | sed "1d")
    FUNCTIONUIH=$(echo "$FUNCTIONS" | sed -n "1p")
  }
  done
}

#mise à jour de la classe fille d'une classe issue d'un .ui
update_ui_child()
{
  uifile2update=$file2update

  CLASS="";
  UI_CLASS="";
  PARENT_CLASS="";
  PARENT_PARAM="(parent,name)";
  UI_CLASS=$(grep "<class>.*</class>" $file2update | head -n 1)
  UI_CLASS=$(echo "$UI_CLASS" | sed "s/.*<class>\(.*\)<\/class>/\1/")
  if [ "$UI_CLASS" = "" ]; then
    echo "UI_CLASS non trouvée $file2update"
    exit
  fi

  PARENT_CLASS=$(grep "<widget class=" $file2update | head -n 1)
  MOTIF=".*<widget class=\"\(.*\)name=.*"
  PARENT_CLASS=$(echo "$PARENT_CLASS" | sed "s/$MOTIF/\1/" )
  MOTIF=".*<widget class=\"\(.*\)\">"
  PARENT_CLASS=$(echo "$PARENT_CLASS" | sed "s/$MOTIF/\1/" )
  PARENT_CLASS=$(echo "$PARENT_CLASS" | sed "s/\"//" )
  if [ "$PARENT_CLASS" = "" ]; then
    echo "PARENT_CLASS non trouvée $file2update  (ui:$PARENT_CLASS->$UI_CLASS)"
    exit 0
  fi
  if [ $PARENT_CLASS = "QWizard" ]; then
    PARENT_PARAM="(parent)";
    if [[ $UI_CLASS = *Dlg ]]; then
      CLASS=$(echo "$UI_CLASS" | sed "s/Dlg//")
    else
      CLASS=$UI_CLASS
      UI_CLASS=$UI_CLASS"Dlg"
    fi
  fi
  if [ $PARENT_CLASS = "CYEventsPanel" ]; then
    if [[ $UI_CLASS = *Wdg ]]; then
      CLASS=$(echo "$UI_CLASS" | sed "s/Wdg//")
    else
      CLASS=$UI_CLASS
      UI_CLASS=$UI_CLASS"Wdg"
    fi
  fi
  if [ $PARENT_CLASS = "CYFrame" ]; then
    if [[ $UI_CLASS = *Frm ]]; then
      CLASS=$(echo "$UI_CLASS" | sed "s/Frm//")
    else
      CLASS=$UI_CLASS
      UI_CLASS=$UI_CLASS"Frm"
    fi
  fi
  if [ $PARENT_CLASS = "CYDialog" ]; then
    if [[ $UI_CLASS = *Dlg ]]; then
      CLASS=$(echo "$UI_CLASS" | sed "s/Dlg//")
    else
      CLASS=$UI_CLASS
      UI_CLASS=$UI_CLASS"Dlg"
    fi
  fi
  if [ $PARENT_CLASS = "CYWidget" ]; then
    if [[ $UI_CLASS = *Wdg ]]; then
      CLASS=$(echo "$UI_CLASS" | sed "s/Wdg//")
    else
      CLASS=$UI_CLASS
      UI_CLASS=$UI_CLASS"Wdg"
    fi
  fi

  if [ "$CLASS" = "" ]; then
    echo "CLASS non trouvée $file2update (ui:$PARENT_CLASS->$UI_CLASS->$CLASS)"
    exit 0
  fi

  ui_class=$(echo "$UI_CLASS" | tr [A-Z] [a-z])
  parent_class=$(echo "$PARENT_CLASS" | tr [A-Z] [a-z])
  class=$(echo "$CLASS" | tr [A-Z] [a-z])

  hfile2update=$(echo "$uifile2update" | sed 's/\(.*\).ui/\1.h/')
  cppfile2update=$(echo "$uifile2update" | sed 's/\(.*\).ui/\1.cpp/')

  update_ui_child_h
  update_ui_child_cpp

  file2update=$uifile2update
  sed -i -e "s/Wdg//g" $file2update
  sed -i -e "s/Dlg//g" $file2update
  sed -i -e "s/Frm//g" $file2update
  sed -i -e "s/dlg.h/.h/g" $file2update
  sed -i -e "s/wdg.h/.h/g" $file2update
  sed -i -e "s/frm.h/.h/g" $file2update
}

#mise à jour le fichier .cpp de la classe fille d'une classe issue d'un .ui
update_ui_child_cpp()
{
  file2update=$cppfile2update

  if test -e $file2update; then
  {
    echo "Modification fichier :"$file2update

    #appel du fichier d'en-tête généré par le .ui
    filename=$(basename "$file2update")
    filename=$(echo "$filename" | sed "s/.cpp//")
    PRECLINE="#include \""$filename".h\""; NEWLINE="#include \"ui_$filename.h\""; append_line

    #construction du widget ui
    LINE="ui->setupUi(this);";
    OLDLINE=":.*$UI_CLASS(\(.*\),\s*\(.*\))";
    if ! (grep -q "$LINE" $file2update); then
    {
      BEGIN=$OLDLINE
      OLD="^{"
      NEW="{\n\t$LINE$INITUI\n"
      END="^\}"
      sed -i -e "/$BEGIN/,/$END/{ /$BEGIN/b;/$END/b; s/$OLD/$NEW/; }" $file2update
    }
    fi
    if [ $PARENT_PARAM = "(parent,name)" ]; then
      NEWLINE=": $PARENT_CLASS(\1,\2), ui(new Ui::$CLASS)"
    else
      NEWLINE=": $PARENT_CLASS$PARENT_PARAM, ui(new Ui::$CLASS)"
    fi
    sed -i -e "s/$OLDLINE/$NEWLINE/" $file2update

    #destruction du widget ui
    LINE="delete ui;"
    if ! (grep -q "$LINE" $file2update); then
    {
      BEGIN="$CLASS::~$CLASS"
      OLD="{"
      NEW="{\n\t$LINE"
      END="\}"
      sed -i -e "/$BEGIN/,/$END/{ /$BEGIN/b;/$END/b; s/$OLD/$NEW/; }" $file2update
    }
    fi
    #récupère le code du fichier relatif *.ui.h
    import_uih

    #appel aux méthodes du parent
    sed -i -e "s/$UI_CLASS/$PARENT_CLASS/" $file2update

    WIDGET="flagInput_CHP"          prepend_ui_parent
    WIDGET="fl_bypass"              prepend_ui_parent
    WIDGET="pushStart"              prepend_ui_parent
    WIDGET="pushRestart"            prepend_ui_parent
    WIDGET="sepH1"                  prepend_ui_parent
    WIDGET="applyButton"            prepend_ui_parent
    WIDGET="info_bp_manu"           prepend_ui_parent
    WIDGET="info_cb_force"          prepend_ui_parent
    WIDGET="iconView"               prepend_ui_parent
    WIDGET="analyseSheet_CS"        prepend_ui_parent
    WIDGET="analyseSheet_CC"        prepend_ui_parent
    WIDGET="SPVAnalyseSheet"        prepend_ui_parent
    WIDGET="RN1AnalyseSheet"        prepend_ui_parent
    WIDGET="RN2AnalyseSheet"        prepend_ui_parent
    #WIDGET="tabWidget->"            prepend_ui_parent

    OLD="sep_";             NEW="ui->"$OLD;   change_begin_line
    OLD="frame->";          NEW="ui->"$OLD;   change_begin_line
    OLD="label->";          NEW="ui->"$OLD;   change_begin_line
 #   OLD="widgetStack->";    NEW="ui->"$OLD;   change_begin_line
    OLD="TIMER_ABS_COM->";  NEW="ui->"$OLD;   change_begin_line

    sed -i -e "s/return station/return ui->station/g" $file2update
  }
  else
  {
    echo "Création fichier :"$file2update
echo -e "#include \"$class.h\"
#include \"ui_$class.h\"

$CLASS::$CLASS(QWidget *parent, const QString &name)
  : $PARENT_CLASS$PARENT_PARAM, ui(new Ui::$CLASS)
{
  ui->setupUi(this);
}

$CLASS::~$CLASS()
{
  delete ui;
}
" >> $file2update

    #récupère le code du fichier relatif *.ui.h
    import_uih
  }
  fi

  file2update=$uifile2update
}

#mise à jour le fichier .h de la classe fille d'une classe issue d'un .ui
update_ui_child_h()
{
  file2update=$hfile2update

  #récupère les déclarations définies dans .ui
  header_ui

  if test -e $file2update; then
  {
    echo "Modification fichier :"$file2update
    sed -i -e "s/: public $UI_CLASS/: public $PARENT_CLASS/" $file2update
    OLDLINE="#include \""$ui_class".h\"";
    NEWLINE="#include <$parent_class.h>\n\nnamespace Ui {\n\tclass $CLASS;\n}";
    change_line

    OLDLINE="#include \".*\/.*\/.*\/"$ui_class".h\"";
    NEWLINE="#include <$parent_class.h>\n\nnamespace Ui {\n\tclass $CLASS;\n}";
    change_line

    OLDLINE="#include <"$ui_class".h>";
    NEWLINE="#include <$parent_class.h>\n\nnamespace Ui {\n\tclass $CLASS;\n}";
    change_line

    if ! (grep -q "Ui::$CLASS" $file2update); then
    {
      ACCESS=""
      OLD="^\};";
      LINES=$HEADER_UI
      LINE=$(echo -e "$LINES" | sed -n "1p")
      while [[ $LINES != "" ]]; do
      {
        KEY=""
        KEY2=""
        if echo "$LINE" | grep ":" >/dev/null 2>&1; then
          ACCESS=$LINE
        else
          KEY=$(echo "$LINE" | sed "s/^\s*//")
          KEY2=$(echo "$KEY" | sed "s/( \(.*\) )/(\1)/")
        fi

        if ! (grep -qF "$KEY" $file2update) &&
           ! (grep -qF "$KEY2" $file2update); then
        {
          #ajout d'une déclaration
          if [ "$ACCESS" != "" ]; then
          {
            #ajout du nouvel accès
            NEW="\n$ACCESS\n};";
            replace
            ACCESS=""
          }
          fi
          NEW="$LINE\n};";
          replace
        }
        fi
        LINES=$(echo "$LINES" | sed "1d")
        LINE=$(echo "$LINES" | sed -n "1p")
      }
      done

      NEW="\nprivate: \/\/ Private attributes\n  Ui::$CLASS *ui;\n};";
      replace
    }
    fi
  }
  else
  {
    echo "Création fichier :"$file2update

    DEFINE=$(echo "$CLASS" | tr [a-z] [A-Z])
    DEFINE=$DEFINE"_H"
    echo -e "#ifndef $DEFINE
#define $DEFINE

// CYLIBS
#include <$parent_class.h>

namespace Ui {
    class $CLASS;
}

class $CLASS : public $PARENT_CLASS
{
  Q_OBJECT
public:
  explicit $CLASS(QWidget *parent = 0, const QString &name=0);
  ~$CLASS();
$HEADER_UI
private:
  Ui::$CLASS *ui;
};

#endif // $DEFINE
" >> $file2update
  }
  fi
  OLD=": ## "  NEW=": \/\/ " replace

  file2update=$uifile2update
}

#mise à jour des fichiers sources
update_file()
{
  if [ $updatefile = 0 ]; then
    echo "Adaptations fichier : " $dir/$file2update
  else
    file2update=$updatefile
    echo "Adaptations fichier : " $file2update
  fi

  #mises à jour
  sed -i -e "s/i18n/tr/g" $file2update
  sed -i -e "s/I18N_NOOP/QT_TR_NOOP_UTF8/g" $file2update
  sed -i -e "s/I18N/TR/g" $file2update
  sed -i -e "s/CY::/Cy::/g" $file2update
  sed -i -e "s/_Wdg/Wdg/g" $file2update
  sed -i -e "s/_Dlg/Wdg/g" $file2update
  sed -i -e "s/_Frm/Frm/g" $file2update
  sed -i -e "s/name()/objectName()/g" $file2update
  sed -i -e "s/QWidgetStack/QStackedWidget/g" $file2update
  sed -i -e "s/qwidgetstack.h/QStackedWidget/g" $file2update
  sed -i -e "s/paletteBackgroundColor/backgroundColor/g" $file2update
  sed -i -e "s/paletteForegroundColor/foregroundColor/g" $file2update
  sed -i -e "s/paletteBackgroundPixmap/backgroundPixmap/g" $file2update
  sed -i -e "s/setPaletteBackgroundColor/setBackgroundColor/g" $file2update
  sed -i -e "s/setPaletteForegroundColor/setForegroundColor/g" $file2update
  sed -i -e "s/setPaletteBackgroundPixmap/setBackgroundPixmap/g" $file2update
  sed -i -e "s/QPopupMenu/QMenu/g" $file2update
  sed -i -e "s/qpopupmenu.h/QMenu/g" $file2update
  sed -i -e "s/QButtonGroup/CYButtonGroup/g" $file2update
  sed -i -e "s/qbuttongroup.h/cybuttongroup.h/g" $file2update
  sed -i -e "s/QLabel/CYLabel/g" $file2update
  sed -i -e "s/qlabel.h/cylabel.h/g" $file2update
  sed -i -e "s/QLineEdit/CYLineEdit/g" $file2update
  sed -i -e "s/qlineeddit.h/cylineeddit.h/g" $file2update
  sed -i -e "s/QListViewItem/QTreeWidgetItem/g" $file2update
  sed -i -e "s/QScrollView/CYScrollView/g" $file2update
  sed -i -e "s/qscrollview.h/cyscrollview.h/g" $file2update
  sed -i -e "s/QPushButton/CYPushButton/g" $file2update
  sed -i -e "s/qpushbutton.h/cypushbutton.h/g" $file2update
  sed -i -e "s/QIconView/QListWidget/g" $file2update
  sed -i -e "s/qiconview.h/QListWidget/g" $file2update
  sed -i -e "s/Q3IconView/QListWidget/g" $file2update
  sed -i -e "s/q3iconview.h/QListWidget/g" $file2update
  sed -i -e "/QListView::/! s/QListView/QTreeWidget/g" $file2update
  sed -i -e "s/qlistview.h/QTreeWidget/g" $file2update
  sed -i -e "s/CYListView/CYTreeWidget/g" $file2update
  sed -i -e "s/cylistview.h/cytreewidget.h/g" $file2update
  sed -i -e "s/kglobal.h/cyglobal.h/g" $file2update
  sed -i -e "s/KConfig/QSettings/g" $file2update
  sed -i -e "s/konfig.h/QSettings/g" $file2update
  sed -i -e "s/KAction/CYAction/g" $file2update
  sed -i -e "s/kaction.h/cyaction.h/g" $file2update
 # sed -i -e "/KToggleAction/,/;/ s/);/, true);/g" $file2update
  sed -i -e "/KToggleAction/,/;/ s/KToggleAction/CYAction/g" $file2update
  sed -i -e "s/qdict.h/QHash/g" $file2update
  sed -i -e "/QDict</,/>/ s/>/*>/g" $file2update
  sed -i -e "/QDict</,/>/ s/QDict</QHash<QString, /g" $file2update
  sed -i -e "/QDictIterator</,/>/ s/>/*>/g" $file2update
  sed -i -e "/QDictIterator</,/>/ s/QDictIterator</QHashIterator<QString, /g" $file2update
  sed -i -e "s/qintdict.h/QHash/g" $file2update
  sed -i -e "/QIntDict</,/>/ s/>/*>/g" $file2update
  sed -i -e "/QIntDict</,/>/ s/QIntDict</QHash<int, /g" $file2update
  sed -i -e "/QIntDictIterator</,/>/ s/>/*>/g" $file2update
  sed -i -e "/QIntDictIterator</,/>/ s/QIntDictIterator</QHashIterator<int, /g" $file2update
  sed -i -e "s/qptrlist.h/QList/g" $file2update
  sed -i -e "/QPtrList</,/>/ s/>/*>/g" $file2update
  sed -i -e "/QPtrList</,/>/ s/QPtrList</QList</g" $file2update
  sed -i -e "/QPtrListIterator</,/>/ s/>/*>/g" $file2update
  sed -i -e "/QPtrListIterator</,/>/ s/QPtrListIterator</QListIterator</g" $file2update
  sed -i -e "s/kaboutdata.h/cyaboutdata.h/g" $file2update
  sed -i -e "s/KAboutData/CYAboutData/g" $file2update
  sed -i -e "s/KMessageBox/CYMessageBox/g" $file2update
  sed -i -e "s/kmessagebox.h/cymessagebox.h/g" $file2update
  sed -i -e "s/dlg.h/.h/g" $file2update
  sed -i -e "s/wdg.h/.h/g" $file2update
  sed -i -e "s/TRUE/true/g" $file2update
  sed -i -e "s/FALSE/false/g" $file2update
  sed -i -e "s/dbList.find/dbList.value/g" $file2update
  sed -i -e "s/const char \*/QString /g" $file2update
  sed -i -e "s/-\*>/->/g" $file2update
  sed -i -e "s/raiseWidget/setCurrentIndex/g" $file2update
  sed -i -e "s/setCurrentPage/setCurrentIndex/g" $file2update
  OLD="(parent && parent->children())"
  NEW="(parent \&\& parent->children().count())"
  replace
  sed -i -e "s/QObjectList \*l = (QObjectList\*)parent->children()/QList<QWidget *> l = parent->findChildren<QWidget *>()/g" $file2update
  sed -i -e "s/l->count()/l.count()/g" $file2update
  sed -i -e "s/l->at(i)/l.at(i)/g" $file2update
  sed -i -e "s/IO_Append/QIODevice::Append/g" $file2update
  sed -i -e "s/IO_ReadOnly/QIODevice::ReadOnly/g" $file2update
  sed -i -e "s/IO_ReadWrite/QIODevice::ReadWrite/g" $file2update
  sed -i -e "s/IO_Translate/QIODevice::Text/g" $file2update
  sed -i -e "s/IO_Truncate/QIODevice::Truncate/g" $file2update
  sed -i -e "s/IO_WriteOnly/QIODevice::WriteOnly/g" $file2update
  sed -i -e "s/IO_Raw/QIODevice::Unbuffered/g" $file2update
  sed -i -e "s/setColumnText(\(.*\),\(.*\))/model()->setHeaderData(\1, Qt::Horizontal,\2)/g" $file2update
  sed -i -e "s/for.*(QHashIterator\(.*\);\(.*\).current().*;.*++\(.*\))/for (QHashIterator\1;\2.value();\3.next())/g" $file2update
#  sed -i -e "s/.current()/.value()/g" $file2update
  sed -i -e "s/setCaption/setWindowTitle/g" $file2update
  sed -i -e "s/setActiveWindow/activateWindow/g" $file2update
#  sed -i -e "s/findItem(\(.*\), \(.*\));/findItems(\1, Qt::MatchExactly,\2).first();/g" $file2update
#  sed -i -e "s/findItem(\(.*\))/findItems(\1, Qt::MatchExactly).first()/g" $file2update
#  sed -i -e "s/findItems(\(.*\), Qt::MatchExactly,\(.*\)).first();/findItem(\1,\2);/g" $file2update

  sed -i -e "s/QToolTip::add(\s*\(.*\)\s*,\s*/\1->setToolTip(/g" $file2update

  OLD="currentKey()";                 NEW="key()";                            replace
  OLD="ascii()";                      NEW="toLocal8Bit()";                    replace
  OLD="find(";                        NEW="value(";                           replace
  OLD="setItemsMovable(\s*true\s*)";  NEW="setMovement(QListView::Free)";     replace
  OLD="setItemsMovable(\s*false\s*)"; NEW="setMovement(QListView::Static)";   replace
  OLD="kfiledialog.h";                NEW="QFileDialog";                      replace

  CLASS="CYMessageBox" PRECLINE="\/\/ CYLIBS"; NEWLINE="#include <cymessagebox.h>"; add_include
  CLASS="CYActionMenu" PRECLINE="\/\/ CYLIBS"; NEWLINE="#include <cyactionmenu.h>"; add_include

  #suppressions
  sed -i -e "/#include <kstandarddirs.h>/d" $file2update
  sed -i -e "/#include <kimageio.h>/d" $file2update
  sed -i -e "/#include <klocale.h>/d" $file2update
  sed -i -e "/#include <kglobalaccel.h>/d" $file2update
  sed -i -e "/#include <kcmdlineargs.h>/d" $file2update
  sed -i -e "/#include <kconfig.h>/d" $file2update
  sed -i -e "/#include <kdebug.h>/d" $file2update
  sed -i -e "/#include <kcrash.h>/d" $file2update
  sed -i -e "/#include <dcopclient.h>/d" $file2update
  sed -i -e "/#include <qobjectlist.h>/d" $file2update

  sed -i -e "/.moc\"/d" $file2update

  OLD="green\s*:\s*red"    NEW="Qt::green:Qt::red"    replace
  OLD="black\s*:\s*yellow" NEW="Qt::black:Qt::yellow" replace
  OLD="white\s*:\s*black"  NEW="Qt::white:Qt::black"  replace
  OLD="black\s*:\s*green"  NEW="Qt::black:Qt::green"  replace
  OLD="green\s*:\s*black"  NEW="Qt::green:Qt::black"  replace
  OLD="yellow\s*:\s*black" NEW="Qt::yellow:Qt::black" replace
  OLD="green\s*:\s*yellow" NEW="Qt::green:Qt::yellow" replace
  OLD="red\s*:\s*yellow"   NEW="Qt::red:Qt::yellow"   replace
  OLD="yellow\s*:\s*red"   NEW="Qt::yellow:Qt::red"   replace

  OLD="(white)"            NEW="(Qt::white)"          replace
  OLD="(red)"              NEW="(Qt::red)"            replace
  OLD="(green)"            NEW="(Qt::green)"          replace
  OLD="(blue)"             NEW="(Qt::blue)"           replace
  OLD="(cyan)"             NEW="(Qt::cyan)"           replace
  OLD="(magenta)"          NEW="(Qt::magenta)"        replace
  OLD="(yellow)"           NEW="(Qt::yellow)"         replace
  OLD="(gray)"             NEW="(Qt::gray)"           replace

  OLD="(black)"            NEW="(Qt::black)"          replace
  OLD="(darkRed)"          NEW="(Qt::darkRed)"        replace
  OLD="(darkGreen)"        NEW="(Qt::darkGreen)"      replace
  OLD="(darkBlue)"         NEW="(Qt::darkBlue)"       replace
  OLD="(darkcyan)"         NEW="(Qt::darkcyan)"       replace
  OLD="(darkMagenta)"      NEW="(Qt::darkMagenta)"    replace
  OLD="(darkYellow)"       NEW="(Qt::darkYellow)"     replace
  OLD="(darkGray)"         NEW="(Qt::darkGray)"       replace

  OLD="(lightGray)"        NEW="(Qt::lightGray)"      replace

  OLD="selectionChanged( QListWidgetItem * item )"
  NEW="selectionChanged( QListWidgetItem * item, QListWidgetItem * previous )"
  replace

  MOTIF="kapplication.h";   comment_line
  MOTIF="setColumnWidthMode";   comment_line

  if [ "${file2update##*/}" = "main.cpp" ]; then
  {
    #mises à jour
    PRECLINE="\/\/ CYLIX";
    NEWLINE="#include \"core.h\"";
    append_line

 #  OLD="aboutData(\(.*\),\(.*\),\(.*\), description, KAboutData::\(.*\),\(.*\),\(.*\),\(.*\),\(.*\))"
 #  NEW="aboutData(\1,\2,\3, description.toUtf8(), CYAboutData::\4,\5,\6,\7,\8)"
 #  replace

    OLD="KAboutData,"
    NEW="CYAboutData"
    replace

    OLD="description,"
    NEW="description.toUtf8(),"
    replace

    OLD="\") + QString(\" & \")+ QT_TR_NOOP_UTF8(\""
    NEW=" \/ "
    replace

    sed -i -e "s/new Cylix\;/new Cylix(argc, argv, \&aboutData, true)\;/g" $file2update

#    sed -i -e "s/^[ \t]*Cylix* cylix = new Cylix/\tCylix* cylix = new Cylix(argc, argv, \&aboutData, true)/g" $file2update

    #mises en commentaire
    sed -i -e "/^static void crashHandler/ , /^}/ s/^/\/\/CYQT5 /" $file2update
    sed -i -e "/^static void sighandler/ , /^}/ s/^/\/\/CYQT5 /" $file2update
    sed -i -e "/^static KCmdLineOptions/ , /^}/ s/^/\/\/CYQT5 /" $file2update
    sed -i -e "s/^[ \t]*KCmdLineArgs/\/\/CYQT5 &/" $file2update
  }
  fi

  if [ "${file2update##*/}" = "commun.h" ]; then
  {
    #mises à jour
    OLD="#include <\/usr\/include\/cytypes.h>"
    NEW="#include <cytypes.h>"
    replace
  }
  fi

  if [ "${file2update##*/}" = "cylix.h" ]; then
  {
    #mises à jour
    sed -i -e "s/KApplication/CYApplication/g" $file2update
    sed -i -e "s/KUniqueApplication/CYUniqueApplication/" $file2update
    sed -i -e "s/kuniqueapplication/cyapplication/g" $file2update
    sed -i -e "s/^[ \t]*Cylix();/\tCylix(int\& argc, char** argv, CYAboutData *aboutData, bool GUIenabled=true);/" \
              $file2update

    #mises en commentaire
    sed -i -e "s/^[ \t]*void slotSettingsChanged/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^[ \t]*KGlobalAccel/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^[ \t]*class KGlobalAccel/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^[ \t]*void slotRestart/\/\/CYQT5 &/" $file2update
  }
  fi

  if [ "${file2update##*/}" = "cylix.cpp" ]; then
  {
    #mises à jour
    sed -i -e "s/KApplication/CYApplication/g" $file2update
    sed -i -e "s/KUniqueApplication/CYUniqueApplication/" $file2update
    sed -i -e "s/kuniqueapplication/cyapplication/g" $file2update
    sed -i -e "s/::Cylix()/::Cylix( int\& argc, char** argv, CYAboutData *aboutData, bool GUIenabled )/" \
              $file2update
    sed -i -e "s/: CYApplication()/\n\t: CYApplication( argc, argv, aboutData, GUIenabled )/" \
              $file2update
    sed -i -e "s/KGlobal::config()/core->config()/g" $file2update

    #mises en commentaire
    sed -i -e "s/^[ \t]*KGlobal::locale()->insertCatalogue/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^[ \t]*setMainWidget/\/\/CYQT5 &/" $file2update
    sed -i -e "/^void Cylix::slotSettingsChanged/ , /^}/ s/^/\/\/CYQT5 /" $file2update
    sed -i -e "s/^[ \t]*keys/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^[ \t]*c->reparseConfiguration()/\/\/CYQT5 &/" $file2update
    FUNCTION="void Cylix::slotRestart"; comment_function
  }
  fi

  if [ "${file2update##*/}" = "core.h" ]; then
  {
    #mises à jour
    PRECLINE="#ifndef CORE_H";
    NEWLINE="  #define VERSION \"0.50_lib5.00\"";
    append_line

    #mises en commentaire
    sed -i -e "s/^[ \t]*virtual void customHelpMenu/\/\/CYQT5 &/" $file2update
  }
  fi

  if [ "${file2update##*/}" = "core.cpp" ]; then
  {
    #mises à jour
    PRECLINE="\/\/ QT";
    NEWLINE="#include <QDockWidget>";
    append_line

    OLD="customerName());"      NEW="customerName().toUtf8());"     replace
    OLD="designerRef());"       NEW="designerRef().toUtf8());"      replace
    OLD=".arg(VERSION));"       NEW=".arg(VERSION).toUtf8());"      replace
    OLD="tr(\"SUPERVISOR\"));"  NEW="tr(\"SUPERVISOR\").toUtf8());" replace
    OLD="mSPV_Auteur);"         NEW="mSPV_Auteur.toUtf8());"        replace
    OLD="mSPV_Date);"           NEW="mSPV_Date.toUtf8());"          replace

    OLD="questionYesNo(qApp->mainWidget()"
    NEW="questionYesNo(0"
    replace

    sed -i -e "s/formats.replace/formats.insert/g" $file2update
    sed -i -e "s/acquisitions.replace/acquisitions.insert/g" $file2update
    sed -i -e "s/QDockWindow \*dock = new QDockWindow ( win, \"dockPanel\" );/QDockWidget \*dock = new QDockWidget ( tr(\"Messaging panel\"), win );/g" $file2update
    sed -i -e "s/win->addDockWindow(dock, tr( \"Events panel\" ), Bottom, FALSE );/win->addDockWidget(Qt::BottomDockWidgetArea, dock);/g" $file2update

    OLD="addDockWindow(\(.*\),.*, Bottom,.*)"
    NEW="addDockWidget(Qt::BottomDockWidgetArea, \1)"
    replace

    sed -i -e "s/.arg(\(.*\).utf8())/.arg(QString(\1.toUtf8()))/g" $file2update


    #mises en commentaire
    sed -i -e "s/^#include <kmainwindow.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <kmenubar.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <kaboutapplication.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <kpopupmenu.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include \"aboutwdg.h\"/\/\/CYQT5 &/" $file2update
    sed -i -e "/^void Core::customHelpMenu/ , /^}/ s/^/\/\/CYQT5 /" $file2update
    sed -i -e "/^[ \t]*if (!qApp->mainWidget/ , /;/ s/^/\/\/CYQT5 /" $file2update
    sed -i -e "s/^[ \t]*dock->setMovingEnabled/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^[ \t]*dock->setHorizontallyStretchable/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^[ \t]*dock->setFixedExtentHeight/\/\/CYQT5 &/" $file2update
  }
  fi
  if [ "${file2update##*/}" = "mainwin.h" ]; then
  {
    #mises à jour

    #mises en commentaire
    sed -i -e "s/^[ \t]*virtual void customEvent/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^[ \t]*KRecentFilesAction/\/\/CYQT5 &/" $file2update

    #suppressions
    sed -i -e "/KAccel/d" $file2update
  }
  fi

  if [ "${file2update##*/}" = "program.h" ]; then
  {
    #mises à jour
    sed -i -e "s/<qmemarray.h>/<QVector>/g" $file2update
    sed -i -e "s/QMemArray/QVector/g" $file2update

    #mises en commentaire

    #suppressions
  }
  fi

  if [ "${file2update##*/}" = "eventsgeneratorspv.cpp" ]; then
  {
    #mises à jour
    sed -i -e "s/^[ \t]*KUser user;/#if defined(Q_WS_WIN)\r\n\tQString loginName = getenv(\"USERNAME\");\r\n#else\r\n\tQString loginName = getenv(\"USER\");\r\n#endif/g" $file2update
    sed -i -e "s/user.loginName()/loginName/g" $file2update

    #mises en commentaire

    #suppressions
    sed -i -e "/kuser.h/d" $file2update
  }
  fi

  if [ "${file2update##*/}" = "ioframe.h" ]; then
  {
    #mises à jour

    #mises en commentaire

    #suppressions
    sed -i -e "/kuser.h/d" $file2update
  }
  fi

  if [ "${file2update##*/}" = "acquisitionsettings.h" ]; then
  {
    #mises à jour
    PRECLINE="#include <qtabwidget.h>"; NEWLINE="#include <QTreeWidgetItem>"; append_line

    #mises en commentaire
    sed -i -e "s/^#include <qobjectlist.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <qscrollview.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "/^[ \t]*if (item1)/ , /setOpen(item1,false);/ s/^/\/\/CYQT5 /" $file2update
    sed -i -e "s/^[ \t]*listView->setColumnWidthMode/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^[ \t]*listView->ensureItemVisible/\/\/CYQT5 &/" $file2update

    #suppressions
  }
  fi

  if [ "${file2update##*/}" = "acquisitionsettings.cpp" ]; then
  {
    #mises à jour
    PRECLINE="#include <qtabwidget.h>"; NEWLINE="#include <QTreeWidgetItem>"; append_line
    WIDGET="listView"           prepend_ui_parent
    WIDGET="buttonApply"        prepend_ui_parent
    WIDGET="widgetStack"        prepend_ui_parent

    OLD="it.current()"
    NEW="(*it)"
    replace

    sed -i -e "/\/\/.*listView->setSelected/d" $file2update
    sed -i -e "s/^\(\s*\).*->setSelected(\(.*\),\(.*\))/\1\2->setSelected(\3)/g" $file2update

    #mises en commentaire
    sed -i -e "s/^#include <qobjectlist.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <qscrollview.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "/^[ \t]*if (item1)/ , /setOpen(item1,false);/ s/^/\/\/CYQT5 /" $file2update
    MOTIF="setColumnWidthMode";   comment_line
    MOTIF="ensureItemVisible";   comment_line

#    sed -i -e "s/^[ \t]*listView->setColumnWidthMode/\/\/CYQT5 &/" $file2update
#    sed -i -e "s/^[ \t]*listView->ensureItemVisible/\/\/CYQT5 &/" $file2update

    #suppressions
  }
  fi


  if [ "${file2update##*/}" = "listcali.cpp" ]; then
  {
    #mises à jour
    OLD="item.current()"
    NEW="item.value()"
    replace

    sed -i -e "s/new QTreeWidgetItem(\(.*\),\(.*\),\(.*\),\(.*\))\(.*;\)/new QTreeWidgetItem(\1,(QStringList()<<\2<<\3<<\4))\5/g" $file2update
  }
  fi

  if [ "${file2update##*/}" = "maintenancecounters.cpp" ]; then
  {
    #mises à jour
    WIDGET="widgetStack"        prepend_ui_parent
  }
  fi

  if [ "${file2update##*/}" = "netinfo.cpp" ]; then
  {
    #mises à jour
    WIDGET="tabWidget"        prepend_ui_parent
  }
  fi

  if [ "${file2update##*/}" = "ioforcing.cpp" ]; then
  {
    #mises à jour
    sed -i -e "s/^[ \t]*frame/ui->frame/g" $file2update
  }
  fi

  if [ "${file2update##*/}" = "startpulsating.cpp" ] ||
     [ "${file2update##*/}" = "startstatic.cpp"    ] ; then
  {
    #mises à jour
    sed -i -e "s/(CYComboBox\s*\*)child(/findChild<CYComboBox *>(/g" $file2update
    sed -i -e "s/(CYWidget\s*\*)child(/findChild<CYWidget *>(/g" $file2update

    #mises en commentaire
    sed -i -e "s/^[ \t]*stationGroupLayout/\/\/CYQT5 &/" $file2update

    #suppressions
  }
  fi

  if [ "${file2update##*/}" = "dbprj1.cpp" ]; then
  {
    #mises à jour
    sed -i -e "s/projet.ui->/projet./g" $file2update
  }
  fi

  if [ "${file2update##*/}" = "dbcal1.cpp" ]; then
  {
    #mises à jour
    sed -i -e "s/isA(\"CYMeasure\")/metaObject()->className()==\"CYMeasure\"/g" $file2update
  }
  fi

  if [ "${file2update##*/}" = "dbcyc1.cpp" ]; then
  {
    #mises à jour
    OLD="it.current()"  NEW="it.value()"   replace
  }
  fi

  if [ "${file2update##*/}" = "dbinfopc.cpp" ]; then
  {
    #mises à jour
    OLD=".remove(\"V\", false)"  NEW=".remove(\"V\", Qt::CaseInsensitive)"   replace
  }
  fi

  if [ "${file2update##*/}" = "dbprg1.cpp" ]; then
  {
    #mises à jour
    OLD="it.current()"  NEW="it.value()"   replace
  }
  fi

  if [ "${file2update##*/}" = "calcylinder.h" ]; then
  {
    #mises à jour
    CLASS="CYF32" PRECLINE="\/\/ CYLIBS"; NEWLINE="#include <cyf32.h>"; add_include

    #suppression
    sed -i -e "/void help(void)/d" $file2update
  }
  fi

  if [ "${file2update##*/}" = "calcylinder.cpp" ]; then
  {
    #mises à jour
    WIDGET="buttonApply"        prepend_ui_parent
    WIDGET="sheet_grafcet"      prepend_ui_parent
  }
  fi


  if [ "${file2update##*/}" = "ioframe.cpp" ]; then
  {
    #mises à jour
    WIDGET="analyseSheet_TOR"        prepend_ui_parent
    WIDGET="analyseSheet_ANA"        prepend_ui_parent
    WIDGET="sheet_grafcet"           prepend_ui_parent
    WIDGET="widgetStack"             prepend_ui_parent

    MOTIF="Synoptic";     sed -i -e "s/ui->iconView\s*,\s*tr(\"$MOTIF\"))\s*;/tr(\"$MOTIF\")\t\t, ui->iconView);/g" $file2update
    MOTIF="Digital I\/O"; sed -i -e "s/ui->iconView\s*,\s*tr(\"$MOTIF\"))\s*;/tr(\"$MOTIF\")\t\t, ui->iconView);/g" $file2update
    MOTIF="Analog I\/O";  sed -i -e "s/ui->iconView\s*,\s*tr(\"$MOTIF\"))\s*;/tr(\"$MOTIF\")\t\t, ui->iconView);/g" $file2update
    MOTIF="Grafcet";      sed -i -e "s/ui->iconView\s*,\s*tr(\"$MOTIF\"))\s*;/tr(\"$MOTIF\")\t\t, ui->iconView);/g" $file2update
  }
  fi

  if [ "${file2update##*/}" = "ioview.cpp" ]; then
  {
    #mises à jour
    sed -i -e "s/^[ \t]*frame/\tui->frame/g" $file2update
  }
  fi

  if [ "${file2update##*/}" = "dangeroustemperature.cpp" ]; then
  {
    #mises à jour
    WIDGET="buttonOK"        prepend_ui_parent
  }
  fi


  if [ "${file2update##*/}" = "doorsopening.cpp" ]; then
  {
    #mises à jour
    WIDGET="displayTHS"       prepend_ui_parent
    WIDGET="displayTFE"       prepend_ui_parent
    WIDGET="displayTAA"       prepend_ui_parent
    WIDGET="displayPFE"       prepend_ui_parent
    WIDGET="displayPFS"       prepend_ui_parent
    WIDGET="displayEnable"    prepend_ui_parent
    WIDGET="displayDOOR_I"    prepend_ui_parent
    WIDGET="displayDOOR_P"    prepend_ui_parent
    WIDGET="DBFgroup"         prepend_ui_parent
    WIDGET="TAAgroup"         prepend_ui_parent
  }
  fi

  if [ "${file2update##*/}" = "pexdoorsopening.cpp" ]; then
  {
    #mises à jour
    OLD="PEXdoorsopening"    NEW="pexdoorsopening"    replace

    WIDGET="displaySQMA"      prepend_ui_parent
    WIDGET="displaySQVOIE"    prepend_ui_parent
    WIDGET="displayTHS"       prepend_ui_parent
    WIDGET="displayEnable"    prepend_ui_parent
    WIDGET="displayDOOR_V"    prepend_ui_parent
  }
  fi


  if [ "${file2update##*/}" = "sealingtestview.cpp" ]; then
  {
    #mises à jour
    WIDGET="scopePression"      prepend_ui_parent
    WIDGET="scopePosition"    prepend_ui_parent
  }
  fi

  if [ "${file2update##*/}" = "waitstartbursting.cpp" ]; then
  {
    #mises à jour
    NEWLINE="#include <cycommand.h>"  NEXTLINE="WaitStartBursting::WaitStartBursting"  prepend_line
    PRECLINE="#include <cycommand.h>" NEWLINE="#include <cycore.h>"                    append_line
  }
  fi

  if [ "${file2update##*/}" = "mainwin.cpp" ]; then
  {
    #mises à jour
    OLD="PEXdoorsopening"    NEW="pexdoorsopening"    replace

    sed -i -e "s/createGUI(.*)/createGUI()/g" $file2update
    sed -i -e "s/CTRL+Key_/Qt::CTRL+Qt::Key_/g" $file2update
    sed -i -e "/Qt::Key_/! s/Key_/Qt::Key_/g" $file2update
    sed -i -e "s/KStdAccel::shortcut(KStdAccel::Quit)/Qt::CTRL+Qt::Key_Q/g" $file2update
    sed -i -e "s/setStatusText/setStatusTip/g" $file2update
    sed -i -e "s/mLoadProjects.replace/mLoadProjects.insert/g" $file2update
    sed -i -e "s/\(.*\)->setChecked(\(.*\),\s*\(.*\));/\1->setCheckable(\3);\n\1->setChecked(\2);/g" $file2update
    sed -i -e "s/,\s*WType_TopLevel//g" $file2update
    sed -i -e "s/mProjectEditor->isShown()/!mProjectEditor->isHidden()/g" $file2update
    sed -i -e "s/cfg->setGroup/cfg->endGroup(); cfg->beginGroup/g" $file2update
    sed -i -e "s/readBoolEntry(\(.*\)))/value(\1).toBool())/g" $file2update
    sed -i -e "s/writeEntry/setValue/g" $file2update
#    sed -i -e "s/cfg->value(\(\".*\"\)\s*,\s*\(.*\))/cfg->value(\(\".*\"\)\s*,\s*\(.*\))/g" $file2update
#    sed -i -e "s/cfg->readBoolEntry(\(\".*\"\)\s*,\s*\(.*\))/cfg->value(\(\".*\"\)\s*,\s*\(.*\))/g" $file2update

    sed -i -e "s/^\(\s*\)proc->addArgument(\"xdg-open\");/\1QString program = \"xdg-open\";\n\1QStringList arguments;/g" $file2update
    OLD="proc->addArgument(file)"       NEW="arguments << file"             replace
    sed -i -e "s/^\(\s*\)if (!proc->start())/\1proc->start(program, arguments);\n\1if (!proc->waitForStarted())/g" $file2update


    #mises en commentaire
    sed -i -e "s/^[ \t]*frame/\tui->frame/g" $file2update
    sed -i -e "s/^#include <kemailsettings.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <kapp.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <kpopupmenu.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <kmenubar.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <kprocess.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <kiconloader.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^#include <krun.h>/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^\s*KStdAction::configureToolbars/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^\s*toolBar(/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^\s*statusBarTog/\/\/CYQT5 &/" $file2update
    sed -i -e "s/^\s*mStatusLabel/\/\/CYQT5 &/" $file2update

    FUNCTION="void MainWin::showStatusBar";         comment_in_function
    FUNCTION="void MainWin::toggleProjectToolBar";  comment_in_function
    FUNCTION="void MainWin::toggleControlToolBar";  comment_in_function
    FUNCTION="void MainWin::toggleMainToolBar";     comment_in_function
    FUNCTION="void MainWin::togglePreselToolBar";   comment_in_function

    FUNCTION="void MainWin::customEvent"; comment_function

    MOTIF="toolBar("; comment_line
  }
fi

  if [ "${file2update##*/}" = "projecteditor.cpp" ]; then
  {
    #mises en commentaire
    sed -i -e "s/, WFlags f//" $file2update

    OLD="CYProjectEditor(parent, name, f)";  NEW="CYProjectEditor(parent, name)";         replace
    OLD="newProject(\(.*\), QString name,";  NEW="newProject(\1, const QString \&name,";  replace

    OLD="KStdAction::open(this, SLOT(openProject()), actionCollection(), \"open\");";
    NEW="new CYAction(tr(\"\&Open\"       ), \"fileopen\"   , QKeySequence::Open  , this, SLOT(openProject()), actionCollection(), \"open\");";
    replace

    OLD="KStdAction::save(this, SLOT(saveProject()), actionCollection(), \"save\");";
    NEW="new CYAction(tr(\"\&Save\"       ), \"filesave\"   , QKeySequence::Save  , this, SLOT(saveProject()), actionCollection(), \"save\");";
    replace

    OLD="KStdAction::quit(this, SLOT(quit()), actionCollection(), \"quit\");";
    NEW="new CYAction(tr(\"\&Quit\"       ), \"exit\"       , QKeySequence::Quit  , this, SLOT(quit())       , actionCollection(), \"quit\");";
    replace

    OLD="KStdAction::copy(this, SLOT(copy()), actionCollection(), \"copy\");";
    NEW="new CYAction(tr(\"\&Copy\"       ), \"editcopy\"   , QKeySequence::Copy  , this, SLOT(copy())       , actionCollection(), \"copy\");";
    replace

    OLD="KStdAction::paste(this, SLOT(paste()), actionCollection(), \"paste\");";
    NEW="new CYAction(tr(\"\&Paste\"      ), \"editpaste\"  , QKeySequence::Paste , this, SLOT(paste())      , actionCollection(), \"paste\");";
    replace

    OLD="QIconSet(findResource(\(.*\)))";  NEW="\1";  replace
    OLD="\"follow_execution\")";            NEW="\"follow_execution\", true)";  replace

    OLD="createGUI(.*)";  NEW="createGUI()";  replace
    MOTIF="mWizard->showPage(";               comment_line
  }
  fi

  if [ "${file2update##*/}" = "projecteditor.h" ]; then
  {
    #mises en commentaire
    sed -i -e "s/QString name,.*WFlags f=WType_TopLevel/QString name/" $file2update
    OLD="newProject(\(.*\), QString name,";  NEW="newProject(\1, const QString \&name,";  replace

  }
  fi

  if [ "${file2update##*/}" = "benchsettings.h" ]; then
  {
    #mises à jour
    PRECLINE="\/\/ QT";
    NEWLINE="#include <QTreeWidgetItem>";
    append_line
  }
  fi

  if [ "${file2update##*/}" = "benchsettings.cpp" ]; then
  {
    #mises à jour
    OLD="Wdg"                           NEW=""                              replace
    OLD="CTInput_TAA2->hide()"          NEW="hideTAA2()"                    replace
    OLD="scope_PidTemperature_TFE"      NEW="scope()"                       replace
    OLD="scope_PidTemperature_TAA"      NEW="scope()"                       replace
    OLD="scope_PidFlow"                 NEW="scope()"                       replace
    OLD="scope_PidPressure"             NEW="scope()"                       replace
    OLD="scope_PidPosition"             NEW="scope()"                       replace
    OLD="scope_PidBurst"                NEW="scope()"                       replace
    OLD="it.current()"                  NEW="(*it)"                         replace

    OLD="scope().cywdg\").arg(dir.path())"
    NEW="%2.cywdg\").arg(dir.path()).arg(pScope->objectName())"
    replace

    sed -i -e "s/^\(\s*\)proc->addArgument(\"mkdir\");/\1QString program = \"mkdir\";\n\1QStringList arguments;/g" $file2update
    OLD="proc->addArgument(\"-p\")"     NEW="arguments << \"-p\""           replace
    OLD="proc->addArgument(path)"       NEW="arguments << path"             replace
    OLD="arguments << \"-p\" << path"   NEW="proc->addArgument(path);proc->start(program, arguments)"       replace
    sed -i -e "s/^\(\s*\)if (!proc->start())/\1proc->start(program, arguments);\n\1if (!proc->waitForStarted())/g" $file2update

    OLD="^\(\s*\)for (item.toFirst(); CYData \*data=item.current(); ++item)";
    NEW="\1for ( ; CYData \*data=item.value(); item.next())";
    replace

    WIDGET="listView"          prepend_ui_parent
    WIDGET="tAACtrl"           prepend_ui_parent
    WIDGET="varTFEPID"         prepend_ui_parent
    WIDGET="varTAAPID"         prepend_ui_parent
    WIDGET="varDBPPID"         prepend_ui_parent
    WIDGET="varPFEPID"         prepend_ui_parent
    WIDGET="varPOSPID"         prepend_ui_parent
    WIDGET="varPFBPID"         prepend_ui_parent
    WIDGET="buttonApply"       prepend_ui_parent
    WIDGET="widgetStack"       prepend_ui_parent

    sed -i -e "s/^\(\s*\).*->setOpen(\(.*\),\(.*\))/\1\2->setExpanded(\3)/g" $file2update
    sed -i -e "s/^\(\s*\).*->ensureItemVisible(\(.*\))/\1\2->setHidden(false)/g" $file2update
    sed -i -e "s/^\(\s*\).*->setSelected(\(.*\),\(.*\))/\1\2->setSelected(\3)/g" $file2update

    #mises en commentaire
    MOTIF="setColumnWidthMode";   comment_line
    MOTIF="currentChanged";       comment_line
  #  sed -i -e "s/for (uint i=0; i < l.count(); i++)/for (int i=0; i < l.count(); i++)/" $file2update
  }
  fi

  if [ "${file2update##*/}" = "taactrl.h" ]; then
  {
    #mises à jour
    PRECLINE="~";             NEWLINE="void hideTAA2();";    append_line
  }
  fi

  if [ "${file2update##*/}" = "taactrl.cpp" ]; then
  {
    #mises à jour
    if ! (grep -q "hideTAA2()" $file2update); then
    {
    echo "
void TAACtrl::hideTAA2()
{
  ui->CTInput_TAA2->hide();
}" >> $file2update
    }
    fi
  }
  fi

  if [ "${file2update##*/}" = "tfepid.h" ] ||
     [ "${file2update##*/}" = "taapid.h" ] ||
     [ "${file2update##*/}" = "dbppid.h" ] ||
     [ "${file2update##*/}" = "pfepid.h" ] ||
     [ "${file2update##*/}" = "pfbpid.h" ] ||
     [ "${file2update##*/}" = "pospid.h" ]; then
  {
    #mises à jour
    PRECLINE="~";             NEWLINE="CYScope \*scope();";         append_line
    PRECLINE="\/\/ CYLIBS";       NEWLINE="#include \"cyscope.h\""; append_line
  }
  fi

  if [ "${file2update##*/}" = "tfepid.cpp" ]; then
  {
    #mises à jour
    CLASS="TFEPID" SCOPE="scope_PidTemperature_TFE";  return_scope
  }
  fi

  if [ "${file2update##*/}" = "taapid.cpp" ]; then
  {
    #mises à jour
    CLASS="TAAPID" SCOPE="scope_PidTemperature_TAA";  return_scope
  }
  fi

  if [ "${file2update##*/}" = "dbppid.cpp" ]; then
  {
    #mises à jour
    CLASS="DBPPID" SCOPE="scope_PidFlow";  return_scope
  }
  fi

  if [ "${file2update##*/}" = "pfepid.cpp" ]; then
  {
    #mises à jour
    CLASS="PFEPID" SCOPE="scope_PidPressure";  return_scope
  }
  fi

  if [ "${file2update##*/}" = "pospid.cpp" ]; then
  {
    #mises à jour
    CLASS="POSPID" SCOPE="scope_PidPosition";  return_scope
  }
  fi

  if [ "${file2update##*/}" = "pfbpid.cpp" ]; then
  {
    #mises à jour
    CLASS="PFBPID" SCOPE="scope_PidBurst";  return_scope
  }
  fi

  if [ "${file2update##*/}" = "posctrl.cpp" ]; then
  {
    #mises à jour
    PRECLINE="#include \"ui_posctrl.h\"";
    NEWLINE="#include \"core.h\"";
    append_line

    PRECLINE="#include \"ui_posctrl.h\"";
    NEWLINE="#include \"posctrl_2sv.h\"";
    append_line

    OLD="Wdg"   NEW=""    replace

    WIDGET="tabWidget"           prepend_ui_parent
  }
  fi

  if [ "${file2update##*/}" = "pospid.cpp" ]; then
  {
    #mises à jour
    PRECLINE="#include \"ui_pospid.h\"";
    NEWLINE="#include \"core.h\"";
    append_line

    PRECLINE="#include \"ui_pospid.h\"";
    NEWLINE="#include \"pospid_2svcor.h\"";
    append_line

    PRECLINE="#include \"ui_pospid.h\"";
    NEWLINE="#include \"pospid_2sv.h\"";
    append_line


    OLD="Wdg"   NEW=""    replace

    WIDGET="PIDPositiontabWidget"           prepend_ui_parent
  }
  fi

  if [ "${file2update##*/}" = "processleak.cpp" ]; then
  {
    #mises à jour
    WIDGET="fast_level"         prepend_ui_parent
    WIDGET="profilesBox"        prepend_ui_parent
    WIDGET="fld"                prepend_ui_parent
    WIDGET="fast_level"         prepend_ui_parent
    WIDGET="fast_freq"          prepend_ui_parent
    WIDGET="fastBox"            prepend_ui_parent
    WIDGET="nbCalculProfilsFLD" prepend_ui_parent
    WIDGET="minLevelFlowFLD"    prepend_ui_parent
    WIDGET="retention_blt"      prepend_ui_parent
    WIDGET="retention_slt"      prepend_ui_parent
    WIDGET="retentionBox"       prepend_ui_parent
    WIDGET="retention_volume"   prepend_ui_parent

    OLD="cyc.current()"   NEW="cyc.value()"   replace
    OLD="insertItem"      NEW="addItem"   replace
  }
  fi

  if [ "${file2update##*/}" = "mainwinview.h" ]; then
  {
    #mises à jour
    PRECLINE="{";             NEWLINE="Q_OBJECT";    append_line

  }
  fi

  if [ "${file2update##*/}" = "mainwinview.cpp" ]; then
  {
    #mises à jour
    sed -i -e "/takeItem(.*row(.*))/! s/\(\s*\)\(.*\)takeItem(\(.*\))/\1\2takeItem(\2row(\3))/g" $file2update

    WIDGET="burstingView"           prepend_ui_parent
    WIDGET="pulsatingView"          prepend_ui_parent
    WIDGET="staticView"             prepend_ui_parent
    WIDGET="widgetStack"            prepend_ui_parent


    OLD="->scopeBURST";   NEW="->scope()";    replace
    OLD="->scopePULS";    NEW="->scope()";    replace
    OLD="->scope;";       NEW="->scope();";   replace
  }
  fi

  if  [ "${file2update##*/}" = "burstingtestmonitoring.h"  ] ||
      [ "${file2update##*/}" = "pulsatingtestmonitoring.h" ] ||
      [ "${file2update##*/}" = "statictestmonitoring.h" ] ; then
  {
    #mises à jour
    NEXTLINE="class.*:.*";    NEWLINE="class CYScope;";       prepend_line
    PRECLINE="~";             NEWLINE="CYScope \*scope();";    append_line
  }
  fi

  if [ "${file2update##*/}" = "burstingtestmonitoring.cpp" ]; then
  {
    #PRECLINE="$";         NEWLINE="CYScope \*scope();";   append_line
    if ! (grep -q "scope()" $file2update); then
    {
    echo "
CYScope *BurstingTestMonitoring::scope()
{
  return ui->scopeBURST;
}" >> $file2update
    }
    fi
  }
  fi

  if [ "${file2update##*/}" = "pulsatingtestmonitoring.cpp" ]; then
  {
    #PRECLINE="$";         NEWLINE="CYScope \*scope();";   append_line
    if ! (grep -q "scope()" $file2update); then
    {
    echo "
CYScope *PulsatingTestMonitoring::scope()
{
  return ui->scopePULS;
}" >> $file2update
    }
    fi
  }
  fi

  if [ "${file2update##*/}" = "statictestmonitoring.cpp" ]; then
  {
    if ! (grep -q "scope()" $file2update); then
    {
    echo "
CYScope *StaticTestMonitoring::scope()
{
  return ui->scope;
}" >> $file2update
    }
    fi
  }  fi


  if [ "${file2update##*/}" = "netmodbus.h" ]; then
  {
    #mises à jour
    NEXTLINE="#include <cynetmodbus.h>"       NEWLINE="#include <QTimer>" prepend_line
  }
  fi

 if [ "${file2update##*/}" = "netmodbus.cpp" ]; then
  {
    #mises à jour
    OLD="mBuffer.replace";            NEW="mBuffer.insert";    replace
    sed -i -e "s/\(\s*\)\(.*\).start(\(.*\),\s*\(.*\))/\1\2.setSingleShot(\4);\n\1\2.start(\3)/g" $file2update
  }
  fi

  if [ "${file2update##*/}" = "burstingedit.cpp" ]; then
  {
    #mises à jour
    WIDGET="flow"             prepend_ui_parent
    WIDGET="temperature"      prepend_ui_parent
    WIDGET="splitText"        prepend_ui_parent
    WIDGET="rmp1_ph_cons"     prepend_ui_parent
    WIDGET="rmp2_ph_cons"     prepend_ui_parent
    WIDGET="rmp3_ph_cons"     prepend_ui_parent
    WIDGET="rmpleak_ph_cons"  prepend_ui_parent
    WIDGET="TFE_fl"           prepend_ui_parent
    WIDGET="TFE_cons"         prepend_ui_parent
    WIDGET="secondRamp"       prepend_ui_parent
    WIDGET="thirdRamp"        prepend_ui_parent
    WIDGET="leakControl"      prepend_ui_parent
    WIDGET="TFE_cons_OK"      prepend_ui_parent

    OLD="ui->TFE_cons_OK";            NEW="TFE_cons_OK";    replace

  }
  fi

  if [ "${file2update##*/}" = "profile.cpp" ]; then
  {
    #mises à jour
    OLD="setName";                  NEW="setObjectName";            replace
    OLD="it.current().*;.*++it;";   NEW="it.value(), it.next();";   replace
    OLD="it.current()"              NEW="it.value()"                replace

    PRECLINE="#include \"profile.h\""; NEWLINE="#include <QTextStream>"; append_line

    #mises en commentaire
    MOTIF="it.toFirst()"; comment_line
  }
  fi

  if [ "${file2update##*/}" = "profileedit.cpp" ]; then
  {
    WIDGET="flow"             prepend_ui_parent
    WIDGET="t1"               prepend_ui_parent
    WIDGET="t2"               prepend_ui_parent
    WIDGET="t3"               prepend_ui_parent
    WIDGET="t4"               prepend_ui_parent
    WIDGET="sinus"            prepend_ui_parent
    WIDGET="ph_cons"          prepend_ui_parent
    WIDGET="pb_cons"          prepend_ui_parent
    WIDGET="type_profil"      prepend_ui_parent
    WIDGET="ph_it_ale"        prepend_ui_parent
    WIDGET="ph_it_def"        prepend_ui_parent
    WIDGET="pb_it_ale"        prepend_ui_parent
    WIDGET="pb_it_def"        prepend_ui_parent
    WIDGET="trapezeGroup"     prepend_ui_parent
    WIDGET="sl_ui"            prepend_ui_parent
    WIDGET="label_ui"         prepend_ui_parent
    WIDGET="sineGroup"        prepend_ui_parent
    WIDGET="teta"             prepend_ui_parent
    WIDGET="rmp_T1"           prepend_ui_parent
    WIDGET="rmp_T2"           prepend_ui_parent
    WIDGET="rmp_T3"           prepend_ui_parent
    WIDGET="rmp_T4"           prepend_ui_parent
    WIDGET="rmp_sinus"        prepend_ui_parent
    WIDGET="title"            prepend_ui_parent
    WIDGET="time"             prepend_ui_parent
    WIDGET="freq"             prepend_ui_parent
    WIDGET="freq_Hz"          prepend_ui_parent

    OLD="mRmp_ui->sinus";       NEW="mRmp_sinus";    replace
    OLD="rmp_ui->sinus";        NEW="rmp_sinus";     replace
    OLD="&ui->title";           NEW="\&title";     replace
    OLD="ui->freq_max";         NEW="freq_max";     replace

    OLD="sl_ui->t1";            NEW="sl_t1";         replace
    OLD="sl_ui->t2";            NEW="sl_t2";         replace
    OLD="sl_ui->t3";            NEW="sl_t3";         replace
    OLD="sl_ui->t4";            NEW="sl_t4";         replace

    OLD="label_ui->t1";         NEW="label_t1";         replace
    OLD="label_ui->t2";         NEW="label_t2";         replace
    OLD="label_ui->t3";         NEW="label_t3";         replace
    OLD="label_ui->t4";         NEW="label_t4";         replace

    OLD="datas.find";           NEW="datas.value";      replace

    #mises en commentaire
  }
  fi

  if [ "${file2update##*/}" = "program.cpp" ]; then
  {
    #mises à jour
    OLD="setName";                  NEW="setObjectName";            replace
    OLD="it.current().*;.*++it;";   NEW="it.value(), it.next();";   replace
    OLD="programs.replace";         NEW="programs.insert";          replace
    OLD="profiles.replace";         NEW="profiles.insert";          replace
    OLD="++pgm";                    NEW="pgm.next()";               replace
    OLD="++cyc";                    NEW="cyc.next()";               replace
    OLD="mProgramsList.remove(";    NEW="mProgramsList.removeAll("; replace
    OLD="datas.find";               NEW="datas.value";              replace
    OLD="pgm.current()"             NEW="pgm.value()"               replace
    OLD="cyc.current()"             NEW="cyc.value()"               replace

    OLD="^\(\s*\)for (Program \*parent = p->mParents.first(); parent; parent = p->mParents.next())";
    NEW="\1QListIterator<Program*> parents(p->mParents);\n\1while(parents.hasNext())";
    replace
    OLD="mProgramsList.removeAll(parent->title())";NEW="mProgramsList.removeAll(parents.next()->title())"; replace

    PRECLINE="#include \"profile.h\""   NEWLINE="#include <QTextStream>"  append_line

    #mises en commentaire
    MOTIF="it.toFirst()"; comment_line
  }
  fi

  if [ "${file2update##*/}" = "programedit.h" ]; then
  {
    PRECLINE="class Program;"           NEWLINE="class Profile;"          append_line
  }
  fi

  if [ "${file2update##*/}" = "programedit.cpp" ]; then
  {
    WIDGET="cyc_0"            prepend_ui_parent
    WIDGET="typeCyc_0"        prepend_ui_parent
    WIDGET="timeCyc_0"        prepend_ui_parent
    WIDGET="nbCyc_0"          prepend_ui_parent
    WIDGET="cyc_1"            prepend_ui_parent
    WIDGET="typeCyc_1"        prepend_ui_parent
    WIDGET="timeCyc_1"        prepend_ui_parent
    WIDGET="nbCyc_1"          prepend_ui_parent
    WIDGET="cyc_2"            prepend_ui_parent
    WIDGET="typeCyc_2"        prepend_ui_parent
    WIDGET="timeCyc_2"        prepend_ui_parent
    WIDGET="nbCyc_2"          prepend_ui_parent
    WIDGET="pgm_0"            prepend_ui_parent
    WIDGET="timePgm_0"        prepend_ui_parent
    WIDGET="nbPgm_0"          prepend_ui_parent
    WIDGET="pgm_1"            prepend_ui_parent
    WIDGET="timePgm_1"        prepend_ui_parent
    WIDGET="nbPgm_1"          prepend_ui_parent

    OLD="datas.find";           NEW="datas.value";        replace
    OLD="clearEdit(";           NEW="clearEditText(";     replace
    OLD="insertStringList";     NEW="addItems";           replace
    OLD="text(i)";              NEW="itemText(i)";        replace
    OLD="setCurrentItem(i)";    NEW="setCurrentIndex(i)"; replace

    sed -i -e "s/!title->/!ui->title->/g" $file2update
  }
  fi

  if [ "${file2update##*/}" = "programitem.cpp" ]; then
  {
    OLD="mParents.remove(";     NEW="mParents.removeAll(";replace

    OLD="^\(\s*\)for (pgm = mParents.first(); pgm; pgm = mParents.next())";
    NEW="\1QListIterator<Program*> parents(mParents);\n\1while(parents.hasNext())";
    replace

    NEWLINE="pgm=parents.next();";
    NEXTLINE="if (pgm->isEnabled())";
    prepend_line

    NEWLINE="#include <QTextStream>";
    NEXTLINE="\/\/ CYLIBS";
    prepend_line
  }
  fi

  if [ "${file2update##*/}" = "project.h" ]; then
  {
    # déplacer les membres déclarés par erreur en tant que slots
    TARGET="public: \/\/ Public attributes"
    MOTIF="QStringList profilesList"                   move_line_after
    MOTIF="Liste par titres des profils disponibles"   move_line_after
    MOTIF="QHash<int, Profile"                         move_line_after
    MOTIF="Liste totale des profils"                   move_line_after
  }
  fi


  if [ "${file2update##*/}" = "project.cpp" ]; then
  {
    OLD="programs.replace(";      NEW="programs.insert(";  replace
    OLD="profiles.replace(";      NEW="profiles.insert(";  replace
    OLD="++pgm";                  NEW="pgm.next()";        replace
    OLD="++cyc";                  NEW="cyc.next()";        replace
    OLD="pgm.current()"           NEW="pgm.value()"        replace
    OLD="cyc.current()"           NEW="cyc.value()"        replace
    OLD="p.current()"             NEW="p.value()"        replace

    PRECLINE="\/\/ QT";
    NEWLINE="#include <QTextStream>";
    append_line

    sed -i -e "s/^\(\s*\)proc->addArgument(\"mkdir\");/\1QString program = \"mkdir\";\n\1QStringList arguments;/g" $file2update
    OLD="proc->addArgument(\"-p\")"     NEW="arguments << \"-p\""           replace
    OLD="proc->addArgument(path)"       NEW="arguments << path"             replace
    sed -i -e "s/^\(\s*\)if (!proc->start())/\1proc->start(program, arguments);\n\1if (!proc->waitForStarted())/g" $file2update
  }
  fi

  if [ "${file2update##*/}" = "projectview.h" ]; then
  {
    MOTIF="mFollowColor";   comment_line
    MOTIF="mDisableColor";  comment_line

    PRECLINE="\/\/ CYLIX";
    NEWLINE="#include \"project.h\"";
    append_line

    NEWLINE="Project \*mProject;"
    if ! (grep -q "$NEWLINE" $file2update); then
      OLD="^\(\s*\)bool mInvalidView;"
      NEW="\1bool mInvalidView;\n\1$NEWLINE"
      replace
    fi
  }
  fi

  if [ "${file2update##*/}" = "projectview.cpp" ]; then
  {
    WIDGET="listView"        prepend_ui_parent
    WIDGET="programEdit"     prepend_ui_parent
    WIDGET="profileEdit"     prepend_ui_parent
    WIDGET="burstingEdit"    prepend_ui_parent
    WIDGET="staticEdit"      prepend_ui_parent
    WIDGET="buttonApply"     prepend_ui_parent
    WIDGET="widgetStack"     prepend_ui_parent

    sed -i -e "s/^\(\s*\)view->/\1ui->view->/g" $file2update

    OLD="pal.setDisabled(mDisableColor);"
    NEW="pal.setCurrentColorGroup(QPalette::Disabled);"
    replace

    OLD="pal.setDisabled(mFollowColor);"
    NEW="pal.setCurrentColorGroup(QPalette::Active);"
    replace

    OLD="setSorting(-1)"
    NEW="setSortingEnabled(false)"
    replace

    OLD="ui->listView->ensureItemVisible(\(.*\));"
    NEW="\1->setHidden(false);"
    replace

    OLD="CYWidget::ui->listView"    NEW="ui->listView"      replace
    OLD="it.value()"                NEW="(*it)"             replace
    OLD="tr(pgm->objectName())"     NEW="pgm->objectName()" replace
    OLD="firstChild()"              NEW="takeChild(0)"      replace
    OLD="it.current()"              NEW="(*it)"             replace

    OLD="child->nextSibling()"
    NEW="ui->listView->nextSibling(child)"
    replace

    sed -i -e "s/new QTreeWidgetItem(\(.*\),\(.*\),\(.*\),\(.*\)*);/new QTreeWidgetItem(\1,(QStringList()<<\2<<\3<<\4));/g" $file2update

    MOTIF="mFollowColor";         comment_line
    MOTIF="mDisableColor";        comment_line
    MOTIF="setColumnWidthMode";   comment_line
  }
  fi

  if [ "${file2update##*/}" = "projectwizard.cpp" ]; then
  {
    WIDGET="nameLine"         prepend_ui_parent
    WIDGET="directoryLine"    prepend_ui_parent
    WIDGET="useProjectCheck"  prepend_ui_parent
    WIDGET="useProjectLine"   prepend_ui_parent

    OLD="setHelpEnabled(QWizard::page(0), false);"
    NEW="setOption(QWizard::HaveHelpButton, false);"
    replace

    OLD="setBackEnabled(QWizard::page(0), false);"
    NEW="setOption(QWizard::NoBackButtonOnStartPage, true);"
    replace

    sed -i -e "/setHelpEnabled(QWizard::page(1), false);/d" $file2update
    MOTIF="setFinishEnabled" comment_line

    OLD="finishButton()->setText(\(.*\));"
    NEW="setButtonText(QWizard::FinishButton, \1);"
    replace

    OLD="^\(\s*\)description"
    NEW="\1ui->description"
    replace

    OLD="CYLYBS"    NEW="CYLIBS"      replace

    FUNCTION="void ProjectWizard::showPage(QWidget \*page)"; comment_function

    OLD="KFileDialog::getOpenFileName(\(.*\),\(.*\),\(.*\)));"
    NEW="QFileDialog::getOpenFileName(\3,\1,\2));"
    replace

    OLD="KFileDialog::getExistingDirectory(\(.*\),\(.*\)));"
    NEW="QFileDialog::getExistingDirectory(\2,\1));"
    replace

    OLD="KURL" NEW="QUrl" replace

    PRECLINE="\/\/ QT";
    NEWLINE="#include <QFileDialog>";
    append_line

    PRECLINE="\/\/ QT";
    NEWLINE="#include <QUrl>";
    append_line
  }
  fi

  if [ "${file2update##*/}" = "projectwizard.h" ]; then
  {
    PRECLINE="class Project;"           NEWLINE="class CYWidget;"          append_line

    MOTIF="showPage(QWidget \*page)"; comment_line
  }
  fi

  if [ "${file2update##*/}" = "staticedit.cpp" ]; then
  {
    WIDGET="flow"         prepend_ui_parent
  }
  fi

  if [ "${file2update##*/}" = "pvcreator.cpp" ]; then
  {
    OLD="kprocess"              NEW="qprocess"              replace
    OLD="QObject(parent,name)"  NEW="QObject(parent)"       replace
    OLD="PvDlg"                 NEW="Pv"                    replace
    OLD="test->insertItem"      NEW="addTestType"           replace
  }
  fi

  if [ "${file2update##*/}" = "pvcreator.h" ]; then
  {
    PRECLINE="\/\/ QT";
    NEWLINE="#include <QTextStream>";
    append_line
  }
  fi

  if [ "${file2update##*/}" = "pv.h" ]; then
  {
    PRECLINE="~Pv()";
    NEWLINE="QString currentTestType();";
    append_line

    PRECLINE="~Pv()";
    NEWLINE="void addTestType(QString desc);";
    append_line
  }
  fi

  if [ "${file2update##*/}" = "pv.cpp" ]; then
  {
    if ! (grep -q "addTestType(" $file2update); then
    {
      echo "
void Pv::addTestType(QString desc)
{
test->insertItem(desc);
}" >> $file2update
    }
    fi

    if ! (grep -q "currentTestType()" $file2update); then
    {
      echo "
QString Pv::currentTestType()
{
return test->currentText();
}" >> $file2update
     }
     fi

    PRECLINE="#include \"ui_pv.h\""   NEWLINE="#include <QFileDialog>"  append_line
    PRECLINE="#include <QFileDialog>" NEWLINE="#include \"core.h\""     append_line
    PRECLINE="#include \"core.h\""    NEWLINE="#include \"project.h\""  append_line

    WIDGET="currentDir"             prepend_ui_parent
    WIDGET="directoryLine"          prepend_ui_parent
    WIDGET="dirButton"              prepend_ui_parent
    WIDGET="currentDir"             prepend_ui_parent
    WIDGET="test"                   prepend_ui_parent

    OLD="insertItem"                NEW="addItem"           replace
    OLD="ui->testDirPath"           NEW="testDirPath"       replace

    OLD="KFileDialog::getExistingDirectory(\(.*\),\(.*\))"
    NEW="QFileDialog::getExistingDirectory(\2,\1)"
    replace
  }
  fi

  if [ "${file2update##*/}" = "pvcreator.cpp" ]; then
  {
    OLD="test->currentText()"
    NEW="currentTestType()"
    replace

    PRECLINE="~Pv()";
    NEWLINE="QString currentTestType();";
    append_line

    PRECLINE="~Pv()";
    NEWLINE="void addTestType(QString desc);";
    append_line

    OLD="KShellProcess \(.*\);"
    NEW="QProcess \1;"
    replace

    OLD="shellP1 <<\(.*\)<<\(.*\)<<\(.*\);"
    NEW="shellP1.start(\1,QStringList()<<\2<<\3);"
    replace

    OLD="shellP2 <<\(.*\);"
    NEW="shellP2.start(\1);"
    replace

    sed -i -e "/start(KProcess::DontCare)/d" $file2update
  }
  fi

  if [ "${file2update##*/}" = "settings.h" ]; then
  {
    #mises à jour
    PRECLINE="#include <qtabwidget.h>"; NEWLINE="#include <QTreeWidgetItem>"; append_line
  }
  fi

  if [ "${file2update##*/}" = "settings.cpp" ]; then
  {
    WIDGET="buttonApply"         prepend_ui_parent
    WIDGET="widgetStack"         prepend_ui_parent
    WIDGET="listView"            prepend_ui_parent
  }
  fi

  if [ "${file2update##*/}" = "spvproject.cpp" ]; then
  {
    OLD="KFileDialog::getOpenFileName(\(.*\),\(.*\),\(.*\));"
    NEW="QFileDialog::getOpenFileName(\3,\1,\2);"
    replace
  }
  fi

  if [ "${file2update##*/}" = "spvpv.cpp" ]; then
  {
    OLD="KFileDialog::getOpenFileName(\(.*\),\(.*\),\(.*\));"
    NEW="QFileDialog::getOpenFileName(\3,\1,\2);"
    replace

    WIDGET="pulsatingTemplateLine"        prepend_ui_parent
    WIDGET="burstingTemplateLine"         prepend_ui_parent
    WIDGET="staticTemplateLine"           prepend_ui_parent
    WIDGET="staticTemplateButton"         prepend_ui_parent
  }
  fi

  if [ "${file2update##*/}" = "synopticac.h" ] ||
     [ "${file2update##*/}" = "synopticec.h" ]; then
  {
    NEXTLINE="namespace Ui"           NEWLINE="class CYTextLabel;"          prepend_line
  }
  fi

  if [ "${file2update##*/}" = "synopticac.cpp" ] ||
     [ "${file2update##*/}" = "synopticec.cpp" ]; then
  {
    WIDGET="textPoste"            prepend_ui_parent

    OLD="ui->textPoste("
    NEW="textPoste("
    replace

    OLD="QToolTip::remove(\s*\(.*\)[ ]s*)"
    NEW="\1->setToolTip(\"\")"
    replace
  }
  fi

  if [ "${file2update##*/}" = "synoview.h" ]; then
  {
    #mises à jour
    sed -i -e "s/^[ \t]*frame/ui->frame/g" $file2update

    #suppressions
    sed -i -e "/vbox.h/d" $file2update
  }
  fi


  if [ "${file2update##*/}" = "synoview.cpp" ]; then
  {
    #mises à jour
    OLD="addChild("
    NEW="setWidget("
    replace
  }
  fi

  if [ "${file2update##*/}" = "testdescription.cpp" ]; then
  {
    #mises à jour
    WIDGET="directory"          prepend_ui_parent
  }
  fi
}

#changement du nom d'une propriété d'une classe dans un fichier .ui
rename_property_ui()
{
  BEGIN="<widget class=\"$CLASS\""
  END="<\/widget>"
  sed -i -e "/$BEGIN/,/$END/{ /$BEGIN/b;/$END/b; s/\"$PROPERTY\"/\"$NEW_PROPERTY\"/; }" $file2update
}

type_property_ui()
{
  BEGIN1="<widget class=\"$CLASS\""
  END1="<\/widget>"
  BEGIN2="<property name=\"$PROPERTY\">"
  END2="<\/property>"
  MOTIF="<$TYPE>"
  NEW_MOTIF="<$NEW_TYPE>"
  sed -i -e "/$BEGIN1/,/$END1/{ /$BEGIN1/b;/$END1/b; /$BEGIN2/{:z;N;/$END2/!bz;s/$MOTIF/$NEW_MOTIF/}; }" $file2update
  MOTIF="<\/$TYPE>"
  NEW_MOTIF="<\/$NEW_TYPE>"
  sed -i -e "/$BEGIN1/,/$END1/{ /$BEGIN1/b;/$END1/b; /$BEGIN2/{:z;N;/$END2/!bz;s/$MOTIF/$NEW_MOTIF/}; }" $file2update
}

type_property_ui2()
{
  BEGIN1="<property name=\"$PROPERTY\">"
  END1="<\/property>"
  MOTIF="<$TYPE>"
  NEW_MOTIF="<$NEW_TYPE>"
  sed -i -e "/$BEGIN1/,/$END1/{ /$BEGIN1/b;/$END1/b; /$BEGIN2/{:z;N;/$END2/!bz;s/$MOTIF/$NEW_MOTIF/}; }" $file2update
  MOTIF="<\/$TYPE>"
  NEW_MOTIF="<\/$NEW_TYPE>"
  sed -i -e "/$BEGIN1/,/$END1/{ /$BEGIN1/b;/$END1/b; /$BEGIN2/{:z;N;/$END2/!bz;s/$MOTIF/$NEW_MOTIF/}; }" $file2update
}

#changement d'une propriété en attribut équivalent d'une classe dans un fichier .ui
property_attribute_ui()
{
  BEGIN1="<widget class=\"$CLASS\""
  END1="<\/widget>"
  NEW_PROPERTY=$ATTRIBUTE
  rename_property_ui
  BEGIN2="<property name=\"$ATTRIBUTE\">"
  END2="<\/property>"
  sed -i -e "/$BEGIN1/,/$END1/{ /$BEGIN1/b;/$END1/b; /$BEGIN2/{:z;N;/$END2/!bz;s/property/attribute/g }; }" $file2update
}


#changement de valeur d'une propriété d'une classe dans un fichier .ui
set_property_ui()
{
  BEGIN1="<widget class=\"$CLASS\""
  END1="<\/widget>"
  BEGIN2="<property name=\"$PROPERTY\">"
  END2="<\/property>"
  sed -i -e "/$BEGIN1/,/$END1/{ /$BEGIN1/b;/$END1/b; /$BEGIN2/{:z;N;/$END2/!bz;s/>$VALUE</>$NEW_VALUE</}; }" $file2update
}

#suppression d'une propriété d'une classe dans un fichier .ui
delete_property_ui()
{
  BEGIN1="<widget class=\"$CLASS\""
  END1="<\/widget>"
  BEGIN2="<property name=\"$PROPERTY\">"
  END2="<\/property>"
  sed -i -e "/$BEGIN1/,/$END1/{ /$BEGIN1/b;/$END1/b; /$BEGIN2/{:z;N;/$END2/!bz;/$PROPERTY/d}; }" $file2update
}

# mise à jour des fichiers .ui
update_file_ui()
{
  if [ $updatefile = 0 ]; then
    echo "Adaptations fichier : " $dir/$file2update
  else
    file2update=$updatefile
    echo "Adaptations fichier : " $file2update
  fi

  CLASS="CYDisplaySimple"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYMultimeter"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYButtonGroup"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posXName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posYName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYCheckBox"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYColorButton"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYComboBox"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYFlagInput"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="stringtrue"       NEW_PROPERTY="stringTrue"        rename_property_ui
  PROPERTY="stringfalse"      NEW_PROPERTY="stringFalse"       rename_property_ui

  CLASS="CYDateWidget"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYDial"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYLabel"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYMoviePixmap"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="dataXName"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="dataYName"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYPixmapView"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFilefalse"  TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   NEW_PROPERTY="pixmapFileTrue"    rename_property_ui
  PROPERTY="pixmapFilefalse"  NEW_PROPERTY="pixmapFileFalse"   rename_property_ui

  CLASS="CYCooler"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFilefalse"  TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   NEW_PROPERTY="pixmapFileTrue"    rename_property_ui
  PROPERTY="pixmapFilefalse"  NEW_PROPERTY="pixmapFileFalse"   rename_property_ui

  CLASS="CYHeater"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFilefalse"  TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   NEW_PROPERTY="pixmapFileTrue"    rename_property_ui
  PROPERTY="pixmapFilefalse"  NEW_PROPERTY="pixmapFileFalse"   rename_property_ui

  CLASS="CYMovieView"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFilefalse"  TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFile"       TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="movieFile"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="dataIndexFile"    TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   NEW_PROPERTY="pixmapFileTrue"    rename_property_ui
  PROPERTY="pixmapFilefalse"  NEW_PROPERTY="pixmapFileFalse"   rename_property_ui

  CLASS="CYPump"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFilefalse"  TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFile"       TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="movieFile"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="dataIndexFile"    TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   NEW_PROPERTY="pixmapFileTrue"    rename_property_ui
  PROPERTY="pixmapFilefalse"  NEW_PROPERTY="pixmapFileFalse"   rename_property_ui

  CLASS="CYVentilator"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFilefalse"  TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFile"       TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="movieFile"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="dataIndexFile"    TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   NEW_PROPERTY="pixmapFileTrue"    rename_property_ui
  PROPERTY="pixmapFilefalse"  NEW_PROPERTY="pixmapFileFalse"   rename_property_ui

  CLASS="CYValve3"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFilefalse"  TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   NEW_PROPERTY="pixmapFileTrue"    rename_property_ui
  PROPERTY="pixmapFilefalse"  NEW_PROPERTY="pixmapFileFalse"   rename_property_ui

  CLASS="CYValve"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFilefalse"  TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="pixmapFiletrue"   NEW_PROPERTY="pixmapFileTrue"    rename_property_ui
  PROPERTY="pixmapFilefalse"  NEW_PROPERTY="pixmapFileFalse"   rename_property_ui

  CLASS="CYTextLabel"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="forcingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="settingName"      TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYLed"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYLevelView"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYLineEdit"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posXName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posYName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYNumDisplay"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posXName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posYName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYTextInput"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posXName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posYName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYNumInput"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posXName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="posYName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="stepDataName"     TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYPushButton"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYRobotTopView"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="lengthDataName"   TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYSlider"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="lengthDataName"   TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYTextEdit"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYWheel"
  PROPERTY="dataName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="flagName"         TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="hideFlagName"     TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="benchType"        TYPE="string" NEW_TYPE="cstring" type_property_ui
  PROPERTY="stepDataName"     TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYFrame"
  PROPERTY="genericMark"      TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYFrame"
  PROPERTY="genericMark"      TYPE="string" NEW_TYPE="cstring" type_property_ui

  CLASS="CYFrame"
  PROPERTY="genericMark"      TYPE="string" NEW_TYPE="cstring" type_property_ui

#  update_ui_child
}

dir_src()
{
  echo "Répertoire source:" $nbFiles $dir
  DEPENDPATH=$DEPENDPATH" \\ \r\n"$path

#  #passage en minuscule des fichiers sources pour les besoins du script
#  cmd="tr [A-Z] [a-z]"
#  for file in *.cpp *.c *.h *.def *.ui *.ui.h; do
#    [ -e $file ] \
#    && mv "$file" "$(echo $file | tr A-Z a-z)"
#  done

  if [ $tp = 0 ]; then
    for file in *.ui; do
      [ -e $file ] \
      && FORMS=$FORMS" \\ \r\n\t\$\$PWD/"$file \
      && file2update=$file && update_file_ui
    done
  fi

#  for file in *.cpp *.c; do
#    [ -e $file ] \
#    && SOURCES=$SOURCES" \\ \r\n\t\$\$PWD/"$file \
#    && file2update=$file && update_file
#  done

#  for file in *.h *.def !.ui.h; do
#    [ -e $file ] \
#    && HEADERS=$HEADERS" \\ \r\n\t\$\$PWD/"$file \
#    && file2update=$file && update_file
#  done

#  rm ./*.ui.h
}

create_pri()
{
  echo "CRÉATION DU FICHIER PRI" $file_pri
  rm $file_pri
  echo -e $SOURCES"\r\n\r\n" >> $file_pri
  echo -e $HEADERS"\r\n\r\n" >> $file_pri
  echo -e $FORMS"\r\n\r\n" >> $file_pri
  echo -e $DISTFILES"\r\n\r\n">> $file_pri
  sed -i -e "s/_wdg.ui/.ui/g" $file_pri
  sed -i -e "s/_dlg.ui/.ui/g" $file_pri
  sed -i -e "s/wdg.ui/.ui/g" $file_pri
  sed -i -e "s/dlg.ui/.ui/g" $file_pri
}

change_dir()
{
  if [ $updatedir = 0 ]; then
    input_dir
  else
    if [ $dir = $updatedir ]; then
      echo "Mise à jour uniquement du répertoire" $dir
      input_dir
    fi
  fi

  # parcours les sous-répertoires
  for i in * ; do [ -d $i ] && dir=$i && cd $i && change_dir ; done;

  cd ..
 }

input_dir()
{
  echo "============ ENTRÉE RÉPERTOIRE ============:" $dir

  SOURCES="SOURCES   +="
  HEADERS="HEADERS   +="
  FORMS="FORMS     +="
  DISTFILES="DISTFILES +="

#  #suppression des sources générées de Qt3 Designer
#  filtre="./*wdg.h ./*wdg.cpp ./*.dlg.h ./*dlg.cpp"
#  nbFiles=$(ls $filtre | wc -l)
#  if [ $nbFiles = 0 ]; then
#    rm $nbFiles
#  fi

#  file_pri="$dir.pri"
#  if [ $dir = "src" ]; then
#    INCLUDEPRI=$INCLUDEPRI"include ( \$\${PWD}/$file_pri )\r\n"
#    path="./"
#  else
#    path=$dir
#    parentdir="$(dirname "$(pwd)")"
#    parent=$(basename "$parentdir");
#    if [ $parent = "src" ] ;then
#      INCLUDEPRI=$INCLUDEPRI"include ( \$\${PWD}/$dir/$file_pri )\r\n"
#    fi
#  fi

#  for file in *.desktop *.cyas *.rc *.ods *.pdf; do
#    [ -e $file ] \
#    && DISTFILES=$DISTFILES" \\ \r\n\t\$\$PWD/"$file
#  done

#  #traitement fichiers en-tête
#  nbFiles=$(ls *.h *.def | wc -l)
#  if [ $nbFiles = 0 ]; then
#    echo "Pas de fichier en-tete"
#  else
#    echo "Répertoire include:" $nbFiles $dir
#    INCLUDEPATH=$INCLUDEPATH" \\ \r\n"$dir
#  fi

#  nbFiles=$(ls *.cpp *.ui *.h *.def | wc -l)
  nbFiles=$(ls *.* | wc -l)
  if [ $nbFiles = 0 ]; then
    echo "Pas de fichier source"
  else
    dir_src
  fi

  #if [ $all = 1 ]; then
#    create_pri
  #fi
}

copy_projectkde3()
{
  if [ $all = 1 ]; then
    srcpath=$i
    echo "RECUPÉRATION DES FICHIERS DU PROJET KDE3:" $srcpath
    cp $srcpath/ChangeLog ./ChangeLog
    cp -r $srcpath/src ./src

    echo "CHANGEMENT D'EXTENSION DES FICHIERS INUTILES"
    mv ./src/acquisition/acquisitionsettingsdlg_APM.ui ./src/acquisition/acquisitionsettingsdlg_APM.qt3ui
    mv ./src/acquisition/acquisitionsettingsdlg_APM+ACO.ui ./src/acquisition/acquisitionsettingsdlg_APM+ACO.qt3ui

    echo "SUPPRESSION DES FICHIERS RÉCUPÉRÉS INUTILES"
    rm ./src/Makefile* ./src/*.o ./src/*.obj ./src/*~ ./src/*.files ./src/*.config ./src/*.creator ./src/*.user ./src/*.includes ./src/*.autosave
    rm ./src/*/Makefile* ./src/*/*.o ./src/*/*.obj ./src/*/*~ ./src/*/*.files ./src/*/*.config ./src/*/*.creator ./src/*/*.user ./src/*/*.includes ./src/*/*.autosave
    rm ./src/benchsettings/taactrltwdg.cpp
    rm ./src/benchsettings/taactrltwdg.h
    rm ./src/benchsettings/pfbsewdg.h

    echo "RECODAGE EN UTF8 LES FICHIERS RÉCUPÉRÉS"
    recode ISO-8859-15..UTF8 ./ChangeLog ./src/*.* ./src/*/*.*
  fi
}

update_projectkde3()
{
  echo "ADAPTATIONS DES FICHIERS SOURCES KDE3 EN QT5 AVEC LES ADAPTATIONS CYLIBS"
  DEPENDPATH="DEPENDPATH +="
  INCLUDEPATH="INCLUDEPATH +="
  dir=$dstpath
  src=""
  change_dir
}

create_projectqt5()
{
  if [ $all = 1 ]; then
    cd $dstpath

    echo "CRÉATION DU FICHIER PROJET"
    cp cyqt5.pro $dstpath.pro

    file=$dstpath.pro
    sed -i -e "s/cyqt5/$dstpath/g" $file

    echo "CRÉATION DU FICHIER SOUS-PROJET SRC"
    file=./src/src.pro
    cp cyqt5src.pro $file
    sed -i -e "s/cyqt5/$dstpath/g" $file
    echo -e $INCLUDEPRI"\r\n\r\n" >> $file
    echo -e $DEPENDPATH"\r\n\r\n" >> $file
    echo -e $INCLUDEPATH"\r\n\r\n" >> $file
  fi
}


echo "Mise à jour du répertoire" $updatedir
cd $updatedir
dir=$(basename "$PWD")
echo $dir
change_dir

#if [ $update = 0 ]; then
#  backup
#  clean_project
#  copy_projectkde3
#  update_projectkde3
#  create_projectqt5
#else
#  if [ $updatedir = 0 ]; then
#    if [ $updatefile = 0 ]; then
#      backup
#      update_projectkde3
#    else
#      echo "Mise à jour du fichier" $updatefile
#      if [ "${updatefile##*.}" = "ui" ]; then
#        update_file_ui
#      else
#        update_file
#      fi
#    fi
#  else
#    echo "Mise à jour du répertoire" $updatedir
#    cd $updatedir
#    dir=$(basename "$PWD")
#    echo $dir
#    input_dir
#  fi
#fi

exit $res

