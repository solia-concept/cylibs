#!/bin/sh
#
# Script de fin de désinstallation de Cylibs
# Auteur: Le Cléach G.
#

CYDIR=$1
QTDIR=$2

if [ "$CYDIR" = "" ] ; then
  echo "ERREUR: MANQUE LE CHEMIN DU RÉPERTOIRE DE CYLIBS"
  exit $?
fi

if [ "$QTDIR" = "" ] ; then
  echo "ERREUR: MANQUE LE CHEMIN DU RÉPERTOIRE DE QT"
  exit $?
fi

echo "SUPPRESSIONS DES PLUGINS QT DESIGNER DE CYLIBS"
rm -fv $QTDIR/plugins/designer/libcy_designer_plugin.so
rm -fv $QTDIR/../../Tools/QtCreator/lib/Qt/plugins/designer/libcy_designer_plugin.so

#--------------------------------------------------------------------------
# FIN FICHIER
