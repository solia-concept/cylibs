#!/bin/sh
#
# Script de fin d'installation de Cylibs
# Auteur: Le Cléach G.
#

CYDIR=$1
QTDIR=$2

ln -fsv $CYDIR/lib/libcy.so.5 /usr/lib
ln -fsv $CYDIR/plugins/designer/libcy_designer_plugin.so $QTDIR/plugins/designer
ln -fsv $CYDIR/plugins/designer/libcy_designer_plugin.so $QTDIR/../../Tools/QtCreator/lib/Qt/plugins/designer

#--------------------------------------------------------------------------
# FIN FICHIER
