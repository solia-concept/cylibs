#!/bin/sh
#
# Script d'installation du module de communication Cylix Ethernet Client
# Auteur: Le Cléach G.
#

fich="/etc/rc.local"
if [ -e $fich ]; then
  echo "ancien système de démarrage automatique d'application"
  if [ "! grep -q 'CRNCY_Clt' $fich" ]; then
  {
    echo "installation du module au démarrage du PC"
    if [ "grep -q 'exit 0' $fich" ]; then
    {
      echo "config Ubuntu"
      sed -i "s/exit 0/CRNCY_Clt/g" $fich
      # annule le remplacement dans l'en-tête
      sed -i "s/\"CRNCY_Clt/\"exit 0/g" $fich
      # annule la suppression à la fin du fichier de "exit 0"
      echo "exit 0" >> $fich
      echo "$fich: add CRNCY_Clt"
    }
    fi
  }
  fi
else
{
  echo "systemd: nouveau système de démarrage automatique d'application"
  fich="/etc/cyethernet.local"
  if [ ! -e $fich ]; then
  {
    echo "#!/bin/sh" >> $fich
    echo "CRNCY_Clt &" >> $fich
    chmod a+x $fich
    systemctl enable cyethernet.service
    systemctl start cyethernet.service
#    systemctl status cyethernet.service
    STATUS="$(systemctl is-active cyethernet.service)"
    if [ "${STATUS}" = "active" ]; then
      echo "SERVICE CYETHERNET OK"
    fi
    }
  fi
}
fi

#--------------------------------------------------------------------------
# FIN FICHIER
