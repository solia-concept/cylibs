#-------------------------------------------------------------------
# Fichier d'inclusion des fichiers Linux de Cylibs pour QtCreator
# Copyright (C) 2016-2017   Gérald Le Cléach
#
# Cylix is free software; you can redistribute it and/or
# modify it under the terms of the LGPL License, Version 3.0
#
#-----------------------------------------------------------------
TEMPLATE = aux

include ( $${PWD}/../cyconfig.pri )

DISTFILES = \
post_install.sh \
post_uninstall.sh \
CRNCY_Clt \
cylowercase \
cypo2csv \
cyuic3 \
cyuic-qt3 \
cyr422m_uninstall \
cyr422m_start \
cyr422m_install \
cyethernet_install.sh \
cyethernet_uninstall.sh \
cyethernet.service \
cyxfce4shortcuts \
xfce4-keyboard-shortcuts.xml \
cykde3toqt5.sh

linux.path   = $${CY_INSTALL_LINUX}
linux.uninstall = $${CY_INSTALL_LINUX}/post_uninstall.sh $$CY_INSTALL_PREFIX $$[QT_INSTALL_DATA]
linux.files = $$DISTFILES
INSTALLS += linux

