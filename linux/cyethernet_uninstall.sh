#!/bin/sh
#
# Script de désinstallation du module de communication Cylix Ethernet Client
# Auteur: Le Cléach G.
#

STATUS="$(systemctl is-active cyethernet.service)"
if [ "${STATUS}" = "active" ]; then
  systemctl stop cyethernet.service
  systemctl disable cyethernet.service
fi
rm -fv "/etc/systemd/system/cyethernet.service"
rm -fv "/etc/cyethernet.local"

#--------------------------------------------------------------------------
# FIN FICHIER
