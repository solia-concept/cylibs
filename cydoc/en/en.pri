include ( ./pics/pics.pri )

lang=en

DISTFILES = \
    $$PWD/analyse.xml \
    $$PWD/annexe_events.xml \
    $$PWD/annexe.xml \
    $$PWD/credits.xml \
    $$PWD/curves.xml \
    $$PWD/cydoc.xml \
    $$PWD/cydoc_oper.xml \
    $$PWD/display.xml \
    $$PWD/doc_book.xml \
    $$PWD/doc_book_oper.xml \
    $$PWD/events.xml \
    $$PWD/install_xubuntu_20-04.xml \
    $$PWD/maintenance.xml \
    $$PWD/mainwin.xml \
    $$PWD/calibrage.xml \
    $$PWD/metrology.xml \
    $$PWD/panel.xml \
    $$PWD/plasma.xml \
    $$PWD/project.xml \
    $$PWD/releasenotes.xml \
    $$PWD/rn_linux_setting.xml \
    $$PWD/user.xml \
    $$PWD/install_xubuntu.xml \
    $$PWD/install_cylix.xml \
    $$PWD/rn_dos.xml \
    $$PWD/rn_linux.xml \
    $$PWD/rn_beckhoff.xml \
    $$PWD/systemback.xml \
    $$PWD/rn_beckhoff_more.xml \
    $$PWD/acquisition.xml \
    $$PWD/menu_aide.xml \
    $$PWD/pid_setting.xml \
    $$PWD/cyreleaseinfo.xml

doc_$${lang}.files = $$DISTFILES
doc_$${lang}.path  = $${CY_INSTALL_CYDOC}/$${lang}
unix{
INSTALLS += doc_$${lang}
}
