<chapter id="display">
  <title
>Graphic displays</title>
  <para
>The graphic displays are graphic objects which allows you to display one or several data in different ways. Each of them can be used in <link linkend="analyse-win"
> the graphic analyse tool </link
> or at various spaces of the interface created during Cylix development.In the first case, it is possible, for the user, to add or to replace one data, according to the kind of display, by making a slide and put action from the data explorer. It is also possible to do the same action thanks to the configuration box of the display which is accessible from its contextual menu (right pressing).It is, thus, possible to change the data of a display when that one is not used in <link linkend="analyse-win"
> the graphic analyse tool</link
>. </para>
  <itemizedlist>
  <title
>List of displays</title>
    <listitem
><link linkend="display_simple"
>Simple display</link
></listitem>
    <listitem
><link linkend="display_multimeter"
>Multimeter</link
></listitem>
    <listitem
><link linkend="display_scope"
>Classic oscilloscope</link
></listitem>
    <listitem
><link linkend="display_cyscopemultiple"
>Oscilloscope multiple formats</link
></listitem>
    <listitem
><link linkend="display_bargraph"
>Bargraph</link
></listitem>
    <listitem
><link linkend="display_datastable"
>Data table</link
></listitem>
  </itemizedlist>
  <sect1 id="display_popupmenu">
    <title
>Contextual menus</title>
    <para
>The contextual menu of the display, accessible by a right pressing, allows to, according the case : </para>
    <sect2 id="display_popupmenu_1">
      <title
>Out of <link linkend="analyse-sheet"
>analyse sheet</link
></title>
      <itemizedlist>
        <listitem
><para
>configuration of the display.</para
></listitem>
        <listitem
><para
>modification of the refresh time.</para
></listitem>
        <listitem
><para
>refresh time interruption.</para
></listitem>
      </itemizedlist>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_popupmenu_1.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
    </sect2>
    <sect2 id="display_popupmenu_2">
      <title
>Dans <link linkend="analyse-sheet"
>analyse sheet</link
></title>
      <itemizedlist>
        <listitem
><para
>configuration of the display.</para
></listitem>
        <listitem
><para
>deletion of the display.</para
></listitem>
        <listitem
><para
>addition of a <link linkend="analyse-cell"
>cell</link
> empty at right.</para
></listitem>
        <listitem
><para
>addition of a <link linkend="analyse-cell"
>cell</link
> empty below.</para
></listitem>
        <listitem
><para
>removal of the <link linkend="analyse-cell"
>cell</link
> in which it is contained.</para
></listitem>
        <listitem
><para
>configuration of the <link linkend="analyse-sheet"
>analyse sheet</link
> in which it is contained.</para
></listitem>
      </itemizedlist>
      <note>
        <para
>The modification of the refresh time is made by the configuration of the <link linkend="analyse-sheet"
>analyse sheet</link
>.</para>
      </note>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_popupmenu_2.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
      <note>
        <para
>The break operation of the refreshing of all graphic displays can be done by entering "Pause" of menu "File " in the graphical analysis window.</para>
      </note>
    </sect2>
  </sect1>
  <?hard-pagebreak?>
  <sect1 id="display_simple">
  <title
>Simple display</title>
  <para
>The simple display allows you to display all kind of data. It is thus possible to display alpha numerical values with a text displaying and boolean values with a LED. </para>
  <mediaobject>
  <imageobject
><imagedata fileref="pics/display_simple.png" format="PNG" scale="50" align="center"/></imageobject>
  </mediaobject>
  <sect2 id="display_simple_popupmenu">
    <title
>Contextual menu</title>
    <para
>The contextual menu includes the main functions of a <link linkend="display_popupmenu"
>graphic display </link
>. </para>
  </sect2>
  <sect2 id="display_simple_settings">
    <title
>Configuration</title>
    <sect3 id="display_simple_settings_general">
      <title
>General</title>
      <para
>In order to modify the title in automatic mode, you have to change the label of the displayed data in the tab "Data". </para>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_simple_settings_general.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
    </sect3>
      <?hard-pagebreak?>
    <sect3 id="display_simple_settings_style">
      <title
>Style</title>
      <para
>The modification of the size of the character allows you to display the value of the data in a rather big way. </para>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_simple_settings_style_text.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
      <para
>It is possible to choose the shape and the look of the LED, but it is also possible to choose its colour(s) and to decide if it will be or not a two-coloured LED. </para>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_simple_settings_style_bool.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
    </sect3>
      <?hard-pagebreak?>
    <sect3 id="display_simple_settings_data">
      <title
>Data</title>
      <para
>In the tab "Data", it is possible to put the data to be displayed from a data explorer and to modify its label. </para>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_simple_settings_data.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
    </sect3>
  </sect2>
</sect1>
  <?hard-pagebreak?>
  <sect1 id="display_multimeter">
  <title
>Multimeter</title>
  <para
>The multimeter displays the values under the form of a numerical (LCD) and analogical multimeter (Galvanometer). Another automatic calculation allows this kind of display to obtain an optimum size rapidly. Thus, it is possible to make a display which is visible from far away. In addition to this, that display has a colour management of alarm compared with thresholds to be checked. </para>
  <mediaobject>
  <imageobject
><imagedata fileref="pics/display_multimeter.png" format="PNG" scale="50" align="center"/></imageobject>
  </mediaobject>
  <sect2 id="display_multimeter_popupmenu">
    <title
>Contextual menu</title>
    <para
>The contextual menu includes the main functions of a <link linkend="display_popupmenu"
>graphic display </link
>. </para>
  </sect2>
  <sect2 id="display_multimeter_settings">
    <title
>Configuration</title>
    <sect3 id="display_multimeter_settings_general">
      <title
>General</title>
      <para
>In order to modify the title in automatic mode, you have to change the label of the displayed data in the tab "Data". </para>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_multimeter_settings_general.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
    </sect3>
    <sect3 id="display_multimeter_settings_style">
      <title
>Style</title>
      <para
>It is possible to activate one or the other kind of display, you will refer to both. In that case, the LCD display is positioned below the analog display.The modification of the size of characters allows you to display the value of the data in a high or small way in the analog display. </para>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_multimeter_settings_style.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
    </sect3>
      <?hard-pagebreak?>
    <sect3 id="display_multimeter_settings_data">
      <title
>Data</title>
      <para
>In the tab "Data", it is possible to put the data to be displayed from a data explorer and to modify its label. </para>
      <para
>The analog display offers a variable scale in order to display in a best way the variations of the data. It is also possible to modify its values by a mere right press on this display. </para>
      <para
>The minimum and maximum thresholds allows you to check the displayed value. If you go over those thresholds, the normal colour defined in the tab "Style" is replaced by the colour of alarm. </para>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_multimeter_settings_data.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_multimeter_alarm.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
    </sect3>
  </sect2>
</sect1>
  <?hard-pagebreak?>
  <sect1 id="display_scope">
    <title
>Oscilloscope</title>
    <para
>The oscilloscope displays the levels of one or several data on the duration compared with the time or compared with another data (XY). Those real time curves are registered before and during the displaying. When several values are displayed in the same time, the levels are displayed in different colors. </para>
    <mediaobject>
    <imageobject
><imagedata fileref="pics/display_scope_time.png" format="PNG" scale="50" align="center"/></imageobject>
    </mediaobject>
    <mediaobject>
    <imageobject
><imagedata fileref="pics/display_scope_xy.png" format="PNG" scale="50" align="center"/></imageobject>
    </mediaobject>
    <sect2 id="display_scope_popupmenu">
      <title
>Contextual menu</title>
      <para>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_scope_popupmenu.png" format="PNG" scale="50" align="center"/></imageobject
></mediaobject
> The contextual menu has the same main functions <link linkend="display_popupmenu"
>graphic display</link
> to which the following functions are added : </para>
      <informaltable
><tgroup cols='2' align='left' colsep='1' rowsep='1'
><colspec colnum="1" colname="col1" colwidth="1*"/>
        <colspec colnum="2" colname="col2" colwidth="2*"/><tbody>
        <row
><entry
>Curve analysis</entry>
        <entry
><link linkend="curves"
>curves' analysis</link
> thanks to cursors which are also accessible by double press on the oscilloscope.</entry>
        </row>
        <row
><entry
>Reset</entry
><entry
>Only in use XY mode: Clears the old values of the curves by resetting their buffers.</entry
></row>
        <row
><entry
>Printing curves</entry
><entry
>Curve printing via the operating system.</entry
></row>
        <row
><entry
><guiicon
><inlinegraphic fileref="pics/cyapplication-pdf.png" width="16"/></guiicon
>Export PDF</entry
><entry
>Printing in PDF.</entry
></row>
        <row
><entry
><guiicon
><inlinegraphic fileref="pics/cytext-csv.png" width="16"/></guiicon
>Export CSV</entry
><entry
>Exports the values of the curves in a CSV file for eg a exploitation in a spreadsheet of type LibreOffice or Excel.</entry
></row>
        </tbody
></tgroup
></informaltable>
    </sect2>
    <sect2 id="display_scope_settings">
      <title
>Configuration</title>

      <sect3 id="display_scope_settings_general">
        <title
>General</title>
        <para
>In the tab "General" it is possible to modify the title and to choose the temporal using mode or XY. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_scope_settings_general.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>
      </sect3>
    <?hard-pagebreak?>
      <sect3 id="display_scope_settings_style">
        <title
>Style</title>
        <para
>Several parameters are used to modify the displaying' style. You can choose the position of the legends around the oscilloscope or simply hide them. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_scope_settings_style.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>
      </sect3>
    <?hard-pagebreak?>
      <sect3 id="display_scope_settings_data_x">
        <title
>X data</title>
        <para
>The data in in abscissa in temporal mode is automatically a time. In, that view, it is also possible to adjust <link linkend="display_scope_scale"
>scale</link
>. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_scope_settings_data_x1.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>

        <para
>The addition of one data in abscissa from the data explorer is only possible in XY mode. In that view, it is also possible to adjust <link linkend="display_scope_scale"
>scale</link
>. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_scope_settings_data_x2.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>
      </sect3>
        <?hard-pagebreak?>
      <sect3 id="display_scope_settings_data_y">
        <title
>Datas Y</title>
        <para
>You can add or suppress data in ordinate from an included data explorer, and you can also modify the colours of their curve. In that view, it is also possible to adjust <link linkend="display_scope_scale"
>the scale</link
>. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_scope_settings_data_y.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>
        <mediaobject>
<!--           <imageobject
><imagedata fileref="pics/display_scope_info.png" format="PNG" scale="50" align="center"/></imageobject
> -->
          <caption
><caution
><para
>Data relative to an axis must be of the same nature; that is to say that they must have the same unit (Bar, l / h ...) and be part of the same type of connection (fast, slow ...). You can however use a <link linkend = "display_cyscopemultiple"
>oscilloscope multiple formats</link
> if you want to put data of different natures.</para
></caution
></caption>
        </mediaobject>
      </sect3>
    </sect2>
    <sect2 id="display_scope_scale">
      <title
>Scales</title>
      <para
>To the nearest decimal point, the minimum and maximum values of the scales are automatically fixed according to the data to be displayed. In order to adjust them, you only have to press with the left button of the mouse on a scale axis or to use the configuration box. </para>
    </sect2>
  </sect1>
  <?hard-pagebreak?>
  <sect1 id="display_cyscopemultiple">
    <title
>Oscilloscope multiple formats</title>
    <para
>Unlike <link linkend="display_scope"
>the classical oscilloscope</link
>, the oscilloscope multiple formats allows many types of curves in the same oscilloscope. To do this, the Y-Left axis has no unit in order to put data of different natures. The Y-right axis is not active in this oscilloscope.</para>
        <mediaobject>
    <imageobject
><imagedata fileref="pics/display_cyscopemultiple.png" format="PNG" scale="50" align="center"/></imageobject>
    </mediaobject>
    <para
>The legend displays into [ ] the unit for each of the curves. In the illustration, the legend of the last curve ends with [x0.02] which means that the curve is displayed with a coefficient of 0.02. This allows you to enlarge or decrease the display of certain curves relative to others in order to optimize the view.</para>
        <mediaobject>
    <imageobject
><imagedata fileref="pics/display_cyscopemultiple_settings_data_y.png" format="PNG" scale="50" align="center"/></imageobject>
    </mediaobject>
	</sect1>
  <?hard-pagebreak?>
  <sect1 id="display_bargraph">
    <title
>Bargraph</title>
    <para
>The bargraph displays the values of data in histogram form. That one can manage several data of different formats and connections. In addition to this, this display offers a color management of alarm compared with thresholds to be overseen. </para>
    <mediaobject>
    <imageobject
><imagedata fileref="pics/display_bargraph.png" format="PNG" scale="50" align="center"/></imageobject>
    </mediaobject>

    <sect2 id="display_bargraph_popupmenu">
      <title
>Contextual menu</title>
      <para
>The contextual menu includes the main functions of a <link linkend="display_popupmenu"
>graphic display </link
>. </para>
    </sect2>

    <sect2 id="display_bargraph_settings">
      <title
>Configuration</title>

      <sect3 id="display_bargraph_settings_general">
        <title
>General</title>
        <para
>In the tab "General" it is possible to modify its title. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_bargraph_settings_general.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>
      </sect3>

      <sect3 id="display_bargraph_settings_style">
        <title
>Style</title>
        <para
>The modification of the size of the characters allows you to display the value of the data in a rather big way in the analogical display. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_bargraph_settings_style.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>
      </sect3>
  <?hard-pagebreak?>
      <sect3 id="display_bargraph_settings_data">
        <title
>Datas</title>
        <para
>In the tab "Data", it is possible to add or suppress data from an integrated data explorer.  It is also possible to modify the label of the histogram. </para>
        <para
>The displaying range is adjusted by values without unit because it is valid for all data to be displayed which can be of different formats. </para>
        <para
>The minimum and maximum thresholds allows you to have a look on displayed values. If those thresholds are exceeded, the usual colour which is defined in the tab "Style" is replaced by the alarm's colour in the histogram which is concerned. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_bargraph_settings_data.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_bargraph_alarm.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
      </sect3>

    </sect2>

  </sect1>
  <?hard-pagebreak?>
  <sect1 id="display_datastable">
    <title
>Data table</title>
    <para
>The table of data is a table which is used to display rapidly several details of several data. It is thus, possible to obtain a synthetic view of all a group of data with a drag and drop operation of that group from the data explorer. It is also possible with drag and drop operation to add data one by one.Each line is for one data and each column is for one kind of information like the value, the label .... </para>
    <para
>Furthermore, it offers the possibility to add blank columns which contain for each line an empty cell which has the possibility to receive a <link linkend="display"
>graphic display</link
> in the same way as in a <link linkend="analyse-sheet"
>analyse sheet</link
>. </para>
    <para
>Finally, it is possible to add a table of n rows, a columnof <link linkend="display_simple"
>simple displays</link
> of n additional data by drag and drop the group of these n data. </para>
    <mediaobject>
    <imageobject
><imagedata fileref="pics/display_datastable.png" format="PNG" scale="50" align="center"/></imageobject>
    </mediaobject>
    <para
>For each data, several parts of information are classified in following columns : </para>
    <itemizedlist>
      <listitem
><para
>Value displayed thanks to a <link linkend="display_simple"
>simple display</link
>.</para
></listitem>
      <listitem
><para
>Label.</para
></listitem>
      <listitem
><para
>Data group.</para
></listitem>
      <listitem
><para
>Host generating the value.</para
></listitem>
      <listitem
><para
>Connection network by which the host exchanges the value.</para
></listitem>
      <listitem
><para
>Unique name which is used as a key in the Cylix data basis.</para
></listitem>
      <listitem
><para
>Help which is informed for several data.</para
></listitem>
      <listitem
><para
>Physical indicator of the machine only for electrical inputs and outputs.</para
></listitem>
      <listitem
><para
>Electrical indicator only for electrical inputs and outputs.</para
></listitem>
      <listitem
><para
>Temporal value which exists for some data like the maintenance counters.</para
></listitem>
    </itemizedlist>
    <para
>Some of the data tables which are used elsewhere than in <link linkend="analyse-win"
>the tool of graphic analysis</link
>, for instance in the forcing window or in the window of maintenance counters, offer other columns which give you useful keyboarding means for some data. </para>
    <itemizedlist>
      <listitem
><para
>Keyboarding of forcing value of outputs.</para
></listitem>
      <listitem
><para
>Keyboarding of a note combined with some data such as the maintenance counters.</para
></listitem>
      <listitem
><para
>Reinitialisation button of temporal value.</para
></listitem>
    </itemizedlist>

    <sect2 id="display_datastable_popupmenu">

      <title
>Contextual menu</title>
      <para
>The contextual menu has the same main functions <link linkend="display_popupmenu"
>graphic display</link
> to which the following functions are added : </para>
      <itemizedlist>
        <listitem
><para
>To hide the column. Hide the column which has the focus.</para
></listitem>
        <listitem
><para
>To display the column. To select in the list of available columns the one which has to be added.</para
></listitem>
        <listitem
><para
>To add a column. A column is added, it contains an empty cell which can include a <link linkend="display"
>graphic display</link
> in the same manner as in a <link linkend="analyse-sheet"
>analyse sheet</link
></para
></listitem>
        <listitem
><para
>To change the label of the column which has the focus.</para
></listitem>
        <listitem
><para
>To change the width of the column which has the focus.</para
></listitem>
        <listitem
><para
>Remove the selected row(s).</para
></listitem>
        <listitem
><para
>To change the height of the line which has the focus.</para
></listitem>
      </itemizedlist>
      <mediaobject>
        <imageobject
><imagedata fileref="pics/display_datastable_popupmenu.png" format="PNG" scale="50" align="center"/></imageobject>
      </mediaobject>
    </sect2>

    <sect2 id="display_datastable_settings">
      <title
>Configuration</title>

      <sect3 id="display_datastable_settings_general">
        <title
>General</title>
        <para
>In the "General" tab, it is possible to modify the title. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_datastable_settings_general.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>
      </sect3>

      <sect3 id="display_datastable_settings_data">
        <title
>Datas</title>
        <para
>In the "Data" tab, it is possible to add or suppress data from an integrated data explorer. It is also possible to modify the label of each data. </para>
        <mediaobject>
          <imageobject
><imagedata fileref="pics/display_datastable_settings_datas.png" format="PNG" scale="50" align="center"/></imageobject>
        </mediaobject>
      </sect3>

    </sect2>

  </sect1>

</chapter>
