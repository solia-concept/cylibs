
DISTFILES = \
    $$PWD/docbook.xsl \
    $$PWD/cy-chunk.xsl \
    $$PWD/cydocbook.xsl \
    $$PWD/cydochtml.xsl \
    $$PWD/cydocpdf.xsl \
    $$PWD/cy-nochunk.xsl

xls.files = $$DISTFILES
xls.path  = $${CY_INSTALL_CYDOC}/xls
unix{
INSTALLS += xls
}
