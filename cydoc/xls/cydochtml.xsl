<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                exclude-result-prefixes="exsl"
                version='1.0'>

<xsl:import href="/usr/share/xml/docbook/stylesheet/docbook-xsl/html/docbook.xsl"/>

<xsl:param name="paper.type" select="'A4'"/>

<xsl:template match="imagedata[not(@scalefit)]">
  <fo:block text-align="center" display-align="center">
    <fo:external-graphic content-width="60%" src="url({@fileref})" />
  </fo:block>
</xsl:template>

</xsl:stylesheet>
