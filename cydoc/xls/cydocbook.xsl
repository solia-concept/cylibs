<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:fo="http://www.w3.org/1999/XSL/Format"
version="1.0">

<xsl:import href="/usr/share/xml/docbook/stylesheet/nwalsh/fo/docbook.xsl"/>

<xsl:param name="paper.type" select="'A4'"/>

<xsl:template match="imagedata[not(@scalefit)]">
  <fo:block text-align="center" display-align="center">
    <fo:external-graphic content-width="60%" src="url({@fileref})" />
  </fo:block>
</xsl:template>

<!--CY Profondeur du sommaire -->
<!--<xsl:param name="toc.section.depth">0</xsl:param>
<xsl:param name="section.autolabel" select="1"></xsl:param>-->

<!--Couleur des para des warnings en rouge-->
<!--<xsl:template match="warning">
  <xsl:call-template name="generic-note">
  <xsl:with-param name="label"></xsl:with-param>
  </xsl:call-template>
  <font color="red">
    <xsl:apply-templates/>
  </font>
</xsl:template>-->

<!--<xsl:template match="warning/para">
  <font color="red">
    <xsl:apply-templates/>
  </font>
</xsl:template>
-->
</xsl:stylesheet>
