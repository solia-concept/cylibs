<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:ng="http://docbook.org/docbook-ng"
                xmlns:db="http://docbook.org/ns/docbook"
                exclude-result-prefixes="db ng exsl"
                version='1.0'>

<xsl:import href="/usr/share/xml/docbook/stylesheet/docbook-xsl/fo/profile-docbook.xsl"/>
<xsl:param name="insert.olink.pdf.frag" select="1"/>
<xsl:param name="insert.xref.page.number" select="1"/>
<xsl:param name="fop1.extensions" select="1"></xsl:param>

<xsl:template match="processing-instruction('hard-pagebreak')">
   <fo:block break-after='page'/>
</xsl:template>

<xsl:template match="processing-instruction('linebreak')">
  <fo:block/>
</xsl:template>

<!-- CY Centrer images -->
<xsl:attribute-set name="imagedata.properties">
  <xsl:attribute name="align">center</xsl:attribute>
</xsl:attribute-set>
<!-- CY -->

<!--CY Profondeur du sommaire -->
<xsl:param name="toc.section.depth">3</xsl:param>
<xsl:param name="section.autolabel" select="1"></xsl:param>

<!--Mise en forme para des warnings en rouge -->
<xsl:template match="warning/para">
  <xsl:variable name="id">
    <xsl:call-template name="object.id"/>
  </xsl:variable>

  <fo:block
            border-top="0.5pt solid black"
            border-bottom="0.5pt solid black"
            padding-top="4pt"
            padding-bottom="4pt"
            id="{$id}">
    <xsl:if test="$admon.textlabel != 0 or title">
      <fo:block keep-with-next='always'
                xsl:use-attribute-sets="admonition.title.properties">
         <xsl:apply-templates select="." mode="object.title.markup"/>
      </fo:block>

      <fo:block color="red">
        <xsl:apply-templates/>
      </fo:block>

  </xsl:if>


<!--    <fo:block xsl:use-attribute-sets="admonition.properties">
      <xsl:apply-templates/>
    </fo:block>-->
  </fo:block>
</xsl:template>

<xsl:template match="link">
    <fo:basic-link internal-destination="{@linkend}"
            xsl:use-attribute-sets="xref.properties"
            text-decoration="underline"
            color="blue">
        <xsl:choose>
            <xsl:when test="count(child::node())=0">
                <xsl:value-of select="@linkend"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </fo:basic-link>
</xsl:template>


<xsl:template match="emphasis[@role='admin_color']">
  <fo:inline color="magenta">
    <xsl:apply-templates/>
  </fo:inline>
</xsl:template>


<xsl:template match="emphasis[@role='bold']">
  <xsl:call-template name="inline.boldseq"/>
</xsl:template>

<!-- Profile elements based on input parameters -->
<xsl:template match="*" mode="profile">

  <xsl:variable name="arch.content">
    <xsl:if test="@arch">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.arch"/>
        <xsl:with-param name="b" select="@arch"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="arch.ok" select="not(@arch) or not($profile.arch) or
                                       $arch.content != '' or @arch = ''"/>

  <xsl:variable name="audience.content">
    <xsl:if test="@audience">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.audience"/>
        <xsl:with-param name="b" select="@audience"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="audience.ok"
                        select="not(@audience) or not($profile.audience) or
                                $audience.content != '' or @audience = ''"/>

  <xsl:variable name="condition.content">
    <xsl:if test="@condition">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.condition"/>
        <xsl:with-param name="b" select="@condition"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="condition.ok" select="not(@condition) or not($profile.condition) or
                                            $condition.content != '' or @condition = ''"/>

  <xsl:variable name="conformance.content">
    <xsl:if test="@conformance">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.conformance"/>
        <xsl:with-param name="b" select="@conformance"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="conformance.ok" select="not(@conformance) or not($profile.conformance) or
                                              $conformance.content != '' or @conformance = ''"/>

  <xsl:variable name="lang.content">
    <xsl:if test="@lang | @xml:lang">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.lang"/>
        <xsl:with-param name="b" select="(@lang | @xml:lang)[1]"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="lang.ok" select="not(@lang | @xml:lang) or not($profile.lang) or
                                       $lang.content != '' or @lang = '' or @xml:lang = ''"/>

  <xsl:variable name="os.content">
    <xsl:if test="@os">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.os"/>
        <xsl:with-param name="b" select="@os"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="os.ok" select="not(@os) or not($profile.os) or
                                     $os.content != '' or @os = ''"/>

  <xsl:variable name="outputformat.content">
    <xsl:if test="@outputformat">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.outputformat"/>
        <xsl:with-param name="b" select="@outputformat"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="outputformat.ok" select="not(@outputformat) or not($profile.outputformat) or
                                     $outputformat.content != '' or @outputformat = ''"/>

  <xsl:variable name="revision.content">
    <xsl:if test="@revision">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.revision"/>
        <xsl:with-param name="b" select="@revision"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="revision.ok" select="not(@revision) or not($profile.revision) or
                                           $revision.content != '' or @revision = ''"/>

  <xsl:variable name="revisionflag.content">
    <xsl:if test="@revisionflag">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.revisionflag"/>
        <xsl:with-param name="b" select="@revisionflag"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="revisionflag.ok" select="not(@revisionflag) or not($profile.revisionflag) or
                                               $revisionflag.content != '' or @revisionflag = ''"/>

  <xsl:variable name="role.content">
    <xsl:if test="@role">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.role"/>
        <xsl:with-param name="b" select="@role"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="role.ok" select="not(@role) or not($profile.role) or
                                       $role.content != '' or @role = ''"/>

  <xsl:variable name="security.content">
    <xsl:if test="@security">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.security"/>
        <xsl:with-param name="b" select="@security"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="security.ok" select="not(@security) or not($profile.security) or
                                           $security.content != '' or @security = ''"/>

  <xsl:variable name="status.content">
    <xsl:if test="@status">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.status"/>
        <xsl:with-param name="b" select="@status"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="status.ok" select="not(@status) or not($profile.status) or
                                           $status.content != '' or @status = ''"/>

  <xsl:variable name="userlevel.content">
    <xsl:if test="@userlevel">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.userlevel"/>
        <xsl:with-param name="b" select="@userlevel"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="userlevel.ok" select="not(@userlevel) or not($profile.userlevel) or
                                            $userlevel.content != '' or @userlevel = ''"/>

  <xsl:variable name="vendor.content">
    <xsl:if test="@vendor">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.vendor"/>
        <xsl:with-param name="b" select="@vendor"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="vendor.ok" select="not(@vendor) or not($profile.vendor) or
                                         $vendor.content != '' or @vendor = ''"/>

  <xsl:variable name="wordsize.content">
    <xsl:if test="@wordsize">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.wordsize"/>
        <xsl:with-param name="b" select="@wordsize"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="wordsize.ok"
                        select="not(@wordsize) or not($profile.wordsize) or
                                $wordsize.content != '' or @wordsize = ''"/>

  <xsl:variable name="attribute.content">
    <xsl:if test="@*[local-name()=$profile.attribute]">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.value"/>
        <xsl:with-param name="b" select="@*[local-name()=$profile.attribute]"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="attribute.ok"
                select="not(@*[local-name()=$profile.attribute]) or
                        not($profile.value) or $attribute.content != '' or
                        @*[local-name()=$profile.attribute] = '' or
                        not($profile.attribute)"/>

  <xsl:variable name="OS.content">
    <xsl:if test="@OS">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.OS"/>
        <xsl:with-param name="b" select="@OS"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="OS.ok"
                select="not(@OS) or not($profile.OS) or
                                   $OS.content != '' or @OS = ''"/>

  <xsl:variable name="RN.content">
      <xsl:if test="@RN">
        <xsl:call-template name="cross.compare">
          <xsl:with-param name="a" select="$profile.RN"/>
          <xsl:with-param name="b" select="@RN"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:variable>
    <xsl:variable name="RN.ok"
                  select="not(@RN) or not($profile.RN) or
                                     $RN.content != '' or @RN = ''"/>

<xsl:variable name="USER.content">
    <xsl:if test="@USER">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.USER"/>
        <xsl:with-param name="b" select="@USER"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="USER.ok"
                select="not(@USER) or not($profile.USER) or
                                   $USER.content != '' or @USER = ''"/>

<xsl:variable name="BENCHTYPE.content">
    <xsl:if test="@BENCHTYPE">
      <xsl:call-template name="cross.compare">
        <xsl:with-param name="a" select="$profile.BENCHTYPE"/>
        <xsl:with-param name="b" select="@BENCHTYPE"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:variable>
  <xsl:variable name="BENCHTYPE.ok"
                select="not(@BENCHTYPE) or not($profile.BENCHTYPE) or
                                   $BENCHTYPE.content != '' or @BENCHTYPE = ''"/>

  <xsl:if test="$arch.ok and
                $audience.ok and
                $condition.ok and
                $conformance.ok and
                $lang.ok and
                $os.ok and
                $outputformat.ok and
                $revision.ok and
                $revisionflag.ok and
                $role.ok and
                $security.ok and
                $status.ok and
                $userlevel.ok and
                $vendor.ok and
                $wordsize.ok and
                $attribute.ok and
                $OS.ok and
                $RN.ok and
                $USER.ok and
                $BENCHTYPE.ok">
    <xsl:copy>
      <xsl:apply-templates mode="profile" select="@*"/>

      <!-- Entity references must be replaced with filereferences for temporary tree-->
      <xsl:if test="@entityref and $profile.baseuri.fixup">
        <xsl:attribute name="fileref">
          <xsl:value-of select="unparsed-entity-uri(@entityref)"/>
        </xsl:attribute>
      </xsl:if>

      <!-- xml:base is eventually added to the root element -->
      <xsl:if test="not(../..) and $profile.baseuri.fixup">
        <xsl:call-template name="add-xml-base"/>
      </xsl:if>

      <xsl:apply-templates select="node()" mode="profile"/>
    </xsl:copy>
  </xsl:if>
</xsl:template>

<xsl:attribute-set name="list.block.spacing">
  <xsl:attribute name="space-before.optimum">0.2em</xsl:attribute>
  <xsl:attribute name="space-before.minimum">0.1em</xsl:attribute>
  <xsl:attribute name="space-before.maximum">0.3em</xsl:attribute>
  <xsl:attribute name="space-after.optimum">0.2em</xsl:attribute>
  <xsl:attribute name="space-after.minimum">0.1em</xsl:attribute>
  <xsl:attribute name="space-after.maximum">0.3em</xsl:attribute>
</xsl:attribute-set>

<!--<xsl:template name="table.layout">
  <xsl:param name="table.content"/>

  <xsl:choose>
    <xsl:when test="$xep.extensions = 0 or self::informaltable">
      <xsl:copy-of select="$table.content"/>
    </xsl:when>
    <xsl:otherwise>
      <fo:table rx:table-omit-initial-header="true">
        <fo:table-header start-indent="0pt">
          <fo:table-row>
            <fo:table-cell>
              <fo:block xsl:use-attribute-sets="formal.title.properties">
                <xsl:apply-templates select="." mode="object.title.markup"/>
                <fo:inline font-style="italic">
                  <xsl:text> (continued) </xsl:text>
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-header>
        <fo:table-body start-indent="0pt">
          <fo:table-row>
            <fo:table-cell>
              <xsl:copy-of select="$table.content"/>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:attribute-set name="table.properties">
  <xsl:attribute name="keep-together.within-column">auto</xsl:attribute>
</xsl:attribute-set>

<xsl:call-template name="table.layout"/>-->

<xsl:attribute-set name="revhistory.title.properties">
  <xsl:attribute name="font-size">12pt</xsl:attribute>
  <xsl:attribute name="font-weight">bold</xsl:attribute>
  <xsl:attribute name="text-align">center</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="revhistory.table.properties">
  <xsl:attribute name="border">0.5pt solid black</xsl:attribute>
<!--  <xsl:attribute name="background-color">#EEEEEE</xsl:attribute>-->
  <xsl:attribute name="width">50%</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="revhistory.table.cell.properties">
  <xsl:attribute name="border">0.5pt solid black</xsl:attribute>
  <xsl:attribute name="font-size">9pt</xsl:attribute>
  <xsl:attribute name="padding">4pt</xsl:attribute>
  <xsl:attribute name="text-align">left</xsl:attribute>
</xsl:attribute-set>

<xsl:template match="symbol[@role = 'symbolfont']">
  <fo:inline font-family="ZapfDingbats">
    <xsl:call-template name="inline.charseq"/>
  </fo:inline>
</xsl:template>

</xsl:stylesheet>



