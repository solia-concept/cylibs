#-------------------------------------------------------------------
# Fichier projet du manuel en ligne Cydoc pour QtCreator
# Copyright (C) 2016-2017   Gérald Le Cléach
#
# Cydoc is free documentation; you can redistribute it and/or
# modify it under the terms of the LGPL License, Version 3.0
#-------------------------------------------------------------------

TEMPLATE = aux

include ( ./fr/fr.pri )
include ( ./en/en.pri )

include ( ./bin/bin.pri )
include ( ./xls/xls.pri )
include ( ./odg/odg.pri )
include ( ./xcf/xcf.pri )
include ( ./po/po.pri )

DISTFILES = \
  README.md

cydoc.files = $$DISTFILES
cydoc.path  = $${CY_INSTALL_CYDOC}
INSTALLS += cydoc
