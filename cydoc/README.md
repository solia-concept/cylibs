# Fichier issu de Cylibs-5.21

CYDOC constitue le manuel en ligne de Cylix accessible par le menu Aide des fenêtres.

Ce manuel est généré automatiquement sous Linux au démarrage de Cylix suivant sa langue active. Ceci permet une mise à jour du texte de Cylix importé dans Cydoc. Ainsi, en annexe un tableau descriptif des évènements du Cylix est créé automatiquement à partir des decriptions et aides saisies dans le code du Cylix et éventuellement traduites. De même, les aides en bulle de toutes les données Cylix peuvent être reprises dans les fichiers XML sous forme d'ENTITY XML génréré au démarrage de Cylix dans le fichier /tmp/cylix_entity.dtd.

1- CYDOC GÉNÉRIQUE:
===================
Un Cydoc générique est un manuel en ligne le plus détaillé possible dans Cylibs mais sans explication spécifique du Cylix à part son tableau d'événements généré de manière automatique. Les différents outils et principes de Cylix y sont expliqués à partir des fichiers XML de Cylibs. Ce type de Cydoc ne nécessite donc aucun travail supplémentaire suite lors du développpement d'un Cylix. Seul une mise à jour des conditions de la génération doit être faite dans le fichier cydoc2pdf.sh du Cylix pour enlever des parties de la doc non adaptées au Cylix comme par exemple le choix de l'OS, du RN...

2- CYDOC PERSONNALISÉ:
=====================
Il est possible de modifier ou compléter un Cydoc pour l'adapter le mieux possible à un Cylix. Pour cela, il faut ajouter des fichiers XML ou des images dans les sous-répertoires du répertoire cydoc du projet Cylix suivant le même principe que le répertoire cydoc du projet Cylibs. Lors de l'installation de Cylix sur le système, les fichiers de Cylibs sont alors écrasés par ceux ayant le même nom dans Cylix. Le démarrage de Cylix génère alors Cydoc à partir de ces nouveaux fichiers. Voir "7- GÉNÉRATION DE CYDOC"

3- LIGNE DE COMMANDE:
=====================
Bien que la commande de génération utilisant le fichier cydoc2pdf.sh soit lancée automatiquement au démarrage de Cylix sous Linux, il peut être utile de l'exécuter en console pour rédiger plus vite la documentation sans faire de multiples démarrages de Cylix.
Pour cela la ligne de commande lancée au démarrage de Cylix sous Linux peut être récupérée dans les messages de sortie console de Cylix. Son exécution en console permet d'avoir des messages d'alerte ou d'erreur expliquant la non ou mauvaise génération de Cydoc.

4- IMPORTER CYDOC:
==================
Depuis le portage de Cylix sous Windows, le PDF généré au démarrage de Cylix sous Linux doit être intégré au même titre que par exemple les schémas électriques afin d'être ensuite ouvert à partir du menu Aide de Cylix.
Le fichier généré sous Linux à importer dans les sources du projet est "/tmp/cydoc/SN/fr/cydoc_fr.pdf" en version française par exemple ou SN est à remplacer par le numéro de série de Cylix. Si ce fichier n'existe pas, il faut lancer la ligne de commande en console comme expliqué dans le point précédent.

5- OUTIL NÉCESSAIRES SOUS LINUX
===============================
sudo apt-get install xsltproc fop docbook-xsl

6- METTRE A JOUR UN DOCBOOK EN ANGLAIS A PARTIR DE LA DOC EN FRANCAIS
=====================================================================
  - Extraction des messages docbook a traduire dans pot & MAJ fichier po existant:
    cydoc2po en `pwd`
  - traduire avec poedit les fichier /doc/po/*.po
    xdg-open ./po/en.po
  - copier le fichier cydoc/fr/pics/ dans le cydoc/en/pics/ et y remplacer lang=fr par lang=en
  - mettre à jour les catpures d'écran suivant la langue
  - Creation des docbook en anglais à partir du francais et du fichier de traduction correspondant
    cypo2cydoc en `pwd`
  - mettre à jour les fichiers en/cydoc.xml:
    <!ENTITY % French "INCLUDE">
    <!ENTITY language "fr">
    =>
    <!ENTITY % English "INCLUDE">
    <!ENTITY language "en">

7- GÉNÉRATION DE CYDOC
======================
  - make install
  - exécuter Cylix et le passer en mode simulation
  - changer si besoins la langue souhaitée et arrêter Cylix (pour cylibstest dans ~/.config/Cylix/cylibstest.conf)
  - copier la commande cydoc2pdf affichée en console
  - visualiser le PDF dans le répertoire relatif du Cylix et de la langue dans /tmp/cydoc/
  - modifier si besoin le texte dans "cydoc/fr/" du projet Qt
  - traduire si besoin les modifications de texte dans la lange souhaitée
  - modifier si besoin le texte dans Cylix ou d'autres fichiers de son répertoire cydoc
  - make install
  - si modification(s), générer à nouveau le PDF en collant dans la console la commande copiée.
  - visualiser le PDF dans /tmp/cydoc/ et si ok le copier dans les sources de Cylix.

8- APPEL DE CYDOC
=================
   - invokeHelp ouvre le PDF de Cylix le manuel directement à une destination spécifique dans Cydoc à partir
     d'un bouton spécifique afin de créer une aide contextuelle.
     Par exemple le bouton d'aide du mode forçage ouvre le PDF sur "cydoc_fr.pdf#maintenance-forcage"
     soit directement au chapitre expliquant le mode forçage.
