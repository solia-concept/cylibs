#!/bin/sh
# Gérald Le Cléach gerald.lecleach@solia-concept.fr
# script permettant de créer un fichier cydoc_LANG.html ou cydoc_LANG.pdf à partir du docbook de cylix.
# 'LANG' est remplacé par la langue spécifiée en option 'fr' pour le français, 'en' pour l'anglais...
# Avec l'option clean tous les fichiers *.html et *.pdf sont supprimés.
# Ce script doit être lancé en local: ./cydoc2html fr

option="$1"
path="$2"
lang="$3"
xml="$4"
xls="$5"
designer_ref="$6"
file_name="$7"

echo "cydocupdate $option $path $lang $xml $xls $designer_ref $file_name"

if [ "$file_name" = "" ] ;
  then file_name="cydoc_$lang"
fi

doc=`pwd`/$lang
if [ "$option" = "cylibs" ] || [ "$option" = "current" ] || [ "$option" = "pdf" ]; then
  # récupère le répertoire contenant la doc courante et le copie dans le répertoire temporaire pour pouvoir le modifier.
  doc=$path/doc/$lang
  echo "$doc"
  cp -R $doc /tmp/cydoc/$designer_ref
  echo "cp -R $doc /tmp/cydoc/$designer_ref"
  doc=/tmp/cydoc/$designer_ref
fi
echo "$doc"

if [ "$option" = "current" ] ; then
  # écrase le fichier annexe_events par défaut par celui de l'application généré au lancement de celle-ci
  cp -f $doc/$lang/annexe_events.docbook $doc/cylibs
  echo "cp -f $doc/$lang/annexe_events.docbook $doc/cylibs"
fi

cd "$doc/$lang"
rm -f *.html
if [ "$option" != "clean" ] ;
  then
  cp $xml $file_name.xml
  echo "cp $xml $file_name.xml"
  xsltproc $xls $file_name.xml > $file_name.html
  echo "xsltproc $xls $file_name.xml > $file_name.html"
#  notify-send "$doc/$lang/$file_name.html ok"

  xsltproc /usr/local/Cylix/cydoc-9102787/doc/cydocpdf.xsl $file_name.xml > $file_name.fo | fop -fo $file_name.fo -pdf $file_name.pdf
fi
