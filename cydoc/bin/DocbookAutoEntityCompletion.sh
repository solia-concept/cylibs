#!/bin/bash
# 2023-28-06 Benoît Le Vot benoit.levot@solia-concept.fr
#
# DocbookAutoEntityCompletion (DAEC) permet de créer un fichier temporaire dans tmp : /tmp/daec.dtd
# DAEC parcoure tous les .xml de la langue sélectionnée pour trouver toutes les entités référencées.
# DAEC écrit ensuite dans /tmp/daec.dtd toutes les entités manquantes dans /tmp/cylix_entity.dtd
# Au démarrage de cylix, si /tmp/daec.dtd existe ses entrées sont recopiées dans /tmp/cylix_entity.dtd en tant qu'entité vide.
#
# Ce script doit être utilisé comme suit : ./DocbookAutoEntityCompletion <lang> <chemin de l'affaire>
# Exemple : ./DocbookAutoEntityCompletion fr /gitkraken/cylix-1036179_BVC

#variables
lang="$1"
srcpath="$2"
daecpath="/tmp/daec.dtd"
docbookpath="/tmp/cylix_entity.dtd"
#regexp : retourne toutes les chaines contenues entre <entry>& et ;</entry>
#patternXML="&([^;#]+);"           #chaines commençant par & et finissant par ;
patternXML="&([A-Z][^;#]*);"     #idem mais 1er charactère n'est pas minuscule
#patternXML="&([^;&#[:lower:]]+);" #idem mais pas de minuscule du tout
patternDTD="<!ENTITY ([^ ]*) "

#tableau indicé : nécéssite bash => ne marche pas avec dash/POSIX (pas de tableaux)
declare -a Entities
declare -a EntitiesDTD
declare -a EntitySortTable

i=0
matched=0
tmpLine=""

##########################################################################################
#guard
echo "\n DocbookAutoEntityCompletion : Autocompleting docbook entity for cylix at $srcpath"
echo "missing entities are listed in /tmp/daec.dtd"
echo "**************************************************"

#####
if [ ! "$2" ]; then
  echo "missing or incorrect source path."
  echo "source path should point to the general folder of the project."
  echo "example use of this script : ./DocbookAutoEntityCompletion.sh fr /home/benoit/Gitkraken/cylix-1036179_BVC"
  exit
fi

#####
if [ ! -f "$docbookpath" ]; then
  echo "no docbook found at /tmp/cylix_entity.dtd"
  echo "launch application at least 1 time before executing this script"
  exit
else
  echo "found /tmp/cylix_entity.dtd"
  echo "listing missing entities..."
fi

##########################################################################################
#main

#mémorisation de toutes les entités dans les xml
for xmlfile in "$srcpath/cydoc/$lang"/*".xml"; do
  echo "Parsing $xmlfile..."
  while IFS= read -r line; do
    tmpLine="$line"
      while [[ $tmpLine =~ $patternXML ]]; do
        Entities+=("${BASH_REMATCH[1]}")
        tmpLine="${tmpLine/${BASH_REMATCH[0]}/}"
      done
  done < "$xmlfile"
  echo "done !"
done

#echo "found following entities :"
#for entity in "${Entities[@]}"; do
#    echo "$entity"
#done

echo
echo "*************************************************************************"
#mémo de toutes les entités dans cylix_entity.dtd
echo "parsing $docbookpath..."
while IFS= read -r line; do
  if [[ $line =~ $patternDTD ]]; then
      EntitiesDTD+=("${BASH_REMATCH[1]}")
  fi
done < "$docbookpath"
echo "done!"

#echo "found following entities :"
#for entity in "${EntitiesDTD[@]}"; do
#    echo "$entity"
#done

echo
echo "*************************************************************************"
#tri des entités existantes
echo "sorting entities..."
for entityXML in "${Entities[@]}"; do
  for entityDTD in "${EntitiesDTD[@]}"; do
    if [[ "$entityXML" == "$entityDTD" ]]; then
      matched=1
#      echo "matched ! $entityXML and $entityDTD"
      break
    fi
  done
  if [ "$matched" == 0 ]; then
    echo "found missing entity : $entityXML"
    EntitySortTable+=("$entityXML")
#    else
#    echo "entity $entityXML was found"
  fi
  matched=0
done
echo "done !"

#écriture dans daec.dtd
echo
echo "*************************************************************************"
if [[ ${EntitySortTable[@]} ]]; then
  echo "writing to $daecpath..."
else
  echo "No missing entity found. Exiting..."
  echo "Good Bye !"
  exit
fi

for entity in "${EntitySortTable[@]}"; do
#  echo "missing entity : $entity"
  echo "$entity" >> "$daecpath"
done
if [ ! -f "$daecpath" ]; then
  echo "error creating file $daecpath"
else
  echo "Done !"
fi
