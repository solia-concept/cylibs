#!/bin/sh
# Gérald Le Cléach gerald.lecleach@solia-concept.fr
# script permettant de créer un fichier cydoc_LANG.pdf à partir du docbook de cylix.
# 'LANG' est remplacé par la langue spécifiée en option 'fr' pour le français, 'en' pour l'anglais...
# Ce script doit être lancé en local: ./cydoc2pdf fr

option="$1"
path="$2"
xml="$3"
xls="$4"
designer_ref="$5"
lang="$6"
file_name="$7"
USER="$8"
BENCHTYPE="$9"

# CONDITIONS : génère la doc en fonction de ces conditions
vendor="SOLIA"
#vendor="CLEMESSY"
OS="LINUX"
#OS="WINDOWS"
#RN="DOS"
#RN="BECKHOFF"
RN="LINUX"

echo "cydoc2pdf $option $path $lang $xml $xls $designer_ref $file_name $BENCHTYPE"
echo "$file_name"
if [ "$file_name" = "" ] ;
  then
  file_name="cydoc_$lang"
  echo "$file_name"
fi
echo "$file_name"

doc=`pwd`/$lang
if [ "$option" = "cylibs" ] || [ "$option" = "current" ]; then
  # récupère le répertoire contenant la doc courante et le copie dans le répertoire temporaire pour pouvoir le modifier.
  doc=$path/$lang
  echo "$doc"
  mkdir -v -p /tmp/cydoc/$designer_ref
  cp -v -R $doc /tmp/cydoc/$designer_ref
  doc=/tmp/cydoc/$designer_ref
  echo "$doc"
fi
echo "$doc"

if [ "$option" = "current" ] ; then
  # écrase le fichier annexe_events par défaut par celui de l'application généré au lancement de celle-ci
  cp -v -f $doc/$lang/auto/annexe_events.xml $doc/$lang
fi

echo "$doc"
echo "$lang"
cd "$doc/$lang"
if [ "$option" != "clean" ] ;
  then
  cp -v $xml $file_name.xml
  xsltproc \
  --stringparam collect.xref.targets  "yes"  \
  --stringparam profile.vendor $vendor \
  --stringparam profile.OS $OS \
  --stringparam profile.RN $RN \
  --stringparam profile.USER $USER \
  --stringparam profile.BENCHTYPE $BENCHTYPE \
  $xls $file_name.xml > $file_name.fo
  fop -fo $file_name.fo -pdf $file_name.pdf
fi
