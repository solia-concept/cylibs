DISTFILES = \
    $$PWD/cydoc2html \
    $$PWD/cydoc2pdf.sh \
    $$PWD/cydoc2po \
    $$PWD/cydocupdate.sh \
    $$PWD/cypo2cydoc \
    $$PWD/cydochtml2ps \
    $$PWD/DocbookAutoEntityCompletion.sh

linux{
bin.files = $$DISTFILES
bin.path  = $${CY_INSTALL_CYDOC}/bin
bin.uninstall = rm /usr/bin/cydoc*;
bin.uninstall = rm /usr/bin/cypo2cydoc
INSTALLS += bin
}

linux{
usrbin.path  = $${CY_INSTALL_CYDOC}/bin
usrbin.extra = ln -fs $${CY_INSTALL_CYDOC}/bin/cy* /usr/bin
INSTALLS += usrbin
}
