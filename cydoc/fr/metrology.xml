<chapter BENCHTYPE="METRO" id="metrology">
  <title>L'outil de métrologie</title>

  <screenshot><mediaobject><imageobject><imagedata fileref="pics/metrology.png" format="PNG" scale="50" align="center"/></imageobject></mediaobject></screenshot>

  <sect1 id="metrology-intro">
    <title>Principe</title>
    <para>Cet outil permet de réaliser une <emphasis role="bold">vérification des capteurs</emphasis> et des mesures intégrées sur le banc d'essai, soit un <emphasis role="bold">contrôle de la chaîne de mesure complète</emphasis> en prenant en compte les <emphasis role="bold">caractéristiques capteur</emphasis> par une fiche capteur paramétrable.</para>
    <para>De plus, le système intègre une <emphasis role="bold">fonction d'étalonnage</emphasis> qui offre la possibilité de corriger d'éventuels non-linéarités de mesures par <emphasis role="bold">interpolation linéaire</emphasis>.</para>
    <para>Pour réaliser une vérification ou un étalonnage d'un capteur, il faut commencer par effacer son tableau en cours puis le compléter progressivement en utilisant par exemple le mode forçage afin de créer les différents points de contrôle.</para>
    <para>A la fin d'une vérification ou d'un étalonnage, <emphasis role="bold">la traçabilité métrologique</emphasis> y est assurée par l'enregistrement d'un message relatif dans les journaux d'événements et la production d'un procès verbal en PDF reprenant toutes les informations contenues à l'écran.</para>
    <screenshot><mediaobject><imageobject><imagedata fileref="pics/EA_SP61_2012-01-25_14-51-20_verification.jpg" format="JPEG" scale="70" align="center"/></imageobject></mediaobject></screenshot>
    <screenshot><mediaobject><imageobject><imagedata fileref="pics/EN_DB3_2012-01-30_14-55-56_verification.jpg" format="JPEG" scale="70" align="center"/></imageobject></mediaobject></screenshot>
    <para>Enfin, la <emphasis role="bold">périodicité de vérification</emphasis> intégrée au moyen, alerte l'opérateur lorsque qu'une vérification d'un capteur est nécessaire.</para>

    <sect2 id="metrology-view">
      <title>Vue centrale</title>
      <para>La vue centrale qui représente la fiche métrologique est divisée en 4 parties :</para>
      <itemizedlist>
        <listitem><para><link linkend="metrology-sensor-sheet">La fiche capteur </link>donnant toutes les informations du capteur;</para></listitem>
        <listitem><para>Les informations saisies lors de la dernière vérification ou du dernier étalonnage;</para></listitem>
        <listitem><para><link linkend="metrology-sensor-table">Tableau des points contrôlés</link>;</para></listitem>
        <listitem><para>Représentation graphique des points contrôlés;</para></listitem>
      </itemizedlist>
    </sect2>

    <sect2 id="metrology-list">
      <title>Liste des capteurs</title>
      <para>La liste des capteurs, à gauche, permet de sélectionner le capteur à traiter.</para>
      <para>Un filtre en haut permet de lister :</para>
      <itemizedlist>
        <listitem><para>que les capteurs contrôlés périodiquement;</para></listitem>
        <listitem><para>que les capteurs non contrôlés périodiquement;</para></listitem>
        <listitem><para>tous les capteurs (désactive le filtre);</para></listitem>
      </itemizedlist>
    </sect2>
  </sect1>

  <sect1 id="metrology-menu">
    <title>La barre de menus</title>
      <para>Une entrée de menu est grisée (interdite) si les conditions machine ne le permettent pas (Vérifiez la messagerie dans le bandeau inférieur Cylix). Si vous ne disposez pas du niveau d'accès adéquat certaines entrées de menu peuvent être cachées.</para>

    <sect2>
      <title>Le menu <guimenu>Fichier</guimenu></title>
      <variablelist>
        <varlistentry>
          <term><menuchoice>
            <guimenu>Cylix</guimenu><guimenuitem>Vérification</guimenuitem>
          </menuchoice></term>
          <listitem><para>Valide conforme ou non conforme la vérification et créé un PV en PDF.</para>
          <para>L'en-tête de la fiche métrologie est alors mise à jour avec la conformité ou non de la vérification ainsi que la prochaine date de vérification.</para>
          <screenshot><mediaobject><imageobject><imagedata fileref="pics/metrology_header_check_ok.png" format="PNG" scale="50" align="center"/></imageobject></mediaobject></screenshot>
          <screenshot><mediaobject><imageobject><imagedata fileref="pics/metrology_header_check_nok.png" format="PNG" scale="50" align="center"/></imageobject></mediaobject></screenshot>
          </listitem>
        </varlistentry>
      </variablelist>
      <variablelist>
        <varlistentry>
          <term><menuchoice>
            <guimenu>Cylix</guimenu><guimenuitem>Etalonner</guimenuitem>
          </menuchoice></term>
          <listitem><para>Sauvegarde la feuille d'étalonnage, étalonne et créé un PV d'étalonnage en PDF.</para>
          <para>L'en-tête de la fiche d'étalonnage est alors mise à jour avec la date de cet étalonnage ainsi que la prochaine date de vérification.</para>
          <screenshot><mediaobject><imageobject><imagedata fileref="pics/metrology_header_calibration.png" format="PNG" scale="50" align="center"/></imageobject></mediaobject></screenshot>
          </listitem>
        </varlistentry>
      </variablelist>
      <variablelist>
        <varlistentry>
          <term><menuchoice>
            <guimenu>Cylix</guimenu><guimenuitem>Quitter</guimenuitem>
          </menuchoice></term>
          <listitem><para>Ferme cet outil de métrologie.</para></listitem>
        </varlistentry>
      </variablelist>
    </sect2>

    <sect2>
      <title>Le menu <guimenu>Édition</guimenu></title>
      <variablelist>
        <varlistentry>
          <term><menuchoice>
            <guimenu>Édition</guimenu><guimenuitem>Fiche capteur</guimenuitem>
          </menuchoice></term>
          <listitem><para>Permet de modifier la <link linkend="metrology-sensor-sheet">fiche capteur</link>.</para></listitem>
        </varlistentry>
        <varlistentry>
          <term><menuchoice>
            <guimenu>Édition</guimenu><guimenuitem>Valeurs constructeur</guimenuitem>
          </menuchoice></term>
          <listitem><para>Recharge la feuille d'étalonnage courante par les valeurs constructeur comme celles de la fiche capteur et celles des 2 points théoriques nécessaires pour faire une interpolation linéaire sur la plage de mesure.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><menuchoice>
            <guimenu>Édition</guimenu><guimenuitem>Effacer tableau</guimenuitem>
          </menuchoice></term>
          <listitem><para>Permet de commencer une vérification ou un étalonnage en effaçant le tableau de points en cours.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><menuchoice>
            <guimenu>Édition</guimenu><guimenuitem>Ajouter point</guimenuitem>
          </menuchoice></term>
          <listitem><para>Ouvre une boîte de saisie relative au <link linkend="metrology-calibrate-mode">mode calibrage</link> pour ajouter un nouveau point de contrôle.</para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term><menuchoice>
            <guimenu>Édition</guimenu><guimenuitem>Supprimer point</guimenuitem>
          </menuchoice></term>
          <listitem><para>Supprime le point de contrôle sélectionné dans le tableau.</para>
          </listitem>
        </varlistentry>
      </variablelist>
    </sect2>

    <sect2 id="metrology-menu_setting">
      <title>Le menu <guimenu>Configuration</guimenu></title>
      <variablelist>
        <varlistentry>
          <term><menuchoice>
            <guimenu>Configuration</guimenu><guimenuitem>Configuration métrologie</guimenuitem>
          </menuchoice></term>
          <listitem><para>Boîte de configuration vous permettant de modifier différentes données propre au fonctionnement de l'outil de métrologie et non de la machine.</para>
          <screenshot><mediaobject><imageobject><imagedata fileref="pics/metrology_setting.png" format="PNG" scale="50" align="center"/></imageobject></mediaobject></screenshot>
<!--          <para><table frame='all'><title>Configuration métrologie</title><tgroup cols='1' align='left' colsep='1' rowsep='1'><tbody>
              <row valign='bottom'><entry>&CY_METRO_VIEW_WHOLE;</entry></row>
            <row valign='bottom'><entry>&CY_METRO_DEV_CURVE_STYLE;</entry></row>
            <row valign='bottom'><entry>&CY_METRO_LIN_CURVE_STYLE;</entry></row>
          </tbody></tgroup></table></para>-->
          </listitem>
        </varlistentry>
      </variablelist>
    </sect2>

    <sect2 id="metrology-menu_aide">
    <title>Le menu <guimenu>Aide</guimenu></title>
    &menu_aide;
    </sect2>
  </sect1>

  <sect1 id="metrology-sensor-sheet">
    <title>La fiche capteur</title>
    <para>La fiche capteur éditable par le menu "Édition" donne, pour chaque capteur, les caractéristiques et les options de l'outil.</para>
    <screenshot><mediaobject><imageobject><imagedata fileref="pics/metrology_sensor_sheet.png" format="PNG" scale="50" align="center"/></imageobject></mediaobject></screenshot>
    <sect2 id="metrology-calibrate-mode">
      <title>Mode de calibrage</title>
      <para>Le premier paramètre de la fiche capteur donne le mode de calibrage qui peut être :</para>
      <itemizedlist>
        <listitem><para>automatique : ajout d'un point de calibrage par lecture de la moyenne de la valeur directe du capteur;</para>
          <screenshot><mediaobject><imageobject><imagedata fileref="pics/metrology_new_point_auto.png" format="PNG" scale="50" align="center"/></imageobject></mediaobject></screenshot>
        </listitem>
      <listitem><para>manuel      : ajout d'un point de calibrage par saisie de la valeur directe du capteur;</para>
          <screenshot><mediaobject><imageobject><imagedata fileref="pics/metrology_new_point_manual.png" format="PNG" scale="50" align="center"/></imageobject></mediaobject></screenshot>
        </listitem>
      </itemizedlist>
    </sect2>
    <sect2 id="metrology-sensor-type">
      <title>Type de capteur</title>
      <para>Cette donnée, non modifiable, dépend en fait du type de sortie du capteur qui peut-être :</para>
      <itemizedlist>
        <listitem><para>analogique  : tension, courant...</para></listitem>
        <listitem><para>numérique   : bus de terrain, train d'impulsions, code numérique binaire...</para></listitem>
      </itemizedlist>
    </sect2>
    <sect2 id="metrology-sensor-range">
      <title>Plage de mesure</title>
      <para>Les points haut et bas peuvent être modifiés pour agrandir ou diminuer la plage de mesure du capteur. Ceci peut servir en cas de remplacement du capteur par un autre avec une plage de mesure différente.</para>
    </sect2>
    <sect2 id="metrology-sensor-info">
      <title>Informations capteurs</title>
      <itemizedlist>
        <listitem><para>Constructeur  : Champ pouvant par exemple indiquer la référence du fabricant.</para></listitem>
        <listitem><para>P/N (Type)    : Type, famille de capteur.</para></listitem>
        <listitem><para>S/N (N Série) : Numéro de série.</para></listitem>
        <listitem><para>Environnement : Fluide, air...</para></listitem>
      </itemizedlist>
<!--        <para><table frame='all'><tgroup cols='1' align='left' colsep='1' rowsep='1'><tbody>
          <row valign='bottom'><entry>Constructeur</entry><entry valign='top'>Champ pouvant par exemple indiquer la référence du fabricant.</entry></row>
          <row valign='bottom'><entry>P/N (Type)</entry><entry valign='top'>Type, famille de capteur.</entry></row>
          <row valign='bottom'><entry>S/N (N Série)</entry><entry valign='top'>Numéro de série.</entry></row>
          <row valign='bottom'><entry>Environnement</entry><entry valign='top'>Champ pouvant par exemple indiquer un fluide utilisé.</entry></row>
        </tbody></tgroup></table></para>-->
    </sect2>
    <sect2 id="metrology-sensor-ut">
      <title>Type d'incertitude</title>
      <itemizedlist>
        <listitem><para>Incertitude - % PE: L'incertitude de mesure totale du capteur est exprimée en pourcentage de la pleine échelle du capteur. Elle est donc figée pour chaque mesurage et peut aussi être exprimée en unitée capteur.</para></listitem>
        <listitem><para>Incertitude - valeur fixe: L'incertitude de mesure totale du capteur est exprimée en unité capteur. Elle est donc figée pour chaque mesurage et peut aussi être exprimée en pourcentage de la pleine échelle du capteur.</para></listitem>
        <listitem><para>Incertitude - % lecture: L'incertitude de mesure totale du capteur est exprimée en pourcentage de la mesure lue.</para></listitem>
      </itemizedlist>
<!--        <para><table frame='all'><tgroup cols='1' align='left' colsep='1' rowsep='1'><tbody>
          <row valign='bottom'><entry>Incertitude - % PE</entry><entry valign='top'>L'incertitude de mesure totale du capteur est exprimée en pourcentage de la pleine échelle du capteur. Elle est donc figée pour chaque mesurage et peut aussi être exprimée en unitée capteur.</entry></row>
          <row valign='bottom'><entry>Incertitude - valeur fixe</entry><entry valign='top'>L'incertitude de mesure totale du capteur est exprimée en unité capteur. Elle est donc figée pour chaque mesurage et peut aussi être exprimée en pourcentage de la pleine échelle du capteur.</entry></row>
          <row valign='bottom'><entry>Incertitude - % lecture</entry><entry valign='top'>L'incertitude de mesure totale du capteur est exprimée en pourcentage de la mesure lue.</entry></row>
        </tbody></tgroup></table></para>-->
    </sect2>
    <sect2 id="metrology-sensor-ctrl">
      <title>Contrôle</title>
      <itemizedlist>
        <listitem><para>Périodicité du contrôle  : permet de calculer le temps de la prochaine vérification.</para></listitem>
        <listitem><para>Contrôle calibrage: alerte l'opérateur lorsque la date de vérification d'un capteur est dépassée.</para></listitem>
      </itemizedlist>
    </sect2>
  </sect1>

  <sect1 id="metrology-sensor-table">
    <title>Tableau des points contrôlés</title>
    <para>Ce tableau liste jusqu'à 20 points de contrôle possible. Avant toute vérification ou tout étalonnage il faut effacer le tableau. Seul un étalonnage à la fin de la saisie des nouveaux points aura une incidence sur le comportement du banc.</para>
    <para>Il est de la responsabilité du métrologue de décider si le vérification du capteur est correcte ou non. Il s'aide du tableau qui lui affiche clairement si les mesures sont comprises dans l'incertitude du capteur ou non. Le métrologue peut aussi décider de linéariser le capteur à partir des valeurs de cette table en supprimant éventuellement certaines valeurs du tableau. En cas de linéarisation le métrologue devra réaliser une nouvelle vérification du capteur pour vérifier que le capteur est alors conforme ou non.</para>
    <para>Lors de la saisie des points, il est possible de saisir un temps de moyennage différent pour chaque point au type de mesure traîtée. Le moyennage débute lorsque l'on demande la saisie d'un nouveau point.</para>
    <para><table frame='all'><title>Tableau de métrologie</title><tgroup cols='2' align='left' colsep='1' rowsep='1'><tbody>
      <row valign='top'><entry>Mini</entry><entry>Incertitude minimum calculée du point.</entry></row>
      <row valign='top'><entry>Étalon</entry><entry>Valeur saisie du capteur étalon.</entry></row>
      <row valign='top'><entry>Maxi</entry><entry>Incertitude maximum calculée du point.</entry></row>
      <row valign='top'><entry>Banc</entry><entry>Valeur moyennée du capteur banc.</entry></row>
      <row valign='top'><entry>Erreur</entry><entry>Écart entre le capteur du banc et le capteur étalon.</entry></row>
      <row valign='top'><entry>Erreur (%PE)</entry><entry>Erreur sur la pleine échelle entre le capteur du banc et le capteur étalon.</entry></row>
      <row valign='top'><entry>Banc (ADC)/Brute</entry><entry>Mesure directe (ADC ou numérique) moyennée.</entry></row>
      <row valign='top'><entry>Ligne Droite</entry><entry>Valeur théorique du capteur calculée sur la mesure directe avec gain et offset issus de la plage de mesure.</entry></row>
      <row valign='top'><entry>Erreur de linéarité</entry><entry>Erreur de linarité par rapport à la valeur brute théorique du capteur.</entry></row>
      <row valign='top'><entry>Temps de moyennage</entry><entry>Temps de moyennage effectif lors de l'ajout du point.</entry></row>
    </tbody></tgroup></table></para>
    <note><para>L'erreur et l'erreur de linéarité sont égales si la mesure banc n'est pas corrigée (Capteur "neuf"), autrement dit elles sont différentes si le capteur a subi une correction par interpollation linéaire.</para></note>
  </sect1>
</chapter>
