commit 5db8d1386f11bb7a79b9d7a01e4dd85601a89cd1 (HEAD -> master)
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Sun Mar 14 21:44:33 2021 +0100

    5.36-qt-5.15.2

commit c4b8ed18221737bd55557ca8d714049a5eb6900a
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Sun Mar 14 09:08:42 2021 +0100

    fix(cydisplay): Gestion des curseurs dans l'analyseur d'oscilloscope par activation forcée des axes

commit ae2e2aff775c7b7a9393448acaa54461740e3294
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Mar 11 14:21:10 2021 +0100

    chore(setup): release 5.36-qt-5.15.2

commit 30e5120c9e9e0c0b445ace1954cc4c6fcf8513be
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Mar 11 14:11:10 2021 +0100

    fix(cydoc): Correction problème crashs aléatoires lors de la génération du manuel Cydoc
    
    Détachement du processus de génération du manuel.

commit 0435c81ea3d23ea15e7aea76f0dcc13122b79f51 (tag: 5.36, origin/master)
Merge: 8abfc45 a03ea77
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Jan 28 10:24:52 2021 +0100

    Merge remote-tracking branch 'origin/master'

commit 8abfc45df69dd6869b3edbf3408b098614954332
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Jan 27 06:35:05 2021 +0100

    fix(linux): démarrage automatique CRNCY_Clt

commit a03ea7735332eb84f31f7a55bcf0d9e7cf347921
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Jan 27 06:35:05 2021 +0100

    fix(linux): démarrage automatique CRNCY_Clt

commit 6fd58dfc43d252d393755d544b88feef90760108
Merge: c7188c1 461cb2b
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 18 05:53:22 2020 +0100

    Merge remote-tracking branch 'origin/master'

commit c7188c14e583f54207e087da5fbfabed8e106520
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 18 05:39:29 2020 +0100

    feat(cydata): Nouveau constructeur sortie TOR

commit 461cb2b610d2a50e387fcf8da7b96e7a53541a01
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 18 05:39:29 2020 +0100

    feat(cydata): Nouveau constructeur sortie TOR

commit 4ff3cce204c4ed2f1c1ee449cf03fb2d70156abb
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Dec 9 16:16:22 2020 +0100

    refactor(cydoc): mise à jour

commit f4062a2ebb7c2ea5dfd1507c6ef72936367c9026 (tag: 5.35)
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Wed Dec 9 06:04:05 2020 +0100

    chore(): Optimisation temps de compilation
    
    L'utilisation de QT += ordered est déconseillée car elle peut ralentir les constructions multi-cores. Il est préférable d'utiliser l'élément .depends entre les sous-projets Qt.

commit 44450bac1ad2a368ee1c68102de4bf5381cce2eb
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Wed Dec 9 05:53:09 2020 +0100

    chore(): Corrections appels post_install & post_uninstall dans projet Qt

commit 854d5d4686c05c30359866dc6d25d7f0c97a58b0
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Tue Dec 8 23:52:26 2020 +0100

    chore(): Ajout du sous-projet Qt languages

commit a7d133b7e959b35d5d619256d47b3f2014cf5e91
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Wed Dec 9 06:22:31 2020 +0100

    chore(): Amélioration des messages généraux projet Qt

commit 2604baf15efd24c86256d0d2d7fc93dc684d080a (tag: 5.34)
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Tue Dec 8 17:39:23 2020 +0100

    chore(setup): release 5.34-qt-5.15.1

commit 050b88ae390b02a6a7abe6708c9eaf61a3e7e978
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Tue Dec 8 17:39:05 2020 +0100

    fix(cylibstest): gestion fichier inexistant pour analyseSheet

commit c6a87aacacdb58f5f6f25a00d1c5c079d60d411d
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 4 18:12:24 2020 +0100

    fix(setup): Exécution post_install à l'installation

commit 3fcd3126af537e5e104f86cdb7c82468fbab133a
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 4 18:10:20 2020 +0100

    refactor(cycore): Nettoyage warnings de compilation

commit 2f00353763a64a59f6f3fbb01cad5057d86ac56a
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 4 18:09:30 2020 +0100

    feat(cydisplay): Ajout en légende d'oscilloscope les groupes des signaux

commit 74d1907b1d5e717d3ddf281c6aa0772cffcae91d
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 4 17:18:38 2020 +0100

    fix(cydata): description événements de calibrage

commit 8217134b3ea0422ffbae41eeb73c9d7aeb62faca
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 4 17:17:08 2020 +0100

    fix(cyanalyse): menu contextuel feuille d'analyse

commit b6088ce1af1ed04e7a9242aa194e2372ff17af7d
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 4 14:33:55 2020 +0100

    chore(): Optimisation projet Qt
    
    Installation en fonction du mode de compilation (Release ou Debug) et en fonction du système d'exploitation (Linux ou Windows).  Cette optimisation est spécialement faite pour le développement de Cylix en sous projet de Cylibs/examples.
    
    Le mode Debug est dédié au développement et installe sur le système les fichiers pour cela, tels que les .h. Sous Linux, les fichiers nécessaires à la génération du manuel d'utilisation cydoc,  tels que les .xml et images sont également installés sur le système. A l'inverse, ceux-ci n'étant pas utilisés sous Windows, ils ne sont donc pas installés pour cet OS. Son système de fichier étant plus lent que celui de Linux, cette économie apporte un gain de temps important pour chaque make install.
    
    Le mode Release est destiné aux versions livrables chez le client. Ce mode permet d'avoir un code compilé optimisé en exécution mais aussi une taille de paquet d’installation optimisée.

commit 9c5c120fc76d1b6181a6d913f4224cfa0d091867
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Fri Dec 4 13:26:08 2020 +0100

    fix(cycore): Compilation 64bits Windows

commit 21ef397697c18c3f4695c65cf6ec658837d0c13f
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Thu Dec 3 06:35:23 2020 +0100

    fix(windows): post_install

commit 43c047c4c3c89ed13035b13df6c0f68e393e8e3c
Author: LE CLEACH Gérald <gerald.lecleach@solia-concept.fr>
Date:   Tue Dec 1 16:41:54 2020 +0100

    refactor(setup): Windows

commit 21a9e04153f151d2c0d40e88c2015858d0ec67c6 (tag: 5.33)
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Sat Nov 28 07:58:52 2020 +0100

    chore(setup): release 5.33-qt-5.15.1

commit fd33cd825ea059f5246a32ae6a0354232d2b9aa7
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Sat Nov 28 07:57:55 2020 +0100

    fix(setup): linuxdeployqt ubuntu 20.04
    
    Rétrogradation à la version 5 de linuxdeployqt car les versions 6 & 7 ne fonctionnent pas sur ubuntu-20.4 qui a a une version glibc trop récent.

commit d276cae350e23e6d55f0736bd8caa40ea674bf0a
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Sat Nov 28 07:33:26 2020 +0100

    fix(cycore): Ajout du type compteur 64bits

commit c1f1eb750daf0ceddea511a873678aaf59c2c7f6
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Nov 27 09:26:30 2020 +0100

    chore(setup): release 5.33-qt-5.15.1

commit 099057247dbad63bf5050c863e2abb506d86e3d7
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Nov 27 09:25:29 2020 +0100

    Correction setup ubuntu 18.04
    
    L'option -unsupported-allow-new-glibc de  linuxdeployqt  permet de faire un setup sous ubuntu 20.04 mais celui-ci n'est pas instalable sous ubuntu 18.04 à cause d'une incompatibilité de version de GLIBC

commit 7b75d323bea0e33fb046dcf2f70d5fa60ff2c07f
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Nov 26 16:29:02 2020 +0100

    fix(cycore): Nom de donnée référence constructeur  dans fenêtre "À propos" de l'application

commit 8282caf0bfc5b197401437e424aab7b698c1be78
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Nov 26 14:56:20 2020 +0100

    chore(setup): release 5.33-qt-5.15.1

commit 5ea961919df9dcf9c7517dec753cb3ce5334b4ea
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Nov 26 14:06:39 2020 +0100

    fix(cycore): Création par erreur d'un journal d'événements à chaque démarrage de Cylix.

commit 2d7e1bb26ba5d9fd951513377292ad663be7c531
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Nov 26 13:36:42 2020 +0100

    feat(cycore): Ajout du type compteur 64bits
    
    - Version identique à celle de la libraire permet de connaître directement la librairie ayant entraîné les derniers changements de ce fichier.

commit 330cb83ff73dd74e4c9cc2152bf4a6b21f6fbb91
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Nov 26 11:23:04 2020 +0100

    feat(cydoc): Ajout bordures de tableau pour les historiques de versions.

commit e52bc121c534ce9e4f88d72314e8a16d892e4955
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Jan 29 07:09:28 2020 +0100

    fix(Qt): QFileInfo::birthTime() not supported on UNIX
    
    Les dates et heures dans l'historique des journaux d'évènements n'apparaissaient plus.

commit 9bd517496ce3f7eb9db4bd67c42c752d58f60921
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Nov 20 05:58:14 2020 +0100

    fix(cyui): CYButtonGroup / Ajout propriété Qt Designer "protectedAccess"
    
    En sortant du mode protégé les boîtes de saisie de nombre CYNumInput semblaient active mais il était impossible de changer de valeur. Pour pouvoir le faire il fait icônifier puis désicônifier la fenêtre parent . Pour info CYSlider fonctionnait correctement.

commit 18861072c80b600fad1b478c7e542e907c9024a7
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Nov 19 17:32:27 2020 +0100

    feat(cyui): CYButtonGroup / Ajout propriété Qt Designer "protectedAccess"

commit 710e60f3b60d65e1a6056bc6f6c8881f4af379d5
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Nov 19 17:34:31 2020 +0100

    feat(cyui): CYPushButton / Amélioration propriété Qt Designer "protectedAccess"
    
    Contrôle automatique à chaque changement d'accès (refresh plus nécessaire)

commit e9ef30e085f6b7431f3a3221400cbb62ffe88ba9
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Nov 19 16:35:07 2020 +0100

    refactor(cycore): Renommage dans infos PC "ref_sta" en "ref"

commit f8c9ad9eb7fabaa1fe6a59d6107b62fc4a81acef
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Nov 18 15:13:53 2020 +0100

    fix(cyui): CYSlider / Initialisation à la valeur de donnée

commit 999ebd4396febc36ae2d36685fd9f83628796b8e
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Nov 18 07:13:15 2020 +0100

    refactor(cyui): Amélioration taille fenêtre "À propos" de l'application
    
    - Mémorisation taille
    - Suppression largeur minimum
    - Largeur par déafut à 550

commit 82fc3a026b11bb90c5d5dfbc05da077853517f73
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Tue Nov 17 12:49:59 2020 +0100

    fix(cyui): CYSlider / Affectation directe de valeur
    
     CYSlider: curseur de saisie d'une donnée numérique.

commit f06b007ec50e00207344a9442da1bde80633f129
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Tue Nov 17 12:17:05 2020 +0100

    fix(cyui): CYNumInput / Gestion événements clavier
    
    CYNumInput: boîte de saisie d'une donnée numérique.

commit 8f7416ceece4f3e1f50974c7986d6a8c24fac7ec
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Nov 12 14:26:49 2020 +0100

    feat(cycore): Ajout du type compteur 64bits

commit 6e3506a99f5b8b8c2464815f70af1b67b80f8120
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Oct 21 13:09:00 2020 +0200

    fix(cycore): Crash si menu dans fichier RC inconnu

commit aa2ad76ba6907978e8120f9c4dd48b12f8e49441
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Oct 21 13:04:14 2020 +0200

    fix(cycore): Crash protection environnement en compilation Debug
    
    Inhibition en compilation Debug.

commit fe11be98f99cf5e376778b99474657eb97633afb (tag: 5.32)
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Oct 7 18:47:41 2020 +0200

    chore(setup): release 5.32-qt-5.15.1

commit 5ebad76796dec7e977a31ee55e2c845254ed44da
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Oct 7 18:40:49 2020 +0200

    fix(cycore): Remplacement  ligne de séparation <hr> par un saut de ligne en DocBook

commit c768d73fe7910eac1da31f0d2f5a61d760ada60a
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Oct 7 18:37:35 2020 +0200

    fix(cydisplay): saut à la ligne avant nom de données dans les messages d'aides

commit f9880ff0b24debbde6c5ed046760f0b886e94405
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Oct 7 18:36:39 2020 +0200

    fix(cyui): saut à la ligne avant nom de données dans les messages d'aides

commit 9df02674e707880e27b3482f33eb3f9a87d3e473
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Oct 7 06:32:17 2020 +0200

    chore(setup): release 5.32-qt-5.15.1

commit b549fc2db7022d7d80438a1262226c17089e6801
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Oct 7 06:09:23 2020 +0200

    fix(examples): Création manuel en ligne générique par cylibstest
    
    - Création du fichier "cylix_entity.dtd" pour générer le manuel sans avoir besoin de le créer en démarrant un cylix.
    - mise à jour "README" pour le choix de la langue

commit 4e4f60f2476e5b8df13caeaa116fafeaf668beca
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Tue Oct 6 15:52:26 2020 +0200

    feat(cydoc): Configuration RN LINUX

commit cf64eeb9f49e5c59cfc589c58fddae0c416160c2
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Tue Oct 6 15:50:08 2020 +0200

    refactor(cydoc): install_rn => rn

commit 69b02cf52d321fe95ebdfa6cbda56383142acd4a
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Tue Sep 29 11:01:05 2020 +0200

    refactor(cyui): Amélioration changement de mot passe
    
    - Géométrie
    - Ajout vérification niveau mot de passe
    - Titres

commit 4b116e6234b1f37a6be25124a31eef447da374dd
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Mon Sep 28 05:49:05 2020 +0200

    fix(cycore): Problème passage en mode constructeur par validation accès admin sans mot de passe

commit 7240e34ba2409ab426b0223057030c13cddd5642
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Sun Sep 27 09:02:40 2020 +0200

    fix(cycore): Analyse graphique: suppression de la proposition d'inclure les sous-groupes s'il n'en existe pas.

commit 53beb58e2792062ed92aa4656b4ae01a12d78872
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Sep 25 17:11:10 2020 +0200

    fix(cyuser): Icônes flèches sous Windows dans administration groupe utilisateurs

commit 520fd2e38e6e99fdc26eeb3fcce554675623322e
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Sep 25 12:22:00 2020 +0200

    chore(git): ignore examples/cylix*

commit b4aea714f8f47fa75fd01e1231ecddbe7dabc29f
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Sep 25 06:32:43 2020 +0200

    refactor(src): remplacement de certaines fonctions Qt obsolètes
    
    Reste 9/42 warnings dont 8 dans qwt

commit b2d637d209e946a3f87ae20e34fd364294a64152
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Sep 25 06:31:31 2020 +0200

    feat(qwt): 6.1.2 => 6.1.5

commit cfd48aae4855aa7c5ff80f14f8cf3879d2467636
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Sep 23 06:30:25 2020 +0200

    refactor(qwt): remplacement de certaines fonctions Qt obsolètes
    
    Reste 8 warnings

commit 6dae10573c4a4265a4fc29d084f11fe648a3ff11
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Sep 23 06:22:06 2020 +0200

    fix(qwt): 6.1.2 => 6.1.5 QwtPointMapper multithreaded pas compatible avec yCoef et  yOffset
    
    Désactivation de QWT_USE_THREADS comme avant

commit e260fc03fcad11d51a720acf0370341b1587006d
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Tue Sep 22 09:33:38 2020 +0200

    feat(qwt): 6.1.2 => 6.1.5

commit 7d9b441069214f387e28362998f9cc092ea7c19f
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Sep 17 06:34:28 2020 +0200

    feat(cyui): Ajout en propriété CYLineEdit::fontSize afin de

commit 3c0feb0fe2a2eddf14f4af3d2d6ffdacbdf6ef5a
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Aug 26 17:50:42 2020 +0200

    fix(cyui): Correction contrôle nombre maxi de lignes boite de saise de texte CYTextEdit

commit cba840b0630e6f90541c99f5ae0dd19ff0c63d95
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Sun Sep 13 07:54:57 2020 +0200

    refactor(qwt): mise à jour des repères qwt2cylibs

commit d5c72a2cab9e09ea6d288bcb2a55a332f333736d
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed Aug 26 17:33:27 2020 +0200

    fix(linux): Script d'installation du module de communication Cylix Ethernet Client
    
    Ancien système de démarrage automatique d'application < ubuntu 18.04

commit 4ca6981b98d1e46b271570037b7be64289e02f89 (tag: 5.31)
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Tue Jul 28 15:25:45 2020 +0200

    refactor(languages): update

commit 7d0aa01281ba6dcbc731bc98f8bfa116a0ab9464
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Jul 24 16:13:54 2020 +0200

    refactor(cydoc): Mise en pages historique des versions

commit b9ffbe272ed656f0e4637271ddc7f927c70fb298
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Jul 24 16:11:45 2020 +0200

    refactor(): commentaires

commit 28d58ed00f5313a0a2d774ed39d920c6f55fb7a0
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Jul 24 16:11:08 2020 +0200

    fix(cydata): Passages à ligne aide en bulle de saisie

commit 3624e677a76c3ed488ef0432d5634cd9b7d93480
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Jul 16 21:24:15 2020 +0200

    fix(windows): Qt5Xml.dll

commit 599e9fc9194c1e251209ea32320dff491d159ac0
Merge: 0bbcc47 dc63f19
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Jul 16 17:54:42 2020 +0200

    Merge remote-tracking branch 'origin/master'

commit 0bbcc47881bed6fec3052dcc5d7eb6133af87965
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Jun 4 15:20:41 2020 +0200

    feat(cyevent): Décodage automatique des  journaux d'événements
    
    UTF-8 ou localisation par défaut

commit dc63f1957e7a8b3410a4e7e9319f7420142e0ad3
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Jun 4 15:20:41 2020 +0200

    chore(examples): gitignore

commit eb27c86433e8502b93445e1d845dba53e2f56241 (tag: 5.30)
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu May 28 21:20:37 2020 +0200

    chore(setup): release 5.30-qt-5.14.1

commit e5f15d91276628ddeef2abf850bcb2eec4552e0f
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu May 28 13:53:14 2020 +0200

    refactor(cydisplay): suppression attente 1s pour l'impression tableau en PDF

commit 4deab6911231248ac07fdcb22e76dd6db6416de0
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu May 28 13:52:55 2020 +0200

    refactor(cydisplay): suppression attente 1s pour l'impression courbe en PDF

commit c68bdf918ec49f17ff8e72047ad6b32e3e9a1392
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu May 28 11:21:48 2020 +0200

    fix(cydisplay): valeurs capture oscilloscope XY
    
    - Le buffer était initialisé uniquement sur le nombre de valeur à bufferiser et non sur toutes les valeurs du buffer.
    - Un refresh non nécessaire ajoutait des mauvaises valeurs avant le premier point.

commit ce0a07f51ecaeae4ad1c12dc600d7ba46576ece8
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu May 28 11:21:48 2020 +0200

    fix(cydisplay): valeurs capture oscilloscope XY
    
    - Le buffer était initialisé uniquement sur le nombre de valeur à bufferiser et non sur toutes les valeurs du buffer.
    - Un refresh non nécessaire ajoutait des mauvaises valeurs avant le premier point.

commit 5773d96d00a926b6f35ded011104072b9dc8b11d (tag: 5.29)
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed May 13 09:41:49 2020 +0200

    chore(examples): gitignore

commit ccfe585e0fc4e188835a53edc4942ecb57be2794
Merge: ee1ba9d 1ccb35e
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed May 13 09:39:13 2020 +0200

    Merge remote-tracking branch 'origin/master'

commit ee1ba9d0725f1eda44ea5b5aaca0e2ca450bb3d3
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Wed May 13 09:38:58 2020 +0200

    chore(setup): clean

commit 087782a9b9bb47c6b8ce635e8218fd824ef37aef
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Tue May 12 23:47:35 2020 +0200

    chore(setup): release 5.29-qt-5.14.1

commit 74af14f46fc5a2f3b4bf5e542cf772d026b0c90e
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Tue May 12 08:13:58 2020 +0200

    fix(cydisplay): Wswitch-unreachable
    
    problème: Possible non exécution de code lors du relâchement de souris dans la capture d’oscilloscope.

commit 518cda6c555a86f3911575ad1cb5c906c760469f
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Apr 24 18:13:27 2020 +0200

    refactor(cyui): CYPIDInput ajout titre PID

commit fe322bba766b06b40f6600c75e897f863d45a174
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Apr 24 09:51:09 2020 +0200

    fix(cyui): CYPIDInput  hideIfNoData NO_I_RAMP

commit 1ccb35ed01b0d16a2da814654be76e5b26c7a332
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Apr 24 09:51:09 2020 +0200

    fix(cyui): CYPIDInput  hideIfNoData NO_I_RAMP

commit f3c1ca25e8704963d2bdaa422044cd46c0acee78
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Apr 24 09:49:53 2020 +0200

    refactor(linux): cykde3toqt5.sh

commit 4b65f800578faee3a9d2821193712fe43dc781fd (tag: 5.28)
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Mon Mar 23 06:25:00 2020 +0100

    chore(setup): release 5.28-qt-5.14.1

commit a2bcc1fada61e477bcda1e4b9877c5b00745a766
Merge: 0230fe4 0a7bdaf
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Sun Mar 22 08:32:40 2020 +0100

    Merge branch 'develop'

commit 0a7bdaf983ba01ec3cc1f16500832b0aa073a49c (origin/develop, develop)
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Fri Mar 13 17:37:28 2020 +0100

    fix(linux): démarrage automatique CRNCY_Clt

commit 01b035f42f57a9ae662aa77a745ead8f6da81c74
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Mar 12 18:41:30 2020 +0100

    fix(linux): démarrage automatique CRNCY_Clt

commit 43efc4c9d4e86959820d6db9b49d2ed4771cb92b
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Mar 12 18:37:30 2020 +0100

    fix(linux): démarrage automatique CRNCY_Clt

commit 6e33951e3025013bea08043e0fb63d1a5df9083e
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Mar 12 18:28:06 2020 +0100

    fix(linux): démarrage automatique CRNCY_Clt

commit fd79a86976991cc366fc6a395fe3261067a58d8a
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Mar 12 11:49:00 2020 +0100

    refactor(cydata): affichage en aide des bornes saisies calibrage en 2 points

commit f8f6d4298be4fa112189c0b74a7953b122d81802
Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
Date:   Thu Mar 12 06:35:35 2020 +0100

    fix(cyacquisition): prise en compte valeur par défaut de l'évènement d'archivage
    
    - archivage périodique et sur demande opérateur par défaut à oui
    - archivage des événements par défaut sur front montant
    Possibilité de changer ces valeur par défaut dans Cylix  lors des
     appels à addPostMortemFlags via le paramètre enable.
