################################################################
# Cylix Library
# Copyright (C) 2015   Gérald Le Cléach
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the Qwt License, Version 1.0
################################################################

include( cyconfig.pri )

message("CY_VERSION: $$CY_VERSION")
message("Qt système: $$[QT_INSTALL_PREFIX]")
message("Cy système: $$CY_INSTALL_PREFIX")

TEMPLATE = subdirs

linux{
    SUBDIRS += linux
}

windows{
    SUBDIRS += windows
}

SUBDIRS += \
    src \
    cydoc \
    setup

contains(CY_CONFIG, CyDesigner ) {
    SUBDIRS += designer
}

contains(CY_CONFIG, CyExamples ) {
    SUBDIRS += examples
    examples.depends = src
}

contains(CY_CONFIG, CyPlayground ) {
    SUBDIRS += playground
}

RESOURCES += \
    doc.qrc

TRANSLATIONS+= \
$${PWD}/languages/cylibs_fr.ts \
$${PWD}/languages/cylibs_de.ts \
$${PWD}/languages/cylibs_zh_CN.ts \
$${PWD}/languages/cylibs_cs.ts \
$${PWD}/languages/cylibs_pl.ts

translations.files = $${TRANSLATIONS}
translations.files ~= s,\\.ts$,.qm,g
translations.path = $${CY_INSTALL_LANG}
INSTALLS += translations

cyspec.files  = cyconfig.pri cyfunctions.pri cy.prf ChangeLog HOWTO TODO Doxyfile
cyspec.path  = $${CY_INSTALL_FEATURES}
INSTALLS += cyspec

linux{
# configuration complémentaire sur le système Linux
post_install.path   = $${CY_INSTALL_LINUX}
post_install.extra  = $${CY_INSTALL_LINUX}/post_install.sh $${CY_INSTALL_PREFIX} $$[QT_INSTALL_DATA]
INSTALLS += post_install
}

windows{
# configuration complémentaire sur le système Windows
post_install.path   = $${CY_INSTALL_WINDOWS}
post_install.extra  = $${CY_INSTALL_WINDOWS}/post_install.bat $${CY_INSTALL_PREFIX} $$[QT_INSTALL_DATA]
INSTALLS += post_install
}

# installation des fichiers documentation
doc.path  = $${CY_INSTALL_DOC}
doc.files = cydoc_fr.pdf cydoc_en.pdf cydoc_oper_fr.pdf cydoc_oper_en.pdf releasenotes_fr.pdf releasenotes_en.pdf
INSTALLS += doc
