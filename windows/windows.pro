#-------------------------------------------------------------------
# Fichier d'inclusion des fichiers Windows de Cylix pour QtCreator
# Copyright (C) 2016-2017   Gérald Le Cléach
#
# Cylix is free software; you can redistribute it and/or
# modify it under the terms of the LGPL License, Version 3.0
#
#-------------------------------------------------------------------
TEMPLATE = aux

include ( $${PWD}/../cyconfig.pri )

DISTFILES = \
    CRNCY_Clt.exe \
    post_install.bat \
    post_uninstall.bat \
    CRNCY_Clt_Ads.exe

windows.path = $${CY_INSTALL_WINDOWS}
windows.uninstall = $${CY_INSTALL_WINDOWS}/post_uninstall.bat $$CY_INSTALL_WINDOWS $$[QT_INSTALL_DATA]
windows.files = $$DISTFILES
INSTALLS += windows
