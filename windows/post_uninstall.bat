@echo off

set CYDIR=%1
set QTDIR=%2

IF "%CYDIR%"=="" (echo MANQUE CYDIR exit /b 1)
IF "%QTDIR%"=="" (echo MANQUE QTDIR exit /b 1)

rem Remplacement des "/" par "\"
set "CYDIR=%CYDIR:/=\%"
rd /s /q "%CYDIR%\..\bin"

rem Remplacement des "/" par "\"
set "QTDIR=%QTDIR:/=\%"
del /q "%QTDIR%\plugins\designer\cy_designer_plugin.dll"
del /q "%QTDIR%\..\..\Tools\QtCreator\bin\plugins\designer\cy_designer_plugin.dll"
