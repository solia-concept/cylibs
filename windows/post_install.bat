@echo off

set CYDIR=%1
set QTDIR=%2

IF "%CYDIR%"=="" (echo MANQUE CYDIR exit /b 1)
IF "%QTDIR%"=="" (echo MANQUE QTDIR exit /b 1)

rem Remplacement des "/" par "\"
set "CYDIR=%CYDIR:/=\%"

rem Remplacement des "/" par "\"
set "QTDIR=%QTDIR:/=\%"

xcopy /f "%QTDIR%\bin\Qt5Xml.dll" /Y /K "%CYDIR%\bin\"
xcopy /f "%QTDIR%\bin\Qt5OpenGL.dll" /Y /K "%CYDIR%\bin\"
xcopy /f "%QTDIR%\bin\Qt5Network.dll" /Y /K "%CYDIR%\bin\"
xcopy /f "%QTDIR%\bin\Qt5PrintSupport.dll" /Y /K "%CYDIR%\bin\"

xcopy /f "%CYDIR%\lib\cy.dll" /Y /K /S "%QTDIR%\bin\"
xcopy /f "%CYDIR%\lib\cy.dll" /Y /K /S "%CYDIR%\bin\"
xcopy /f "%CYDIR%\plugins\designer\cy_designer_plugin.dll" /Y /K /S "%QTDIR%\plugins\designer\"
xcopy /f "%CYDIR%\plugins\designer\cy_designer_plugin.dll" /Y /K /S "%QTDIR%\..\..\Tools\QtCreator\bin\plugins\designer\"

windeployqt "%CYDIR%\bin\cylibstest.exe

set PATH=%PATH%;%QTDIR%\bin
