#-------------------------------------------------------------------
# Fichier projet Cylix pour QtCreator
# Copyright (C) 2016-2017   Gérald Le Cléach
#
# Cylix is free software; you can redistribute it and/or
# modify it under the terms of the LGPL License, Version 3.0
#-------------------------------------------------------------------

#-------------------------------------------------------------------
# Application Cylix
include( $${PWD}/cylix.pro )

#-------------------------------------------------------------------
# Manuel en ligne
include( $${PWD}/cydoc/cydoc.pri )

#-------------------------------------------------------------------
# Fichiers spécifiques à Linux
include( $${PWD}/linux/linux.pri )

#-------------------------------------------------------------------
# Fichiers spécifiques à Windows
include( $${PWD}/windows/windows.pri )

#-------------------------------------------------------------------
# Setup d'installation
include( $${PWD}/setup/setup.pri )

DISTFILES += \
    $${PWD}/ChangeLog \
    $${PWD}/HOWTO \
    $${PWD}/TODO \
