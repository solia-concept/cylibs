#-------------------------------------------------------------------
# Fichier projet Cylix pour QtCreator
# Copyright (C) 2016-2017   Gérald Le Cléach
#
# Cylix is free software; you can redistribute it and/or
# modify it under the terms of the Qt License, Version 1.0
#-------------------------------------------------------------------

include ( $${PWD}/cylix.pri ) # CONTIENT LA VERSION CYLIX
TARGET = cylix-$${CYLIX_SN}
TEMPLATE  = app

######################################################################
# Install libraries
######################################################################
QT += core
QT += gui
QT += widgets
QT += xml
QT += opengl
QT += svg
QT += network
QT += printsupport

unix {
#    cyAddLibrary(/usr/lib, MvxSock)
}

######################################################################
include ( $${PWD}/acquisition/acquisition.pri )
include ( $${PWD}/analysesheets/analysesheets.pri )
include ( $${PWD}/benchsettings/benchsettings.pri )
include ( $${PWD}/commun/commun.pri )
include ( $${PWD}/control/control.pri )
include ( $${PWD}/core/core.pri )
include ( $${PWD}/datas/datas.pri )
include ( $${PWD}/events/events.pri )
include ( $${PWD}/icons/icons.pri )
include ( $${PWD}/maintenance/maintenance.pri )
include ( $${PWD}/mainwin/mainwin.pri )
include ( $${PWD}/modbus/modbus.pri )
include ( $${PWD}/pics/pics.pri )
include ( $${PWD}/project/project.pri )
include ( $${PWD}/pv/pv.pri )
include ( $${PWD}/settings/settings.pri )
include ( $${PWD}/synoptic/synoptic.pri )
include ( $${PWD}/test/test.pri )
include ( $${PWD}/tools/tools.pri )

DEPENDPATH += \
./ \
acquisition \
benchsettings \
control \
core \
datas \
events \
maintenance \
mainwin \
modbus \
project \
pv \
settings \
synoptic \
test \
pv

INCLUDEPATH += \
src \
acquisition \
benchsettings \
commun \
control \
core \
datas \
events \
maintenance \
mainwin \
modbus \
project \
settings \
synoptic \
test \
pv

SOURCES += \
  $$PWD/cylix.cpp \
  $$PWD/main.cpp


HEADERS += \
  $$PWD/cylix.h \
  $$PWD/cylixdef.h \
    cylixdef.h

RESOURCES += \
    guirc.qrc

DISTFILES += \
    diagram_Elec.PDF \
    diagram_Hydr.PDF

######################################################################
# Traductions
######################################################################
TRANSLATIONS += \
  $$PWD/languages/cylix-$${CYLIX_SN}_fr.ts \
  $$PWD/languages/cylix-$${CYLIX_SN}_zh_CN.ts

translations.files = $${TRANSLATIONS}
translations.files ~= s,\\.ts$,.qm,g
#translations.extra = cp -fp $${CY_INSTALL_LANG}/* $${CYLIX_INSTALL_LANG}
translations.path = $${CYLIX_INSTALL_LANG}
INSTALLS += translations

######################################################################
# Installation
######################################################################

# installation des fichiers documentation
doc.path  = $${CYLIX_INSTALL_DOC}
doc.files = diagram_Elec.pdf diagram_Hydr.pdf cydoc_fr.pdf
INSTALLS += doc

target.path += $${CYLIX_INSTALL_BIN}
INSTALLS += target

