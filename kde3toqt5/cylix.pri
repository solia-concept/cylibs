#-------------------------------------------------------------------
# Fichier d'inclusion Cylix pour QtCreator
# Copyright (C) 2016-2017   Gérald Le Cléach
#
# Cylix is free software; you can redistribute it and/or
# modify it under the terms of the LGPL License, Version 3.0
#-------------------------------------------------------------------

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# RECOMPILER AU MOINS LE FICHIER "main.cpp" POUR UNE PRISE EN COMPTE
CYLIX_SN=appname
CYLIX_VER_MAJ      = 0      # évolutions majeures
CYLIX_VER_MIN      = 00     # évolutions mineures
CYLIX_VER_PAT      = 00     # Patch ou correctif de développement
CYLIBS_VER         = 5.12   # version compatible Cylibs
DEFINES += DEF_CYLIX_DATE=\\\"2017-10-11\\\"
DEFINES += DEF_CYLIX_RN_MIN=0.00

CYLIX_NAME=cylix-$${CYLIX_SN}
DEFINES += DEF_CYLIX_SN=\\\"$$CYLIX_SN\\\"
DEFINES += DEF_CYLIX_NAME=\\\"$$CYLIX_NAME\\\"

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#CYLIX_CONFIG += release # A commenter en développement.
# CYLIX_VER_PAT est pour la version de développement
# et n'est pas prise en commpte lors d'une version livrable
contains(CYLIX_CONFIG, release){
  CYLIX_VERSION = $${CYLIX_VER_MAJ}.$${CYLIX_VER_MIN}
}
else{
  CYLIX_VERSION = $${CYLIX_VER_MAJ}.$${CYLIX_VER_MIN}.$${CYLIX_VER_PAT}
}
DEFINES += DEF_CYLIX_VERSION=\\\"$$CYLIX_VERSION\\\"

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# CYLIX_CONFIG += cylibs_release
# Utilisation de cylibs installé sur le système pour le développement
# classique d'un cylix et pour les versions livrables des clients.
# A commenter, en cas d'intégration à Cylibs, pour exécuter en application
# locale de cylibs pour développer plus rapidement cylibs.
contains(CYLIX_CONFIG, cylibs_release){
  unix{
    include ( /usr/local/Cylix/Cylibs/$${CYLIBS_VER}/features/cy.prf )
  }
  win32{
    include ( C:\Cylix\Cylibs\\$${CYLIBS_VER}\features\cy.prf )
  }
}
else{
  include( $${PWD}/../examples.pri )
}

######################################################################
# Install paths
######################################################################

unix {
    CYLIX_INSTALL_PREFIX    = /usr/local/Cylix/cylix-$${CYLIX_SN}
}

win32 {
    CYLIX_INSTALL_PREFIX    = C:/Cylix/cylix-$${CYLIX_SN}
}

CYLIX_INSTALL_DOC    = $${CYLIX_INSTALL_PREFIX}/doc
CYLIX_INSTALL_CYDOC  = $${CYLIX_INSTALL_DOC}/cydoc
CYLIX_INSTALL_BIN    = $${CYLIX_INSTALL_PREFIX}/bin
CYLIX_INSTALL_CYAS   = $${CYLIX_INSTALL_PREFIX}/cyas
CYLIX_INSTALL_ODS    = $${CYLIX_INSTALL_PREFIX}/ods
CYLIX_INSTALL_PICS   = $${CYLIX_INSTALL_PREFIX}/pics
CYLIX_INSTALL_LANG   = $${CYLIX_INSTALL_PREFIX}/languages
CYLIX_INSTALL_LINUX  = $${CYLIX_INSTALL_PREFIX}/linux
CYLIX_INSTALL_WINDOWS= $${CYLIX_INSTALL_PREFIX}/windows

DEFINES += CYLIX_STR_INSTALL=\\\"$$CYLIX_INSTALL_PREFIX\\\"
DEFINES += CYLIX_STR_INSTALL_DOC=\\\"$$CYLIX_INSTALL_DOC\\\"
DEFINES += CYLIX_STR_INSTALL_CYDOC=\\\"$$CYLIX_INSTALL_CYDOC\\\"
DEFINES += CYLIX_STR_INSTALL_BIN=\\\"$$CYLIX_INSTALL_BIN\\\"
DEFINES += CYLIX_STR_INSTALL_CYAS=\\\"$$CYLIX_INSTALL_CYAS\\\"
DEFINES += CYLIX_STR_INSTALL_ODS=\\\"$$CYLIX_INSTALL_ODS\\\"
DEFINES += CYLIX_STR_INSTALL_PICS=\\\"$$CYLIX_INSTALL_PICS\\\"
DEFINES += CYLIX_STR_INSTALL_LANG=\\\"$$CYLIX_INSTALL_LANG\\\"
DEFINES += CYLIX_STR_INSTALL_LINUX=\\\"$$CYLIX_INSTALL_LINUX\\\"
DEFINES += CYLIX_STR_INSTALL_WINDOWS=\\\"$$CYLIX_INSTALL_WINDOWS\\\"

