#ifndef CYPROJECTTESTDIRDIALOG_H
#define CYPROJECTTESTDIRDIALOG_H

// CYLIBS
#include "cydialog.h"

class CYProject;

namespace Ui {
    class CYProjectTestDirDialog;
}

class CYProjectTestDirDialog : public CYDialog
{
  Q_OBJECT
public:
  CYProjectTestDirDialog(QWidget *parent=0, const QString &name=0);
  ~CYProjectTestDirDialog();

public slots:
    void init( CYProject * project );
    void refresh();
    void accept();
    void load();

protected:
    QString mExistingDir;
    int mNumTest;
    QString mTestDirPath;
    QString mPath;
    CYProject *mProject;
    bool mWithNumTest;
    QString mDirName;
    bool mAuto;

private:
  Ui::CYProjectTestDirDialog *ui;
};

#endif // CYPROJECTTESTDIRDIALOG_H
