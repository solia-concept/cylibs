/***************************************************************************
                          cyproject.cpp  -  description
                             -------------------
    begin                : ven nov 21 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyproject.h"

// QT
#include <QTextStream>
// CYLIBS
#include "cycore.h"
#include "cyu32.h"
#include "cyf32.h"
#include "cyflag.h"
#include "cytime.h"
#include "cyeventsreport.h"
#include "cyprojecteditor.h"
#include "cyprojecttestdirdialog.h"
#include "cyacquisition.h"
#include "cyfilecsv.h"
#include "cymessagebox.h"

CYProject::CYProject(QObject *parent, const QString &name, QString filename, bool tmp, int index)
  : QObject(parent),
    mTmp(tmp),
    mIndex(index)
{
  setObjectName(name);
  mSent = false;
  mEnableRestart = false;
  mTestDirPath = "";
  mIntercalTestDir = "";
  mWithoutMessageBox = false;
  mEventsReport = 0;
  mProtectDir = "";
  mTitle  = name;

  mFileInfo.setFile(filename);
  core->historyProject()->setPath(QString("%1/parametres").arg(dirPath()));
}

CYProject::~CYProject()
{
}

void CYProject::initProject(bool tmp)
{
  Q_UNUSED(tmp)
  CYWARNINGTEXT(QString("OBSOLETE!!!\n Le passage par paramètre de 'tmp' est remplacé par un passage par référence. Ceci est  nécessaire pour un abandon correct de la création de répertoire d'essai.\n Lors d'une mise à jour de Cylibs, remplacer dans le code de Cylix 'Project::initProject(bool tmp=true)' par 'Project::initProject2(bool &tmp)' ainsi que l'appel CYProject::initProject(tmp);' par 'CYProject::initProject2(tmp);'."));
}

void CYProject::initProject2(bool &tmp)
{
  mModified   = false;
  mEditor     = core->projectEditor();
  mNumTest    = 0;
  mDummy      = false;
  mDataToApply  = false;

  connect(this, SIGNAL(testChanged(int)), core, SIGNAL(testChanged(int)));

  foreach (CYDB *db, dbList)
    db->designer();

  setProjectEnvironment(mFileInfo.filePath());

  // Chargement des données dans le cas d'un projet existant.
  QFile file(mFileInfo.filePath());
  if (file.exists())
    load(mFileInfo.filePath());

  // Initialisation du répertoire d'essai.
  if (!tmp)
  {
    if (!initTestDir())
      tmp=false;  // Si projet annulé par utilisateur => projet forcé en temporaire
  }
}

bool CYProject::initTestDir()
{
  bool newEventFile = false;
  if ( mTestDirPath.isEmpty() || core->projectLoaded() )
  {
    CYProjectTestDirDialog *dlg = new CYProjectTestDirDialog( mEditor, "CYProjectTestDirDialog");
    dlg->init( this );
    if (!dlg->exec())
      return false;
    newEventFile = true;
  }

  core->mkdir(mTestDirPath);

  mEventsReport = new CYEventsReport(this, "EventsReport", mTestDirPath, newEventFile, mIndex);
  emit testChanged(mIndex);
  return true;
}

void CYProject::setProjectEnvironment(const QString &fileName)
{
  mFileInfo.setFile(fileName);

  QHashIterator<QString, CYDB *> it(dbList);
  while (it.hasNext())
  {
    it.next();
    it.value()->setFileName(mFileInfo.path()+"/"+it.key()+".cydata");
  }
}

bool CYProject::load()
{
  return load(mFileInfo.filePath());
}

bool CYProject::load(const QString &filename)
{
  setModified(false);

  mFileInfo.setFile(filename);
  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly))
    return false;

  QString errorMsg;
  int errorLine;
  int errorColumn;

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  if (!doc.setContent(&file, &errorMsg, &errorLine, &errorColumn))
  {
    if (!mWithoutMessageBox)
      CYMessageBox::sorry(mEditor, tr("The file %1 does not contain valid XML !\n"
                                      "%2: %3 %4").arg(filename).arg(errorMsg).arg(errorLine).arg(errorColumn));
    return false;
  }
  // Vérifie le type propre du document.
  if (doc.doctype().name() != "CYLIXProject")
  {
    if (!mWithoutMessageBox)
      CYMessageBox::sorry(mEditor, QString(tr("The file %1 does not contain a valid"
                                      "definition, which must have a document type ").arg(filename)
                                      +"'CYLIXProject'"));
    return false;
  }

  // Charge les paramètres du projet.
  loadSettings();
  // Charge le contenu du fichier projet.
  load(doc);

  setModified(false);
  return true;
}

void CYProject::load(QDomDocument &doc)
{
  QDomElement el = doc.documentElement();
  mTitle  = el.attribute("Title");
  mNumTest = el.attribute("NumTest", "0").toUInt();
  mDummy  = el.attribute("Dummy").toInt();
  mTestDirPath = el.attribute("TestDirPath");
  mEnableRestart = el.attribute("EnableRestart", "0").toInt();
  mSent= el.attribute("Sent", "0").toInt();
  core->historyProject()->setFileName(el.attribute("HistorySettings"));
}

bool CYProject::save()
{
  return save(mFileInfo.filePath());
}

bool CYProject::saveWithoutMessageBox()
{
  mWithoutMessageBox = true;
  bool ret = save();
  mWithoutMessageBox = false;
  return ret;
}

bool CYProject::save(const QString &filename)
{
  QDomDocument doc("CYLIXProject");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  mFileInfo.setFile(filename);

  core->mkdir(mFileInfo.path());

  QFile file(filename);
  if (!file.open(QIODevice::WriteOnly))
  {
    if (!mWithoutMessageBox)
      CYWARNINGTEXT(tr("Can't save file %1!").arg(filename));
    return false;
  }

  // Enregistre les informations le projet.
  QDomElement el = doc.createElement("Project");
  save(doc, el);

  QTextStream s(&file);
  s.setCodec("UTF-8");
  s << doc;
  file.close();
  core->backupFile(filename);

  // Sauvegarde les paramètres du projet.
  if (!saveSettings())
    return false;

  setModified(false);
  return true;
}

void CYProject::save(QDomDocument &doc, QDomElement &el)
{
  doc.appendChild(el);
  el.setAttribute("Title", title());
  el.setAttribute("Dummy", mDummy);
  el.setAttribute("NumTest", mNumTest);
  el.setAttribute("TestDirPath", mTestDirPath);
  el.setAttribute("EnableRestart", mEnableRestart);
  el.setAttribute("Sent", mSent);
  el.setAttribute("HistorySettings", core->historyProject()->fileName());
}

void CYProject::resetSettings()
{
  foreach (CYDB *db, dbList)
    db->designer();
}

void CYProject::loadSettings()
{
  foreach (CYDB *db, dbList)
    db->load();
  update();
}

bool CYProject::saveSettings()
{
  foreach (CYDB *db, dbList)
    if (db->save()!=1)
      return false;
  return true;
}

void CYProject::send()
{
  foreach (CYDB *db, dbList)
    db->writeData();
  setSent(true);
}

void CYProject::close()
{
//   save();
}

void CYProject::setModified()
{
  mModified = true;
  emit modifie(true);
}

void CYProject::setModified(bool val)
{
  mModified = val;
  emit modifie(val);
}

void CYProject::setProgramState(QString state)
{
  mProgramState = state;
  emit programStateChanged();
}

void CYProject::copy(CYProject *prj)
{
  QString title = mTitle;
  int numTest   = mNumTest;
  bool enableRestart = mEnableRestart;
  QString testDirPath = mTestDirPath;
  bool sent = mSent;

  QFileInfo saveFileInfo = mFileInfo;
  mFileInfo = prj->fileInfo();
  setProjectEnvironment(mFileInfo.filePath());
  CYProject::load();

  mFileInfo = saveFileInfo;
  setProjectEnvironment(mFileInfo.filePath());
  mTitle  = title;
  mNumTest = numTest;
  mEnableRestart = enableRestart;
  mTestDirPath = testDirPath;
  mSent = sent;
  core->historyProject()->setFileName("");
}

void CYProject::clean()
{
}


/*! Autorise ou non selon \a val la reprise d'essai.
    \fn CYProject::setEnableRestart(bool val)
 */
void CYProject::setEnableRestart(bool val)
{
  mEnableRestart = val;
}


/*! @return \a true si le redémarrage d'essai est autorisé.
    \fn CYProject::enableRestart()
 */
bool CYProject::enableRestart()
{
  return mEnableRestart;
}

bool CYProject::sent()
{
  return mSent;
}

void CYProject::setSent(bool val)
{
  mSent = val;
  save();
}


/*!
    \fn CYProject::setDesignerValue()
 */
void CYProject::setDesignerValue()
{
}


/*! Ajoute une acquisition au projet
    @param fileName nom du fichier de configuration sans extension (.cyacq est automatiquement mis)
    \fn CYProject::addAcquisition(CYAcquisition *acquisition, QString fileName)
 */
void CYProject::addAcquisition(CYAcquisition *acquisition, QString fileName)
{
  if (!acquisition)
  {
    CYMESSAGE;
    return;
  }
  acquisition->setSetupFileName(dirPath()+"/" +fileName +".cyacq");
  mAcquisitions.insert(acquisition->objectName(), acquisition);
}


/*! @return l'acquisition ayant pour nom \a name.
    \fn CYProject::acquisition(QString name)
 */
CYAcquisition * CYProject::acquisition(QString name)
{
  return mAcquisitions.value(name);
}


/*! Efface tous les paramètres des acquisitions du projet.
    \fn CYProject::copyAcquisitionsSetup()
 */
void CYProject::clearAcquisitionsSetup()
{
  foreach (CYAcquisition *acquisition, mAcquisitions)
    acquisition->clearSetup();
}

/*! Copie tous les paramètres des acquisitions du projet \a from.
    \fn CYProject::copyAcquisitionsSetup(CYProject *from)
 */
void CYProject::copyAcquisitionsSetup(CYProject *from)
{
  foreach (CYAcquisition *acquisition, mAcquisitions)
  {
    QString setupFileName = acquisition->setupFileName().section("/", -1);
    acquisition->setSetupFileName(from->dirPath()+"/"+setupFileName);
    uint fileNum = acquisition->fileNum();
    acquisition->loadSetup();
    acquisition->setFileNum(fileNum);
    acquisition->setSetupFileName(dirPath()+"/"+setupFileName);
  }
}

/*! Sauvegarde tous les paramètres des acquisitions du projet.
    \fn CYProject::saveAcquisitionsSetup()
 */
void CYProject::saveAcquisitionsSetup()
{
  foreach (CYAcquisition *acquisition, mAcquisitions)
    acquisition->saveSetup();
}

/*! Charge tous les paramètres des acquisitions du projet.
    \fn CYProject::loadAcquisitionsSetup()
 */
void CYProject::loadAcquisitionsSetup()
{
  foreach (CYAcquisition *acquisition, mAcquisitions)
    acquisition->loadSetup();
}

QString CYProject::baseDirectory()
{
  QDir dir(dirPath());
  if (core->projectSetpointsProtected())
    dir.cdUp();
  return dir.dirName();
}

CYData *CYProject::findData(const QString &name, bool fatal, bool notify)
{
  QString dataName = name;
  dataName.prepend(mPrefixDataName);
  dataName.append(mSuffixDataName);
  return core->findData(dataName, fatal, notify);
}

