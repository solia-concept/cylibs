//
// C++ Implementation: cyprojecteditor
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyprojecteditor.h"

// QT
#include <QFileDialog>
// CYLIBS
#include <cycore.h>
#include <cyproject.h>
#include <cyeventsmanager.h>
#include <cyflag.h>
#include "cymessagebox.h"

CYProjectEditor::CYProjectEditor(const QString &name, int index, const QString &label)
  : CYWin(0, name, index, label)
{
  mProject = 0;
  mInvalidView = false;
  mFollowExecutionAction = 0;
  mErrorBox=false;
  mErrorMsg="";

  QTimer *timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(showErrorBox()));
  timer->start(1000);

  connect( this, SIGNAL( quitPanel() ), core, SIGNAL( quitPanel() ) );
  core->setProjectEditor(this);
}

CYProjectEditor::~CYProjectEditor()
{
}

void CYProjectEditor::readProperties(QSettings* cfg)
{
  CYWin::readProperties(cfg);

  /* Nous pouvons ignorer 'isMaximized' parce que nous ne pouvons pas mettre
   * la fenêtre en taille maxi, nous sauvegardons alors à la place les coordonnées */
//   if (cfg->readBoolEntry("isMinimized") == true)
//     showMinimized();
//   else
//   {
//     int wx = cfg->readNumEntry("PosX", 100);
//     int wy = cfg->readNumEntry("PosY", 100);
//     int ww = cfg->readNumEntry("SizeX", 600);
//     int wh = cfg->readNumEntry("SizeY", 375);
//     setGeometry(wx, wy, ww, wh);
//   }

  QList<QVariant> varList= cfg->value("SplitterSizeList").toList();
  QList<int> sizes;
  foreach(QVariant var, varList)
    sizes << var.toInt();

  if (sizes.isEmpty())
  {
    // commence avec un rapport de 30/70
    sizes.append(30);
    sizes.append(70);
  }
//  splitter->setSizes(sizes);

  if (!cfg->value("StatusBarHidden", true).toBool())
  {
    mStatusBarTog->setChecked(true);
    showStatusBar();
  }

//  setMinimumSize(sizeHint());
}

void CYProjectEditor::saveProperties(QSettings* cfg)
{
  CYWin::saveProperties(cfg);

  cfg->setValue("PosX", x());
  cfg->setValue("PosY", y());
  cfg->setValue("SizeX", width());
  cfg->setValue("SizeY", height());
  cfg->setValue("isMinimized", isMinimized());
}

bool CYProjectEditor::openProject(CYProject *prj, bool edit, bool tmp, int index)
{
  if (!prj || (index==-1))
    return false;
  mProject = prj;
  mProject->setEditor(this);
  mProject->setReadOnly(!edit);

  connect(mProject, SIGNAL(modifie(bool)), SLOT(modifie(bool)));

  CYProject *oldProject = core->project(index);
  core->setProject(mProject, tmp, index);
  initView(mProject);
  if (oldProject && !tmp)
  {
    core->setCloseProjectEvent();
    core->setLoadProjectEvent();
    core->saveBenchSettings();
  }

  refreshCaption();
  return true;
}

bool CYProjectEditor::createProject()
{
  return createProject(0);
}

bool CYProjectEditor::createProject(int index)
{
  return createProject(this, index);
}

bool CYProjectEditor::createProject(QWidget *parent, int index)
{
  Q_UNUSED(parent)
  if (!isHidden())
    if (!quit())
      return false;

  mIndexStation = index;
  CYProject *oldPrj = core->project(index);
  if (wizardProjectExec(index))
  {
    if (openProject(wizardProject(), true, true))
    {
      CYProject *newPrj = core->project(index);
      if (oldPrj)
      {
        core->setProject(oldPrj, true, index);
        core->setCloseProjectEvent();
      }

      core->setProject(newPrj, false, index);
      mProject->setEnableRestart(false);
      mProject->save();

      core->setCreateProjectEvent();
      return true;
    }
  }
  return false;
}

bool CYProjectEditor::openProject()
{
  return openProject(0);
}

bool CYProjectEditor::openProject(int index)
{
  QFileDialog fd(0, tr("Select project file"), core->baseDirectory(), "*.cyprj");
  if (fd.exec())
  {
    QStringList files = fd.selectedFiles();
    QString selected;
    if (!files.isEmpty())
    {
        selected = files[0];

        if (openProject(selected, false, false, index))
        {
          mProject->setEnableRestart(false);
          //       mProject->setSent(false);
          mProject->save();

          return true;
        }
    }
    else
      openProject(index);
  }
  return false;
}

bool CYProjectEditor::editProject()
{
  return editProject(0);
}

bool CYProjectEditor::editProject(int index)
{
  mProject = (CYProject *)core->project(index);
  if (!mProject)
    return false;
  connect(mProject, SIGNAL(modifie(bool)), SLOT(modifie(bool)));

  if (mFollowExecutionAction)
    mFollowExecutionAction->setEnabled(false);

  core->setNotifyDatasLoading( false );
  bool res = openProject(mProject, true, true, index);
  core->setNotifyDatasLoading( true );
  return res;
}

bool CYProjectEditor::viewProject()
{
  return viewProject(0);
}

bool CYProjectEditor::viewProject(int index)
{
  mProject = (CYProject *)core->project(index);
  connect(mProject, SIGNAL(modifie(bool)), SLOT(modifie(bool)));

  if (mFollowExecutionAction)
    mFollowExecutionAction->setEnabled(true);

  core->setNotifyDatasLoading( false );
  bool res = openProject(mProject, false, true, index);
  core->setNotifyDatasLoading( true );
  return res;
}

bool CYProjectEditor::openProject(const QString &filename, bool edit, bool tmp, int index)
{
  QFile file(filename);
  QFileInfo fileInfo(filename);
  if (file.exists() && !fileInfo.isFile())
  {
    CYMessageBox::error(this, tr("The selected object is not a file !")+"\n"+tr("Select a project file ends with \".cyprj\"."), tr("Cannot find the project !"));
    return false;
  }
  else if (file.exists() && !filename.endsWith(".cyprj"))
  {
    CYMessageBox::error(this, tr("The selected file is not a project file !")+"\n"+tr("Select a project file ends with \".cyprj\"."), tr("Cannot find the project !"));
    return false;
  }
  else if (file.exists())
  {
    return openProject(newProject(0, "CYProject", filename, tmp, index), edit, tmp, index);
  }
  else
  {
    core->setProjectLack(true);
    int ret = CYMessageBox::warningYesNoCancel(this,
                                              tr("Do you want to load an existing project or create a new project ?"),
                                              tr("Cannot find the current project !"), tr("&Create"), tr("&Load"));
    if (ret == CYMessageBox::Yes)
    {
      if (!createProject())
      {
        return openProject(filename, false, false, index);
      }
      else
        return true;
    }
    else if (ret == CYMessageBox::No)
    {
      if (!openProject(index))
        return openProject(filename, false);
      else
        return true;
    }
    else if ( !core->project(index) )
      emit quitPanel();
  }
  return false;
}

bool CYProjectEditor::saveProject()
{
  return saveProject(0);
}

bool CYProjectEditor::saveProject(int index)
{  
  if (mProject->readOnly())
  {
    CYMessageBox::sorry(this, tr("The project cannot be saved because the editor has been open in read only mode!"));
    return false;
  }

  emit applyView();
  if (mInvalidView)
    return false;

  if (mProject->enableRestart())
  {
    if (CYMessageBox::Continue !=
        CYMessageBox::warningContinueCancel(this, "<p align=""center"">"+tr("If you save this project the restart test will be forbidden !")+"</p>"))
      return false;
  }
  mProject->setEnableRestart(false);

  if (mProject->sent() && core->projectSetpointsProtected())
  {
    QDir dir(mProject->dirPath());
    dir.cdUp();
    if (!QFile::exists(dir.path()+"/project.cyprj")) // première sauvegarde du projet en mode protégé
      dir.setPath(mProject->dirPath());

    dir.setPath(dir.path()+"/"+core->projectSetpointsProtectedDir());
    core->mkdir(dir.path());

    mProject->setNumTest(0);
    mProject->setTestDirPath(0);

    QString filename(dir.path() + "/" + mProject->fileInfo().fileName());
    mProject->setProjectEnvironment(filename);
    core->setProject(mProject, false, index);
    core->saveBenchSettings();
  }
  mProject->setSent(false);
  mProject->save();
  core->setSaveProjectEvent();
  return true;
}

bool CYProjectEditor::quit()
{
  if (mProject)
  {
    if (mProject->CYProject::isModified())
    {
      QString msg = QString(tr("The project was modified.\n"
                                 "Do you want to save your changes?"));
      int res = CYMessageBox::warningYesNoCancel(this, msg, tr("Unsaved changes"));

      if (res == CYMessageBox::Cancel)
        return false;
      else if (res == CYMessageBox::Yes)
      {
        if (!saveProject())
          return false;
      }
      else if (res == CYMessageBox::No)
        mProject->load();
    }
  }
  close();
  return true;
}

void CYProjectEditor::modifie(bool val)
{
  if (val)
    stateChanged("modifie");
  else
    stateChanged("save");
  if (action("save"))
    action("save")->setEnabled(val);
  refreshCaption();
}

void CYProjectEditor::refreshCaption()
{
  QString title;
  if (mProject)
  {
    if (mProject->readOnly())
    {
      if (mFollowExecutionAction && mFollowExecutionAction->isChecked())
        title = tr("View: %1").arg(mProject->fileName())+tr(" (Execution following ON)");
      else
        title = tr("View: %1").arg(mProject->fileName());
    }
    else if (!mProject->CYProject::isModified())
      title = tr("Edit: %1").arg(mProject->fileName());
    else
      title = tr("Edit: %1 (modified)").arg(mProject->fileName());
  }
  if (!mProject->label().isEmpty())
    title.prepend(QString("%1 :").arg(mProject->label()));
  setWindowTitle(title);
}

bool CYProjectEditor::queryClose()
{
  if (quit())
    return CYWin::queryClose();
  else
    return false;
}

void CYProjectEditor::copy()
{
  emit copyView();
}

void CYProjectEditor::paste()
{
  emit pasteView();
}

void CYProjectEditor::enableCopy(bool state)
{
  if (action("copy"))
    action("copy")->setEnabled(state);
}

void CYProjectEditor::enablePaste(bool state)
{
  if (action("paste"))
    action("paste")->setEnabled(state);
}

void CYProjectEditor::showErrorBox()
{
  if (!mErrorMsg.isEmpty() && !mErrorBox)
  {
    mErrorBox=true;
    CYMessageBox::error(this, mErrorMsg, tr("Project error"));
    mErrorMsg="";
    mErrorBox=false;
  }
}
