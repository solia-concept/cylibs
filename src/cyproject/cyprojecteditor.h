//
// C++ Interface: cyprojecteditor
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYPROJECTEDITOR_H
#define CYPROJECTEDITOR_H

// QT
#include <qwidget.h>
// CYLIBS
#include <cywin.h>

class CYProject;

/** CYProjectEditor est l'outil d'édition de projet d'essai.
  * @see CYProject.
  * @short Editeur de projet d'essai.
  * @author Gérald LE CLEACH
  */

class CYProjectEditor : public CYWin
{
  Q_OBJECT
public:
    /** Construction de l'éditeur de projet d'essai.
      * @param name     Nom de l'éditeur.
      * @param index    Index de la fenêtre.
      * @param label    Libellé de la fenêtre.*/
    CYProjectEditor(const QString &name, int index, const QString &label);
  
    ~CYProjectEditor();
  
    /** @return le projet. */
    virtual CYProject *project() { return mProject; }
  
    /** Sauvegarde les propriétes de l'éditeur. */
    virtual void saveProperties(QSettings *);
    /** Charge les propriétes de l'éditeur. */
    virtual void readProperties(QSettings *);
    /** Précède la fermeture de l'éditeur. */
    virtual bool queryClose();
    /** @return \a false si l'assistant de création de projet a reçu l'ordre d'annuler la création de projet. */
    virtual bool wizardProjectExec(int index=0)  = 0;
    /** @return le projet généré par l'assistant de création de projet. */
    virtual CYProject* wizardProject() = 0; 
    /** @return  \a true si la vue est invalide. */
    bool invalidView() { return mInvalidView; }

public slots: // Public slots
    /** Créer un nouveau projet avec l'assistant de création de projet. */
    virtual bool createProject();
    /** Créer un nouveau projet avec l'assistant de création de projet.
      * @param index  Index de projet (0 si un seul project actif à la fois).*/
    virtual bool createProject(int index);
    /** Créer un nouveau projet avec l'assistant de création de projet.
      * @param parent Widget appelant.
      * @param index  Index de projet (0 si un seul project actif à la fois).*/
    virtual bool createProject(QWidget *parent, int index=0);
    /** Edite le projet en cours. */
    virtual bool editProject();
    /** Edite le projet en cours.
      * @param index  Index de projet (0 si un seul project actif à la fois). */
    virtual bool editProject(int index);
    /** Visualise le projet en cours. */
    virtual bool viewProject();
    /** Visualise le projet en cours.
      * @param index  Index de projet (0 si un seul project actif à la fois). */
    virtual bool viewProject(int index);
    /** Ouvre un projet.
      * @param edit   Si false la modification est impossible.
      * @param tmp    Chargement temporaire (inutile de garder une trace de ce chargement).
      * @param index  Index de projet (0 si un seul project actif à la fois) */
    virtual bool openProject(CYProject *prj, bool edit = true, bool tmp=true, int index=0);
    /** Ouvre un projet à partir d'une boîte de sélection du fichier projet. */
    virtual bool openProject();
    /** Ouvre un projet à partir d'une boîte de sélection du fichier projet.
      * @param index  Index de projet (0 si un seul project actif à la fois). */
    virtual bool openProject(int index);
    /** Ouvre un projet ayant pour fichier projet \a filename.
      * @param edit   Si false la modification est impossible.
      * @param tmp    Chargement temporaire (inutile de garder une trace de ce chargement)
      * @param index  Index de projet (0 si un seul project actif à la fois) */
    virtual bool openProject(const QString &filename, bool edit = true, bool tmp=true, int index=0);
    /** Sauvegarde le projet. */
    virtual bool saveProject();
    /** Sauvegarde le projet.
      * @param index  Index de projet (0 si un seul project actif à la fois). */
    virtual bool saveProject(int index);

    /** Demande la fermeture de l'éditeur de projet.
      * @return \a true si la demande est validée. */
    virtual bool quit();
  
    /** Traîtement lorsque le projet vient d'être modifié si \a val vaut \a true
      * ou lorsque le projet vient d'être enregitsré si \a val vaut \a false.*/
    virtual void modifie(bool val);
    /** Rafraîchit le titre de la fenêtre. */
    virtual void refreshCaption();
    /** Autorise ou non la copie de paramètres. */
    virtual void enableCopy(bool state);
    /** Autorise ou non le collage de paramètres. */
    virtual void enablePaste(bool state);
    /** Copie de paramètres d'un élément du programe. */
    virtual void copy();
    /** Colle les paramètres copiés d'un élément du programe dans un autre du même type. */
    virtual void paste();
    /** Saisir \a true pour rendre la vue invalide. */
    void setInvalidView(bool val) { mInvalidView = val; }

    /** Saisie le message d'erreur qui doit apparaître dans une boîte surgissante.
        L'erreur est automatiquement supprimée une fois cette boîte affichée.
      * @param msg Message d'erreur à afficher. */
    virtual void setErrorMsg(QString msg) { mErrorMsg=msg; }

protected slots: // Protected slots
    /** Affiche une boîte de message d'erreur si \a mErrorMsg n'est pas vide. */
    virtual void showErrorBox();

signals: // Signals
    /** Donne l'ordre d'une copie à la vue en cours. */
    void copyView();
    /** Donne l'ordre d'un collage à la vue en cours. */
    void pasteView();
    /** Donne l'ordre d'appliquer la vue courante. */
    void applyView();
    /** Emis suite à un refus de créer un projet lors d'un premier démarrage de CYLIX. */
    void quitPanel();

protected: // Proteted methods
    /** Initialise la vue centrale selon le projet \a prj. */
    virtual void initView(CYProject *prj) = 0;
    /** Appel au constructeur de projet.
      * @param parent   Parent.
      * @param name     Nom du projet.
      * @param filename Nom du fichier projet.
      * @param tmp      création temporaire (pas de nouveau répertoire).
      * @param index    Index de projet (0 si un seul project actif à la fois).*/
    virtual CYProject* newProject(QObject *parent, const QString &name, QString filename, bool tmp=false, int index=0) = 0;

protected:
    /** Projet édité. */
    CYProject  *mProject;
    CYAction   *mFollowExecutionAction;
    /** Vue invalide. */
    bool mInvalidView;

    QString mErrorMsg;
    bool    mErrorBox;

    int mIndexStation;
};

#endif
