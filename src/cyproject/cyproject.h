/***************************************************************************
                          cyproject.h  -  description
                             -------------------
    begin                : ven nov 21 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYPROJECT_H
#define CYPROJECT_H

// QT
#include <QWidget>
#include <QFile>
#include <QHash>
#include <QStringList>
#include <QDomElement>
#include <QFileInfo>
#include <QDir>

class CYCore;
class CYDB;
class CYEventsReport;
class CYAcquisition;
class CYData;

/**
  Un projet regroupe les consignes, descriptions et résultats d'essai. Afin
  de sauvegarder ceci une arborescence de projet est nécessaire.

  \par Arborescence de projet:
  <dl><dt>+ Triangle (Répertoire projet)</dt>
      <dt><dl>
          <dt>+ 050915_105950 (Sous-répertoire projet)</dt>
          <dt><dl>
              <dt>+ Test_1  (Répertoire d'essai)</dt>
              <dt><dl>
                  <dt>+ acquisition (Répertoire d'acquisition)</dt>
                  <dt><dl>
                      <dt>+ fast (Répertoire d'acquisition rapide)</dt>
                      <dt><dl>
                          <dt>- acquisition_*.csv (Fichiers d'acquisition rapide)</dt>
                      </dl></dt>
                      <dt>+ slow (Répertoire d'acquisition lente)</dt>
                  </dl></dt>
              </dl></dt>
          </dl></dt>
      </dl></dt>
  </dl>

<dl><dt>+ Triangle (Répertoire projet)</dt>
<dd><dl><dt>+ 050915_105950 (Sous-répertoire projet)</dt></dl></dd>
<dd><dl><dt>+ Test_1  (Répertoire d'essai)<dt></dl></dd>
<dd><dl>+ Test_1  (Répertoire d'essai)</dl></dd></dl></dd>
<dd><dl>+ acquisition (Répertoire d'acquisition)</dl></dd></dl></dd></dl></dd>
<dd><dl>+ fast (Répertoire d'acquisition rapide)</dl></dd></dl></dd></dl></dd></dl></dd>
<dd><dl>- acquisition_*.csv (Fichiers d'acquisition rapide)</dl></dd></dl></dd></dl></dd></dl></dd></dl></dd>
<dd><dl>+ slow (Répertoire d'acquisition lente)</dl></dd></dl></dd></dl></dd></dl></dd>
<dd><dt>- acquisition_*.csv (Fichier d'acquisition lente)</dt></dd></dt></dd></dt></dd>
<dd><dt>+ post-mortem (Répertoire du post-mortem)
<dd><dt>+ fast (Répertoire du post-mortem rapide)
<dd><dt>+ archives (Répertoire d'archivage du post-mortem rapide)
<dd><dt>- fault_PMF_*.csv (Fichiers d'archivage du post-mortem rapide suite à un défaut)</dt></dd>
<dd><dt>- operator_PMF_*.csv (Fichiers d'archivage du post-mortem rapide suite à une demante opérateur)</dt></dd></dt></dd>
<dd><dt>- post-mortem_*.csv (Fichiers du post-mortem rapide)</dt></dd></dt></dd>
<dd><dt>+ slow (Répertoire du post-mortem lente)
<dd><dt>+ archives (Répertoire d'archivage du post-mortem lent)
<dd><dt>- fault_PMS_*.csv (Fichiers d'archivage du post-mortem lent suite à un défaut)</dt></dd>
<dd><dt>- operator_PMS_*.csv (Fichiers d'archivage du post-mortem lent suite à une demante opérateur)</dt></dd></dt></dd>
<dd><dt>- post-mortem_*.csv (Fichiers du post-mortem lent)</dt></dd></dt></dd></dt></dd>
<dd><dt>- description.cydata (Fichier de description)</dt></dd>
<dd><dt>- events_*.cyevt (Fichiers d'évènements)</dt></dd>
<dd><dt>- *.cydata (Fichiers de consignes)</dt></dd></dt></dd>
<dd><dt>+ Test_2  (Répertoire d'essai)
<dd><dt>...</dt></dd></dt></dd>
<dd><dt>+ Test  (Répertoire d'essai)
<dd><dt>...</dt></dd></dt></dd>
<dd><dt>+ 8 pièces  (Répertoire d'essai)
<dd><dt>...</dt></dd></dt></dd></dt></dd>
<dd><dt>+ 050918_141532 (Sous-répertoire projet)
<dd><dt>...</dt></dd></dt></dd>
</dl>

    Un projet est généré par l'assistant de création de projet qui le sauvegarde automatiquement
  dans un sous-répertoire du répertoire projet ayant pour nom la date et l'heure courante.
  L'éditeur de projet s'ouvre alors afin de compléter le projet. Tant que le projet n'est pas
  lancé sur la machine, chaque enregistrement à partir de l'éditeur de projet se fait dans ce
  sous-répertoire. Une fois qu'un démarrage d'essai a lieu la sauvegarde du projet à partir de
  l'éditeur dans ce sous-répertoire n'est plus possible afin de conserver une trace de l'essai
  qui a déjà été réalisé. La sauvegarde se fait alors automatiquement dans un nouveau sous-répertoire
  et tout enregistrement se fait par la suite dans celui-ci jusqu'au nouveau démarrage d'essai.
  Le changement de sous répertoire induit une impossibilité de reprise d'essai puisque ce changement
  a eu lieu après modification de consignes.

    Le répertoire d'essai contient la description de l'essai saisie au démarrage ou la reprise d'essai
  ainsi que tous les résultats d'essai tels que les acquisitions, les fichies d'évènements, les PV d'essai...
  Lors d'une reprise d'essai il est possible de créer ou non un nouveau répertoire d'essai. Ceci permet de
  lancer une campagne d'essais avec les mêmes consignes.
  @short Projet d'essai.
  @author Gérald LE CLEACH
  */

class CYProject : public QObject
{
  Q_OBJECT
public:
  /** Construction d'un projet d'essai.
    * @param parent   Parent.
    * @param name     Nom du projet.
    * @param filename Nom du fichier projet.
    * @param tmp      création temporaire (pas de nouveau répertoire).
    * @param index    Index de projet (0 si un seul project actif à la fois). */
  CYProject(QObject *parent, const QString &name, QString filename, bool tmp=false, int index=0);
  ~CYProject();

  /** @return l'index de projet (0 si un seul project actif à la fois)*/
  int index() { return mIndex; }

  /** Initialise le projet.
    * param tmp Chargement temporaire (inutile de garder une trace de ce chargement). */
  virtual void initProject(bool tmp=true);

  /** Initialise le projet.
    * param tmp Chargement temporaire (inutile de garder une trace de ce chargement). */
  virtual void initProject2(bool &tmp);

  /** @return \a true Si l'initialisation du répertoire d'essai s'est fait jusqu'au bout. */
  virtual bool initTestDir();

  /** Fixe l'environnement du projet.
    * @param fileName Nom complet du fichier projet. */
  virtual void setProjectEnvironment(const QString &fileName);

  /** @return l'étiquette du projet (à surchager en cas de projets multiples ex(tr("Station%1").arg(mIndex)) */
  virtual QString label() { return 0; }

  /** @return le titre du projet. */
  QString title() { return mTitle; }
  /** Saisie le titre du projet. */
  void setTitle(QString title) { mTitle = title; }
  /** @return le répertoire projet. */
  QString dirPath() const { return mFileInfo.path(); }
  /** @return le nom du fichier projet. */
  QString fileName() const { return mFileInfo.filePath(); }
  /** @return les infos du fichier projet. */
  QFileInfo fileInfo() const { return mFileInfo; }

  /** Saisie l'éditeur du projet. */
  void setEditor(QWidget *editor) { mEditor = editor; }
  /** @return l'éditeur du projet. */
  QWidget *editor() { return mEditor; }

  /** @return l'état de modification du projet. */
  bool isModified() const { return mModified; }
  /** @return \a true si le projet est vierge. */
  bool isDummy() { return mDummy; }

  /** @return \a true si le projet est en lecture seule. */
  bool readOnly() { return mReadOnly; }
  /** Place le projet en lecture seule. */
  void setReadOnly(bool state) { mReadOnly = state; }

  /** @return le journal d'évènements. */
  CYEventsReport *eventsReport() { return mEventsReport; }
  /** Saisie le journal d'évènements. */
  void setEventsReport(CYEventsReport *events) { mEventsReport = events; }

  /** @return le numéro de l'essai en cours. */
  virtual int numTest() { return mNumTest; }
  /** Saisie le numéro de l'essai en cours. */
  virtual void setNumTest(int num) { mNumTest = num; }
  /** @return le chemin du répertoire d'essai courant. */
  virtual QString testDirPath() { return mTestDirPath; }
  /** Saisie le chemin du répertoire d'essai courant. */
  virtual void setTestDirPath( QString path ) { mTestDirPath = path; }
  /** @return le(s) répertoire(s) intercalaire(s) entre le répertoire projet et le répertoire répertoire d'essai courant. */
  virtual QString intercalTestDir() { return mIntercalTestDir; }
  /** Saisie le(s) répertoire(s) intercalaire(s) entre le répertoire projet et le répertoire répertoire d'essai courant. */
  virtual void setIntercalTestDir( QString path ) { mIntercalTestDir = path; }
  /** @return le nom du répertoire d'essai courant. */
  virtual QString testDirName() { return mTestDirName; }
  /** Saisie le nom du répertoire d'essai courant. */
  virtual void setTestDirName( QString name ) { mTestDirName = name; }
  /** @return \a true si le numéro de test est ajouté à la fin du nom du répertoire d'essai en création manuel. */
  virtual bool manuWithNumTest() { return mManuWithNumTest; }
  /** Saisir \a true si le numéro de test doit être ajouté à la fin du nom du répertoire d'essai en création manuel. */
  virtual void setManuWithNumTest( bool withNumTest ) { mManuWithNumTest = withNumTest; }

  /** @return la durée en secondes du programme principal. */
  virtual uint programTime() { return 0; }

  /** @return \a true si le projet a déjà été envoyé au RN(s). */
  virtual bool sent();
  /** Saisir \a true si le projet a déjà été envoyé au RN(s). */
  virtual void setSent(bool val);

  virtual bool enableRestart();
  void setDesignerValue();
  virtual void addAcquisition(CYAcquisition *acquisition, QString fileName);
  virtual CYAcquisition * acquisition(QString name);
  virtual void clearAcquisitionsSetup();
  virtual void copyAcquisitionsSetup(CYProject *from);
  virtual void saveAcquisitionsSetup();
  virtual void loadAcquisitionsSetup();

  /** @return le répertoire de base du projet. Si les consignes du projet sont protégées, la fonction retourne le répertoire parent au répertoire créé automatiquement avec la date et l'heure et contenant les fichiers de paramètrage. */
  virtual QString baseDirectory();

  /** @return la donnée ayant pour finissant par \a name avec le préfix de l'élément, ou 0 si la recheche échoue.
   @param fatal  Mettre à \a false pour ne pas quitter l'application.
   @param notify Mettre à \a false pour ne pas informer l'utilisateur que la donnée n'existe pas. */
  virtual CYData *findData(const QString &name, bool fatal = true, bool notify = true);

  /*! @return le préfix du nom des données ratachées et de leurs bases rattachées. */
  const QString prefixDataName() { return mPrefixDataName; }
  /*! @return le suffix du nom des données ratachées et de leurs bases rattachées. */
  const QString suffixDataName() { return mSuffixDataName; }

  /// Saisie \a true si des données du projet sont en cours d'édition et en attente d'application
  void setDataToApply(bool state) { mDataToApply = state; }
  /// @return \a true si des données du projet sont en cours d'édition et en attente d'application
  bool isDataToApply() { return mDataToApply; }

signals: // Signals
  /** Emis lorsque le projet vient d'être modifié si \a val vaut \a true
    * ou lorsque le projet vient d'être enregitsré si \a val vaut \a false.*/
  void modifie(bool val);
  /** Emis à chaque changement d'essai. */
  void testChanged(int index);
  /** Signale que le programme vient de changer d'état. */
  void programStateChanged();
  /** Emet la durée en secondes du programme principal à chaque changement de celle-ci. */
  void programTimeChanged(uint time);

public slots: // Public slots
  /** Place le projet comme modifié. */
  void setModified();
  /** Saisie l'état de modification du projet. */
  void setModified(bool val);

  /** Charge le projet à partir du fichier projet. */
  virtual bool load();
  /** Charge le projet à partir du fichier projet \a filename. */
  virtual bool load(const QString &filename);
  /** Charge le contenu du fichier projet. */
  virtual void load(QDomDocument &doc);
  /** Sauvegarde le projet dans le fichier projet. */
  virtual bool save();
  /** Sauvegarde le projet dans le fichier projet sans que l'utilisateur ne soit informé lors d'un problème d'accès au fichier. */
  virtual bool saveWithoutMessageBox();
  /** Sauvegarde le projet dans le fichier projet \a filename. */
  virtual bool save(const QString &filename);
  /** Sauvegarde le contenu du fichier projet. */
  virtual void save(QDomDocument &doc, QDomElement &el);
  /** Ferme le projet. */
  virtual void close();
  /** Applique les nouveaux paramètres du projet. */
  virtual void update() {}
  /** Envoie les paramètres utiles à l'essai. */
  virtual void send();

  /** Remise à zéro des paramètres du projet. */
  virtual void resetSettings();
  /** Charge les paramètres du projet. */
  virtual void loadSettings();
  /** Sauvegarde les paramètres du projet. */
  virtual bool saveSettings();
  /** Sauvegarde l'historique des paramètres.
   *  Permet de sauvegarder dans un fichier CSV l'évolution des paramètres. */
  virtual bool saveHistorySettings() { return false; }

  /** Copie d'un projet \a prj. */
  virtual void copy(CYProject *prj);

  /** @return l'état du programme. */
  QString programState() { return mProgramState; }
  /** Saisie l'état du programme. */
  virtual void setProgramState(QString state);

  /** Nettoyer le projet.
    * Utile après la création d'un nouveau projet
    * ou le chargement d'un autre projet. */
  virtual void clean();

  virtual void setEnableRestart(bool val);

public: // Public attributes
  /** Bases de données du projet. */
  QHash<QString, CYDB*> dbList;

protected: // Protected attributes
  /** Créé temporairement (pas de nouveau répertoire). */
  bool mTmp;
  /** Index de projet (0 si un seul project actif à la fois)*/
  int mIndex;
  /** Titre du projet. */
  QString mTitle;
  /** Info sur le fichier projet. */
  QFileInfo mFileInfo;
  /** Editeur du projet. */
  QWidget *mEditor;

  /** Numéro de l'essai en cours. */
  int mNumTest;
  /** Vaut \a true si le projet est vierge. */
  bool mDummy;

  /** Vaut \a true si le projet a été modifié depuis sa dernière sauvegarde. */
  bool mModified;
  /** Vaut \a true si le projet est en lecture seule. */
  bool mReadOnly;

  /** Journal d'évènements. */
  CYEventsReport *mEventsReport;
  /** Chemin du répertoire d'essai courant. */
  QString mTestDirPath;
  /** Répertoire(s) intercalaire(s) entre le répertoire projet et le répertoire répertoire d'essai courant. */
  QString mIntercalTestDir;
  /** Nom du répertoire d'essai courant. */
  QString mTestDirName;
  /** Vaut \a true si le numéro de test est ajouté à la fin du nom du répertoire d'essai en création manuel. */
  bool mManuWithNumTest;

  /** Etat du programme. */
  QString mProgramState;
  /** Désactive les boîtes de messagerie. */
  bool mWithoutMessageBox;

  /** Vaut \a true si le projet a déjà été envoyé au RN(s). */
  bool mSent;

  bool mEnableRestart;

  /** Liste des acquisitions. */
  QHash<QString, CYAcquisition*> mAcquisitions;

  /** Nom du répertoire (date et heure) de protection des paramètres */
  QString mProtectDir;

  QString mPrefixDataName;
  QString mSuffixDataName;

  bool mDataToApply;
};

#endif
