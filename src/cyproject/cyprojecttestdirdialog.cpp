#include "cyprojecttestdirdialog.h"
#include "ui_cyprojecttestdirdialog.h"

// QT
#include <QFileDialog>
// CYLIBS
#include "cycore.h"
#include "cyproject.h"
#include "cymessagebox.h"


CYProjectTestDirDialog::CYProjectTestDirDialog(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYProjectTestDirDialog)
{
  ui->setupUi(this);
  connect(ui->buttonDir, SIGNAL(clicked()), this, SLOT(load()));
}

CYProjectTestDirDialog::~CYProjectTestDirDialog()
{
  delete ui;
}

void CYProjectTestDirDialog::init( CYProject *project )
{
  mProject = project;
  mPath = mProject->dirPath();

  QString intercal = mProject->intercalTestDir();
  if (!intercal.isEmpty())
    mPath=mPath+"/"+intercal;

  mDirName = mProject->testDirName();
  mWithNumTest = mProject->manuWithNumTest();
  ui->lineEdit->setText( mDirName );
  if ( mProject->testDirPath().isEmpty() )
  {
    ui->checkBoxNew->setChecked( true );
    ui->buttonGroup->setEnabled( true );
    ui->buttonDir->setEnabled( true );
    ui->checkBoxNew->hide();
  }
  else
  {
    ui->buttonGroup->setEnabled( core->newTestDir() );
    ui->buttonDir->setEnabled( !core->newTestDir() );
    ui->checkBoxNew->setChecked( core->newTestDir() );
  }
  ui->checkBox->setChecked( mWithNumTest );
  ui->checkBoxAuto->setChecked( core->testDirAuto() );
  refresh();
}


void CYProjectTestDirDialog::refresh()
{
  if ( !ui->checkBoxNew->isChecked())
  {
    mTestDirPath = (mExistingDir.isEmpty()) ? mProject->testDirPath() : mExistingDir;
  }
  else
  {
    mNumTest = mProject->numTest()+1;

    if ( ui->checkBoxAuto->isChecked() || ui->lineEdit->text().isEmpty() )
    {
      mTestDirPath = mPath+"/test";
    }
    else
    {
      mTestDirPath = mPath+"/"+ui->lineEdit->text();
    }
    if (ui->checkBoxAuto->isChecked() || ui->checkBox->isChecked())
    {
      mTestDirPath.append( QString("_%1").arg( mNumTest ) );
    }
  }
  mTestDirPath.replace("//", "/");
  ui->textLabel->setText( mTestDirPath );
}

void CYProjectTestDirDialog::accept()
{
  core->setNewTestDir( ui->checkBoxNew->isChecked() );
  if ( ui->checkBoxNew->isChecked() || !ui->checkBoxNew->isVisible() )
  {
    if (mTestDirPath==mProject->testDirPath())
    {
      CYMessageBox::sorry(this, tr("If you want to change the test directory, you must change its name !"));
      return;
    }
    QDir dir;
    if (dir.exists(mTestDirPath))
    {
      int res = CYMessageBox::warningYesNo(this, tr("This directory already exists !\n"
                                                     "It may contain results of an other test.\n"
                                                     "Do you really want to use this directory ?"));
      if (res == CYMessageBox::No)
        return;
    }

    mProject->setTestDirPath( mTestDirPath );
    core->setTestDirAuto( ui->checkBoxAuto->isChecked() );
    mProject->setNumTest( mNumTest );
    mProject->setManuWithNumTest( ui->checkBox->isChecked() );
    mProject->setTestDirName( ui->lineEdit->text() );
  }
  else if (!mTestDirPath.isEmpty() && (mTestDirPath!=mProject->testDirPath()))
  {
    mProject->setTestDirPath( mTestDirPath );
    core->setTestDirAuto( false );
    mProject->setNumTest( mProject->numTest()+1 );
    mProject->setManuWithNumTest( false );
    mProject->setTestDirName( mTestDirPath.section( '/', -1 ) );
  }
  CYDialog::accept();
}

void CYProjectTestDirDialog::load()
{
  QString path = QFileDialog::getExistingDirectory(this, tr("Choose another directory"), mTestDirPath);
  if (!path.isEmpty())
    mExistingDir=path;
  refresh();
}
