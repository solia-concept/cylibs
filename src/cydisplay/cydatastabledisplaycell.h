/***************************************************************************
                          cydatastabledisplaycell.h  -  description
                             -------------------
    begin                : mer jan 26 2005
    copyright            : (C) 2005 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDATASTABLEDISPLAYCELL_H
#define CYDATASTABLEDISPLAYCELL_H

// QT
#include <qwidget.h>
#include <qlayout.h>
#include <QDomElement>
// CYLIBS
#include "cydisplaycell.h"
#include "cymessagebox.h"

class CYDatasTable;
class CYDatasTableRow;
class CYDatasTableCol;
class CYDisplay;

/** @short Cellule d'une table de données réservée au afficheurs CYDisplay.
  *@author LE CLÉACH Gérald
  */

class CYDatasTableDisplayCell : public CYDisplayCell
{
   Q_OBJECT
public: 
  CYDatasTableDisplayCell(CYDatasTable *table, CYDatasTableRow *row, CYDatasTableCol*col, QWidget *parent=0, const QString &name=0);
  ~CYDatasTableDisplayCell();

  /** Place un afficheur simple dans la cellule.
    * @param name   Nom de la donnée / ou du groupe de données.
    * @param input  Vaut \a true si l'afficheur doit se comporter comme un objet de saisie. */
  void setDisplaySimple(const QString& name, bool input=false);
  /** Place l'afficheur dans la cellule.
    * @param  name  Nom de la donnée. */
  void setDisplay(const QString& name);
  /** Place l'afficheur dans la cellule. */
  void setDisplay(CYDisplay *display=0);

  /** Construit un afficheur dans la cellule à partir d'un élément DOM. */
  bool createDisplayFromDOM(QDomElement &el);
  /** Ajoute un afficheur dans la cellule à partir d'un élément DOM. */
  bool addDisplayToDOM(QDomDocument &doc, QDomElement &element, bool save=true);

  /** @return la table de données contenant cette cellule. */
  CYDatasTable *table() { return mTable; }
  /** @return la ligne de la table de données contenant cette cellule. */
  CYDatasTableRow *tableRow() { return mTableRow; }
  /** Colonne de la table de données contenant cette cellule. */
  CYDatasTableCol *tableCol() { return mTableCol; }

  /** @return le texte contenu par la cellule lorsque c'est possible.
      Les cellules ne pouvant pas retourner de texte simple tels que par
      exemple celles contenant un oscilloscope, la valeur \a ok est alors à \a false. */
  virtual QString text(bool &ok);

protected: // Protected methods
  virtual void focusInEvent(QFocusEvent * event);

private: // Private methods
  /** Initialise la cellule. */
  void init();

protected: // Protected attributes
  /** Table de données contenant cette cellule. */
  CYDatasTable *mTable;
  /** Ligne de la table de données contenant cette cellule. */
  CYDatasTableRow *mTableRow;
  /** Colonne de la table de données contenant cette cellule. */
  CYDatasTableCol *mTableCol;
};

#endif
