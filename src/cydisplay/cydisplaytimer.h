/***************************************************************************
                          cydisplaytimer.h  -  description
                             -------------------
    début                  : Sun Feb 9 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDISPLAYTIMER_H
#define CYDISPLAYTIMER_H

/**
 * Chaque objet qui affiche des CYDatas doivent hériter de cette
 * classe pour profiter du paramétrage avancé d'intervalle de mise à jour.
 * @author Gérald LE CLEACH.
 */

class CYDisplayTimer
{
public:
  /** Constructeur.*/
  CYDisplayTimer() { }
  /** Destructeur.*/
  virtual ~CYDisplayTimer() { }

  /** Fixe le nombre de secondes pour l'intervalle de rafraîchissement.*/
  void updateIntervalSec(int interval) { secUpdateInterval = interval; }
  /** Fixe le nombre de millisecondes pour l'intervalle de rafraîchissement.*/
  void updateIntervalMSec(int interval) { msecUpdateInterval = interval; }
  /** Récupère le nombre de secondes pour l'intervalle de rafraîchissement.*/
  int updateIntervalSec() { return secUpdateInterval; }
  /** Récupère le nombre de millisecondes pour l'intervalle de rafraîchissement.*/
  int updateIntervalMSec() { return msecUpdateInterval; }

private:
  /** Nombre de secondes pour l'intervalle de rafraîchissement.*/
  int secUpdateInterval;
  /** Nombre de millisecondes pour l'intervalle de rafraîchissement.*/
  int msecUpdateInterval;
};

#endif
