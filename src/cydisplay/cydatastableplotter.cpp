/***************************************************************************
                          cydatastableplotter.cpp  -  description
                             -------------------
    begin                : jeu nov 18 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydatastableplotter.h"

// QT
#include <QHeaderView>
#include <QStandardItem>
// CYLIBS
#include "cyflag.h"
#include "cyu32.h"
#include "cyf64.h"
#include "cyled.h"
#include "cyadc16.h"
#include "cyadc32.h"
#include "cystring.h"
#include "cycs.h"
#include "cycc.h"
#include "cynumdisplay.h"
#include "cytextlabel.h"
#include "cytextinput.h"
#include "cyflaginput.h"
#include "cynuminput.h"
#include "cycheckbox.h"
#include "cydatastable.h"
#include "cydatastablecol.h"
#include "cydatastablerow.h"
#include "cydatastabledisplaycell.h"
#include "cyao.h"
#include "cydisplay.h"
#include "cyanalysesheet.h"


CYDatasTablePlotter::CYDatasTablePlotter(QWidget *parent, const QString &name, CYDatasTable *display)
: QTableWidget(parent),
  mDisplay(display)
{
  setObjectName(name);
//  setColumnCount(0);

  mRowHeightChanging = false;
  setSelectionMode(QAbstractItemView::ExtendedSelection);
  setEditTriggers(QAbstractItemView::NoEditTriggers);
  verticalHeader()->setSectionsMovable(true);
  horizontalHeader()->setSectionsMovable(true);
  setSortingEnabled(true);

  connect(horizontalHeader(), SIGNAL(sectionMoved(int,int,int)), mDisplay, SLOT(swapColumns(int,int,int)));
  connect(horizontalHeader(), SIGNAL(sectionResized(int,int,int)), mDisplay, SLOT(memColumnWidth(int,int,int)));
  connect(horizontalHeader(), SIGNAL(sectionPressed(int)), this, SLOT(selectColumn(int)));
  connect(horizontalHeader(), SIGNAL(sortIndicatorChanged(int, Qt::SortOrder)),  mDisplay, SIGNAL(modifie()));

  connect(verticalHeader(), SIGNAL(sectionMoved(int,int,int)), mDisplay, SLOT(swapRows(int,int,int)));
  connect(verticalHeader(), SIGNAL(sectionPressed(int)), this, SLOT(selectRow(int)));
  connect(verticalHeader(), SIGNAL(sectionResized(int,int,int)), mDisplay, SLOT(memRowHeight(int,int,int)));

  connect(this, SIGNAL(showPopupMenu()), mDisplay, SLOT(showPopupMenu()));
}

CYDatasTablePlotter::~CYDatasTablePlotter()
{
}

void CYDatasTablePlotter::addColumn(CYDatasTableCol *dtc, bool cydisplay)
{
  int id = dtc->id;

  int nb=columnCount();
  if ( nb < id+1 )
    setColumnCount( id+1 );

  setColumnWidth(id, dtc->width());
  setHorizontalHeaderItem(id, new QTableWidgetItem(dtc->label()));

  if (dtc->shown())
    showColumn(id);
  else
    hideColumn(id);

  if (cydisplay)
  {
    QMapIterator<int, CYDatasTableRow *> ir(mDisplay->mRows);
    ir.toFront();
    while (ir.hasNext())
    {
      ir.next();
      CYDatasTableRow *dtr = ir.value();
      CYDatasTableDisplayCell *wdg = new CYDatasTableDisplayCell(mDisplay, dtr, dtc, this, "CYDatasTableDisplayCell");
      setCellWidget(dtr->pos(), dtc->id, wdg);
    }
  }
}

void CYDatasTablePlotter::addRow(CYDatasTableRow *dtr, bool loading)
{
  CYData *data = dtr->data;
  int row = dtr->pos();

  if (!columnCount())
  {
    QMapIterator<int, CYDatasTableCol *> ic(mDisplay->mCols);
    ic.toFront();
    while (ic.hasNext())
    {
      ic.next();
      CYDatasTableCol *dtc = ic.value();
      if (dtc)
      {
        addColumn(dtc, false);
        setHorizontalHeaderItem(dtc->id, dtc->header());
      }
    }
  }

  QMapIterator<int, CYDatasTableCol *> ic(mDisplay->mCols);
  ic.toFront();
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if (!dtc)
      continue;
    int col = dtc->id;
    switch ( dtc->type() )
    {
      case CYDatasTableCol::Id:
      {
        QString txt = QString("%1").arg(dtr->id);
        setItem(row, col, new QTableWidgetItem(txt));
        break;
      }
      case CYDatasTableCol::Value :
      {
        CYDatasTableDisplayCell *wdg = new CYDatasTableDisplayCell(mDisplay, dtr, dtc, this, "CYDatasTableDisplayCell");
//        wdg->installEventFilter(this);
        if (!loading)
          wdg->setDisplaySimple(data->objectName());
        setCellWidget(row, col, wdg);
        break;
      }
      case CYDatasTableCol::Forcing:
      {
        if (data->inherits("CYMeasure"))
        {
          CYMeasure *mes = (CYMeasure *)data;
          connect(this , SIGNAL(initForcingValues()), mes, SLOT(initForcingValue()));
          connect(dtr, SIGNAL(changingForcingCell(CYDatasTableRow* )), this, SLOT(changeForcingCell(CYDatasTableRow* )));
        }
        createForcingCell(dtr, col);
        break;
      }
      case CYDatasTableCol::Label:
      {
        QString txt = dtr->label;
        setItem(row, col, new QTableWidgetItem(txt));
        break;
      }
      case CYDatasTableCol::Group:
      {
        QString txt=data->group();
        setItem(row, col, new QTableWidgetItem(txt));
        break;
      }
      case CYDatasTableCol::Link:
      {
        setItem(row, col, new QTableWidgetItem(data->linksName()));
        break;
      }
      case CYDatasTableCol::Host:
      {
        setItem(row, col, new QTableWidgetItem(data->hostsName()));
        break;
      }
      case CYDatasTableCol::Name:
      {
        setItem(row, col, new QTableWidgetItem(data->objectName()));
        break;
      }
      case CYDatasTableCol::Help:
      {
        setItem(row, col, new QTableWidgetItem(data->help()));
        break;
      }
      case CYDatasTableCol::Phys:
      {
        setItem(row, col, new QTableWidgetItem(data->phys()));
        break;
      }
      case CYDatasTableCol::Elec:
      {
        setItem(row, col, new QTableWidgetItem(data->elec()));
        break;
      }
      case CYDatasTableCol::ADC:
      {
        if (data->adc16())
        {
          CYDatasTableDisplayCell *wdg = new CYDatasTableDisplayCell(mDisplay, dtr, dtc, this, QString("wdg_%1").arg(data->adc16()->objectName()));
          wdg->setDisplaySimple(data->adc16()->objectName());
          wdg->setMinimumWidth(0);
          setCellWidget(row, col, wdg);
        }
        if (data->adc32())
        {
          CYDatasTableDisplayCell *wdg = new CYDatasTableDisplayCell(mDisplay, dtr, dtc, this, QString("wdg_%1").arg(data->adc32()->objectName()));
          wdg->setDisplaySimple(data->adc32()->objectName());
          wdg->setMinimumWidth(0);
          setCellWidget(row, col, wdg);
        }
        break;
      }
      case CYDatasTableCol::Display:
      {
        CYDatasTableDisplayCell *wdg = new CYDatasTableDisplayCell(mDisplay, dtr, dtc, this, "CYDatasTableDisplayCell");
//        wdg->installEventFilter(this);
        setCellWidget(row, col, wdg);
        break;
      }
      case CYDatasTableCol::Reset:
      {
        if (data->resetData())
        {
          CYDatasTableDisplayCell *wdg = new CYDatasTableDisplayCell(mDisplay, dtr, dtc, this, "CYDatasTableDisplayCell");
//          wdg->installEventFilter(this);
          if (!loading)
            wdg->setDisplaySimple(data->resetData()->objectName(), true);
          setCellWidget(row, col, wdg);
          break;
        }
        break;
      }
      case CYDatasTableCol::Partial:
      {
        if (data->partialData())
        {
          CYDatasTableDisplayCell *wdg = new CYDatasTableDisplayCell(mDisplay, dtr, dtc, this, "CYDatasTableDisplayCell");
//          wdg->installEventFilter(this);
          if (!loading)
            wdg->setDisplaySimple(data->partialData()->objectName());
          setCellWidget(row, col, wdg);
          break;
        }
        break;
      }
      case CYDatasTableCol::Note:
      {
        if (data->noteData())
        {
          CYDatasTableDisplayCell *wdg = new CYDatasTableDisplayCell(mDisplay, dtr, dtc, this, "CYDatasTableDisplayCell");
//          wdg->installEventFilter(this);
          if (!loading)
            wdg->setDisplaySimple(data->noteData()->objectName(), true);
          setCellWidget(row, col, wdg);
          break;
        }
        break;
      }
      case CYDatasTableCol::Control:
      {
        if (data->inherits("CYMeasure"))
        {
          CYMeasure *mes = (CYMeasure *)data;
          if (mes->nbForcing()>1)
          {
            CYComboBox *wdg = new CYComboBox(this, QString("wdg_%1").arg(data->objectName()));
            wdg->setDataName(mes->ctrlForcing()->objectName().toUtf8());
            connect(wdg, SIGNAL(activated(int)), mes, SLOT( setCtrlForcing(int)));
            connect(wdg, SIGNAL(activated(int)), mDisplay, SIGNAL(modifie()));
            connect(mes, SIGNAL(ctrlForcing(int )), wdg, SLOT(setValue(int))); // v5.12.09
            connect(mes, SIGNAL(ctrlForcing(int )), dtr, SLOT(changeForcingCell()));
            setCellWidget(row, col, wdg);
          }
          else
          {
            QString *txt = mes->descForcing(0);
            if (txt)
              setItem(row, col, new QTableWidgetItem(*txt));
          }
          break;
        }
        break;
      }
      case CYDatasTableCol::Addr:
      {
        setItem(row, col, new QTableWidgetItem(data->addr()));
        break;
      }
      case CYDatasTableCol::Threshold:
      {
        if (data->thresholdData())
        {
          CYNumInput*wdg = new CYNumInput(this, QString("wdg_%1").arg(data->objectName()));
          wdg->setDataName(data->thresholdData()->dataName());
          wdg->setMinimumWidth(0);
          wdg->setAutoMinimumSize(false);
          setCellWidget(row, col, wdg);
        }
        break;
      }
     default: CYMESSAGETEXT(QString("%1").arg(dtc->type()));
    }
    if (data->mode() == Cy::Designer)
    {
      QWidget *wdg = cellWidget(row, col);
      if (wdg)
      {
        QFont font = wdg->font();
        font.setItalic(true);
        wdg->setFont(font);
      }
    }
    setRowHeight(row, dtr->height());
    if (dtr->shown())
      showRow(row);
    else
      hideRow(row);
  }
}

void CYDatasTablePlotter::createForcingCell(CYDatasTableRow *dtr, int col)
{
  CYData *data = dtr->data;
  int row = dtr->pos();

  if (data->dataForcing())
  {
    switch (data->dataForcing()->type())
    {
      case Cy::VFL    :
      case Cy::Bool   :
                      {
                        CYFlagInput *wdg = new CYFlagInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setReadOnly(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::Word   :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::VS8    :
                      {
                        if (data->dataForcing()->isFlag())
                        {
                          CYFlagInput *wdg = new CYFlagInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                          wdg->setDataName(data->dataForcing()->dataName());
                          wdg->setForcingName(data->dataForcing()->dataName());
                          wdg->setMinimumWidth(0);
                          wdg->setReadOnly(false);
                          setCellWidget(row, col, wdg);
                        }
                        else
                        {
                          CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                          wdg->setDataName(data->dataForcing()->dataName());
                          wdg->setForcingName(data->dataForcing()->dataName());
                          wdg->setMinimumWidth(0);
                          wdg->setAutoMinimumSize(false);
                          setCellWidget(row, col, wdg);
                        }
                        break;
                      }
      case Cy::VS16   :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::VS32   :
      case Cy::VS64   :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::VU8    :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::VU16   :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::VU32   :
      case Cy::VU64   :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::VF32   :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::VF64   :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::Time   :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::Sec    :
                      {
                        CYNumInput *wdg = new CYNumInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      case Cy::String :
                      {
                        CYTextInput *wdg = new CYTextInput(this, QString("wdg_%1").arg(data->dataForcing()->objectName()));
                        wdg->setDataName(data->dataForcing()->dataName());
                        wdg->setForcingName(data->dataForcing()->dataName());
                        wdg->setMinimumWidth(0);
                        wdg->setAutoMinimumSize(false);
                        wdg->setReadOnly(false);
                        setCellWidget(row, col, wdg);
                        break;
                      }
      default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+data->dataForcing()->objectName());
    }
  }
}

void CYDatasTablePlotter::changeForcingCell(CYDatasTableRow *dtr)
{
  int row = dtr->pos();
  QMapIterator<int, CYDatasTableCol *> ic(mDisplay->mCols);
  ic.toFront();
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if (dtc->type() == CYDatasTableCol::Forcing)
    {
      int col = dtc->id;
      CYData *data = dtr->data;
      if (data->inherits("CYMeasure"))
      {
        CYNumInput *wdg = (CYNumInput *)cellWidget(row, col);
        wdg->setDataName(data->dataForcing()->dataName());
        wdg->setForcingName(data->dataForcing()->dataName());
        wdg->findData();
      }
    }
  }
}

void CYDatasTablePlotter::activateNextCell()
{
}

void CYDatasTablePlotter::columnWidthChanged ( int col )
{
  Q_UNUSED(col)
  // TODO QT5
//  QTableWidget::columnWidthChanged ( col );
//  mDisplay->col(col)->setWidth( columnWidth( col ) );
//  mDisplay->setModified(true);
}

void CYDatasTablePlotter::rowHeightChanged ( int row )
{
  Q_UNUSED(row)
    // TODO QT5
//  QTableWidget::rowHeightChanged ( row );
//  if (mDisplay->row(row) && mDisplay->row(row)->shown())
//  {
//    mDisplay->row(row)->setHeight( rowHeight( row ) );
//    mDisplay->setModified(true);
//  }
//  if (!mRowHeightChanging)
//  {
//    mRowHeightChanging = true;
//    mDisplay->setRowHeight(rowHeight( row ), row);
//    mRowHeightChanging = false;
//  }
}

void CYDatasTablePlotter::contentsMousePressEvent ( QMouseEvent * e )
{
  Q_UNUSED(e)

  // TODO QT5
//  QTableWidget::contentsMousePressEvent(e);
//  if (e->button() == Qt::RightButton)
//  {
//    QWidget *wdg  = currentWidget();
//    if(wdg && wdg->inherits("CYDatasTableDisplayCell"))
//    {
//      CYDatasTableDisplayCell *cell = (CYDatasTableDisplayCell *)wdg;
//      CYDisplay *display = (CYDisplay *)cell->display();
//      if (display)
//        display->showPopupMenu();
//    }
//    else
//      emit showPopupMenu();
//  }
}

void CYDatasTablePlotter::showHideColumns()
{
  QMapIterator<int, CYDatasTableCol *> ic(mDisplay->mCols);
  ic.toFront();
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if (!dtc)
      continue;
    if ( (dtc->type()==CYDatasTableCol::Forcing) || (dtc->type()==CYDatasTableCol::Control))
    {
      if (mDisplay->forcing() && dtc->shown())
      {
        showColumn(dtc->id);
      }
      else
        hideColumn(dtc->id);
    }
    else if (dtc->shown())
      showColumn(dtc->id);
    else
      hideColumn(dtc->id);
  }
}

/*void CYDatasTablePlotter::wheelEvent(QWheelEvent * e)
{
  if (e->state() == Qt::ControlButton)
    e->ignore();
}
*/
QWidget * CYDatasTablePlotter::currentWidget()
{
  return cellWidget(currentRow(), currentColumn());
}

void CYDatasTablePlotter::showEvent(QShowEvent *e)
{
  QTableWidget::showEvent(e);
  emit initForcingValues();
}
