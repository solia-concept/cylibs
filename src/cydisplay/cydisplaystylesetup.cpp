#include "cydisplaystylesetup.h"
#include "ui_cydisplaystylesetup.h"
#include "cydisplaystyle.h"

CYDisplayStyleSetup::CYDisplayStyleSetup(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYDisplayStyleSetup)
{
  ui->setupUi(this);
}

CYDisplayStyleSetup::~CYDisplayStyleSetup()
{
  delete ui;
}

void CYDisplayStyleSetup::setStyle(CYDisplayStyle *style)
{
  ui->fgColor1->setColor(style->fgColor1);
  ui->fgColor2->setColor(style->fgColor2);
  ui->alarmColor->setColor(style->alarmColor);
  ui->backgroundColor->setColor(style->backgroundColor);
  ui->gridColor->setColor(style->gridColor);
  ui->fontSize->setValue(style->fontSize);
}

QListWidget *CYDisplayStyleSetup::colorList()
{
  return ui->colorList;
}

void CYDisplayStyleSetup::initConnect(CYDisplayStyle *style)
{
  connect(ui->changeColor, SIGNAL(clicked()), style, SLOT(editColor()));
  connect(ui->colorList, SIGNAL(currentItemChanged(QListWidgetItem *,QListWidgetItem *)), this, SLOT(selectionChanged(QListWidgetItem *,QListWidgetItem *)));
  connect(ui->buttonApply, SIGNAL(clicked()), style, SLOT(applyToWorksheet()));
  connect(ui->colorList, SIGNAL(doubleClicked ( QListWidgetItem *)), style, SLOT(editColor()));
}

void CYDisplayStyleSetup::selectionChanged(QListWidgetItem *lwi,QListWidgetItem *)
{
  ui->changeColor->setEnabled(lwi != 0);
}

void CYDisplayStyleSetup::apply(CYDisplayStyle *style)
{
  style->fgColor1 = ui->fgColor1->color();
  style->fgColor2 = ui->fgColor2->color();
  style->alarmColor = ui->alarmColor->color();
  style->backgroundColor = ui->backgroundColor->color();
  style->gridColor = ui->gridColor->color();
  style->fontSize = ui->fontSize->value();
}
