/***************************************************************************
                          cydatastablerow.h  -  description
                             -------------------
    begin                : mer nov 24 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDATASTABLEROW_H
#define CYDATASTABLEROW_H

// QT
#include <QObject>
#include <QList>
#include <QDomElement>
// CYLIBS
#include "cydisplayitem.h"

class CYDatasTable;
class CYDatasTableDisplayCell;

/** @short Ligne d'une table de données correspondant à une donnée.
  * @author LE CLÉACH Gérald
  */

class CYDatasTableRow : public CYDisplayItem
{
  Q_OBJECT
public:
  CYDatasTableRow(QObject *parent, CYData *d, int i, int pos, bool shown, int height, QString l);
  ~CYDatasTableRow();

  /** @return la position de la ligne. */
  int pos();
  /** Saisie la position de la ligne. */
//  void setPos(int val) { mPos = val; }

  /** @return true si la ligne est affichée. */
  bool shown() { return mShown; }
  /** Saisir \a true si la ligne est affichée. */
  void setShown(bool state) { mShown = state; }

  /** @return la hauteur de la ligne. */
  int height() { return mHeight; }
  /** Saisie la hauteur de la ligne. */
  void setHeight(int val) { mHeight = val; }

  /** Ajoute une cellule contenant un afficheur CYDisplay. */
  void addDisplayCell(CYDatasTableDisplayCell *cell);
  /** @return la cellule contenant un afficheur CYDisplay dans la colonne \a col. */
  CYDatasTableDisplayCell *displayCell(int col);

  /** Construit les afficheurs de la ligne à partir d'un élément DOM. */
  void createDisplaysFromDOM(QDomElement &el);
  /** Ajoute les afficheurs de la ligne dans un élément DOM. */
  void addDisplaysToDOM(QDomDocument &doc, QDomElement &element, bool save);

  /** Démarre le rafraîchissement. */
  void startRefresh();
  /** Arrête le rafraîchissement. */
  void stopRefresh();
    CYDatasTable * table();

public slots:
  void changeForcingCell();

signals:
  void changingForcingCell(CYDatasTableRow *dtr);
  void changeCtrl(bool val);

protected: // Protected attributes
  /** Position de la ligne. */
  int mPos;
  /** Vaut \a true si la ligne est affichée. */
  bool mShown;
  /** Hauteur de la ligne. */
  int mHeight;
  /** Cellules d'afficheur. */
  QList<CYDatasTableDisplayCell*> mDisplayCell;
};

#endif

