#ifndef CYDISPLAYSTYLESETUP_H
#define CYDISPLAYSTYLESETUP_H

// QT
#include <QListWidget>
// CYLIBS
#include <cydialog.h>

class CYDisplayStyle;

namespace Ui {
    class CYDisplayStyleSetup;
}

class CYDisplayStyleSetup : public CYDialog
{
  Q_OBJECT

public:
  CYDisplayStyleSetup(QWidget *parent=0, const QString &name="displayStyleSetup");
  ~CYDisplayStyleSetup();

  void setStyle(CYDisplayStyle *style);
  QListWidget *colorList();

  void initConnect(CYDisplayStyle *style);
  void apply(CYDisplayStyle *style);

public slots:
  void selectionChanged(QListWidgetItem *,QListWidgetItem *);

private:
  Ui::CYDisplayStyleSetup *ui;
};

#endif // CYDISPLAYSTYLESETUP_H
