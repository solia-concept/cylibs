//
// C++ Interface: cyscopelegend
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSCOPESIGNALLEGEND_H
#define CYSCOPESIGNALLEGEND_H

// CYLIBS
#include "cyscopeplotter.h"

class CYScopeLegend;

/**
@short Légende d'oscilloscope.

@author Gérald LE CLEACH
*/
class CYScopeSignalLegend : public CYFrame
{
Q_OBJECT
public:
    CYScopeSignalLegend(const CYScopeLegend *legend, CYScopeSignal *signal);

    ~CYScopeSignalLegend();

protected:
    const CYScopeLegend *mLegend;
    CYScopeSignal * mSignal;
    CYScopePlotter * mPlotter;
    QVBoxLayout *mVBL;
};


#endif
