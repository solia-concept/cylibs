#ifndef CYSCOPELEGEND_H
#define CYSCOPELEGEND_H

// QT
#include <QGridLayout>
// QWT
#include "qwt_legend.h"

class CYScopePlotter;
class CYDB;
class CYData;
class CYFrame;

class CYScopeLegend : public QwtLegend
{
  Q_OBJECT
public:
  CYScopeLegend(CYScopePlotter *);

protected:
  virtual QWidget *createWidget( const QwtLegendData & ) const;

  CYScopePlotter *mPlot;
  CYDB *mDB;
};

#endif // CYSCOPELEGEND_H
