/***************************************************************************
                          cydisplayitem.h  -  description
                             -------------------
    début                  : mer sep 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDISPLAYITEM_H
#define CYDISPLAYITEM_H

// QT
#include <qobject.h>
#include <qstring.h>
#include <qcolor.h>
#include <qpen.h>
// CYLIBS
#include <cyf64.h>

class CYDB;
class CYData;

/** @short Donnée d'un afficheur.
  * @author Gérald LE CLEACH.
  */

class CYDisplayItem : public QObject
{
  Q_OBJECT

public:
    CYDisplayItem(QObject *parent, CYData *d, int i, QString label=0, double coefficient=1.0, double offset=0.0);
    ~CYDisplayItem();

    /** Donnée. */
    CYData *data;
    /** Index de la donnée par rapport à l'afficheur. */
    int id;
    /** Nom de l'hôte (des hôtes). */
    QString host;
    /** Nom de la connection (des connections). */
    QString link;
    /** Groupe d'appartenance. */
    QString group;
    /** Type du signal. */
    QString type;
    /** Etiquette du signal. */
    QString label;
    /** Description du signal. */
    QString desc;
    /** Unité du signal. */
    QString unit;
    /** Type physique du signal. */
    QString physical;

    QString title;
    QPen pen;
    double coefficient;
    double offset;

    /** Validité de la donnée. */
    bool isOk(bool testFlag=true);
    virtual QString status();
    virtual bool dataLabel();
    /** @return la valeur de la donnée.
      * Nécessaire depuis le passage en 64bits. */
    double val();
    /** @return la valeur maxi. */
    double max();
    /** @return la valeur mini. */
    double min();


public slots:
    virtual void updateFormat();

    /** Initialise la bufferisation discontinue */
    void initDiscontBuffer();

signals:
    void formatUpdated();

protected:
    bool mDataLabel;
    bool mFormatUpdated;
};

#endif
