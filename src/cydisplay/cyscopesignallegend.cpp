//
// C++ Implementation: cyscopesignallegend
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyscopesignallegend.h"

// QWT
#include "qwt_legend_label.h"
#include "qwt_painter.h"
// CYLIBS
#include "cycore.h"
#include "cyf64.h"
#include "cycolor.h"
#include "cyframe.h"
#include "cyscopelegend.h"
#include "cyscopesignal.h"
#include "cynumdisplay.h"

CYScopeSignalLegend::CYScopeSignalLegend(const CYScopeLegend *parent, CYScopeSignal *signal)
 : CYFrame(0),
   mLegend( parent ),
   mSignal( signal )
{
  if (!signal || !signal->scope())
    return;

  setObjectName(signal->objectName());
  mPlotter = signal->scope()->plot();

  mVBL = new QVBoxLayout;
  setLayout(mVBL);
  mVBL->setMargin(0);
  mVBL->setSpacing(0);

  QwtLegendLabel *label = new QwtLegendLabel;
  label->setText(signal->legend(true));
  label->setToolTip(signal->legend(true));
  QColor c = signal->pen.color();

  if(c.isValid())
  {
    int h=12;
    int w=32;
    QImage image( w, h, QImage::Format_ARGB32 );
    QPainter painter( &image );
    painter.fillRect(0,0,w,h,mPlotter->backgroundColor()->val());
    painter.setPen(signal->pen);
    painter.drawLine(0,h/2,w,h/2);
    painter.end();

    QPixmap pixmap = QPixmap::fromImage(image);
    label->setIcon(pixmap);
  }
  label->setItemMode( mLegend->defaultItemMode() );
  mVBL->addWidget(label);

//  connect(mPlotter, SIGNAL(updateLegends()), this, SLOT(update()));

  mTimer->start(250);
}


CYScopeSignalLegend::~CYScopeSignalLegend()
{
}

