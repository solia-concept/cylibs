//
// C++ Interface: cydisplaysimplesetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDISPLAYSIMPLESETUP_H
#define CYDISPLAYSIMPLESETUP_H

// CYLIBS
#include <cydialog.h>

class CYDisplaySimple;


namespace Ui {
    class CYDisplaySimpleSetup;
}

/**
@short Boîte de configuration d'un afficheur simple.

@author Gérald LE CLEACH
*/
class CYDisplaySimpleSetup : public CYDialog
{
Q_OBJECT
public:
    CYDisplaySimpleSetup(CYDisplaySimple *parent = 0, const QString &name = 0);

    ~CYDisplaySimpleSetup();

protected:
    virtual void init();
protected:
    CYDisplaySimple * mDisplaySimple;

public slots:
    virtual void apply();
    void setStylePage(int page);

private slots:
    void help();

private:
    Ui::CYDisplaySimpleSetup *ui;
};

#endif
