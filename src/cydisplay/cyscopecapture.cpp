/***************************************************************************
                          cyscopecapture.cpp  -  description
                             -------------------
    début                  : Mon Mar 24 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyscopecapture.h"

// QT
#include <QLabel>
#include <QLayout>
#include <QKeyEvent>
#include <QGridLayout>
#include <QString>
#include <QMenu>
#include <QMouseEvent>
#include <QShowEvent>
#include <QPushButton>
// QWT
#include "qwt_plot_dict.h"
#include "qwt_plot_canvas.h"
#include "qwt_scale_div.h"
#include "qwt_plot_layout.h"
#include "qwt_scale_draw.h"
#include "qwt_plot_grid.h"
#include "qwt_legend.h"
#include "qwt_text_label.h"
#include "qwt_legend_label.h"
#include "qwt_plot_zoomer.h"
#include "qwt_picker_machine.h"
// CYLIBS
#include "cycore.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cys16.h"
#include "cystring.h"
#include "cyscopeaxis.h"
#include "cyscopesignal.h"
#include "cynumdisplay.h"
#include "cypixmapview.h"
#include "cytextlabel.h"
#include "cyscopesetup.h"
#include "cydataslistview.h"
#include "cydataseditlist.h"
#include "cyscopecapturelegend.h"
#include "cyplotpicker.h"
#include "cyscopecursorlegend.h"
#include "cyplotcurve.h"

CYScopeCapture::CYScopeCapture(CYScope *scope, QWidget *parent, const QString &name, int timerInterval, CYDB *db, CYScope::Mode mode, CYScope::SamplingMode sample)
  : CYScopePlotter(scope, parent, name, timerInterval, db, mode, sample, true)
{
  mSetup = 0;
  mInitSimple = false;
  mInitDouble = false;
  mInitFollow = false;
  mCursorX = xBottom;
  mCursorY = yLeft;

  marqueurXcur1=-1;
  marqueurYcur1=-1;
  marqueurXcur2=-1;
  marqueurYcur2=-1;

  mCursor1Color = Qt::red;
  mCursor2Color = Qt::green;

  setAutoReplot(true);

  if (parent && parent->parent() && parent->parent()->inherits("CYScopeAnalyser"))
  {
    copy(scope->plot());
  }

  // Curseurs
  mPicker->setStateMachine( new QwtPickerDragPointMachine() );
  mPicker->setRubberBandPen( QColor( Qt::green ) );
  mPicker->setRubberBand( QwtPicker::CrossRubberBand );
  mPicker->setTrackerPen( QColor( Qt::white ) );
  mPicker->setEnabled( true );

  mPickerZoom = new CYPlotPicker( QwtPlot::xBottom, QwtPlot::yLeft,
      QwtPlotPicker::RectRubberBand, QwtPicker::ActiveOnly, canvas() );
  QPen pen(Qt::DashLine);
  pen.setColor(Qt::white);
  mPickerZoom->setRubberBandPen( pen );
  mPickerZoom->setTrackerMode( QwtPicker::ActiveOnly );
  mPickerZoom->setTrackerPen( QColor( Qt::white ) );
  mPickerZoom->setEnabled( false );
  mPickerZoom->setStateMachine( new QwtPickerDragRectMachine() );

  k_curseur = 1;
  step = 1;
  faststep = step *10;

  penCur1On.setColor(mCursor1Color);
  penCur1Off.setColor(mCursor1Color);
  penCur2On.setColor(mCursor2Color);
  penCur2Off.setColor(mCursor2Color);

  penCur1On.setWidth(mPicker->rubberBandPen().width());
  penCur1Off.setWidth(mPicker->rubberBandPen().width());
  penCur2On.setWidth(mPicker->rubberBandPen().width());
  penCur2Off.setWidth(mPicker->rubberBandPen().width());

  penCur1Off.setStyle(Qt::DashDotDotLine);
  penCur2Off.setStyle(Qt::DashDotDotLine);

  marqueurXcur1 = insertLineMarker(0, mCursorX);
  marqueurYcur1 = insertLineMarker(0, mCursorY);

  marqueurXcur2 = insertLineMarker(0, mCursorX);
  marqueurYcur2 = insertLineMarker(0, mCursorY);

  updateFontSize();

  connect( mPicker, SIGNAL( moved( const QPoint & ) ), SLOT( plotMouseMoved( const QPoint & ) ) );
  connect( mPicker, SIGNAL( mousePressed( const QMouseEvent & ) ), SLOT( plotMousePressed( const QMouseEvent & ) ) );
  connect( mPicker, SIGNAL( mouseReleased( const QMouseEvent & ) ), SLOT( plotMouseReleased( const QMouseEvent & ) ) );
}

CYScopeCapture::~CYScopeCapture()
{
}

void CYScopeCapture::showEvent(QShowEvent *e)
{
  QHashIterator<QString, CYNumDisplay *> it(displayUnit);
  while (it.hasNext())
  {
    it.next();
    CYNumDisplay *display = it.value();
    CYData *data = core->findData(db(), it.key());
    display->setEnableColor(data->enableColor());
    display->setText( data->unit() );
  }
  CYScopePlotter::showEvent(e);
}


void CYScopeCapture::captureCurves(CYScopePlotter *s)
{
  dateTime = QDateTime::currentDateTime();
  switch (modeVal())
  {
    case CYScope::Time  : captureCurvesTimeMode(s);
                          break;
    case CYScope::XY    : captureCurvesXYMode(s, xBottom);
                          captureCurvesXYMode(s, xTop);
                          break;
    default             : CYFATAL;
  }

  showCursors(false);
  initPosCursor();
  mPicker->setRubberBand(QwtPicker::CrossRubberBand);
  mPicker->setRubberBandPen(penCur1On);

  if (scopeAxis[xBottom]->isEnabled())
    mCursorX = xBottom;
  else if (scopeAxis[xTop]->isEnabled())
    mCursorX = xTop;

  if (scopeAxis[yLeft]->isEnabled())
    mCursorY = yLeft;
  else if (scopeAxis[yRight]->isEnabled())
    mCursorY = yRight;

  k_curseur = 2;
  Yft(posXcur2);
  k_curseur = 1;
  Yft(posXcur1);
//  mPicker->setEnabled(false);
  showCursors(true);
  mLegendPosition->setVal(QwtPlot::RightLegend);

  emit updateCursorLegends();

  replot();
}

void CYScopeCapture::captureCurvesTimeMode(CYScopePlotter *s)
{
  QListIterator<CYScopeSignal *> it(s->ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *signal = it.next();
#if CY_DEBUG_SAVE_CURVES > 0
    signal->setEnableBuffering(false);
#else
    CYScopeSignal *newSignal = addCurve(signal->data, signal->id, signal->label, signal->pen, signal->coefficient, signal->axis());
    newSignal->copieBuffer(signal->buffer(offset_mesure, s->samplingModeVal()), s->samplingModeVal());
    setCurveData(newSignal->id, timeScopeAxis()->xTime.data(), newSignal->buffer(offset_mesure, s->samplingModeVal()).data(), size_mesure, signal->coefficient);
#endif
  }
}

void CYScopeCapture::captureCurvesXYMode(CYScopePlotter *s, int xAxis)
{
  if (!s->xSignal(xAxis))
    return;
  mXSignal.insert(xAxis, s->xSignal(xAxis)->capture());

  if (!xSignal(xAxis))
    return;

  changeRangeX();
  QListIterator<CYScopeSignal *> it(s->ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *signal = it.next();
#if CY_DEBUG_SAVE_CURVES > 0
    signal->setEnableBuffering(false);
#else
    CYScopeSignal *newSignal = addCurve(signal->data, signal->id, signal->label, signal->pen, signal->coefficient, signal->axis());
//    newSignal->setEnableBuffering(false);
    newSignal->copieBuffer(signal->buffer(offset_mesure, s->samplingModeVal()), s->samplingModeVal());
    setCurveData(newSignal->id,
                 xSignal(xAxis)->buffer(offset_mesure, s->samplingModeVal()).data(),
                 newSignal->buffer(offset_mesure, s->samplingModeVal()).data(),
                 size_mesure, signal->coefficient);
    newSignal->setYCoef(signal->coefficient);
#endif
  }
}

/***************************************************************************
 *                           Gestion des curseurs                          *
 ***************************************************************************/
void CYScopeCapture::initPosCursor(const int div)
{
  QRectF rect = canvas()->contentsRect();

  int lineWidth=0;
  if (canvas()->inherits("QFrame"))
  {
    QFrame *frame = (QFrame *)canvas();
    lineWidth = frame->lineWidth();
  }
  posXcur1 = rect.left() + (rect.width() / div) - 2*lineWidth;
  posYcur1 = rect.top() + (rect.height() / div) - 2*lineWidth;
  posXcur2 = rect.right() - (rect.width() / div);
  posYcur2 = rect.bottom() - (rect.height() / div);
  transformPosToVal();

  replot();
}

void CYScopeCapture::showCursors(const bool enable)
{
  if (!enable)
  {
    showMarker(marqueurXcur1, false);
    showMarker(marqueurYcur1, false);
    showMarker(marqueurXcur2, false);
    showMarker(marqueurYcur2, false);
  }
  else if (axisCursors())
  {
    transformValToPos();

    showMarker(marqueurXcur1, true);
    showMarker(marqueurYcur1, true);
    showMarker(marqueurXcur2, true);
    showMarker(marqueurYcur2, true);

//    marqueurXcur1 = insertLineMarker(0, mCursorX);
    setMarkerXPos(marqueurXcur1, axisCursor1(mCursorX)->val() );
//    marqueurYcur1 = insertLineMarker(0, mCursorY);
    setMarkerYPos(marqueurYcur1, axisCursor1(mCursorY)->val());

//    marqueurXcur2 = insertLineMarker(0, mCursorX);
    setMarkerXPos(marqueurXcur2, axisCursor2(mCursorX)->val() );
//    marqueurYcur2 = insertLineMarker(0, mCursorY);
    setMarkerYPos(marqueurYcur2, axisCursor2(mCursorY)->val());

    switch (k_curseur)
    {
      case 1: // curseur 1 "On" et curseur 2 "Off"
      {
        setMarkerLinePen(marqueurXcur1, penCur1On);
        setMarkerLinePen(marqueurYcur1, penCur1On);

        setMarkerLinePen(marqueurXcur2, penCur2Off);
        setMarkerLinePen(marqueurYcur2, penCur2Off);
      }
      break; // curseur 1 "On" et curseur 2 "Off" affichés

      case 2: // curseur 1 "Off" et curseur 2 "On"
      {
        setMarkerLinePen(marqueurXcur1, penCur1Off);
        setMarkerLinePen(marqueurYcur1, penCur1Off);

        setMarkerLinePen(marqueurXcur2, penCur2On);
        setMarkerLinePen(marqueurYcur2, penCur2On);
      }
      break; // curseur 1 "Off" et curseur 2 "On" affichés
    }
  }
//   emit updateCursorLegends();
}

void CYScopeCapture::permutCursorsOnOff()
{
  switch (k_curseur)
  {
    case 1: // curseur 2 devient actif
    {
      k_curseur = 2;
      mPicker->setRubberBandPen(penCur2On);
      setMarkerLinePen(marqueurXcur1, penCur1Off);
      setMarkerLinePen(marqueurYcur1, penCur1Off);
      setMarkerLinePen(marqueurXcur2, penCur2On);
      setMarkerLinePen(marqueurYcur2, penCur2On);
    }
    break;
    case 2: // curseur 1 devient actif
    {
      k_curseur = 1;
      mPicker->setRubberBandPen(penCur1On);
      setMarkerLinePen(marqueurXcur1, penCur1On);
      setMarkerLinePen(marqueurYcur1, penCur1On);
      setMarkerLinePen(marqueurXcur2, penCur2Off);
      setMarkerLinePen(marqueurYcur2, penCur2Off);
    }
    break;
    default: CYWARNINGTEXT("Pb de permutation des états des curseurs");
  }
  replot();
}

void CYScopeCapture::keyPressEvent(QKeyEvent *e)
{
  switch (e->QInputEvent::modifiers())
  {
    case Qt::AltModifier:  // Déplacement rapide
    {
      switch (e->key())
      {
        case Qt::Key_Space  : permutCursorsOnOff();      break;
        case Qt::Key_Left   : moveCursorX(true , true);  break;
        case Qt::Key_Right  : moveCursorX(false, true);  break;
        case Qt::Key_Down   : moveCursorY(false, true);  break;
        case Qt::Key_Up     : moveCursorY(true , true);  break;
      }
    }
    break;
    default: // Déplacement normal
    {
      switch (e->key())
      {
        case Qt::Key_Space  : permutCursorsOnOff();      break;
        case Qt::Key_Left   : moveCursorX(true , false); break;
        case Qt::Key_Right  : moveCursorX(false, false); break;
        case Qt::Key_Down   : moveCursorY(false, false); break;
        case Qt::Key_Up     : moveCursorY(true , false); break;
      }
    }
    break;
  }
  switch (k_curseur)
  {
    case 1 : Yft(posXcur1);break;
    case 2 : Yft(posXcur2);break;
  }

  replot();
}

void CYScopeCapture::moveCursorX(const bool left, const bool fast)
{
  int pos=0;
  int s;

  transformValToPos();

  // Détection du curseur à déplacer
  switch (k_curseur)
  {
    case 1: pos = posXcur1;
      break;
    case 2: pos = posXcur2;
      break;
  }

  // Réglage du pas de déplacement
  if (!fast)
    s = step;
  else
    s = faststep;

  if (left)
  {
    // Test la borne inférieure
    if ((pos - s) >= (canvasMap(mCursorX).p1()))
      pos = pos - s;
  }
  else
  {
    // Test la borne supérieure
    if ((pos + s) <= (canvasMap(mCursorX).p2()))
      pos = pos + s;
  }

  // Déplacement du curseur sélectionn
  switch (k_curseur)
  {
    case 1: // Déplacement en X du curseur 1
    {
      posXcur1 = pos;
      transformPosToVal();
      setMarkerXPos(marqueurXcur1, scopeAxis[mCursorX]->cursor1()->val());
      setMarkerLinePen(marqueurXcur1, mPicker->rubberBandPen());
    }
    break;
    case 2: // Déplacement en X du curseur 2
    {
      posXcur2 = pos;
      transformPosToVal();
      setMarkerXPos(marqueurXcur2, scopeAxis[mCursorX]->cursor2()->val() );
      setMarkerLinePen(marqueurXcur2, mPicker->rubberBandPen());
    }
    break;
  }
}

void CYScopeCapture::moveCursorY(const bool down, const bool fast)
{
  int pos=0;
  int s;

  transformValToPos();

  // Détection du curseur à déplacer
  switch (k_curseur)
  {
    case 1: pos = posYcur1;
      break;
    case 2: pos = posYcur2;
      break;
  }

  // Réglage du pas de déplacement
  if (!fast)
    s = step;
  else
    s = faststep;

  if (down)
  {
    // Test la borne inférieure
    if ((pos - s) >= (canvasMap(mCursorY).p2()))
      pos = pos - s;
  }
  else
  {
    // Test la borne supérieure
    if ((pos + s) <= (canvasMap(mCursorY).p1()))
      pos = pos + s;
  }

  // Déplacement du curseur sélectionn
  switch (k_curseur)
  {
    case 1: // Déplacement en Y du curseur 1
    {
      posYcur1 = pos;
      transformPosToVal();
      setMarkerYPos(marqueurYcur1, scopeAxis[mCursorY]->cursor1()->val());
      setMarkerLinePen(marqueurYcur1, mPicker->rubberBandPen());
    }
    break;
    case 2:// Déplacement en Y du curseur 2
    {
      posYcur2 = pos;
      transformPosToVal();
      setMarkerYPos(marqueurYcur2, scopeAxis[mCursorY]->cursor2()->val());
      setMarkerLinePen(marqueurYcur2, mPicker->rubberBandPen());
    }
    break;
  }
}

void CYScopeCapture::plotMousePressed(const QMouseEvent &e)
{
  if (!mZooming && axisCursors())
  {
//    removeMarkers();

    mInitMove = false;
    transformValToPos();
    updateCursorValues(e.pos());
    Yft(e.pos().x());
    switch (k_curseur)
    {
      case 1: // L'outline devient curseur 1 et conservation du curseur 2
      {
        // cache le marqueur du curseur 1
        showMarker(marqueurXcur1, false);
        showMarker(marqueurYcur1, false);

        // Conservation du curseur 2  en mode "Off" selon l'axe x
        setMarkerXPos(marqueurXcur2, axisCursor2(mCursorX)->val());
        setMarkerLinePen(marqueurXcur2, penCur2Off);

        // Conservation du curseur 2 en mode "Off" selon l'axe y
        setMarkerYPos(marqueurYcur2, axisCursor2(mCursorY)->val());
        setMarkerLinePen(marqueurYcur2, penCur2Off);

        mPicker->setRubberBandPen(penCur1On);
      }
      break; // curseur 1 est l'outline et curseur 2 conserv

      case 2: // L'outline devient curseur 2 et conservation du curseur 1
      {
        // cache le marqueur du curseur 1
        showMarker(marqueurXcur2, false);
        showMarker(marqueurYcur2, false);

        // Conservation du curseur 1 en mode "Off" selon l'axe x
        setMarkerXPos(marqueurXcur1, axisCursor1(mCursorX)->val());
        setMarkerLinePen(marqueurXcur1, penCur1Off);

        // Conservation du curseur 1 en mode "Off" selon l'axe y
        setMarkerYPos(marqueurYcur1, axisCursor1(mCursorY)->val());
        setMarkerLinePen(marqueurYcur1, penCur1Off);

        mPicker->setRubberBandPen(penCur2On);
      }
      break; // curseur 2 est l'outline et curseur 1 conserv
    }

    replot();
  }
  else if (mZooming)
  {
    QHashIterator<int, CYScopeAxis *> it(scopeAxis);
    while (it.hasNext())
    {
      it.next();
      CYScopeAxis *axis = it.value();
      if (axis)
        axis->plotMousePressed(e);
    }
  }

  CYScopePlotter::plotMousePressed(e);
}

void CYScopeCapture::plotMouseMoved(const QPoint &pos)
{
  if (!mZooming)
  {
    updateCursorValues(pos);
    Yft(pos.x());

//    if (!mInitMove)
//    {
//      mInitMove = true;
//      enableOutline(false);
//      replot();
//      enableOutline(true);
//    }
  }
}

void CYScopeCapture::plotMouseReleased(const QMouseEvent &e)
{
  if ( (e.button() == Qt::RightButton) )
  {
    QMenu *pm;
    pm = new QMenu(this);
    pm->exec(QCursor::pos());
  }
  if (!mZooming)
  {
    transformValToPos();
    updateCursorValues(e.pos());
    Yft(e.pos().x());
    switch (k_curseur)
    {
      case 1: // Conservation du curseur 1
      {
        // Conservation du curseur 1 selon l'axe x
        setMarkerXPos(marqueurXcur1, scopeAxis[mCursorX]->cursor1()->val());
        setMarkerLinePen(marqueurXcur1, penCur1On);

        // Conservation du curseur 1 selon l'axe y
        setMarkerYPos(marqueurYcur1, scopeAxis[mCursorY]->cursor1()->val());
        setMarkerLinePen(marqueurYcur1, penCur1On);
      }
      break; // curseur 1 conserv

      case 2: // Conservation du curseur 2
      {
        // Conservation du curseur 2 selon l'axe x
        setMarkerXPos(marqueurXcur2, scopeAxis[mCursorX]->cursor2()->val());
        setMarkerLinePen(marqueurXcur2, penCur2On);

        // Conservation du curseur 2 selon l'axe y
        setMarkerYPos(marqueurYcur2, scopeAxis[mCursorY]->cursor2()->val());
        setMarkerLinePen(marqueurYcur2, penCur2On);
      }
      break; // curseur 2 conserv
    }
    showCursors(true);

    replot();
  }
  else
  {
    QHashIterator<int, CYScopeAxis *> it(scopeAxis);
    while (it.hasNext())
    {
      it.next();
      CYScopeAxis *axis = it.value();
      if (axis)
        axis->plotMouseReleased(e);
    }

    changeRangeX();
    changeRangeY();
    mZooming = false;
    mPickerZoom->setEnabled(mZooming);
  }

  CYScopePlotter::plotMouseReleased(e);
}

void CYScopeCapture::Yft(const int posX)
{
  double res, cx, dmin;
  res=0.0;
  QwtPlotCurve *c;
  QwtPlotItemList list = itemList(QwtPlotItem::Rtti_PlotCurve);
  // Traitement des différentes courbes existantes
  for (int i = 0; i < list.size(); ++i)
  {
    c= (QwtPlotCurve *)list.at(i);
    dmin = 1.0e10;
    // Détection de y pour un x donn
    for (uint j=0; j < c->dataSize() ; j++)
    {
      const QwtScaleMap xMap = canvasMap( c->xAxis() );
      const QPointF sample = c->sample( j );
      cx = (double) qAbs((int)xMap.transform(sample.x()) - posX);

      if (cx < dmin)
      {
        dmin = cx;
        res = (sample.y());
      }
    }

    // Emission de la valeur de la courbe traitée
    CYF64 *d1 = ySignal.at(i)->cursor(Cursor1);
    CYF64 *d2 = ySignal.at(i)->cursor(Cursor2);
    CYF64 *dd = ySignal.at(i)->cursor(Delta);
    switch (k_curseur)
    {
      case 1 :   d1->setVal(res); break;
      case 2 :   d2->setVal(res); break;
    }
    dd->setVal( d2->val() - d1->val() );
    emit followValue(i, res);
  }
}

void CYScopeCapture::transformPosToVal()
{
  QHashIterator<int, CYScopeAxis *> it(scopeAxis);
  it.toFront();
  while (it.hasNext())
  {
    it.next();
    CYScopeAxis *axis = it.value();
    if (axis)
      axis->transformPosToVal();
  }
}

void CYScopeCapture::transformValToPos()
{
  scopeAxis[mCursorX]->transformValToPos();
  scopeAxis[mCursorY]->transformValToPos();
}


/***************************************************************************
 *                           Gestion d'impression                          *
 ***************************************************************************/
void CYScopeCapture::printCurves(QPainter *p, QRectF *rPlot)
{
  p->setFont(titleLabel()->text().font());
  p->setPen(Qt::black);

  // impression du titre
  if (!title().isEmpty())
  {
    QRectF rect;
    p->drawText(*rPlot, Qt::AlignTop | Qt::AlignHCenter | Qt::TextWordWrap, title().text(), &rect);
    // augmente la hauteur du rectangle contenant le scope
    rPlot->setTop(rPlot->top() + rect.height()+10);
  }

  QwtPlotRenderer renderer;
  renderer.setDiscardFlag(QwtPlotRenderer::DiscardBackground, true);
  renderer.setDiscardFlag(QwtPlotRenderer::DiscardCanvasBackground, true);
  renderer.setDiscardFlag(QwtPlotRenderer::DiscardLegend, true);
  renderer.render(this, p, *rPlot);
}

QColor CYScopeCapture::cursorColor(Cursor cursor)
{
  switch(cursor)
  {
    case Cursor1  : return mCursor1Color;
    case Cursor2  : return mCursor2Color;
    case Delta    : return Qt::white;
    case IDelta   : return Qt::white;
    default       : CYMESSAGE;
                    return Qt::black;
  }
}


void CYScopeCapture::addYSignal(int id, CYScopeSignal *signal)
{
  CYScopePlotter::addYSignal(id, signal);
}

/*! Initilalise les afficheurs d'une donnée Yfx d'un curseur
    \fn CYScopeCapture::initYfxDisplay(CYFrame *frame, CYData *data)
 */
void CYScopeCapture::initYfxDisplay(CYFrame *frame, CYData *data)
{
  if (!data)
    return;

  QHBoxLayout *yBox = new QHBoxLayout( frame );
  CYNumDisplay *display;

  display = new CYNumDisplay( this );
  display->setShowLabel(true);
  display->setSuffix("\t:");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( data->dataName() );
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  display->setAlwaysOk(true);
  yBox->addWidget(display);
  yBox->setStretchFactor(display, 1);

  display = new CYNumDisplay( this );
  display->setAlignment(Qt::AlignRight);
  display->setLocaleDB(mDB);
  display->setHideIfNoData( true );
  display->setShowUnit(false);
  display->setDataName( data->dataName() );
  display->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
  display->setAlwaysOk(true);
  yBox->addWidget(display);
  yBox->setStretchFactor(display, 6);

  display = new CYNumDisplay( this );
  display->setShowLabel(true);
//   display->setSuffix("\t:");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setEnableColor(data->enableColor());
  display->setText( data->unit() );
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  display->setAlwaysOk(true);
  yBox->addWidget(display);
  yBox->setStretchFactor(display, 6);
}


/*! Met à jour les valeurs du curseur actif par rapport au point
    \fn CYScopeCapture::updateCursorValues(const QPoint point)
 */
void CYScopeCapture::updateCursorValues(const QPoint point)
{
  switch (k_curseur)
  {
    case 1:
    {
      posXcur1 = point.x();
      posYcur1 = point.y();
    }
    break;
    case 2:
    {
      posXcur2 = point.x();
      posYcur2 = point.y();
    }
    break;
  }
  transformPosToVal();
}



/*! Imprime la légende avec les valeurs affichées.
    \fn CYScopeCapture::printLegends(QPainter *p, QRectF *r, int top, int &nb)
 */
void CYScopeCapture::printLegends(QPainter *p, QRectF *r, int top, int &nb)
{
  int w = plotLayout()->legendRatio()*r->width();
  if ( !scopeAxis[yLeft]->isEnabled() || !scopeAxis[yRight]->isEnabled() )
    w = w*7/12;

  QRectF rect, brect;

  rect = QRectF(r->left()-w, top, w, 0);
  brect = rect;
  r->setWidth(r->width()-w);

  rect = QRectF(r->right()+4, top, w, r->height());
  brect = rect;
  QPen oldPen;

  // Curseurs
  oldPen = p->pen();
  p->drawText(rect, Qt::AlignTop|Qt::AlignHCenter|Qt::TextWordWrap, tr("Cursors"), &brect);
  brect.setWidth(rect.width());
  brect.moveLeft(rect.left());
//  p->drawRect(brect);

  printCursorAxis(p, rect, brect, Cursor1, xBottom);
  printCursorAxis(p, rect, brect, Cursor1, yLeft, yRight);
  printCursorAxis(p, rect, brect, Cursor2, xBottom);
  printCursorAxis(p, rect, brect, Cursor2, yLeft, yRight);
  printCursorAxis(p, rect, brect, Delta  , xBottom);
  printCursorAxis(p, rect, brect, IDelta , xBottom);
  printCursorAxis(p, rect, brect, Delta  , yLeft, yRight);

  // dessine pour les courbes
  int old_nb = nb;

  int rectTop = brect.bottom();

  rect = QRectF(r->left()-2*w, rectTop, w, 0);
  int end = ySignal.count();
  for (int i=old_nb; i<ySignal.count(); i++)
  {
    printCursorSignal(p, rect, brect, ySignal.at(i));
    if (brect.bottom()>r->bottom())
    {
      end = i-1;
      break;
    }
  }

  rect = QRectF(r->right()+4, rectTop, w, r->height());
  brect.setBottom(rect.top());
  nb = end;

  for (int i=old_nb; i<end; i++)
  {
    printCursorSignal(p, rect, brect, ySignal.at(i));
  }
}


/*! Ouvre la boîte de configuration.
    \fn CYScopeCapture::settings()
 */
void CYScopeCapture::settings()
{
  if (!mSetup)
  {
    mSetup = new CYScopeSetup(this, "CYScopeSetup");
    Q_CHECK_PTR(mSetup);
    connect( mSetup, SIGNAL( applying() ), SLOT( applySettings() ) );
  }
  mSetup->exec();
}


void CYScopeCapture::applySettings()
{
  updateFontSize();

//  if (mSetup)
//  {
//    CYDatasListView *list;
//    list = mSetup->dataYList();

//    for ( uint i = 0; i < ySignal.count(); ++i )
//    {
//      CYScopeSignal *signal = ySignal.at(i);
//      signal->pen = list->pen( i+1 );
//      QTreeWidgetItem *item = list->findItems( QString("%1").arg(signal->id), Qt::MatchExactly, list->numColumn() ).first();
//      signal->label = list->label( item );
//      // TODO QWT
////      setCurveTitle( signal->id, signal->label );
//    }
//  }
  CYScopePlotter::applySettings();
}



/*! Initiallise la base de données de l'afficheur.
    \fn CYScopeCapture::initDB()
 */
void CYScopeCapture::initDB()
{
  CYScopePlotter::initDB();
/*  mTitle = new CYString(mDB, "TITLE", new QString(title()));
  mTitle->setLabel(tr("Title"));

  mDB->setUnderGroup(tr("Style"));
  mFontSize = new CYS16(mDB, "FONT_SIZE", new s16, 13, 1, 500, 0);
  mFontSize->setLabel(tr("Font size"));*/

  mDB->setUnderGroup(0);
}


/*! @return la taille de police
    \fn CYScopeCapture::fontSize()
 */
int CYScopeCapture::fontSize()
{
  return mScope->fontSize();
}


/*!
    Called internally when the legend has been clicked on.
    Emits a legendClicked() signal.
    \fn CYScopeCapture::lgdClicked()
 */
void CYScopeCapture::lgdClicked()
{
  if ( sender()->parent() && sender()->parent()->inherits("CYFrame") )
  {
//    TODO QWT
//    long key = d_legend->key((QWidget *)sender()->parent());
//    if ( key >= 0 )
//        emit legendClicked(key);
  }
}


/*!
    \fn CYScopeCapture::curveClicked(long key)
 */
void CYScopeCapture::curveClicked(long key)
{
  static bool enable = true;
  if ( !enable )
  {
    enable = true;
    return;
  }
  enable = false;

  CYScopePlotter::curveClicked( key );
}

CYF64 *CYScopeCapture::axisCursor1(QwtPlot::Axis axis)
{
  return scopeAxis[axis]->cursor1();
}

CYF64 *CYScopeCapture::axisCursor2(QwtPlot::Axis axis)
{
  return scopeAxis[axis]->cursor2();
}

CYF64 *CYScopeCapture::axisCursorD(QwtPlot::Axis axis)
{
  return scopeAxis[axis]->cursorD();
}

CYF64 *CYScopeCapture::axisCursorID(QwtPlot::Axis axis)
{
  return scopeAxis[axis]->cursorID();
}


/*!
    \fn CYScopeCapture::axisCursors()
 */
bool CYScopeCapture::axisCursors()
{
  return (axisCursor1(mCursorX) && axisCursor1(mCursorY) && axisCursor2(mCursorX) && axisCursor2(mCursorY) ) ? true : false;
}


/*!
    \fn CYScopeCapture::printCursorAxis(QPainter *p, QRectF &rect, QRectF &brect, Cursor cursor, QwtPlot::Axis axis1, QwtPlot::Axis axis2)
 */
void CYScopeCapture::printCursorAxis(QPainter *p, QRectF &rect, QRectF &brect, Cursor cursor, QwtPlot::Axis axis1, QwtPlot::Axis axis2)
{
  QPen oldPen = p->pen();
  QPen newPen;
  QString txt;
  qreal h = 0;
  bool double_axes = false;
  rect.setTop(brect.bottom());

  QRectF r1 = rect;
  QRectF r2 = rect;

  if ( scopeAxis[yLeft]->isEnabled() && scopeAxis[yRight]->isEnabled() )
  {
    r1.setLeft(rect.left()+2*rect.width()/12);
    r1.setWidth(rect.width()-7*rect.width()/12);
    r2.setLeft(r1.right());
    r2.setWidth(rect.width()-7*rect.width()/12);
  }
  else
  {
    if ( scopeAxis[yLeft]->isEnabled() )
    {
      r1.setLeft(rect.left()+4*rect.width()/12);
      r1.setWidth(rect.width()-4*rect.width()/12);
      r2.setWidth(0);
    }
    else if ( scopeAxis[yRight]->isEnabled() )
    {
      r1.setLeft(rect.left()+4*rect.width()/12);
      r1.setWidth(rect.width()-4*rect.width()/12);
      r2.setWidth(0);
    }
  }

  // TODO QWT
  //  if ((cursor==Cursor1) || (cursor==Cursor2))
//    newPen.setColor(QwtPlotRenderer().color(cursorColor(cursor), QwtPlotRenderer::Curve));
//  else
    newPen = oldPen;

  if (scopeAxis[axis1]->cursor(cursor) && scopeAxis[axis2] && scopeAxis[axis2]->cursor(cursor))
  {
    double_axes = true;
    p->setPen(newPen);
    txt = scopeAxis[axis1]->cursor(cursor)->label();
    p->drawText(rect, Qt::AlignTop|Qt::AlignLeft, txt, &brect);
    h = qMax(brect.height(), h);

    p->setPen(newPen);
    txt = scopeAxis[axis1]->cursor(cursor)->toString();
    p->drawText(r1, Qt::AlignTop|Qt::AlignRight|Qt::TextWordWrap, txt, &brect);
    h = qMax(brect.height(), h);

    p->setPen(newPen);
    txt = scopeAxis[axis2]->cursor(cursor)->toString();
    p->drawText(r2, Qt::AlignTop|Qt::AlignRight|Qt::TextWordWrap, txt, &brect);
    h = qMax(brect.height(), h);
  }
  else if (scopeAxis[axis1]->cursor(cursor))
  {
    p->setPen(newPen);
    txt = scopeAxis[axis1]->cursor(cursor)->label();
    p->drawText(rect, Qt::AlignTop|Qt::AlignLeft, txt, &brect);
    h = qMax(brect.height(), h);

    p->setPen(newPen);
    txt = scopeAxis[axis1]->cursor(cursor)->toString();

    if ((axis1==xBottom) && scopeAxis[yLeft] && scopeAxis[yLeft]->cursor(Cursor1) && scopeAxis[yRight] && scopeAxis[yRight]->cursor(Cursor1))
    {
      r1.setWidth(0);
      r2.setLeft(rect.left()+2*rect.width()/12);
      r2.setWidth(rect.width()-2*rect.width()/12);
      p->drawText(r2, Qt::AlignTop|Qt::AlignRight|Qt::TextWordWrap, txt, &brect);
    }
    else
      p->drawText(r1, Qt::AlignTop|Qt::AlignRight|Qt::TextWordWrap, txt, &brect);
    h = qMax(brect.height(), h);
  }
  else if (scopeAxis[axis2] && scopeAxis[axis2]->cursor(cursor))
  {
    p->setPen(newPen);
    txt = scopeAxis[axis2]->cursor(cursor)->label();
    p->drawText(rect, Qt::AlignTop|Qt::AlignLeft, txt, &brect);
    h = qMax(brect.height(), h);

    p->setPen(newPen);
    txt = scopeAxis[axis2]->cursor(cursor)->toString();
    p->drawText(r1, Qt::AlignTop|Qt::AlignRight|Qt::TextWordWrap, txt, &brect);
    h = qMax(brect.height(), h);
  }
  p->setPen(oldPen);
  brect = rect;
  brect.setHeight(h);
  p->drawRect(brect);
  p->drawLine(r1.left(), r1.top()+1, r1.left(), r1.top()+h);
  if (double_axes)
    p->drawLine(r2.left(), r2.top()+1, r2.left(), r2.top()+h);
}


/*!
    \fn CYScopeCapture::printCursorSignal(QPainter *p, QRectF &rect, QRectF &brect, CYScopeSignal *signal)
 */
void CYScopeCapture::printCursorSignal(QPainter *p, QRectF &rect, QRectF &brect, CYScopeSignal *signal)
{
  qreal wLine = 20;
  int space_col = 4;
  qreal h = 0;
  QRectF rTmp;

  // texte de légende d'une courbe
  rect.setTop(brect.bottom());

  QPen oldPen = p->pen();
  QPen newPen;
  QString txt;

  txt=signal->legend(true);
  p->drawText(QRectF(rect.left()+wLine+space_col, rect.top(), rect.width()-wLine-space_col, rect.height()),
              Qt::AlignTop|Qt::AlignHCenter|Qt::TextWordWrap, txt, &brect);
  h = qMax(brect.height(), h);

  rTmp = rect;
  rTmp.setHeight(h);

  // trait de légende d'une courbe
  rect.setTop(brect.bottom());
  newPen = signal->pen;
  // TODO QWT
//  newPen.setColor(QwtPlotRenderer().color(newPen.color(), QwtPlotRenderer::Curve));
  p->setPen(newPen);
  p->drawLine(brect.left()-wLine-space_col, rect.top()-brect.height()/2, brect.left()-space_col, rect.top()-brect.height()/2);

  p->setPen(oldPen);
  printCursorSignal(p, rect, brect, signal, Cursor1);
  printCursorSignal(p, rect, brect, signal, Cursor2);
  printCursorSignal(p, rect, brect, signal, Delta);
}

void CYScopeCapture::printCursorSignal(QPainter *p, QRectF &rect, QRectF &brect, CYScopeSignal *signal, Cursor cursor)
{
  qreal h = 0;
  QRectF rTmp;
  QRectF r1, r2;
  QPen oldPen = p->pen();
  QPen newPen;
  QString txt;


  // valeur de légende d'une courbe à l'instant T1
  rect.setTop(brect.bottom());
  r1 = rect;
  r2 = rect;

  if ( scopeAxis[yLeft]->isEnabled() && scopeAxis[yRight]->isEnabled() )
  {
    r1.setLeft(rect.left()+2*rect.width()/12);
    r1.setWidth(rect.width()-7*rect.width()/12);
    r2.setLeft(r1.right());
    r2.setWidth(rect.width()-7*rect.width()/12);
  }
  else
  {
    if ( signal->axis()==yLeft )
    {
      r1.setLeft(rect.left()+4*rect.width()/12);
      r1.setWidth(rect.width()-4*rect.width()/12);
      r2.setWidth(0);
    }
    else if ( signal->axis()==yRight )
    {
      r1.setWidth(0);
      r2.setLeft(rect.left()+4*rect.width()/12);
      r2.setWidth(rect.width()-4*rect.width()/12);
    }
  }

  rect.setTop(brect.top()+brect.height());
  if ( (cursor==Cursor1) || (cursor==Cursor2) )
  {
    // TODO QWT
//    newPen.setColor(QwtPlotRenderer().color(cursorColor(cursor), QwtPlotRenderer::Curve));
    p->setPen(newPen);
  }
  txt = signal->cursor(cursor)->label();
  p->drawText(rect, Qt::AlignTop|Qt::AlignLeft, txt, &brect);
  h = qMax(brect.height(), h);

  txt = signal->cursor(cursor)->toString();
  if ( signal->axis()==yLeft )
    p->drawText(r1, Qt::AlignTop|Qt::AlignRight|Qt::TextWordWrap, txt, &brect);
  else if ( signal->axis()==yRight )
    p->drawText(r2, Qt::AlignTop|Qt::AlignRight|Qt::TextWordWrap, txt, &brect);
  h = qMax(brect.height(), h);

  p->setPen(oldPen);
  rTmp = rect;
  rTmp.setHeight(h);
  p->drawRect(rTmp);

  p->drawLine(r1.left(), r1.top()+1, r1.left(), r1.top()+h);
  p->drawLine(r2.left(), r2.top()+1, r2.left(), r2.top()+h);
}

void CYScopeCapture::setLegendPosition(const int val )
{
  insertLegend( NULL );
  if ( val!=-1 )
  {
    mCaptureLegend= new CYScopeCaptureLegend(this);
    insertLegend( mCaptureLegend,
                  QwtPlot::LegendPosition( RightLegend ), 0.2 );
  }
}

/*!  Activation du zoom par défaut;
    \fn CYScopeCapture::zoom11()
 */
void CYScopeCapture::zoom11()
{
  setAxisAutoScale(yLeft , true);
  setAxisAutoScale(yRight, true);
  refresh();
  refresh();
}

/*!  Activation du zoom par sélection;
    \fn CYScopeCapture::zoom(bool val)
 */
void CYScopeCapture::zoom()
{
  mZooming = true;
  QHashIterator<int, CYScopeAxis *> it(scopeAxis);
  while (it.hasNext())
  {
    it.next();
    CYScopeAxis *axis = it.value();
    if (axis)
      axis->setAutoScale(false);
  }
  mPickerZoom->setEnabled(mZooming);
}

/*!  Zoom avant suivant le coefficient \a mZoomCoef.
    \fn CYScopeCapture::zoomIn()
 */
void CYScopeCapture::zoomIn()
{
  zoomIn(mZoomCoef);
}

/*!  Zoom avant suivant le coefficient \a coef.
    \fn CYScopeCapture::zoomIn(double coef)
 */
void CYScopeCapture::zoomIn(double coef)
{
  if (coef <= 0.0)
    return;

  QHashIterator<int, CYScopeAxis *> it(scopeAxis);
  while (it.hasNext())
  {
    it.next();
    CYScopeAxis *axis = it.value();
    if (axis)
      axis->zoomIn(coef);
  }

  changeRangeX();
  changeRangeY();
}

/*!  Zoom arrière suivant le coefficient \a mZoomCoef.
    \fn CYScopeCapture::zoomOut()
 */
void CYScopeCapture::zoomOut()
{
  zoomOut(mZoomCoef);
}

/*!  Zoom arrière suivant le coefficient \a coef.
    \fn CYScopeCapture::zoomOut(double coef)
 */
void CYScopeCapture::zoomOut(double coef)
{
  if (coef <= 0.0)
    return;

  QHashIterator<int, CYScopeAxis *> it(scopeAxis);
  while (it.hasNext())
  {
    it.next();
    CYScopeAxis *axis = it.value();
    if (axis)
      axis->zoomOut(coef);
  }

  changeRangeX();
  changeRangeY();
}

void CYScopeCapture::setCursorX1(double val)
{
  setMarkerXPos(marqueurXcur1, val);
  axisCursor1(mCursorX)->setVal(val);
  scopeAxis[mCursorX]->transformValToPos();
  k_curseur = 1;
  Yft(posXcur1);
}

void CYScopeCapture::setCursorY1(double val)
{
  setMarkerYPos(marqueurYcur1, val);
  axisCursor1(mCursorY)->setVal(val);
  scopeAxis[mCursorY]->transformValToPos();
}

void CYScopeCapture::setCursorX2(double val)
{
  setMarkerXPos(marqueurXcur2, val);
  axisCursor2(mCursorX)->setVal(val);
  scopeAxis[mCursorX]->transformValToPos();
  k_curseur = 2;
  Yft(posXcur2);
}

void CYScopeCapture::setCursorY2(double val)
{
  setMarkerYPos(marqueurYcur2, val);
  axisCursor2(mCursorY)->setVal(val);
  scopeAxis[mCursorY]->transformValToPos();
}

