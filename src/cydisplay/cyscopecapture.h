/***************************************************************************
                          cyscopecapture.h  -  description
                             -------------------
    début                  : Mon Mar 24 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYSCOPECAPTURE_H
#define CYSCOPECAPTURE_H

// QT
#include <QPainter>
#include <QDateTime>
#include <QHBoxLayout>
#include <QString>
#include <QShowEvent>
#include <QGridLayout>
#include <QFocusEvent>
#include <QFrame>
#include <QMouseEvent>
#include <QKeyEvent>
// QWT
#include "qwt_plot_renderer.h"
// CYLIBS
#include "cyscopeplotter.h"

class CYString;
class CYScopeSetup;
class CYS16;
class CYNumDisplay;
class QwtPlotZoomer;
class CYPlotPicker;
class CYScopeCaptureLegend;

/** Capture d'un traceur d'oscilloscope (CYScopePlotter) utilisé lors
  * de l'analyse des courbes (CYScopeAnalyse).
  * @short Capture de traceur d'oscilloscope.
  * @author Gérald LE CLEACH.
  */

class CYScopeCapture : public CYScopePlotter
{
  Q_OBJECT

  Q_ENUMS(Cursor)

  friend class CYScope;

public:
  CYScopeCapture(CYScope *scope, QWidget *parent=0, const QString &name=0, int timerInterval=2000, CYDB *db=0, CYScope::Mode mode=CYScope::Time, CYScope::SamplingMode sample=CYScope::Continuous);
  ~CYScopeCapture();

  /** Capture les courbes. */
  void captureCurves(CYScopePlotter *s);
  /** Capture les courbes en mode CYScope::Time. */
  void captureCurvesTimeMode(CYScopePlotter *s);
  /** Capture les courbes en mode CYScope::XY. */
  void captureCurvesXYMode(CYScopePlotter *s, int xAxis);

  virtual void keyPressEvent(QKeyEvent *e);

  enum Cursor
  {
    /** Curseur 1 */
    Cursor1,
    /** Curseur 2 */
    Cursor2,
    /** Delta entre les 2 curseurs */
    Delta,
    /** Inverse du Delta entre les 2 curseurs */
    IDelta
  };

  long marqueurXcur1;
  long marqueurYcur1;
  long marqueurXcur2;
  long marqueurYcur2;

  /** Date et heure de la capture. */
  QDateTime dateTime;

  /** @return la couleur du curseur \a cursor. */
  QColor cursorColor(Cursor cursor);
  /** Imprime les légendes.
      * @param nb Nombre de légendes de courbes déjà imprimés. */
  virtual void printLegends(QPainter *p, QRectF *r, int top, int &nb);
  virtual int fontSize();
  CYF64 *axisCursor1(QwtPlot::Axis axis);
  CYF64 *axisCursor2(QwtPlot::Axis axis);
  CYF64 *axisCursorD(QwtPlot::Axis axis);
  CYF64 *axisCursorID(QwtPlot::Axis axis);
  bool axisCursors();
  void printCursorAxis(QPainter *p, QRectF &rect, QRectF &brect, Cursor cursor, QwtPlot::Axis axis1, QwtPlot::Axis axis2=QwtPlot::axisCnt);
  void printCursorSignal(QPainter *p, QRectF &rect, QRectF &brect, CYScopeSignal *signal);
  void printCursorSignal(QPainter *p, QRectF &rect, QRectF &brect, CYScopeSignal *signal, Cursor cursor);

  /** Affichage des légendes. */
  virtual void setLegendPosition( const int val );


public slots:
  virtual void plotMousePressed(const QMouseEvent &e);
  virtual void plotMouseMoved(const QPoint &pos);
  virtual void plotMouseReleased(const QMouseEvent &e);

  void printCurves(QPainter *p,QRectF *rPlot);
  int  getCursor() { return k_curseur; }
  void initPosCursor(const int div = 6);
  void moveCursorX(const bool left = false, const bool fast = false);
  void moveCursorY(const bool down = false, const bool fast = false);
  void showCursors(const bool enable = true);
  void permutCursorsOnOff();

  void settings();
  void applySettings();
  virtual void curveClicked(long key);

  virtual void zoom11();
  virtual void zoom();
  virtual void zoomIn();
  virtual void zoomIn( double coef );
  virtual void zoomOut();
  virtual void zoomOut( double coef );

  /** Positionnne le curseur 1 en X suivant la valeur \a val. */
  void setCursorX1(double val);
  /** Positionnne le curseur 1 en Y suivant la valeur \a val. */
  void setCursorY1(double val);
  /** Positionnne le curseur 2 en X suivant la valeur \a val. */
  void setCursorX2(double val);
  /** Positionnne le curseur 2 en Y suivant la valeur \a val. */
  void setCursorY2(double val);

signals:
  void followValue(const int curve, const double value);
  void updateCursorLegends();

protected:
  virtual void showEvent( QShowEvent *e );

  /** Ajoute un signal en Y. */
  virtual void addYSignal(int id, CYScopeSignal *signal);

protected:
  void focusInEvent(QFocusEvent *) { setFrameShadow(QFrame::Sunken); }
  void focusOutEvent(QFocusEvent *) { setFrameShadow(QFrame::Raised); }
  /** Recherche les valeurs des courbes pour un instant donné. */
  void Yft(const int posX);
  /** Transforme les coordonnées graphiques en valeurs de curseurs. */
  void transformPosToVal();
  /** Transforme les valeurs des curseurs en coordonnées graphiques. */
  void transformValToPos();
  void updateCursorValues(const QPoint point);

  void initYfxDisplay(CYFrame *frame, CYData *data);

  int k_curseur;
  int step;
  int faststep;

  QColor mCursor1Color;
  QColor mCursor2Color;

  QPen penCur1On;
  QPen penCur1Off;
  QPen penCur2On;
  QPen penCur2Off;

  bool mInitSimple;
  bool mInitDouble;
  bool mInitFollow;

  bool mInitMove;

  CYScopeSetup *mSetup;

  QwtPlot::Axis mCursorX;
  QwtPlot::Axis mCursorY;

  QHash<QString, CYNumDisplay*> displayUnit;

  CYFrame     *mAxisFrame;
  QGridLayout *mAxisGrid;
  CYPlotPicker *mPickerZoom;

  CYScopeCaptureLegend *mCaptureLegend;

protected:
  virtual void initDB();
protected slots:
  virtual void lgdClicked();
};

#endif
