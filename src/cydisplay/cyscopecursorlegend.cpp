//
// C++ Implementation: cyscopecursorlegend
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyscopecursorlegend.h"

// QWT
#include "qwt_legend_label.h"
#include "qwt_painter.h"
// CYLIBS
#include "cycore.h"
#include "cyf64.h"
#include "cycolor.h"
#include "cyframe.h"
#include "cyscopecapturelegend.h"
#include "cyscopesignal.h"
#include "cynumdisplay.h"

CYScopeCursorLegend::CYScopeCursorLegend(const CYScopeCaptureLegend *parent, CYScopeSignal *signal)
 : CYFrame(0),
   mLegend( parent ),
   mSignal( signal )
{
  if (!signal)
    return;

  setObjectName(signal->objectName());
  mCapture = signal->scopeCapture();

  mVBL = new QVBoxLayout;
  setLayout(mVBL);
  mVBL->setSpacing(1);

  QwtLegendLabel *label = new QwtLegendLabel;
  label->setText(signal->legend(true));
  label->setToolTip(signal->legend(true));
  QColor c = signal->pen.color();

  if(c.isValid())
  {
    int h=12;
    int w=32;
    QImage image( w, h, QImage::Format_ARGB32 );
    QPainter painter( &image );
    painter.fillRect(0,0,w,h,mCapture->backgroundColor()->val());
    painter.setPen(signal->pen);
    painter.drawLine(0,h/2,w,h/2);
    painter.end();

    QPixmap pixmap = QPixmap::fromImage(image);
    label->setIcon(pixmap);
  }
  label->setItemMode( mLegend->defaultItemMode() );
  mVBL->addWidget(label);

  initYfxDisplay(signal->cursor(CYScopeCapture::Cursor1));
  initYfxDisplay(signal->cursor(CYScopeCapture::Cursor2));
  initYfxDisplay(signal->cursor(CYScopeCapture::Delta));

  connect(mCapture, SIGNAL(updateCursorLegends()), this, SLOT(update()));

  mTimer->start(250);
}


CYScopeCursorLegend::~CYScopeCursorLegend()
{
}

/*! Initilalise les afficheurs d'une donnée Yfx d'un curseur
    \fn CYScopeCaptureLegend::initYfxDisplay(CYData *data)
 */
void CYScopeCursorLegend::initYfxDisplay(CYData *data)
{
  if (!data)
    return;

  QHBoxLayout *yBox = new QHBoxLayout;
  yBox->setSpacing(1);


  CYNumDisplay *display;

  display = new CYNumDisplay;
  display->setShowLabel(true);
  display->setSuffix("\t:");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mCapture->db());
  display->setDataName( data->dataName());
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  display->setAlwaysOk(true);
  yBox->addWidget(display);
  yBox->setStretchFactor(display, 1);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignRight);
  display->setLocaleDB(mCapture->db());
  display->setHideIfNoData( true );
  display->setShowUnit(false);
  display->setDataName( data->dataName() );
  display->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
  display->setAlwaysOk(true);
  yBox->addWidget(display);
  yBox->setStretchFactor(display, 6);

  display = new CYNumDisplay;
  display->setShowLabel(true);
  //   display->setSuffix("\t:");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mCapture->db());
  display->setEnableColor(data->enableColor());
  display->setText( data->unit() );
  yBox->addWidget(display);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  display->setAlwaysOk(true);
  yBox->setStretchFactor(display, 6);

  mVBL->addLayout(yBox);
}

/*!
    \fn CYScopeCursorLegend::drawIdentifier(QPainter *, const QRect &) const
 */
void CYScopeCursorLegend::drawIdentifier(QPainter *painter, const QRect &rect) const
{
  if ( rect.isEmpty() )
      return;
return;
  CYF64 *data = (CYF64 *)core->findData(mCapture->db(), objectName());

  painter->save();

  CYColor *backgroundColor = (CYColor *)core->findData(mCapture->db(), "BACKGROUND_COLOR");
  QBrush brush = QBrush( backgroundColor->val() );

  QwtPainter::fillRect(painter, rect, brush);

  if (objectName()==QString("X1_BOTTOM"))
  {
    painter->setPen( mCapture->marker(mCapture->marqueurXcur1)->linePen() );
    QwtPainter::drawLine(painter, rect.center().x(), rect.top(), rect.center().x(), rect.bottom());
  }
  else if (objectName()==QString("X2_BOTTOM"))
  {
    painter->setPen( mCapture->marker(mCapture->marqueurXcur2)->linePen() );
    QwtPainter::drawLine(painter, rect.center().x(), rect.top(), rect.center().x(), rect.bottom());
  }
  else if (objectName()==QString("DX_BOTTOM"))
  {
    painter->setPen( mCapture->marker(mCapture->marqueurXcur1)->linePen() );
    QwtPainter::drawLine(painter, rect.width()*2/8, rect.top(), rect.width()*2/8, rect.bottom());
    painter->setPen( mCapture->marker(mCapture->marqueurXcur2)->linePen() );
    QwtPainter::drawLine(painter, rect.width()*7/8, rect.top(), rect.width()*7/8, rect.bottom());
/*    painter->setPen( QPen( data->enableColor() ) );
    QwtPainter::drawLine(painter, rect.width()*2/8, rect.center().y(), rect.width()*7/8, rect.center().y());*/
  }
  else if (objectName()==QString("Y1_LEFT"))
  {
    painter->setPen( mCapture->marker(mCapture->marqueurYcur1)->linePen() );
    QwtPainter::drawLine(painter, rect.left(), rect.center().y(), rect.right(), rect.center().y());
  }
  else if (objectName()==QString("Y2_LEFT"))
  {
    painter->setPen( mCapture->marker(mCapture->marqueurYcur2)->linePen() );
    QwtPainter::drawLine(painter, rect.left(), rect.center().y(), rect.right(), rect.center().y());
  }
  else if (objectName()==QString("DY_LEFT"))
  {
    painter->setPen( mCapture->marker(mCapture->marqueurYcur1)->linePen() );
    QwtPainter::drawLine(painter, rect.left(), rect.height()*2/8, rect.right(), rect.height()*2/8);
    painter->setPen( mCapture->marker(mCapture->marqueurYcur2)->linePen() );
    QwtPainter::drawLine(painter, rect.left(), rect.height()*7/8, rect.right(), rect.height()*7/8);
/*    painter->setPen( QPen( data->enableColor() ) );
    QwtPainter::drawLine(painter, rect.center().x(), rect.height()*2/8, rect.center().x(), rect.height()*7/8);*/
  }
  else if (QString(objectName()).contains("_CURSOR"))
  {
    if (QString(objectName()).contains("_DELTA_CURSOR"))
    {
      painter->setPen( mCapture->marker(mCapture->marqueurXcur1)->linePen() );
      QwtPainter::drawLine(painter, rect.width()*2/8, rect.top(), rect.width()*2/8, rect.bottom());
      painter->setPen( mCapture->marker(mCapture->marqueurXcur2)->linePen() );
      QwtPainter::drawLine(painter, rect.width()*7/8, rect.top(), rect.width()*7/8, rect.bottom());
      painter->setPen( QPen( data->enableColor() ) );
      QwtPainter::drawLine(painter, rect.left(), rect.bottom(), rect.right(), rect.top());
    }
    else if (QString(objectName()).contains("_CURSOR1"))
    {
      painter->setPen( mCapture->marker(mCapture->marqueurXcur1)->linePen() );
      QwtPainter::drawLine(painter, rect.center().x(), rect.top(), rect.center().x(), rect.bottom());
      QString deltaName = data->objectName();
      deltaName.replace("_CURSOR1", "_DELTA_CURSOR");
      CYF64 *delta = (CYF64 *)core->findData( mCapture->db(), deltaName);
      painter->setPen( QPen( delta->enableColor() ) );
      QwtPainter::drawLine(painter, rect.left(), rect.bottom(), rect.right(), rect.top());
    }
    else
    {
      painter->setPen( mCapture->marker(mCapture->marqueurXcur2)->linePen() );
      QwtPainter::drawLine(painter, rect.center().x(), rect.top(), rect.center().x(), rect.bottom());
      QString deltaName = data->objectName();
      deltaName.replace("_CURSOR2", "_DELTA_CURSOR");
      CYF64 *delta = (CYF64 *)core->findData( mCapture->db(), deltaName);
      painter->setPen( QPen( delta->enableColor() ) );
      QwtPainter::drawLine(painter, rect.left(), rect.bottom(), rect.right(), rect.top());
    }
  }

  painter->restore();
}
