//
// C++ Implementation: cyscopeacquisition
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyscopeacquisition.h"

// QT
#include <QFileDialog>
#include <QLocale>
#include <QTextStream>
// CYLIBS
#include "cycore.h"
#include "cyscope.h"
#include "cyscopecapture.h"
#include "cyscopesignal.h"
#include "cyscopeaxis.h"
#include "cyu32.h"
#include "cys32.h"
#include "cyf64.h"
#include "cytime.h"
#include "cystring.h"
#include "cyaboutdata.h"
#include "cyapplication.h"

CYScopeAcquisition::CYScopeAcquisition(CYScope *scope, const QString &name, CYFlag *enable)
 : CYAcquisition(scope, name, name, enable),
   mScope(scope)
{
  mScopeCapture = 0;
  init();
}


CYScopeAcquisition::~CYScopeAcquisition()
{
}

/*! Exporter les courbes dans un fichier texte
    @param saveAs Par défaut vaut à \a true pour avoir la boîte de sélection du fichier, pour avoir automatiquement un nom de fichier il suffit de le mettre à \a false.
    \fn CYScopeAcquisition::exportCurves(bool saveAs)
 */
void CYScopeAcquisition::exportCurves(bool saveAs)
{
  exportCurves(true, saveAs);
}

/*! Exporter les courbes dans un fichier texte
  *  Enrichissement de \fn CYScopeAcquisition::exportCurves(bool saveAs) pour exporter l'acquisition déjà capturée.
    @param saveAs     Saisir \a true pour avoir la boîte de sélection du fichier et \a false pour avoir automatiquement un nom de fichier .
    @param newCapture Saisir \a true pour faire capturer avant d'exporter et \a false pour utiliser la dernière capture.
    \fn CYScopeAcquisition::exportCurves(bool newCapture, bool saveAs)
 */
void CYScopeAcquisition::exportCurves(bool saveAs, bool newCapture)
{
  if (newCapture)
    capture();

  mSaveAs = saveAs;
  mUseNewFile  = true;
  save();
  delete mScopeCapture;
  mScopeCapture = 0;
}

void CYScopeAcquisition::currentFile()
{
  if (mFile)
    delete mFile;
  mFile = 0;

  if (!mForceFileName.isEmpty())
  {
    mFileName = mForceFileName;
    mUseNewFile  = false;
    newFile();
    return;
  }
  else if (mSaveAs)
  {
    setNameDir(tr("curves"));
    QString fileName = QFileDialog::getSaveFileName((QWidget*)mScope->parent(), tr("Saving under"), mDir->absolutePath(), QString("*.%1").arg(extensionFileName->val()));
    if (fileName.isEmpty())
      return;

    mFileName = fileName;

    mUseNewFile  = false;
    newFile();
    return;
  }
  else if (maxNbFiles->val()==1)
  {
    mFileName = QString("%1/%2.%3").arg(mDir->absolutePath()).arg(prefixFileName->val()).arg(extensionFileName->val());
    newFile();
    return;
  }
  else if (mUseNewFile)
  {
    mNbLinesFile = 0;
    mUseNewFile  = false;
    mFileNum = 1;
    mFileName = QString("%1/%2_%3.%4").arg(mDir->absolutePath()).arg(prefixFileName->val()).arg(mFileNum).arg(extensionFileName->val());
    QFile file;
    while (file.exists(mFileName))
    {
      mFileNum++;
      mFileName = QString("%1/%2_%3.%4").arg(mDir->absolutePath()).arg(prefixFileName->val()).arg(mFileNum).arg(extensionFileName->val());
    }
    newFile();
    return;
  }
  if (mFile)
    delete mFile;
  mFile = new QFile(mFileName);
}

void CYScopeAcquisition::capture()
{
  // Création du nouveau graphe
  mScopeCapture = new CYScopeCapture( mScope, (QWidget *)mScope->parent(), "capture", mScope->plot()->getTimerInterval(), 0, mScope->plot()->modeVal(), mScope->plot()->samplingModeVal());

  // Recopie les données du graphe source
  mScopeCapture->captureCurves(mScope->plot());
  mScopeCapture->copy(mScope->plot());

  double time=0.0;
  QLocale locale = QLocale::system();
  // Supprime le séparateur de groupe tel que celui du millier
  // Par exemple en Français "1 000,2" devient "1000,2"
  locale.setNumberOptions(locale.numberOptions() | QLocale::OmitGroupSeparator);
  if (mScopeCapture->modeVal()==CYScope::Time)
  {
    int nb = mScopeCapture->timeScopeAxis()->xTime.size();
    for ( int i = 0; i < nb ; i++ )
		{
      CYScopeAxis *axis=mScopeCapture->timeScopeAxis();
      double newTime = axis->xTime[i];
      if (newTime<axis->minVal())
				continue;
      if (newTime>axis->maxVal())
        break;

      if ((newTime-time)<(double)acqTimer->val()*axis->step())
        continue;

      time = newTime;
      QString *line = new QString;
      bool l2r = ( mScopeCapture->timeScopeAxis()->leftData()->val() < mScopeCapture->timeScopeAxis()->rightData()->val() ) ? true : false;
      if (!l2r)
      	time = -1*time;
      line->append(locale.toString(time, 'f', mScopeCapture->timeScopeAxis()->nbDec()));
			line->append(separator->val());
      QListIterator<CYScopeSignal *> it(mScopeCapture->ySignal);
			while (it.hasNext())
			{
				CYScopeSignal *signal = it.next();
        if (mScopeCapture->samplingModeVal()==CYScope::Continuous)
					line->append(locale.toString(signal->bufferVal(i)));
        if (mScopeCapture->samplingModeVal()==CYScope::Bursts)
					line->append(locale.toString(signal->burstsBufferVal(i)));
				line->append(separator->val());
      }
			line->append("\n");
      addLine(line, l2r);
		}
  }
  else if (mScopeCapture->modeVal()==CYScope::XY)
  {
    CYScopeSignal *xSignal = mScope->plot()->xSignal(QwtPlot::xBottom);

    if (xSignal)
    {
      if (mScope->plot()->samplingModeVal()==CYScope::Continuous)
      {
        int nb = xSignal->bufferSize();
        for (int i=0; i<nb; i++)
        {
          QString *line = new QString;
          line->append(locale.toString(xSignal->bufferVal(i), 'f', xSignal->data->nbDec()));
          line->append(separator->val());
          QListIterator<CYScopeSignal *> it(mScope->plot()->ySignal);
          while (it.hasNext())
          {
            CYScopeSignal *signal = it.next();
            line->append(locale.toString(signal->bufferVal(i)));
            line->append(separator->val());
          }
          line->append("\n");
          addLine(line);
        }
      }
      if (mScope->plot()->samplingModeVal()==CYScope::Bursts)
      {
        for (int i=0; i<xSignal->burstsBufferSize(); i++)
        {
          QString *line = new QString;
          line->append(locale.toString(xSignal->burstsBufferVal(i), 'f', xSignal->data->nbDec()));
          line->append(separator->val());
          QListIterator<CYScopeSignal *> it(mScope->plot()->ySignal);
          while (it.hasNext())
          {
            CYScopeSignal *signal = it.next();
            line->append(locale.toString(signal->burstsBufferVal(i)));
            line->append(separator->val());
          }
          line->append("\n");
          addLine(line);
        }
      }
    }
  }

  if (mCurrentBuffer==1)
    mCurrentBuffer = 2;
  else
    mCurrentBuffer = 1;  
}


/*! Sauvegarde la configuration d'acquisition.
    \fn CYScopeAcquisition::saveSetup()
 */
bool CYScopeAcquisition::saveSetup()
{
  int res=mScope->saveSettings();
  return (res==1) ? true : false;
}


/*!
    \fn CYScopeAcquisition::init()
 */
void CYScopeAcquisition::init()
{
  CYAcquisition::init();
  mSaveAs = true;
  mNoMaxLinesFile= true;

  acqTimer = new CYTime(mDB, "AQC_TIMER", new u32, "100ms", "10ms", "10000ms", 1.0);
  acqTimer->setLabel(tr("Acquisition period"));

  mDB->setDOMTagDatas("AcquisitionData");
}


/*! Création de l'en-tête du fichier d'acqusition.
    \fn CYScopeAcquisition::header(QFile *file, QDateTime startTime, QDateTime startTime, QList<CYAcquisitionData*> headerList, QList<CYAcquisitionData*> acquisitionList, int poste)
 */
void CYScopeAcquisition::header(QFile *file, QDateTime startTime, QList<CYAcquisitionData*> headerList, QList<CYAcquisitionData*> acquisitionList, int poste)
{
  Q_UNUSED(startTime)
  Q_UNUSED(headerList)
  Q_UNUSED(acquisitionList)
  Q_UNUSED(poste)

  CYAboutData *data = cyapp->aboutData();

  QTextStream stream(file);
  stream << QString("%1%2%3%4%5%6%7\n")
            .arg("Fichier_crb").arg(separator->val())
            .arg(core->customerName()).arg(separator->val())
            .arg(core->designerRef()).arg(separator->val())
            .arg(data->internalVersion());

  QDate date = mScopeCapture->dateTime.date();
  QTime time = mScopeCapture->dateTime.time();
  startDateTimeString->setVal(QString("%1 %2"));
  stream << QString(tr("Date:")+"\t%1 %2").arg(date.toString(Qt::ISODate)).arg(time.toString(Qt::ISODate));

  stream << "\n";
  stream << tr("Title")     << separator->val() << mScope->title() << "\n";
  stream << tr("X axis")    << separator->val() << mScope->plot()->axisTitle(QwtPlot::xBottom).text()    << "\n";
  stream << tr("X min")     << separator->val() << mScope->plot()->scopeAxis[QwtPlot::xBottom]->minVal() << "\n";
  stream << tr("X max")     << separator->val() << mScope->plot()->scopeAxis[QwtPlot::xBottom]->maxVal() << "\n";
  stream << tr("X unit")    << separator->val() << mScope->plot()->scopeAxis[QwtPlot::xBottom]->unit()   << "\n";
  stream << tr("X decimal") << separator->val() << mScope->plot()->scopeAxis[QwtPlot::xBottom]->nbDec()  << "\n";

  if (mScope->hasMultipleFormats())
  {
    int nb = mScope->datas.count();
    stream << tr("Y axis")    ;
    for (int i = 0; i<nb; i++)
      stream << separator->val() << mScope->plot()->curveLegend(i);
    stream << "\n";
    stream << tr("Y min")    ;
    for (int i = 0; i<nb; i++)
      stream << separator->val() << mScope->plot()->ySignal[i]->min();
    stream << "\n";
    stream << tr("Y max")    ;
    for (int i = 0; i<nb; i++)
      stream << separator->val() << mScope->plot()->ySignal[i]->max();
    stream << "\n";
    stream << tr("Y unit")  ;
    for (int i = 0; i<nb; i++)
      stream << separator->val() << mScope->datas[i]->unit;
    stream << "\n";
    stream << tr("Y decimal");
    for (int i = 0; i<nb; i++)
      stream << separator->val() << mScope->datas[i]->data->nbDec();
    stream << "\n";
    stream << tr("Y color");
    for (int i = 0; i<nb; i++)
      stream << separator->val() << mScope->plot()->curveColor(i).name();
    stream << "\n";
  }

  if (mScopeCapture->modeVal()==CYScope::Time)
  {
    stream << QString("%1 (%2)%3").arg(tr("Time")).arg(mScope->plot()->timeScopeAxis()->unit()).arg(separator->val());
  }
  else if (mScopeCapture->modeVal()==CYScope::XY)
  {
    CYScopeSignal *xSignal = mScope->plot()->xSignal(QwtPlot::xBottom);
    if (xSignal)
      stream << QString("%1 (%2)%3").arg(xSignal->label).arg(xSignal->unit).arg(separator->val());
  }
  QListIterator<CYScopeSignal *> it(mScope->plot()->ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *signal = it.next();
    stream << QString("%1 (%2)%3").arg(signal->label).arg(signal->unit).arg(separator->val());
  }
  stream << "\n";
}
