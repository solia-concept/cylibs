/***************************************************************************
                          cyscopesignal.h  -  description
                             -------------------
    début                  : mer sep 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYSCOPESIGNAL_H
#define CYSCOPESIGNAL_H

// QT
#include <QVector>
// QWT
#include "qwt_plot.h"
// CYLIBS
#include "cyscope.h"
#include "cydisplayitem.h"
#include "cyscopecapture.h"

class CYF64;
class CYScopeAxis;

/** @short Voie de mesure d'un oscilloscope.
  * @author Gérald LE CLEACH.
  */

class CYScopeSignal : public CYDisplayItem
{
  Q_OBJECT

public:
    CYScopeSignal(QObject *parent, CYData *data, int id, QString l, QPen pen, double coefficient, QwtPlot::Axis axis, bool trigger=false);
    ~CYScopeSignal();

    /** @return l'axe des y relatif. */
    QwtPlot::Axis axis() { return mAxis; }

    /** @return la valeur courante de la donnée. */
    double *val(CYScope::SamplingMode mode=CYScope::Continuous);

    /** @return \a true si la donnée est en cours de lecture. */
    bool dataReading();
    /** Copie le buffer \a buf dans le buffer du signal. */
    void copieBuffer(QVector<double> buf, CYScope::SamplingMode mode);
    /** @return une copie du buffer à partir de l'index \a offset. */
    QVector<double> buffer(int offset, CYScope::SamplingMode mode);
    /** @return l'index de la valeur courante dans le buffer. */
    virtual int idBuffer();
    /** @return une copie du buffer par rafale à partir de l'index \a offset. */
    QVector<double> burstsBuffer(int offset);

    /** Autorise ou non la bufferisation des valeurs. */
    void setEnableBuffering(bool val);
    /** @return \a true si la bufferisation des valeurs est autorisée. */
    bool enableBuffering() { return mEnableBuffering; }
    virtual int bufferSize();
    virtual int burstsBufferSize();
    virtual double bufferVal(int id);
    virtual double burstsBufferVal(int id);
    virtual int resetNb(CYScope::SamplingMode mode);
    bool isReseted(CYScope::SamplingMode mode);
    bool isTriggered();

    virtual CYScopeSignal * capture();
    /** @return l'index buffer au moment de la copie */
    int copyId() { return mCopyId; }
    bool hidden();
    CYScope * scope();
    CYScopeCapture *scopeCapture() { return mScopeCapture; }

    void initCaptureDatas();

    /** @return la donnée contenant la valeur du curseur. */
    CYF64 *cursor(CYScopeCapture::Cursor cursor);

    /** @return l'axe de l'oscilloscope relatif à ce signal. */
    CYScopeAxis * scopeAxis();
    QString legend(bool withGroup=false, bool withUnit=true);

    /** Démarre le trigger
     * @param level seuil de déclenchement du trigger
     * @param pos   pré-trigger
     * @param slop  choix de rampe de déclenchement */
    virtual void startTrigger(double level, double post, Cy::Change slope);
    /** Contrôle l'état du trigger et calcul en cas de détection
      * les index de reset du signal source
      * de déclenchement du trigger.
      * @return true si le trigger est déclenché. */
    virtual bool triggerState();
    /** @return true s'il s'agit du signal source de déclenchement du trigger. */
    bool isTriggerSource() { return mTriggerSource; }
    /** @return l'index buffer de la valeur de déclenchement du trigger. */
    virtual int triggerIdBuffer();
    /** @return le compteur de remplissage du buffer trigger après déclenchement. */
    int triggerBufferCpt();

public slots:
    virtual void reset();
    virtual void buffering();
    void hide();
    void show();
    /** Saisie le coefficient d'échelle du signal en Y. */
    virtual void setYCoef( double coef );
    /** Saisie l'offset d'échelle du signal en Y. */
    virtual void setYOffset( double offset );
    /** Saisi le signal défini comme signal source du trigger */
    virtual void setTriggerSignal(CYScopeSignal *source);
    /** Arrête le trigger */
    virtual void stopTrigger();
    /** Le défini comme signal source du trigger */
    virtual void setTriggerSource(bool val) { mTriggerSource = val; }

signals:
    void enableCurve( int key );
    void disableCurve( int key );

protected: // Protected attributes
    QVector<double> mBuffer;
    QVector<double> mBurstsBuffer;
    QVector<double> mCapture;

    bool mEnableBuffering;
    int mResetNb;
    int mResetId1;
    int mResetId2;
    int mCopyId;
    int mBurstsResetNb;
    int mBurstsResetId1;
    int mBurstsResetId2;
    bool mHidden;
    QwtPlot::Axis mAxis;

    CYScopeCapture * mScopeCapture;
    QHash<int, CYF64*> mCursors;

    CYScopeSignal *mTriggerSignal;
    bool mTriggerSource;
    bool mTriggerState;
    int mTriggerNb;
    int mTriggerId1;
    int mTriggerId2;
};

#endif
