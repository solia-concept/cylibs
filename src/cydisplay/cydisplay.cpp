/***************************************************************************
                          cydisplay.cpp  -  description
                             -------------------
    début                  : ven mar 14 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cydisplay.h"

// QT
#include <QContextMenuEvent>
#include <QTextStream>
#include <QTimerEvent>
#include <QLabel>
#include <QEvent>
#include <QResizeEvent>
#include <QMenu>
#include <QFocusEvent>
#include <QMouseEvent>
#include <QEvent>
#include <QToolTip>
// CYLIBS
#include "cycore.h"
#include "cydb.h"
#include "cynethost.h"
#include "cydata.h"
#include "cyf64.h"
#include "cys16.h"
#include "cytime.h"
#include "cyflag.h"
#include "cystring.h"
#include "cyanalysesheet.h"
#include "cyanalysecell.h"
#include "cyanalysecellremoveevent.h"
#include "cydisplayitem.h"
#include "cydisplaystyle.h"
#include "cydisplaytimer.h"
#include "cydisplaytimersetup.h"
#include "cydatastabledisplaycell.h"
#include "cydatastable.h"
#include "cydisplaysatusindicator.h"
#include "cyapplication.h"
#include "cyglobal.h"

CYDisplay::CYDisplay(QWidget *parent, const QString &name, const QString &title, bool nf)
  : CYFrame(parent, name)
{
  mVersionCYDOM = core->version();

  mDeleting= false;
  mCreating= true;
  mUpdatingFormat = false;
  mRefreshNotVisible = false;

  mStatus = (Status)-1;
  mStatusIndicator       = 0;
  mStatusIndicatorEnabled = true;

  if (nf)
  {
    mFrame = 0;
    setContentsMargins(0,0,0,0);
  }
  else
  {
    mFrame = new QGroupBox(this);
    Q_CHECK_PTR(mFrame);
  }
  setContextMenuPolicy(Qt::CustomContextMenu);
   connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
           this, SLOT(contextMenu(const QPoint&)));

  init(title);
}

CYDisplay::~CYDisplay()
{
  if (mFrame)
    delete mFrame;
  delete mDB;
}

void CYDisplay::init(QString title)
{
  mCell  = 0;
  nbItem = 0;

  mModified      = false;
  mForcing       = false;
  mCreatingFromDOM = false;
  plotterWdg     = 0;
  mRefreshStop   = true;
  mRefreshTimer  = new QTimer(this);

  pauseOnHide       = false;
  pausedWhileHidden = false;
  mShowPopupMenu    = true;

  // TOCHECK QT5
  mBoxLayout= new QVBoxLayout;
  if (!mFrame)
  {
    this->setLayout(mBoxLayout);
    mBoxLayout->setMargin(0);
  }
  else
    mFrame->setLayout(mBoxLayout);

  if (parent() && parent()->inherits("CYAnalyseCell"))
  {
    mCell = (CYAnalyseCell *)parent();
    connect(this, SIGNAL(split(int)), mCell, SLOT(split(int)));
  }

  if (!core)
  {
    mDB = new CYDB(new CYNetHost(this, QString("Host_%1").arg(objectName())), QString("DB_%1").arg(objectName()), -1, 0, false);
    mStyle = new CYDisplayStyle(this, "mStyle");
  }
  else
  {
    mDB = new CYDB(core->localHost(), QString("DB_%1").arg(objectName()), -1, 0, false);
    mStyle = core->style;
  }
  mDB->setHelpDesignerValue(false);

  mRefreshTime = new CYTime(mDB, "REFRESH_TIME", new u32, "250ms", "40ms", "3600000ms");

  mTitle = new CYString(mDB, "TITLE", new QString(title));
  mTitle->setLabel(tr("Title"));

  mTitleAuto = new CYFlag(mDB, "TITLE_AUTO", new flg, true);
  mTitleAuto->setLabel(tr("Automatic title"));
  mTitleAuto->setHelp(0, tr("Edit the data's label to change the title."), tr("Enter the title in the box below."));

  mTitleWithUnit = new CYFlag(mDB, "TITLE_WITH_UNIT", new flg, true);
  mTitleWithUnit->setLabel(tr("Add unit to the title"));

  mDB->setUnderGroup(tr("Style"));
  if (core)
    mFontSize = new CYS16(mDB, "FONT_SIZE", new s16, core->style->getFontSize(), 1, 500, 0);
  else
    mFontSize = new CYS16(mDB, "FONT_SIZE", new s16, 10, 1, 500, 0);
  mFontSize->setLabel(tr("Font size"));
  mDB->setUnderGroup(0);

  setTitle(mTitle->val());
  setMinimumSize(32, 32);
  setModified(false);

  if (parent() && parent()->inherits("CYDatasTableDisplayCell"))
  {
    CYDatasTableDisplayCell *cell = (CYDatasTableDisplayCell *)parent();
    /* Tous les clics RMB dans la forme de boîte seront gérés par CYDisplay::eventFilter. */
    cell->table()->installEventFilter(this);
  }
  /* Tous les clics RMB dans la forme de boîte seront gérés par CYDisplay::eventFilter. */
  if (mFrame)
    mFrame->installEventFilter(this);

  /* Permet d'appeler updateToolTip() dans le cas où la classe dérivée n'y arrive pas. */
  updateToolTip();
  setFocusPolicy(Qt::StrongFocus);
}

void CYDisplay::registerPlotterWidget(QWidget *plotter)
{
  plotterWdg = plotter;
  /** Si l'afficheur se trouve dans une table de données,
  c'est celle-ci qui lui donne l'ordre de rafraîchissement donc pas de indicateur de status sur un afficheur d'une table de données. */
  if (parent() && !parent()->inherits("CYDatasTableDisplayCell"))
  {
    mStatusIndicator = new CYDisplaySatusIndicator(plotterWdg, QString("%1_StatusIndicator").arg(objectName()), this);
  }
}

void CYDisplay::applySettings()
{
  setTitle(mTitle->val());
  validate();
}

void CYDisplay::setTitle(const QString &t)
{
  if (!mFrame)
    return;

  mTitle->setVal( t );

  /* En Changeant le titre de la trame il est possible que la taille de la trame augmente
   * et par conséquent casse le positionnement. Pour éviter ceci,nous sauvons la taille
   * d'origine et la restaurons après avoir mis le titre de la trame. */
  QSize s = mFrame->size();

  QString tmp;
  if (!mUnit.isEmpty() && mTitleWithUnit->val())
    tmp = QString(mTitle->val() + " [" + mUnit + "]");
  else
    tmp = mTitle->val();

  emit titleChanged(tmp);
  mFrame->setTitle(tmp);
  mFrame->setGeometry(0, 0, s.width(), s.height());
}

QString CYDisplay::title()
{
  return mTitle->val();
}

QString CYDisplay::boxTitle() const
{
  return mTitle->val();
}

void CYDisplay::setBoxTitle(const QString &title)
{
  setTitle(title);
}

void CYDisplay::setModified(bool mfd)
{
  if ( mUpdatingFormat )
    return;

  if (mfd != mModified)
  {
    mModified = mfd;
    if (mModified)
      emit displayModified(mModified);
  }
}

//-----------------------------------------------------
//                 GESTION DES DONNNEES
//-----------------------------------------------------

bool CYDisplay::addGroup(QString group)
{
  Q_UNUSED(group)
  return false;
}

bool CYDisplay::addData(const QString &dataName, QString label)
{
  if (!core)
    return false;

  if (dataName.isEmpty())
    return false;

  CYData *data = core->findData(dataName, false);
  if (!data)
    return false;

  return(addData(data, label));
}

bool CYDisplay::addData(CYData *data, QString label)
{
  CYDisplayItem *item = new CYDisplayItem(this, data, ++nbItem, label);

  if ( mTitleAuto->val() )
    setTitle( item->label );

  registerItem(item);
  return true;
}

bool CYDisplay::removeData(uint idx)
{
  unregisterItem(idx);
  nbItem--;
  return true;
}

void CYDisplay::registerItem(CYDisplayItem *ddi)
{
  if (!core)
    return;
//a_voir
//  /* Vérifie que nous avons bien une connexion d'établie avec l'hôte. Lorsqu'une feuille
//   * d'analyses à été enregistrée lors de l'arrivée de données, l'info de connexion n'est
//   * pas enregistrée dans celle-ci. Dans un tel cas l'utilisateur peut re-saisir l'info de
//   * connexion et la connexion sera établie. */
//  if (!core->engageHost(ddi->hostName))
//  {
//    QString msg = tr("Impossible to connect to \'%1\'!").arg(ddi->hostName);
//    CYMessageBox::error(this, msg);
//  }

/*  if (!inherits("CYDisplaySimple"))
  {
   */
  connect( ddi, SIGNAL( formatUpdated() ), this, SLOT( updateFormat() ) );
//   }

  datas.append(ddi);
}

void CYDisplay::unregisterItem(uint idx)
{
  CYDisplayItem *item = 0;
  item = datas.at(idx);
  if (item)
  {
    disconnect( item, SIGNAL( formatUpdated() ), this, SLOT( updateFormat() ) );
    if (datas.at(idx))
    {
      delete datas.takeAt(idx);
    }
  }
}

void CYDisplay::collectHosts(QStringList  &list)
{
  foreach (CYDisplayItem *d, datas)
    if (!list.contains(d->host))
      list.append(d->host);
}


//-----------------------------------------------------
//               GESTION DES EVENEMENTS
//-----------------------------------------------------

bool CYDisplay::eventFilter(QObject *o, QEvent *e)
{
  // Clique gauche
  if (e->type() == QEvent::MouseButtonRelease)
  {
    if (((QMouseEvent *)e)->button() == Qt::LeftButton)
      setFocus();
  }

  return CYFrame::eventFilter(o, e);
}

void CYDisplay::timerEvent(QTimerEvent *)
{
  if (isVisible())
    refresh();
}

void CYDisplay::resizeEvent(QResizeEvent *)
{
  if (mStatusIndicatorEnabled && mStatusIndicator)
    mStatusIndicator->showOrHide();

  if (!mFrame)
    return;
  mFrame->setGeometry(rect());
}

void CYDisplay::focusInEvent(QFocusEvent *)
{
  setFrameShadow(QFrame::Sunken);
}

void CYDisplay::focusOutEvent(QFocusEvent *)
{
  setFrameShadow(QFrame::Raised);
}

//-----------------------------------------------------
//                 GESTION DU TEMPS
//-----------------------------------------------------

void CYDisplay::setupTimer()
{
  ts = new CYDisplayTimerSetup(this, "CYDisplayTimerSetup");
  Q_CHECK_PTR(ts);
  ts->setLocaleDB(mDB);
  ts->exec();
  delete ts;
  changeRefreshTimer();
}

void CYDisplay::timerToggled(bool value)
{
  Q_UNUSED(value)
//  TODO QT3
//  ts->interval_sec->setEnabled(!value);
//  ts->interval_msec->setEnabled(!value);
}

void CYDisplay::changeRefreshTimer()
{
  if (!mRefreshStop)
    startRefresh();
}

void CYDisplay::stopRefresh()
{
  /** Si l'afficheur se trouve dans une table de données,
  c'est celle-ci qui lui donne l'ordre de rafraîchissement. */
  if (!parent()->inherits("CYDatasTableDisplayCell"))
  {
      disconnect(mRefreshTimer, SIGNAL(timeout()), this, SLOT(refresh()));
      mRefreshTimer->stop();
  }
  if ( !mRefreshStop )
  {
    mRefreshStop = true;
    setModified(true);
  }
  refreshStatus();
}

void CYDisplay::startRefresh()
{
  if (mRefreshStop)
  {
    /** Si l'afficheur se trouve dans une table de données,
    c'est celle-ci qui lui donne l'ordre de rafraîchissement. */
//    if (parent() && !parent()->inherits("CYDatasTableDisplayCell"))
    {
      connect(mRefreshTimer, SIGNAL(timeout()), this, SLOT(refresh()));
      mRefreshTimer->start(mRefreshTime->val());
    }
    mRefreshStop = false;
    setModified(true);
  }
  else
  {
    /** Si l'afficheur se trouve dans une table de données,
    c'est celle-ci qui lui donne l'ordre de rafraîchissement. */
//    if (parent() && !parent()->inherits("CYDatasTableDisplayCell"))
    {
      mRefreshTimer->start(mRefreshTime->val());
    }
    setModified(true);
  }
  refreshStatus();
}

void CYDisplay::setIsOnTop(bool onTop)
{
  if (!pauseOnHide)
    return;

  if (onTop && pausedWhileHidden)
  {
    startRefresh();
    pausedWhileHidden = false;
  }
  else if (!onTop && !mRefreshStop)
  {
    stopRefresh();
    pausedWhileHidden = true;
  }
}

//-----------------------------------------------------
//                 GESTION DE L'AIDE
//-----------------------------------------------------

void CYDisplay::updateToolTip()
{
  QString msg = QString(tr("<qt><p>This is a data display. To customize a sensor display click "
                             "and hold the right mouse button on either the frame or the "
                             "display box and select the <i>Properties</i> entry from the popup "
                             "menu. Select <i>Remove</i> to delete the display from the work "
                             "sheet.</p>%1</qt>")).arg(additionalToolTip());
  this->setToolTip(msg);
}

QString CYDisplay::additionalToolTip()
{
  return QString();
}

//-----------------------------------------------------
//                 GESTION DU DOM
//-----------------------------------------------------

QColor CYDisplay::restoreColorFromDOM(QDomElement &de, const QString &attr, const QColor &fallback)
{
  bool ok;
  uint c = de.attribute(attr).toUInt(&ok);
  if (!ok)
    return (fallback);
  else
    return (QColor((c >> 16) & 0xFF, (c >> 8) & 0xFF, c & 0xFF));
}

void CYDisplay::addColorToDOM(QDomElement &de, const QString &attr, const QColor &col)
{
  int r, g, b;
  col.getRgb(&r, &g, &b);
  de.setAttribute(attr, (r << 16) | (g << 8) | b);
}

void CYDisplay::internCreateFromDOM(QDomElement &element)
{
  if (mFrame)
  {
    mFrame->updateGeometry();
  }

  if (compareVersionCYDOM("2.26")<0) // COMPATIBILITÉ ASCENDANTE < 2.26
  {
    mTitle->setVal( element.attribute("title", 0) );
  }

  mDB->loadFromDOM(element, false);

  setTitle( mTitle->val() );

  mRefreshTime->setVal(element.attribute("refreshTime").toUInt());
}

void CYDisplay::internAddToDOM(QDomDocument &doc, QDomElement &element)
{
  element.setAttribute("refreshTime" , mRefreshTime->val());

  mDB->saveToDOM(doc, element);
}

bool CYDisplay::createFromDOM(QDomElement &, QStringList *ErrorList)
{
    Q_UNUSED(ErrorList)
  // ne devrait jamais être servir.
  return true;
}

bool CYDisplay::addToDOM(QDomDocument &, QDomElement &, bool)
{
  // ne devrait jamais être servir.
  return true;
}

CYTime *CYDisplay::refreshTime()
{
  return mRefreshTime;
}

void CYDisplay::setSettingsFile(const QString fileName)
{
  mSettingsFile = fileName;
}

int CYDisplay::loadSettings()
{
  return loadSettings(mSettingsFile);
}

int CYDisplay::loadSettings(QString &fileName)
{
  if (fileName.isEmpty())
    return -4;

  QFile file(fileName);

  if (!file.open(QIODevice::ReadOnly))
  {
    CYMESSAGETEXT(tr("Create a new file %1").arg(fileName))
    saveSettings(fileName);
    return 1;
  }

  QString errorMsg;
  int errorLine;
  int errorColumn;

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  if (!doc.setContent(&file, &errorMsg, &errorLine, &errorColumn))
  {
    CYWARNINGTEXT(tr("The file %1 does not contain valid XML ! %2: %3 %4").arg(fileName).arg(errorMsg).arg(errorLine).arg(errorColumn));
    return -1;
  }
  // Vérifie le type propre du document.
  if (doc.doctype().name() != "CYLIXDisplay")
  {
    CYWARNINGTEXT(tr("The file %1 does not contain a valid"
                       "definition, which must have a document type ").arg(fileName)
                      +"'CYLIXDisplay'");
    return -2;
  }

  // Charge les informations sur la base de données.
  QDomElement db = doc.documentElement();

  // Version de la librairie lors de la sauvegarde du document.
  mVersionCYDOM = db.attribute("CYDOM");

  if (!createFromDOM(db))
    return -3;

  return 2;
}

int CYDisplay::saveSettings()
{
  if (mSettingsFile.isEmpty())
    return -1;
  return saveSettings(mSettingsFile);
}

int CYDisplay::saveSettings(QString &fileName)
{
  if (fileName.isEmpty())
    return -4;

  QDomDocument doc("CYLIXDisplay");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  // Enregistre les informations sur la base de données.
  QDomElement db = doc.createElement("CYDisplay");

  db.setAttribute("CYDOM", core->version());
//   mVersionCYDOM = core->version();

  doc.appendChild(db);
  if (!addToDOM(doc, db))
    return -1;

  QFile file(fileName);
  if (!file.open(QIODevice::WriteOnly))
  {
    CYWARNINGTEXT(tr("Can't save file %1!").arg(fileName));
//    CYWARNINGTEXT(tr("Can't save file %1!").arg(fileName));
    return -2;
  }
  QTextStream s(&file);
  s.setCodec("UTF-8");
  s << doc;
  file.close();
  core->backupFile(fileName);

  return 1;
}

bool CYDisplay::isEmpty()
{
  return datas.isEmpty();
}

void CYDisplay::validate()
{
  if (!parent()->inherits("CYAnalyseCell"))
    saveSettings();
  else
    setModified(true);
}

bool CYDisplay::forcing() const
{
  return mForcing;
}

void CYDisplay::setForcing(const bool val)
{
  mForcing = val;
}

void CYDisplay::contextMenu(const QPoint& pos)
{
  Q_UNUSED(pos)
//  TODO QT5
  showPopupMenu();
}

int CYDisplay::showPopupMenu(QMenu *pm0, int index, bool end)
{
  if (!mShowPopupMenu)
    return index;

  QMenu *pm;
  if (pm0)
  {
    if (index==0)                                     // Création d'un sous-menu surgissant
    {
      pm = pm0->addMenu(tr("Display")); index++;
    }
    else                                              // Ajout d'entrées dans un menu surgissant
      pm = pm0;
  }
  else                                                // Création d'un menu surgissant
  {
    pm = new QMenu(tr("Display")); index++;
    pm->addSeparator();
  }

  if (!end)
  {
    if (core)
    {
      if (hasSettingsDialog())
        pm->addAction(core->loadIconSet("cyconfigure.png", 22), tr("&Settings"), this, SLOT(settings()));
      if ( analyseSheet() && !analyseSheet()->isProtected() )
        pm->addAction(core->loadIconSet("cyedit-delete.png", 22), tr("&Remove"      ), this, SLOT( remove() ));
    }
  }
  else
  {
    if (parent()->inherits("CYDatasTableDisplayCell"))
    {
      pm->addSeparator();
      CYDatasTableDisplayCell *cell = (CYDatasTableDisplayCell *)parent();
      index = cell->table()->showPopupMenu(pm, 0);
    }
    else if (parent()->inherits("CYAnalyseCell"))
    {
      pm->addSeparator();
      mCell = (CYAnalyseCell *)parent();
      index = mCell->showPopupMenu(pm, 0);
    }
    else
    {
      pm->addSeparator();
      pm->addAction(tr("&Setup update interval"), this, SLOT(setupTimer()));
      if (core)
      {
        if (mRefreshStop)
          pm->addAction(core->loadIconSet("cyplayer_play.png", 22), tr("&Continue update"), this, SLOT(startRefresh()));
        else
          pm->addAction(core->loadIconSet("cyplayer_pause.png", 22), tr("P&ause update"), this, SLOT(stopRefresh()));
      }
    }
  }

  if (!pm0)
    pm->exec(QCursor::pos());

  return index;
}

void CYDisplay::remove()
{
  QEvent *ev = new QEvent(QEvent::User);
  cyapp->postEvent(parent(), ev);
}


/*! Rafraîchit le status.
    \fn CYDisplay::refreshStatus()
 */
void CYDisplay::refreshStatus()
{
  if (!isVisible())
    return;

  Status oldStatus = mStatus;
  if (connected())
  {
    mStatus = Connect;

    if (refreshIsStop())
      mStatus = Pause;

    if (core && !core->analyseRefreshEnabled())
      mStatus = GlobalPause;
  }
  else
    mStatus = NoConnect;


  if (mStatusIndicator)
  {
    if (!mStatusIndicatorEnabled && mStatusIndicator->isVisible())
      mStatusIndicator->hide();

    if (mStatusIndicatorEnabled && !mStatusIndicator->isVisible())
      mStatusIndicator->show();

    if ((!mStatusIndicator->firstRefresh()) || (mStatus != oldStatus))
    {
      mStatusIndicator->refresh();
      emit statusChanged(mStatus);
    }
  }
}

/*! @return le status.
    \fn CYDisplay::status()
 */
CYDisplay::Status CYDisplay::status()
{
  return mStatus;
}


/*! @return \a true si connexion de la/les donnée(s) de l'afficheur est bien en place.
    \fn CYDisplay::connected()
 */
bool CYDisplay::connected()
{
  if (!datas.isEmpty())
    if (CYDisplayItem *item = datas.first())
      return item->isOk(false);
  return false;
}


/*! \return le QLabel utilisé en titre PopupMenu
    \fn CYDisplay::popupMenuLabel(QString title)
 */
QLabel * CYDisplay::popupMenuLabel(QString title)
{
  QLabel *label = new QLabel(0);
  QColor color = QColor(Qt::blue);
  label->setFrameStyle(QFrame::StyledPanel);
  label->setAlignment(Qt::AlignHCenter);
  label->setTextFormat(Qt::AutoText);
  label->setText(QString("<i><font color=%1 size=+1>%2</font></i>").arg(color.name()).arg(title));
  return label;
}


/*! Initialise les textes à traduire de l'afficheur.
    @param title  Titre de l'afficheur.
    \fn CYDisplay::initTRDisplay(QString title)
 */
void CYDisplay::initTRDisplay(QString title)
{
  setBoxTitle(title);
}


/*! Supprime toutes les données
    \fn CYDisplay::removeDatas()
 */
void CYDisplay::removeDatas()
{
  while ( !datas.isEmpty() )
    removeData( datas.count()-1 );
}


/*! @return la taille de police
    \fn CYDisplay::fontSize()
 */
int CYDisplay::fontSize()
{
  return mFontSize->val();
}


/*! Saisie la taille de police
    \fn CYDisplay::setFontSize(int val)
 */
void CYDisplay::setFontSize(int val)
{
  mFontSize->setVal( val );
}

int CYDisplay::compareVersionCYDOM(QString than)
{
  QString versionCYDOM = (analyseSheet()) ? analyseSheet()->versionCYDOM() : mVersionCYDOM;
  return compareVersion(versionCYDOM, than);
}


/*! @return la feuille d'analyse rattachée à l'afficheur. @return \a 0 si l'afficheur n'est pas rattaché à une feuille d'analyse.
    \fn CYDisplay::analyseSheet()
 */
CYAnalyseSheet * CYDisplay::analyseSheet()
{
  if ( mCell )
    return mCell->sheet();
  else if ( parent() && parent()->inherits("CYDatasTableDisplayCell") )
  {
    CYDatasTableDisplayCell *cell = (CYDatasTableDisplayCell *)parent();
    return cell->table()->analyseSheet();
  }
  else
    return 0;
}

void CYDisplay::initDiscontBuffer()
{
  foreach (CYDisplayItem *d, datas)
    d->initDiscontBuffer();
}
