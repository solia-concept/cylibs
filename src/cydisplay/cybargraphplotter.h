/***************************************************************************
                          cybargraphplotter.h  -  description
                             -------------------
    begin                : jeu déc 4 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYBARGRAPHPLOTTER_H
#define CYBARGRAPHPLOTTER_H

// QT
#include <QWidget>
#include <QList>
#include <QPainter>

class CYDB;
class CYData;
class CYF64;
class CYFlag;
class CYColor;
class CYBargraphSignal;
class CYBargraph;

/** CYBargraphPlotter constitue un traceur d'un bargraph.
  * @short Traceur de bargraph.
  * @author Gérald LE CLEACH.
  */
  
class CYBargraphPlotter : public QWidget
{
  Q_OBJECT

    /** La classe CYBarGraph y a accès libre. */
    friend class CYBargraph;

public:
    CYBargraphPlotter(CYBargraph *bargraph, QWidget *parent=0, const QString &name=0);
    ~CYBargraphPlotter();

    CYBargraphSignal *addBar(CYData *data, int id, QString label);
    bool deleteBar(uint idx);

    void changeRange(double min, double max);

    void setLimits(double l, bool la, double u, bool ua);

    /** @return l'élément correspondant à l'index \a i du traceur. */
    CYBargraphSignal *item(uint i);
    void setColors(QColor normal, QColor alarm, QColor background);

protected:
    virtual void paintEvent(QPaintEvent * event);

private:
    /** Base de données locale à cet afficheur. */
    CYDB *mDB;

    void init();

    CYF64 *mMinValue;
    CYF64 *mMaxValue;
    CYFlag *mSens;
    CYF64 *mAlarmLowerBound;
    CYFlag *mAlarmLowerEnable;
    CYF64 *mAlarmUpperBound;
    CYFlag *mAlarmUpperEnable;

    CYColor *mNormalColor;
    CYColor *mAlarmColor;
    CYColor *mBackgroundColor;

    bool autoRange;
    QList<CYBargraphSignal*> items;

protected:
    CYBargraph * mBargraph;
protected:
    virtual void initDB();
};

#endif
