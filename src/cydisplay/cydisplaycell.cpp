/***************************************************************************
                          cydisplaycell.cpp  -  description
                             -------------------
    begin                : mer jan 26 2005
    copyright            : (C) 2005 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydisplaycell.h"

// QT
#include <QClipboard>
#include <QResizeEvent>
#include <QEvent>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QEvent>
#include <QMimeData>
// CYLIBS
#include "cymessagebox.h"
#include "cyanalysesheet.h"
#include "cydisplaydummy.h"
#include "cymultimeter.h"
#include "cyscope.h"
#include "cybargraph.h"
#include "cydatastable.h"
#include "cycore.h"
#include "cydata.h"

CYDisplayCell::CYDisplayCell(QWidget *parent, const QString &name)
: CYFrame(parent,name)
{
//  parent->setContentsMargins(0, 0, 0, 0);
//  this->setContentsMargins(0, 0, 0, 0);
//
//  mCellLayout= new QHBoxLayout(this);
//  mCellLayout->setSpacing(0);
//  mCellLayout->setMargin(0);
//  mCellLayout->setContentsMargins(0,0,0,0);
//
//  this->setLayout(mCellLayout);
//  mGrid = new QGridLayout(this, 1, 1);
  mGrid = new QGridLayout(this);
  mGrid->setMargin(0);

  init();
}

CYDisplayCell::~CYDisplayCell()
{
}

void CYDisplayCell::init()
{
  setAcceptDrops(true);
  mDisplay  = 0;
  mConnectModifie  = false;
  mDisplayCopying  = false;
  mDisplayPasting  = false;
  mModifieOnResize = false;
  mForcing         = false;
}

void CYDisplayCell::setDisplay(const QString &)
{
}

void CYDisplayCell::setDisplay(CYDisplay *)
{
}

void CYDisplayCell::dragEnterEvent(QDragEnterEvent* event)
{
//  if (event->mimeData()->hasText())
    event->acceptProposedAction();
}

void CYDisplayCell::dropEvent(QDropEvent* event)
{
  QString dObj;

  if (event->mimeData()->hasText())
  {
    QString name = event->mimeData()->text();

    if (!name.isEmpty())
      setDisplay(name);
  }
  event->acceptProposedAction();
}

void CYDisplayCell::customEvent(QEvent* ev)
{
  if (ev->type() == QEvent::User)
  {
    removeDisplay();
  }
}

bool CYDisplayCell::createDisplayFromDOM(QDomElement &)
{
  return true;
}

bool CYDisplayCell::addDisplayToDOM(QDomDocument &, QDomElement &, bool )
{
  return true;
}

void CYDisplayCell::cutDisplay()
{
  copyDisplay();
  removeDisplay();
}

void CYDisplayCell::copyDisplay()
{
  mDisplayCopying = true;
  QClipboard* clip = QApplication::clipboard();
  clip->setText(displayAsXML());
  mDisplayCopying = false;
}

void CYDisplayCell::pasteDisplay()
{
  mDisplayPasting = true;
  clipboardDisplay();
  mDisplayPasting = false;
}

void CYDisplayCell::clipboardDisplay()
{
  QClipboard* clip = QApplication::clipboard();
  QDomDocument doc;
  /* Récupère le texte dans le presse-papiers et vérifie la valididé de
   * l'en-tête XML et du type propre du document. */
  if (!doc.setContent(clip->text()) || doc.doctype().name() != "CYLIXDisplay")
  {
    CYMessageBox::sorry(this, tr("The clipboard does not contain a valid display description."));
    return;
  }

  QDomElement el = doc.documentElement();
  createDisplayFromDOM(el);
}

QString CYDisplayCell::asXML()
{
  /* Nous construisons une description XML de la cellule. */
  QDomDocument doc("CYLIXCell");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  QDomElement el = doc.createElement("cell");
  doc.appendChild(el);
  el.setAttribute("size", 100);

  addDisplayToDOM(doc, el, false);

  return doc.toString();
}

QString CYDisplayCell::displayAsXML()
{
  if (!mDisplay)
    return QString();

  /* Nous construisons une description XML de l'afficheur. */
  QDomDocument doc("CYLIXDisplay");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  QDomElement el;
  if (mDisplayCopying)
    el = doc.createElement("display");
  else
    return 0;

  doc.appendChild(el);
  el.setAttribute("class", mDisplay->metaObject()->className());
  mDisplay->addToDOM(doc, el);

  return doc.toString();
}

void CYDisplayCell::removeDisplay()
{
  // Les afficheurs activent cet évènement s'ils veulent être supprimés.
  if (CYMessageBox::warningYesNo(this, tr("Do you really want to delete the display?")) == CYMessageBox::Yes)
  {
    setDisplay();
  }
}

void CYDisplayCell::setModified(bool state)
{
  if (!isVisible())
    return;

  emit modified(state);
}

void CYDisplayCell::connectModifie(bool)
{
}

void CYDisplayCell::resizeEvent ( QResizeEvent * e)
{
  CYFrame::resizeEvent ( e );

  if ( mConnectModifie )
  {
    if (mModifieOnResize)
      setModified(true);
    else
      mModifieOnResize = true;
  }
}

bool CYDisplayCell::forcing() const
{
  return mForcing;
}

void CYDisplayCell::setForcing(const bool val)
{
  mForcing = val;
  if (mDisplay)
    mDisplay->setForcing(val);
}

CYDisplay * CYDisplayCell::display()
{
  return mDisplay;
}

void CYDisplayCell::startRefresh()
{
  if (mDisplay)
    mDisplay->startRefresh();
}

void CYDisplayCell::stopRefresh()
{
  if (mDisplay)
    mDisplay->stopRefresh();
}
