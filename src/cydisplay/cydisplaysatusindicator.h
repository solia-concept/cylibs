//
// C++ Interface: cydisplaysatusindicator
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDISPLAYSATUSINDICATOR_H
#define CYDISPLAYSATUSINDICATOR_H

// QT
#include <qlabel.h>
#include <qpixmap.h>
#include <qtimer.h>

class CYDisplay;

/**
@short Indique le status de l'afficheur dont il appartient.

@author Gérald LE CLEACH
*/
class CYDisplaySatusIndicator : public QWidget
{
Q_OBJECT
public:
    CYDisplaySatusIndicator(QWidget *parent, const QString &name, CYDisplay *display);

    ~CYDisplaySatusIndicator();
    bool firstRefresh();
    void showOrHide();

public slots:
    virtual void refresh();
    virtual void flash();

protected:
    bool mFirstRefresh;
    CYDisplay* mDisplay;
    QLabel  *mLabel;
    QTimer  *mTimer;
    QPixmap mConnectPixmap;
    QPixmap mNoConnectPixmap;
    QPixmap mPausePixmap;
    QPixmap mCurrentPixmap;
};

#endif
