/***************************************************************************
                          cydisplaysimple.h  -  description
                             -------------------
    begin                : mer jan 26 2005
    copyright            : (C) 2005 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDISPLAYSIMPLE_H
#define CYDISPLAYSIMPLE_H

// ANSI
#include <math.h>
#include <stdlib.h>
// CYLIBS
#include "cydisplay.h"

class CYDisplayItem;
class CYDisplaySimpleSetup;
class CYDataWdg;
class CYLed;
class CYNumDisplay;
class CYTextLabel;
class CYFlagInput;
class CYNumInput;
class CYTextEdit;
class KLed;
class CYFlag;
class CYColor;
class CYS16;
class CYU8;


/** @short Afficheur simple.
  * @author Gérald LE CLEACH.
  */

class CYDisplaySimple : public CYDisplay
{
  Q_OBJECT
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)

public:
  /** Construit un afficheur numérique.
    * @param parent  Widget parent.
    * @param name    Nom de l'afficheur.
    * @param title   Titre de l'afficheur.
    * @param nf      Pas de trame existante.
    * @param input   Vaut \a true si c'est l'afficheur doit se comporter comme un objet de saisie. */
  CYDisplaySimple(QWidget *parent=0, const QString &name=0, const QString &title=0, bool nf=false, bool input=false);
  virtual ~CYDisplaySimple();

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return le nom de la donnée flag associée. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée. */
  virtual void setFlagName(const QByteArray &name);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return si le flag associé doit être pris en compte dans le sens inverse. */
  bool inverseFlag() const;
  /** Inverse le sens du flag associé si \a inverse est à \p true. */
  void setInverseFlag(const bool inverse);

  /** @return \a true si c'est l'afficheur doit se comporter comme un objet de saisie. */
  bool input() { return mInput; }
  /** Saisir \a true pour que l'afficheur se comporte comme un objet de saisie. */
  void setInput(bool val) { mInput = val; }

  /** Ajoute une donnée dans l'afficheur avec une étiquette. */
  virtual bool addData(CYData *data, QString label = 0);

  void resizeEvent(QResizeEvent *);

  bool createFromDOM(QDomElement &element, QStringList *ErrorList=nullptr);
  bool addToDOM(QDomDocument &doc, QDomElement &element, bool save=true);

  virtual bool hasSettingsDialog() const { return true; }

  /** Gestion du menu surgissant.
    * @param pm0    Menu appelant s'il existe.
    * @param index  Index de l'entrée du menu appelant.
    * @param end    Fin du menu. */
  virtual int showPopupMenu(QMenu *pm0=0, int index=0, bool end=false);

  /** @return le texte contenu par l'afficheur lorsque c'est possible.
      Les afficheurs ne pouvant pas retourner de texte simple tels que par
      exemple un oscilloscope, la valeur \a ok est alors à \a false. */
  virtual QString text(bool &ok);

public slots:
   /** Rafraîchit l'afficheur. */
  virtual void refresh();
  /** Exécute la boîte de configuration. */
  void settings();
  /** met à jour le style de la led. */
  void updateLedStyle();
  /** met à jour le style de l'afficheur numérique. */
  void updateNumDisplayStyle();
  /** met à jour le style de l'afficheur alpha-numérique. */
  void updateTextLabelStyle();

    void selectSettingsStylePage();

  /** Applique le résultat de la boîte de configuration. */
  void applySettings();

    virtual void updateFormat();

protected: // Protected methods
  void createLed(QString dataName);
  void createNumDisplay(QString dataName);
  void createTextLabel(QString dataName);
  void createFlagInput(QString dataName);
  void createNumInput(QString dataName);
  void createTextInput(QString dataName);

protected: // Protected attributes
  /** Afficheur. */
  QWidget *mWidget;
  /** CYDataWdg. */
  CYDataWdg *mDataWdg;

  /** Vaut \a true si c'est l'afficheur doit se comporter comme un objet de saisie. */
  bool mInput;

  /** Boîte de configuration. */
  CYDisplaySimpleSetup * mSetup;

  CYU8 *mNumBase;
  CYFlag *mShowUnit;

  /** Objet de visualisation pour les données booléennes. */
  CYLed *mLed;
  /** Vaut \a true si le mode bi-couleur de la led est actif, sinon c'est le paramètre de contraste qui différencie l'état. */
  CYFlag *mLedBicolor;
  /** Couleur de l'état \a true de la led en mode bi-couleur, ou couleur de la led en mode normal. */
  CYColor *mLedColorOn;
  /** Couleur de l'état \a false de la led en mode bi-couleur. */
  CYColor *mLedColorOff;
  /** Forme de la led. */
  CYS16 *mLedShape;
  /** Look de la led. */
  CYS16 *mLedLook;

  /** Couleur d'affichage utilisée lorsque la valeur est valide. */
  CYColor *mEnableColor;
  /** Couleur d'affichage utilisée lorsque la valeur n'est pas valide. */
  CYColor *mDisableColor;
  /** Couleur de fond. */
  CYColor *mBackgroundColor;

  /** Couleur de fond automatique. Prend la couleur de la donnée. */
  CYFlag *mCustomColor;
  /** Objet de visualisation pour les données numériques. */
  CYNumDisplay *mNumDisplay;
  /** Objet de visualisation pour les données alpha-numériques. */
  CYTextLabel *mTextLabel;
  /** Objet de saisie pour les données booléennes. */
  CYFlagInput *mFlagInput;
  /** Objet de saisie pour les données numériques. */
  CYNumInput *mNumInput;
  /** Objet de saisie pour les données alpha-numériques. */
  CYTextEdit *mTextInput;
private:
    void init();
};

#endif
