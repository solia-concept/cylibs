/***************************************************************************
                          cydisplaydummy.cpp  -  description
                             -------------------
    début                  : Mon Feb 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cydisplaydummy.h"

// QT
#include <QResizeEvent>
#include <QMenu>
// CYLIBS
#include "cycore.h"
#include "cyanalysecell.h"
#include "cydisplaysimple.h"
#include "cymultimeter.h"
#include "cyscope.h"
#include "cybargraph.h"
#include "cydatastable.h"

CYDisplayDummy::CYDisplayDummy(QWidget *parent, const QString &name, const QString &)
  : CYDisplay(parent, name)
{
  mType = Dummy;

  setMinimumSize(16, 16);

  this->setToolTip(tr("Empty cell"));

  QString txt;
  if (parent->inherits("CYAnalyseCell"))
    txt = tr("analyse sheet");
  else if (parent->inherits("CYDatasTablePlotter"))
    txt = tr("datas table");

  QString msg = QString( "<p>"+
                         tr("This is an empty cell in an %1. Drag a data or group of datas from "
                             "the data browser and drop it here. A data(s) display will "
                             "appear that allows you to monitor the values of the data(s) "
                             "over time.").arg(txt)
                         +"</p>" );

  this->setToolTip(msg);
}


CYDisplayDummy::~CYDisplayDummy()
{
}

void CYDisplayDummy::resizeEvent(QResizeEvent *)
{
  if (mFrame)
    mFrame->setGeometry(0, 0, width(), height());
}

bool CYDisplayDummy::createFromDOM(QDomElement &element, QStringList *ErrorList )
{
    Q_UNUSED(ErrorList)
  if (!core)
    return false;
  mCreatingFromDOM = true;

  internCreateFromDOM(element);

  setModified(false);
  mCreatingFromDOM = false;
  return (true);
}

bool CYDisplayDummy::addToDOM(QDomDocument &doc, QDomElement &element, bool save)
{
  internAddToDOM(doc, element);

  if (save)
    setModified(false);

  return (true);
}


int CYDisplayDummy::showPopupMenu(QMenu *pm0, int index, bool )
{
  QMenu *pm;
  if (pm0)
  {
    if (index==0)                                     // Création d'un sous-menu surgissant
    {
      pm = pm0->addMenu(tr("Dummy display")); index++;
    }
    else                                              // Ajout d'entrées dans un menu surgissant
      pm = pm0;
  }
  else                                                // Création d'un menu surgissant
  {
    pm = new QMenu(tr("Dummy display")); index++;
    pm->addSeparator();
  }

  QMenu *pm2 = pm->addMenu(tr("&Select a display type"));

  pm2->addAction(tr("&Simple"), mCell, SLOT( newDisplaySimple() ));
  QMenu *pm3 = pm2->addMenu(tr("&Multimeter"));
  pm3->addAction(tr("&Analog"), mCell, SLOT( newAnalogMultimeter() ));
  pm3->addAction(tr("&Digital"), mCell, SLOT( newLCDMultimeter() ));

  QAction * action;
  action = pm2->addAction(tr("&Classical oscilloscope"), mCell, SLOT(newScope()));
  action->setWhatsThis( tr("Oscilloscope may have two ordinate axes, each with its own format."));
  action = pm2->addAction(tr("Oscilloscope multiple formats"), mCell, SLOT(newScopeMultiple()));
  action->setWhatsThis(tr("Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets."));
  pm2->addAction(tr("&BarGraph"), mCell, SLOT( newBargraph() ));
  pm2->addAction(tr("Datas &table"), mCell, SLOT( newDatasTable() ));

  index = CYDisplay::showPopupMenu(pm, index, true);

  if (!pm0)
    pm->exec(QCursor::pos());

  return index;
}
