//
// C++ Implementation: cybargraphsetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cybargraphsetup.h"
#include "ui_cybargraphsetup.h"

// CYLIBS
#include "cycore.h"
#include "cybargraph.h"
#include "cybargraphplotter.h"
#include "cybargraphsignal.h"
#include "cydataseditlist.h"
#include "cydataslistview.h"

CYBargraphSetup::CYBargraphSetup(CYBargraph *parent, const QString &name)
  : CYDialog(0,name), ui(new Ui::CYBargraphSetup)
{
  mBargraph = parent;
  ui->setupUi(this);

  init();
}


CYBargraphSetup::~CYBargraphSetup()
{
  delete ui;
}

/*!
    \fn CYBargraphSetup::init()
 */
void CYBargraphSetup::init()
{
  setUpdateIfVisible(false);

  setLocaleDB( mBargraph->db() );

  for (int i = 0; i < mBargraph->datas.count(); ++i)
  {
    CYBargraphSignal *signal= (CYBargraphSignal *)mBargraph->datas.at(i);
    ui->datasEditList->addData(signal, signal->id);
  }
}


/*!
    \fn CYBargraphSetup::apply()
 */
void CYBargraphSetup::apply()
{
  CYDatasListView *list;

  list = ui->datasEditList->list();
  mBargraph->removeDatas();
  for (QTreeWidgetItemIterator it(list); (*it); ++it)
  {
    mBargraph->addData(core->findData(list->dataName(*it)));
    CYBargraphSignal *signal = (CYBargraphSignal *)mBargraph->datas.at(0);
    signal->label = list->label(*it);
    signal->coefficient = list->coef(*it);
    signal->offset = list->offset(*it);
  }

  CYDialog::update();

  mBargraph->applySettings();
  ui->applyButton->setDisabled( true );
}


/*! Affiche l'aide relative du manuel.
    \fn CYBargraphSetup::help()
 */
void CYBargraphSetup::help()
{
  core->invokeHelp( "cydoc", "display_bargraph_settings" );
}
