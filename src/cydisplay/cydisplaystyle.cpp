/***************************************************************************
                          cydisplaystyle.cpp  -  description
                             -------------------
    début                  : Mon Feb 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cydisplaystyle.h"

// QT
#include <qcolordialog.h>
//Added by qt3to4:
#include <QPixmap>
#include <QStringList>
// CYLIBS
#include "cydisplaystylesetup.h"
#include "cycolorbutton.h"
#include "cycore.h"

CYDisplayStyle::CYDisplayStyle(QObject *parent, const QString &name)
  : QObject(parent)
{
  setObjectName(name);
  fgColor1 = QColor(Qt::green);	// light green
  fgColor2 = QColor(Qt::green);	// light green
  alarmColor = Qt::red;
  backgroundColor = QColor(Qt::black); // almost black
  gridColor = Qt::gray;
  int offset = (core) ? core->offsetFontSize() : 0;
  fontSize = 13+offset;
  dataColors.append(new QColor(0x1889ff));	// soft blue
  dataColors.append(new QColor(0xff7f08));	// reddish
  dataColors.append(new QColor(0xffeb14));	// bright yellow
  uint v = 0x00ff00;
  for (uint i = dataColors.count(); i < 32; ++i)
  {
    v = (((v + 82) & 0xff) << 23) | (v >> 8);
    dataColors.append(new QColor(v & 0xff, (v >> 16) & 0xff, (v >> 8) & 0xff));
  }
}

CYDisplayStyle::~CYDisplayStyle()
{
}

void CYDisplayStyle::readProperties(QSettings *cfg)
{
  QColor color;

  color = QColor(cfg->value("fgColor1").toString());
  if (color.isValid())
    fgColor1 = color;

  color = QColor(cfg->value("fgColor2").toString());
  if (color.isValid())
    fgColor2 = color;;

  color = QColor(cfg->value("alarmColor").toString());
  if (color.isValid())
    alarmColor = color;;

  color = QColor(cfg->value("backgroundColor").toString());
  if (color.isValid())
    backgroundColor = color;;

  color = QColor(cfg->value("gridColor").toString());
  if (color.isValid())
    gridColor = color;

  fontSize = cfg->value("fontSize", fontSize).toUInt();
  QStringList sl = cfg->value("dataColors").toStringList();
  if (!sl.isEmpty())
  {
    while (!dataColors.isEmpty())
      delete dataColors.takeFirst();
    QStringList::const_iterator  it = sl.begin();
    for ( ; it != sl.end(); ++it)
      dataColors.append(new QColor(*it));
  }
}

void CYDisplayStyle::saveProperties(QSettings *cfg)
{
  cfg->setValue("fgColor1", fgColor1.name());
  cfg->setValue("fgColor2", fgColor2.name());
  cfg->setValue("alarmColor", alarmColor.name());
  cfg->setValue("backgroundColor", backgroundColor.name());
  cfg->setValue("gridColor", gridColor.name());
  cfg->setValue("fontSize", fontSize);
  QStringList sl;
  QList<QColor*>::iterator it;
  for (it = dataColors.begin(); it != dataColors.end(); ++it)
    sl.append((*it)->name());
  cfg->setValue("dataColors", sl);
}

void CYDisplayStyle::configure(QWidget *parent)
{
  setup = new CYDisplayStyleSetup(parent, "CYDisplayStyleSetup");

  setup->setStyle(this);

  for (int i = 0; i < dataColors.count(); ++i)
  {
    QPixmap pm(12, 12);
    pm.fill(*dataColors.at(i));
    QListWidgetItem item(QIcon(pm), tr("Color %1").arg(i));
    setup->colorList()->addItem(&item);
  }

  setup->initConnect(this);
  if (setup->exec())
    apply();

  delete setup;
}

void CYDisplayStyle::editColor()
{
  QListWidgetItem *item = setup->colorList()->currentItem();

  if (!item)
    return;

  QColor c = QColorDialog::getColor( item->icon().pixmap(1,1).toImage().pixel(1, 1) );
  if(c.isValid())
  {
    QPixmap newPm(12, 12);
    newPm.fill(c);
    item->setIcon(QIcon(newPm));
  }
}

void CYDisplayStyle::apply()
{
  setup->apply(this);
  while (!dataColors.isEmpty())
    delete dataColors.takeFirst();
  for (int i = 0; i < setup->colorList()->count(); ++i)
  {
    QListWidgetItem *item = setup->colorList()->item(i);
    QColor color = QColor(item->icon().pixmap(1,1).toImage().pixel(1, 1));
    dataColors.append(&color);
  }
}
