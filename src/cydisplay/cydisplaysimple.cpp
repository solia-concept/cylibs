/***************************************************************************
                          cydisplaysimple.cpp  -  description
                             -------------------
    begin                : mer jan 26 2005
    copyright            : (C) 2005 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydisplaysimple.h"

// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "cyu8.h"
#include "cyf64.h"
#include "cys16.h"
#include "cyflag.h"
#include "cystring.h"
#include "cyled.h"
#include "cycolor.h"
#include "cynumdisplay.h"
#include "cytextlabel.h"
#include "cytextedit.h"
#include "cyflaginput.h"
#include "cynuminput.h"
#include "cycheckbox.h"
#include "cydisplayitem.h"
#include "cydisplaystyle.h"
#include "cydisplaysimplesetup.h"
#include "cydatastable.h"

CYDisplaySimple::CYDisplaySimple(QWidget *parent, const QString &name, const QString &title, bool nf, bool input)
  : CYDisplay(parent, name, title, nf)
{
  mInput  = input;
  mSetup = 0;

  init();
}

CYDisplaySimple::~CYDisplaySimple()
{
}

void CYDisplaySimple::setDataName(const QByteArray &name)
{
  mDataWdg->setDataName(name);
}

QByteArray CYDisplaySimple::dataName() const
{
  return mDataWdg->dataName();
}

void CYDisplaySimple::setFlagName(const QByteArray &name)
{
  mDataWdg->setFlagName(name);
}

QByteArray CYDisplaySimple::flagName() const
{
  return mDataWdg->flagName();
}

void CYDisplaySimple::setBenchType(const QByteArray &name)
{
  mDataWdg->setBenchType(name);
}

QByteArray CYDisplaySimple::benchType() const
{
  return mDataWdg->benchType();
}

void CYDisplaySimple::setInverseFlag(bool inverse)
{
  mDataWdg->setInverseFlag(inverse);
}

bool CYDisplaySimple::inverseFlag() const
{
  return mDataWdg->inverseFlag();
}

bool CYDisplaySimple::addData(CYData *data, QString label)
{
  if (label.isEmpty())
    label=data->label();

  removeDatas();

  if (!CYDisplay::addData(data, label))
    return (false);

  if (mLed)
  {
    delete mLed;
    mLed = 0;
  }
  if (mNumDisplay)
  {
    delete mNumDisplay;
    mNumDisplay = 0;
  }
  if (mTextLabel)
  {
    delete mTextLabel;
    mTextLabel = 0;
  }
  if (mFlagInput)
  {
    delete mFlagInput;
    mFlagInput = 0;
  }
  if (mNumInput)
  {
    delete mNumInput;
    mNumInput = 0;
  }
  if (mTextInput)
  {
    delete mTextInput;
    mTextInput = 0;
  }

  if (!mInput)
  {
    switch (data->type())
    {
      case Cy::Bool   :
                      {
                        createLed(data->objectName());
                        break;
                      }
      case Cy::Word   :
                      {
                        createNumDisplay(data->objectName());
                        break;
                      }
      case Cy::VFL    :
      case Cy::VS8    :
                      {
                        if (data->isFlag())
                        {
                          createLed(data->objectName());
                        }
                        else                        {
                          createNumDisplay(data->objectName());
                        }
                        break;
                      }
      case Cy::VS16   :
      case Cy::VS32   :
      case Cy::VS64   :
      case Cy::VU8    :
      case Cy::VU16   :
      case Cy::VU32   :
      case Cy::VU64   :
      case Cy::VF32   :
      case Cy::VF64   :
      case Cy::Time   :
      case Cy::Sec    :
                      {
                        createNumDisplay(data->objectName());
                        break;
                      }
      case Cy::String :
                      {
                        createTextLabel(data->objectName());
                        break;
                      }
      default         : CYFATALOBJECTDATA(objectName(), data->objectName());
    }
  }
  else
  {
    switch (data->type())
    {
      case Cy::Bool   :
                      {
                        createFlagInput(data->objectName());
                        break;
                      }
      case Cy::Word   :
                      {
                        createNumInput(data->objectName());
                        break;
                      }
      case Cy::VFL    :
      case Cy::VS8    :
                      {
                        if (data->isFlag())
                        {
                          createFlagInput(data->objectName());
                        }
                        else
                        {
                          createNumInput(data->objectName());
                        }
                        break;
                      }
      case Cy::VS16   :
      case Cy::VS32   :
      case Cy::VS64   :
      case Cy::VU8    :
      case Cy::VU16   :
      case Cy::VU32   :
      case Cy::VU64   :
      case Cy::VF32   :
      case Cy::VF64   :
      case Cy::Time   :
      case Cy::Sec    :
                      {
                        createNumInput(data->objectName());
                        break;
                      }
      case Cy::String :
                      {
                        createTextInput(data->objectName());
                        break;
                      }
      default         : CYFATALOBJECTDATA(objectName(), data->objectName());
    }
  }

  updateFormat();
  mDataWdg->setDisplaySimple(this);
  mBoxLayout->addWidget( mWidget );
  registerPlotterWidget(mWidget);
  mWidget->setMinimumSize(0, 0);
  mWidget->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding));
  setMinimumSize(0, 0);
  mDataWdg->setFontSize( mFontSize->val() );
  linkDatas();
  return (true);
}

void CYDisplaySimple::refresh()
{
  if (mDeleting || mCreating)
    return;

  if ( !mRefreshNotVisible && !isVisible() )
    return;

  if (!core)
    return;

  refreshStatus();

  if (core && !core->analyseRefreshEnabled())
    return;

  emit refreshing();
}

void CYDisplaySimple::resizeEvent(QResizeEvent *e)
{
  CYDisplay::resizeEvent(e);
}

bool CYDisplaySimple::createFromDOM(QDomElement &element, QStringList *ErrorList)
{
    Q_UNUSED(ErrorList)
  if (!core)
    return false;
  mCreatingFromDOM = true;
  hide();

  if (compareVersionCYDOM("2.26")<0) // COMPATIBILITÉ ASCENDANTE < 2.26
  {
    int offset = (core) ? core->offsetFontSize() : 0;
    QString size = QString("%1").arg(13+offset);
    mFontSize->setVal( element.attribute("fontSize", size).toInt() );
    mLedBicolor->setVal((bool)element.attribute("mLedBicolor", "0").toInt());
    mLedColorOn->setVal(restoreColorFromDOM(element, "mLedColorOn", mLedColorOn->def()));
    mLedColorOff->setVal(restoreColorFromDOM(element, "mLedColorOff", mLedColorOff->def()));
    mLedShape->setVal(element.attribute("mLedShape", QString("%1").arg(mLedShape->def())).toInt());
    mLedLook->setVal(element.attribute("mLedLook", QString("%1").arg(mLedLook->def())).toInt());
  }

  mInput = (bool)element.attribute("input", "0").toInt();

  internCreateFromDOM(element);

  QString title = mTitle->val();

  if (!element.attribute("dataName").isEmpty())
    CYDisplay::addData(element.attribute("dataName"), element.attribute("label"));

  if (compareVersionCYDOM("2.26")<0 && !title.isEmpty()) // COMPATIBILITÉ ASCENDANTE < 2.26
  {
    setTitle(title);
  }

  mDataWdg->setFontSize( mFontSize->val() );

  show();
  adjustSize();
  setModified(false);
  mCreatingFromDOM = false;
  return (true);
}

bool CYDisplaySimple::addToDOM(QDomDocument &doc, QDomElement &element, bool save)
{
  if (datas.size() && datas.at(0))
  {
    element.setAttribute("dataName", datas.at(0)->objectName());
    if ( datas.at(0)->data->label() != datas.at(0)->label )
      element.setAttribute("label", datas.at(0)->label );
  }

  element.setAttribute("input", mInput);

  internAddToDOM(doc, element);

  if (save)
    setModified(false);

  return (true);
}

int CYDisplaySimple::showPopupMenu(QMenu *pm0, int index, bool )
{
  if (index==-1)  // Appel d'un CYLineEdit
  {
    index = 0;
    pm0->addMenu(tr("Simple display"));  index++;
    pm0->addSeparator();

    index = CYDisplay::showPopupMenu(pm0, index);
    index = CYDisplay::showPopupMenu(pm0, index, true);
  }
  else
  {
    QMenu *pm;
    if (pm0)
    {
      if (index==0)                                     // Création d'un sous-menu surgissant
      {
        pm = pm0->addMenu(tr("Simple display")); index++;
      }
      else                                              // Ajout d'entrées dans un menu surgissant
        pm = pm0;
    }
    else                                                // Création d'un menu surgissant
    {
      pm = new QMenu(tr("Simple display")); index++;
      pm->addSeparator();
    }

    index = CYDisplay::showPopupMenu(pm, index);
    index = CYDisplay::showPopupMenu(pm, index, true);

    if (!pm0)
    {
      pm0 = pm;
      pm->exec(QCursor::pos());
    }
  }

  return index;
}


void CYDisplaySimple::settings()
{
  if (!mSetup)
  {
    mSetup = new CYDisplaySimpleSetup(this, "CYDisplaySimpleSetup");
    Q_CHECK_PTR(mSetup);
  }
//   mSetup->title->setText(title());
//   mSetup->title->setFocus();
//   mSetup->fontSize->setValue(mFontSize);
//
//   for (uint i = 0; i < datas.count(); ++i)
//   {
//     CYDisplayItem *d = datas.at(i);
//     QString n = QString("%1").arg(i + 1, 2);
//     QString status = datas.at(i)->isOk() ? tr("Ok") : tr("Error");
//     mSetup->dataList->list->clear();
//     new QListViewItem(mSetup->dataList->list, d->label, d->group, d->link, d->host, d->unit, status, d->name);
//   }
//
//   connect(mSetup->applyButton, SIGNAL(clicked()), this, SLOT(applySettings()));

  if ( !mUpdatingFormat )
  {
    selectSettingsStylePage();
    mSetup->exec();
  }
}

/*! Sélectionne la bonne page de configuration du style en fonction de l'objet en cours.
    \fn CYDisplaySimple::selectSettingsStylePage()
 */
void CYDisplaySimple::selectSettingsStylePage()
{
  if (!mSetup)
    return;

  if (mLed)
    mSetup->setStylePage(0);
  else
    mSetup->setStylePage(1);
}

void CYDisplaySimple::applySettings()
{
  mDataWdg->setFontSize( mFontSize->val() );

  CYDisplay::applySettings();

  updateLedStyle();
  updateNumDisplayStyle();
  updateTextLabelStyle();

  repaint();

  if ( !mUpdatingFormat )
    selectSettingsStylePage();
}


/*! Création de l'objet de visualisation pour les données booléennes.
    \fn CYDisplaySimple::createLed(QString dataName)
 */
void CYDisplaySimple::createLed(QString dataName)
{
  if (!mFrame)
    mLed = new CYLed(this, "mLed");
  else
    mLed = new CYLed(mFrame, "mLed");
  mWidget = mLed;
  mDataWdg = mLed;
  mLed->setMinimumWidth(0);
  mLed->setDataName(dataName.toUtf8());
  updateLedStyle();
  mLed->show();
  connect(this, SIGNAL(refreshing()), mLed, SLOT(refresh()));
}

/*! Création de l'objet de visualisation pour les données numériques.
    \fn CYDisplaySimple::createNumDisplay(QString dataName)
 */
void CYDisplaySimple::createNumDisplay(QString dataName)
{
  if (!mFrame)
    mNumDisplay = new CYNumDisplay(this, "mNumDisplay");
  else
    mNumDisplay = new CYNumDisplay(mFrame, "mNumDisplay");

  mWidget = mNumDisplay;
  mDataWdg = mNumDisplay;
  mNumDisplay->setMinimumWidth(0);
  mNumDisplay->setDataName(dataName.toUtf8());
  mNumDisplay->setAcceptDrops(false);
  mNumDisplay->setNumBase( (Cy::NumBase )mNumBase->val() );
  updateNumDisplayStyle();
  mNumDisplay->show();
  connect(this, SIGNAL(refreshing()), mNumDisplay, SLOT(refresh()));
}

/*! Création de l'objet de visualisation pour les données alpha-numériques.
    \fn CYDisplaySimple::createTextLabel(QString dataName)
 */
void CYDisplaySimple::createTextLabel(QString dataName)
{
  if (!mFrame)
    mTextLabel = new CYTextLabel(this, "mTextLabel");
  else
    mTextLabel = new CYTextLabel(mFrame, "mTextLabel");
  mWidget = mTextLabel;
  mDataWdg = mTextLabel;
  mTextLabel->setMinimumWidth(0);
  mTextLabel->setDataName(dataName.toUtf8());
  updateTextLabelStyle();
  mTextLabel->show();
  connect(this, SIGNAL(refreshing()), mTextLabel, SLOT(refresh()));
}

/*! Création de l'objet de saisie pour les données booléennes.
    \fn CYDisplaySimple::createFlagInput(QString dataName)
 */
void CYDisplaySimple::createFlagInput(QString dataName)
{
  if (!mFrame)
    mFlagInput = new CYFlagInput(this, "mFlagInput");
  else
    mFlagInput = new CYFlagInput(mFrame, "mFlagInput");
  mWidget = mFlagInput;
  mDataWdg = mFlagInput;
  mFlagInput->setMinimumWidth(0);
  mFlagInput->setDataName(dataName.toUtf8());
  mFlagInput->setReadOnly(false);
  mFlagInput->show();
}

/*! Création de l'objet de saisie pour les données numériques.
    \fn CYDisplaySimple::createNumInput(QString dataName)
 */
void CYDisplaySimple::createNumInput(QString dataName)
{
  if (!mFrame)
    mNumInput = new CYNumInput(this, "mNumInput");
  else
    mNumInput = new CYNumInput(mFrame, "mNumInput");
  mWidget = mNumInput;
  mDataWdg = mNumInput;
  mNumInput->setMinimumWidth(0);
  mNumInput->setDataName(dataName.toUtf8());
  mNumInput->setAutoMinimumSize(false);
  mNumInput->setNumBase( (CYNumInput::NumBase )mNumBase->val() );
  mNumInput->show();
}

/*! Création de l'objet de saisie pour les données alpha-numériques.
    \fn CYDisplaySimple::createTextInput(QString dataName)
 */
void CYDisplaySimple::createTextInput(QString dataName)
{
  if (!mFrame)
    mTextInput = new CYTextEdit(this, "mTextInput");
  else
    mTextInput = new CYTextEdit(mFrame, "mTextInput");
  mWidget = mTextInput;
  mDataWdg = mTextInput;
  mTextInput->setMinimumWidth(0);
  mTextInput->setDataName(dataName.toUtf8());
  mTextInput->setReadOnly(false);
  mTextInput->show();
}


void CYDisplaySimple::updateLedStyle()
{
  if (!mLed)
    return;
  mLed->setShape((CYLed::Shape)mLedShape->val());
  mLed->setLook((CYLed::Look)mLedLook->val());
  mLed->setBicolor(mLedBicolor->val());
  mLed->setColor(mLedColorOn->val());
  mLed->setColorOn(mLedColorOn->val());
  mLed->setColorOff(mLedColorOff->val());
  mLed->setValue(mLed->value());
}


void CYDisplaySimple::updateNumDisplayStyle()
{
  if (!mNumDisplay)
    return;

  mNumDisplay->setShowUnit(mShowUnit->val());

  if (!mCustomColor->val())
  {
    mNumDisplay->setType( CYLineEdit::Process );
  }
  else
  {
    mNumDisplay->setType( CYLineEdit::Free );
    QPalette palette = mNumDisplay->palette(CYLineEdit::Free);

    palette.setColor(QPalette::Active  , QPalette::Text, mEnableColor->val() );
    palette.setColor(QPalette::Active  , QPalette::Base, mBackgroundColor->val() );
    palette.setColor(QPalette::Inactive, QPalette::Text, mEnableColor->val() );
    palette.setColor(QPalette::Disabled, QPalette::Text, mDisableColor->val() );
    palette.setColor(QPalette::Inactive, QPalette::Base, mBackgroundColor->val() );
    palette.setColor(QPalette::Disabled, QPalette::Base, mBackgroundColor->val() );

    mNumDisplay->setPalette( palette, true );
    mNumDisplay->setType();
  }
}


void CYDisplaySimple::updateTextLabelStyle()
{
  if (!mTextLabel)
    return;

  mTextLabel->setEnableColor( mEnableColor->val() );
  mTextLabel->setDisableColor( mDisableColor->val() );
  QPalette pal = palette();
  pal.setColor(QPalette::Window, mBackgroundColor->val());
  mTextLabel->setPalette(pal);
  if (!mCustomColor->val())
  {
    mTextLabel->setEnableProcessColor( true );
  }
  else
  {
    mTextLabel->setEnableProcessColor( false );
  }
}

/*!
    \fn CYDisplaySimple::init()
 */
void CYDisplaySimple::init()
{
  mLed        = 0;
  mNumDisplay = 0;
  mTextLabel  = 0;
  mFlagInput  = 0;
  mNumInput   = 0;
  mTextInput  = 0;

  mDB->setGroup(tr("Simple display"));

  mNumBase = new CYU8(mDB, "NUM_BASE", new u8, Cy::BaseData);
  mNumBase->setLabel(tr("Numerical base"));
  mNumBase->addNote(tr("The numerical base is used only if the data is only an unsigned integer type!"));
  mNumBase->setChoiceLabel(Cy::BaseData, tr("Automatic"));
  mNumBase->setChoiceLabel(Cy::Binary, tr("Binary"));
  mNumBase->setChoiceLabel(Cy::Decimal, tr("Decimal"));
  mNumBase->setChoiceLabel(Cy::Hexadecimal, tr("Hexadecimal"));
  mNumBase->setChoiceLabel(Cy::Octal, tr("Octal"));

  mDB->setUnderGroup(tr("Style"));

  mLedBicolor = new CYFlag(mDB, "LED_BICOLOR", new flg, false);
  mLedBicolor->setLabel(tr("Bicolor"));

  mLedColorOn = new CYColor(mDB, "LED_COLOR_ON", new QColor(Qt::green));
  mLedColorOn->setLabel(tr("Color On"));

  mLedColorOff = new CYColor(mDB, "LED_COLOR_OFF", new QColor(Qt::red));
  mLedColorOff->setLabel(tr("Color Off"));

  mLedShape = new CYS16(mDB, "LED_SHAPE", new s16, CYLed::Circular, 0);
  mLedShape->setLabel(tr("Shape"));
  mLedShape->setChoiceLabel(CYLed::Rectangular , tr("Rectangular"));
  mLedShape->setChoiceLabel(CYLed::Circular    , tr("Circular"));

  mLedLook = new CYS16(mDB, "LED_LOOK", new s16, CYLed::Raised, 0);
  mLedLook->setLabel(tr("Look"));
  mLedLook->setChoiceLabel(CYLed::Flat   , tr("Flat"));
  mLedLook->setChoiceLabel(CYLed::Raised , tr("Raised"));
  mLedLook->setChoiceLabel(CYLed::Sunken , tr("Sunken"));

  mShowUnit = new CYFlag(mDB, "SHOW_UNIT", new flg, true);
  mShowUnit->setLabel(tr("Show unit"));

  mCustomColor = new CYFlag(mDB, "CUSTOM_COLOR", new flg, false);
  mCustomColor->setLabel(tr("Customized colors"));
  mCustomColor->setHelp(0, 0, tr("The colors will be those of data"));

  mEnableColor = new CYColor(mDB, "ENABLE_COLOR", new QColor(Qt::black));
  mEnableColor->setLabel(tr("Text color of valid value"));

  mDisableColor = new CYColor(mDB, "DISABLE_COLOR", new QColor(Qt::gray));
  mDisableColor->setLabel(tr("Text color of not valid value"));

  mBackgroundColor = new CYColor(mDB, "BACKGROUND_COLOR", new QColor(palette().color(QPalette::Window)));
  mBackgroundColor->setLabel(tr("Background color"));

  mDB->setUnderGroup(0);
//   mWordwrap = new CYFlag(mDB, "WORDWRAP", new flg, false);
//   mWordwrap->tr("Word wrap mode");
//
//   mHAlign = new CYU8(mDB, "HALIGN", new u8, false);
//   mHAlign->tr("Word wrap mode");

  if (!mFrame)
  {
    mDataWdg = new CYDataWdg(this);
  }
  else
  {
    mDataWdg = new CYDataWdg(mFrame);
  }

  setModified(false);
  mCreating= false;

  startRefresh();
}


/*! Mise à jour du format
    \fn CYDisplaySimple::updateFormat()
 */
void CYDisplaySimple::updateFormat()
{
  mUpdatingFormat = true;

  if (datas.isEmpty())
    return;

  CYDisplayItem *d = datas.first();

  if (!d)
    return;

  CYData *data = d->data;

  mUnit = data->unit();

  if ( mTitleAuto->val() )
    setTitle( d->label );
  else
    setTitle(mTitle->val());

  this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());

  mUpdatingFormat = false;
}

QString CYDisplaySimple::text(bool &ok)
{
  ok=false;
  CYDisplayItem *d = datas.first();

  if (!d)
    return 0;

  CYData *data = d->data;
  if (!data)
    return 0;

  ok=true;
  return data->toString(mShowUnit->val());
}
