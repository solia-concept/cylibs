//
// C++ Implementation: cydisplaysatusindicator
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "cydisplaysatusindicator.h"

// QT
#include <QLayout>
#include <QToolTip>
#include <QLabel>
// CYLIBS
#include "cycore.h"
#include "cydisplay.h"

CYDisplaySatusIndicator::CYDisplaySatusIndicator(QWidget *parent, const QString &name, CYDisplay *display)
 : QWidget(parent)
 , mDisplay(display)
{
  setObjectName(name);
  mFirstRefresh = false;
  QGridLayout *grid = new QGridLayout(this);
  mLabel = new QLabel(this);
  grid->setContentsMargins(0,0,0,0);
  grid->addWidget(mLabel, 0, 0);
  mTimer = new QTimer(this);
  if (core)
  {
    mConnectPixmap   = core->loadIcon("cyconnect_established.png", 22);
    mNoConnectPixmap = core->loadIcon("cyconnect_no.png", 22);
    mPausePixmap     = core->loadIcon("cyplayer_pause.png", 22);
  }
  connect(mTimer, SIGNAL(timeout()), SLOT(flash()));
}


CYDisplaySatusIndicator::~CYDisplaySatusIndicator()
{
}

/*! Met à jour l'image en fonction du status courant du widget.
    \fn CYDisplaySatusIndicator::refresh()
 */
void CYDisplaySatusIndicator::refresh()
{
  mLabel->clear();
  switch (mDisplay->status())
  {
    case CYDisplay::Connect:
      mCurrentPixmap = mConnectPixmap;
      mLabel->setToolTip(tr("Connection is OK."));
      mTimer->stop();
      break;
    case CYDisplay::NoConnect:
      mCurrentPixmap = mNoConnectPixmap;
      mLabel->setToolTip(tr("Connection has been lost."));
      mTimer->start(500);
      break;
    case CYDisplay::Pause:
      mCurrentPixmap = mPausePixmap;
      mLabel->setToolTip(tr("Update is pausing."));
      mTimer->start(500);
      break;
    case CYDisplay::GlobalPause:
      mCurrentPixmap = mPausePixmap;
      mLabel->setToolTip(tr("The refresh of all graphic analyzers is pausing."));
      mTimer->start(500);
      break;
    default: CYMESSAGE;
  }
  flash();
  showOrHide();
  mFirstRefresh = true;
}

/*! Génération du clignotement.
    \fn CYDisplaySatusIndicator::flash()
 */
void CYDisplaySatusIndicator::flash()
{
  if (!mLabel->pixmap(Qt::ReturnByValue).isNull())
  {
    mLabel->setPixmap(mCurrentPixmap);
    adjustSize();
  }
  else
    mLabel->clear();
}


/*! @return \a true si refresh a été aplelée au moins une fois.
    \fn CYDisplaySatusIndicator::firstRefresh()
 */
bool CYDisplaySatusIndicator::firstRefresh()
{
  return mFirstRefresh;
}


/*!
    \fn CYDisplaySatusIndicator::showOrHide()
 */
void CYDisplaySatusIndicator::showOrHide()
{

  int width  = mDisplay->rect().width();
  int height = mDisplay->rect().height();
  int maxw   = frameGeometry().width()*8;
  int maxh   = frameGeometry().height()*8;
  if ((width<maxw) || (height<maxh))
    hide();
  else
    show();
}
