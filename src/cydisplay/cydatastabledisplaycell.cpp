/***************************************************************************
                          cydatastabledisplaycell.cpp  -  description
                             -------------------
    begin                : mer jan 26 2005
    copyright            : (C) 2005 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydatastabledisplaycell.h"

// CYLIBS
#include "cydatastable.h"
#include "cydatastablerow.h"
#include "cydatastablecol.h"
#include "cydisplaydummy.h"
#include "cydisplaysimple.h"
#include "cymultimeter.h"
#include "cyscope.h"
#include "cyscopemultiple.h"
#include "cybargraph.h"
#include "cycore.h"
#include "cydata.h"
//Added by qt3to4:
#include <QMenu>

CYDatasTableDisplayCell::CYDatasTableDisplayCell(CYDatasTable *table, CYDatasTableRow *row, CYDatasTableCol*col, QWidget *parent, const QString &name)
: CYDisplayCell(parent,name)
{
  mTable = table;
  mTableRow = row;
  mTableCol = col;
  row->addDisplayCell(this);
  connect(this, SIGNAL(modified(bool)), mTable, SLOT(setModified(bool)));
  init();
}

CYDatasTableDisplayCell::~CYDatasTableDisplayCell()
{
}

void CYDatasTableDisplayCell::init()
{
  mDisplay  = new CYDisplayDummy(this, "CYDisplayDummy");
  mGrid->addWidget( mDisplay, 0, 0 );
}

bool CYDatasTableDisplayCell::createDisplayFromDOM(QDomElement &el)
{
  QString classType = el.attribute("class");

  CYDisplay* newDisplay;
  if (classType == "CYDisplaySimple")
    newDisplay = new CYDisplaySimple(this, "CYDisplaySimple", 0, true);
  else if (classType == "CYMultimeter")
    newDisplay = new CYMultimeter(this, "CYMultimeter", 0, true);
  else if (classType == "CYScope")
    newDisplay = new CYScope(this, "CYScope", 0, true);
  else if (classType == "CYScopeMultiple")
    newDisplay = new CYScopeMultiple(this, "CYScopeMultiple", 0, true);
  else if (classType == "CYBargraph")
    newDisplay = new CYBargraph(this, "CYBargraph", 0, true);
  else
  {
    newDisplay = new CYDisplayDummy(this, "CYDisplayDummy");
  }
  Q_CHECK_PTR(newDisplay);

  // charge les paramètres spécifiques d'affichage
  if (!newDisplay->createFromDOM(el))
    return false;

  setDisplay(newDisplay);

  return true;
}

bool CYDatasTableDisplayCell::addDisplayToDOM(QDomDocument &doc, QDomElement &element, bool save)
{
  if (mDisplay && QString(mDisplay->metaObject()->className())=="CYDisplayDummy")
    return false;

  QDomElement el = doc.createElement("display");
  element.appendChild(el);
  el.setAttribute("class", mDisplay->metaObject()->className());
  el.setAttribute("col", mTableCol->id);
  mDisplay->addToDOM(doc, el, save);
  return true;
}

void CYDatasTableDisplayCell::setDisplaySimple(const QString& name, bool input)
{
  if (!core)
    return;

  /* Si l'afficheur existant est un emplacement vide
   * nous remplaçons le widget. Sinon nous essayons juste de remplacer
   * la nouvelle donnée à l'afficheur existant. */
  if ( QString(mDisplay->metaObject()->className())=="CYDisplayDummy" )
  {
//    CYDisplay* newDisplay = 0;

    CYData *data=0;
    if ((data = core->findData(name)) == 0)
      CYFATAL

    CYDisplaySimple *display = new CYDisplaySimple(this, "CYDisplaySimple", 0, true, input);
    display->addData(data);
    display->installEventFilter(this);
    setDisplay(display);
  }
  else
  {
    CYData *data;
    if ((data = core->findData(name)) == 0)
      CYFATAL
    mDisplay->addData(data);
  }
  setModified(true);
}

void CYDatasTableDisplayCell::setDisplay(const QString& name)
{
  if (!core)
    return;

  bool group = false;
  if (name.contains(tr("Group")))
    group = true;

  /* Si l'afficheur existant est un emplacement vide
   * nous remplaçons le widget. Sinon nous essayons juste d'ajouter
   * la nouvelle donnée à l'afficheur existant. */
  if ( QString(mDisplay->metaObject()->className())=="CYDisplayDummy" )
  {
    CYDisplay* newDisplay = 0;

    CYData *data=0;
    if (!group)
    {
      if ((data = core->findData(name)) == 0)
        CYFATAL;
    }
    else
      CYMessageBox::sorry(this, tr("No display for datas group"));

    /* Si le type de donnée est supporté par plus d'un type d'afficheur
     * nous faisons apparaître un menu afin que l'utilisateur puisse
     * sélectionner quel afficheur il veut. */
    if (group || data)
    {
      QMenu pm(tr("Select a display type"));
      pm.addSeparator();

      QAction *simpleAct = pm.addAction(tr("&Simple"));
      simpleAct->setEnabled( !group );

      QMenu *pm2 = pm.addMenu(tr("&Multimeter"));
      QAction *analogAct = pm2->addAction(tr("&Analog"));
      analogAct->setEnabled( !group && data->isNum() && !data->isFlag() );
      QAction *digitalAct = pm2->addAction(tr("&Digital"));
      digitalAct->setEnabled( !group && data->isNum() && !data->isFlag() );
      pm2->setEnabled( !group && data->isNum() && !data->isFlag() );

      QAction *classicScopeAct = pm.addAction(tr("&Classical oscilloscope"));
      classicScopeAct->setEnabled( !group && data->isNum() && /*!data->isFlag() && */(data->mode()==Cy::User) );
      classicScopeAct->setWhatsThis(tr("Oscilloscope may have two ordinate axes, each with its own format."));

      QAction *multiScopeMAct = pm.addAction(tr("Oscilloscope multiple formats"));
      multiScopeMAct->setEnabled( !group && data->isNum() && /*!data->isFlag() && */(data->mode()==Cy::User) );
      multiScopeMAct->setWhatsThis(tr("Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets."));

      QAction *bargraphAct = pm.addAction(tr("&BarGraph"));
      bargraphAct->setEnabled( !group && data->isNum() && !data->isFlag() );

      QAction *dataTableAct = pm.addAction(tr("Datas &table"));

      QAction *action = pm.exec(QCursor::pos());
      if (action==simpleAct)
      {
        CYDisplaySimple *display = new CYDisplaySimple(this, "CYDisplaySimple");
        display->addData(data);
        newDisplay = display;
      }
      else if (action==analogAct)
      {
        CYMultimeter *display = new CYMultimeter(this, "CYMultiMeter", 0, false, true, false);
        display->addData(data);
        newDisplay = display;
      }
      else if (action==digitalAct)
      {
        CYMultimeter *display = new CYMultimeter(this, "CYMultiMeter", 0, false, false, true);
        display->addData(data);
        newDisplay = display;
      }
      else if (action==classicScopeAct)
      {
        CYScope *display = new CYScope(this, "CYScope", 0, false, CYScope::Time, CYScope::Continuous);
        display->addData(data);
        newDisplay = display;
      }
      else if (action==multiScopeMAct)
      {
        CYScopeMultiple *display = new CYScopeMultiple(this, "CYScope", 0, false, CYScope::Time, CYScope::Continuous);
        display->addData(data);
        newDisplay = display;
      }
      else if (action==bargraphAct)
      {
        CYBargraph *display = new CYBargraph(this, "CYBarGraph");
        display->addData(data);
        newDisplay = display;
      }
      else if (action==dataTableAct)
      {
        CYDatasTable *display = new CYDatasTable(this, "CYDatasTable");
        newDisplay = display;
        if (group)
          display->addGroup(name);
        else
          display->addData(data);
      }
      else if (action==analogAct)
      {
        CYMultimeter *display = new CYMultimeter(this, "CYMultiMeter", 0, false, true, false);
        display->addData(data);
        newDisplay = display;
      }
      else
        return;
    }
    else
    {
      if (!group && !data->isNum())
        CYMessageBox::sorry(this, tr("No display for this type of data"));
      return;
    }
    newDisplay->installEventFilter(this);
    setDisplay(newDisplay);
  }
  else
  {
    CYData *data;
    if ((data = core->findData(name)) == 0)
      CYFATAL
    mDisplay->addData(data);
  }


//  setModifieOnResize(true);
  setModified(true);
}

void CYDatasTableDisplayCell::setDisplay(CYDisplay *display)
{
  if (mDisplay)
  {
   // mCellLayout->removeWidget(mDisplay);
    stopRefresh();
    delete mDisplay;
  }

  if (!display)
  {
    // insert un nouvel afficheur
    mDisplay = new CYDisplayDummy(this, "CYDisplayDummy");
  }
  else
  {
    mDisplay = display;
//    if (mDisplay->useSheetRefreshTime())
//      mDisplay->setUpdateInterval(updateIntervalSec() * 1000 + updateIntervalMSec());
//    connect(mDisplay, SIGNAL(showPopupMenu(CYDisplay*)), mTable, SLOT(showPopupMenu(CYDisplay*)));
//    connect(mDisplay, SIGNAL(showPopupMenu(CYDisplay*)), mTable, SLOT(showPopupMenu(CYDisplay*)));
    connect(mDisplay, SIGNAL(displayModified(bool)), mTable, SLOT(setModified(bool)));
  }

//  mCellLayout->addWidget( mDisplay );
  mGrid->addWidget( mDisplay, 0, 0 );


  if (isVisible())
  {
    mDisplay->show();
    mDisplay->refresh();
//    // Informe le parent de la possibilité d'avoir une nouvelle taille.
//    ((QWidget*) mSheet->parent()->parent())->setMinimumSize(((QWidget*) mSheet->parent()->parent())->sizeHint());
  }
  mGrid->activate();

  if (isVisible())
    linkDatas();

  startRefresh();
  setModified(true);
}

void CYDatasTableDisplayCell::focusInEvent(QFocusEvent * event)
{
  CYDisplayCell::focusInEvent(event);
  mTable->setCurrentCell(mTableRow->id, mTableCol->id);
}

QString CYDatasTableDisplayCell::text(bool &ok)
{
  return mDisplay->text(ok);
}
