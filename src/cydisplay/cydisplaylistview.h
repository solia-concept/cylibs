/***************************************************************************
                          cydisplaylistview.h  -  description
                             -------------------
    begin                : ven déc 17 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDISPLAYLISTVIEW_H
#define CYDISPLAYLISTVIEW_H

// QT
#include <QTreeWidget>
#include <QDropEvent>
#include <QDragEnterEvent>

/** @short Liste des données traîtées par l'afficheur.
  * @author LE CLÉACH Gérald
  */

class CYDisplayListView : public QTreeWidget
{
   Q_OBJECT
public: 
  CYDisplayListView(QWidget *parent=0, const QString &name=0);
  ~CYDisplayListView();

protected: // Protected methods
  /** Appelée lors d'un glisser à l'intérieur du widget. */
  virtual void dragEnterEvent( QDragEnterEvent * ev ) ;
  /** Appelée lorsqu'on fini un glisser-déposer à l'intérieur du widget. */
  virtual void dropEvent( QDropEvent * ev );
};

#endif
