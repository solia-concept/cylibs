/***************************************************************************
                          cyscopeplotter.h  -  description
                             -------------------
    début                  : mer sep 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYSCOPEPLOTTER_H
#define CYSCOPEPLOTTER_H

// QT
#include <QList>
#include <QInputDialog>
// CYLIBS
#include "cyplot.h"
#include "cyscope.h"

class CYDB;
class CYData;
class CYF64;
class CYFlag;
class CYU8;
class CYS16;
class CYU32;
class CYColor;
class CYScopeSignal;
class CYScopeAcquisition;
class CYScopeAxis;

/** @short Traceur d'oscilloscope.
  * @author Gérald LE CLEACH.
  * @see QwtPlot
  */

class CYScopePlotter : public CYPlot
{
  Q_OBJECT

  /** La classe CYScope y a accès libre. */
  friend class CYScope;

signals: // Signals
  /** Emis avec en paramètre \a true à chaque modification de la configuration de l'oscilloscope. */
  void modified( bool state );
  /** Emis à chaque validation des valeurs de l'ensemble des données. */
  void validate();

public:
  /** Constructeur du traceur. */
  CYScopePlotter( CYScope *scope, QWidget *parent = 0, const QString &name = 0, int timerInterval = 2000, CYDB *db = 0, CYScope::Mode mode = CYScope::Time, CYScope::SamplingMode sample = CYScope::Continuous, bool capture=false);
  /** Destructeur du traceur. */
  ~CYScopePlotter();

  /** @return l'oscilloscope auquel il est rattaché. */
  CYScope *scope()
  {
    return mScope;
  }

  /** Fonction d'initialisation de l'échelle des temps. */
  void initTimeScale( uint nb, float step, int burstsRatio = 1 );

  /** Ajoute une courbe de mesure au scope. */
  CYScopeSignal *addCurve( CYData *data, int id, QString label, QPen pen, double coefficient, QwtPlot::Axis yAxis = QwtPlot::yLeft, bool trigger=false );
  /** Supprime une courbe de mesure du scope.
    * @param down Indique si la suppression est temporaire.  */
  void deleteCurve( int idx, bool tmp = false );

//  /** @return le titre du scope. */
//  QString title() const;

  /** @return l'aide en bulle. */
  QString tooltip()
  {
    return mToolTip;
  }

  /** @return \a la donnée du mode d'utilisation. */
//   CYU8 *mode()
//   {
//     return mMode;
//   }
  /** Saisie le mode d'utilisation. */
  void setModeVal( CYScope::Mode mode );
  /** @return \a le mode d'utilisation. */
  CYScope::Mode modeVal();

  /** @return \a la donnée du mode d'échantillonnage. */
//   CYU8 *samplingMode()
//   {
//     return mSamplingMode;
//   }
  /** Saisie le mode d'échantillonnage. */
  void setSamplingModeVal( CYScope::SamplingMode samplingMode );
  /** @return \a le mode d'échantillonnage. */
  CYScope::SamplingMode samplingModeVal();

  /** Récupère l'intervalle de temps de rafraîchissement. */
  int getTimerInterval() const
  {
    return timerInterval;
  }
  /** Récupère la taille de la plage de mesure. */
  uint getSizeMesure() const
  {
    return size_mesure;
  }

  /** Récupère l'offset de la plage de mesure. */
  uint getOffsetMesure() const
  {
    return offset_mesure;
  }

  /** Fixe l'intervalle de temps de rafraîchissement. */
  void setTimerInterval( const int time )
  {
    timerInterval = time;
  }
  /** Fixe la taille de la plage de mesure. */
  void setSizeMesure( const uint size )
  {
    size_mesure = size;
  }
  /** Fixe l'offset de la plage de mesure. */
  void setOffsetMesure( const uint offset )
  {
    offset_mesure = offset;
  }

  /** Fixe les données de l'axe des X. */
  // TOCHECK QT5 void setRawDataX( const double *data, uint size );
  /** Change l'échelle des x.
      @param r2l Force le sens de l'axe de gauche à droite. */
  void changeRangeX(int axis, double min, double max);
  /** Change l'échelle des x. */
  void changeRangeX();
  /** Change l'échelle des y. */
  void changeRangeY(int axis, double min, double max);
  /** Change l'échelle des y. */
  void changeRangeY();

  /** Change la couleur de la grille principale. */
  void setGridColor( const QColor & );
  /** Change la couleur de la grille secondaire. */
  void setGridMinColor( const QColor & );

  /** Position de la légende. */
  virtual void setLegendPosition( const int val );

  /** Gestion du clavier. */
  virtual void keyPressEvent( QKeyEvent *e ) { Q_UNUSED(e); }
  /** Gestion de redimmensionnement du scope. */
  void resizeEvent( QResizeEvent *e );

  /** Affichage des légendes si le scope est assez grand
      @param size:    : Taille du widget plot. */
  void enableLegend( const QSize size );

  /** @return le titre de la courbe \a curve. */
  QString curveTitle( int curve );
  /** @return la légende de la courbe \a curve qui peut etre différente du titre si le libellé a été modifié. */
  QString curveLegend( int curve );
  /** @return la couleur de la courbe \a curve. */
  QColor curveColor( int curve );

  /** Fonction de copie des paramètres du scope. */
  void copy( CYScopePlotter *s );
  virtual CYScopeSignal * xSignal( int xAxis = xBottom );
  virtual CYScopeSignal *addXSignal( CYData *data, QString label, double coefficient, QwtPlot::Axis xAxis);
  virtual bool createFromDOM( QDomElement &element );
  virtual void addToDOM( QDomDocument &doc, QDomElement &element );

  /** Base de données de cet afficheur. */
  CYDB *db()
  {
    return mDB;
  }
  virtual CYScopeAcquisition * acquisition();
  virtual void exportCurves( bool saveAs = true );
  virtual void exportCurves( bool saveAs, bool newCapture, bool open );
  void backupScaleValues();
  void setTimeAxis( QwtPlot::Axis xAxis = xBottom )
  {
    mTimeAxis = xAxis;
  }
  QwtPlot::Axis timeAxis()
  {
    return mTimeAxis;
  }
  /** @a return l'axe des temps. */
  CYScopeAxis * timeScopeAxis();
  /** Construit l'échelle temporelle à partir des bornes \a left et \a right.*/
  void changeTimeScale(double left, double right);
  /** Construit l'échelle \a axis des x en mode XY à partir des bornes \a left et \a right.
      @param r2l Force le sens de l'axe de gauche à droite. */
  void changeXScale(int axis, double left, double right);

  /** Saisie le coefficient d'échelle du \a signal en Y. */
  virtual void setYCoef( CYScopeSignal *signal, double coef );
  /** Saisie l'offset d'échelle du \a signal en Y. */
  virtual void setYOffset( CYScopeSignal *signal, double offset );

  bool hasMultipleFormats();

  /*! @return \a TRUE si le trigger est activé
      \fn CYScope::triggerIsEnabled()
    */
  bool triggerIsEnabled();

  /** Capture déjà en cours. */
  bool capturing();

  /** @return \a true si au moins une courbe a sa donnée en bufferisation discontinue */
  bool discontData() { return mDiscontData; }

  /** @return la couleur de fond. */
  CYColor *backgroundColor() { return mBackgroundColor; }
  /** @return la couleur de premier plan.
   *  Cette couleur est automatiquement l'inverse de la couleur de fond. */
  virtual QColor foregroundColor();

signals: // Signals
  /** Emis lors d'un clic gauche de la souris. */
  void leftButtonMousePressed();
  /** Emis lors d'un clic droit de la souris. */
  void rightButtonMousePressed();

  void enableActionZoomIn( bool );
  void enableActionZoomOut( bool );
  /*!
    Un signal est émis lorsque la souris reçoit un double clique
    dans le traceur.
    \param e Mouse event object
   */
  void mouseDoubleClick();

  /** Emis au début et à la fin d'une capture d'une acquistion dicontinue. */
  void discontCapturing(bool start);

public slots: // Public slots
  /** Rafraîchit le traceur. */
  virtual void refresh();
  virtual void curveClicked( long );
  virtual void yLeftClicked();
  virtual void yRightClicked();
  virtual void xTopClicked();
  virtual void xBottomClicked();
  /** Met à jour la taille de la police. */
  void updateFontSize();

  virtual void plotMousePressed( const QMouseEvent &e );
  virtual void plotMouseReleased( const QMouseEvent &e );
  virtual void resetCurves();
  virtual void applySettings();
  virtual void enableCurve( int key );
  virtual void disableCurve( int key );
  virtual void editCurveLabel( int curve );
  virtual void editCurvePen( int curve );
  virtual void editCurveCoef( int curve );
  virtual void setFontSize( int val );

  /** Ouvre la boîte de confifuration du trigger. */
  virtual void setupTrigger();
  /** Active ou désactive le trigger suivant \a val. */
  virtual void enableTrigger(bool val);
  /** Reset le trigger pour recommencer une détection. */
  virtual void resetTrigger();

  /** Efface la liste des signaux disponible pour le trigger */
  virtual void clearTriggerSignals();
  /** Saisie le signal source du trigger */
  virtual void setTriggerSource(CYScopeSignal * signal);
  /** Saisie le format du trigger */
  virtual void setTriggerFormat(CYFormat *format);

  /** Autorise ou non la bufferisation des valeurs. */
  void setEnableBuffering(bool val);

protected:
  /** Fonction d'initialisation des paramètres du scope. */
  void init( int timer, CYScope::Mode mode, CYScope::SamplingMode acq);
  /** Ajoute une courbe de mesure au scope. */
  CYScopeSignal *addCurve( QwtPlot::Axis yAxis, CYData *data, int id, QString label, QPen pen, double coefficient, bool trigger);
  /** Ajoute un signal en Y. */
  virtual void addYSignal( int id, CYScopeSignal *signal );
  /** Construit l'échelle temporelle */
  void changeTimeScale();
  /** Construit les échelles des x en mode XY */
  void changeXScales();
  /** Construction de l'aide. */
  virtual void createHelp();
  virtual void showEvent( QShowEvent *e );
  virtual void hideEvent( QHideEvent *e );
  virtual void refreshTimeMode();
  virtual void refreshXYMode();
  virtual void refreshXYMode( int Axis );
  virtual void initDB();
  virtual void updateCurvesStyle();
  virtual void updateCurvesPen();
  virtual int fontSize();
  virtual void mouseDoubleClickEvent(QMouseEvent * event);

public:
  /** Liste des voies de mesure actives en y. */
  QList<CYScopeSignal*> ySignal;

  QHash<int, CYScopeAxis*> scopeAxis;

  int posXcur1;
  int posYcur1;
  int posXcur2;
  int posYcur2;

  void updateFormat();

protected: // Protected attributes
  /** Oscilloscope auquel il est rattaché. */
  CYScope *mScope;
  /** Base de données de cet afficheur. */
  CYDB *mDB;

  /** Couleur de fond. */
  CYColor *mBackgroundColor;

  /** Affichage de la grille principale si majGrid est à 'true'. */
  CYFlag *mMajGrid;
  /** Couleur de la grille principale. */
  CYColor *mMajGridColor;
  /** Crayon de la grille principale. */
  QPen majGridPen;
  /** Affichage de la grille secondaire si minGrid est à 'true'. */
  CYFlag *mMinGrid;
  /** Couleur de la grille secondaire. */
  CYColor *mMinGridColor;
  /** Crayon de la grille secondaire. */
  QPen minGridPen;

  /** Position des légendes @see QwtPlot::LegendPosition */
  CYS16 *mLegendPosition;
  /** Taille du scope suffisante pour afficher les légendes. */
  bool sizeEnableLegend;

  /** Signaux en X (non disponible en mode Time) */
  QHash<int, CYScopeSignal*> mXSignal;

  /** Aide en bulle. */
  QString mToolTip;
  /** Mode d'utilisation du scope. */
  CYU8 *mMode;
  /** Mode d'échantilonnage du scope. */
  CYU8 *mSamplingMode;

  /** Style des courbes. */
  CYU8 *mCurvesStyle;
  /** Epaisseur des courbes. */
  CYU32 *mCurvesWidth;

  /** Source trigger. */
  CYU8 *mTriggerSource;
  /** Seuil du trigger. */
  CYF64 *mTriggerLevel;
  /** Front du trigger. */
  CYS16 *mTriggerSlope;
  /** Post-Trigger. */
  CYF64 *mPostTrigger;
  bool mTriggerEnabled;

protected:
  /** Intervalle de temps de rafraîchissement. */
  int timerInterval;
  /** Taille de la plage de mesure. */
  uint size_mesure;
  /** Offset de la plage de mesure. */
  uint offset_mesure;

  /** Etat de l'échelle des x. Ce flag est à 'false' lorsque l'échelle est en cours de
    * redimensionnement afin de ne pas générer d'erreur lors du raffraichissement du scope. */
  bool rangeX;

  /** Axe de référence de y (axe gauche). */
  long yLeftRef;
  /** Couleur de l'axe de référence des y (axe gauche). */
  QColor yLeftRefColor;
  /** Crayon de l'axe de référence des y  (axe gauche). */
  QPen yLeftRefPen;

  /** Axe de référence de y (axe droit). */
  long yRightRef;
  /** Couleur de l'axe de référence des y (axe droit). */
  QColor yRightRefColor;
  /** Crayon de l'axe de référence des y  (axe droit). */
  QPen yRightRefPen;

  QwtPicker::RubberBand mOutlineStyle;

  bool mZooming;
  double mZoomValX1;
  double mZoomValY1;
  double mZoomValX2;
  double mZoomValY2;
  double mZoomCoef;

  CYScopeAcquisition * mAcquisition;
  CYFlag *mAcquisitionEnable;
  QwtPlot::Axis mTimeAxis;

  bool mInitDB;
  /** Base de données propre à cet afficheur si \a true. */
  bool mLocalDB;

  bool mInit;
  bool mIsCapture;

  CYScopeSignal *mTriggerSignal;
  int mTriggerXMarker;
  int mTriggerYMarker;

  /** Vaut \a true si au moins une courbe a sa donnée en bufferisation discontinue */
  bool mDiscontData;
  int mDiscontDataState;
};

#endif
