//
// C++ Implementation: cyscopetriggersetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyscopetriggersetup.h"
#include "ui_cyscopetriggersetup.h"

// CYLIBS
#include "cyscope.h"
#include "cycombobox.h"
#include "cys16.h"
#include "cyscopesignal.h"
#include "cyformat.h"

CYScopeTriggerSetup::CYScopeTriggerSetup(QWidget *parent, QString name)
 : CYWidget(parent,name), ui(new Ui::CYScopeTriggerSetup)
{
	ui->setupUi(this);

	connect(ui->signalInput, SIGNAL(valueChanged(int)), SLOT(setSignal(int)));
}


CYScopeTriggerSetup::~CYScopeTriggerSetup()
{
	delete ui;
}



void CYScopeTriggerSetup::setScope(CYScope *scope)
{
  mScope = scope;
  setLocaleDB(mScope->db());
}

void CYScopeTriggerSetup::apply()
{
  CYWidget::apply();
}

void CYScopeTriggerSetup::setSignal(int id)
{
  if (id==0)
  {
    ui->group->setDisabled(true);
  }
  else
  {
    CYScopeSignal *signal = mScope->plot()->ySignal.at(id-1);
    if (signal)
    {
      mScope->plot()->setTriggerFormat(signal->data->format());
      ui->levelInput->refresh();
      ui->group->setEnabled(true);
    }
    else
    {
      ui->group->setDisabled(true);
    }
  }

}

void CYScopeTriggerSetup::showEvent(QShowEvent *e)
{
  CYWidget::showEvent(e);
  CYComboBox *box = ui->signalInput;
  CYS16 *source = (CYS16 *)box->CYDataWdg::data();
  if (source)
    setSignal(source->val());
}
