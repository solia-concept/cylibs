//
// C++ Interface: cydatastablesetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDATASTABLESETUP_H
#define CYDATASTABLESETUP_H

// CYLIBS
#include <cydialog.h>

class CYDatasTable;
class CYDatasListView;

namespace Ui {
    class CYDatasTableSetup;
}

/**
@short Boîte de configuration d'un CYDatasTable.

@author Gérald LE CLEACH
*/
class CYDatasTableSetup : public CYDialog
{
  Q_OBJECT

public:
  CYDatasTableSetup(CYDatasTable *parent = 0, const QString &name = 0);

  ~CYDatasTableSetup();

  CYDatasListView *dataEditList();

protected:
  virtual void init();
protected:
  CYDatasTable * mDatasTable;
public slots:
  virtual void apply();
private slots:
  void help();

private:
  Ui::CYDatasTableSetup *ui;
};

#endif
