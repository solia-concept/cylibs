/***************************************************************************
                          cymultimeter.h  -  description
                             -------------------
    début                  : Mon Feb 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYMULTIMETER_H
#define CYMULTIMETER_H

// ANSI
#include <math.h>
#include <stdlib.h>
// CYLIBS
#include "cydisplay.h"
#include "cylcdnumber.h"

class CYDisplayItem;
class CYMultimeterSetup;
class CYDial;
class CYDB;
class CYData;
class CYU8;
class CYF64;
class CYFlag;
class CYColor;

/**
  * CYMultimeter est un afficheur de type multimètre.
  * @short Multimètre.
  * @author Gérald LE CLEACH.
  */

class CYMultimeter : public CYDisplay
{
  Q_OBJECT
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)

public:
  /** Construit un afficheur de type multimètre.
    * @param parent  Widget parent.
    * @param name    Nom de l'afficheur.
    * @param title   Titre de l'afficheur.
    * @param nf      Pas de trame existante.
    * @param ana     Affiche le multimètre analogique.
    * @param lcd     Affiche le multimètre numérique. */
  CYMultimeter(QWidget *parent=0, const QString &name=0, const QString &title=0, bool nf=false, bool ana=false, bool lcd=true);
  virtual ~CYMultimeter();

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return le nom de la donnée flag associée. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée. */
  virtual void setFlagName(const QByteArray &name);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return si le flag associé doit être pris en compte dans le sens inverse. */
  bool inverseFlag() const;
  /** Inverse le sens du flag associé si \a inverse est à \p true. */
  void setInverseFlag(const bool inverse);

  /** Ajoute une donnée dans l'afficheur. */
  virtual bool addData(CYData *data);
  /** Ajoute une donnée dans l'afficheur avec une étiquette. */
  virtual bool addData(CYData *data, QString label);

  void resizeEvent(QResizeEvent *);

  bool createFromDOM(QDomElement &element, QStringList *ErrorList=nullptr);
  bool addToDOM(QDomDocument &doc, QDomElement &element, bool save=true);

  virtual bool hasSettingsDialog() const
  {
    return (true);
  }

  void settings();

  /** Gestion du menu surgissant.
    * @param pm0    Menu appelant s'il existe.
    * @param index  Index de l'entrée du menu appelant.
    * @param end    Fin du menu. */
  virtual int showPopupMenu(QMenu *pm0=0, int index=0, bool end=false);

  /** @return \a true le multimètre analogique doit être affiché. */
  virtual bool showAna();
  /** Saisie \a true pour afficher le multimètre analogique. */
  virtual void setShowAna(bool val);

  /** @return \a true le multimètre LCD doit être affiché. */
  virtual bool showLCD();
  /** Saisie \a true pour afficher le multimètre LCD. */
  virtual void setShowLCD(bool val);

public slots:
   /** Rafraîchit l'afficheur. */
  virtual void refresh();
  /** Met à jour la vue. */
  void updateView();
  void applySettings();

  virtual void updateFormat();

protected: // Protected methods
  /** Un double-clic permet en mode simulation de changer la valeur de la donnée visualisée */
  virtual void mouseDoubleClickEvent ( QMouseEvent * e );

private: // Private attributes
  /** Afficheur analogique. */
  CYDial *mAna;

  CYU8 *mNumBase;
  /** Vaut \a true pour afficher le multimètre analogique. */
  CYFlag *mShowAna;

  CYF64 *mAlarmLowerBound;
  CYFlag *mAlarmLowerEnable;
  CYF64 *mAlarmUpperBound;
  CYFlag *mAlarmUpperEnable;

  CYColor *mAnaTextColor;
  CYColor *mAnaNormalColor;
  CYColor *mAnaAlarmColor;
  CYColor *mAnaBackgroundColor;

  CYColor *mLCDNormalColor;
  CYColor *mLCDAlarmColor;
  CYColor *mLCDBackgroundColor;

  /** Afficheur LCD. */
  CYLCDNumber *mLCD;
  /** Vaut \a true pour afficher le multimètre LCD. */
  CYFlag *mShowLCD;

  CYMultimeterSetup *mSetup;
};

#endif
