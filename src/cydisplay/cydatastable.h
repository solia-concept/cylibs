/***************************************************************************
                          cydatastable.h  -  description
                             -------------------
    begin                : mer nov 3 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDATASTABLE_H
#define CYDATASTABLE_H

// QT
#include <QMap>
#include <QtPrintSupport/QPrinter>
// CYLIBS
#include "cydisplay.h"

class CYWidget;
class CYDatasTableCol;
class CYDatasTableRow;
class CYDatasTablePlotter;
class CYDatasTableSetup;

/** @short Tableau de données.
  * @author LE CLÉACH Gérald
  */

class CYDatasTable : public CYDisplay
{
  Q_OBJECT
  Q_PROPERTY( bool printDesignerLogo READ printDesignerLogo WRITE setPrintDesignerLogo )
  Q_PROPERTY( bool printDesignerRef READ printDesignerRef WRITE setPrintDesignerRef )
  Q_PROPERTY( int printHeightRow READ printHeightRow WRITE setPrintHeightRow )

  friend class CYDatasTableSetup;
  friend class CYDatasTablePlotter;

public:
  /** Construit un tableau de données.
    * @param parent  Widget parent.
    * @param name    Nom de l'oscilloscope.
    * @param title   Titre de l'oscilloscope.
    * @param nf      Pas de trame existante. */
  CYDatasTable(QWidget *parent=0, const QString &name="datasTable", const QString &title=tr("Datas table"), bool nf=false);
  ~CYDatasTable();

  /** @return la traceur de table de données. */
  CYDatasTablePlotter *plot() { return mPlot; }

  /** Ajoute un groupe de données dans l'afficheur. */
  virtual bool addGroup(QString txt);
  /** Ajoute une donnée dans l'afficheur. */
  virtual bool addData(CYData *data);
  /** Ajoute une donnée dans l'afficheur avec une étiquette. */
  virtual bool addData(CYData *data, QString label);
  /** Ajoute une donnée dans l'afficheur avec les paramètres d'affichage dans le tableau.
    * @return la nouvelle ligne créée. */
  CYDatasTableRow *addData(CYData *data, int pos, bool shown, int height, QString label);
  /** Supprime une donnée de l'afficheur. */
  virtual bool removeData(uint idx);

  /** Construit un afficheur à partir d'un DOM. */
  virtual bool createFromDOM(QDomElement &, QStringList *ErrorList=nullptr);
  /** Ajoute un afficheur dans un DOM. */
  virtual bool addToDOM(QDomDocument &, QDomElement &, bool=true);

  /** @return le nombre de colonnes du dictionnaire de colonnes. */
  int nbCols();
  /** @return la colonne à l'index \a i du dictionnaire de colonnes. */
  CYDatasTableCol *col(int i);
  /** @return la colonne de type. */
  CYDatasTableCol *colType(int type);

  /** @return le nombre de lignes du dictionnaire de lignes. */
  int nbRows();
  /** @return la ligne à l'index \a i du dictionnaire de lignes. */
  CYDatasTableRow *row(int i);
  /** @return la ligne à la ligne \a row du QTableWidget. */
  CYDatasTableRow *dtr(int row);

  virtual bool hasSettingsDialog() const
  {
    return (true);
  }

  /** Saisir \a true pour activer le mode forçage. */
  virtual void setForcing(const bool val);

  /** @return \a true lorsque le mode RAZ est actif. */
  virtual bool reset() const { return mReset; }
  /** Saisir \a true pour activer le mode RAZ. */
  virtual void setReset(const bool val);

  /** @return \a true lorsque le mode valeur partielle est actif. */
  virtual bool partial() const { return mPartial; }
  /** Saisir \a true pour activer le mode valeur partielle. */
  virtual void setPartial(const bool val);

  /** @return \a true lorsque le mode note est actif. */
  virtual bool note() const { return mNote; }
  /** Saisir \a true pour activer le mode note. */
  virtual void setNote(const bool val);

  /** @return \a true lorsque le mode seuil d'alerte est actif. */
  virtual bool threshold() const { return mThreshold; }
  /** Saisir \a true pour activer le mode seuil d'alerte . */
  virtual void setThreshold(const bool val);

  /** @return la colonne ayant pour position \a pos. */
  CYDatasTableCol *columnAtPos(int pos);

  virtual bool connected();

  /** Saisie la cellule courante étant la cellule à la position (row, column).*/
  virtual void setCurrentCell(int row, int column);

  /** Impression d'une page
    * @param page Nombre de pages déjà imprimés.
    * @param nb   Nombre de légendes de courbes déjà imprimés. */
  void printPage(QPainter *p, QPrinter *printer, int &page, int &nb);
  /** Impression de l'en-tête. */
  void printHeader(QPainter *p, QRectF *r);
  /** Impression du pied de page. */
  void printFooter(QPainter *p, QRectF *r, int page);
  /** Impression d'une ligne la table. */
  bool printRow(QPainter *p, QRectF *r, int numRow);

  /** @return \a true si le logo constructeur est à imprimer. */
  bool printDesignerLogo() const { return mPrintDesignerLogo; }
  /** @return \a true si la référence constructeur est à imprimer. */
  bool printDesignerRef() const { return mPrintDesignerRef; }
  /** @return la hauteur minimum d'une ligne pour l'impression.
    * 0 (valeur par défaut) optimise au plus juste la hauteur de ligne. */
  int printHeightRow() const { return mPrintHeightRow; }

  /** @return la colonne visuelle de la colonne logique \a logicalColumn. */
  int visualColumn(int logicalColumn) const;
  /** @return la ligne visuelle de la ligne logique \a logicalRow. */
  int visualRow(int logicalRow) const;

public slots: // Public slots
  /** Démarre le rafraîchissement. */
  virtual void startRefresh();
  /** Arrête le rafraîchissement. */
  virtual void stopRefresh();
  /** Rafraîchit la table. */
  virtual void refresh();
  /** Déplace la colonne à l'index visuel \a from à la place de celle à l'index visuel \a to. */
  virtual void moveColumn(int from, int to);
  /** Permute 2 colonnes. */
  virtual void swapColumns( int logicalIndex, int col1, int col2 );
  /** Permute 2 lignes. */
//  virtual void swapRows( int row1, int row2, bool swapHeader = false );
//  /** Permute 2 lignes. */
  /** Déplace la ligne à l'index visuel \a from à la place de celle à l'index visuel \a to. */
  virtual void moveRow(int from, int to);
  virtual void swapRows( int logicalIndex, int oldVisualIndex, int newVisualIndex );
  /** Affiche / cache la colonne \a column en fonction de \a enable. */
  void showColumn(bool enable, int pos);
  void settings();
  void applySettings();
  /** Masque la colonne courrante. */
  void maskCurrentColumn();
  /** Ajoute une colonne. */
  virtual CYDatasTableCol *addColumn();
  /** Ajoute une colonne.
    @param label      Étiquette par défaut.
    @param inputLabel Ouvre une boîte de saisie de l'étiquette. */
  virtual CYDatasTableCol *addColumn(QString label, bool inputLabel=true);
  /** Initialise les valeurs des données de forçage. */
  virtual void initForceValues();
  /** @return l'étiquette de la colonne en cours de traîtement */
  QString inputColumnLabel(QString label="");
  /** Affiche la colonne masquée sélectionnée par l'action relative \a action.
    * Recherche la première colonne ayant le même nom que l'action et l'affiche.
    */
  void showColumn(QAction *action);
  /** Affiche la colonne masquée d'indice \a id. */
  void showColumn(int id);
  /** Change l'étiquette de la colonne courante. */
  virtual void changeColumnLabel();
  /** Saisie l'étiquette de la colonne à la position \a pos. */
  virtual void setColumnLabel(int pos, QString label);
  /** Gestion du menu surgissant.
    * @param pm0    Menu appelant s'il existe.
    * @param index  Index de l'entrée du menu appelant.
    * @param end    Fin du menu. */
  virtual int showPopupMenu(QMenu *pm0=0, int index=0, bool end=false);

  /** Permute le mode forçage. */
  virtual void toggleForcing();
  /** Permute le mode RAZ. */
  virtual void toggleReset();
  /** Permute le mode valeur partielle. */
  virtual void togglePartial();
  /** Permute le mode note. */
  virtual void toggleNote();
  /** Permute le mode seuil d'alerte. */
  virtual void toggleThreshold();

  virtual void setColumnWidth();
  virtual void setColumnWidth(int w, int col);
  virtual void memColumnWidth(int logicalIndex, int oldSize, int newSize);
  virtual void memRowHeight(int logicalIndex, int oldSize, int newSize);
  virtual void removeRows();
  virtual void setRowHeight();
  virtual void setRowHeight(int h, int row);

  virtual void removeDatas();
  virtual void updateFormat();

  /*! Exporter la table dans un fichier texte
      @param saveAs Par défaut vaut à \a true pour avoir la boîte de sélection du fichier, pour avoir automatiquement un nom de fichier il suffit de le mettre à \a false.
      \fn CYDatasTable::exportTable(bool saveAs)
  */
  void exportTable( bool saveAs );
  /*! Exporter la table dans un fichier PDF
      \fn CYDatasTable::exportPDF(QString fileName)
  */
  void exportPDF(QString fileName="");

  /** Saisir \a true pour activer l'impression du logo constructeur. */
  void setPrintDesignerLogo(const bool enable) { mPrintDesignerLogo=enable; }
  /** Saisir \a true pour activer l'impression de la référence constructeur. */
  void setPrintDesignerRef(const bool enable) { mPrintDesignerRef=enable; }
  /** Saisie la hauteur minimum d'une ligne pour l'impression.
    * 0 (valeur par défaut) optimise au plus juste la hauteur de ligne. */
  void setPrintHeightRow(const int height) { mPrintHeightRow=height; }

protected: // Protected methods
  /** Initialise les colonnes. */
  virtual void initColumns();
  virtual void initTRDisplay(QString title);

protected: // Protected attributes
  /** Traceur de table de données. */
  CYDatasTablePlotter *mPlot;
  /** Widget CY afin de profiter des avantages entre CYWidget et les différents CYDataWdg. */
  CYWidget *mCYWdg;

  /** Dictionnaire des colonnes. */
  QMap<int, CYDatasTableCol*> mCols;
  /** Dictionnaire des lignes. */
  QMap<int, CYDatasTableRow*> mRows;
  /** Dictionnaire des colonnes masquées. */
  QMap<int, CYDatasTableCol*> mMaskedCols;

  /** Outil de configuration.  */
  CYDatasTableSetup *mSetup;

  /** Vaut \a true lorsque le mode RAZ est actif. */
  bool mReset;
  /** Vaut \a true lorsque le mode valeur partielle est actif. */
  bool mPartial;
  /** Vaut \a true lorsque le mode note est actif. */
  bool mNote;
  /** Vaut \a true lorsque le mode seuil d'alerte est actif. */
  bool mThreshold;
  /** Décalage en cours de permutation de colonne.*/
  int mSwapColsOffset;
  /** Décalage en cours de permutation de ligne .*/
  int mSwapRowsOffset;
  /** Nombre ligne imprimées */
  int mPrintRows;
  bool mPrintDesignerLogo;
  bool mPrintDesignerRef;
  int mPrintHeightRow;
  bool mPrintNewPage;
};

#endif
