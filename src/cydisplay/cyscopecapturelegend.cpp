#include "cyscopecapturelegend.h"

// QT
#include <QLabel>
#include <QLayout>
#include <QKeyEvent>
#include <QString>
#include <QMenu>
#include <QMouseEvent>
#include <QShowEvent>
#include <QHBoxLayout>
// QWT
#include "qwt_plot_dict.h"
#include "qwt_plot_canvas.h"
#include "qwt_scale_div.h"
#include "qwt_plot_layout.h"
#include "qwt_scale_draw.h"
#include "qwt_plot_grid.h"
#include "qwt_legend.h"
#include "qwt_text_label.h"
#include "qwt_legend_label.h"
// CYLIBS
#include "cycore.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cys16.h"
#include "cystring.h"
#include "cyframe.h"
#include "cyscopeaxis.h"
#include "cyscopesignal.h"
#include "cynumdisplay.h"
#include "cypixmapview.h"
#include "cytextlabel.h"
#include "cyscopesetup.h"
#include "cydataslistview.h"
#include "cydataseditlist.h"
#include "cyscopecapture.h"
#include "cyscopesignal.h"
#include "cyplotcurve.h"
#include "cyscopecursorlegend.h"

CYScopeCaptureLegend::CYScopeCaptureLegend(CYScopeCapture *parent) :
  QwtLegend(parent),
  mPlot(parent),
  mDB(parent->db())
{
  mInitAxisDisplay=false;
  initAxisDisplay();
}

/*!
  \brief Create a widget to be inserted into the legend

  The default implementation returns a QwtLegendLabel.

  \param data Attributes of the legend entry
  \return Widget representing data on the legend

  \note updateWidget() will called soon after createWidget()
        with the same attributes.
  \sa QwtLegend
 */
QWidget *CYScopeCaptureLegend::createWidget( const QwtLegendData &data ) const
{
  int curveKey=0;
  const QVariant key=data.value(CYPlotCurve::KeyRole);
  if ( key.canConvert<int>() )
  {
    curveKey = key.value<int>();
  }
  if (curveKey<1)
    return QwtLegend::createWidget( data );

  if (mPlot->ySignal.isEmpty() || (curveKey>mPlot->ySignal.count()))
    return QwtLegend::createWidget( data );

  CYScopeSignal *signal = mPlot->ySignal.at(curveKey-1);
  if (!signal)
  {
    CYWARNINGTEXT(QString("curveKey=%1").arg(curveKey));
    return QwtLegend::createWidget( data );
  }

  CYScopeCursorLegend *widget = new CYScopeCursorLegend(this, signal);
  return widget;
}


/*! Initilalise les afficheurs des curseurs des axes
    \fn CYScopeCaptureLegend::initAxisDisplay()
 */
void CYScopeCaptureLegend::initAxisDisplay()
{
  CYFrame *mFrame;
  QGridLayout *mGrid;
  int row=0;
  int col=0;

  QwtLegendLabel *button;
  CYNumDisplay *display;
  QWidget *hBox;
  QHBoxLayout *layout;

  mFrame = new CYFrame(contentsWidget());
  if ( contentsWidget()->layout() )
    contentsWidget()->layout()->addWidget( mFrame );
  mFrame->CYTemplate::startTimer(200);
  mGrid = new QGridLayout( mFrame );

  button = new QwtLegendLabel( mFrame );
  button->setText(tr("Cursors"));
  mGrid->addWidget(button, row, col, 1, -1);
  mGrid->setColumnStretch(0, 1);
  mGrid->setColumnStretch(1, 3);
  mGrid->setSpacing(1);

  row++;
  col=0;

  display = new CYNumDisplay;
  display->setShowLabel(true);
  display->setSuffix("\t");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "BOTTOM_CURSOR1" );
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mGrid->addWidget(display, row, col++, 1, 1);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setHideFlagName( "BOTTOM_ENABLE" );
  display->setInverseHideFlag(true);
  display->setDataName( "BOTTOM_CURSOR1" );
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mGrid->addWidget(display, row, col++, 1, -1);

  row++;
  col=0;

  display = new CYNumDisplay;
  display->setShowLabel(true);
  display->setSuffix("\t");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "LEFT_CURSOR1" );
  display->setHideFlagName( "LEFT_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mGrid->addWidget(display, row, col++, 1, 1);

  hBox = new QWidget;
  hBox->setContentsMargins(0,0,0,0);

  layout = new QHBoxLayout;
  layout->setContentsMargins(0,0,0,0);
  layout->setSpacing(1);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "LEFT_CURSOR1" );
  display->setHideFlagName( "LEFT_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  layout->addWidget(display);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "RIGHT_CURSOR1" );
  display->setHideFlagName( "RIGHT_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  layout->addWidget(display);

  hBox->setLayout(layout);
  mGrid->addWidget(hBox, row, col++, 1, -1);

  row++;
  col=0;

  display = new CYNumDisplay;
  display->setShowLabel(true);
  display->setSuffix("\t");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "BOTTOM_CURSOR2" );
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mGrid->addWidget(display, row, col++, 1, 1);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "BOTTOM_CURSOR2" );
  display->setHideFlagName( "BOTTOM_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mGrid->addWidget(display, row, col++, 1, 1);

  row++;
  col=0;

  display = new CYNumDisplay;
  display->setShowLabel(true);
  display->setSuffix("\t");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "LEFT_CURSOR2" );
  display->setHideFlagName( "LEFT_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mGrid->addWidget(display, row, col++, 1, 1);

  hBox = new QWidget;
  hBox->setContentsMargins(0,0,0,0);

  layout = new QHBoxLayout;
  layout->setContentsMargins(0,0,0,0);
  layout->setSpacing(1);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "LEFT_CURSOR2" );
  display->setHideFlagName( "LEFT_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  layout->addWidget(display);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "RIGHT_CURSOR2" );
  display->setHideFlagName( "RIGHT_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  layout->addWidget(display);

  hBox->setLayout(layout);
  mGrid->addWidget(hBox, row, col++, 1, -1);

  row++;
  col=0;

  display = new CYNumDisplay( mFrame );
  display->setShowLabel(true);
  display->setSuffix("\t");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "BOTTOM_DELTA_CURSOR" );
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mGrid->addWidget(display, row, col++, 1, 1);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "BOTTOM_DELTA_CURSOR" );
  display->setHideFlagName( "BOTTOM_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mGrid->addWidget(display, row, col++, 1, 1);

  row++;
  col=0;

  if (mPlot->modeVal()==CYScope::Time)
  {
    display = new CYNumDisplay( mFrame );
    display->setShowLabel(true);
    display->setSuffix("\t");
    display->setAlignment(Qt::AlignLeft);
    display->setLocaleDB(mDB);
    display->setDataName( "BOTTOM_INV_DELTA_CURSOR" );
    display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    display->setAlwaysOk(true);
    mGrid->addWidget(display, row, col++, 1, 1);

    hBox = new QWidget;
    layout = new QHBoxLayout;
    display = new CYNumDisplay;
    display->setAlignment(Qt::AlignLeft);
    display->setLocaleDB(mDB);
    display->setDataName( "BOTTOM_INV_DELTA_CURSOR" );
    display->setHideFlagName( "BOTTOM_ENABLE" );
    display->setInverseHideFlag(true);
    display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    display->setAlwaysOk(true);
    mGrid->addWidget(display, row, col++, 1, 1);
    row++;
  }

  col=0;
  display = new CYNumDisplay;
  display->setShowLabel(true);
  display->setSuffix("\t");
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "LEFT_DELTA_CURSOR" );
  display->setHideFlagName( "LEFT_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  mGrid->addWidget(display, row, col++, 1, 1);

  hBox = new QWidget;
  hBox->setContentsMargins(0,0,0,0);

  layout = new QHBoxLayout;
  layout->setContentsMargins(0,0,0,0);
  layout->setSpacing(1);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "LEFT_DELTA_CURSOR" );
  display->setHideFlagName( "LEFT_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  layout->addWidget(display);

  display = new CYNumDisplay;
  display->setAlignment(Qt::AlignLeft);
  display->setLocaleDB(mDB);
  display->setDataName( "RIGHT_DELTA_CURSOR" );
  display->setHideFlagName( "RIGHT_ENABLE" );
  display->setInverseHideFlag(true);
  display->setAlwaysOk(true);
  display->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  layout->addWidget(display);

  hBox->setLayout(layout);
  mGrid->addWidget(hBox, row, col++, 1, -1);
}
