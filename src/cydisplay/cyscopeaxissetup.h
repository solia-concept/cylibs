//
// C++ Interface: cyscopeaxissetup
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSCOPEAXISSETUP_H
#define CYSCOPEAXISSETUP_H

// CYLIBS
#include <cywidget.h>

class CYScopeAxis;

namespace Ui {
		class CYScopeAxisSetup;
}

/**
@short Widget de paramètrage d'un axe d'oscilloscope.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYScopeAxisSetup : public CYWidget
{
Q_OBJECT
public:
    CYScopeAxisSetup(QWidget *parent = 0, const QString &name = 0);

    ~CYScopeAxisSetup();
    /** Saisie le CYScopeAxis associé */
    void setScopeAxis(CYScopeAxis *axis);

protected:
    CYScopeAxis * mScopeAxis;

private:
    Ui::CYScopeAxisSetup *ui;
};

#endif
