/***************************************************************************
                          cymultimeter.cpp  -  description
                             -------------------
    déut                  : Mon Feb 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèues
                 utiles au déeloppement d'un superviseur CYLIX

 ***************************************************************************/

#include "cymultimeter.h"

// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "cyu8.h"
#include "cyf64.h"
#include "cyu32.h"
#include "cys16.h"
#include "cyflag.h"
#include "cystring.h"
#include "cycolor.h"
#include "cydial.h"
#include "cynumdialog.h"
#include "cydisplayitem.h"
#include "cydisplaystyle.h"
#include "cymultimetersetup.h"
//Added by qt3to4:
#include <QString>
#include <QResizeEvent>
#include <QMouseEvent>
#include <QMenu>

CYMultimeter::CYMultimeter(QWidget *parent, const QString &name, const QString &title, bool nf, bool ana, bool num)
  : CYDisplay(parent, name, title, nf)
{
  mType = Multimeter;
  mSetup = 0;

  mDB->setGroup(tr("Multimeter"));

  mTitle->setHelp(tr("Edit the data's label to change the title"));

  mShowAna = new CYFlag(mDB, "SHOW_ANA", new flg, false);
  mShowAna->setVal(ana);
  mShowAna->setLabel(tr("Analog display"));

  mShowLCD = new CYFlag(mDB, "SHOW_LCD", new flg, true);
  mShowLCD->setVal(num);
  mShowLCD->setLabel(tr("Digital display"));

  mNumBase = new CYU8(mDB, "NUM_BASE", new u8, Cy::BaseData);
  mNumBase->setLabel(tr("Numerical base"));
  mNumBase->addNote(tr("The numerical base is used only if the data is only an unsigned integer type!"));
  mNumBase->setChoiceLabel(Cy::BaseData, tr("Automatic"));
  mNumBase->setChoiceLabel(Cy::Binary, tr("Binary"));
  mNumBase->setChoiceLabel(Cy::Decimal, tr("Decimal"));
  mNumBase->setChoiceLabel(Cy::Hexadecimal, tr("Hexadecimal"));
  mNumBase->setChoiceLabel(Cy::Octal, tr("Octal"));

  mDB->setUnderGroup(tr("Alarm"));
  mDB->changeDataName( "LCD_ALARM_UPPER_BOUND", "ALARM_UPPER_BOUND" );
  mAlarmUpperBound = new CYF64(mDB, "ALARM_UPPER_BOUND" , new f64);
  mAlarmUpperBound->setLabel(tr("Upper bound value"));
//   mAlarmUpperBound->setNbDec(0);
  mDB->changeDataName( "LCD_ALARM_UPPER_ENABLE", "ALARM_UPPER_ENABLE" );
  mAlarmUpperEnable = new CYFlag(mDB, "ALARM_UPPER_ENABLE" , new flg);
  mAlarmUpperEnable->setLabel(tr("Upper alarm enable"));

  mDB->changeDataName( "LCD_ALARM_LOWER_BOUND", "ALARM_LOWER_BOUND" );
  mAlarmLowerBound = new CYF64(mDB, "ALARM_LOWER_BOUND" , new f64);
  mAlarmLowerBound->setLabel(tr("Lower bound value"));
//   mAlarmLowerBound->setNbDec(0);
  mDB->changeDataName( "LCD_ALARM_LOWER_ENABLE", "ALARM_LOWER_ENABLE" );
  mAlarmLowerEnable = new CYFlag(mDB, "ALARM_LOWER_ENABLE" , new flg);
  mAlarmLowerEnable->setLabel(tr("Lower alarm enable"));
  mDB->setUnderGroup(0);

  mDB->setUnderGroup(tr("Style")+":"+tr("Analog"));
  mAnaNormalColor = new CYColor(mDB, "ANA_NORMAL_COLOR", new QColor(Qt::green));
  mAnaNormalColor->setLabel(tr("Normal color"));
  mAnaAlarmColor = new CYColor(mDB, "ANA_ALARM_COLOR", new QColor(Qt::red));
  mAnaAlarmColor->setLabel(tr("Alarm color"));
  mAnaBackgroundColor = new CYColor(mDB, "ANA_BACKGROUND_COLOR", new QColor(Qt::white));
  mAnaBackgroundColor->setLabel(tr("Background color"));
  mAnaTextColor = new CYColor(mDB, "ANA_TEXT_COLOR", new QColor(Qt::black));
  mAnaTextColor->setLabel(tr("Text color"));
  mFontSize->setGroup(mDB->underGroup());
  mDB->setUnderGroup(0);

  mDB->setUnderGroup(tr("Style")+":"+tr("Digital"));
  mLCDNormalColor = new CYColor(mDB, "LCD_NORMAL_COLOR", new QColor(Qt::green));
  mLCDNormalColor->setLabel(tr("Normal color"));
  mLCDAlarmColor = new CYColor(mDB, "LCD_ALARM_COLOR", new QColor(Qt::red));
  mLCDAlarmColor->setLabel(tr("Alarm color"));
  mLCDBackgroundColor = new CYColor(mDB, "LCD_BACKGROUND_COLOR", new QColor(Qt::black));
  mLCDBackgroundColor->setLabel(tr("Background color"));
  mDB->setUnderGroup(0);

  if (!mFrame)
  {
    mAna = new CYDial(mDB, this, "meterANA");
    mLCD = new CYLCDNumber(this, "meterLCD");
  }
  else
  {
    mAna = new CYDial(mDB, mFrame, "meterANA");
    mLCD = new CYLCDNumber(mFrame, "meterLCD");
  }
  Q_CHECK_PTR(mLCD);
  Q_CHECK_PTR(mAna);

  registerPlotterWidget(mLCD);

  connect(mAna, SIGNAL(validate()), SLOT(validate()));

  QSizePolicy sz = QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  sz.setHeightForWidth(true);
  mAna->setSizePolicy(sz);
  mLCD->setSizePolicy(sz);

  mLCD->hide();
  mAna->hide();

  mLCD->setSegmentStyle(QLCDNumber::Filled);

  mLCD->setDigitColor( mLCDNormalColor->val() );
  mLCD->setBackgroundColor( mLCDBackgroundColor->val() );

  mBoxLayout->addWidget(mAna);
  mBoxLayout->addWidget(mLCD);

  loadSettings();
  updateView();

  setModified(false);

  startRefresh();
}

CYMultimeter::~CYMultimeter()
{
}

void CYMultimeter::setDataName(const QByteArray &name)
{
  mAna->setDataName(name);
  CYDisplay::addData(name);
}

QByteArray CYMultimeter::dataName() const
{
  return mAna->dataName();
}

void CYMultimeter::setFlagName(const QByteArray &name)
{
  mAna->setFlagName(name);
}

QByteArray CYMultimeter::flagName() const
{
  return mAna->flagName();
}

void CYMultimeter::setBenchType(const QByteArray &name)
{
  mAna->setBenchType(name);
}

QByteArray CYMultimeter::benchType() const
{
  return mAna->benchType();
}

void CYMultimeter::setInverseFlag(bool inverse)
{
  mAna->setInverseFlag(inverse);
}

bool CYMultimeter::inverseFlag() const
{
  return mAna->inverseFlag();
}

bool CYMultimeter::addData(CYData *data)
{
  return addData(data, data->label());
}

bool CYMultimeter::showAna()
{
  return mShowAna->val();
}

void CYMultimeter::setShowAna(bool val)
{
  mShowAna->setVal( val );
  updateView();
}

bool CYMultimeter::showLCD()
{
  return mShowLCD->val();
}

void CYMultimeter::setShowLCD(bool val)
{
  mShowLCD->setVal( val );
  updateView();
}

bool CYMultimeter::addData(CYData *data, QString label)
{
  nbItem = 0;
  while (!datas.isEmpty())
      delete datas.takeFirst();

  if (!CYDisplay::addData(data, label))
    return (false);
  updateFormat();
  return (true);
}

void CYMultimeter::refresh()
{
  if ( !mRefreshNotVisible && !isVisible() )
    return;

  if (!core)
    return;

  refreshStatus();

  if (core && !core->analyseRefreshEnabled())
    return;

  if (datas.isEmpty())
    return;

  CYDisplayItem *d = datas.first();

  if (!d)
    return;

  double val = d->val();

  // ANA
  mAna->setValue(d->data->toString(true, !core->simulation(), (Cy::NumBase)mNumBase->val()));

  // NUM
  QString txt = d->data->toString(false, !core->simulation(), (Cy::NumBase)mNumBase->val());
  txt = txt.replace(",", ".");
  mLCD->display(txt);

//  QLCDNumber::Mode mode;
//  Cy::NumBase base;
//  if (((Cy::NumBase)mNumBase->val())==Cy::BaseData)
//    base = d->data->numBase();
//  else
//    base = (Cy::NumBase)mNumBase->val();

//  switch (base)
//  {
//    case Cy::Binary     : mode = QLCDNumber::Bin; break;
//    case Cy::Decimal    : mode = QLCDNumber::Dec; break;
//    case Cy::Hexadecimal: mode = QLCDNumber::Hex; break;
//    case Cy::Octal      : mode = QLCDNumber::Oct; break;
//  default:
//    break;
//  }
//  mLCD->setMode(mode);
//  mLCD->display(d->data->valToDouble());

  if (mAlarmLowerEnable->val() && (val < mAlarmLowerBound->val()))
  {
    mAna->setColors( mAnaAlarmColor->val(), mAnaBackgroundColor->val(), mAnaTextColor->val() );
    mLCD->setDigitColor( mLCDAlarmColor->val() );
  }
  else if (mAlarmUpperEnable->val() && (val > mAlarmUpperBound->val()))
  {
    mAna->setColors( mAnaAlarmColor->val(), mAnaBackgroundColor->val(), mAnaTextColor->val() );
    mLCD->setDigitColor( mLCDAlarmColor->val() );
  }
  else
  {
    mAna->setColors( mAnaNormalColor->val(), mAnaBackgroundColor->val(), mAnaTextColor->val() );
    mLCD->setDigitColor( mLCDNormalColor->val() );
  }
}

void CYMultimeter::resizeEvent(QResizeEvent *e)
{
  CYDisplay::resizeEvent(e);
}

bool CYMultimeter::createFromDOM(QDomElement &element, QStringList *ErrorList)
{
    Q_UNUSED(ErrorList)
  if (!core)
    return false;
  mCreatingFromDOM = true;
  hide();

  if (compareVersionCYDOM("2.26")<0) // COMPATIBILITÉ ASCENDANTE < 2.26
  {
    mAlarmLowerBound->setVal( element.attribute("lowlimit", QString("%1").arg(mAlarmLowerBound->def())).toDouble() );
    mAlarmLowerEnable->setVal( element.attribute("lowlimitactive", QString("%1").arg(mAlarmLowerEnable->def())).toInt() );
    mAlarmUpperBound->setVal( element.attribute("uplimit", QString("%1").arg(mAlarmUpperBound->def())).toDouble() );
    mAlarmUpperEnable->setVal( element.attribute("uplimitactive", QString("%1").arg(mAlarmUpperEnable->def())).toInt() );

    mLCDNormalColor->setVal(restoreColorFromDOM(element, "normalDigitColor", mLCDNormalColor->def()));
    mLCDAlarmColor->setVal(restoreColorFromDOM(element, "alarmDigitColor", mLCDAlarmColor->def()));
    mLCDBackgroundColor->setVal(restoreColorFromDOM(element, "backgroundColor", mLCDBackgroundColor->def()));

    mFontSize->setVal( element.attribute( "fontSize", QString(mFontSize->def()) ).toInt() );
  }
  if ((compareVersionCYDOM("2.27")<0)) // COMPATIBILITÉ ASCENDANTE < 2.27
  {
    mShowAna->setVal( (bool)element.attribute("showAna", QString("%1").arg(mShowAna->def())).toInt() );
    mShowLCD->setVal( (bool)element.attribute("showLCD", QString("%1").arg(mShowLCD->def())).toInt() );
  }
  internCreateFromDOM(element);
  QString title = mTitle->val();

  if (!CYDisplay::addData(element.attribute("dataName"), element.attribute("label")))
    qDebug("bool CYMultimeter::createFromDOM(QDomElement &element)");

  internCreateFromDOM(element); // Récupération des échelles

  if (compareVersionCYDOM("2.26")<0 && !title.isEmpty()) // COMPATIBILITÉ ASCENDANTE < 2.26
  {
    setTitle(title);
  }

  mAna->setFontSizeLabel( mFontSize->val() );

  updateView();
  show();

  setModified(false);
  mCreatingFromDOM = false;
  return (true);
}

bool CYMultimeter::addToDOM(QDomDocument &doc, QDomElement &element, bool save)
{
  QDomElement db = doc.createElement("DataBase");
  element.appendChild(db);

  if (!datas.isEmpty() && datas.at(0))
  {
    element.setAttribute("dataName", datas.at(0)->objectName());
    if ( datas.at(0)->data->label() != datas.at(0)->label )
      element.setAttribute("label", datas.at(0)->label );
  }

  internAddToDOM(doc, element);

  if (save)
    setModified(false);

  return (true);
}

void CYMultimeter::settings()
{
  if (!mSetup)
  {
    mSetup = new CYMultimeterSetup(this, "CYMultimeterSetup");
    Q_CHECK_PTR(mSetup);
  }

  if ( !mUpdatingFormat )
    mSetup->exec();
}

void CYMultimeter::applySettings()
{
  updateView();

  repaint();

  CYDisplay::applySettings();
}

void CYMultimeter::updateView()
{
  int h1 = 0;
  int w1 = 0;
  int h2 = 0;
  int w2 = 0;

  mAna->hide();
  mLCD->hide();

  if (!mFrame)
    setMinimumSize(2*layout()->margin(), 3*layout()->margin());
  else
    setMinimumSize(2*mFrame->layout()->margin(), 3*mFrame->layout()->margin());

  if (!mShowAna->val())
  {
    mAna->removeEventFilter(this);
  }
  else
  {
    mAna->installEventFilter(this);
    mAna->show();
    h1 = mAna->minimumHeight();
    w1 = mAna->minimumWidth();
  }

  if (!mShowLCD->val())
  {
    mLCD->removeEventFilter(this);
  }
  else
  {
    mLCD->installEventFilter(this);
    mLCD->show();
    h2 = mLCD->minimumHeight();
    w2 = mLCD->minimumWidth();
  }

  mAna->setFontSizeLabel( mFontSize->val() );
  mAna->updateScale();

  setMinimumSize(minimumWidth() + qMax(w1, w2), minimumHeight() + h1 + h2);

  if (mFrame)
    mFrame->setMinimumSize(minimumSize());

  mAna->setColors( mAnaNormalColor->val(), mAnaBackgroundColor->val(), mAnaTextColor->val() );
  mLCD->setBackgroundColor( mLCDBackgroundColor->val() );

  updateGeometry();
}

int CYMultimeter::showPopupMenu(QMenu *pm0, int index, bool )
{
  QMenu *pm;
  if (pm0)
  {
    if (index==0)                                     // Crétion d'un sous-menu surgissant
    {
      pm = pm0->addMenu(tr("Multimeter")); index++;
    }
    else                                              // Ajout d'entrées dans un menu surgissant
      pm = pm0;
  }
  else                                                // Crétion d'un menu surgissant
  {
    pm = new QMenu(tr("Multimeter")); index++;
    pm->addSeparator();
  }

  index = CYDisplay::showPopupMenu(pm, index);
  index = CYDisplay::showPopupMenu(pm, index, true);

  if (!pm0)
    pm->exec(QCursor::pos());

  return index;
}


/*! Mise à jour du format
    \fn CYMultimeter::updateFormat()
 */
void CYMultimeter::updateFormat()
{
  mUpdatingFormat = true;

  if (datas.isEmpty())
    return;

  CYDisplayItem *d = datas.first();

  if (!d)
    return;

  CYU32 *data = (CYU32 *)d->data;

  mUnit = data->unit();

  int nbDigit = data->nbDigit((Cy::NumBase)mNumBase->val());
  if (data->nbDec()>0)
    nbDigit++;

  if ( mTitleAuto->val() )
    setTitle( d->label );
  else
    setTitle(mTitle->val());

  mLCD->setDigitCount(nbDigit);

  mAlarmLowerBound->setFormat( data->format() );
  mAlarmUpperBound->setFormat( data->format() );

  mAna->setData(data);
//   mAna->linkData();
//   mAna->localeDB()->designer();
//   updateView();
  this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());

  mUpdatingFormat = false;
}

void CYMultimeter::mouseDoubleClickEvent ( QMouseEvent * e )
{
  if (!core->simulation())
    return CYDisplay::mouseDoubleClickEvent ( e );

  if (datas.isEmpty())
    return;

  CYDisplayItem *d = datas.first();
  if (!d)
    return;

  CYNumDialog *dlg = new CYNumDialog(this);
  dlg->setDataName(d->objectName().toUtf8());
  dlg->exec();
}
