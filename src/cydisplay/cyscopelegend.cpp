#include "cyscopelegend.h"

// QT
#include <QLabel>
#include <QLayout>
#include <QKeyEvent>
#include <QString>
#include <QMenu>
#include <QMouseEvent>
#include <QShowEvent>
#include <QHBoxLayout>
// QWT
#include "qwt_plot_dict.h"
#include "qwt_plot_canvas.h"
#include "qwt_scale_div.h"
#include "qwt_plot_layout.h"
#include "qwt_scale_draw.h"
#include "qwt_plot_grid.h"
#include "qwt_legend.h"
#include "qwt_text_label.h"
#include "qwt_legend_label.h"
// CYLIBS
#include "cycore.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cys16.h"
#include "cystring.h"
#include "cyframe.h"
#include "cyscopeaxis.h"
#include "cyscopesignal.h"
#include "cynumdisplay.h"
#include "cypixmapview.h"
#include "cytextlabel.h"
#include "cyscopesetup.h"
#include "cydataslistview.h"
#include "cydataseditlist.h"
#include "cyscopeplotter.h"
#include "cyscopesignallegend.h"
#include "cyplotcurve.h"

CYScopeLegend::CYScopeLegend(CYScopePlotter *parent) :
  QwtLegend(parent),
  mPlot(parent),
  mDB(parent->db())
{
}

/*!
  \brief Create a widget to be inserted into the legend

  The default implementation returns a QwtLegendLabel.

  \param data Attributes of the legend entry
  \return Widget representing data on the legend

  \note updateWidget() will called soon after createWidget()
        with the same attributes.
  \sa QwtLegend
 */
QWidget *CYScopeLegend::createWidget( const QwtLegendData &data ) const
{
  int curveKey=0;
  const QVariant key=data.value(CYPlotCurve::KeyRole);
  if ( key.canConvert<int>() )
  {
    curveKey = key.value<int>();
  }
  if (curveKey<1)
    return QwtLegend::createWidget( data );

  if (mPlot->ySignal.isEmpty() || (curveKey>mPlot->ySignal.count()))
    return QwtLegend::createWidget( data );

  CYScopeSignal *signal = mPlot->ySignal.at(curveKey-1);
  if (!signal)
  {
    CYWARNINGTEXT(QString("curveKey=%1").arg(curveKey));
    return QwtLegend::createWidget( data );
  }

  CYScopeSignalLegend *widget = new CYScopeSignalLegend(this, signal);
  return widget;
}
