#ifndef CYDISPLAYTIMERSETUP_H
#define CYDISPLAYTIMERSETUP_H

#include <cydialog.h>

namespace Ui {
    class CYDisplayTimerSetup;
}

class CYDisplayTimerSetup : public CYDialog
{
public:
  CYDisplayTimerSetup(QWidget *parent=0, const QString &name="displayTimerSetup");
  ~CYDisplayTimerSetup();

private:
    Ui::CYDisplayTimerSetup *ui;
};

#endif // CYDISPLAYTIMERSETUP_H
