/***************************************************************************
                          cydatastablerow.cpp  -  description
                             -------------------
    begin                : mer nov 24 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydatastablerow.h"

// CYLIBS
#include "cydatastable.h"
#include "cydatastablecol.h"
#include "cydatastabledisplaycell.h"
#include "cyf64.h"

CYDatasTableRow::CYDatasTableRow(QObject *parent, CYData *d, int i, int pos, bool shown, int height, QString l)
  : CYDisplayItem(parent, d, i, l),
    mPos(pos),
    mShown(shown),
    mHeight(height)
{
  connect( this, SIGNAL( formatUpdated()), table(), SLOT( updateFormat() ) );
}

CYDatasTableRow::~CYDatasTableRow()
{
  mDisplayCell.clear();
}

int CYDatasTableRow::pos()
{
  return table()->visualRow(id);
}

void CYDatasTableRow::addDisplayCell(CYDatasTableDisplayCell *cell)
{
  if (pos() != cell->tableRow()->pos())
    CYFATAL;

  if (mDisplayCell.indexOf(cell)==-1)
  {
    QListIterator<CYDatasTableDisplayCell *> it(mDisplayCell);
    while (it.hasNext())
    {
      CYDatasTableDisplayCell *c = it.next();
      if (c->tableCol()->id == cell->tableCol()->id)
      {
        CYWARNING; // il existe déja une cellule dans la même colonne
        return;
      }
    }
    mDisplayCell.append(cell);
  }
  else
    CYWARNING;
}

CYDatasTableDisplayCell *CYDatasTableRow::displayCell(int col)
{
  QListIterator<CYDatasTableDisplayCell *> it(mDisplayCell);
  while (it.hasNext())
  {
    CYDatasTableDisplayCell *c = it.next();
    if (c->tableCol()->id == col)
    {
      return c;
    }
  }
  return 0;
}

void CYDatasTableRow::createDisplaysFromDOM(QDomElement &element)
{
  QDomNodeList list = element.elementsByTagName("display");
  for (int i=0; i<list.count(); i++)
  {
    QDomElement el = list.item(i).toElement();
    int col = el.attribute("col").toInt();
    if (table()->compareVersionCYDOM("5.05")<0)
      col++; // décalage de la colonne id innnexistante avant la version 5.05

    CYDatasTableDisplayCell *wdg = displayCell(col);
    if (wdg)
      wdg->createDisplayFromDOM(el);
  }
}

void CYDatasTableRow::addDisplaysToDOM(QDomDocument &doc, QDomElement &element, bool save)
{
  QListIterator<CYDatasTableDisplayCell *> it(mDisplayCell);
  it.toFront();
  while (it.hasNext())
  {
    CYDatasTableDisplayCell *c = it.next();
    c->addDisplayToDOM(doc, element, save);
  }
}

void CYDatasTableRow::startRefresh()
{
  QListIterator<CYDatasTableDisplayCell *> it(mDisplayCell);
  while (it.hasNext())
  {
    CYDatasTableDisplayCell *c = it.next();
    c->startRefresh();
  }
}

void CYDatasTableRow::stopRefresh()
{
  QListIterator<CYDatasTableDisplayCell *> it(mDisplayCell);
  while (it.hasNext())
  {
    CYDatasTableDisplayCell *c = it.next();
    c->stopRefresh();
  }
}


/*!
    \fn CYDatasTableRow::changeForcingCell()
 */
void CYDatasTableRow::changeForcingCell()
{
  emit changingForcingCell(this);
}


/*! @return la table parent.
    \fn CYDatasTableRow::table()
 */
CYDatasTable * CYDatasTableRow::table()
{
  return (CYDatasTable *)parent();
}
