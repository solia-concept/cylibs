//
// C++ Interface: cyscopeacquisition
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSCOPEACQUISITION_H
#define CYSCOPEACQUISITION_H


// CYLIBS
#include "cyacquisition.h"

class CYScope;
class CYScopeCapture;

/**
@short Acquisition d'une capture d'oscilloscope.

@author Gérald LE CLEACH
*/
class CYScopeAcquisition : public CYAcquisition
{
Q_OBJECT
public:
    CYScopeAcquisition(CYScope *s, const QString &name = 0, CYFlag *enable=0);

    ~CYScopeAcquisition();

    /** @return le nom forcé du fichier. Si ce nom est un champ vide alors le nom du fichier est automatique. */
    QString forceFileName() { return mForceFileName; }

public slots:
    virtual void capture();
    virtual bool saveSetup();
    virtual void header(QFile *file, QDateTime startTime, QList<CYAcquisitionData*> headerList, QList<CYAcquisitionData*> acquisitionList, int poste);

    /** Cherche le fichier courrant d'acquisition. */
    virtual void currentFile();
    /**  Saisie du nom forcé du fichier. Dans ce cas le nom du fichier n'est plus automatique.
     *  Pour remettre à nouveau un nom automatique, il suffit de saisir un champ vide. */
    void setForceFileName(const QString fileName) { mForceFileName = fileName; }

    virtual void exportCurves(bool saveAs=true);
    virtual void exportCurves(bool saveAs, bool newCapture);
protected:
    CYScope * mScope;
    CYScopeCapture * mScopeCapture;

    /**  Par défaut vaut à \a true pour avoir la boîte de sélection du fichier, pour avoir automatiquement un nom de fichier il suffit de le mettre à \a false. */
    bool mSaveAs;
    /**  Saisie le nom forcé du fichier. */
    QString mForceFileName;

protected:
    virtual void init();
};

#endif
