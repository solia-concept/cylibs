/***************************************************************************
                          cydisplaydummy.h  -  description
                             -------------------
    début                  : Mon Feb 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDISPLAYDUMMY_H
#define CYDISPLAYDUMMY_H

// CYLIBS
#include "cydisplay.h"
//Added by qt3to4:
#include <QResizeEvent>
#include <QMenu>

/** CYDisplayDummy représente un afficheur "vierge". Ce type d'afficheur est utile
  * lors de la manipulation dynamique d'afficheurs. Celui-ci réserve un emplacement
  * afin que l'utilisateur puisse y insérer à tout moment un véritable afficheur.
  * @short Afficheur "vierge".
  * @author Gérald LE CLEACH.
  */

class CYDisplayDummy : public CYDisplay
{
  Q_OBJECT

public:
  /** Construit un afficheur "vierge".
    * @param parent  Widget parent.
    * @param name    Nom de l'oscilloscope. */
  CYDisplayDummy(QWidget *parent=0, const QString &name=0, const QString &title=0);
  ~CYDisplayDummy();

  /** Redimensionne l'afficheur. */
  void resizeEvent(QResizeEvent *);

  virtual bool createFromDOM(QDomElement &element, QStringList *ErrorList=nullptr);
  virtual bool addToDOM(QDomDocument &doc, QDomElement &element, bool save=true);

  /** Gestion du menu surgissant.
    * @param pm0    Menu appelant s'il existe.
    * @param index  Index de l'entrée du menu appelant.
    * @param end    Fin du menu. */
  virtual int showPopupMenu(QMenu *pm0=0, int index=0, bool end=false);
};

#endif
