/***************************************************************************
                          cydatastableplotter.h  -  description
                             -------------------
    begin                : jeu nov 18 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDATASTABLEPLOTTER_H
#define CYDATASTABLEPLOTTER_H

// QT
#include <QTableWidget>
#include <QDomElement>

class CYData;
class CYDatasTable;
class CYDatasTableCol;
class CYDatasTableRow;

/** @short Traceur de table de données.
  * @author LE CLÉACH Gérald
  */

class CYDatasTablePlotter : public QTableWidget
{
  Q_OBJECT
public:
  CYDatasTablePlotter(QWidget *parent=0, const QString &name=0, CYDatasTable *display=0);
  ~CYDatasTablePlotter();

  /** Ajoute un colonne \a col.
    * @param cydisplay Indique si les cellules de la colonne sont destinées à des afficheurs (CYDisplay).*/
  void addColumn(CYDatasTableCol *dtc, bool cydisplay=true);
  /** Ajoute un ligne de donnée \a row. */
  void addRow(CYDatasTableRow *dtr, bool loading);

  /** Affiche / cache les colonnes. */
  virtual void showHideColumns();
  
  /** @return la table CYDisplay. */
  CYDatasTable *table() { return mDisplay; }
  
public slots: // Public slots
  /** @return le widget courant. */
  QWidget * currentWidget();
  /** Crée la cellule de forçage. */
  void createForcingCell(CYDatasTableRow *dtr, int col);
  /** Change la cellule de forçage. */
  void changeForcingCell(CYDatasTableRow *dtr);

signals: // Signals
  /** Demande l'affichage du menu surgissant */
  void showPopupMenu();
  /** Demande l'initialisation des valeurs de forçage. */
  void initForcingValues();

protected slots: // Protected slots
  /** Fonction appelée à chaque changement de largeur de colonne. */
  virtual void columnWidthChanged ( int col );
  /** Fonction appelée à chaque changement d'hauteur de ligne. */
  virtual void rowHeightChanged ( int row );

protected: // Protected methods
  /** Annule l'activation de la cellule suivante de QT car INSTABLE. */
  virtual void activateNextCell();
  /** Gestion d'une pression sur un bouton de souris. */
  virtual void contentsMousePressEvent ( QMouseEvent * e );
  /** Fonction appelée à l'affichage du widget. */
  virtual void showEvent(QShowEvent *e);
//   /** Gestion de la molette. */
//   virtual void wheelEvent(QWheelEvent * e);

protected: // Protected attributes
  /** Afficheur CYDisplay */
  CYDatasTable *mDisplay;
  /** Vaut \a true lorsqu'un changement de largeur sur plusieurs colonnes est en cours. */
  bool mColWidthChanging;
  /** Vaut \a true lorsqu'un changement de hauteur sur plusieurs lignes est en cours. */
  bool mRowHeightChanging;
};

#endif
