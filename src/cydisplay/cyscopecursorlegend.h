//
// C++ Interface: cyscopecursorlegend
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSCOPECURSORLEGEND_H
#define CYSCOPECURSORLEGEND_H

// CYLIBS
#include "cyscopecapture.h"

class CYScopeCaptureLegend;

/**
@short Légende de curseur d'oscilloscope.

@author Gérald LE CLEACH
*/
class CYScopeCursorLegend : public CYFrame
{
Q_OBJECT
public:
    CYScopeCursorLegend(const CYScopeCaptureLegend *legend, CYScopeSignal *signal);

    ~CYScopeCursorLegend();
    virtual void drawIdentifier(QPainter *, const QRect &) const;

protected:
    virtual void initYfxDisplay(CYData *data);

protected:
    const CYScopeCaptureLegend *mLegend;
    CYScopeSignal * mSignal;
    CYScopeCapture * mCapture;
    QVBoxLayout *mVBL;
};


#endif
