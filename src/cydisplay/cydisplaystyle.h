/***************************************************************************
                          cydisplaystyle.h  -  description
                             -------------------
    début                  : Mon Feb 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDISPLAYSTYLE_H
#define CYDISPLAYSTYLE_H

// QT
#include <QColor>
#include <QList>
#include <QPushButton>
#include <QSpinBox>
#include <QImage>
#include <QListWidget>
#include <QSettings>

class CYDisplayStyleSetup;

/**
 * Style d'un afficheur CYDisplay.
 * @short Style d'un afficheur.
 */
 
class CYDisplayStyle : public QObject
{
  Q_OBJECT
  friend class CYDisplayStyleSetup;

public:
  CYDisplayStyle(QObject *parent=0, const QString &name=0);
  ~CYDisplayStyle();

  void readProperties(QSettings *cfg);
  void saveProperties(QSettings *cfg);

  const QColor &getFgColor1() const
  {
    return fgColor1;
  }
  const QColor &getFgColor2() const
  {
    return fgColor2;
  }
  const QColor &getAlarmColor() const
  {
    return alarmColor;
  }
  const QColor &getBackgroundColor() const
  {
    return backgroundColor;
  }
  const QColor &getGridColor() const
  {
    return gridColor;
  }
  uint getFontSize() const
  {
    return fontSize;
  }
  const QColor &getDataColor(int i)
  {
    static QColor dummy;
    if (i < dataColors.count())
      return *(dataColors.at(i));
    else
      return dummy;
  }
  uint getDataColorCount() const
  {
    return dataColors.count();
  }

public slots:
  void configure(QWidget *parent=0);
  void editColor();

signals:
  void applyStyleToAnalyseSheet();

private:
  void apply();

  CYDisplayStyleSetup *setup;

  QColor fgColor1;
  QColor fgColor2;
  QColor alarmColor;
  QColor backgroundColor;
  QColor gridColor;
  uint fontSize;
  QList<QColor*> dataColors;
};

#endif
