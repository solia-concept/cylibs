#ifndef CYSCOPESCALEDIALOG_H
#define CYSCOPESCALEDIALOG_H

// CYLIBS
#include <cydialog.h>

class CYScopeAxis;

namespace Ui {
    class CYScopeScaleDialog;
}

class CYScopeScaleDialog : public CYDialog
{
public:
  CYScopeScaleDialog(QWidget *parent=0, const QString &name="scopeScale");
  ~CYScopeScaleDialog();

  void setScopeAxis(CYScopeAxis *axis);

private:
    Ui::CYScopeScaleDialog *ui;
};

#endif // CYScopeScaleDialog_H
