//
// C++ Interface: cyscopeaxis
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSCOPEAXIS_H
#define CYSCOPEAXIS_H

// QT
#include <QObject>
#include <QMouseEvent>
#include <QList>
// QWT
#include "qwt_plot.h"
// CYLIBS
#include "cyscopecapture.h"
#include "cyscopesignal.h"

class CYF64;
class CYFlag;
class CYFormat;

/**
@short Objet lié à un axe d'oscilloscope.
Cet objet regroupe plusieurs données relatives à un axe d'oscilloscope.

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYScopeAxis : public QObject
{
Q_OBJECT
public:
    CYScopeAxis(CYScopePlotter *parent, QwtPlot::Axis axis);

    ~CYScopeAxis();
    void initDB();
    void copy(CYScopePlotter *s);

    /** Fonction d'initialisation de l'échelle des temps. */
    void initTimeScale(uint nb, float step, int burstsRatio=1);

    /** @return la donnée mini de l'axe. */
    CYF64 *minData();
    /** @return la donnée maxi de l'axe. */
    CYF64 *maxData();

    /** @return la donnée gauche de l'axe. */
    CYF64 *leftData() { return mLeft; }
    /** @return la donnée droite de l'axe. */
    CYF64 *rightData() { return mRight; }

    /** @return la valeur mini de l'axe. */
    double minVal();
    /** Saisie la valeur mini de l'axe. */
    void setMinVal(double val);
    /** @return la valeur maxi de l'axe. */
    double maxVal();
    /** Saisie la valeur maxi de l'axe. */
    void setMaxVal(double val);

    /** @return le nombre de décimales de l'axe. */
    int nbDec() { return mNbDec; }
    /** Saisie le nombre de décimales de l'axe. */
    void setNbDec( int val ) { mNbDec = val; }

    /** Saisie le titre de l'axe. */
    void setTitle(const QString &);
    /** @return le titre. */
    QString title() const { return mTitle; }

    /** Saisie l'unité de l'axe. */
    void setUnit(const QString &text) { mUnit = text; }
    /** @return l'unité. */
    QString unit() const { return mUnit; }

    /** Construit l'échelle temporelle à partir des bornes \a left et \a right. */
    void changeTimeScale(double left, double right);
    /** Construit l'échelle temporelle */
    void changeTimeScale();
    /** Construit l'échelle des y à partir des bornes \a left et \a right. */
    void changeXScale(double left, double right);
    /** Construit l'échelle des x de l'axe en mode XY. */
    void changeXScale();
    /** Construit l'échelle des y à partir des bornes \a min et \a max. */
    void changeYScale(double min, double max);
    /** Construit l'échelle des y. */
    void changeYScale();

    void initX(CYData *data);
    void initY(CYData *data);

    /** @return le nom des connexions réseau rattachées à cet axe. */
    QString linksName();
    /** @return le numéro du format utilisé. */
    int numFormat() { return mNumFormat; }
    /** @return le format utilisé. */
    CYFormat *format();
    /**
     * @brief compatibleFormat Vérifie la compatibilité d'un format avec celui de l'axe.
     * @param format Format à vérifier
     * @return \a true si le format est compatible
     */
    bool compatibleFormat(CYFormat *format);

    /** @return le type de l'axe. */
    QwtPlot::Axis type() { return mType; }
    /** @return \a true si et seulement si l'axe est de type \a QwtPlot::xBottom ou \a QwtPlot::xTope. */
    bool isX();
    /** @return \a true si et seulement si l'axe est de type \a QwtPlot::yLeft ou \a QwtPlot::yRight. */
    bool isY();
    /** @return \a true si et seulement si l'axe est un axe de temps */
    bool isTimeScale();

    void plotMousePressed(const QMouseEvent &e);
    void plotMouseReleased(const QMouseEvent &e);
    bool enableZoomOut();
    void backupScaleValues();

    void initCaptureDatas();

    bool isEnabled();
    /** @return le nombre de pas de l'échelle de l'axe. */
    int nbStep();
    /** Saisie le nombre de pas de l'échelle de l'axe. */
    void setNbStep( int nb );
    /** @return le pas de l'échelle de l'axe. */
    double step();
    /** Saisie le pas de l'échelle de l'axe. */
    void setStep( double val );

    /** @return la donnée contenant la valeur du curseur. */
    CYF64 *cursor(CYScopeCapture::Cursor cursor);
    /** @return la donnée contenant la valeur du curseur 1. */
    CYF64 *cursor1();
    /** @return la donnée contenant la valeur du curseur 2. */
    CYF64 *cursor2();
    /** @return la donnée contenant le delta des valeurs des curseurs. */
    CYF64 *cursorD();
    /** @return la donnée contenant le delta inverse des valeurs des curseurs. */
    CYF64 *cursorID();

    /** Transforme les coordonnées graphiques en valeurs de curseurs. */
    void transformPosToVal();
    /** Transforme les valeurs des curseurs en coordonnées graphiques. */
    void transformValToPos();
    void calculDelta();
    void init();
    /** @return la description de l'axe: Left, Right, Bottom ou Top */
    QString desc();

    /** @return le traceur auquel appartient cet axe. */
    CYScopePlotter * plotter() { return mPlotter; }

public slots:
    void setFontSize( int val );
    void zoomIn(double coef);
    void zoomOut(double coef);
    void setupScale();
    void updateOptions();
    void setLog(bool val);
    void setAutoScale(bool val);
    void setShow(bool val);

signals: // Signals
    void enableActionZoomIn(bool);
    void enableActionZoomOut(bool);
    void limitsChanged();

public:
    /** Liste des signaux. */
    QList<CYScopeSignal*> signal;

    /** Tableau des valeurs temporelles en x. */
    QVector<double> xTime;

protected:
    QwtPlot::Axis mType;
    QString mDesc;
    CYScopePlotter *mPlotter;

    CYF64 *mMax, *mMin, *mLeft, *mRight, *mTime;
    CYFlag *mAutoScale, *mAutoScaleHide, *mShow, *mLog, *mEnable;

    bool mR2L;

    bool mInit;
    /** Titre de l'axe. */
    QString mTitle;
    /** Unité de l'axe. */
    QString mUnit;
    /** Nombre de décimales de l'axe. */
    int mNbDec;

    QString mLinksName;
    int mNumFormat;

    double mZoomVal1;
    double mZoomVal2;

    CYScopeCapture * mScopeCapture;
    QHash<int, CYF64*> mCursors;

    double mStep;
    int    mNbStep;

    bool mInitX, mInitY;
    bool mIsTimeScale;
};

#endif
