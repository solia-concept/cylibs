/***************************************************************************
                          cybargraph.cpp  -  description
                             -------------------
    début                  : Mon Feb 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cybargraph.h"

// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "cys16.h"
#include "cyf64.h"
#include "cycolor.h"
#include "cystring.h"
#include "cydisplayitem.h"
#include "cydisplaystyle.h"
#include "cybargraphsetup.h"
#include "cybargraphsignal.h"
#include "cybargraphplotter.h"
#include "cydataslistview.h"
#include "cydataseditlist.h"
#include "cynumdialog.h"
#include "cymessagebox.h"
//Added by qt3to4:
#include <QString>
#include <QResizeEvent>
#include <QMouseEvent>
#include <QMenu>

CYBargraph::CYBargraph(QWidget *parent, const QString &name, const QString &title, bool nf)
  : CYDisplay(parent, name, title, nf)
{
  mType = Bargraph;
  bars = 0;
  flags = 0;
  mSetup = 0;

  if (!mFrame)
  {
    mPlot = new CYBargraphPlotter(this, this, "CYBargraphPlotter");
  }
  else
  {
    mPlot = new CYBargraphPlotter(this, mFrame, "CYBargraphPlotter");
  }
  Q_CHECK_PTR(mPlot);

  mBoxLayout->addWidget(mPlot);

  setMinimumSize(sizeHint());

  /* Tous les clics RMB dans le widget mPlot seront gérés par CYDisplay::eventFilter. */
  mPlot->installEventFilter(this);/**/

  registerPlotterWidget(mPlot);

  loadSettings();
  setModified(false);

  adjustSize();

  startRefresh();
}

CYBargraph::~CYBargraph()
{
}

bool CYBargraph::addData(CYData *data, QString label)
{
  if (!data)
    qDebug("bool CYBargraph::addData(CYData *data)");

  if (!data->isNum())
  {
    CYMessageBox::sorry(this, QString(tr("This display cannot treat this type of data !")));
    return (false);
  }

  if (label.isEmpty())
    label = data->label();

  foreach (CYDisplayItem *d, datas)
  {
    if ( d->data->objectName() == data->objectName() )
    {
      if (isVisible())
        CYMessageBox::sorry(this, tr("This data is already displayed in this bargraph."));
      return (false);
    }
  }
  CYBargraphSignal* signal = mPlot->addBar(data, ++nbItem, label);

  if (!signal)
    return (false);

  registerItem(signal);

  ++bars;

  QString toolTip;
  for (uint i = 0; i < bars; ++i)
  {
    toolTip   += "<nobr>"+QString("<i>%1</i>").arg(datas.at(i)->data->displayHelp(false, true))+datas.at(i)->data->infoCY()+"</nobr><br>";
  }
  mPlot->setToolTip(toolTip);

  return (true);
}

bool CYBargraph::removeData(uint idx)
{
  if (idx >= bars)
    CYFATAL

  mPlot->deleteBar(idx);
  bars--;
  CYDisplay::removeData(idx);

  QString tooltip;
  for (uint i = 0; i < bars; ++i)
  {
    if (i == 0)
      tooltip += QString("%1: %2 -> %3").arg(datas.at(i)->host).arg(datas.at(i)->link).arg(datas.at(i)->objectName());
    else
      tooltip += QString("\n%1: %2 -> %3").arg(datas.at(i)->host).arg(datas.at(i)->link).arg(datas.at(i)->objectName());
  }
  mPlot->setToolTip(tooltip);

  return (true);
}

void CYBargraph::refresh()
{
  if ( !mRefreshNotVisible && !isVisible() )
    return;

  if (!core)
    return;

  refreshStatus();

  if (core && !core->analyseRefreshEnabled())
    return;

  mPlot->repaint();
}

void CYBargraph::settings()
{
  if (!mSetup)
  {
    mSetup = new CYBargraphSetup(this, "CYBargraphSetup");
    Q_CHECK_PTR(mSetup);
  }

  if ( !mUpdatingFormat )
    mSetup->exec();
}

void CYBargraph::applySettings()
{
  if (mFrame)
  {
    mFrame->updateGeometry();
  }
  CYDisplay::applySettings();
}

void CYBargraph::resizeEvent(QResizeEvent *)
{
  if (!mFrame)
    mPlot->setGeometry(0, 0, width(), height());
  else
    mFrame->setGeometry(0, 0, width(), height());
}

QSize CYBargraph::sizeHint(void)
{
  if (!mFrame)
    return (mPlot->sizeHint());
  else
    return (mFrame->sizeHint());
}

bool CYBargraph::createFromDOM(QDomElement &element, QStringList *ErrorList)
{
    Q_UNUSED(ErrorList)
  if (!core)
    return false;

  mCreatingFromDOM = true;
  removeDatas();

  if (compareVersionCYDOM("2.26")<0) // COMPATIBILITÉ ASCENDANTE < 2.26
  {
    mPlot->changeRange( element.attribute("min", "0").toDouble(), element.attribute("max", "0").toDouble() );
    mPlot->setLimits( element.attribute("lowlimit", "0").toDouble(), element.attribute("lowlimitactive", "0").toInt(), element.attribute("uplimit" , "0").toDouble(), element.attribute("uplimitactive" , "0").toInt() );
    mPlot->setColors( restoreColorFromDOM(element, "normalColor", mPlot->mNormalColor->def()), restoreColorFromDOM(element, "alarmColor", mPlot->mAlarmColor->def()), restoreColorFromDOM(element, "backgroundColor", mPlot->mBackgroundColor->def()) );

    mFontSize->setVal( element.attribute( "fontSize", QString(mFontSize->def()) ).toInt() );
  }


  QDomNodeList dnList = element.elementsByTagName("beam");
  for (int i = 0; i < dnList.count(); ++i)
  {
    QDomElement el = dnList.item(i).toElement();
    CYData *d = core->db.value(el.attribute("dataName"));
    if (!d)
    {
      qDebug("bool CYBargraph::createFromDOM(QDomElement &element)");
      mCreatingFromDOM = false;
      return false;
    }
    else
      addData(d, el.attribute("dataLabel"));
  }

  internCreateFromDOM(element);
  setModified(false);
  mCreatingFromDOM = false;
  return (true);
}

bool CYBargraph::addToDOM(QDomDocument &doc, QDomElement &element, bool save)
{
  for (uint i = 0; i < bars; ++i)
  {
    QDomElement beam = doc.createElement("beam");
    element.appendChild(beam);
    beam.setAttribute("dataName", datas.at(i)->objectName());
    if (datas.at(i)->label!=datas.at(i)->data->label())
      beam.setAttribute("dataLabel", datas.at(i)->label);
  }

  internAddToDOM(doc, element);

  if (save)
    setModified(false);

  return (true);
}

int CYBargraph::showPopupMenu(QMenu *pm0, int index, bool end)
{
  Q_UNUSED(end)
  QMenu *pm;
  if (pm0)
  {
    if (index==0)                                     // Création d'un sous-menu surgissant
    {
      pm = pm0->addMenu(tr("Bargraph")); index++;
    }
    else                                              // Ajout d'entrées dans un menu surgissant
      pm = pm0;
  }
  else                                                // Création d'un menu surgissant
  {
    pm = new QMenu(tr("Bargraph"), this); index++;
    pm->addSeparator();
  }

  index = CYDisplay::showPopupMenu(pm, index);
  index = CYDisplay::showPopupMenu(pm, index, true);


  if (!pm0)
    pm->exec(QCursor::pos());

  return index;
}


/*! Supprime toutes les données
    \fn CYBargraph::removeDatas()
 */
void CYBargraph::removeDatas()
{
  while (!datas.isEmpty())
    removeData(bars-1);
}


/*! Mise à jour du format
    \fn CYBargraphs::updateFormat()
 */
void CYBargraph::updateFormat()
{
  mUpdatingFormat = true;
  settings();
  mSetup->linkDatas();
  mSetup->apply();
  mUpdatingFormat = false;
}

void CYBargraph::mouseDoubleClickEvent ( QMouseEvent * e )
{
  if (!core->simulation())
    return CYDisplay::mouseDoubleClickEvent ( e );

  for ( int i = 0; i < datas.count(); ++i )
  {
    CYDisplayItem *d = datas.at(i);
    CYNumDialog *dlg = new CYNumDialog(this);
    dlg->setDataName(d->objectName().toUtf8());
    dlg->exec();
  }
}
