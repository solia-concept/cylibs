/***************************************************************************
                          cybargraphsignal.cpp  -  description
                             -------------------
    begin                : jeu déc 4 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cybargraphsignal.h"

// CYLIBS
#include "cydata.h"

CYBargraphSignal::CYBargraphSignal(QObject *parent, CYData *data, int id, QString l)
  : CYDisplayItem(parent, data, id, l)
{
}
