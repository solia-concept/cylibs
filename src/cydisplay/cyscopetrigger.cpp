#include "cyscopetrigger.h"
#include "ui_cyscopetrigger.h"

CYScopeTrigger::CYScopeTrigger(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYScopeTrigger)
{
  ui->setupUi(this);
}

CYScopeTrigger::~CYScopeTrigger()
{
  delete ui;
}

void CYScopeTrigger::setScope(CYScope *scope)
{
  ui->widget->setScope(scope);
}
