//
// C++ Implementation: cyscopesetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyscopesetup.h"
#include "ui_cyscopesetup.h"

// QT
#include <QTreeWidget>
#include <QTreeWidgetItem>
// CYLIBS
#include "cycore.h"
#include "cyscope.h"
#include "cyflag.h"
#include "cyu8.h"
#include "cyf64.h"
#include "cycolor.h"
#include "cyscopeplotter.h"
#include "cydataseditlist.h"
#include "cyscopesignal.h"
#include "cycombobox.h"
#include "cytextlabel.h"
#include "cyflaginput.h"
#include "cyscopecapture.h"
#include "cyscopeplotter.h"
#include "cyscopeaxis.h"
#include "cyscopeaxissetup.h"
#include "cynuminput.h"
#include "cydataslistview.h"

CYScopeSetup::CYScopeSetup(CYScopePlotter *parent, const QString &name)
  : CYDialog(0,name), ui(new Ui::CYScopeSetup)
{
  mPlot = parent;
  ui->setupUi(this);

  ui->dataY->setMaxNbDatas( core->maxCurvesByScope() );
  if ( mPlot->inherits("CYScopeCapture" ) )
  {
    ui->modeGroup->hide();
    ui->dataX->hideBrowser();
    ui->dataY->hideBrowser();
    ui->dataY->setEnableDelete(false);
  }
  QTreeWidgetItem *item = ui->dataY->list()->headerItem();
  item->setText(ui->dataY->list()->columnCount(), tr("Axis"));
}

CYScopeSetup::~CYScopeSetup()
{
  delete ui;
}

/*! Change l'affichage en fonction du mode sélectionné.
    \fn CYScopeSetup::setMode( int mode )
 */
void CYScopeSetup::setMode( int mode )
{
  switch (mode)
  {
    case CYScope::Time  : ui->dataXFrame->hide();
                          break;
    case CYScope::XY    : ui->dataXFrame->show();
                          break;
    default             : CYMESSAGE;
  }
}


/*! Fonction appelée à l'affichage du widget.
    \fn CYScopeSetup::showEvent(QShowEvent *e)
 */
void CYScopeSetup::showEvent(QShowEvent *e)
{
  init();

  CYDialog::showEvent(e);

#if CY_DEBUG_SAVE_CURVES == 0
  CYU8 *mode = (CYU8 *)core->findData(mPlot->db(), "MODE");
  setMode(mode->val());
#endif

  samplingModeShown();
}


/*!
    \fn CYScopeSetup::init()
 */
void CYScopeSetup::init()
{
  setUpdateIfVisible(false);

  ui->dataY->list()->setBackgroundColorPen(mPlot->backgroundColor()->val());
  ui->dataY->list()->clear();
  ui->dataX->list()->clear();

  ui->dataY->enabelFilterBox(false);
  ui->dataX->enabelFilterBox(false);

  setLocaleDB( mPlot->db() );

  ui->bottomAxis->setScopeAxis( mPlot->scopeAxis[QwtPlot::xBottom] );
  ui->leftAxis->setScopeAxis( mPlot->scopeAxis[QwtPlot::yLeft] );
  if (!mPlot->hasMultipleFormats())
    ui->rightAxis->setScopeAxis( mPlot->scopeAxis[QwtPlot::yRight] );
  else
  {
    ui->rightAxis->hide();
  }


  for ( int i = 0; i < mPlot->ySignal.count(); ++i )
  {
    CYScopeSignal *ySignal = mPlot->ySignal.at(i);
    ui->dataY->addData( ySignal );
    QTreeWidgetItem *item = ui->dataY->list()->findItems(ySignal->data->objectName(), Qt::MatchExactly, ui->dataY->list()->dataNameColumn()).first();
    if (item)
      item->setText(ui->dataY->list()->column(tr("Axis")), ySignal->scopeAxis()->desc());
  }

  CYScopeSignal * xSignal = mPlot->xSignal(QwtPlot::xBottom);
  if (xSignal)
    ui->dataX->addData(xSignal);
}


/*!
    \fn CYScopeSetup::apply()
 */
void CYScopeSetup::apply()
{
  CYDialog::update();

  emit applying();

  samplingModeShown();

  ui->applyButton->setDisabled( true );

  linkDatas();
}


/*! Affiche la saisie du mode d'échantillonnage si celui par rafales est possible
    \fn CYScopeSetup::samplingModeShown()
 */
bool CYScopeSetup::samplingModeShown()
{
  if (!mPlot->ySignal.isEmpty() && mPlot->ySignal.first())
  {
    CYData *data = mPlot->ySignal.first()->data;
    if (data && data->burstsBufferFlag())
    {
      ui->samplingModeInput->show();
      ui->samplingModeLabel->show();
      return true;
    }
    else
    {
      ui->samplingModeInput->hide();
      ui->samplingModeLabel->hide();
      return false;
    }
  }
  else
  {
    ui->samplingModeInput->hide();
    ui->samplingModeLabel->hide();
  }
  return false;
}


/*! Affiche l'aide relative du manuel.
    \fn CYScopeSetup::help()
 */
void CYScopeSetup::help()
{
  core->invokeHelp( "cydoc", "display_scope_settings");
}

CYDatasListView *CYScopeSetup::dataXList()
{
  return ui->dataX->list();
}

CYDatasListView *CYScopeSetup::dataYList()
{
  return ui->dataY->list();
}

