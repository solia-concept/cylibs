/***************************************************************************
                          cyscopeanalyser.h  -  description
                             -------------------
    début                  : ven mai 16 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYSCOPEANALYSER_H
#define CYSCOPEANALYSER_H

// QT
#include <QWidget>
#include <QLayout>
#include <QImage>
#include <QStackedWidget>
#include <QComboBox>
#include <QPushButton>
#include <QFrame>
// CYLIBS
#include <cywin.h>

class CYScope;
class CYScopeCapture;

/** CYScopeAnalyser est l'analyseur des n courbes capturées par l'oscilloscope.
  * Il permet de connaître les valeurs représentées par les courbes au moyen de
  * curseurs.
  * 3 mode d'utilisation existent:
  *   - Simple  : 1 valeur en X et 1 valeur en Y.
  *   - Double  : 2 valeurs en X, 2 valeurs en Y et les deltas relatifs.
  *   - Follow  : mesure les n valeurs en X des n courbes pour un une abscisse donnée.
  * Un type d'impression est possible pour chaque mode d'utilisation. Chacun de ces types
  * imprime les n courbes avec les curseurs présents et les valeurs qui en découlent.
  * @author Gérald LE CLEACH
  */

class CYScopeAnalyser : public CYWin
{
   Q_OBJECT
public:
  CYScopeAnalyser(CYScope *s, const QString &name, int index, const QString &label, bool gui);
  ~CYScopeAnalyser();
  /** Imprime les légendes.
    * @param nb Nombre de légendes de courbes déjà imprimés. */
  void printLegends(QPainter *p, QRectF *r, int top, int &nb);
  int nbCurves();

  virtual void createGUI(const QString &xmlfile=0);

  /** Positionnne le curseur 1 en X suivant la valeur \a val. */
  void setCursorX1(double val);
  /** Positionnne le curseur 1 en Y suivant la valeur \a val. */
  void setCursorY1(double val);
  /** Positionnne le curseur 2 en X suivant la valeur \a val. */
  void setCursorX2(double val);
  /** Positionnne le curseur 2 en Y suivant la valeur \a val. */
  void setCursorY2(double val);

  /** @return le traceur de l'oscilloscope. */
  virtual CYScopeCapture *plot() { return mPlotter; }

  /** @return \a true si la fenêtre est utilisée en tant qu'interface graphique,
   * sinon il s'agit d'une fenêtre furtive cachée pour faire un export. */
  bool isGUI() { return mGUI; }

public slots:
  void print();
  void exportPDF();
  void exportPDF(QString fileName);
  /*! Exporter les courbes dans un fichier texte
      @param saveAs Par défaut vaut à \a true pour avoir la boîte de sélection du fichier, pour avoir automatiquement un nom de fichier il suffit de le mettre à \a false.
      \fn CYScopeAnalyser::exportCSV(bool saveAs)
   */
  virtual void exportCSV( bool saveAs = true );
  /*! Exporter les courbes dans un fichier texte
      @param fileName Nom du fichier CSV.
      \fn CYScopeAnalyser::exportCSV(QString fileName)
   */
  virtual void exportCSV( QString fileName );

private:
  QFrame *mFrame;

  /** Capture des courbes. */
  CYScopeCapture *mPlotter;

  QStackedWidget        *mStack;
  QGridLayout *mGrid;

  bool mGUI;

public slots:
    void save();
};

#endif
