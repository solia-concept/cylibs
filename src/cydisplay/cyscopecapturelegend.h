#ifndef CYSCOPECAPTURELEGEND_H
#define CYSCOPECAPTURELEGEND_H

// QT
#include <QGridLayout>
// QWT
#include "qwt_legend.h"

class CYScopeCapture;
class CYDB;
class CYData;
class CYFrame;

class CYScopeCaptureLegend : public QwtLegend
{
  Q_OBJECT
public:
  CYScopeCaptureLegend(CYScopeCapture *);

protected:
  virtual QWidget *createWidget( const QwtLegendData & ) const;
  virtual void initAxisDisplay();

  CYScopeCapture *mPlot;
  CYDB *mDB;

  bool mInitAxisDisplay;
};

#endif // CYSCOPECAPTURELEGEND_H
