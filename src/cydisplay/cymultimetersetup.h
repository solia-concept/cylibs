//
// C++ Interface: cymultimetersetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMULTIMETERSETUP_H
#define CYMULTIMETERSETUP_H

// CYLIBS
#include "cydialog.h"

class CYMultimeter;

namespace Ui {
    class CYMultimeterSetup;
}

/**
@short Boîte de configuration d'un multimètre.

@author Gérald LE CLEACH
*/
class CYMultimeterSetup : public CYDialog
{
Q_OBJECT
public:
    CYMultimeterSetup(CYMultimeter *parent = 0, const QString &name = 0);

    ~CYMultimeterSetup();

protected:
    virtual void init();

public slots:
    virtual void apply();

private:
    CYMultimeter * mMultimeter;
private slots:
    void help();

private:
    Ui::CYMultimeterSetup *ui;
};

#endif
