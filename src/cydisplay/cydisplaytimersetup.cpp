#include "cydisplaytimersetup.h"
#include "ui_cydisplaytimersetup.h"

CYDisplayTimerSetup::CYDisplayTimerSetup(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYDisplayTimerSetup)
{
  ui->setupUi(this);
}

CYDisplayTimerSetup::~CYDisplayTimerSetup()
{
  delete ui;
}
