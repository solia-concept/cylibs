//
// C++ Interface: cyscopesetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSCOPESETUP_H
#define CYSCOPESETUP_H

// CYLIBS
#include "cydialog.h"

class CYScopePlotter;
class CYDatasListView;

namespace Ui {
    class CYScopeSetup;
}

/**
@short Boîte de configuration de l'oscilloscope.

@author Gérald LE CLEACH
*/
class CYScopeSetup : public CYDialog
{
Q_OBJECT
public:
  CYScopeSetup(CYScopePlotter *parent = 0, const QString &name = 0);

    ~CYScopeSetup();
    bool samplingModeShown();

    void init();

    CYDatasListView *dataXList();
    CYDatasListView *dataYList();

signals:
    void applying();

public slots:
    virtual void apply();

protected slots:
    virtual void setMode( int mode );

protected:
    virtual void showEvent(QShowEvent *e);

private slots:
    void help();

protected:
    CYScopePlotter * mPlot;

private:
    Ui::CYScopeSetup *ui;
};

#endif
