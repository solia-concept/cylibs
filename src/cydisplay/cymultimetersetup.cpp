//
// C++ Implementation: cymultimetersetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//

// QT
#include <QTreeWidgetItemIterator>
// CYLIBS
#include "cycore.h"
#include "cymultimeter.h"
#include "cymultimetersetup.h"
#include "ui_cymultimetersetup.h"
#include "cydataslistview.h"

CYMultimeterSetup::CYMultimeterSetup(CYMultimeter *parent, const QString &name)
  : CYDialog(0,name),
   mMultimeter(parent)
{
  ui=new Ui::CYMultimeterSetup;
  ui->setupUi(this);

  init();
}


CYMultimeterSetup::~CYMultimeterSetup()
{
  delete ui;
}

/*!
    \fn CYMultimeterSetup::init()
 */
void CYMultimeterSetup::init()
{
  setUpdateIfVisible(false);

  setLocaleDB( mMultimeter->db() );

  for (int i = 0; i < mMultimeter->datas.count(); ++i)
    ui->datasEditList->addData(mMultimeter->datas.at(i));
}


/*!
    \fn CYMultimeterSetup::apply()
 */
void CYMultimeterSetup::apply()
{
  CYDatasListView *list;

  CYDialog::update();

  list = ui->datasEditList->list();
  mMultimeter->removeDatas();
  QTreeWidgetItemIterator it(list);
  while (*it)
  {
    mMultimeter->addData(core->findData(list->dataName(*it)),list->label(*it));

     ++it;
  }
  linkDatas();
  CYDialog::update();

  mMultimeter->applySettings();

  ui->applyButton->setDisabled( true );
}


/*! Affiche l'aide relative du manuel.
    \fn CYMultimeterSetup::help()
 */
void CYMultimeterSetup::help()
{
  core->invokeHelp( "cydoc", "display_multimeter_settings");
}
