/***************************************************************************
                          cyscopesignal.cpp  -  description
                             -------------------
    début                  : mer sep 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyscopesignal.h"

// CYLIBS
#include "cycore.h"
#include "cyf64.h"
#include "cydb.h"
#include "cypen.h"
#include "cyscopecapture.h"
#include "cyscopeaxis.h"

CYScopeSignal::CYScopeSignal(QObject *parent, CYData *data, int id, QString l, QPen p, double coef, QwtPlot::Axis axis, bool trigger)
  : CYDisplayItem(parent, data, id, l, coef),
    mAxis(axis)
{
  pen = p;
  mEnableBuffering = true;
  mResetNb = 0;
  mResetId1 = 1;
  mResetId2 = 2;
  mBurstsResetNb = 0;
  mBurstsResetId1 = 1;
  mBurstsResetId2 = 2;
  mHidden = false;
  mTriggerSource = trigger;
  mTriggerSignal = 0 ;
  mTriggerState = false;

  title = QString("%1: %2").arg(data->group()).arg(data->label());
  if (!unit.isEmpty())
    title.append(QString(" [%1]").arg(unit));
  if (coefficient!=1.0)
    title.append(QString(" [x%1]").arg(coefficient));

  mScopeCapture = 0;
  initCaptureDatas();
}

CYScopeSignal::~CYScopeSignal()
{
}

double *CYScopeSignal::val(CYScope::SamplingMode mode)
{
  if (!mEnableBuffering)
  {
    if ((mode==CYScope::Continuous) || !data->burstsBufferFlag())
      return mBuffer.data();
    else
      return mBurstsBuffer.data();
  }
  if (core && !core->analyseRefreshEnabled())
    return 0;

  if ((mode==CYScope::Continuous) || !data->burstsBufferFlag())
    return data->bufferCurrent();
  else
    return data->burstsBufferCurrent();
}

bool CYScopeSignal::dataReading()
{
  if (!data)
    return false;
  return data->reading();
}

void CYScopeSignal::copieBuffer(QVector<double> buf, CYScope::SamplingMode mode)
{
  if (scope()->plot()->samplingModeVal()==CYScope::Bursts)
    mCopyId = data->idBurstsBuffer();
  else if (scope()->plot()->triggerIsEnabled())
    mCopyId = data->postTriggerIdBuffer();
  else
    mCopyId = data->idBuffer();    

  if (mode==CYScope::Bursts)
  {
    mBurstsBuffer.detach();
    mBurstsBuffer.resize(buf.size());
    mBurstsBuffer=buf;
  }
  else
  {
    mBuffer.detach();
    mBuffer.resize(buf.size());
    mBuffer=buf;
  }
}

int CYScopeSignal::idBuffer()
{
  return data->idBuffer();
}

QVector<double> CYScopeSignal::buffer(int offset, CYScope::SamplingMode mode)
{
  if (mode==CYScope::Bursts)
    return burstsBuffer(offset);
  else
  {
    if (offset!=0)
    {
      mCapture.resize(mBuffer.size()-offset+1);
      for ( int i = offset; i < (int)mBuffer.size(); i++ )
        mCapture[i-offset] = mBuffer[i];
      return mCapture;
    }
    return mBuffer;
  }
}


QVector<double> CYScopeSignal::burstsBuffer(int offset)
{
  if (data->burstsBufferFlag())
  {
    if (offset!=0)
    {
      mCapture.resize(mBurstsBuffer.size()-offset+1);
      for ( int i = offset; i < (int)mBurstsBuffer.size(); i++ )
        mCapture[i-offset] = mBurstsBuffer[i];
      return mCapture;
    }
  }
  return mBurstsBuffer;
}


/*! RAZ du signal.
    \fn CYScopeSignal::reset()
 */
void CYScopeSignal::reset()
{
  if (!data)
    return;

  mResetNb = scope()->plot()->getSizeMesure();
  mResetId1 = data->idBuffer();
  if ( (mResetId1 - mResetNb) > 0 )
    mResetId2 = mResetId1 - mResetNb;
  else
    mResetId2 = mResetId1 + mResetNb;

  if (data->burstsBufferFlag())
  {
    mBurstsResetNb = data->format()->burstsBufferSize();
    mBurstsResetId1 = data->idBurstsBuffer();
    if ( (mBurstsResetId1 - mBurstsResetNb) > 0 )
      mBurstsResetId2 = mBurstsResetId1 - mBurstsResetNb;
    else
      mBurstsResetId2 = mBurstsResetId1 + mBurstsResetNb;
  }
}

void CYScopeSignal::setTriggerSignal(CYScopeSignal *source)
{
  if (source==this)
  {
    mTriggerSource = true;
    data->setExtTrigger(0);
    mTriggerSignal = 0;
  }
  else
  {
    mTriggerSource = false;
    data->setExtTrigger(source->data); // pointeur sur donnée source
    mTriggerSignal = source; // pointeur sur signal source
  }
}

void CYScopeSignal::startTrigger(double level, double post, Cy::Change slope)
{
//   if (!mTriggerSignal) // pas de trigger actif
//     return;

  if (mTriggerSource) // démarrage du trigger s'il s'agit du signal source
    data->startTrigger(level, post, slope);
}

void CYScopeSignal::stopTrigger()
{
//   if (!mTriggerSignal) // pas de trigger actif
//     return;

  if (mTriggerSource) // arrêt du trigger s'il s'agit du signal source
    data->stopTrigger();

   mTriggerSignal=0;
}

bool CYScopeSignal::triggerState()
{
  if (!mTriggerSource && !data) // s'il ne s'agit pas du signal source trigger
    return false;

  if (dataReading())
    return mTriggerState;

  if (!mTriggerState && data->triggerState()) // détection déclenchement
  {
    mTriggerState = true;
  }
  else if (mTriggerState && !data->triggerState())  // désactivation trigger
    mTriggerState = false;

  return mTriggerState;
}

/*! @return la taille du buffer des valeurs
    \fn CYScopeSignal::bufferSize()
 */
int CYScopeSignal::bufferSize()
{
  return mBuffer.size();
}

/*! @return la taille du buffer par rafales des valeurs
    \fn CYScopeSignal::burstsBufferSize()
 */
int CYScopeSignal::burstsBufferSize()
{
  return mBurstsBuffer.size();
}

/*! @return la valeur à l'index \a id du buffer
    \fn CYScopeSignal::bufferVal(int id)
 */
double CYScopeSignal::bufferVal(int id)
{
  int size = mBuffer.size();
  if (id>size)
  {
//    CYMESSAGETEXT(QString("%1: %2>3").arg(data->objectName()).arg(id).arg(mBuffer.size()));
    return 0;
  }
  return mBuffer[id];
}

/*! @return la valeur à l'index \a id du buffer par rafales
    \fn CYScopeSignal::burstsBufferVal(int id)
 */
double CYScopeSignal::burstsBufferVal(int id)
{
  return mBurstsBuffer[id];
}

/*! @return le nombre de valeurs RAZ
    \fn CYScopeSignal::resetNb()
 */
int CYScopeSignal::resetNb(CYScope::SamplingMode mode)
{
  if ((mode==CYScope::Continuous) || !data->burstsBufferFlag())
    return mResetNb;
  else
    return mBurstsResetNb;
}


/*! @return true si le signal a été reseté.
    \fn CYScopeSignal::isReseted(CYScope::SamplingMode mode)
 */
bool CYScopeSignal::isReseted(CYScope::SamplingMode mode)
{
  if (mEnableBuffering && (core && core->analyseRefreshEnabled()))
  {
    if ((mode==CYScope::Continuous) || !data->burstsBufferFlag())
    {
      if (mResetNb>0)
      {
        int id = data->idBuffer();
        if (data->tableBufferNb())
          mResetNb = id+1; // simple tableau à remplir => pas de buffer tournant
        else if (id!=mResetId1) // buffer tournant
        {
          int nb = 0;
          if (id<mResetId1)  // début du reset
          {
            nb = qAbs(id + data->bufferSize()-mResetId2);
          }
          else if (id>mResetId1) // rebouclage du reset
          {
            nb = id-mResetId1;
          }
          if (nb>mResetNb)
            mResetNb = 0; // fin du reset
          else if (nb!=0)
            mResetNb = nb;
        }
        return true;
      }
    }
    else
    {
      if (mBurstsResetNb>0)
      {
        int id = data->idBurstsBuffer();
        if (id!=mBurstsResetId1)
        {
          int nb = 0;
          if (id<mBurstsResetId1)  // début du reset
          {
            nb = qAbs(id + data->bufferSize()-mBurstsResetId2);
          }
          else if (id>mBurstsResetId1) // rebouclage du reset
          {
            nb = id-mBurstsResetId1;
          }
          if (nb>mBurstsResetNb)
            mBurstsResetNb = 0; // fin du reset
          else if (nb!=0)
            mBurstsResetNb = nb;
        }
        return true;
      }
    }
  }
  return false;
}


void CYScopeSignal::setEnableBuffering(bool val)
{
  mEnableBuffering = val;
}

/*! Bufferisation. Sauvegarde les dernières valeurs dans un tableau.
    \fn CYScopeSignal::buffering()
 */
void CYScopeSignal::buffering()
{
  if (mEnableBuffering && (core && core->analyseRefreshEnabled()))
  {
    int bufferSize;
    int size;

    if (data->tableBufferSize())
    {
      // bufferisation d'un tableau de valeurs
      bufferSize = data->tableBufferSize();
      mBuffer.fill(data->initBufferVal(), data->tableBufferSize());
      size = data->tableBufferCpt();
    }
    else
    {
      // bufferisation temporelle
      bufferSize = data->format()->bufferSize(!data->isReadFast());
      mBuffer.fill(data->initBufferVal(), bufferSize);
      size = bufferSize-mResetNb;
    }

    if (size<0)
      CYFATAL // Permet une sortie plus explicite qu'un crash logiciel

    if (scope()->plot()->triggerIsEnabled())
      std::copy(data->bufferTrigger(), data->bufferTrigger() + size, mBuffer.begin());
    else
      std::copy(data->bufferCurrent(), data->bufferCurrent() + size, mBuffer.begin());

    if (data->burstsBufferFlag())
    {
      bufferSize = data->format()->burstsBufferSize();
      size = bufferSize-mBurstsResetNb;

      mBurstsBuffer.fill(data->initBufferVal(), bufferSize);
      std::copy(data->burstsBufferCurrent(), data->burstsBufferCurrent() + size, mBurstsBuffer.begin());
    }
  }
}


/*! @return le signal capturé avec une copie du buffer.
    \fn CYScopeSignal::capture()
 */
CYScopeSignal * CYScopeSignal::capture()
{
  CYScopeSignal *signal =  new CYScopeSignal(this, data, id, label, pen, coefficient, mAxis);
  Q_CHECK_PTR(signal);

  signal->mBuffer.detach();
  signal->mBuffer=mBuffer;
  signal->mResetNb = mResetNb;
  signal->mResetId1 = mResetId1;
  signal->mResetId2 = mResetId2;

  signal->mBurstsBuffer.detach();
  signal->mBurstsBuffer=mBurstsBuffer;
  signal->mBurstsResetNb = mBurstsResetNb;
  signal->mBurstsResetId1 = mBurstsResetId1;
  signal->mBurstsResetId2 = mBurstsResetId2;

  signal->setEnableBuffering(false);
  return signal;
}


/*! @return \a true si le signal est caché.
    \fn CYScopeSignal::hidden()
 */
bool CYScopeSignal::hidden()
{
  return mHidden;
}


/*! Cache le signal
    \fn CYScopeSignal::hide()
 */
void CYScopeSignal::hide()
{
  mHidden = true;
  emit disableCurve( id );
}


/*! Affiche le signal
    \fn CYScopeSignal::show()
 */
void CYScopeSignal::show()
{
  mHidden = false;
  emit enableCurve( id );
}


/*! @return l'oscilloscope parent.
    \fn CYScopeSignal::scope()
 */
CYScope * CYScopeSignal::scope()
{
  if ( parent()->inherits("CYScopeSignal") )
  {
    CYScopeSignal *p = ( CYScopeSignal *)parent();
    return p->scope();
  }
  if ( parent()->inherits("CYScopePlotter") )
  {
    CYScopePlotter *p = ( CYScopePlotter *)parent();
    return p->scope();
  }
  if ( parent()->inherits("CYScope") )
  {
    return ( CYScope *)parent();
  }
  return 0;
}


/*! Initialise les données relatives à une capture d'oscilloscope.
    \fn CYScopeSignal::initCaptureDatas()
 */
void CYScopeSignal::initCaptureDatas()
{
  mCursors.clear();
  if (!parent()->inherits("CYScopeCapture"))
    return;
  mScopeCapture = (CYScopeCapture *)parent();
  CYDB *db = mScopeCapture->db();

  CYF64 *df64;

  df64 = new CYF64(db, QString("%1_CURSOR1").arg(data->objectName()), new f64, data->numFormat());
  df64->setFormat(data->format());
  df64->setMin(scopeAxis()->minVal());
  df64->setMax(scopeAxis()->maxVal());
  df64->setLabel("Y1fx");
  df64->setGroup(data->group()+":"+data->label());
  df64->setEnableColor( mScopeCapture->cursorColor(CYScopeCapture::Cursor1) );
  mCursors.insert(CYScopeCapture::Cursor1, df64);

  df64 = new CYF64(db, QString("%1_CURSOR2").arg(data->objectName()), new f64, data->numFormat());
  df64->setFormat(data->format());
  df64->setMin(scopeAxis()->minVal());
  df64->setMax(scopeAxis()->maxVal());
  df64->setLabel("Y2fx");
  df64->setGroup(data->group()+":"+data->label());
  df64->setEnableColor( mScopeCapture->cursorColor(CYScopeCapture::Cursor2) );
  mCursors.insert(CYScopeCapture::Cursor2, df64);

  df64 = new CYF64(db, QString("%1_DELTA_CURSOR").arg(data->objectName()), new f64, data->numFormat());
  df64->setFormat(data->format());
  double max = qMax(qAbs(scopeAxis()->minVal()), qAbs(scopeAxis()->maxVal()));
  df64->setMin(-max);
  df64->setMax(max);
  df64->setLabel("dYfx");
  df64->setGroup(data->group()+":"+data->label());
  mCursors.insert(CYScopeCapture::Delta, df64);
}


CYF64 *CYScopeSignal::cursor(CYScopeCapture::Cursor cursor)
{
  return mCursors[cursor];
}


CYScopeAxis * CYScopeSignal::scopeAxis()
{
  return scope()->plot()->scopeAxis[mAxis];
}

void CYScopeSignal::setYCoef( double coef )
{
  scope()->setYCoef(this, coef);
}

void CYScopeSignal::setYOffset( double offset )
{
  scope()->setYOffset(this, offset);
}

/*! @return la légende du signal
    \fn CYScopeSignal::legend(bool withGroup=false, bool withUnit=true)
 */
QString CYScopeSignal::legend(bool withGroup, bool withUnit)
{
  QString txt;
  if (withGroup && data)
  {
    txt=QString("%1: %2").arg(data->group()).arg(data->label());
    if (data->label()!=label)
      txt.append(QString("=> %1").arg(label));
  }
  else
    txt=label;
  if (withUnit && !unit.isEmpty())
    txt.append(QString(" [%1]").arg(unit));
  if ((coefficient!=1.0) && (offset>1e-300))
    txt.append(QString(" [x%1 + %2]").arg(coefficient).arg(offset));
  else if (coefficient!=1.0)
    txt.append(QString(" [x%1]").arg(coefficient));
  else if (offset>1e-300)
    txt.append(QString(" [+%1]").arg(offset));
  return txt;
}

int CYScopeSignal::triggerIdBuffer()
{
  return data->triggerIdBuffer();
}

int CYScopeSignal::triggerBufferCpt()
{
  return data->triggerBufferCpt();
}

