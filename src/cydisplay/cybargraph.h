/***************************************************************************
                          cybargraph.h  -  description
                             -------------------
    début                  : Mon Feb 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYBARGRAPH_H
#define CYBARGRAPH_H

// ANSI
#include <assert.h>
#include <string.h>
// CYLIBS
#include "cydisplay.h"

class CYDisplayItem;
class CYBargraphPlotter;
class CYBargraphSetup;
class CYBargraphSignal;

/**
 * CYBargraph est un CYDisplay de type bargraph(s).
 * @short Bargraph(s).
 * @author Gérald LE CLEACH.
 */

class CYBargraph : public CYDisplay
{
  Q_OBJECT

public:
  /**
   * Construit un bargraph.
   * @param parent  Widget parent.
   * @param name    Nom de l'afficheur.
   * @param title   Titre de l'afficheur.
   * @param nf      Pas de trame existante.
   */
  CYBargraph(QWidget *parent=0, const QString &name=0, const QString &title=0, bool nf=false);
  virtual ~CYBargraph();

  /** Ajoute une donnée dans l'afficheur. */
  virtual bool addData(CYData *data) { return addData(data, 0); }
  /** Ajoute une donnée dans l'afficheur. */
  virtual bool addData(CYData *data, QString label);
  bool removeData(uint idx);

  /** @return le traceur de l'afficheur. */
  virtual CYBargraphPlotter *plot() { return mPlot; }

  virtual QSize sizeHint(void);

  bool createFromDOM(QDomElement &element, QStringList *ErrorList=nullptr);
  bool addToDOM(QDomDocument &doc, QDomElement &element, bool save=true);

  virtual bool hasSettingsDialog() const
  {
    return (true);
  }

  void settings();

  /** Gestion du menu surgissant.
    * @param pm0    Menu appelant s'il existe.
    * @param index  Index de l'entrée du menu appelant.
    * @param end    Fin du menu. */
  virtual int showPopupMenu(QMenu *pm0=0, int index=0, bool end=false);

public slots:
   /** Rafraîchit l'afficheur. */
  virtual void refresh();
  void applySettings();
  virtual void removeDatas();

    virtual void updateFormat();

protected: // Protected methods
  virtual void resizeEvent(QResizeEvent *);
  /** Un double-clic permet en mode simulation de changer la valeur de la donnée visualisée */
  virtual void mouseDoubleClickEvent ( QMouseEvent * e );

private:
  uint bars;
  CYBargraphPlotter *mPlot;
  CYBargraphSetup *mSetup;
  ulong flags;
};

#endif
