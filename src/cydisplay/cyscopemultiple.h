//
// C++ Interface: cyscopemultiple
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSCOPEMULTIPLE_H
#define CYSCOPEMULTIPLE_H

#include <cyscope.h>

/** CYScopeMultiple est un CYScope avec une multitude de formats de données sur un seul axe avec en légende de chaque courbe l'unité entre crochets.
  @short Oscilloscope aux formats multiples
	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYScopeMultiple : public CYScope
{
Q_OBJECT
public:
  /** Construit un oscilloscope aux formats multiples.
    * @param parent   Widget parent.
    * @param name     Nom de l'oscilloscope.
    * @param title    Titre de l'oscilloscope.
    * @param nf       Pas de trame existante.
    * @param mode     Mode d'utilisation de l'oscilloscope.
    * @param sampling Mode d'échantillonnage de l'oscilloscope.
    */
    CYScopeMultiple( QWidget *parent = 0, const QString &name = "scope", const QString &title = 0, bool nf = false, CYScope::Mode mode = Time, CYScope::SamplingMode acq = Continuous);

    ~CYScopeMultiple();
};

#endif
