#ifndef CYSCOPETRIGGER_H
#define CYSCOPETRIGGER_H

// CYLIBS
#include <cydialog.h>

class CYScope;

namespace Ui {
    class CYScopeTrigger;
}

class CYScopeTrigger : public CYDialog
{
  Q_OBJECT
public:
  explicit CYScopeTrigger(QWidget *parent = 0, const QString &name=0);
  ~CYScopeTrigger();

  void setScope(CYScope *scope);

private:
  Ui::CYScopeTrigger *ui;
};

#endif // CYSCOPETRIGGER_H

