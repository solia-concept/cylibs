//
// C++ Interface: cybargraphsetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYBARGRAPHSETUP_H
#define CYBARGRAPHSETUP_H

// CYLIBS
#include <cydialog.h>

class CYBargraph;


namespace Ui {
    class CYBargraphSetup;
}

/**
@short Boîte de configuration d'un CYBargraph

@author Gérald LE CLEACH
*/
class CYBargraphSetup : public CYDialog
{
  Q_OBJECT
public:
  CYBargraphSetup(CYBargraph *parent = 0, const QString &name = 0);

  ~CYBargraphSetup();

protected:
  virtual void init();
public slots:
  virtual void apply();
private slots:
  void help();

protected:
  CYBargraph * mBargraph;

private:
  Ui::CYBargraphSetup *ui;
};

#endif
