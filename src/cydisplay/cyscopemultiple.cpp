//
// C++ Implementation: cyscopemultiple
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyscopemultiple.h"

CYScopeMultiple::CYScopeMultiple(QWidget *parent, const QString &name, const QString &title, bool nf, CYScope::Mode mode, CYScope::SamplingMode acq)
 : CYScope(parent, name, title, nf, mode, acq)
{
  mScopeMultiple=true;
}


CYScopeMultiple::~CYScopeMultiple()
{
}
