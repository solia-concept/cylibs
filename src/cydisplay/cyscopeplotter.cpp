/***************************************************************************
                          cyscopeplotter.cpp  -  description
                             -------------------
    début                  : mer sep 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
  
                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX
                 
 ***************************************************************************/

#include "cyscopeplotter.h"

// QT
#include <QToolTip>
#include <QDesktopServices>
// QWT
#include "qwt_plot_dict.h"
//#include "qwt_scale.h"
#include "qwt_plot_canvas.h"
#include "qwt_plot_grid.h"
#include "qwt_plot_curve.h"
#include "qwt_abstract_legend.h"
#include "qwt_legend.h"
#include <qwt_scale_widget.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_magnifier.h>
// CYLIBS
#include "cycore.h"
#include "cynethost.h"
#include "cyformat.h"
#include "cydb.h"
#include "cydata.h"
#include "cyu8.h"
#include "cys16.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cytime.h"
#include "cycolor.h"
#include "cystring.h"
#include "cydisplaystyle.h"
#include "cyscope.h"
#include "cyscopesignal.h"
#include "cyscopeacquisition.h"
#include "cyscopeaxis.h"
#include "cypen.h"
#include "cypendialog.h"
#include "cymessagebox.h"
#include "cyscopetrigger.h"
#include "cyscopetriggersetup.h"
#include "cyplotcurve.h"
#include "cyscopeanalyser.h"
#include "cyscopelegend.h"

CYScopePlotter::CYScopePlotter(CYScope *scope, QWidget *parent, const QString &name, int timer, CYDB *db, CYScope::Mode mode, CYScope::SamplingMode sampling, bool capture)
  : CYPlot(parent, name),
    mScope(scope),
    mDB(db),
    mIsCapture(capture)
{
  mInit=false;
  mAcquisition = nullptr;
  mInitDB = false;
  mLocalDB = false;
  mTriggerEnabled = false;
  mTriggerSignal = nullptr;
  mDiscontData = false;
  mDiscontDataState = 0;
  // TODO QWT d_canvas->setCacheMode(false);
  size_mesure=0;

  init(timer, mode, sampling);
  mInit=true;
}

CYScopePlotter::~CYScopePlotter()
{
  scopeAxis.clear();
  if (mLocalDB)
    delete mDB;
}

void CYScopePlotter::init(int timer, CYScope::Mode mode, CYScope::SamplingMode sampling)
{
  mZooming  = false;
  mZoomCoef = 2.0;
  // TOCHEK QWT
  mOutlineStyle = QwtPicker::CrossRubberBand;
  offset_mesure = 0;
  mTimeAxis = xBottom;
  
  for (int a = 0; a < axisCnt; a++)
  {
    delete scopeAxis.take(a);
    scopeAxis.insert(a, new CYScopeAxis(this, (QwtPlot::Axis)a));
  }

  initDB();

  setModeVal(mode);
  setSamplingModeVal(sampling);

  // La détection automatique ne fonctionne pas pour les tableaux de pointeurs.
  timerInterval = timer;
  rangeX = true;
  sizeEnableLegend = true;
  
  // Style
  ((QFrame *)canvas())->setLineWidth(2);
  setLineWidth(2);
  setMinimumSize(800, 500);
  
  // Quelque chose de plus petit que ça n'a aucun sens.
  setMinimumSize(50, 50);
  setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
  
  // Insertion d'un axe de référence pour y gauche = 0
  yLeftRef = insertLineMarker("", QwtPlot::yLeft);
  setMarkerYPos(yLeftRef, 0.0);
  yLeftRefColor = Qt::gray;
  yLeftRefPen.setColor(yLeftRefColor);
  setMarkerPen(yLeftRef, yLeftRefPen);

  // Insertion d'un axe de référence pour y droit = 0
  yRightRef = insertLineMarker("", QwtPlot::yRight);
  setMarkerYPos(yRightRef, 0.0);
  yRightRefColor = Qt::gray;
  yRightRefPen.setColor(yRightRefColor);
  setMarkerPen(yRightRef, yRightRefPen);
  
  QString note = tr("<p>If you click on the scale, a dialog box will appear to change it.</p>");
  axisWidget(QwtPlot::xTop)->setToolTip(note);
  axisWidget(QwtPlot::xBottom)->setToolTip(note);
  axisWidget(QwtPlot::yLeft)->setToolTip(note);
  axisWidget(QwtPlot::yRight)->setToolTip(note);

  //QT5connect(this, SIGNAL(legendClicked(long)), SLOT(curveClicked(long)));

  applySettings();
}

//QString CYScopePlotter::title() const
//{
//  return mScope->title();
//}

void CYScopePlotter::initTimeScale(uint nb, float step, int burstsRatio)
{
  timeScopeAxis()->initTimeScale(nb, step, burstsRatio);
}

void CYScopePlotter::changeTimeScale(double left, double right)
{
  timeScopeAxis()->changeTimeScale(left, right);
}

void CYScopePlotter::changeTimeScale()
{
  timeScopeAxis()->changeTimeScale();
}

void CYScopePlotter::changeXScale(int axis, double min, double max)
{
  if (axis==QwtPlot::xBottom)
    scopeAxis[QwtPlot::xBottom]->changeXScale(min, max);
  else if (axis==QwtPlot::xTop)
    scopeAxis[QwtPlot::xTop]->changeXScale(min, max);
}

void CYScopePlotter::changeXScales()
{
  scopeAxis[QwtPlot::xBottom]->changeXScale();
  scopeAxis[QwtPlot::xTop]->changeXScale();
}

void CYScopePlotter::copy(CYScopePlotter *s)
{
  s->db()->copy();
  mDB->paste();
  
  if (!s->ySignal.isEmpty() && s->ySignal.first())
  {
    CYData *data = s->ySignal.first()->data;
    if (data && (modeVal() == CYScope::Time))
    {
      if (!data->isReadFast() || (samplingModeVal() == CYScope::Continuous))
        initTimeScale(data->format()->bufferSize(!data->isReadFast()), data->format()->acquisitionTime(!data->isReadFast()));
      else
        initTimeScale(data->format()->burstsBufferSize(), data->format()->acquisitionTime(false), data->format()->burstsRatio());
    }
  }
  
  //  s->db()->copy();
  //  mDB->paste();
  mScope->setTitle(s->title().text());
  timerInterval = s->timerInterval;
  
  // Fond
  //  setCanvasBackground(QBrush(s->mBackgroundColor->val()));
  
  // Grille principale
  mMajGrid->setVal(s->mMajGrid->val());
  setGridColor(s->mMajGridColor->val());
  
  // Grille secondaire
  mMinGrid->setVal(s->mMinGrid->val());
  setGridMinColor(s->mMinGridColor->val());
  
  mLegendPosition->setVal(s->mLegendPosition->val());
  //  enableLegend(mLegends->val());
  setFontSize(s->fontSize());

  acquisition()->separator->setVal(s->acquisition()->separator->val());
  acquisition()->dirName->setVal(s->acquisition()->dirName->val());
  acquisition()->setNameDir(acquisition()->dirName->val());

  QHashIterator<int, CYScopeAxis*> it(scopeAxis);
  while (it.hasNext())
  {
    it.next();
    CYScopeAxis* axis = it.value();
    axis->copy(s);
  }
  applySettings();
}

CYScopeSignal* CYScopePlotter::addCurve(CYData *data, int id, QString label, QPen pen, double coefficient, QwtPlot::Axis yAxis, bool trigger)
{
  if (ySignal.count() >= core->maxCurvesByScope())
  {
    if (isVisible())
      CYMessageBox::information(this, QString(tr("For a good display the maximum number of curves is %1!")).arg(core->maxCurvesByScope()));
    return 0;
  }
  
  CYScopeAxis *ax1, *ax2;
  if (yAxis == yLeft)
  {
    ax1 = scopeAxis[yLeft];
    ax2 = scopeAxis[yRight];
  }
  else if (yAxis == yRight)
  {
    ax1 = scopeAxis[yRight];
    ax2 = scopeAxis[yLeft];
  }
  else
    CYFATAL;
  
  if (!ySignal.isEmpty() && !scope()->scopePreview())
  {
    if (ySignal.first()->data->acquisitionTime() != data->acquisitionTime())
    {
      CYMessageBox::information(this, QString(tr("You must put data with the same acquisition time of the data(s) "
                                                 "already displayed by this oscilloscope")));
      return 0;
    }
    if (ySignal.first()->data->bufferSize() != data->bufferSize())
    {
      CYMessageBox::information(this, QString(tr("You must put data with the same buffer size of the data(s) "
                                                 "already displayed by this oscilloscope")));
      return 0;
    }
    if (ySignal.first()->data->burstsBufferSize() != data->burstsBufferSize())
    {
      CYMessageBox::information(this, QString(tr("You must put data with the same burst buffer size of the data(s) "
                                                 "already displayed by this oscilloscope")));
      return 0;
    }
  }

  if (!hasMultipleFormats())
  {
    if (ySignal.count() == 0)
    {
      // Première donnée de l'oscilloscope
      ax1->initY(data);

      CYScopeSignal *signal = addCurve(yAxis, data, id, label, pen, coefficient, trigger);

      applySettings();
      return signal;
    }
    else if (ax1->numFormat() == 0)
    {
      if (ax2->linksName() != data->linksName())
      {
        CYMessageBox::information(this, QString(tr("You must put data from the same connection of the data(s)"
                                                   "already displayed by this oscilloscope")));
        return 0;
      }
      // Première donnée sur cet axe
      ax1->initY(data);

      CYScopeSignal *signal = addCurve(yAxis, data, id, label, pen, coefficient, trigger);

      applySettings();
      return signal;
    }
    else if (ax1->compatibleFormat(data->format()))
    {
      if (ax1->linksName() != data->linksName())
      {
        CYMessageBox::information(this, QString(tr("You must put data from the same connection of the data(s)"
                                                   "already displayed by this oscilloscope")));
        return 0;
      }

      return addCurve(yAxis, data, id, label, pen, coefficient, trigger);
    }
    else if (ax2->numFormat() == 0)
    {
      if (ax1->linksName() != data->linksName())
      {
        CYMessageBox::information(this, QString(tr("You must put data from the same connection of the data(s)"
                                                   "already displayed by this oscilloscope")));
        return 0;
      }
      // Première donnée sur l'autre axe
      ax2->initY(data);

      CYScopeSignal *signal = addCurve(ax2->type(), data, id, label, pen, coefficient, trigger);

      applySettings();
      return signal;
    }
    else if (ax1->compatibleFormat(data->format()))
    {
      if (ax2->linksName() != data->linksName())
      {
        CYMessageBox::information(this, QString(tr("You must put data from the same connection of the data(s)"
                                                   "already displayed by this oscilloscope")));
        return 0;
      }
      return addCurve(ax2->type(), data, id, label, pen, coefficient, trigger);
    }
    else
    {
      CYMessageBox::information(this, QString(tr("You must put the same physical type of the data(s) "
                                                 "already displayed by this oscilloscope")));
      return 0;
    }
  }
  else
  {
    if (ySignal.count() == 0)
    {
      // Première donnée de l'oscilloscope
      ax1->initY(data);

      CYScopeSignal *signal = addCurve(yAxis, data, id, label, pen, coefficient, trigger);

      applySettings();
      return signal;
    }
    else
      return addCurve(yAxis, data, id, label, pen, coefficient, trigger);
  }
}

CYScopeSignal* CYScopePlotter::addCurve(QwtPlot::Axis yAxis, CYData *data, int id, QString label, QPen pen, double coefficient, bool trigger)
{
  CYScopeAxis *ax = scopeAxis[yAxis];
  
  if (ax && !ax->isY())
    return 0;
  
  // connexion d'une nouvelle données aux graphes
  CYScopeSignal *signal =  new CYScopeSignal(this, data, id, label, pen, coefficient, yAxis, trigger);

  Q_CHECK_PTR(signal);
  connect(signal, SIGNAL(enableCurve(int)), SLOT(enableCurve(int)));
  connect(signal, SIGNAL(disableCurve(int)), SLOT(disableCurve(int)));
  connect(signal, SIGNAL(formatUpdated()), scope(), SLOT(updateFormat()));
  
  long key = insertCurve(signal->title, xBottom, yAxis);
  QwtPlotCurve *c = curve(key);
  if (c)
  {
    c->setStyle((QwtPlotCurve::CurveStyle)mCurvesStyle->val());
    QBrush brush( Qt::NoBrush );
    brush.setColor(signal->pen.color());
    c->setBrush(brush);
    c->setPen(signal->pen);
  }
  addYSignal(id, signal);
  setLegendPosition(mLegendPosition->val());

  mTriggerSource->setChoiceLabel(key, QString("%1 : %2").arg(signal->id).arg(signal->label));
  mTriggerSource->setChoiceHelp(key, signal->title);
  if (signal->isTriggerSource())
    mTriggerSource->setVal(key);

  createHelp();
  replot();
  return signal;
}

void CYScopePlotter::addYSignal(int id, CYScopeSignal *signal)
{
  ySignal.insert(id, signal);
}

void CYScopePlotter::deleteCurve(int key, bool tmp)
{
  long i;

  if (!tmp)
  {
    CYScopeSignal *old = ySignal.at(key);
    if (!old)
      return;
    disconnect(old, SIGNAL(enableCurve(int)), this, SLOT(enableCurve(int)));
    disconnect(old, SIGNAL(disableCurve(int)), this, SLOT(disableCurve(int)));
    if (scope() && QString(scope()->objectName())!="unnamed")
      disconnect(old, SIGNAL(formatUpdated()), scope(), SLOT(updateFormat()));
  }

  CYScopeSignal *signal=0;
  if ((key+1)<ySignal.size())
    signal = ySignal.at(key + 1);
  removeCurve(key + 1);
  ySignal.removeAt(key);

  if (signal)
  {
    QString title = signal->legend();
    i = insertCurve(title);
    setCurveRawData(i, timeScopeAxis()->xTime.data(), signal->val(samplingModeVal()), size_mesure, signal->coefficient);
    // TOCHECK QWT
    //    setCurveStyle(i, mCurvesStyle->val());
    //    setCurvePen(i, signal->pen);
    QwtPlotCurve *c = curve(i);
    c->setStyle((QwtPlotCurve::CurveStyle)mCurvesStyle->val());
    c->setPen(signal->pen);
    signal->id = signal->id - 1;
    addYSignal(i - 1, signal);
    deleteCurve(key + 1, true);
  }

  if (ySignal.isEmpty())
  {
    for (int i = QwtPlot::yLeft; i < QwtPlot::axisCnt; i++)
    {
      scopeAxis[i]->init();
      setAxisTitle(i, "");
    }
  }
  replot();
  createHelp();
}

void CYScopePlotter::refresh()
{
  if (rangeX)
  {
    if (modeVal() == CYScope::Time)
    {
      refreshTimeMode();
    }
    else if (modeVal() == CYScope::XY)
    {
      refreshXYMode();
    }
  }
}

void CYScopePlotter::showEvent(QShowEvent *e)
{
  QwtPlot::showEvent(e);
}

void CYScopePlotter::hideEvent(QHideEvent *e)
{
  QwtPlot::hideEvent(e);
}

//void CYScopePlotter::setRawDataX(const double *data, uint size)
//{
//  timeScopeAxis()->xTime.setRawData(data, size);
//}

void CYScopePlotter::changeRangeX(int axis, double min, double max)
{
  rangeX = false;

  if (modeVal() == CYScope::Time)
  {
    changeTimeScale(min, max);
  }
  else if (modeVal() == CYScope::XY)
  {
    changeXScale(axis, min, max);
  }

  rangeX = true;
  setLegendPosition(mLegendPosition->val());
  refresh();
}

void CYScopePlotter::changeRangeX()
{
  rangeX = false;

  if (modeVal() == CYScope::Time)
  {
    changeTimeScale();
  }
  else if (modeVal() == CYScope::XY)
  {
    changeXScales();
  }

  rangeX = true;
  setLegendPosition(mLegendPosition->val());
  refresh();
}

void CYScopePlotter::changeRangeY(int axis, double min, double max)
{
  scopeAxis[axis]->changeYScale(min, max);
  if (isVisible())
    emit modified(true);
  refresh();
}

void CYScopePlotter::changeRangeY()
{
  scopeAxis[yLeft]->changeYScale();
  scopeAxis[yRight]->changeYScale();
  if (isVisible())
    emit modified(true);
  refresh();
}

void CYScopePlotter::setGridColor(const QColor &color)
{
  mMajGridColor->setVal(color);
  majGridPen.setColor(mMajGridColor->val());
  majGridPen.setStyle(Qt::DotLine);
  QwtPlotItemList list = itemList(QwtPlotItem::Rtti_PlotGrid);
  if (list.size()>0)
  {
    QwtPlotGrid *grid = (QwtPlotGrid *)list.first();
    grid->setMajorPen(majGridPen);
    grid->enableX(mMajGrid->val());
    grid->enableY(mMajGrid->val());
  }
}

void CYScopePlotter::setGridMinColor(const QColor &color)
{
  mMinGridColor->setVal(color);
  minGridPen.setColor(mMinGridColor->val());
  minGridPen.setStyle(Qt::DotLine);
  if (mMinGrid->val())
    majGridPen.setWidth(2);
  else
    majGridPen.setWidth(1);
  QwtPlotItemList list = itemList(QwtPlotItem::Rtti_PlotGrid);
  if (list.size()>0)
  {
    QwtPlotGrid *grid = (QwtPlotGrid *)list.first();
    grid->setMajorPen(minGridPen);
    grid->enableXMin(mMinGrid->val());
    grid->enableYMin(mMinGrid->val());
  }
}

void CYScopePlotter::setLegendPosition( const int val )
{
  mLegendPosition->setVal(val);
  if (mIsCapture)
  {
    mLegendPosition->setVal(QwtPlot::RightLegend);
    if (!mInit)  // appel après constructeur à sa propre méthode
      return;
  }

  insertLegend( NULL );
  if ((mLegendPosition->val()!=-1) && sizeEnableLegend)
  {
    insertLegend( new CYScopeLegend(this), (QwtPlot::LegendPosition)val);
  }
}

void CYScopePlotter::enableLegend(const QSize size)
{
  //   QSize minSize(400, 300);
  QSize minSize(0, 0);

  if (((size.width()) < (minSize.width()))  || ((size.height()) < (minSize.height())))
    sizeEnableLegend = false;
  else
    sizeEnableLegend = true;

  setLegendPosition(mLegendPosition->val());
}

QString CYScopePlotter::curveTitle(int curve)
{
  CYScopeSignal *signal = ySignal.at(curve);
  if (signal)
    return (signal->title);
  else
    return QString(tr("No title"));
}

QString CYScopePlotter::curveLegend(int curve)
{
  CYScopeSignal *signal = ySignal.at(curve);
  if (signal)
    return (signal->legend(true));
  else
    return QString(tr("No legend"));
}

QColor CYScopePlotter::curveColor(int curve)
{
  CYScopeSignal *signal = ySignal.at(curve);
  if (signal)
    return (signal->pen.color());
  else
    return Qt::red;
}

void CYScopePlotter::resizeEvent(QResizeEvent *e)
{
  QwtPlot::resizeEvent(e);
  enableLegend(e->size());
}

void CYScopePlotter::curveClicked(long key)
{
  CYScopeSignal *signal = ySignal.at(key - 1);
  if (!signal)
  {
    CYMESSAGE;
    return;
  }
  QMenu pm;
  QAction *labelAct = pm.addAction(tr("Change label"));
  QAction *penAct = pm.addAction(tr("Change pen"));
  QAction *coefAct = pm.addAction(tr("Change scale coefficient"));

  if (pm.exec(QCursor::pos())==labelAct)
    editCurveLabel(key - 1);

  if (pm.exec(QCursor::pos())==penAct)
    editCurvePen(key - 1);

  if (pm.exec(QCursor::pos())==coefAct)
    editCurveCoef(key - 1);
}

void CYScopePlotter::yLeftClicked()
{
  scopeAxis[yLeft]->setupScale();
}

void CYScopePlotter::yRightClicked()
{
  scopeAxis[yRight]->setupScale();
}

void CYScopePlotter::xTopClicked()
{
  scopeAxis[xTop]->setupScale();
}

void CYScopePlotter::xBottomClicked()
{
  scopeAxis[xBottom]->setupScale();
}

QColor CYScopePlotter::foregroundColor()
{
  QRgb colorVal = 0xFFFFFF-backgroundColor()->val().rgb();
  return QColor(colorVal);
}

void CYScopePlotter::createHelp()
{
  mToolTip   = "";
  if (!mScope->description().isEmpty())
    mToolTip.append(QString("<i><font color=""%1"">%2</font></i>").arg(foregroundColor().name()).arg(mScope->description()));

  QListIterator<CYScopeSignal *> it(ySignal);

  if (modeVal()==CYScope::XY)
  {
    CYScopeSignal *signal = xSignal(QwtPlot::xBottom);
    if (signal)
    {
      QString txt = "X: ";
      txt+=signal->legend(true);
      mToolTip   += "<nobr>" + QString("<i><font color=""%1"">%2</font></i>").arg(foregroundColor().name()).arg(txt) +"  "+ signal->data->infoCY(foregroundColor()) + "</nobr><br>";
    }
  }

  while (it.hasNext())
  {
    CYScopeSignal *signal = it.next();
    QString txt = QString("Y %1: ").arg(axisTitle(signal->axis()).text());
    txt+=signal->legend(true);
    mToolTip   += "<nobr>" + QString("<i><font color=""%1"">%2</font></i>").arg(signal->pen.color().name()).arg(txt) +"  "+ signal->data->infoCY(foregroundColor()) + "</nobr><br>";
  }

  QString note;
  if (!inherits("CYScopeCapture"))
  {
    QString txt = QString("<hr>" +
                          tr("If you double click on the scope a window to analyse curves will be created."));
    note += QString("<i><font color=""%1"">%2</font></i>").arg(foregroundColor().name()).arg(txt);
  }
  canvas()->setToolTip(mToolTip + note);
}

void CYScopePlotter::updateFontSize()
{
  setFontSize(fontSize());
}

void CYScopePlotter::plotMousePressed(const QMouseEvent &)
{
}

void CYScopePlotter::plotMouseReleased(const QMouseEvent &)
{
}


/*! @return le signal en X (non disponible en mode Time)
    @param axis
    \fn CYScopePlotter::xSignal()
 */
CYScopeSignal * CYScopePlotter::xSignal(int xAxis)
{
  return mXSignal[xAxis];
}


/*! Ajoute la donnée du signal d'un axe en X.
    \fn CYScopePlotter::addXSignal(CYData *data, QString label, double coefficient, QwtPlot::Axis xAxis)
 */
CYScopeSignal *CYScopePlotter::addXSignal(CYData *data, QString label, double coefficient, QwtPlot::Axis xAxis)
{
  if ((xAxis != QwtPlot::xBottom) && (xAxis != QwtPlot::xTop))
  {
    CYWARNING;
    return 0;
  }

  //   if ( (ySignal.count()>0) && (mLinksName != data->netLinkobjectName()) )
  //   {
  //     CYMessageBox::information(this, QString(tr("You must put data from the same connection of the data(s)"
  //                                                 "already displayed by this oscilloscope")));
  //     return 0;
  //   }

  if (modeVal() == CYScope::XY)
  {
    CYScopeSignal *old_signal = mXSignal[xAxis];
    if (!old_signal ||
        (mScope->oldMode() != mScope->mode()) ||
        (mScope->oldSamplingMode() != mScope->samplingMode()) ||
        ((old_signal->data->unit() != data->unit()) && (old_signal->data->physical() != data->physical()) && (old_signal->data->linksName() != data->linksName())))
    {
      //       scopeAxis[xAxis]->initX(data);
    }

    scopeAxis[xAxis]->initX(data);
    scopeAxis[xAxis]->setTitle(label);
  }
  // connexion d'une nouvelle données aux graphes
  CYScopeSignal *signal =  new CYScopeSignal(this, data, 0, label, QPen(), coefficient, xAxis);
  Q_CHECK_PTR(signal);

  disconnect(mXSignal[xAxis], SIGNAL(formatUpdated()), scope(), SLOT(updateFormat()));
  connect(signal, SIGNAL(formatUpdated()), scope(), SLOT(updateFormat()));

  mXSignal.insert(xAxis, signal);

  if (modeVal() != CYScope::XY)
    return signal;

  changeRangeX();

  setLegendPosition(mLegendPosition->val());
  replot();
  createHelp();
  return signal;
}


/*! Rafraîchisement par rapport au temps.
    \fn CYScopePlotter::refreshTimeMode()
 */
void CYScopePlotter::refreshTimeMode()
{
  if (mTriggerSignal && mTriggerSignal->triggerState())
  {
    if (timeScopeAxis()->xTime.size()>mTriggerSignal->triggerBufferCpt())
    {
      double val = timeScopeAxis()->xTime[mTriggerSignal->triggerBufferCpt()];
      setMarkerXPos(mTriggerXMarker, val );
    }
  }

  QListIterator<CYScopeSignal *> it(ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *signal = it.next();


    if ( !inherits("CYScopeCapture") )
    {
      if (signal->dataReading())
        return;
      signal->buffering();
    }
    int id = ySignal.indexOf(signal) + 1;
    if ( inherits("CYScopeCapture") )
    {
      setCurveData(id, timeScopeAxis()->xTime.data(),
                   signal->buffer(offset_mesure, samplingModeVal()).data(),
                   (size_mesure - signal->resetNb(samplingModeVal())), signal->coefficient);
    }
    else if ( triggerIsEnabled() )
    {
      setCurveData(id, timeScopeAxis()->xTime.data(),
                   signal->buffer(offset_mesure, samplingModeVal()).data(),
                   (size_mesure - signal->resetNb(samplingModeVal())), signal->coefficient);
    }
    else if (signal->isReseted(samplingModeVal()) || (core && !core->analyseRefreshEnabled()) )
    {
      setCurveData(id, timeScopeAxis()->xTime.data(),
                   signal->buffer(offset_mesure, samplingModeVal()).data(),
                   (size_mesure - signal->resetNb(samplingModeVal())), signal->coefficient);
    }
    else
    {
      setCurveRawData(id, timeScopeAxis()->xTime.data(),
                      signal->val(samplingModeVal()), size_mesure, signal->coefficient);
    }
  }
  if (inherits("CYScopeCapture"))
  {
    bool zoomOut = true;
    /*    for(QIntDictIterator<CYScopeAxis> it(scopeAxis); it.current(); ++it)
          if (!it.current()->enableZoomOut());
            zoomOut = false;*/
    emit enableActionZoomOut(zoomOut);
  }
  replot();
}


/*! Rafraîchisement par à la donnée en X (Courbe XY).
    \fn CYScopePlotter::refreshXYMode()
 */
void CYScopePlotter::refreshXYMode()
{
  refreshXYMode(xBottom);
  refreshXYMode(xTop);
  if (inherits("CYScopeCapture"))
  {
    bool zoomOut = true;
    //     for(QIntDictIterator<CYScopeAxis> it(scopeAxis); it.current(); ++it)
    //       if (!it.current()->enableZoomOut());
    //         zoomOut = false;
    emit enableActionZoomOut(zoomOut);
  }
}


/*! Rafraîchisement par à la donnée en X (Courbe XY).
    \fn CYScopePlotter::refreshXYMode(int xAxis)
 */
void CYScopePlotter::refreshXYMode(int xAxis)
{
  if (!xSignal(xAxis))
    return;

  QListIterator<CYScopeSignal *> it(ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *curve = it.next();

    if (curve->dataReading() || xSignal(xAxis)->dataReading())
      return;

    if (!inherits("CYScopeCapture"))
    {
      xSignal(xAxis)->buffering();
      curve->buffering();

      xSignal(xAxis)->isReseted(samplingModeVal());
      if (curve->isReseted(samplingModeVal()) || (core && !core->analyseRefreshEnabled()))
      {
        setCurveData(ySignal.indexOf(curve) + 1,
                     xSignal(xAxis)->buffer(offset_mesure, samplingModeVal()).data(),
                     curve->buffer(offset_mesure, samplingModeVal()).data(),
                     (size_mesure - curve->resetNb(samplingModeVal())), curve->coefficient);
      }
      else
      {
        setCurveRawData(ySignal.indexOf(curve) + 1,
                        xSignal(xAxis)->val(samplingModeVal()),
                        curve->val(samplingModeVal()),
                        size_mesure, curve->coefficient);
      }
    }
  }
  replot();
}


void CYScopePlotter::setModeVal(CYScope::Mode mode)
{
  mMode->setVal(mode);
}

CYScope::Mode CYScopePlotter::modeVal()
{
  return (CYScope::Mode)mMode->val();
}


void CYScopePlotter::setSamplingModeVal(CYScope::SamplingMode samplingMode)
{
  mSamplingMode->setVal(samplingMode);

  if (ySignal.isEmpty())
    return;

  CYData *data = ySignal.first()->data;
  if (data && (modeVal() == CYScope::Time))
  {
    if (!data->isReadFast() || (samplingModeVal() == CYScope::Continuous))
      initTimeScale(data->format()->bufferSize(!data->isReadFast()), data->format()->acquisitionTime(!data->isReadFast()));
    else
      initTimeScale(data->format()->burstsBufferSize(), data->format()->acquisitionTime(false), data->format()->burstsRatio());
  }
}

CYScope::SamplingMode CYScopePlotter::samplingModeVal()
{
  return (CYScope::SamplingMode)mSamplingMode->val();
}

/*!
    \fn CYScopePlotter::createFromDOM(QDomElement &element)
 */
bool CYScopePlotter::createFromDOM(QDomElement &element)
{
  // Connection des différents données en X.
  QDomNodeList dnList = element.elementsByTagName("xSignal");
  for (int i = 0; i < dnList.count(); ++i)
  {
    QDomElement el = dnList.item(i).toElement();
    CYData *d = core->db.value(el.attribute("dataName"));
    mScope->addDataX(d, el.attribute("dataLabel"), el.attribute("coefficient","1.0").toDouble(), (QwtPlot::Axis)el.attribute("axis").toInt());
    CYScopeSignal *signal = mScope->addDataX(d, el.attribute("dataLabel"), el.attribute("coefficient","1.0").toDouble(), (QwtPlot::Axis)el.attribute("axis").toInt());
    if (signal)
      signal->setTriggerSource(( bool )el.attribute( "trigger", "0" ).toUInt());
  }
  return true;
}


/*!
    \fn CYScopePlotter::addToDOM(QDomDocument &doc, QDomElement &element)
 */
void CYScopePlotter::addToDOM(QDomDocument &doc, QDomElement &element)
{
  QHashIterator<int, CYScopeSignal*> it(mXSignal);
  while (it.hasNext())
  {
    it.next();
    CYScopeSignal *signal=it.value();
    if (!signal)
      continue;
    QDomElement xSignal = doc.createElement("xSignal");
    element.appendChild(xSignal);
    xSignal.setAttribute("axis", (uint)it.key());
    if (signal->data)
    {
      xSignal.setAttribute("dataName", signal->data->objectName());
      if (signal->label != signal->data->label())
        xSignal.setAttribute("dataLabel", signal->label);
    }
    xSignal.setAttribute("coefficient", signal->coefficient);
    xSignal.setAttribute( "trigger", signal->isTriggerSource() );
  }
}


/*! RAZ des courbes
    \fn CYScopePlotter::resetCurves()
 */
void CYScopePlotter::resetCurves()
{
  if (xSignal(QwtPlot::xBottom))
    xSignal(QwtPlot::xBottom)->reset();
  QListIterator<CYScopeSignal *> it(ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *curve = it.next();
    curve->reset();
  }
  //refresh();// A_VOIR
}


/*!
    \fn CYScopePlotter::acquisition()
 */
CYScopeAcquisition * CYScopePlotter::acquisition()
{
  return mAcquisition;
}


/*! Exporter les courbes dans un fichier texte
    @param saveAs Par défaut vaut à \a true pour avoir la boîte de sélection du fichier, pour avoir automatiquement un nom de fichier il suffit de le mettre à \a false.
    \fn CYScopePlotter::exportCurves(bool saveAs)
 */
void CYScopePlotter::exportCurves(bool saveAs)
{
  exportCurves(saveAs, true, true);
}

/*! Exporter les courbes dans un fichier texte
  *  Enrichissement de \fn CYScopePlotter::exportCurves(bool saveAs) pour exporter l'acquisition déjà capturée.
    @param saveAs     Saisir \a true pour avoir la boîte de sélection du fichier et \a false pour avoir automatiquement un nom de fichier .
    @param newCapture Saisir \a true pour faire capturer avant d'exporter et \a false pour utiliser la dernière capture.
    @param open       Ouverture automatique.
    \fn CYScopePlotter::exportCurves(bool saveAs, bool newCapture, bool open)
 */
void CYScopePlotter::exportCurves(bool saveAs, bool newCapture, bool open)
{
  refresh();
  acquisition()->exportCurves(saveAs, newCapture);
  QString fileName = acquisition()->fileName();
  if (open)
  {
    QUrl url = UrlFromUserInput(fileName);
    if (!QDesktopServices::openUrl(url))
    {
      CYMessageBox::error(0, tr("Can't open %1").arg(url.fileName()));
      return;
    }
  }
}

bool CYScopePlotter::capturing()
{
  return (mDiscontDataState>0) ? true : false;
}

/*! Initiallise la base de données de l'afficheur.
    \fn CYScopePlotter::initDB()
 */
void CYScopePlotter::initDB()
{
  //   if (mInitDB)
  //     return;

  if (!mDB)
  {
    mLocalDB = true;
    if (!core)
      mDB = new CYDB(new CYNetHost(this, QString("Host_%1").arg(objectName())), QString("DB_%1").arg(objectName()), -1, 0, false);
    else
      mDB = new CYDB(core->localHost(), QString("DB_%1").arg(objectName()), -1, 0, false);
  }
  mDB->setHelpDesignerValue(false);
  mDB->setGroup(tr("Scope"));

  QHashIterator<int, CYScopeAxis*> it(scopeAxis);
  while (it.hasNext())
  {
    it.next();
    it.value()->initDB();
  }

  mDB->setUnderGroup(0);

  mMode = new CYU8(mDB, "MODE", new u8);
  mMode->setLabel(tr("Use mode"));
  mMode->setChoiceLabel(CYScope::Time, tr("Time"));
  mMode->setChoiceLabel(CYScope::XY  , tr("XY"));

  mSamplingMode = new CYU8(mDB, "SAMPLING_MODE", new u8);
  mSamplingMode->setLabel(tr("Sampling mode"));
  mSamplingMode->setChoiceLabel(CYScope::Continuous, tr("Continuous"));
  mSamplingMode->setChoiceLabel(CYScope::Bursts    , tr("In bursts"));

  mDB->setUnderGroup(tr("Trigger"));

  mTriggerSource = new CYU8(mDB, "TRIGGER_SIGNAL", new u8);
  mTriggerSource->setLabel(tr("Signal"));
  mTriggerSource->setChoiceLabel(0, tr("Nothing"));
  mTriggerSource->setChoiceHelp(0, tr("Disable trigger"));

  mTriggerLevel = new CYF64(mDB, "TRIGGER_LEVEL", new f64, 1.0, -1e7, 1e7);
  mTriggerLevel->setAutoMinMaxFormat( false );
  mTriggerLevel->setLabel(tr("Level"));

  mPostTrigger = new CYF64(mDB, "POST_TRIGGER", new f64, 25.0, 0.0, 99.0);
  mPostTrigger->setUnit("%");
  mPostTrigger->setAutoMinMaxFormat( false );
  mPostTrigger->setLabel(tr("Post-Trigger"));

  mTriggerSlope = new CYS16(mDB, "TRIGGER_SLOPE", new s16);
  mTriggerSlope->setLabel(tr("Slope"));
  QString path;// = QString("%1/cylibs/pics/").arg(KGlobal::dirs()->findResourceDir("data", "cylibs/pics/flag_false.png")).ascii();
  //  mTriggerSlope->setChoice(None    , QPixmap(QString(path+"flag_false.png"))      , tr("No")      , tr("No backup"));
  mTriggerSlope->setChoice(Cy::Rising  , QPixmap(":/icons/cyedge_rising.png")   , tr("Rising")  , tr("Trigger over the level."));
  mTriggerSlope->setChoice(Cy::Falling , QPixmap(":/icons/cyedge_falling.png")  , tr("Falling") , tr("Trigger below the level."));

  mDB->setUnderGroup(tr("Style"));

  mCurvesStyle = new CYU8(mDB, "CURVES_STYLE", new u8,QwtPlotCurve::Lines);
  mCurvesStyle->setLabel(tr("Curves style"));
  mCurvesStyle->setDef(QwtPlotCurve::Lines);
  mCurvesStyle->setChoiceLabel(QwtPlotCurve::NoCurve, tr("No Curve"));
  mCurvesStyle->setChoiceLabel(QwtPlotCurve::Lines  , tr("Lines"));
  mCurvesStyle->setChoiceLabel(QwtPlotCurve::Sticks , tr("Sticks"));
  mCurvesStyle->setChoiceLabel(QwtPlotCurve::Steps  , tr("Steps"));
  mCurvesStyle->setChoiceLabel(QwtPlotCurve::Dots   , tr("Dots"));

  mCurvesWidth = new CYU32(mDB, "CURVES_WIDTH", new u32, 1, 1, 50);
  mCurvesWidth->setLabel(tr("Curves width"));

  mBackgroundColor = new CYColor(mDB, "BACKGROUND_COLOR", new QColor(Qt::black));
  mBackgroundColor->setLabel(tr("Background color"));

  mMajGrid = new CYFlag(mDB, "MAJ_GRID", new flg, true);
  mMajGrid->setLabel(tr("Major gridlines"));

  mMinGrid = new CYFlag(mDB, "MIN_GRID", new flg, false);
  mMinGrid->setLabel(tr("Minor gridlines"));

  mMajGridColor = new CYColor(mDB, "MAJ_GRID_COLOR", new QColor(Qt::gray));
  mMajGridColor->setLabel(tr("Major gridlines color"));

  mMinGridColor = new CYColor(mDB, "MIN_GRID_COLOR", new QColor(Qt::darkGray));
  mMinGridColor->setLabel(tr("Minor gridlines color"));

  mLegendPosition = new CYS16(mDB, "POSITION_LEGEND");
  mLegendPosition->setChoiceLabel(-1, tr("Hide legend"));
  mLegendPosition->setChoiceLabel(QwtPlot::LeftLegend, tr("Left legend"));
  mLegendPosition->setChoiceLabel(QwtPlot::RightLegend, tr("Right legend"));
  mLegendPosition->setChoiceLabel(QwtPlot::BottomLegend, tr("Bottom legend"));
  mLegendPosition->setChoiceLabel(QwtPlot::TopLegend, tr("Top legend"));
  mLegendPosition->setDef(QwtPlot::BottomLegend);
  mLegendPosition->designer();
  mLegendPosition->setLabel(tr("Legend position"));

  mDB->setUnderGroup(0);

  mDB->setUnderGroup(tr("Acquisition"));

  mAcquisitionEnable = new CYFlag(mDB, "ACQUISITION_ENABLE", new flg);
  mAcquisitionEnable->setLabel(tr("Enable acquisition"));

  mAcquisition = new CYScopeAcquisition(mScope, QString("%1_ACQUISITION").arg(objectName()), mAcquisitionEnable);

  mDB->setUnderGroup(0);

  mInitDB = true;
}

/*! Mise à jour du format
    \fn CYScopePlotter::updateFormat()
 */
void CYScopePlotter::updateFormat()
{
  // TODO CYScopePlotter::updateFormat(): analyser la mise en commentaire suite à la charge CPU avec les trames LIN 1099814
//  // Fond.
//  canvas()->setStyleSheet(QString("background-color:%1;").arg(mBackgroundColor->val().name()));

//  setFontSize(fontSize());

//  // Grille secondaire.
//  setGridMinColor(mMinGridColor->val());

//  // Grille principale.
//  setGridColor(mMajGridColor->val());

//  // Affichage des légendes.
//  setLegendPosition(mLegendPosition->val());

//  // détection donnée en acquisition discontinue
//  mDiscontData = false;

  // Echelle de l'axe bas.
//  changeRangeX();
  //   QwtPlot::enableAxis(QwtPlot::xBottom, mBottomShow->val());

  // Echelle de l'axe gauche.
//  changeRangeY();

//  // Ajuste la taille de la police des titres des axes.
//  updateFontSize();

//  updateCurvesStyle();
//  updateCurvesPen();
//  updateLayout();
}


/*! Applique les paramètres
    \fn CYScopePlotter::applySettings()
 */
void CYScopePlotter::applySettings()
{
  // Fond.
  canvas()->setStyleSheet(QString("background-color:%1;").arg(mBackgroundColor->val().name()));

  setFontSize(fontSize());

  // Grille secondaire.
  setGridMinColor(mMinGridColor->val());

  // Grille principale.
  setGridColor(mMajGridColor->val());

  // Affichage des légendes.
  setLegendPosition(mLegendPosition->val());

  // détection donnée en acquisition discontinue
  mDiscontData = false;
  //  QListIterator<CYScopeSignal *> it(ySignal);
  //  while (it.hasNext())
  //  {
  //    CYScopeSignal *signal = it.next();

  //    CYData *data = signal->data;
  //    if (data->discontBufferIT())
  //    {
  //      mDiscontData = true;
  //      break;
  //    }
  //  }
  // Echelle de l'axe bas.
  changeRangeX();
  //   QwtPlot::enableAxis(QwtPlot::xBottom, mBottomShow->val());

  // Echelle de l'axe gauche.
  changeRangeY();

  // Ajuste la taille de la police des titres des axes.
  updateFontSize();

  updateCurvesStyle();
  updateCurvesPen();
  updateLayout();
}


/*! Met à jour le style des courbes
    \fn CYScopePlotter::updateCurvesStyle()
 */
void CYScopePlotter::updateCurvesStyle()
{
  QListIterator<CYScopeSignal *> it(ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *signal = it.next();
    QwtPlotCurve *c = curve(signal->id);
    c->setStyle((QwtPlotCurve::CurveStyle)mCurvesStyle->val());
  }
}


/*! Met à jour le crayon des courbes
    \fn CYScopePlotter::updateCurvesPen()
 */
void CYScopePlotter::updateCurvesPen()
{
  QListIterator<CYScopeSignal *> it(ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *signal = it.next();
    QwtPlotCurve *c = curve(signal->id);
    c->setPen(signal->pen);
    /*    setCurvePen(signal->id, QPen(signal->color, mCurvesWidth->val()));*/
  }
}


/*! Active une courbe
    \fn CYScopePlotter::enableCurve( int key )
 */
void CYScopePlotter::enableCurve(int key)
{
  QwtPlotCurve *c = curve(key);
  if (c)
    c->setVisible(true);
}

/*! Désactive une courbe
    \fn CYScopePlotter::disableCurve( int key )
 */
void CYScopePlotter::disableCurve(int key)
{
  QwtPlotCurve *c = curve(key);
  if (c)
    c->setVisible(false);
}


/*! @return la taille de police
    \fn CYScopePlotter::fontSize()
 */
int CYScopePlotter::fontSize()
{
  return mScope->fontSize();
}


/*! Saisie la taille de police
    \fn CYDisplay::setFontSize(int val)
 */
void CYScopePlotter::setFontSize(int val)
{
  mScope->setFontSize(val);

  // Axes
  QHashIterator<int, CYScopeAxis*> it(scopeAxis);
  while (it.hasNext())
  {
    it.next();
    it.value()->setFontSize(val);
  }
}


/*! Edition du libellé de la courbe \a curve
    \fn CYScopePlotter::editCurveLabel( int curve )
 */
void CYScopePlotter::editCurveLabel(int curve)
{
  CYScopeSignal *signal = ySignal.at(curve);
  if (signal)
  {
    bool ok;
    QString text = QInputDialog::getText(this, tr("Data label"), tr("Enter the new label: "), QLineEdit::Normal, signal->label, &ok);

    if (ok && !text.isEmpty())
    {
      QwtPlotCurve *c = CYPlot::curve(signal->id);
      c->setTitle(signal->legend());

      if (isVisible())
        emit modified(true);

      if (mScope)
        mScope->saveSettings();
    }
  }
}


/*! Edition du crayon de la courbe \a curve
    \fn CYScopePlotter::editCurvePen( int curve )
 */
void CYScopePlotter::editCurvePen(int curve)
{
  CYScopeSignal *signal = ySignal.at(curve);
  if (signal)
  {
    CYPen *pen = new CYPen(new CYDB(this), "PEN", signal->label, signal->pen);
    CYPenDialog *dlg = new CYPenDialog(this);
    dlg->setPen(pen);
    if (dlg->exec())
    {
      signal->pen = pen->pen();
      QwtPlotCurve *c = CYPlot::curve(signal->id);
      c->setPen(signal->pen);
      replot();

      if (isVisible())
        emit modified(true);

      if (mScope)
        mScope->saveSettings();
    }
  }
}


/*! Edition du coefficient d'échelle de la courbe \a curve
    \fn CYScopePlotter::editCurveCoef( int curve )
 */
void CYScopePlotter::editCurveCoef(int curve)
{
  bool ok;
  double limit = 1000000;

  CYScopeSignal *signal = ySignal.at(curve);
  if (signal)
  {
    double res = QInputDialog::getDouble(this, tr("Display coefficient"), tr("Enter the new display coefficient: "), signal->coefficient, -limit, limit, 2, &ok );
    if (ok)
    {
      if (res==0.0)
      {
        CYMessageBox::sorry(this, tr("You must enter a coefficient display different of 0 !"));
        return editCurveCoef(curve);
      }
      else
      {
        setYCoef(signal, res);
        replot();

        if (isVisible())
          emit modified(true);

        if (mScope)
          mScope->saveSettings();
      }
    }
  }
}


/*! Garde en mémoire les valeurs des échelles.
    \fn CYScopePlotter::backupScaleValues()
 */
void CYScopePlotter::backupScaleValues()
{

}


CYScopeAxis * CYScopePlotter::timeScopeAxis()
{
  return scopeAxis[mTimeAxis];
}

void CYScopePlotter::setYCoef( CYScopeSignal *signal, double coef )
{
  signal->coefficient = coef;
  QwtPlotCurve *c = curve(signal->id);
  if (c)
    c->setYCoef(coef);
  c->setTitle(signal->legend());
  createHelp();
}

void CYScopePlotter::setYOffset( CYScopeSignal *signal, double offset )
{
  signal->offset = offset;
  QwtPlotCurve *c = curve(signal->id);
  if (c)
    c->setYOffset(offset);
  c->setTitle(signal->legend());
  createHelp();
}

/*! @return \a true s'il s'agit d'un oscilloscope aux multiples formats
    \fn CYScopePlotter::hasMultipleFormats()
 */
bool CYScopePlotter::hasMultipleFormats()
{
  return mScope->hasMultipleFormats();
}



bool CYScopePlotter::triggerIsEnabled()
{
  return (mTriggerEnabled && (mTriggerSource->val()!=0)) ? true : false;
}

void CYScopePlotter::clearTriggerSignals()
{
  mTriggerSource->choiceDict.clear();
  mTriggerSource->setChoiceLabel(0, tr("Nothing"));
  mTriggerSource->setChoiceHelp(0, tr("Disable trigger"));
}

void CYScopePlotter::setTriggerSource(CYScopeSignal * signal)
{
  mTriggerSignal = signal;

  QListIterator<CYScopeSignal *> it(ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *curve = it.next();
    curve->setTriggerSignal(signal);
  }
}


void CYScopePlotter::setTriggerFormat(CYFormat *format)
{
  mTriggerLevel->setFormat(format);
}

void CYScopePlotter::setupTrigger()
{
  CYScopeTrigger *dlg = new CYScopeTrigger(this);
  dlg->setScope(mScope);
  if (dlg->exec())
  {
    if (mTriggerSource->val())
      enableTrigger(true);
    else
      enableTrigger(false);
  }
}

void CYScopePlotter::enableTrigger(bool val)
{
  if (!mTriggerEnabled && !val)
    return;

  if (mTriggerEnabled && val) // reset
  {
    removeMarker(mTriggerYMarker);
    removeMarker(mTriggerXMarker);
  }

  if (val) // activation du trigger
  {
    if (mTriggerSource->val()==0)
    {
      QString msg = tr("No trigger signal!\nDo you want to configure it?");
      int res = CYMessageBox::warningYesNo(this, msg, tr("Activation of the trigger"));
      if (res==CYMessageBox::Yes)
        mScope->setupTrigger();
      else
        enableTrigger(false);
    }
    if (mTriggerSource->val()!=0) // recherche du signal source
    {
      CYScopeSignal *signalSource = ySignal.at(mTriggerSource->val()-1);
      if (!signalSource)
      {
        CYMessageBox::error(this, tr("The source signal of the trigger was not found!"));
        mTriggerEnabled = false;
        return;
      }
      //       else if (signalSource->isTriggerSource())
      //       {
      //         CYMessageBox::error(this, TRFR("Le signal source du trigger n'est pas défini en tant que tel!"));
      //         return;
      //       }
      QListIterator<CYScopeSignal *> it(ySignal);
      while (it.hasNext())
      {
        CYScopeSignal *signal = it.next();
        signal->setTriggerSignal(signalSource);
      }
      setTriggerSource(signalSource);
      removeMarker(mTriggerYMarker);
      removeMarker(mTriggerXMarker);
      double post_trigger = 100.0-(mPostTrigger->val()*timeScopeAxis()->xTime.size()/signalSource->bufferSize());
      signalSource->startTrigger(mTriggerLevel->val(), post_trigger, (Cy::Change)mTriggerSlope->val());
      mTriggerYMarker = insertLineMarker(tr("Trigger"), QwtPlot::yLeft);
      mTriggerXMarker = insertLineMarker(tr("Trigger"), QwtPlot::xBottom);
      QPen pen = signalSource->pen;
      pen.setStyle(Qt::DashDotLine);
      setMarkerLinePen(mTriggerXMarker, pen);
      setMarkerLinePen(mTriggerYMarker, pen);
      setMarkerYPos(mTriggerYMarker, mTriggerLevel->val() );
      mTriggerEnabled = true;
    }
  }
  else // désactivation du trigger
  {
    QListIterator<CYScopeSignal *> it(ySignal);
    while (it.hasNext())
    {
      CYScopeSignal *signal = it.next();
      signal->stopTrigger();
    }
    removeMarker(mTriggerYMarker);
    removeMarker(mTriggerXMarker);
    mTriggerSignal = 0;
    mTriggerEnabled = false;
  }
}

void CYScopePlotter::resetTrigger()
{

}

void CYScopePlotter::mouseDoubleClickEvent(QMouseEvent * e)
{
  if (e->button()==Qt::LeftButton)
    emit mouseDoubleClick();

  QwtPlot::mouseDoubleClickEvent(e);
}

void CYScopePlotter::setEnableBuffering(bool val)
{
  QListIterator<CYScopeSignal *> it(ySignal);
  while (it.hasNext())
  {
    CYScopeSignal *signal = it.next();
    signal->setEnableBuffering(val);
  }
}
