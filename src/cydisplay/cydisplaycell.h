/***************************************************************************
                          cydisplaycell.h  -  description
                             -------------------
    begin                : mer jan 26 2005
    copyright            : (C) 2005 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDISPLAYCELL_H
#define CYDISPLAYCELL_H

// QT
#include <qlayout.h>
//Added by qt3to4:
#include <QEvent>
#include <QResizeEvent>
#include <QGridLayout>
#include <QDropEvent>
#include <QDragEnterEvent>
// CYLIBS
#include "cyframe.h"

class CYDisplay;

/** @short Cellule contenant un afficheur.
  * @author LE CLÉACH Gérald
  */

class CYDisplayCell : public CYFrame
{
  Q_OBJECT
public:
  /** Constructeur de la cellule.
    * @param parent parent de la cellule.
    * @param name   nom de la cellule. */
  CYDisplayCell(QWidget *parent, const QString &name);

  ~CYDisplayCell();

  /** Place l'afficheur dans la cellule.
    * @param  name  Nom de la donnée / ou du groupe de données. */
  virtual void setDisplay(const QString& name);
  /** Place l'afficheur dans la cellule. */
  virtual void setDisplay(CYDisplay *display=0);

  /** Construit un afficheur dans la cellule à partir d'un élément DOM. */
  virtual bool createDisplayFromDOM(QDomElement &el);
  /** Ajoute un afficheur dans la cellule à partir d'un élément DOM. */
  virtual bool addDisplayToDOM(QDomDocument &doc, QDomElement &element, bool save=true);
  /** Active ou désactive les connexions de modifications. */
  virtual void connectModifie(bool enable);
  /** Création d'un afficheur à partir du presse-papier. */
  virtual void clipboardDisplay();

public slots: // Public slots
  /** Démarre le rafraîchissement. */
  virtual void startRefresh();
  /** Arrête le rafraîchissement. */
  virtual void stopRefresh();

public: // Public methods
  /** @return \a true si la cellule est en mode forçage. */
  virtual bool forcing() const;
  /** Saisir \a true pour activer le mode forçage. */
  virtual void setForcing(const bool val);

public slots: // Public slots
  /** Coupe l'afficheur. */
  virtual void cutDisplay();
  /** Copie l'afficheur. */
  virtual void copyDisplay();
  /** Colle un afficheur. */
  virtual void pasteDisplay();
  /* Construit une description XML de la cellule. */
  virtual QString asXML();
  /* Construit une description XML de l'afficheur. */
  virtual QString displayAsXML();
  /** Supprime l'afficheur. */
  virtual void removeDisplay();
  /** Saisie l'état de modification de la cellule. */
  virtual void setModified(bool state);
  /** Permet ou non de signaler une modification lors d'un redimensionnement de la cellule. */
  virtual void setModifieOnResize(bool val) { mModifieOnResize = val; }
  /** @return l'afficheur que contient la cellule. */
  virtual CYDisplay * display();

protected: // Protected methods
  /** Gestion de l'entée de souris dans le widget lors d'un glisser. */
  virtual void dragEnterEvent(QDragEnterEvent* ev);
  /** Gestion du déposer d'un glisser-déposer. */
  virtual void dropEvent(QDropEvent* ev);
  /** Gestion d'un QCustomEvent. */
  virtual void customEvent(QEvent* ev);
  /** Appelé à chaque changement de taille de la cellule. */
  virtual void resizeEvent ( QResizeEvent * e);

private: // Private methods
  /** Fonction d'initialisation. */
  void init();

protected: // Protected attributes
  /** Positionnement. */
  QGridLayout *mGrid;
  //QHBoxLayout *mCellLayout;
  /** Afficheur. */
  CYDisplay *mDisplay;
  /** En cours de copie d'afficheur. */
  bool mDisplayCopying;
  /** En cours de collage d'afficheur. */
  bool mDisplayPasting;
  /** Connexions de modifications active/désactive . */
  bool mConnectModifie;
  /** Permet de signaler une modification lors d'un redimensionnement de la cellule. */
  bool mModifieOnResize;
  /** Mode forçage. */
  bool mForcing;
};

#endif
