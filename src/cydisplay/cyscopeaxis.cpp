//
// C++ Implementation: cyscopeaxis
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
// ANSI
#include <stdlib.h>
// QT
#include <QMouseEvent>
#include <QVector>
// QWT
#include <qwt_scale_engine.h>
#include <qwt_scale_widget.h>
// CYLIBS
#include "cycore.h"
#include "cyscopeaxis.h"
#include "cydb.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cyscopescaledialog.h"
#include "cyscopeaxissetup.h"

CYScopeAxis::CYScopeAxis(CYScopePlotter *parent, QwtPlot::Axis axis)
 : QObject(parent),
   mType(axis),
   mPlotter(parent)
{
  mTime=0;
  mRight=0;
  mLeft=0;
  mAutoScale=0;
  mIsTimeScale = false;
  mR2L=true;
  init();
  connect(this, SIGNAL( enableActionZoomIn(bool)),  mPlotter, SIGNAL( enableActionZoomIn(bool)));
  connect(this, SIGNAL( enableActionZoomOut(bool)),  mPlotter, SIGNAL( enableActionZoomOut(bool)));
}


CYScopeAxis::~CYScopeAxis()
{
}

void CYScopeAxis::init()
{
  mInit = false;
  mLinksName = "";
	mNumFormat = 0;
  mScopeCapture = 0;
  mNbStep = -1;

  mCursors.clear();

  switch (mType)
  {
    case QwtPlot::yLeft   : mDesc = tr("Left axis");   setObjectName("LEFT");   break;
    case QwtPlot::yRight  : mDesc = tr("Right axis");  setObjectName("RIGHT");  break;
    case QwtPlot::xBottom : mDesc = tr("Bottom axis"); setObjectName("BOTTOM"); break;
    case QwtPlot::xTop    : mDesc = tr("Top axis");    setObjectName("TOP");    break;
    default               : CYFATAL;
  }
}

/*! Initiallise la base de données de l'axe.
    \fn CYScopeAxis::initDB()
 */
void CYScopeAxis::initDB()
{
  CYDB *db = mPlotter->db();

  mInitX = mInitY = false;
  db->setUnderGroup(mDesc);

  mAutoScale = new CYFlag(db, QString("%1_AUTO_SCALE").arg(objectName()), new flg, false);
  mAutoScale->setLabel(tr("Auto scale"));
  mAutoScale->setChoiceTrue(tr("Auto scale"));
  mAutoScale->setChoiceFalse(tr("Scale settable"));

  mAutoScaleHide = new CYFlag(db, QString("%1_AUTO_SCALE_HIDE").arg(objectName()), new flg, false);
  mAutoScaleHide->setLabel(tr("Hide the auto-scale option"));

  if (( mType==QwtPlot::yLeft ) || ( mType==QwtPlot::yRight ))
  {
    db->changeDataName( QString("MAX_%1").arg(objectName()), QString("%1_MAX").arg(objectName()) );
    mMax = new CYF64(db, QString("%1_MAX").arg(objectName()), new f64, 100.0);
    mMax->setAutoMinMaxFormat( false );
    mMax->setLabel(tr("Maximum value"));

    db->changeDataName( QString("MIN_%1").arg(objectName()), QString("%1_MIN").arg(objectName()) );
    mMin = new CYF64(db, QString("%1_MIN").arg(objectName()), new f64,   0.0);
    mMin->setAutoMinMaxFormat( false );
    mMin->setLabel(tr("Minimum value"));
  }
  else if (( mType==QwtPlot::xBottom ) || ( mType==QwtPlot::xTop ))
  {
    db->changeDataName( QString("MAX_%1").arg(objectName()), QString("LEFT_%1").arg(objectName()) );
    mLeft = new CYF64(db, QString("LEFT_%1").arg(objectName()) , new f64);
    mLeft->setLabel(tr("Left value"));
    db->changeDataName( QString("MIN_%1").arg(objectName()), QString("RIGHT_%1").arg(objectName()) );
    mRight = new CYF64(db, QString("RIGHT_%1").arg(objectName()) , new f64);
    mRight->setLabel(tr("Right value"));

    mTime = new CYF64(db, QString("%1_TIME").arg(objectName()), new f64);
  }

  mShow = new CYFlag(db, QString("%1_SHOW").arg(objectName()), new flg, true);
  mShow->setLabel(tr("Shown"));
  mShow->setChoiceTrue(tr("Show axis"));
  mShow->setChoiceFalse(tr("Hide axis"));

  db->changeDataName( QString("LOG_%1").arg(objectName()), QString("%1_LOG").arg(objectName()) );
  mLog = new CYFlag(db, QString("%1_LOG").arg(objectName()), new flg);
  mLog->setLabel(tr("Logarithmic scale"));
  mLog->setChoiceTrue(tr("Enable logarithmic scale"));
  mLog->setChoiceFalse(tr("Disable logarithmic scale"));

  mEnable = new CYFlag(db, QString("%1_ENABLE").arg(objectName()), new flg, false);
  mEnable->setVolatile(true);
  mEnable->setLabel(tr("Enabled"));
  mEnable->setChoiceTrue(tr("Axis enabled"));
  mEnable->setChoiceFalse(tr("Axis disabled"));

  CYF64 *df64;

  df64 = new CYF64(db, QString("%1_CURSOR1").arg(objectName()), new f64);
  mCursors.insert(CYScopeCapture::Cursor1, df64);

  df64 = new CYF64(db, QString("%1_CURSOR2").arg(objectName()), new f64);
  mCursors.insert(CYScopeCapture::Cursor2, df64);

  df64 = new CYF64(db, QString("%1_DELTA_CURSOR").arg(objectName()), new f64);
  mCursors.insert(CYScopeCapture::Delta, df64);

  df64 = new CYF64(db, QString("%1_INV_DELTA_CURSOR").arg(objectName()), new f64);
  mCursors.insert(CYScopeCapture::IDelta, df64);
}


/*! Copie des paramètres de l'axe de l'oscilloscope \a s.
    \fn CYScopeAxis::copy(CYScopePlotter *s)
 */
void CYScopeAxis::copy(CYScopePlotter *s)
{
  CYScopeAxis *src = s->scopeAxis[mType];

  mLinksName = src->linksName();
  mIsTimeScale = src->isTimeScale();

  if (mAutoScale)
    mAutoScale->setVal(src->mAutoScale->val());

  mLog->setVal(src->mLog->val());
  mShow->setVal(src->mShow->val());

  if (( mType==QwtPlot::yLeft ) || ( mType==QwtPlot::yRight ))
  {
		mInitY = true;
		mNumFormat = src->mMin->numFormat();

    mMin->setFormat(src->mMin->format());
    mMin->setVal(src->mMin->val());
    mMin->setDef(src->mMin->def());
    mMin->setMin(src->mMin->min());
    mMin->setMax(src->mMin->max());

    mMax->setFormat(src->mMax->format());
    mMax->setVal(src->mMax->val());
    mMax->setDef(src->mMax->def());
    mMax->setMin(src->mMax->min());
    mMax->setMax(src->mMax->max());
    mPlotter->changeRangeY();
  }
  else if (( mType==QwtPlot::xBottom ) || ( mType==QwtPlot::xTop ))
  {
		mInitX = true;
		mNumFormat = src->mLeft->numFormat();

    mLeft->setFormat(src->mLeft->format());
    mLeft->setVal(src->mLeft->val());
    mLeft->setDef(src->mLeft->def());
    mLeft->setMin(src->mLeft->min());
    mLeft->setMax(src->mLeft->max());

    mRight->setFormat(src->mRight->format());
    mRight->setVal(src->mRight->val());
    mRight->setDef(src->mRight->def());
    mRight->setMin(src->mRight->min());
    mRight->setMax(src->mRight->max());
    mPlotter->changeRangeX();
  }

  mTitle = src->mTitle;
  mUnit = src->mUnit;
  if ( mUnit.isEmpty() )
    mPlotter->setAxisTitle(mType, mTitle );
  else
    mPlotter->setAxisTitle(mType, mTitle + " (" + mUnit + ")");

  mPlotter->enableAxis(mType, isEnabled());
}

CYF64 *CYScopeAxis::minData()
{
  if (( mType==QwtPlot::xBottom ) || ( mType==QwtPlot::xTop ))
    return ( mLeft->val() < mRight->val() ) ? mLeft : mRight ;
  else return mMin;
}

CYF64 *CYScopeAxis::maxData()
{
  if (( mType==QwtPlot::xBottom ) || ( mType==QwtPlot::xTop ))
    return ( mLeft->val() > mRight->val() ) ? mLeft : mRight ;
  else
    return mMax;
}

double CYScopeAxis::minVal()
{
  return minData()->val();
}

void CYScopeAxis::setMinVal(double val)
{
  minData()->setVal(val);
}

double CYScopeAxis::maxVal()
{
  return maxData()->val();
}

void CYScopeAxis::setMaxVal(double val)
{
  maxData()->setVal(val);
}

void CYScopeAxis::setTitle(const QString &text)
{
  if (mPlotter->axisTitle(mType) == text)
    return;
  mTitle = QString(text);
  mPlotter->setAxisTitle(mType, mTitle);
  mPlotter->QwtPlot::enableAxis(mType, false);
  mPlotter->QwtPlot::enableAxis(mType, isEnabled());

}

/*!
    \fn CYScopeAxis::updateOptions()
 */
void CYScopeAxis::updateOptions()
{
  if (( mType==QwtPlot::yLeft ) || ( mType==QwtPlot::yRight ))
  {
    if (mLog->val())
    {
      QwtLogScaleEngine *scale = new QwtLogScaleEngine;
      scale->setAttributes(QwtScaleEngine::Floating);
      mPlotter->setAxisScaleEngine(mType, scale);
    }
    else
    {
      QwtLinearScaleEngine *scale = new QwtLinearScaleEngine;
      scale->setAttributes(QwtScaleEngine::Floating);
      mPlotter->setAxisScaleEngine(mType, scale);
    }
  }
  else if (( mType==QwtPlot::xBottom ) || ( mType==QwtPlot::xTop ))
  {
    if ( mLog->val() )
    {
      if ( mLeft->val() > mRight->val() )
      {
        QwtLogScaleEngine *scale = new QwtLogScaleEngine;
        scale->setAttributes(QwtScaleEngine::Inverted|QwtScaleEngine::Floating);
        mPlotter->setAxisScaleEngine(mType, scale);
      }
      else
      {
        QwtLogScaleEngine *scale = new QwtLogScaleEngine;
        scale->setAttributes(QwtScaleEngine::Floating);
        mPlotter->setAxisScaleEngine(mType, scale);
      }
    }
    else
    {
      if ( mLeft->val() > mRight->val() )
      {
        QwtLinearScaleEngine *scale = new QwtLinearScaleEngine;
        scale->setAttributes(QwtScaleEngine::Inverted|QwtScaleEngine::Floating);
        mPlotter->setAxisScaleEngine(mType, scale);
      }
      else
      {
        QwtLinearScaleEngine *scale = new QwtLinearScaleEngine;
        scale->setAttributes(QwtScaleEngine::Floating);
        mPlotter->setAxisScaleEngine(mType, scale);
      }
    }
  }
  if (mAutoScale && mAutoScale->val())
  {
    mPlotter->setAxisAutoScale(mType);
  }
}

void CYScopeAxis::setLog(bool val)
{
  if (( mType==QwtPlot::yLeft ) || ( mType==QwtPlot::yRight ))
  {
    if (val)
    {
      maxData()->setExponential(true);
      maxData()->setMax(1.0e50);
      maxData()->setMin(1.0e-50);
      minData()->setExponential(true);
      minData()->setMax(1.0e50);
      minData()->setMin(1.0e-50);
    }
    else
    {
      maxData()->setExponential(false);
      maxData()->initMinMax();
      minData()->setExponential(false);
      minData()->initMinMax();
    }
  }
  else if (( mType==QwtPlot::xBottom ) || ( mType==QwtPlot::xTop ))
  {
    if ( val )
    {
      if (mPlotter && mPlotter->modeVal()==CYScope::XY)
      {
        maxData()->setMax(1.0e50);
        maxData()->setMin(1.0e-50);
        minData()->setMax(1.0e50);
        minData()->setMin(1.0e-50);
      }
    }
    else
    {
      if (mPlotter && mPlotter->modeVal()==CYScope::XY)
      {
        maxData()->initMinMax();
        minData()->initMinMax();
      }
    }
  }
  updateOptions();
  emit limitsChanged();
}

void CYScopeAxis::setAutoScale(bool val)
{
  if (!mAutoScale)
  {
    CYWARNING
    return;
  }

  mAutoScale->setVal(val);

  updateOptions();
  emit limitsChanged();
}

void CYScopeAxis::setShow(bool val)
{
  mShow->setVal(val);
  mPlotter->enableAxis(mType, isEnabled());
}

void CYScopeAxis::changeXScale(double left, double right)
{
  mLeft->setVal(left);
  mRight->setVal(right);
  changeXScale();
}

void CYScopeAxis::changeXScale()
{
  if (!mPlotter->xSignal(mType))
    return;

  mAutoScaleHide->setVal(false);

  f64 left = mLeft->val();
  f64 right = mRight->val();

  CYData *data = mPlotter->xSignal(mType)->data;
  initX(data);
  mLeft->setVal(left);
  mRight->setVal(right);

  if (mPlotter->samplingModeVal()==CYScope::Bursts)
  {
    mPlotter->setSizeMesure(data->format()->burstsBufferSize());
  }
  else if (data->tableBufferSize())
  {
    mPlotter->setSizeMesure(data->tableBufferSize());
  }
  else
  {
    mPlotter->setSizeMesure(data->format()->bufferSize(!data->isReadFast()));
  }

  if ( mLog->val() )
  {
    if ( mLeft->val() > mRight->val() )
    {
      QwtLogScaleEngine *scale = new QwtLogScaleEngine;
      scale->setAttributes(QwtScaleEngine::Inverted|QwtScaleEngine::Floating);
      mPlotter->setAxisScaleEngine(mType, scale);
      mPlotter->setAxisScale(mType, mRight->val(), mLeft->val());
    }
    else
    {
      QwtLogScaleEngine *scale = new QwtLogScaleEngine;
      scale->setAttributes(QwtScaleEngine::Floating);
      mPlotter->setAxisScaleEngine(mType, scale);
      mPlotter->setAxisScale(mType, mLeft->val(), mRight->val());
    }
  }
  else
  {
    if ( mLeft->val() > mRight->val() )
    {
      QwtLinearScaleEngine *scale = new QwtLinearScaleEngine;
      scale->setAttributes(QwtScaleEngine::Inverted|QwtScaleEngine::Floating);
      mPlotter->setAxisScaleEngine(mType, scale);
      mPlotter->setAxisScale(mType, mRight->val(), mLeft->val());
    }
    else
    {
      QwtLinearScaleEngine *scale = new QwtLinearScaleEngine;
      scale->setAttributes(QwtScaleEngine::Floating);
      mPlotter->setAxisScaleEngine(mType, scale);
      mPlotter->setAxisScale(mType, mLeft->val(), mRight->val());
    }
  }
  if (mAutoScale && mAutoScale->val())
  {
    mPlotter->setAxisAutoScale(mType);
  }
}


void CYScopeAxis::changeYScale(double min, double max)
{
  setMinVal(min);
  setMaxVal(max);
  changeYScale();
}

void CYScopeAxis::changeYScale()
{
  if ( mLog->val() )
  {
    QwtLogScaleEngine *scale = new QwtLogScaleEngine;
    scale->setAttributes(QwtScaleEngine::Floating);
    mPlotter->setAxisScaleEngine(mType, scale);
  }
  else
  {
    QwtLinearScaleEngine *scale = new QwtLinearScaleEngine;
    scale->setAttributes(QwtScaleEngine::Floating);
    mPlotter->setAxisScaleEngine(mType, scale);
  }

  if (minVal()>maxVal())
  {
    double tmp = minVal();
    setMinVal(maxVal());
    setMaxVal(tmp);
  }

  mPlotter->setAxisScale(mType, minVal(), maxVal());
  if (mAutoScale && mAutoScale->val())
  {
    mPlotter->setAxisAutoScale(mType);
  }
}


/*! Saisie la taille de police
    \fn CYScopeAxis::setFontSize( int val )
 */
void CYScopeAxis::setFontSize( int val )
{
  QFont font = mPlotter->axisFont(mType);
  font.setPointSize( val );
  mPlotter->setAxisFont(mType, font);
  QwtScaleWidget *wdg = mPlotter->axisWidget(mType);
  QwtText text = wdg->title();
  wdg->setFont(font);
  font.setBold(true);
  text.setFont( font );
  wdg->setTitle( text );
  mPlotter->QwtPlot::enableAxis(mType, isEnabled());
}


/*! Initialise en mode XY les données en X à partir de la donnée \a data.
    \fn CYScopeAxis::initX(CYData *data)
 */
void CYScopeAxis::initX(CYData *data)
{
  if ( !isX() )
    return;

  mIsTimeScale = false;
  mAutoScaleHide->setVal(false);

  mLinksName = data->linksName();
  mNumFormat = data->numFormat();

  mUnit  = data->unit();
  if ( (mPlotter->modeVal()==CYScope::XY) &&
       (mPlotter->xSignal(QwtPlot::xBottom)) )
    mTitle = mPlotter->xSignal(QwtPlot::xBottom)->label;
  else
    mTitle = data->physical();

  CYF64 *tmp = (CYF64 *)data;
  mNbDec = data->nbDec();

  double oldMin = minData()->val();
  double oldMax = maxData()->val();

  mRight->setFormat(data->format());
  mRight->setMin(tmp->minToDouble());
  mRight->setMax(tmp->maxToDouble());
  mRight->setDef(!mR2L ? tmp->minToDouble() : tmp->maxToDouble());
  mRight->designer();

  mLeft->setFormat(data->format());
  mLeft->setMin(tmp->minToDouble());
  mLeft->setMax(tmp->maxToDouble());
  mLeft->setDef(!mR2L ? tmp->maxToDouble() : tmp->minToDouble());
  mLeft->designer();

  if ( mInitX && oldMin!=oldMax )
  {
    if ( ( oldMin>minData()->min() ) && ( oldMin<minData()->max() ) )
      minData()->setVal(oldMin);
    if ( ( oldMax>maxData()->min() ) && ( oldMax<maxData()->max() ) )
      maxData()->setVal(oldMax);
  }
  mInitX = true;

  initCaptureDatas();

  mPlotter->setAxisTitle(mType, mTitle + " (" + mUnit + ")");
  setLog(mLog->val());
  mInit = true;
}


/*! Initialise les données en Y à partir de la donnée \a data.
    \fn CYScopeAxis::initY(CYData *data)
 */
void CYScopeAxis::initY(CYData *data)
{
  if ( !isY() )
    return;

  mLinksName = data->linksName();
  mNumFormat = data->numFormat();

  double oldMin = minData()->val();
  double oldMax = maxData()->val();

  if (!mPlotter->hasMultipleFormats())
  {
    mUnit  = data->unit();
    mTitle = data->physical();

    CYF64 *tmp = (CYF64 *)data;
    mNbDec = data->nbDec();

    mMin->setFormat( tmp->format() );
    mMin->format()->setNbDec( mNbDec );
    mMin->initMinMax();
    mMin->setPrecision(0.0);
    mMin->setDef(tmp->minToDouble());
    mMin->designer();

    mMax->setFormat( tmp->format() );
    mMax->format()->setNbDec( mNbDec );
    mMax->setPrecision(0.0);
    mMax->initMinMax();
    mMax->setDef(tmp->maxToDouble());
    mMax->designer();

    if ( mInitY && oldMin!=oldMax )
    {
      if ( ( oldMin>minData()->min() ) && ( oldMin<minData()->max() ) )
        minData()->setVal(oldMin);
      if ( ( oldMax>maxData()->min() ) && ( oldMax<maxData()->max() ) )
        maxData()->setVal(oldMax);
    }
    mInitY = true;
  }

  initCaptureDatas();

  if (!mPlotter->hasMultipleFormats())
  {
    if (!mUnit.isEmpty())
      mPlotter->setAxisTitle(mType, mTitle + " (" + mUnit + ")");
    else
      mPlotter->setAxisTitle(mType, mTitle);
    mPlotter->setAxisScale(mType, minVal(), maxVal());
  }
  else
  {
    mPlotter->setAxisTitle(mType,"");
  }
  setLog(mLog->val());
  mInit = true;
}


QString CYScopeAxis::linksName()
{
  return mLinksName;
}

CYFormat *CYScopeAxis::format()
{
  if (core)
   return core->formats[mNumFormat];
  return nullptr;
}

bool CYScopeAxis::compatibleFormat(CYFormat *extFormat)
{
  if (format())
  {
    CYFormat *axisFormat = format();
    if (axisFormat->unit() != extFormat->unit())
      return false;
    return true;
  }
  return false;
}

bool CYScopeAxis::isX()
{
  return (( mType==QwtPlot::xBottom ) || ( mType==QwtPlot::xTop )) ? true : false;
}


bool CYScopeAxis::isY()
{
  return (( mType==QwtPlot::yLeft ) || ( mType==QwtPlot::yRight )) ? true : false;
}

bool CYScopeAxis::isTimeScale()
{
  return mIsTimeScale;
}

/*! Zoom avant suivant le coefficient \a coef.
    \fn CYScopeAxis::zoomIn(double coef)
 */
void CYScopeAxis::zoomIn(double coef)
{
  if ( coef <= 0.0)
    return;

  double delta = ( maxVal() - minVal() ) / coef;
  double min = minVal()+delta/2;
  double max = maxVal()-delta/2;
  if (min==max)
    return;

  if (min < minData()->min())
    setMinVal(minData()->min());
  else if (min > minData()->max())
    setMinVal(minData()->max());
  else
    setMinVal(min);

  if (max < maxData()->min())
    setMaxVal(maxData()->min());
  else if (max > maxData()->max())
    setMaxVal(maxData()->max());
  else
    setMaxVal(max);
}


/*! Zoom arrière suivant le coefficient \a coef.
    \fn CYScopeAxis::zoomOut(double coef)
 */
void CYScopeAxis::zoomOut(double coef)
{
  if ( coef <= 0.0)
    return;

  double delta = (maxVal()-minVal())/coef;
  double min = minVal()-delta*2;
  double max = maxVal()+delta*2;
  if (min==max)
    return;

  if (min < minData()->min())
    setMinVal(minData()->min());
  else if (min > minData()->max())
    setMinVal(minData()->max());
  else
    setMinVal(min);

  if (max < maxData()->min())
    setMaxVal(maxData()->min());
  else if (max > maxData()->max())
    setMaxVal(maxData()->max());
  else
    setMaxVal(max);
}


void CYScopeAxis::plotMousePressed(const QMouseEvent &e)
{
  if (isX())
    mZoomVal1 = mPlotter->invTransform(mType, e.pos().x());
  else
    mZoomVal1 = mPlotter->invTransform(mType, e.pos().y());
}


void CYScopeAxis::plotMouseReleased(const QMouseEvent &e)
{
  if (isX())
    mZoomVal2 = mPlotter->invTransform(mType, e.pos().x());
  else
    mZoomVal2 = mPlotter->invTransform(mType, e.pos().y());

  if (mZoomVal2 < minData()->min())
    mZoomVal2 = minData()->min();
  if (mZoomVal2 > maxData()->max())
    mZoomVal2 = minData()->max();

  if (mZoomVal1<mZoomVal2)
  {
    setMinVal(mZoomVal1);
    setMaxVal(mZoomVal2);
  }
  else
  {
    setMinVal(mZoomVal2);
    setMaxVal(mZoomVal1);
  }
}


/*! \a return true si l'axe n'empêche pas un Zoom arrière
    \fn CYScopeAxis::enableZoomOut()
 */
bool CYScopeAxis::enableZoomOut()
{
  if (!isEnabled())
    return true;
  return ( (minData()->val() == minData()->min()) && (maxData()->val() == maxData()->max()) ) ? false : true;
}


/*! Ouvre la boîte de dialogue de l'échelle
    \fn CYScopeAxis::setupScale()
 */
void CYScopeAxis::setupScale()
{
  CYScopeScaleDialog *dlg = new CYScopeScaleDialog(mPlotter, "CYScopeScaleDialog");
  dlg->setWindowTitle(mPlotter->axisTitle(mType).text());
  dlg->setScopeAxis(this);
  if ( isX() )
  {
    dlg->exec();
    mPlotter->changeRangeX();
    mPlotter->enableAxis(mType, isEnabled());
  }
  else
  {
    dlg->exec();
    mPlotter->changeRangeY();
    mPlotter->enableAxis(mType, isEnabled());
  }
}


/*!
    \fn CYScopeAxis::backupScaleValues()
 */
void CYScopeAxis::backupScaleValues()
{
    /// @todo implement me
}


/*! Initialise les données relatives à une capture d'oscilloscope.
    \fn CYScopeAxis::initCaptureDatas()
 */
void CYScopeAxis::initCaptureDatas()
{
  if (!mPlotter->inherits("CYScopeCapture"))
    return;
  mScopeCapture = (CYScopeCapture *)parent();

  cursor1()->setLabel(isX()?"X1":"Y1");
  cursor1()->setFormat(minData()->format());
  cursor1()->setGroup(title());
  cursor1()->setEnableColor( mScopeCapture->cursorColor( CYScopeCapture::Cursor1 ) );

  cursor2()->setLabel(isX()?"X2":"Y2");
  cursor2()->setFormat(minData()->format());
  cursor2()->setGroup(title());
  cursor2()->setEnableColor( mScopeCapture->cursorColor( CYScopeCapture::Cursor2 ) );

  cursorD()->setLabel(isX()?"dX":"dY");
  cursorD()->setFormat(minData()->format());
  cursorD()->setGroup(title());

  if (isX())
  {
    if (mPlotter->modeVal()==CYScope::Time)
    {
      cursorID()->setLabel("1/dX");
      cursorID()->setGroup(title());
      cursorID()->setMax( 100000.0 );
      cursorID()->setMin( 0.0 );
      cursorID()->setNbDec( 3 );
      cursorID()->setUnit( tr("Hz") );
    }
  }

}

CYF64 *CYScopeAxis::cursor(CYScopeCapture::Cursor cursor)
{
  return mCursors[cursor];
}


CYF64 *CYScopeAxis::cursor1()
{
  return mCursors[CYScopeCapture::Cursor1];
}


CYF64 *CYScopeAxis::cursor2()
{
  return mCursors[CYScopeCapture::Cursor2];
}


CYF64 *CYScopeAxis::cursorD()
{
  return mCursors[CYScopeCapture::Delta];
}


CYF64 *CYScopeAxis::cursorID()
{
  return mCursors[CYScopeCapture::IDelta];
}



/*! @return \a true si l'axe a été initialisé et doit être affiché.
    \fn CYScopeAxis::isEnabled()
 */
bool CYScopeAxis::isEnabled()
{
  bool val = (mInit && mShow && mShow->val());
  if (mEnable)
    mEnable->setVal(val);
  return val;
}


void CYScopeAxis::setStep( double val )
{
  mStep = val;
}


double CYScopeAxis::step()
{
  return mStep;
}


void CYScopeAxis::setNbStep( int nb )
{
  mNbStep = nb;
}


int CYScopeAxis::nbStep()
{
  return mNbStep;
}

void CYScopeAxis::initTimeScale(uint nb, float step, int burstsRatio)
{
  mIsTimeScale = true;
  mAutoScaleHide->setVal(true);

  setNbStep(nb);
  setNbDec(2);
  if (step<1000.0)
  {
    if (burstsRatio>1)
    {
      setUnit( tr("ms") );
      setStep( (double)step/burstsRatio );
      setNbDec(1);
    }
    else
    {
      setUnit( tr("sec") );
      setStep( (double)step/1000.0 );
      if (step>100.0)
        setNbDec(1);
      else if (step<10.0)
        setNbDec(3);
    }
  }
  else if (step<60000.0)
  {
    setUnit( tr("min") );
    setStep( (double)step/60000.0 );
    if (step>6000.0)
      setNbDec(1);
  }
  else
  {
    setUnit( tr("hour") );
    setStep( (double)step/3600000.0 );
  }

  mR2L = (mRight->val()>mLeft->val()) ? true : false;
  CYFormat *fmt = new CYFormat(this, "MM_TIME_FMT", unit(), tr("Time"), 0.0, CYScopeAxis::step()*nbStep(), nbDec(), 0.0);

  mTime->setFormat(fmt);
  mTime->setMax(CYScopeAxis::step()*nbStep());
  mTime->setMin(0.0);
  mTime->calculNbDigits();
  initX(mTime);

  changeTimeScale();
}

void CYScopeAxis::changeTimeScale(double left, double right)
{
  mLeft->setVal(left);
  mRight->setVal(right);
  changeTimeScale();
}

void CYScopeAxis::changeTimeScale()
{
  if (mNbStep<0)
    return;

  mAutoScaleHide->setVal(true);

  uint init;
  uint end;
  init = (uint)( ( minVal()/mStep )+0.5 );
  end  = (uint)( ( maxVal()/mStep )+0.5 );

  uint nb   = qAbs(end - init);

  if (nb<=0)
    return;

  if (nb<4)
  {
    while (nb<4)
    {
      setMinVal( minVal() - mStep );
      setMaxVal( maxVal() + mStep );
      init = (uint)( ( minVal()/mStep )+0.5 );
      end  = (uint)( ( maxVal()/mStep )+0.5 );
      nb   = qAbs(end - init);
      emit enableActionZoomIn(false);
    }
  }
  else
  {
    emit enableActionZoomIn(true);
  }

  QVector<double> f(end);
  for (uint i=0; i<end ; i++)
  {
    if (i<init)
      f[i] = 0;
    else
      f[i] = (i * mStep);
  }
  xTime = f;

  mPlotter->setOffsetMesure(0);
  mPlotter->setSizeMesure(end);

  if ( mLeft->val() > mRight->val() )
  {
    QwtLinearScaleEngine *scale = new QwtLinearScaleEngine;
    scale->setAttributes(QwtScaleEngine::Inverted|QwtScaleEngine::Floating);
    mPlotter->setAxisScaleEngine(mType, scale);
    mPlotter->setAxisScale(mType, maxVal(), minVal());
  }
  else
  {
    QwtLinearScaleEngine *scale = new QwtLinearScaleEngine;
    scale->setAttributes(QwtScaleEngine::Floating);
    mPlotter->setAxisScaleEngine(mType, scale);
    mPlotter->setAxisScale(mType, minVal(), maxVal());
  }
}

void CYScopeAxis::transformPosToVal()
{
  if (!isEnabled())
    return;

  if (!cursor1() || !cursor2())
    return;

  if (isX())
  {
    cursor1()->setVal( mPlotter->invTransform(mType, mPlotter->posXcur1) );
    cursor2()->setVal( mPlotter->invTransform(mType, mPlotter->posXcur2) );
  }
  if (isY())
  {
    cursor1()->setVal( mPlotter->invTransform(mType, mPlotter->posYcur1) );
    cursor2()->setVal( mPlotter->invTransform(mType, mPlotter->posYcur2) );
  }

  calculDelta();
}

void CYScopeAxis::transformValToPos()
{
  if (!isEnabled())
    return;

  if (!cursor1() || !cursor2())
    return;

  if (isX())
  {
    mPlotter->posXcur1 = mPlotter->transform(mType, cursor1()->val() );
    mPlotter->posXcur2 = mPlotter->transform(mType, cursor2()->val() );
  }
  if (isY())
  {
    mPlotter->posYcur1 = mPlotter->transform(mType, cursor1()->val() );
    mPlotter->posYcur2 = mPlotter->transform(mType, cursor2()->val() );
  }

  calculDelta();
}

void CYScopeAxis::calculDelta()
{
  cursorD()->setVal( cursor1()->val() - cursor2()->val() );

  if (isX() && (mPlotter->modeVal()==CYScope::Time) )
  {
    float ms = 0.0;
    if ( cursorD()->unit() == tr("ms") )
    {
      ms = 1.0;
    }
    else if ( cursorD()->unit() == tr("sec") )
    {
      ms = 1000.0;
    }
    else if ( cursorD()->unit() == tr("min") )
    {
      ms = 60000.0;
    }
    else if ( cursorD()->unit() == tr("hour") )
    {
      ms = 3600000.0;
    }

    if ( cursorD()->toString( false ).toDouble() == 0.0 )
      cursorID()->setVal( cursorID()->max()+1.0 );
    else
      cursorID()->setVal(  1000.0 / ( cursorD()->val() * ms ) );

    if ( cursorID()->val() < 0.0 )
      cursorID()->setVal( cursorID()->val() * (-1.0) );
  }
}


QString CYScopeAxis::desc()
{
  return mDesc;
}

