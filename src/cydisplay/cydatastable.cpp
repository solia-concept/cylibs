/***************************************************************************
                          cydatastable.cpp  -  description
                             -------------------
    begin                : mer nov 3 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydatastable.h"

// QT
#include <QLayout>
#include <QInputDialog>
#include <QMenu>
#include <QHeaderView>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QDesktopServices>
#include <QPainter>
// CYLIBS
#include "cycore.h"
#include "cydb.h"
#include "cydata.h"
#include "cyf64.h"
#include "cynetlink.h"
#include "cynethost.h"
#include "cytype.h"
#include "cywidget.h"
#include "cydisplayitem.h"
#include "cydatastablecol.h"
#include "cydatastablerow.h"
#include "cydatastabledisplaycell.h"
#include "cydatastableplotter.h"
#include "cydatastablesetup.h"
#include "cydatasbrowser.h"
#include "cydisplaylistview.h"
#include "cyanalysecell.h"
#include "cyanalysesheet.h"
#include "cydataslistview.h"
#include "cyproject.h"
#include "cyapplication.h"

CYDatasTable::CYDatasTable(QWidget *parent, const QString &name, const QString &title, bool nf)
  : CYDisplay(parent, name, title, nf)
{
  mType = DatasTable;
  mReset   = false;
  mPartial = false;
  mNote    = false;
  mThreshold =false;
  mSetup = 0;
  mSwapColsOffset=0;
  mSwapRowsOffset=0;
  mPrintDesignerLogo = true;
  mPrintDesignerRef  = true;
  mPrintHeightRow = 0;

  mDB->setGroup(tr("Datas table"));

  if (!mFrame)
  {
    mCYWdg = new CYWidget( this, "mCYWdg" );
    mPlot = new CYDatasTablePlotter(mCYWdg, "CYDatasTablePlotter", this);
    mBoxLayout->addWidget(mPlot);
  }
  else
  {
    mCYWdg = new CYWidget( mFrame, "mCYWdg" );
    mPlot = new CYDatasTablePlotter(mCYWdg, "CYDatasTablePlotter", this);
    mBoxLayout->addWidget(mPlot);
  }

  /* Tous les clics RMB dans le widget plot seront gérés par CYDisplay::eventFilter. */
  mPlot->installEventFilter(this);

  registerPlotterWidget(mPlot);

  updateGeometry();
  mCreating= false;

  startRefresh();
}

CYDatasTable::~CYDatasTable()
{
  mDeleting= true;
}

bool CYDatasTable::addGroup(QString txt)
{
  if (!mCols.count())
    initColumns();

  if (core)
    core->addGroup( txt, this );
  mPlot->showHideColumns();
  return true;
}

bool CYDatasTable::addData(CYData *data)
{
  return addData(data, data->label());
}

bool CYDatasTable::addData(CYData *data, QString label)
{
  int pos = mPlot->rowCount();
  mPlot->setRowCount(pos+1);

  if (addData(data, pos, true, 20, label))
    return true;
  return false;
}

CYDatasTableRow *CYDatasTable::addData(CYData *data, int pos, bool shown, int height, QString label)
{
  if (!mCols.count())
    initColumns();

  CYDatasTableRow *row = new CYDatasTableRow(this, data, pos, pos, shown, height, label);
  mRows.insert(pos, row);
  mPlot->addRow(row, mCreatingFromDOM);
  mPlot->showHideColumns();
  if (isVisible())
    linkDatas();
  return row;
}

bool CYDatasTable::removeData(uint idx)
{
  CYDatasTableRow *dtr = mRows[idx];
  if (!dtr)
    return false;

  // sauvegarde position avant décallage de son indice
  int pos=dtr->pos();

  // décale l'élément en fin de liste avant de le supprimer
  while ( dtr->pos() < (mPlot->rowCount()-1) )
    swapRows( dtr->id, dtr->pos(), dtr->pos()+1);

  mRows.remove(dtr->id);
  mPlot->removeRow(pos);
  mPlot->setRowCount(mRows.count());
  return true;
}

void CYDatasTable::startRefresh()
{
  CYDisplay::startRefresh();
  QMapIterator<int, CYDatasTableRow *> i(mRows);
  i.toFront();
  while (i.hasNext())
  {
    i.next();
    CYDatasTableRow *dtr = i.value();
    dtr->startRefresh();
  }
}

void CYDatasTable::stopRefresh()
{
  CYDisplay::stopRefresh();
  QMapIterator<int, CYDatasTableRow *> i(mRows);
  i.toFront();
  while (i.hasNext())
  {
    i.next();
    CYDatasTableRow *dtr = i.value();
    dtr->stopRefresh();
  }
}

void CYDatasTable::refresh()
{
  if (mDeleting || mCreating)
    return;

  if ( !mRefreshNotVisible && !isVisible() )
    return;

  if (!core)
    return;

  refreshStatus();

  if (core && !core->analyseRefreshEnabled())
    return;

  mCYWdg->refresh();
}

bool CYDatasTable::createFromDOM(QDomElement &element, QStringList *ErrorList)
{
  mCreatingFromDOM = true;
  removeDatas();

  int offset=0;
  if (compareVersionCYDOM("5.05")<0)
  {
    offset++; // décalage de la colonne id innnexistante avant la version 5.05
    mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Id, mCols.count(), false,  40, tr("Id")));
  }
  // Chargement des propriétés des colonnes.
  int id=1;
  QDomNodeList colList = element.elementsByTagName("column");
  for (int num=0; num<colList.count(); num++)
  {
    QDomElement el = colList.item(num).toElement();

    CYDatasTableCol::Type type = (CYDatasTableCol::Type)(el.attribute("type").toInt()+offset);
    QString posKde3 = el.attribute("pos", ""); // récupération ancien fichier kde3
    id = (!posKde3.isEmpty()) ? posKde3.toInt() : el.attribute("id", QString::number(id)).toInt();
    id+=offset;

    CYDatasTableCol *dtc = new CYDatasTableCol(type,
                                               id,
                                               (bool)el.attribute("shown").toInt(),
                                               el.attribute("width").toInt(),
                                               el.attribute("label")
                                               );
    mCols.insert(id, dtc);    
    mPlot->addColumn(dtc, false);
    mPlot->setHorizontalHeaderItem(dtc->id, dtc->header());
  }

  // Chargement des propriétés des lignes.
  QDomNodeList rowList = element.elementsByTagName("row");
  mPlot->setRowCount(rowList.count());
  int cptHide = 0;
  int cptError = 0;
  int cpt = 0;
  while ( cpt<rowList.count() )
  {
    bool find = false;
    QDomElement el;
    for (int num=cpt; num<rowList.count(); num++)
    {
      el = rowList.item(num).toElement();
      if (( el.attribute("pos").toInt()==cpt ) || ( el.attribute("id").toInt()==cpt ))
      {
        find = true;
        break;
      }
    }
    if (!find)
    {
      for (int num=0; num<rowList.count(); num++)
      {
        el = rowList.item(num).toElement();
        if (( el.attribute("pos").toInt()==cpt ) || ( el.attribute("id").toInt()==cpt ))
        {
          find = true;
          break;
        }
      }
    }
    if (!find)
      el = rowList.item(cpt).toElement();

    cpt++;

    QString dataName = el.attribute("data");
    CYData *d = core->db.value(dataName);
    if (!d)
    {
        if(ErrorList!=nullptr){
            ErrorList->append(dataName);//pour utilisation plus tard par CYWARNINGLIST, évite 12000 messages
        }
        else CYWARNINGDATA(dataName);
        cptError++;
        continue;
    }
    bool inAnalyseSpace = false;
    if (mCell && mCell->sheet() && mCell->sheet()->parent()->inherits("CYAnalyseSpace"))
      inAnalyseSpace = true;
    if ( (d->mode()==Cy::Designer) && ( !inAnalyseSpace || !core->designer() ) )
    {
      cptHide++;
      continue;
    }
    int pos = el.attribute("pos", QString::number(cpt-1)).toInt()-cptHide-cptError;
    CYDatasTableRow *row = addData(d, pos, el.attribute("shown").toInt(), el.attribute("height").toInt(), el.attribute("label", d->label()));
    if (!row)
      CYFATALOBJECTDATA(objectName(), dataName);
    row->createDisplaysFromDOM(el);
  }
  mPlot->setRowCount(rowList.count()-cptHide-cptError);

  // Titre de la table.
  setForcing((bool)element.attribute("forcing").toInt());
  setReset((bool)element.attribute("reset").toInt());
  setPartial((bool)element.attribute("partial").toInt());
  setNote((bool)element.attribute("note").toInt());
  setThreshold((bool)element.attribute("threshold").toInt());

  // Prise en compte de l'intervalle de rafraîchissement.
  internCreateFromDOM(element);

  mPlot->showHideColumns();

  setModified(false);
  mCreatingFromDOM = false;
  return (true);
}

bool CYDatasTable::addToDOM(QDomDocument &doc, QDomElement &element, bool save)
{
  // Sauvegarde des propriétés des colonnes.
  QMapIterator<int, CYDatasTableCol *> ic(mCols);
  ic.toFront();
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    QDomElement col = doc.createElement("column");
    element.appendChild(col);
    col.setAttribute("type", dtc->type());
    col.setAttribute("id", dtc->id);
    col.setAttribute("label", dtc->label());
    col.setAttribute("width", dtc->width());
    col.setAttribute("shown", dtc->shown());
  }

  // Sauvegarde des propriétés des lignes.
//  QMapIterator<int, CYDatasTableRow *> ir(mRows);
//  ir.toFront();
//  while (ir.hasNext())
//  {
//    ir.next();
//    CYDatasTableRow *dtr = ir.value();
  // Sauvegarde des propriétés des lignes.
  for (int pos=0; pos<mPlot->rowCount(); pos++)
  {
    CYDatasTableRow *dtr = CYDatasTable::dtr(pos);
    if (!dtr)
      continue;
    QDomElement row = doc.createElement("row");
    element.appendChild(row);
    row.setAttribute("data", dtr->data->objectName());
    row.setAttribute("id", dtr->id);
    row.setAttribute("pos", pos);
    row.setAttribute("height", dtr->height());
    row.setAttribute("shown", dtr->shown());
    // n'enregistre pas l'étiquette lorsqu'elle est la même que celle de la donnée
    // afin dans ce cas de charger sa traduction
    if (dtr->label!=dtr->data->label())
      row.setAttribute("label", dtr->label);
    dtr->addDisplaysToDOM(doc, row, save);
  }

  // Titre de la table.
  element.setAttribute("forcing", forcing());
  element.setAttribute("reset", reset());
  element.setAttribute("partial", partial());
  element.setAttribute("note", note());
  element.setAttribute("threshold", threshold());

  // Sauvegarde l'intervalle de rafraîchissement.
  internAddToDOM(doc, element);

  if (save)
    setModified(false);

  return (true);
}

int CYDatasTable::nbCols()
{
  return mCols.count();
}

CYDatasTableCol *CYDatasTable::col(int i)
{
  return mCols[i];
}

CYDatasTableCol *CYDatasTable::colType(int type)
{
  QMapIterator<int, CYDatasTableCol *> ic(mCols);
  ic.toFront();
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if (dtc->type()==type)
      return dtc;
  }
  return 0;
}

int CYDatasTable::nbRows()
{
  return mRows.count();
}

CYDatasTableRow *CYDatasTable::row(int i)
{
  return mRows[i];
}

CYDatasTableRow *CYDatasTable::dtr(int row)
{
  CYDatasTableCol *dtc = colType(CYDatasTableCol::Id);
  int col = dtc->id;

  QTableWidgetItem *item = mPlot->item(row, col);
  if (!item)
  {
    CYMESSAGETEXT(QString("item(%1, %2)").arg(row).arg(col));
    return 0;
  }

  int idr = item->text().toInt();
  CYDatasTableRow *dtr = mRows[idr];
  if (!dtr)
  {
    CYMESSAGETEXT(QString("mRows[%1]").arg(idr));
    return 0;
  }

  return dtr;
}

void CYDatasTable::moveColumn(int from, int to)
{
  mPlot->horizontalHeader()->moveSection(from, to);
}

void CYDatasTable::swapColumns( int logicalIndex, int oldVisualIndex, int newVisualIndex )
{
  if (mSwapColsOffset==0)
  {
    // début permutation
    mSwapColsOffset = oldVisualIndex-newVisualIndex;
  }

  if (mSwapColsOffset==0)
    return; // pas de permutation

  CYDatasTableCol *dtc1 = mCols[logicalIndex];
  if (!dtc1)
    return;
  if (dtc1->id!=oldVisualIndex)
    return;

  CYDatasTableCol *dtc2;
  if (mSwapColsOffset>0)
  {
    dtc2 = mCols[logicalIndex-1]; // colonne à gauche
    mSwapColsOffset--;
  }
  else
  {
    dtc2 = mCols[logicalIndex+1]; // colonne à droite
    mSwapColsOffset++;
  }

  if (!dtc2)
    return;
  dtc1->id = dtc2->id;
  dtc2->id = logicalIndex;

  mCols.insert( dtc1->id, dtc1 );
  mCols.insert( dtc2->id, dtc2 );

  if (mSwapColsOffset>0) // permuter avec colonne à gauche
    swapColumns(dtc1->id, dtc1->id, dtc1->id-1);

  if (mSwapColsOffset<0) // permuter avec colonne à droite
    swapColumns(dtc1->id, dtc1->id, dtc1->id+1);

  setModified(true);
}

void CYDatasTable::moveRow(int from, int to)
{
  mPlot->verticalHeader()->moveSection(from, to);
}

void CYDatasTable::swapRows( int logicalIndex, int oldVisualIndex, int newVisualIndex )
{
  if (mSwapRowsOffset==0)
  {
    // début permutation
    mSwapRowsOffset = oldVisualIndex-newVisualIndex;
  }

  if (mSwapRowsOffset==0)
    return; // pas de permutation

  CYDatasTableRow *dtr1 = mRows[logicalIndex];
  if (dtr1->id!=oldVisualIndex)
    return;

  CYDatasTableRow *dtr2;
  if (mSwapRowsOffset>0)
  {
    dtr2 = mRows[logicalIndex-1]; // ligne au dessus
    mSwapRowsOffset--;
  }
  else
  {
    dtr2 = mRows[logicalIndex+1]; // ligne en dessous
    mSwapRowsOffset++;
  }

  dtr1->id = dtr2->id;
  dtr2->id = logicalIndex;

//  int pos1 = dtr1->pos();
//  int pos2 = dtr2->pos();
//  dtr1->setPos(pos2);
//  dtr2->setPos(pos1);

  int height1 = dtr1->height();
  int height2 = dtr2->height();
  dtr1->setHeight(height2);
  dtr2->setHeight(height1);

  mRows.insert( dtr1->id, dtr1 );
  mRows.insert( dtr2->id, dtr2 );

  if (mSwapRowsOffset>0) // permuter avec ligne au dessus
    swapRows(dtr1->id, dtr1->id, dtr1->id-1);

  if (mSwapRowsOffset<0)// permuter avec ligne en dessous
    swapRows(dtr1->id, dtr1->id, dtr1->id+1);

  setModified(true);
}

void CYDatasTable::settings()
{
  if (!mSetup)
  {
    mSetup = new CYDatasTableSetup(this, "CYDatasTableSetup");
    Q_CHECK_PTR(mSetup);
  }

  if ( !mUpdatingFormat )
    mSetup->exec();
}

void CYDatasTable::applySettings()
{
  if ( !mUpdatingFormat )
  {
    QList<CYDatasTableRow*> rowsToRemove;
    CYDatasListView *list = mSetup->dataEditList();
    QMapIterator<int, CYDatasTableRow *> ir(mRows);
    ir.toFront();
    while (ir.hasNext())
    {
      ir.next();
      CYDatasTableRow *dtr = ir.value();
      if (!list->find(dtr))
      {
        rowsToRemove.append(dtr);
      }
    }

    if (!rowsToRemove.isEmpty())
    {
      QListIterator<CYDatasTableRow*> it(rowsToRemove);
      it.toBack();
      while (it.hasPrevious())
      {
        CYDatasTableRow *dtr=it.previous();
        removeData(dtr->id);
      }
    }

    for (QTreeWidgetItemIterator it(list); *it; ++it)
    {
      int num = list->num(*it)-1;
      CYDatasTableRow *dtr = mRows[num];
      if (!dtr) // Ajout d'une donnée
      {
        addData(core->findData(list->dataName(*it)), list->label(*it));
      }
      else if (dtr->label != list->label(*it)) // Modification d'étiquette
      {
        dtr->label = list->label(*it);
        QMapIterator<int, CYDatasTableCol *> ic(mCols);
        ic.toFront();
        while (ic.hasNext())
        {
          ic.next();
          CYDatasTableCol *dtc = ic.value();
          if ( dtc->type() == CYDatasTableCol::Label )
          {
            mPlot->item(num, dtc->id)->setText(list->label(*it));
            mPlot->QTableWidget::setRowHeight(dtr->id, dtr->height());
            break;
          }
        }
      }
    }
  }
  else
  {
    QMapIterator<int, CYDatasTableRow *> ir(mRows);
    ir.toFront();
    while (ir.hasNext())
    {
      ir.next();
      CYDatasTableRow *dtr = ir.value();
      QMapIterator<int, CYDatasTableCol *> ic(mCols);
      ic.toFront();
      while (ic.hasNext())
      {
        ic.next();
        CYDatasTableCol *dtc = ic.value();
        if ( dtc->type() == CYDatasTableCol::Label )
        {
          mPlot->item(ir.key(), dtc->id)->setText(dtr->label);
          break;
        }
      }
    }
  }
  if (mFrame)
  {
    mFrame->updateGeometry();
  }
  CYDisplay::applySettings();
}

void CYDatasTable::toggleForcing()
{
  setForcing(!forcing());
}

void CYDatasTable::setForcing(const bool val)
{
  CYDisplay::setForcing(val);
  mPlot->showHideColumns();
}

void CYDatasTable::toggleReset()
{
  setReset(!reset());
}

void CYDatasTable::setReset(const bool val)
{
  mReset = val;
  mPlot->showHideColumns();
}

void CYDatasTable::togglePartial()
{
  setPartial(!partial());
}

void CYDatasTable::setPartial(const bool val)
{
  mPartial = val;
  mPlot->showHideColumns();
}

void CYDatasTable::toggleNote()
{
  setNote(!note());
}

void CYDatasTable::setNote(const bool val)
{
  mNote = val;
  mPlot->showHideColumns();
}

void CYDatasTable::toggleThreshold()
{
  setThreshold(!threshold());
}

void CYDatasTable::setThreshold(const bool val)
{
  mThreshold = val;
  mPlot->showHideColumns();
}

void CYDatasTable::maskCurrentColumn()
{
  showColumn(false, mPlot->currentColumn());
}

void CYDatasTable::setCurrentCell(int row, int column)
{
  mPlot->setCurrentCell(row, column);
}

CYDatasTableCol *CYDatasTable::columnAtPos(int pos)
{
  QMapIterator<int, CYDatasTableCol *> ic(mCols);
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if (!dtc)
      continue;
    if (dtc->id == pos)
      return dtc;
  }
  return 0;
}

void CYDatasTable::showColumn(bool enable, int pos)
{
  CYDatasTableCol *dtc = columnAtPos(pos);
  if (dtc)
  {
    dtc->setShown(enable);
    setModified(true);
  }
  mPlot->showHideColumns();
}

void CYDatasTable::initForceValues()
{
  if (!mForcing)
    return;

  QMapIterator<int, CYDatasTableRow *> ir(mRows);
  while (ir.hasNext())
  {
    ir.next();
    CYDatasTableRow *dtr = ir.value();
    dtr->data->initForcingValue();
  }
}

void CYDatasTable::initColumns()
{
  /** 0*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Id        , mCols.count(), false,  40, tr("Id")));
  /** 1*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Value     , mCols.count(), true , 150, tr("Value")));
  /** 2*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Forcing   , mCols.count(), false, 150, tr("Forcing")));
  /** 3*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Label     , mCols.count(), true , 200, tr("Label")));
  /** 4*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Group     , mCols.count(), false, 200, tr("Group")));
  /** 5*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Link      , mCols.count(), false, 150, tr("Connection(s)")));
  /** 6*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Host      , mCols.count(), false, 140, tr("Host(s)")));
  /** 7*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Name      , mCols.count(), true , 170, tr("Name")));
  /** 8*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Help      , mCols.count(), false, 200, tr("Help")));
  /** 9*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Phys      , mCols.count(), false, 100, tr("Phys")));
  /**10*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Elec      , mCols.count(), false, 100, tr("Elec")));
  /**11*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::ADC       , mCols.count(), false, 150, tr("ADC")));
  /**12*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Display   , mCols.count(), false, 150, tr("Display")));
  /**13*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Reset     , mCols.count(), false, 150, tr("Reset")));
  /**14*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Partial   , mCols.count(), false, 150, tr("Partial")));
  /**15*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Note      , mCols.count(), false, 150, tr("Note")));
  /**16*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Control   , mCols.count(), false, 150, tr("Control")));
  /**17*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Addr      , mCols.count(), false, 100, tr("Addr")));
  /**18*/ mCols.insert(mCols.count(), new CYDatasTableCol(CYDatasTableCol::Threshold , mCols.count(), false, 150, tr("Threshold")));
  updateGeometry();
}

CYDatasTableCol *CYDatasTable::addColumn()
{
  return addColumn("");
}

CYDatasTableCol *CYDatasTable::addColumn(QString label, bool inputLabel)
{
  if (inputLabel)
    label=inputColumnLabel(label);
  CYDatasTableCol *dtc = new CYDatasTableCol(CYDatasTableCol::Display  , mCols.count(), true , 150, label);
  mCols.insert(mCols.count(), dtc);
  mPlot->addColumn(dtc);
  return dtc;
}

QString CYDatasTable::inputColumnLabel(QString label)
{
  bool ok;
  QString text = QInputDialog::getMultiLineText(this, tr("Column label"), tr("Enter the label of this column:"), label, &ok);
  if ( ok && !text.isEmpty() )
  {
    return text;
  }
  else
  {
    return label;
  }
}

void CYDatasTable::changeColumnLabel()
{
  CYDatasTableCol *dtc = mCols[mPlot->currentColumn()];
  if (!dtc)
    CYFATAL;
  dtc->setLabel(inputColumnLabel(dtc->label()));
  mPlot->setHorizontalHeaderItem(dtc->id, dtc->header());
}

void CYDatasTable::setColumnLabel(int pos, QString label)
{
  CYDatasTableCol *dtc = columnAtPos(pos);
  if (!dtc)
    CYFATAL
        dtc->setLabel(label);
  mPlot->setHorizontalHeaderItem(dtc->id, dtc->header());
}

int CYDatasTable::showPopupMenu(QMenu *pm0, int index, bool end)
{
  Q_UNUSED(end)
  QMenu *pm;
  if (pm0)
  {
    if (index==0)                                     // Création d'un sous-menu surgissant
    {
      pm = pm0->addMenu(tr("Datas table")); index++;
    }
    else                                              // Ajout d'entrées dans un menu surgissant
      pm = pm0;
  }
  else                                                // Création d'un menu surgissant
  {
    pm = new QMenu(tr("Datas table"), this); index++;
    pm->addSeparator();
  }

  index = CYDisplay::showPopupMenu(pm, index);

  pm->addSeparator();
  if (core)
  {
    QAction *action;
    if (core->designer())
    {
      action = pm->addAction(tr("&Forcing mode"  ), this, SLOT( toggleForcing())  ); index++;
      action->setCheckable(true);
      action->setChecked(forcing());
      action = pm->addAction(tr("Reset mode"     ), this, SLOT( toggleReset()  )  ); index++;
      action->setCheckable(true);
      action->setChecked(reset());
      action = pm->addAction(tr("Partial mode"   ), this, SLOT( togglePartial())  ); index++;
      action->setCheckable(true);
      action->setChecked(partial());
      action = pm->addAction(tr("Note mode"      ), this, SLOT( toggleNote()   )  ); index++;
      action->setCheckable(true);
      action->setChecked(note());
      action = pm->addAction(tr("Alert threshold"), this, SLOT( toggleThreshold())); index++;
      action->setCheckable(true);
      action->setChecked(threshold());
      pm->addSeparator();
    }
    pm->addAction(core->loadIconSet("cyhide_table_column.png", 22), tr("Hide column"), this, SLOT( maskCurrentColumn() )); index++;
  }

  QMenu *pm2 = (core) ? pm->addMenu(core->loadIconSet("cyshow_table_column.png", 22), tr("Show column"))
                      : new QMenu(pm);
  mMaskedCols.clear();
  QMapIterator<int, CYDatasTableCol *> ic(mCols);
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if (dtc && !dtc->shown())
    {
      bool enable = core->designer();
      switch (dtc->type())
      {
        case CYDatasTableCol::Id       : break;
        case CYDatasTableCol::Value    : enable = true ; break;
        case CYDatasTableCol::Forcing  : break;
        case CYDatasTableCol::Label    : enable = true ; break;
        case CYDatasTableCol::Group    : enable = true ; break;
        case CYDatasTableCol::Link     : enable = true ; break;
        case CYDatasTableCol::Host     : enable = true ; break;
        case CYDatasTableCol::Name     : enable = true ; break;
        case CYDatasTableCol::Help     : enable = true ; break;
        case CYDatasTableCol::Phys     : enable = true ; break;
        case CYDatasTableCol::Elec     : enable = true ; break;
        case CYDatasTableCol::ADC      : enable = true ; break;
        case CYDatasTableCol::Reset    : break;
        case CYDatasTableCol::Partial  : enable = true ; break;
        case CYDatasTableCol::Note     : break;
        case CYDatasTableCol::Control  : break;
        case CYDatasTableCol::Addr     : enable = true ; break;
        case CYDatasTableCol::Threshold: break;
        default: break;
      }
      if ( enable )
      {

        pm2->addAction(dtc->label()); ++index;
        mMaskedCols.insert(index, dtc);
      }
    }
  }
  if (core)
  {
    connect(pm2, SIGNAL(triggered(QAction*)), SLOT( showColumn(QAction*) ) );
    pm->addAction(tr("Add column"), this, SLOT( addColumn() )); index++;
    pm->addAction(tr("Change column label"), this, SLOT( changeColumnLabel() )); index++;
    pm->addAction(core->loadIconSet("cyresizecol.png", 22), tr("Change column width"), this, SLOT( setColumnWidth() )); index++;

    pm->addSeparator();
    pm->addAction(core->loadIconSet("cyhide_table_row.png", 22), tr("Remove row"), this, SLOT( removeRows() )); index++;
    pm->addAction(core->loadIconSet("cyresizerow.png", 22), tr("Change row height"), this, SLOT( setRowHeight() )); index++;

    pm->addSeparator();
    pm->addAction(core->loadIconSet("cyapplication-pdf.png", 22), tr("Export PDF"), this, SLOT( exportPDF() )); index++;
  }
  index = CYDisplay::showPopupMenu(pm, index, true);

  if (!pm0)
    pm->exec(QCursor::pos());

  return index;
}

void CYDatasTable::showColumn(QAction *action)
{
  CYDatasTableCol *dtc = 0;
  QMapIterator<int, CYDatasTableCol *> ic(mMaskedCols);
  while (ic.hasNext())
  {
    ic.next();
    dtc = ic.value();
    if (dtc->label()==action->text())
      showColumn(true, dtc->id);
  }
}

void CYDatasTable::showColumn(int id)
{
  CYDatasTableCol *dtc = mMaskedCols[id];
  if (dtc)
    showColumn(true, dtc->id);
}


/*! Saisie par boîte de dialogue la largeur des colonnes sélectionnées.
    \fn CYDatasTable::setColumnWidth()
 */
void CYDatasTable::setColumnWidth()
{
  int old = 20;
  int width=0;
  CYDatasTableCol *dtc = 0;
  bool first = true;
  QMapIterator<int, CYDatasTableCol *> ic(mCols);
  while (ic.hasNext())
  {
    ic.next();
    dtc = ic.value();
    QItemSelectionModel *selection = mPlot->selectionModel();
    if (selection->isColumnSelected(dtc->id, mPlot->model()->index(0,0)))
    {
      old = dtc->width();

      if ( first )
      {
        bool *ok = 0;
        width = QInputDialog::getInt(this,
                                     tr("Change column width"),
                                     tr("Input the new width of selected columns"),
                                     old, 1, 1000, 1, ok);
        if (ok)
          CYFATAL
              first = false;
      }
      if (width>0)
        setColumnWidth(width, dtc->id);
    }
  }
}


/*! Saisie la largeur des colonnes sélectionnées.
    \fn CYDatasTable::setColumnWidth()
 */
void CYDatasTable::setColumnWidth(int w, int col)
{
  QMapIterator<int, CYDatasTableCol *> ic(mCols);
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if (col==dtc->id)
    {
      dtc->setWidth( w );
      mPlot->setColumnWidth(dtc->id, w);
    }
  }
}

/*! Mémorise la largeur des colonnes sélectionnées.
    \fn CYDatasTable::memColumnWidth()
 */
void CYDatasTable::memColumnWidth(int logicalIndex, int oldSize, int newSize)
{
  Q_UNUSED(oldSize)
  if (newSize==0)
    return;
  CYDatasTableCol *dtc = mCols[logicalIndex];
  if (dtc)
    dtc->setWidth(newSize);
  setModified(true);
}

/*! Mémorise la hauteur des lignes sélectionnées.
    \fn CYDatasTable::memRowHeight()
 */
void CYDatasTable::memRowHeight(int logicalIndex, int oldSize, int newSize)
{
  Q_UNUSED(oldSize)
  CYDatasTableRow *dtc = mRows[logicalIndex];
  if (dtc)
    dtc->setHeight(newSize);
  setModified(true);
}

/*! Supprime les lignes sélectionnées
    \fn CYDatasTable::removeRows()
 */
void CYDatasTable::removeRows()
{
  CYDatasTableRow *dtr=0;
  QList<CYDatasTableRow*> rowsToRemove;
  QModelIndexList selectedList = mPlot->selectionModel()->selectedRows();
  for( int i=0; i<selectedList.count(); i++)
  {
    int pos = selectedList.at(i).row();
    QMapIterator<int, CYDatasTableRow *> ir(mRows);
    ir.toFront();
    while (ir.hasNext())
    {
      ir.next();
      dtr = ir.value();
      if (dtr->pos()==pos)
      {
        rowsToRemove.append(dtr);
        break;
      }
    }
  }

  if (rowsToRemove.isEmpty())
  {
    CYMessageBox::sorry(mThis, tr("No row selected !"), tr("Remove row"));
    return;
  }

  if (CYMessageBox::No == CYMessageBox::warningYesNo(mThis, tr("Are you sure to remove the setected row(s) ?"), tr("Remove row")))
    return;


  dtr = rowsToRemove.takeLast();
  while (dtr)
  {
//    mSwapHeight = false;
//    int pos=dtr->pos;
//    while ( dtr->pos() < (mPlot->rowCount()-1) )
//    {
////      swapRows( dtr->pos(), dtr->pos()+1, true );
//      swapRows( dtr->id, dtr->pos(), dtr->pos()+1);
//    }
//    mSwapHeight = true;

    removeData(dtr->id);
    if (!rowsToRemove.isEmpty())
      dtr = rowsToRemove.takeLast();
    else
      break;
  }

  QMapIterator<int, CYDatasTableRow *> ir(mRows);
  ir.toFront();
  while (ir.hasNext())
  {
    ir.next();
    CYDatasTableRow *dtr = ir.value();
    if (dtr->id!=ir.key())
    {
      dtr->id=ir.key();
    }
  }
  validate();
}


/*! Saisie par boîte de dialogue la hauteur des ligne sélectionnées.
    \fn CYDatasTable::setRowHeight()
 */
void CYDatasTable::setRowHeight()
{
  int old = 20;
  int height=0;
  bool first = true;
  QModelIndexList selectedRows = mPlot->selectionModel()->selectedRows();

  foreach( QModelIndex index, selectedRows )
  {
    int pos = index.row();
    CYDatasTableRow *dtr=0;
    QMapIterator<int, CYDatasTableRow *> ir(mRows);
    ir.toFront();
    while (ir.hasNext())
    {
      ir.next();
      dtr = ir.value();
      if (dtr->pos()==pos)
        break;
      dtr = 0;
    }
    if (!dtr)
      continue;
    old = dtr->height();
    if ( first )
    {
      bool *ok = 0;
      height = QInputDialog::getInt(this,
                                    tr("Change row height"),
                                    tr("Input the new height of selected rows"),
                                    old, 1, 1000, 1, ok);
      if (ok)
        CYFATAL
            first = false;
    }
    if (height>0)
      setRowHeight(height, dtr->pos());
  }
  validate();
}

/*! Saisie la hauteur \a h des ligne sélectionnées.
    \fn CYDatasTable::setRowHeight()
 */
void CYDatasTable::setRowHeight(int h, int row)
{
  QMapIterator<int, CYDatasTableRow *> ir(mRows);
  ir.toFront();
  while (ir.hasNext())
  {
    ir.next();
    CYDatasTableRow *dtr = ir.value();
    if (row==dtr->pos())
    {
      dtr->setHeight(h);
      mPlot->setRowHeight(dtr->pos(), h);
    }
  }
}


/*! Initialise les textes à traduire de l'afficheur.
    @param title  Titre de l'afficheur.
    \fn CYDatasTable::initTRDisplay(QString title)
 */
void CYDatasTable::initTRDisplay(QString title)
{
  CYDisplay::initTRDisplay(title);

  QMapIterator<int, CYDatasTableCol *> ic(mCols);
  ic.toFront();
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    switch (dtc->type())
    {
      case CYDatasTableCol::Id       : dtc->setLabel(tr("Id"))           ; break;
      case CYDatasTableCol::Value    : dtc->setLabel(tr("Value"))        ; break;
      case CYDatasTableCol::Forcing  : dtc->setLabel(tr("Forcing"))      ; break;
      case CYDatasTableCol::Label    : dtc->setLabel(tr("Label"))        ; break;
      case CYDatasTableCol::Group    : dtc->setLabel(tr("Group"))        ; break;
      case CYDatasTableCol::Link     : dtc->setLabel(tr("Connection"))   ; break;
      case CYDatasTableCol::Host     : dtc->setLabel(tr("Host"))         ; break;
      case CYDatasTableCol::Name     : dtc->setLabel(tr("Name"))         ; break;
      case CYDatasTableCol::Help     : dtc->setLabel(tr("Help"))         ; break;
      case CYDatasTableCol::Phys     : dtc->setLabel(tr("Phys"))         ; break;
      case CYDatasTableCol::Elec     : dtc->setLabel(tr("Elec"))         ; break;
      case CYDatasTableCol::ADC      : dtc->setLabel(tr("ADC"))          ; break;
      case CYDatasTableCol::Reset    : dtc->setLabel(tr("Reset"))        ; break;
      case CYDatasTableCol::Partial  : dtc->setLabel(tr("Partial"))      ; break;
      case CYDatasTableCol::Note     : dtc->setLabel(tr("Note"))         ; break;
      case CYDatasTableCol::Control  : dtc->setLabel(tr("Control"))      ; break;
      case CYDatasTableCol::Addr     : dtc->setLabel(tr("Addr"))         ; break;
      case CYDatasTableCol::Threshold: dtc->setLabel(tr("Threshold"))    ; break;

      default: break;
    }
    mPlot->setHorizontalHeaderItem(dtc->id, dtc->header());
  }
}


/*! Supprime toutes les données
    \fn CYDatasTable::removeDatas()
 */
void CYDatasTable::removeDatas()
{
  while ( !mRows.isEmpty() )
    removeData( mRows.count()-1 );
}


/*! Mise à jour du format
    \fn CYDatasTable::updateFormat()
 */
void CYDatasTable::updateFormat()
{
  mUpdatingFormat = true;
  settings();
  mSetup->linkDatas();
  mSetup->apply();
  mUpdatingFormat = false;
}

/*! @return \a true si connexion de la/les donnée(s) de l'afficheur est bien en place.
    \fn CYDatasTable::connected()
 */
bool CYDatasTable::connected()
{
  QMapIterator<int, CYDatasTableRow *> ir(mRows);
  ir.toFront();
  while (ir.hasNext())
  {
    ir.next();
    CYDatasTableRow *dtr = ir.value();
    if (dtr)
      return dtr->isOk(false);
  }
  return false;
}


void CYDatasTable::exportTable( bool saveAs )
{
  Q_UNUSED(saveAs)
}


void CYDatasTable::exportPDF(QString fileName)
{
  QPrinter *printer = new QPrinter();

  CYProject *prj=0;
  if (core)
    prj = core->project();
  if (prj)
  {
    QDir dir(tr("%1/table").arg(prj->testDirPath()));
    core->mkdir(dir.path());
    if (fileName.isEmpty())
      fileName = QFileDialog::getSaveFileName(this, "", dir.absolutePath(), QString("*.pdf"));
    if (fileName.isEmpty())
      return;
  }
  else
    fileName = tr("%1/table.pdf").arg(core->baseDirectory());

  printer->setOutputFormat(QPrinter::PdfFormat);
  printer->setOutputFileName(fileName);
  printer->setColorMode(QPrinter::Color);
  printer->setPageOrientation(QPageLayout::Landscape);

  // Donne un nom au document
  QString docName = title();

  // Le titre de la table peut être multi-lignes, mais le format PostScript ne peut le supporter.
  // Nous remplaçons donc chaque "\n"
  docName.replace(QRegExp(QString::fromLatin1("\n")), QString(" -- "));
  printer->setDocName(docName);

  // Donne le nom du créateur
  printer->setCreator(cyapp->caption());

  // construit un dessin pour remplir l'objet printer.
  QPainter painter;

  //-------------------------------------------------------------------------------------
  // Début du dessin
  painter.begin(printer);

  int nb = 0;
  int num_page = 0;

  mPrintRows = 0;
  while (nb<nbRows())
  {
    QFont font = painter.font();
    QPen pen = painter.pen();
    num_page++;
    if (num_page>1)
      printer->newPage();
    mPrintNewPage=true;
    printPage(&painter, printer, num_page, nb);
    painter.setFont(font);
    painter.setPen(pen);
  }

  QUrl url = UrlFromUserInput(fileName);
  // fin du dessin, celui-ci envoye automatiquement les données à l'imprimante
  //-------------------------------------------------------------------------------------
  if (!painter.end())
    CYMessageBox::error(this, tr("Can't print %1").arg(url.fileName()));
  else
  {
    QUrl url = UrlFromUserInput(fileName);
    if (!QDesktopServices::openUrl(url))
    {
      CYMessageBox::error(this, tr("Can't open %1").arg(url.fileName()));
      return;
    }
  }
}

void CYDatasTable::printPage(QPainter *p, QPrinter *printer, int &num_page, int &nb)
{
  QRectF r, rTmp, rGraph;

  // abandon si la taille du capture est nul ou si l'une des marges d'impression est nulle
  if ((printer->width() == 0) || (printer->height() == 0) || this->size().isNull())
    return;

  // Prise en compte des marges d'impression
  r = printer->pageRect(QPrinter::DevicePixel);
  // correction pageRect trop grand
  r.setWidth(r.width()-printer->pageLayout().margins().right()*2);
  r.setHeight(r.height()-printer->pageLayout().margins().bottom()*2);

  if (r.isNull()) return;

  // Les opérations QPainter pour la page i
  rTmp = r;

  // Création de l'en-tête
  printHeader(p, &rTmp);

  // Création du pied de page
  printFooter(p, &rTmp, num_page);

  // Création de la zone d'affichage des infos supplémentaires
  if (core)
    core->printInfo(p, &rTmp);

  // Création de la zone graphique
  rGraph.setRect(rTmp.left(), rTmp.top(), rTmp.width(), rTmp.height());

  // impression des libellés des colonnes
  if (mPrintNewPage)
  {
    printRow(p, &rGraph, nb);
    mPrintNewPage=false;
  }

  // impression des lignes du tableau
  while (nb<nbRows())
  {
    if (!printRow(p, &rGraph, nb))
      return; // passage à la page suivante
    nb++;
  }
}

void CYDatasTable::printHeader(QPainter *p, QRectF *r)
{
  QRectF rect;
  QRectF rHeader = QRectF(r->left(), r->top(), r->width(), r->height());

  if (core)
  {
    int wlogo    = 0;
    int hlogo    = 0;
    int htext    = 0;
    int hbench   = 0;
    int hproject = 0;

    p->setBackground(Qt::blue);
    QPixmap logo = core->customerLogo();
    // force la taille à 64
    logo = logo.scaledToHeight(64, Qt::SmoothTransformation);
    p->drawPixmap(rHeader.left(), rHeader.top(), logo);
    wlogo = logo.width();
    hlogo = logo.height();

    QString project;
    CYProject *prj = core->project();
    if (prj)
      project = prj->testDirPath();
    p->drawText(QRectF(rHeader.left()+wlogo, rHeader.top(), rHeader.width()-wlogo, hlogo),
                Qt::AlignTop|Qt::AlignCenter|Qt::TextWordWrap, project, &rect);
    hproject = rect.height();

    QString bench = core->printingHeader()+ " - " + core->customerRef() + "\n" + core->testType();
    p->drawText(QRectF(rHeader.left()+wlogo, rHeader.top()+hproject, rHeader.width()-wlogo, hlogo),
                Qt::AlignBottom|Qt::AlignCenter|Qt::TextWordWrap, bench, &rect);
    hbench = rect.height();


    // ajuste la hauteur du rectangle contenant l'en-tête
    htext = hbench + hproject + 20;
    rHeader.setHeight(qMax(hlogo, htext));
  }
  else
    rHeader.setHeight(0);

  // diminue la hauteur du rectangle disponible
  int height = r->height() - rHeader.height();
  r->setRect(r->left(), rHeader.bottom(), r->width(), height);
}

void CYDatasTable::printFooter(QPainter *p, QRectF *r, int page)
{
  QRectF rect;
  QRectF rFooter = QRectF(r->left(), r->bottom(), r->width(), r->height());

  if (core)
  {
    qreal h      = 0;
    qreal wlogo  = 0;
    qreal wbench = 0;
    qreal wpage  = 0;
    qreal wdate  = 0;

    // geometries du logo
    QPixmap logo;
    if (printDesignerLogo())
    {
      logo = core->designerLogo();
      // force la taille à 32
      logo = logo.scaledToHeight(32, Qt::SmoothTransformation);
      wlogo = logo.width()+2;
      h = logo.height()+2;
    }

    // geometries du numéro de page
    p->drawText(QRectF(rFooter.right()-wpage, rFooter.top(), rFooter.width()-wlogo-wbench, 0),
                Qt::AlignCenter, tr("Page %1").arg(99), &rect);
    wpage = rect.width();
    h = qMax(h, rect.height());

    // geometries de la date
    QString date(QDateTime::currentDateTime().toString(core->dateTimeFormat()));
    p->drawText(QRectF(rFooter.right()-wdate-wpage, rFooter.top(), rFooter.width()-wlogo-wbench, 0),
                Qt::AlignCenter, date, &rect);
    wdate = rect.width();
    h = qMax(h, rect.height());

    // geometries de la reference constructeur
    QString designerRef;
    if (printDesignerRef())
    {
      designerRef = core->designerRef();
      p->drawText(QRectF(rFooter.left()+wlogo, rFooter.top(), rFooter.width()-wlogo-wdate-wpage, 0),
                  Qt::AlignCenter|Qt::TextWordWrap, designerRef, &rect);
      wbench = rect.width();
      h = qMax(h, rect.height());
    }

    // geometries du message d'etat du process
    QString state = core->processState();
    QString R = state.section('~', 0, 0);
    QString G = state.section('~', 1, 1);
    QString B = state.section('~', 2, 2);
    // TODO QWT
//    QColor color = QwtPlotRenderer().color(QColor(R.toInt(), G.toInt(), B.toInt()), QwtPlotRenderer::Curve);
    QColor color = QColor(R.toInt(), G.toInt(), B.toInt());
    state = state.section('~', 3, 3);
    p->drawText(QRectF(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.width()-wlogo-wdate-wbench, 0),
                Qt::AlignCenter|Qt::TextWordWrap, state, &rect);
    h = qMax(h, rect.height());

    // repositionne le rectangle pied de page
    rFooter.setRect(r->left(), r->bottom()-h, r->width(), r->height());

    // dessine le cadre
    rFooter.setHeight(h);
    p->drawRect(rFooter);

    // dessine le logo
    if (printDesignerLogo())
      p->drawPixmap(rFooter.left()+1, rFooter.top()+1, logo);

    // dessine la reference constructeur
    if (printDesignerRef())
    {
      p->drawLine(rFooter.left()+wlogo, rFooter.top(), rFooter.left()+wlogo, rFooter.bottom());
      p->drawText(QRectF(rFooter.left()+wlogo, rFooter.top(), wbench, h),
                  Qt::AlignCenter|Qt::TextWordWrap, designerRef, &rect);
    }

    // geometries du message d'etat du process
    p->drawLine(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.left()+wlogo+wbench, rFooter.bottom());
    QPointF procTopLeft = QPointF(rFooter.left()+wlogo+wbench, rFooter.top());
    QPointF procBottomLeft = QPointF(rFooter.left()+wlogo+wbench, rFooter.bottom());
    qreal procWidth = rFooter.width()-(wlogo+wbench+wdate+wpage);
    QRectF procRect=QRectF(procTopLeft, QSize(procWidth, rFooter.height()));
    QLinearGradient linearGrad(procTopLeft, procBottomLeft);
    linearGrad.setColorAt(0, Qt::white);
    linearGrad.setColorAt(0.5, Qt::black);
    linearGrad.setColorAt(1, Qt::white);
    QBrush brush(linearGrad);
    p->fillRect(procRect, brush);

    QPen oldPen = p->pen();
    QPen newPen;
    newPen.setColor(color);
    p->setPen(newPen);
    p->drawText(QRectF(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.width()-wlogo-wdate-wbench-wpage, h),
                Qt::AlignCenter|Qt::TextWordWrap, state, &rect);
    p->setPen(oldPen);

    // dessine la date
    p->drawLine(rFooter.right()-wdate-wpage, rFooter.top(), rFooter.right()-wdate-wpage, rFooter.bottom());
    p->drawText(QRectF(rFooter.right()-wdate-wpage, rFooter.top(), wdate, h),
                Qt::AlignCenter, date, &rect);

    // dessine le numéro de page
    p->drawLine(rFooter.right()-wpage, rFooter.top(), rFooter.right()-wpage, rFooter.bottom());
    p->drawText(QRectF(rFooter.right()-wpage, rFooter.top(), wpage, h),
                Qt::AlignCenter, tr("Page %1").arg(page), &rect);
  }
  else
    rFooter.setHeight(0);

  // diminue la hauteur du rectangle disponible
  int height = r->height() - rFooter.height();
  r->setRect(r->left(), r->top(), r->width(), height);
}

bool CYDatasTable::printRow(QPainter *p, QRectF *r, int numRow)
{
  QString text;
  qreal hLine=mPrintHeightRow;
  qreal wLine=0;
  qreal wOffset=0;
  qreal wCell=0;
  int nbCell=0;
  int cptCell=0;
  QPen oldPen = p->pen();

  CYDatasTableRow *dtr = row(numRow);
  QMapIterator<int, CYDatasTableCol *> ic(mCols);

  // calcul la taille totale des largeurs des colonnes affichées
  ic.toFront();
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if ( dtc->shown() && dtc->width())
    {
      nbCell++;
      wLine+=dtc->width();
    }
  }

  bool ok;
  QRectF boundingRect;
  // calcule la hauteur de ligne
  ic.toFront();
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if ( dtc->shown() && wLine )
    {
      if (mPrintNewPage)
      {
        // libellé de la colonne
        text = dtc->label();
      }
      else if (dtr->displayCell(dtc->id))
      {
        // valeur afficheur dans cellule
        CYDatasTableDisplayCell *cell = dtr->displayCell(dtc->id);
        text = cell->text(ok);
      }
      else
      {
        // texte direct dans cellule
        QTableWidgetItem *item = mPlot->item(dtr->id, dtc->id);
        if (item)
          text = item->text();
      }
      wCell = (r->width()*dtc->width())/wLine;
      QRectF rect = QRectF(r->left()+wOffset, r->top(), wCell, 0);
      wOffset+=wCell;
      if (mPrintNewPage)
        p->drawText(rect, Qt::AlignCenter|Qt::TextWordWrap, text, &boundingRect);
      else
        p->drawText(rect, Qt::AlignVCenter|Qt::AlignLeft|Qt::TextWordWrap, text, &boundingRect);
      hLine=qMax(hLine, boundingRect.height());
    }
  }

  if (hLine>r->height()) // passage à la page suivante
  {
    if (mPrintNewPage)
        CYFATAL
    else
      mPrintNewPage=true;
    return false;
  }

  p->setPen(oldPen);
  wOffset=0;
  // dessine le texte de chaque cellule de la ligne
  ic.toFront();
  while (ic.hasNext())
  {
    ic.next();
    CYDatasTableCol *dtc = ic.value();
    if ( dtc->shown() && dtc->width() )
    {
      cptCell++;
      if (mPrintNewPage)
      {
        // libellé de la colonne
        text = dtc->label();
      }
      else if (dtr->displayCell(dtc->id))
      {
        // valeur afficheur dans cellule
        CYDatasTableDisplayCell *cell = dtr->displayCell(dtc->id);
        text = cell->text(ok);
      }
      else
      {
        // texte direct dans cellule
        QTableWidgetItem *item = mPlot->item(dtr->id, dtc->id);
        if (item)
          text = item->text();
      }
      wCell = (r->width()*dtc->width())/wLine;
      QRectF rect = QRectF(r->left()+wOffset, r->top(), wCell, hLine);
      wOffset+=wCell;
      if (mPrintNewPage)
        p->drawText(rect, Qt::AlignCenter|Qt::TextWordWrap, text, &boundingRect);
      else
        p->drawText(rect, Qt::AlignVCenter|Qt::AlignLeft|Qt::TextWordWrap, text, &boundingRect);
      if (mPrintNewPage)
        p->drawRect(rect);
      else
      {
        p->drawLine(rect.topLeft(), rect.bottomLeft());
        if (cptCell==nbCell)
          p->drawLine(rect.topRight(), rect.bottomRight());
        p->drawLine(rect.bottomLeft(), rect.bottomRight());
      }
    }
  }
  p->setPen(oldPen);

  // diminue la hauteur du rectangle disponible
  r->setRect(r->left(), boundingRect.bottom(), r->width(), r->height() - hLine);
  return true;
}

int CYDatasTable::visualColumn(int logicalColumn) const
{
  return mPlot->visualColumn(logicalColumn);
}

int CYDatasTable::visualRow(int logicalRow) const
{
  return mPlot->visualRow(logicalRow);
}
