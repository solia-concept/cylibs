#include "cyscopescaledialog.h"
#include "ui_cyscopescaledialog.h"
#include "cyscopeaxis.h"
#include "cyscopeaxissetup.h"

CYScopeScaleDialog::CYScopeScaleDialog(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYScopeScaleDialog)
{
  ui->setupUi(this);
}

CYScopeScaleDialog::~CYScopeScaleDialog()
{
  delete ui;
}

void CYScopeScaleDialog::setScopeAxis(CYScopeAxis *axis)
{
  ui->scopeAxisSetup->setScopeAxis(axis);
}
