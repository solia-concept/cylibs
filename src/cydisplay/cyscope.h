/***************************************************************************
                          cyscope.h  -  description
                             -------------------
    début                  : lun fév 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYSCOPE_H
#define CYSCOPE_H

// QWT
#include "qwt_plot.h"
// CYLIBS
#include "cydisplay.h"

class QwtPlot;

class CYData;
class CYScopePlotter;
class CYScopeAnalyser;
class CYScopeCapture;
class CYScopeSetup;
class CYScopeAcquisition;
class CYScopeSignal;
class CYU8;

/** CYScope est un CYDisplay de type osciloscope classique : deux formats de données maximum avec leur axe respectif (gauche et droite).
  * @short Oscilloscope.
  * @author Gérald LE CLEACH.
  */

class CYScope : public CYDisplay
{
  Q_OBJECT
  Q_ENUMS( Mode )
  Q_PROPERTY( Mode mode READ mode WRITE setMode )
  Q_ENUMS( SamplingMode )
  Q_PROPERTY( SamplingMode samplingMode READ samplingMode WRITE setSamplingMode )
  Q_PROPERTY( bool printDesignerLogo READ printDesignerLogo WRITE setPrintDesignerLogo )
  Q_PROPERTY( bool printDesignerRef READ printDesignerRef WRITE setPrintDesignerRef )

public:
  /** @short Mode d'utilisation de l'oscilloscope. */
  enum Mode
  {
    /** Axe des X en temps. */
    Time,
    /** Axe des X associée à une donnée au choix parmis les données de la même connexion réseau. */
    XY,
  };

  /** @short Mode d'échantillonnage de l'oscilloscope. */
  enum SamplingMode
  {
    /** En continu. */
    Continuous,
    /** Par rafales. */
    Bursts,
  };

  /** Construit un oscilloscope.
    * @param parent   Widget parent.
    * @param name     Nom de l'oscilloscope.
    * @param title    Titre de l'oscilloscope.
    * @param nf       Pas de trame existante.
    * @param mode     Mode d'utilisation de l'oscilloscope.
    * @param sampling Mode d'échantillonnage de l'oscilloscope.
    */
  CYScope( QWidget *parent = 0, const QString &name = "scope", const QString &title = 0, bool nf = false, CYScope::Mode mode = Time, CYScope::SamplingMode acq = Continuous);

  virtual ~CYScope();

  /** @return le traceur de l'oscilloscope. */
  virtual CYScopePlotter *plot()
  {
    return mPlot;
  }

  /** Ajoute une donnée dans l'afficheur. */
  virtual bool addData( CYData *data, QString label = 0);
  /** Ajoute une donnée dans l'afficheur avec une couleur prédéfini. */
  virtual bool addData( QColor color, CYData *data, QString label = 0);
  /** Ajoute une donnée dans l'afficheur avec sa couleur d'affichage. */
  virtual CYScopeSignal *addDataY( CYData *data, QString label, QPen pen, double coefficient=1.0, QwtPlot::Axis yAxis = QwtPlot::yLeft, bool trigger=false );
  /** @return le premier signal en Y qui a pour donnée \a data */
  virtual CYScopeSignal *signalY( CYData *data );

  virtual bool removeData( uint idx );

  virtual bool createFromDOM( QDomElement &element, QStringList *ErrorList=nullptr );
  virtual bool addToDOM( QDomDocument &doc, QDomElement &element, bool save = true );

  virtual bool hasSettingsDialog() const
  {
    return ( true );
  }

  void settings();

  /** Gestion du menu surgissant.
    * @param pm0    Menu appelant s'il existe.
    * @param index  Index de l'entrée du menu appelant.
    * @param end    Fin du menu. */
  virtual int showPopupMenu( QMenu *pm0 = 0, int index = 0, bool end = false );

  virtual CYScopeSignal *addDataX( CYData *data, QString label = 0, double coefficient=1.0, QwtPlot::Axis xAxis = QwtPlot::xBottom );
  virtual CYScopeSignal *addDataY( CYData *data, QwtPlot::Axis yAxis = QwtPlot::yLeft, bool trigger=false);

  /** @return le mode d'utilisation. */
  CYScope::Mode mode() const;
  /** @return l'ancien mode d'utilisation. */
  CYScope::Mode oldMode() const
  {
    return mOldMode;
  }

  /** @return le mode d'échantillonnage. */
  CYScope::SamplingMode samplingMode() const;
  /** @return l'ancien mode d'échantillonnage. */
  CYScope::SamplingMode oldSamplingMode() const
  {
    return mOldSamplingMode;
  }

  /** @return \a true si le logo constructeur est à imprimer. */
  bool printDesignerLogo() const { return mPrintDesignerLogo; }
  /** @return \a true si la référence constructeur est à imprimer. */
  bool printDesignerRef() const { return mPrintDesignerRef; }

  /** @return le nombre de courbes sur l'axe Y gauche. */
  uint nbYLeft()
  {
    return mNbYLeft;
  }
  /** @return le nombre de courbes sur l'axe Y droit. */
  uint nbYRight()
  {
    return mNbYRight;
  }

  virtual CYScopeAcquisition * acquisition();

  /** Saisie le coefficient d'échelle du \a signal en Y. */
  virtual void setYCoef( CYScopeSignal *signal, double coef );

  /** Saisie l'offset d'échelle du \a signal en Y. */
  virtual void setYOffset( CYScopeSignal *signal, double offset );

  /*! @return \a true s'il s'agit d'un oscilloscope aux multiples formats
      \fn CYScope::hasMultipleFormats()
    */
  bool hasMultipleFormats()
  {
    return mScopeMultiple;
  }

  /**
   * Affecte l'oscillocope comme spécifique à la prévisualisation d'essai
   * avec des buffers dynamiques en fonction des consignes à visualiser.
   */
  inline void setScopePreview(bool val) { mScopePreview = val; }

  /**
   * @return trui si l'oscilloscope est spécifique à la prévisualisation d'essai.
   * @see setScopePreview
   */
  inline bool scopePreview() { return mScopePreview; }

  virtual void setTitle(const QString &title);


  /*! @return \a TRUE si le trigger est activé
      \fn CYScope::triggerIsEnabled()
    */
  virtual bool triggerIsEnabled();

  /** @return la taille du tableau de valeurs bufferisé des données affichées.
   * Vaut -1 dans le cas d'une bufferisation temporelle. */
  int tableBufferSize();

public slots:
  void applySettings();
  CYScopeAnalyser *analyseCurve();
  CYScopeAnalyser *analyseCurve(QString title, bool gui=true);
  void printCurve();
  /** Supprime toutes données de l'afficheur. */
  void removeDatas();
  /** Rafraîchit l'afficheur. */
  virtual void refresh();
  /** Démarre le rafraîchissement. */
  virtual void startRefresh();
  /** Arrête le rafraîchissement. */
  virtual void stopRefresh();
  /** Saisie le mode d'utilisation. */
  void setMode( const Mode mode );
  /** Saisie du mode d'échantillonnage. */
  void setSamplingMode( const SamplingMode acq );

  /** Saisir \a true pour activer l'impression du logo constructeur. */
  void setPrintDesignerLogo(const bool enable) { mPrintDesignerLogo=enable; }
  /** Saisir \a true pour activer l'impression de la référence constructeur. */
  void setPrintDesignerRef(const bool enable) { mPrintDesignerRef=enable; }

  virtual void resetCurves();
  virtual void exportPDF();
  virtual void exportCSV( bool saveAs = true );
  virtual void exportCSV( bool saveAs,  bool newCapture, bool open );
  virtual void updateFormat();

  virtual void setupTrigger();
  virtual void resetTrigger();

  /** Autorise ou non la bufferisation des valeurs. */
  void setEnableBuffering(bool val);

  /**
   * @brief Saisie une description de l'oscilloscope.
   * Cette description est affichée en aide en bulle de l'oscilloscope.
   */
  void setDescription(const QString & txt);
  /**
   * @return description de l'oscilloscope.
   */
  inline QString description() { return mDescription; }


protected: // Protected methods
  virtual void init( CYScope::Mode mode, CYScope::SamplingMode sampling);
  virtual void resizeEvent( QResizeEvent * );

protected:  // Private attributes
  CYScopePlotter *mPlot;
  CYScopeSetup *mSetup;

  uint mNbYLeft;
  uint mNbYRight;

  Mode mOldMode;
  SamplingMode mOldSamplingMode;

  bool mPrintDesignerLogo;
  bool mPrintDesignerRef;

  bool mScopeMultiple;

  bool mScopePreview;

  QString mDescription;
};

#endif
