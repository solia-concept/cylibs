//
// C++ Implementation: cydisplaysimplesetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cydisplaysimplesetup.h"
#include "ui_cydisplaysimplesetup.h"

// QT
#include <QTreeWidgetItemIterator>
// CYLIBS
#include "cycore.h"
#include "cydisplaysimple.h"
#include "cydataslistview.h"

CYDisplaySimpleSetup::CYDisplaySimpleSetup(CYDisplaySimple *parent, const QString &name)
  : CYDialog(0,name),
   mDisplaySimple(parent)
{
  ui=new Ui::CYDisplaySimpleSetup;
  ui->setupUi(this);

  init();
}


CYDisplaySimpleSetup::~CYDisplaySimpleSetup()
{
  delete ui;
}

/*!
    \fn CYDisplaySimpleSetup::init()
 */
void CYDisplaySimpleSetup::init()
{
  setUpdateIfVisible(false);

  setLocaleDB( mDisplaySimple->db() );

  for (int i = 0; i < mDisplaySimple->datas.count(); ++i)
    ui->datasEditList->addData(mDisplaySimple->datas.at(i));
}


/*!
    \fn CYDisplaySimpleSetup::apply()
 */
void CYDisplaySimpleSetup::apply()
{
  CYDatasListView *list;

  CYDialog::update();

  list = ui->datasEditList->list();
  mDisplaySimple->removeDatas();
  QTreeWidgetItemIterator it(list);
  while (*it)
  {
    mDisplaySimple->addData(core->findData(list->dataName(*it)),list->label(*it));
     ++it;
  }
  mDisplaySimple->applySettings();

  ui->applyButton->setDisabled( true );
}


/*! Affiche l'aide relative du manuel.
    \fn CYDisplaySimpleSetup::help()
 */
void CYDisplaySimpleSetup::help()
{
  core->invokeHelp( "cydoc", "display_simple_settings");
}

void CYDisplaySimpleSetup::setStylePage(int page)
{
  ui->widgetStack->setCurrentIndex(page);
}
