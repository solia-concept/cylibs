//
// C++ Implementation: cydatastablesetup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ui_cydatastablesetup.h"
#include "cydatastablesetup.h"

// CYLIBS
#include "cycore.h"
#include "cydatastable.h"
#include "cydatastablerow.h"
#include "cydataseditlist.h"

CYDatasTableSetup::CYDatasTableSetup(CYDatasTable *parent, const QString &name)
  : CYDialog(0,name),
   mDatasTable( parent )
{
  ui=new Ui::CYDatasTableSetup;
  ui->setupUi(this);

  init();
}


CYDatasTableSetup::~CYDatasTableSetup()
{
  delete ui;
}

/*!
    \fn CYDatasTableSetup::init()
 */
void CYDatasTableSetup::init()
{
  setUpdateIfVisible(false);

  setLocaleDB( mDatasTable->db() );

  QMapIterator<int, CYDatasTableRow *> ir(mDatasTable->mRows);
  ir.toFront();
  while (ir.hasNext())
  {
    ir.next();
    CYDatasTableRow *dtr = ir.value();
    ui->datasEditList->addData(dtr, dtr->pos()+1);
  }
}


/*!
    \fn CYDatasTableSetup::apply()
 */
void CYDatasTableSetup::apply()
{
//   CYDatasListView *list;

  CYDialog::update();

//   list = ui->datasEditList->list;
//   mDatasTable->removeDatas();
//   for (QListViewItemIterator it(list); it.current(); ++it)
//     mDatasTable->addData(core->findData(list->dataName(it.current())), list->label(it.current()));

  mDatasTable->applySettings();

  ui->applyButton->setDisabled( true );
}


/*! Affiche l'aide relative du manuel.
    \fn CYDatasTableSetup::help()
 */
void CYDatasTableSetup::help()
{
  core->invokeHelp( "cydoc", "display_datastable_settings");
}

CYDatasListView *CYDatasTableSetup::dataEditList()
{
  return ui->datasEditList->list();
}
