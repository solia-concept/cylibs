/***************************************************************************
                          cyscopeprinter.cpp  -  description
                             -------------------
    début                  : Tue Mar 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyscopeprinter.h"

// ANSI
#include <unistd.h>
// QT
#include <QProcess>
#include <QFileDialog>
#include <QPixmap>
#include <QPaintDevice>
#include <QPrintDialog>
#include <QDesktopServices>
#include <QPageLayout>
// QWT
#include "qwt_math.h"
#include "qwt_plot_layout.h"
// CYLIBS
#include "cycore.h"
#include "cyscopecapture.h"
#include "cyscopeanalyser.h"
#include "cyproject.h"
#include "cy.h"
#include "cyapplication.h"
#include "cymessagebox.h"

CYScopePrinter::CYScopePrinter(CYScope *scope, const QString &name)
  : QWidget(scope)
{
  setObjectName(name);

  // Création du nouveau graphe
  capture = new CYScopeCapture(scope, (QWidget *)scope->parent(), "scopefixed", scope->plot()->getTimerInterval(), 0, scope->plot()->modeVal(), scope->plot()->samplingModeVal());

  // Recopie les données du graphe parent
  capture->copy(scope->plot());
  capture->captureCurves(scope->plot());
  capture->copy(scope->plot());
  isAnalyse = false;

  capture->replot();
}

CYScopePrinter::CYScopePrinter(CYScopeAnalyser *dlg, CYScopeCapture *s, const QString &name)
  : QWidget(dlg)
{
  setObjectName(name);

  // Création du nouveau graphe
  capture = s;
  analyse = dlg;
  isAnalyse = true;

  capture->replot();
}

CYScopePrinter::~CYScopePrinter()
{
}

void CYScopePrinter::print()
{
  QPrinter *printer = new QPrinter();

  CYProject *prj=0;
  if (core)
    prj = core->project();
  if (prj)
  {
    QDir dir(tr("%1/curves").arg(prj->testDirPath()));
    core->mkdir(dir.path());
    int cpt = dir.entryList(QDir::Files).count();

    QString fileName = tr("%1/curve_%2.ps").arg(dir.path()).arg(cpt);
    printer->setOutputFileName(fileName);
  }
  else
    printer->setOutputFileName(tr("%1/curve").arg(core->baseDirectory()));

  printer->setColorMode(QPrinter::Color);
  printer->setPageOrientation(QPageLayout::Landscape);

  // Affiche la boîte de dialogue d'impression
  QPrintDialog dialog(printer, this);
  if (dialog.exec())
  {
    // Donne un nom au document
    QString docName = capture->QwtPlot::title().text();

    // Le titre du graphe peut être multi-lignes, mais le format PostScript ne peut le supporter.
    // Nous remplaçons donc chaque "\n"
    docName.replace(QRegExp(QString::fromLatin1("\n")), QString(" -- "));
    printer->setDocName(docName);

    // Donne le nom du créateur
    printer->setCreator(cyapp->caption());

    // construit un dessin pour remplir l'objet printer.
    QPainter painter;

    //-------------------------------------------------------------------------------------
    // Début du dessin
    painter.begin(printer);

    int nb = 0;
    int num_page = 0;

    while (nb<capture->ySignal.count())
    {
      QFont font = painter.font();
      QPen pen = painter.pen();
      num_page++;
      if (num_page>1)
        printer->newPage();
      newPage(&painter, printer, num_page, nb);
      painter.setFont(font);
      painter.setPen(pen);
    }

    // fin du dessin, celui-ci envoye automatiquement les données à l'imprimante
    painter.end();
    //-------------------------------------------------------------------------------------
  }
}

void CYScopePrinter::printPDF()
{
  printPDF("");
}

void CYScopePrinter::printPDF(QString fileName)
{
  QPrinter *printer = new QPrinter();

  CYProject *prj=0;
  if (core)
    prj = core->project();
  if (prj)
  {
    QDir dir(tr("%1/curves").arg(prj->testDirPath()));
    core->mkdir(dir.path());
    if (fileName.isEmpty())
      fileName = QFileDialog::getSaveFileName(this, tr("Saving under"), dir.absolutePath(), QString("*.pdf"));
    if (fileName.isEmpty())
      return;
  }
  else
    fileName = tr("%1/curve.pdf").arg(core->baseDirectory());

  printer->setOutputFormat(QPrinter::PdfFormat);
  printer->setOutputFileName(fileName);
  printer->setColorMode(QPrinter::Color);
  printer->setPageOrientation(QPageLayout::Landscape);

  // Donne un nom au document
  QString docName = capture->QwtPlot::title().text();

  // Le titre du graphe peut être multi-lignes, mais le format PostScript ne peut le supporter.
  // Nous remplaçons donc chaque "\n"
  docName.replace(QRegExp(QString::fromLatin1("\n")), QString(" -- "));
  printer->setDocName(docName);

  // Donne le nom du créateur
  printer->setCreator(cyapp->caption());

  // construit un dessin pour remplir l'objet printer.
  QPainter painter;

  //-------------------------------------------------------------------------------------
  // Début du dessin
  painter.begin(printer);

  int nb = 0;
  int num_page = 0;

  while (nb<capture->ySignal.count())
  {
    QFont font = painter.font();
    QPen pen = painter.pen();
    num_page++;
    if (num_page>1)
      printer->newPage();
    newPage(&painter, printer, num_page, nb);
    painter.setFont(font);
    painter.setPen(pen);
  }

  QUrl url = UrlFromUserInput(fileName);
  // fin du dessin, celui-ci envoye automatiquement les données à l'imprimante
  //-------------------------------------------------------------------------------------
  if (!painter.end())
    CYMessageBox::error(this, tr("Can't print %1").arg(url.fileName()));
  else
  {
    if (!QDesktopServices::openUrl(url))
    {
      CYMessageBox::error(this, tr("Can't open %1").arg(url.fileName()));
      return;
    }
  }
}

void CYScopePrinter::newPage(QPainter *p, QPrinter *printer, int &num_page, int &nb)
{
  QRectF r, rTmp, rGraph;

  // abandon si la taille du capture est nul ou si l'une des marges d'impression est nulle
  if ((printer->width() == 0) || (printer->height() == 0) || this->size().isNull())
    return;

  // Prise en compte des marges d'impression
  r = printer->pageRect(QPrinter::DevicePixel);
  // correction pageRect trop grand
  r.setWidth(r.width()-printer->pageLayout().margins().right()*2);
  r.setHeight(r.height()-printer->pageLayout().margins().bottom()*2);

//  r = printer->pageLayout().paintRect();
  if (r.isNull()) return;

  // Les opérations QPainter pour la page i
  rTmp = r;

  // Création de l'en-tête
  header(p, &rTmp);

  // Création du pied de page
  footer(p, &rTmp, num_page);

  // Création de la zone d'affichage des infos supplémentaires
  if (core)
    core->printInfo(p, &rTmp);

  // Création de la zone d'affichage des légendes
  if (isAnalyse)
  {
    analyse->printLegends(p, &rTmp, rTmp.top(), nb); // avec valeurs / curseurs
  }
  else
  {
    capture->enableOutline(false);
    legends(p, &rTmp);
    nb = capture->ySignal.count();
  }

  // Création de la zone graphique
  rGraph.setRect(rTmp.left(), rTmp.top(), rTmp.width(), rTmp.height());
  capture->printCurves(p, &rGraph);
}

void CYScopePrinter::header(QPainter *p, QRectF *r)
{
  QRectF rect;
  QRectF rHeader = QRectF(r->left(), r->top(), r->width(), r->height());

  if (isAnalyse)
    rHeader.setWidth(rHeader.width()-capture->plotLayout()->legendRatio()*rHeader.width());

  if (core)
  {
    int wlogo    = 0;
    int hlogo    = 0;
    int htext    = 0;
    int hbench   = 0;
    int hproject = 0;

    QPixmap logo = core->customerLogo();
    p->drawPixmap(rHeader.left(), rHeader.top(), logo);
    wlogo = logo.width();
    hlogo = logo.height();

    QString project;
    CYProject *prj=0;
    if (core)
      prj = core->project();
    if (prj)
      project = prj->testDirPath();
    p->drawText(QRectF(rHeader.left()+wlogo, rHeader.top(), rHeader.width()-wlogo, hlogo),
                Qt::AlignTop|Qt::AlignCenter|Qt::TextWordWrap, project, &rect);
    hproject = rect.height();

    QString bench = core->printingHeader()+ " - " + core->customerRef() + " - " + core->testType();
    p->drawText(QRectF(rHeader.left()+wlogo, rHeader.top(), rHeader.width()-wlogo, hlogo),
                Qt::AlignBottom|Qt::AlignCenter|Qt::TextWordWrap, bench, &rect);
    hbench = rect.height();


    // ajuste la hauteur du rectangle contenant l'en-tête
    htext = hbench + hproject;
    rHeader.setHeight(qMax(hlogo, htext));
  }
  else
    rHeader.setHeight(0);

  // diminue la hauteur du rectangle disponible
  int height = r->height() - rHeader.height()-10;
  r->setRect(r->left(), rHeader.bottom()+10, r->width(), height);
}

void CYScopePrinter::footer(QPainter *p, QRectF *r, int page)
{
  QRectF rect;
  QRectF rFooter = QRectF(r->left(), r->bottom(), r->width(), r->height());

  if (core)
  {
    qreal h      = 0;
    qreal wlogo  = 0;
    qreal wbench = 0;
    qreal wpage  = 0;
    qreal wdate  = 0;
    qreal margin = 2;

    // geometries du logo
    QPixmap logo;
    if (capture->scope()->printDesignerLogo())
    {
      logo = core->designerLogo();
      wlogo = logo.width()+margin;
      h = logo.height()+margin;
    }

    // geometries du numéro de page
    p->drawText(QRectF(rFooter.right()-wpage, rFooter.top(), rFooter.width()-wlogo-wbench, 0),
                Qt::AlignCenter, tr("Page %1").arg(99), &rect);
    wpage = rect.width()+margin;
    h = qMax(h, rect.height());

    // geometries de la date
    QString date(QDateTime::currentDateTime().toString(core->dateTimeFormat()));
    p->drawText(QRectF(rFooter.right()-wdate-wpage, rFooter.top(), rFooter.width()-wlogo-wbench, 0),
                Qt::AlignCenter, date, &rect);
    wdate = rect.width()+margin;
    h = qMax(h, rect.height());

    // geometries de la reference constructeur
    QString designerRef;
    if (capture->scope()->printDesignerRef())
    {
      designerRef = core->designerRef();
      p->drawText(QRectF(rFooter.left()+wlogo, rFooter.top(), rFooter.width()-wlogo-wdate-wpage, 0),
                  Qt::AlignCenter|Qt::TextWordWrap, designerRef, &rect);
      wbench = rect.width()+margin;
      h = qMax(h, rect.height());
    }

    // geometries du message d'etat du process
    QString state = core->processState();
    QString R = state.section('~', 0, 0);
    QString G = state.section('~', 1, 1);
    QString B = state.section('~', 2, 2);
    // TODO QWT
//    QColor color = QwtPlotRenderer().color(QColor(R.toInt(), G.toInt(), B.toInt()), QwtPlotRenderer::Curve);
    QColor color = QColor(R.toInt(), G.toInt(), B.toInt());
    state = state.section('~', 3, 3);
    p->drawText(QRectF(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.width()-wlogo-wdate-wbench, 0),
                Qt::AlignCenter|Qt::TextWordWrap, state, &rect);
    h = qMax(h, rect.height());

    // repositionne le rectangle pied de page
    rFooter.setRect(r->left(), r->bottom()-h, r->width(), r->height());

    // dessine le cadre
    rFooter.setHeight(h);
    p->drawRect(rFooter);

    // dessine le logo
    if (capture->scope()->printDesignerLogo())
      p->drawPixmap(rFooter.left()+margin/2, rFooter.top()+margin/2, logo);

    // dessine la reference constructeur
    if (capture->scope()->printDesignerRef())
    {
      p->drawLine(rFooter.left()+wlogo, rFooter.top(), rFooter.left()+wlogo, rFooter.bottom());
      p->drawText(QRectF(rFooter.left()+wlogo, rFooter.top(), wbench, h),
                  Qt::AlignCenter|Qt::TextWordWrap, designerRef, &rect);
    }

    // geometries du message d'etat du process
    p->drawLine(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.left()+wlogo+wbench, rFooter.bottom());
    QPointF procTopLeft = QPointF(rFooter.left()+wlogo+wbench, rFooter.top());
    QPointF procBottomLeft = QPointF(rFooter.left()+wlogo+wbench, rFooter.bottom());
    qreal procWidth = rFooter.width()-(wlogo+wbench+wdate+wpage);
    QRectF procRect=QRectF(procTopLeft, QSize(procWidth, rFooter.height()));
    QLinearGradient linearGrad(procTopLeft, procBottomLeft);
    linearGrad.setColorAt(0, Qt::white);
    linearGrad.setColorAt(0.5, Qt::black);
    linearGrad.setColorAt(1, Qt::white);
    QBrush brush(linearGrad);
    p->fillRect(procRect, brush);

    QPen oldPen = p->pen();
    QPen newPen;
    newPen.setColor(color);
    p->setPen(newPen);
    p->drawText(QRectF(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.width()-wlogo-wdate-wbench-wpage, h),
                Qt::AlignCenter|Qt::TextWordWrap, state, &rect);
    p->setPen(oldPen);

    // dessine la date
    p->drawLine(rFooter.right()-wdate-wpage, rFooter.top(), rFooter.right()-wdate-wpage, rFooter.bottom());
    p->drawText(QRectF(rFooter.right()-wdate-wpage, rFooter.top(), wdate, h),
                       Qt::AlignCenter, date, &rect);

    // dessine le numéro de page
    p->drawLine(rFooter.right()-wpage, rFooter.top(), rFooter.right()-wpage, rFooter.bottom());
    p->drawText(QRectF(rFooter.right()-wpage, rFooter.top(), wpage, h),
                Qt::AlignCenter, tr("Page %1").arg(page), &rect);

    p->drawLine(rFooter.left()+wlogo, rFooter.top(), rFooter.left()+wlogo, rFooter.bottom());
    p->drawLine(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.left()+wlogo+wbench, rFooter.bottom());

    p->drawRect(rFooter);
  }
  else
    rFooter.setHeight(0);

  // diminue la hauteur du rectangle disponible
  int height = r->height() - rFooter.height();
  r->setRect(r->left(), r->top(), r->width(), height);
}

void CYScopePrinter::legends(QPainter *p, QRectF *r)
{
  QRectF rect;
  int nb = capture->ySignal.count();

  // COURBES
  qreal ml  = 5;   // marges légendes
  qreal wl1 = 35;  // largeur trait de legende courbe
  qreal wl2 = 0;   // largeur texte de legende courbe
  qreal wl  = 0;   // largeur legende courbe
  qreal hl  = 0;   // hauteur legende courbe
  // determine largeur d'un rectangle maximum contenant un texte de legende courbe
  // et la hauteur totale des textes de legende courbe
  for (int i=0; i<nb; i++)
  {
    QString curve = capture->curveLegend(i);
    p->drawText(QRectF(0, 0, r->width()-wl1, 0),
                Qt::AlignTop|Qt::AlignLeft|Qt::TextWordWrap, curve, &rect);
    wl2 = qMax(wl2, rect.width());
    hl = qMax(hl, rect.height());
  }
  wl = 2*ml+wl1+wl2;

  // determine le nombre de colonnes
  int nb_col = (r->width()/(wl));
  if (!nb_col)
    nb_col=1;

  // determine le nombre de lignes
  int nb_row = nb/nb_col;
  if (nb%nb_col)
    nb_row++;

  // dessine pour les courbes
  int col=0;
  int row=0;
  for (int i=0; i<nb; i++)
  {
    int x0 = r->left() + ml + col*wl ;
    int y0 = r->bottom()-(nb_row-row)*(ml + hl);

    QRectF r1 = QRectF(x0, y0, r->width(), hl);

    // trait de légende d'une courbe
    QPen oldPen = p->pen();
    QPen newPen;
    newPen.setColor(QColor(capture->curveColor(i)));
    p->setPen(newPen);
    int hor = r1.bottom() + (r1.top()- r1.bottom())/2;
    p->drawLine(r1.left(), hor, r1.left()+wl1-5, hor);
    p->setPen(oldPen);

    // texte de légende d'une courbe
    QString curve = capture->curveLegend(i);
    p->drawText(QRectF(r1.left()+wl1, r1.top(), wl2, r1.height()),
                Qt::AlignTop|Qt::AlignLeft|Qt::TextWordWrap, curve, &rect);

    col++;
    if (col>=nb_col)
    {
      col=0;
      row++;
    }
  }

  r->setHeight(r->height()-nb_row*(ml + hl)-ml);
}
