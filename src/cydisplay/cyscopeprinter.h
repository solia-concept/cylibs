/***************************************************************************
                          cyscopeprinter.h  -  description
                             -------------------
    début                  : Tue Mar 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYSCOPEPRINTER_H
#define CYSCOPEPRINTER_H

// QT
#include <qwidget.h>
#include <qprinter.h>
// CYLIBS
#include "cyscope.h"

class CYScopeCapture;

/** @short Impression de courbes.
  * @author Gérald LE CLEACH.
  */

class CYScopePrinter : public QWidget
{
  Q_OBJECT

public:
  CYScopePrinter(CYScope *scope=0, const QString &name=0);
  CYScopePrinter(CYScopeAnalyser *dlg=0, CYScopeCapture *s=0, const QString &name=0);
  ~CYScopePrinter();
  /** Impression d'une page
    * @param page Nombre de pages déjà imprimés.
    * @param nb   Nombre de légendes de courbes déjà imprimés. */
  void newPage(QPainter *p, QPrinter *printer, int &page, int &nb);
  /** Création de l'en-tête. */
  void header(QPainter *p, QRectF *r);
  /** Création du pied de page. */
  void footer(QPainter *p, QRectF *r, int page);
  /** Impression des légendes des courbes. */
  void legends(QPainter *p, QRectF *r);

public slots:
  /** Impression */
  void print();
  /** Impression en PDF */
  void printPDF();
  /** Impression en PDF
    * @param fileName Nom du fichier. */
  void printPDF(QString fileName);

private:
  CYScopeCapture *capture;
  CYScopeAnalyser *analyse;

  bool isAnalyse;
};

#endif
