/***************************************************************************
                          cyscopeanalyser.cpp  -  description
                             -------------------
    début                  : ven mai 16 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyscopeanalyser.h"

// #include "cysimpleanalyser.xpm"
// #include "cydoubleanalyser.xpm"
// #include "cyfollowanalyser.xpm"

// QT
#include <QImage>
#include <QMenu>
// CYLIBS
#include "cycore.h"
#include "cyframe.h"
#include "cyscope.h"
#include "cyscopecapture.h"
#include "cyscopeprinter.h"
#include "cyscopeaxis.h"
#include "cyscopeacquisition.h"
#include "cyflag.h"


CYScopeAnalyser::CYScopeAnalyser(CYScope *scope, const QString &name, int index, const QString &label, bool gui)
  : CYWin(scope, name, index, label, Qt::Widget, false)
{
  Q_UNUSED(gui)
  setAttribute(Qt::WA_DeleteOnClose);
  mGUI = true;

  mFrame = new CYFrame(this, "mFrame");
  Q_CHECK_PTR(mFrame);
  setCentralWidget(mFrame);
  //   mFrame->CYTemplate::startTimer(200);

  // Création du nouveau graphe
  int timer = scope->plot()->getTimerInterval();
  mPlotter = new CYScopeCapture(scope, mFrame, "mPlotter", timer, 0, scope->plot()->modeVal(), scope->plot()->samplingModeVal());

  // Recopie les données du graphe source
  mPlotter->captureCurves(scope->plot());

  if (mGUI)
  {
    setPlainCaption(label);

    // Création d'un gestionnaire de positionnement de type grille
    mGrid = new QGridLayout(mFrame);

    mPlotter->setFocusPolicy(Qt::TabFocus);
    mPlotter->setFocus();
    mPlotter->setMinimumSize(300, 200);

    // mise en page du graphe pour le gestionnaire de positionnement
    QSizePolicy sz((QSizePolicy::Policy)7, (QSizePolicy::Policy)5);
    sz.setHorizontalStretch(1);
    sz.setWidthForHeight(mPlotter->sizePolicy().hasHeightForWidth());
    mPlotter->setSizePolicy(sz);

    // insertion du graphe dans le gestionnaire de positionnement
    mGrid->addWidget(mPlotter);

    CYAction *action;
    action = new CYAction(tr("&Quit"      ), "cywindow-close"  ,  Qt::CTRL+Qt::Key_Q , this  , SLOT(close())          , actionCollection(), "quit");
    action = new CYAction(tr("&Print..."  ), "cydocument-print",  Qt::CTRL+Qt::Key_P , this  , SLOT(print())          , actionCollection(), "fileprint");
#if CY_DEBUG_SAVE_CURVES > 0
    action = new CYAction(tr("&Save"      ), "filesave"   ,  Qt::CTRL+Qt::Key_S , this      , SLOT(save ())          , actionCollection(), "filesave");
#endif
    action = new CYAction(tr("&Export PDF"), "cyapplication-pdf", 0           , this  , SLOT(exportPDF())     , actionCollection(), "export_pdf");
    action = new CYAction(tr("E&xport CSV"), "cytext-csv"       , 0           , scope , SLOT(exportCSV())     , actionCollection(), "export_csv");


    action = new CYAction(tr("Auto zoom Y"), "cyzomm-original"    , 0           , mPlotter  , SLOT(zoom11())   , actionCollection(), "view_zoom_11");

    action = new CYAction(tr("&Zoom"      ), "cyzomm-select"    , 0           , mPlotter  , SLOT(zoom())   , actionCollection(), "view_zoom");
    connect(mPlotter, SIGNAL(enableActionZoomIn(bool)), action, SLOT(setEnabled(bool)));
//    action->setCheckable(true);

    action = new CYAction(tr("Zoom &In"   ), "cyzomm-in"   , 0           , mPlotter  , SLOT(zoomIn())        , actionCollection(), "view_zoom_in");
    connect(mPlotter, SIGNAL(enableActionZoomIn(bool)), action, SLOT(setEnabled(bool)));

    action = new CYAction(tr("Zoom &Out"  ), "cyzomm-out"   , 0           , mPlotter  , SLOT(zoomOut())       , actionCollection(), "view_zoom_out");
    connect(mPlotter, SIGNAL(enableActionZoomOut(bool)), action, SLOT(setEnabled(bool)));


    action = new CYAction(tr("&Permut cursors"), "cycursors", 0, mPlotter, SLOT(permutCursorsOnOff()), actionCollection(), "permut");

    updateGeometry();
  }

  // force l'activation des échelles Y nécessaires pour les curseurs
  QHashIterator<int, CYScopeAxis*> it(mPlotter->scopeAxis);
  while (it.hasNext())
  {
    it.next();
    it.value()->setShow(true);
  }

  mPlotter->refresh();
  mPlotter->initPosCursor();
}

CYScopeAnalyser::~CYScopeAnalyser()
{
}

void CYScopeAnalyser::createGUI(const QString &)
{
  CYWin::createGUI();

  mFileMenu = new QMenu( this );
  mFileMenu->setTitle(tr("&File"));
  menuBar()->addMenu( mFileMenu );
  actionCollection()->addActionTo("fileprint" , mFileMenu);
  actionCollection()->addActionTo("export_pdf", mFileMenu);
  actionCollection()->addActionTo("export_csv", mFileMenu);
  actionCollection()->addActionTo("quit"      , mFileMenu);

  mEditMenu = new QMenu( this );
  mEditMenu->setTitle(tr("&Edit"));
  menuBar()->addMenu( mEditMenu );
  actionCollection()->addActionTo("view_zoom_11" , mEditMenu);
  actionCollection()->addActionTo("view_zoom_in" , mEditMenu);
  actionCollection()->addActionTo("view_zoom_out", mEditMenu);
  actionCollection()->addActionTo("view_zoom"    , mEditMenu);
  mEditMenu->addSeparator();
  actionCollection()->addActionTo("permut"       , mEditMenu);

  QToolBar * mainTools = new QToolBar( this );
  mainTools->setWindowTitle( tr("Main Toolbar") );
  addToolBar(mainTools);
  actionCollection()->addActionTo("quit"         , mainTools);
  actionCollection()->addActionTo("fileprint"    , mainTools);
  actionCollection()->addActionTo("export_pdf"   , mainTools);
  actionCollection()->addActionTo("export_csv"   , mainTools);
  actionCollection()->addActionTo("view_zoom_11" , mainTools);
  actionCollection()->addActionTo("view_zoom_in" , mainTools);
  actionCollection()->addActionTo("view_zoom_out", mainTools);
  actionCollection()->addActionTo("view_zoom"    , mainTools);
  actionCollection()->addActionTo("permut"       , mainTools);
}

void CYScopeAnalyser::printLegends(QPainter *p, QRectF *r, int top, int &nb)
{
  mPlotter->printLegends(p, r, top, nb);
}

/*!
    \fn CYScopeAnalyser::save()
 */
void CYScopeAnalyser::save()
{
  /// @todo implement me
}


/*!
    \fn CYScopeAnalyser::nbCurves()
 */
int CYScopeAnalyser::nbCurves()
{
  return mPlotter->ySignal.count();
}

void CYScopeAnalyser::print()
{
  CYScopePrinter *printer = new CYScopePrinter(this, mPlotter, "CYScopePrinter");
  Q_CHECK_PTR(printer);
  printer->print();
}

void CYScopeAnalyser::exportPDF()
{
  CYScopePrinter *printer = new CYScopePrinter(this, mPlotter, "CYScopePrinter");
  Q_CHECK_PTR(printer);
  printer->printPDF();
}

void CYScopeAnalyser::exportPDF(QString fileName)
{
  CYScopePrinter *printer = new CYScopePrinter(this, mPlotter, "CYScopePrinter");
  Q_CHECK_PTR(printer);
  printer->printPDF(fileName);
}

void CYScopeAnalyser::exportCSV( bool saveAs )
{
  plot()->exportCurves(saveAs);
}

void CYScopeAnalyser::exportCSV( QString fileName )
{
  plot()->acquisition()->setForceFileName(fileName);
  plot()->exportCurves(false);
}

void CYScopeAnalyser::setCursorX1(double val)
{
  mPlotter->setCursorX1(val);
}

void CYScopeAnalyser::setCursorY1(double val)
{
  mPlotter->setCursorY1(val);
}

void CYScopeAnalyser::setCursorX2(double val)
{
  mPlotter->setCursorX2(val);
}

void CYScopeAnalyser::setCursorY2(double val)
{
  mPlotter->setCursorY2(val);
}
