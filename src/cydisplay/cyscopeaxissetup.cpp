//
// C++ Implementation: cyscopeaxissetup
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyscopeaxissetup.h"
#include "ui_cyscopeaxissetup.h"
#include "cyscopeaxis.h"
#include "cyflaginput.h"
#include "cynuminput.h"
#include "cytextlabel.h"
#include "cybuttongroup.h"

CYScopeAxisSetup::CYScopeAxisSetup(QWidget *parent, const QString &name)
 : CYWidget(parent, name), ui(new Ui::CYScopeAxisSetup)
{
 ui->setupUi(this);
}

CYScopeAxisSetup::~CYScopeAxisSetup()
{
  delete ui;
}

void CYScopeAxisSetup::setScopeAxis(CYScopeAxis *axis)
{
  mScopeAxis = axis;
  CYScopePlotter *plotter = mScopeAxis->plotter();
  connect(this, SIGNAL(validate()), plotter, SIGNAL(validate()));
  setLocaleDB(plotter->db());
  setGenericMark(mScopeAxis->objectName().toUtf8());
  ui->buttonGroup->setTitle(mScopeAxis->desc());

  connect( ui->logInput, SIGNAL(state(bool)), mScopeAxis, SLOT(setLog(bool)));
  connect( mScopeAxis, SIGNAL(limitsChanged()), ui->maxInput, SLOT(linkData()));
  connect( mScopeAxis, SIGNAL(limitsChanged()), ui->minInput, SLOT(linkData()));

  if ( mScopeAxis->isX() )
  {
    ui->maxInput->hide();
    ui->maxLabel->hide();
    ui->minInput->hide();
    ui->minLabel->hide();
    if (plotter->modeVal()==CYScope::Time)
    {
      ui->logInput->hide();
    }
  }
  else
  {
    ui->rightInput->hide();
    ui->rightLabel->hide();
    ui->leftInput->hide();
    ui->leftLabel->hide();
  }
}
