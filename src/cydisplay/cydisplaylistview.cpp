/***************************************************************************
                          cydisplaylistview.cpp  -  description
                             -------------------
    begin                : ven déc 17 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydisplaylistview.h"

// QT
#include <QMimeData>
// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "cydatasbrowser.h"
#include "cynethost.h"
#include "cynetlink.h"

CYDisplayListView::CYDisplayListView(QWidget *parent, const QString &name )
: QTreeWidget(parent)
{
  setObjectName(name);
  setAcceptDrops( true );

  QStringList labels;
  labels<<tr( "#" );
  labels<<tr( "Host" );
  labels<<tr( "Connection" );
  labels<<tr( "Group" );
  labels<<tr( "Label" );
  labels<<tr( "Unit" );
  labels<<tr( "Status" );
  labels<<tr( "Designer Name" );
  setHeaderLabels(labels);
  setMinimumSize( QSize( 300, 0 ) );
  setAllColumnsShowFocus( true );
  // TODO QT5
//  setItemMargin( 3 );
}

CYDisplayListView::~CYDisplayListView()
{
}

void CYDisplayListView::dragEnterEvent( QDragEnterEvent * ev )
{
  // TOCHECK QT5
//  ev->accept(QTextDrag::canDecode(ev));
  if (ev->mimeData()->hasFormat("text/plain"))
    ev->accept();
}

void CYDisplayListView::dropEvent( QDropEvent * ev )
{
  if (!core)
    return;

  if (ev->mimeData())
  {
    QString txt = ev->mimeData()->text();

    if (txt.isEmpty())
      return;

    bool group = false;
    if (txt.contains(tr("Group")))
      group = true;

    if (!group)
    {
      CYData *data=0;
      if ((data = core->findData(txt)) == 0)
        CYFATAL
      QString n = QString("%1").arg(topLevelItemCount() + 1, 2);
      QString status = data->isOk() ? tr("Ok") : tr("Error");
      QStringList labels;
      labels<<n;
      labels<<data->label();
      labels<<data->group();
      labels<<data->linksName();
      labels<<data->hostsName();
      labels<<data->unit();
      labels<<status;
      labels<<data->objectName();
      QTreeWidgetItem *lvi = new QTreeWidgetItem(this, labels);
      addTopLevelItem(lvi);
    }
    else
    {
      QString tmp;
      int pos;

      tmp = tr("Filter")+": ";
      pos = txt.indexOf(tmp)+tmp.length();
      txt = txt.right(txt.length()-pos);

      tmp = tr("Host")+": ";
      pos = txt.indexOf(tmp);
      QString filterName = txt.left(pos-1);
      CYDatasBrowser::Filter filter = (CYDatasBrowser::Filter)filterName.toInt();
      pos = txt.indexOf(tmp)+tmp.length();
      txt = txt.right(txt.length()-pos);

      tmp = tr("Link")+": ";
      pos = txt.indexOf(tmp);
      QString hostName = txt.left(pos-1);
      pos = txt.indexOf(tmp)+tmp.length();
      txt = txt.right(txt.length()-pos);

      tmp = tr("Group")+": ";
      pos = txt.indexOf(tmp);
      QString linkName = txt.left(pos-1);
      pos = txt.indexOf(tmp)+tmp.length();
      QString group = txt.right(txt.length()-pos);

      CYNetHost *host;
      QHashIterator<QString, CYNetHost *> ith(core->hostList);
      while (ith.hasNext())
      {
        ith.next();                   // must come first
        host = ith.value();
        CYNetLink *link;
        QHashIterator<QString, CYNetLink *> itl(host->linkList);
        while (itl.hasNext())
        {
          itl.next();                   // must come first
          link = itl.value();
          CYDB *db;
          QHashIterator<QString, CYDB *> itdb(link->dbList);
          while (itdb.hasNext())
          {
            itdb.next();                   // must come first
            db = itdb.value();
            if (db->host(hostName) && db->link(linkName))
            {
              CYData *data;
              QHashIterator<QString, CYData *> itd(db->datas);
              while (itd.hasNext())
              {
                itd.next();                   // must come first
                data = itd.value();
                if ( data->group().contains(group) &&
                     ( (filter == CYDatasBrowser::All ) ||
                       ( (filter == CYDatasBrowser::User) && (data->mode()==Cy::User) ) ||
                       ( (filter == CYDatasBrowser::Sys ) && (data->mode()!=Cy::User) ) ) )
                {
                  QString n = QString("%1").arg(topLevelItemCount() + 1, 2);
                  QString status = data->isOk() ? tr("Ok") : tr("Error");
                  QStringList labels;
                  labels<<n;
                  labels<<data->label();
                  labels<<data->group();
                  labels<<data->linksName();
                  labels<<data->hostsName();
                  labels<<data->unit();
                  labels<<status;
                  labels<<data->objectName();
                  QTreeWidgetItem *lvi = new QTreeWidgetItem(this, labels);
                  addTopLevelItem(lvi);
                }
              }
            }
          }
        }
      }
    }
  }
}
