//
// C++ Interface: cyscopetriggersetup
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSCOPETRIGGERSETUP_H
#define CYSCOPETRIGGERSETUP_H

#include <cywidget.h>

namespace Ui {
	class CYScopeTriggerSetup;
}

class CYScope;
class CYS16;

/** Widget de configuration du Trigger.
    @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYScopeTriggerSetup : public CYWidget
{
Q_OBJECT
public:
  CYScopeTriggerSetup(QWidget *parent = 0, QString name = 0);

  ~CYScopeTriggerSetup();

  void setScope(CYScope *scope);

public slots:
  void setSignal(int id);
  void apply();

protected:
    virtual void showEvent(QShowEvent *e);

protected:
  CYScope *mScope;

private: // Private attributes
  Ui::CYScopeTriggerSetup *ui;
};

#endif
