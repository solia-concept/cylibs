/***************************************************************************
                          cydatastablecol.cpp  -  description
                             -------------------
    begin                : ven nov 19 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydatastablecol.h"

CYDatasTableCol::CYDatasTableCol(Type type, int id, bool shown, int width, QString label)
: id(id),
  mType(type),
//  mPos(id),
  mShown(shown),
  mWidth(width),
  mLabel(label)
{
  mHeader = new QTableWidgetItem(label);
}

CYDatasTableCol::~CYDatasTableCol()
{
}

void CYDatasTableCol::setLabel(QString string)
{
  mLabel = string;
  mHeader->setText(mLabel);
}
