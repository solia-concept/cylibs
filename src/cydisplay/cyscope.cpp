/***************************************************************************
                          cyscope.cpp  -  description
                             -------------------
    début                  : lun fév 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyscope.h"

// QT
#include <QTreeWidgetItem>
// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "cyu8.h"
#include "cyf64.h"
#include "cys16.h"
#include "cyflag.h"
#include "cytime.h"
#include "cycolor.h"
#include "cynuminput.h"
#include "cyanalysesheet.h"
#include "cyanalysecell.h"
#include "cydisplayitem.h"
#include "cydisplaystyle.h"
#include "cyscopeplotter.h"
#include "cyscopesignal.h"
#include "cyscopecapture.h"
#include "cyscopeanalyser.h"
#include "cyscopeprinter.h"
#include "cyscopesetup.h"
#include "cydataslistview.h"
#include "cydataseditlist.h"
#include "cyscopeacquisition.h"
#include "cyscopeaxis.h"
#include "cymessagebox.h"

CYScope::CYScope( QWidget *parent, const QString &name, const QString &title, bool nf, CYScope::Mode mode, CYScope::SamplingMode sampling)
  : CYDisplay( parent, name, title, nf )
{
  mSetup = 0;
  mScopeMultiple=false;
  mScopePreview = false;
  init( mode, sampling);
}

void CYScope::init( CYScope::Mode m, CYScope::SamplingMode sampling)
{
  mType = Scope;
  mNbYLeft = 0;
  mNbYRight = 0;
  mPrintDesignerLogo = true;
  mPrintDesignerRef  = true;

  loadSettings();

  if ( analyseSheet() && analyseSheet()->inherits( "CYAnalyseSheetCapture" ) )
  {
    mPlot = new CYScopeCapture( this, mFrame, "CYScopePlotter", mRefreshTime->val(), mDB, m, sampling );
    setFocusProxy(mPlot);
  }
  else
  {
    if ( !mFrame )
    {
      mPlot = new CYScopePlotter( this, this, "CYScopePlotter", mRefreshTime->val(), mDB, m, sampling );
    }
    else
    {
      mPlot = new CYScopePlotter( this, mFrame, "CYScopePlotter", mRefreshTime->val(), mDB, m, sampling );
    }
    /*  connect(mPlot, SIGNAL(legendClicked(long)), SLOT(curveClicked(long)));*/
    connect( mPlot, SIGNAL( mouseDoubleClick() ), SLOT( analyseCurve() ) );
  }
  Q_CHECK_PTR( mPlot );
  connect( mPlot, SIGNAL( modified( bool ) ), SLOT( setModified( bool ) ) );
  connect( mPlot, SIGNAL( validate() ), SLOT( validate() ) );

  mBoxLayout->addWidget(mPlot);

  /* Tous les clics RMB dans le widget mPlot seront gérés par CYDisplay::eventFilter. */
  mPlot->installEventFilter( this );

  registerPlotterWidget( mPlot );

  /*  connect(mPlot, SIGNAL(legendClicked(long)), SLOT(curveClicked(long)));*/
  //   connect( mPlot, SIGNAL( modified( bool ) ), SLOT( setModified( bool ) ) );
  //   connect( mPlot, SIGNAL( plotMouseDoubleClick( const QMouseEvent & ) ), SLOT( analyseCurve() ) );

  setModified( false );
  adjustSize();

  startRefresh();
}

CYScope::~CYScope()
{
  delete mPlot;
}

bool CYScope::addData( CYData *data, QString label)
{
  return addData(mStyle->getDataColor( nbItem ), data, label);
}

bool CYScope::addData( QColor color, CYData *data, QString label)
{
  if ( !data )
    CYWARNING;

  if ( data && data->mode() != Cy::User )
  {
    CYMessageBox::error(this, tr("This data can not be put into an oscilloscope because it is not buffered!"));
    return false;
  }

  if ( data->tableBufferSize() )
  {
    // foçage en mode XY si la donnée est issue d'un tableau de valeur
    // soit une bufferisation intemporelle
    mPlot->setModeVal( CYScope::XY );
  }

  if ( mPlot->modeVal() == CYScope::Time )
  {
    if ( addDataY( data, label, QPen(color) ) )
      return true;
    else
      return false;
  }
  else if ( mPlot->modeVal() == CYScope::XY )
  {
    int res = CYMessageBox::warningYesNoCancel( mThis, tr( "On which axis do you want this data must to be ?" ), tr( "Axis selection" ), tr( "&Y" ), tr( "&X" ) );

    if ( res == CYMessageBox::Yes )
    {
      if ( addDataY( data, label, QPen(color) ) )
        return true;
      else
        return false;
    }
    if ( res == CYMessageBox::No )
    {
      if ( addDataX( data, 0 ) )
        return true;
      else
        return false;
    }
    else if ( res == CYMessageBox::Cancel )
      return false;
  }
  return false;
}

CYScopeSignal *CYScope::addDataY( CYData *data, QString label, QPen pen, double coefficient, QwtPlot::Axis yAxis, bool trigger)
{
  if ( !data )
  {
    CYWARNING;
    return 0;
  }

  if ( !data->isNum() )
  {
    CYMessageBox::sorry( this, tr( "This display cannot treat this type of data !" ) );
    return 0;
  }

  if ( data->mode() != Cy::User )
  {
    CYWARNINGTEXT( data->objectName() );
    return 0;
  }

  if ( datas.isEmpty() && ( mPlot->modeVal() == CYScope::Time ) )
  {
    if ( !data->isReadFast() || ( mPlot->samplingModeVal() == CYScope::Continuous ) )
    {
      mPlot->initTimeScale( data->format()->bufferSize( !data->isReadFast() ), data->format()->acquisitionTime( !data->isReadFast() ) );
    }
    else
    {
      mPlot->initTimeScale( data->format()->burstsBufferSize(), data->format()->acquisitionTime( false ), data->format()->burstsRatio() );
    }
  }
  else
  {
    foreach ( CYDisplayItem *d, datas )
    {
      if ( d->data->objectName() == data->objectName() )
      {
        if ( isVisible() )
          CYMessageBox::sorry( this, tr( "This data is already displayed in this oscilloscope." ) + "( " + data->objectName() + " )" );
        return 0;
      }
    }
  }
  CYScopeSignal *signal = mPlot->addCurve( data, ++nbItem, label, pen, coefficient, yAxis, trigger);

  if ( !signal )
  {
    nbItem--;
    return 0;
  }

  registerItem( signal );

  ++mNbYLeft;

  return signal;
}

CYScopeSignal *CYScope::signalY( CYData *data )
{
  foreach ( CYDisplayItem *d, datas )
  {
    if ( d->data == data )
      return (CYScopeSignal *)d;
  }
  return 0;
}

bool CYScope::removeData( uint idx )
{
  if ( idx >= mNbYLeft )
    CYFATAL

  mPlot->deleteCurve( idx );
  mNbYLeft--;
  CYDisplay::removeData( idx );

  mPlot->setToolTip( mPlot->tooltip() );
  setModified( true );
  return ( true );
}

void CYScope::refresh()
{
  if ( !mRefreshNotVisible && !isVisible() )
    return;

  if ( !core )
    return;

  refreshStatus();

  if ( core && !core->analyseRefreshEnabled() )
    return;

  mPlot->refresh();
}


void CYScope::stopRefresh()
{
  /** Si l'afficheur se trouve dans une table de données,
  c'est celle-ci qui lui donne l'ordre de rafraîchissement. */
  if ( !parent()->inherits( "CYDatasTableDisplayCell" ) )
  {
    if ( mCell && !mRefreshStop )
    {
      disconnect( mCell->sheet(), SIGNAL( refreshing() ), this, SLOT( refresh() ) );
    }
    else
    {
      disconnect( mRefreshTimer, SIGNAL( timeout() ), this, SLOT( refresh() ) );
      mRefreshTimer->stop();
    }
  }
  if ( !mRefreshStop )
  {
    mRefreshStop = true;
    setModified( true );
  }

  refreshStatus();
}

void CYScope::startRefresh()
{
  if ( mRefreshStop )
  {
    /** Si l'afficheur se trouve dans une table de données,
    c'est celle-ci qui lui donne l'ordre de rafraîchissement. */
    if (parent() && !parent()->inherits( "CYDatasTableDisplayCell" ) )
    {
      if ( mCell )
      {
        connect( mCell->sheet(), SIGNAL( refreshing() ), this, SLOT( refresh() ) );
      }
      else
      {
        connect( mRefreshTimer, SIGNAL( timeout() ), this, SLOT( refresh() ) );
        mRefreshTimer->start( mRefreshTime->val() );
      }
    }
    mRefreshStop = false;
    setModified( true );
  }
  else
  {
    /** Si l'afficheur se trouve dans une table de données,
    c'est celle-ci qui lui donne l'ordre de rafraîchissement. */
    if ( parent() && !parent()->inherits( "CYDatasTableDisplayCell" ) )
    {
      if ( !mCell )
      {
        mRefreshTimer->start( mRefreshTime->val() );
      }
    }
    setModified( true );
  }
  refreshStatus();
}

void CYScope::resizeEvent( QResizeEvent* )
{
  if ( !mFrame )
    mPlot->setGeometry( 0, 0, width(), height() );
  else
    mFrame->setGeometry( 0, 0, width(), height() );
}

CYScopeAnalyser *CYScope::analyseCurve()
{
  return analyseCurve(tr("CYLIX: Scope Capture Window"));
}

CYScopeAnalyser *CYScope::analyseCurve(QString title, bool gui)
{
  CYScopeAnalyser *analyse = new CYScopeAnalyser( this, "SCOPEANALYSER", 0, title, gui);
  Q_CHECK_PTR( analyse );
  if (gui)
  {
    analyse->createGUI();
    analyse->initHelpMenu();
    analyse->show();
  }
  return analyse;
}

void CYScope::printCurve()
{
  CYScopePrinter *printer = new CYScopePrinter( this, "CYScopePrinter" );
  Q_CHECK_PTR( printer );
  printer->print();
}

void CYScope::settings()
{
  mOldMode = mode();
  mOldSamplingMode = samplingMode();

  if (!mSetup)
  {
    mSetup = new CYScopeSetup( plot(), "CYScopeSetup" );
    Q_CHECK_PTR( mSetup );
    connect( mSetup, SIGNAL( applying() ), SLOT( applySettings() ) );
  }
  if ( !mUpdatingFormat )
    mSetup->exec();
}

void CYScope::applySettings()
{
  // Arrête le rafraîchissement des courbes.
  stopRefresh();

  plot()->updateFontSize();
  plot()->clearTriggerSignals();
  if ( mSetup && !( analyseSheet() && analyseSheet()->inherits( "CYAnalyseSheetCapture" ) ) )
  {
    removeDatas();
    CYDatasListView *list;

    list = mSetup->dataXList();
    QTreeWidgetItem *lvi = list->currentItem();
    if ( lvi )
      addDataX( core->findData( list->dataName( lvi ) ), list->label( lvi ), list->coef( lvi ), QwtPlot::xBottom );

    list = mSetup->dataYList();
    QList<QTreeWidgetItem*> lviToRemove;
    for ( QTreeWidgetItemIterator it( list ); (*it); ++it )
    {

      CYScopeSignal *signal;
      QTreeWidgetItem *lvi = (*it);
      if ( lvi->text( list->column( tr( "Axis" ) ) ) == tr( "Left" ) )
        signal = addDataY( core->findData( list->dataName( lvi ) ), list->label( lvi ), list->pen( lvi ), list->coef( lvi), QwtPlot::yLeft );
      else if ( lvi->text( list->column( tr( "Axis" ) ) ) == tr( "Right" ) )
        signal = addDataY( core->findData( list->dataName( lvi ) ), list->label( lvi ), list->pen( lvi ), list->coef( lvi), QwtPlot::yRight );
      else
        signal = addDataY( core->findData( list->dataName( lvi ) ), list->label( lvi ), list->pen( lvi ), list->coef( lvi) );
      if ( signal )
      {
        lvi->setText( list->column( tr( "Axis" ) ), signal->scopeAxis()->desc() );
      }
      else
        lviToRemove.append( lvi );
    }

    if (!lviToRemove.isEmpty())
    {
      lvi = lviToRemove.last();
      QListIterator<QTreeWidgetItem*> i(lviToRemove);
      i.toBack();
      while (i.hasPrevious())
      {
        QTreeWidgetItem *next = i.previous();
        // TOCHECK QT5
        list->removeItemWidget(lvi,0);
        lvi = next;
      }
    }
  }

  mPlot->applySettings();

  if ( mFrame )
  {
    mFrame->updateGeometry();
  }

  CYDisplay::applySettings();

  // Redémarre le rafraîchissement des courbes.
  startRefresh();
}

bool CYScope::createFromDOM( QDomElement &element, QStringList *ErrorList )
{
    Q_UNUSED(ErrorList)
  mCreatingFromDOM = true;
  removeDatas();

  if (compareVersionCYDOM("2.26")<0) // COMPATIBILITÉ ASCENDANTE < 2.26
  {
    mPlot->mLegendPosition->setVal(( bool )element.attribute( "labels", QString( "%1" ).arg( mPlot->mLegendPosition->def() ) ).toInt() );
    mPlot->mMajGrid->setVal(( bool )element.attribute( "majGrid", QString( "%1" ).arg( mPlot->mMajGrid->def() ) ).toUInt() );
    mPlot->mMinGrid->setVal(( bool )element.attribute( "minGrid", QString( "%1" ).arg( mPlot->mMinGrid->def() ) ).toUInt() );
    mPlot->mMajGridColor->setVal( restoreColorFromDOM( element, "majGridColor", mPlot->mMajGridColor->def() ) );
    mPlot->mMinGridColor->setVal( restoreColorFromDOM( element, "minGridColor", mPlot->mMinGridColor->def() ) );
    mPlot->mBackgroundColor->setVal( restoreColorFromDOM( element, "canvasBackground", mPlot->mBackgroundColor->def() ) );

    CYF64 *data;

    data = mPlot->scopeAxis[QwtPlot::xBottom]->rightData();
    data->setVal( element.attribute( "bottomRight", QString( "%1" ).arg( data->val() ) ).toDouble() );
    data = mPlot->scopeAxis[QwtPlot::xBottom]->leftData();
    data->setVal( element.attribute( "bottomLeft", QString( "%1" ).arg( data->val() ) ).toDouble() );

    data = mPlot->scopeAxis[QwtPlot::yLeft]->minData();
    data->setVal( element.attribute( "minLeft", QString( "%1" ).arg( data->val() ) ).toDouble() );
    data = mPlot->scopeAxis[QwtPlot::yLeft]->maxData();
    data->setVal( element.attribute( "maxLeft", QString( "%1" ).arg( data->val() ) ).toDouble() );

    mFontSize->setVal( element.attribute( "fontSizeTitle", QString( "%1" ).arg( mStyle->getFontSize() ) ).toInt() );
  }

  // Prise en compte de l'intervalle de rafraîchissement.
  internCreateFromDOM( element );

  if ( core )
  {
    // Connection des différents données à mesurer.
    QDomNodeList dnList = element.elementsByTagName( "curve" );
    for ( int i = 0; i < dnList.count(); ++i )
    {
      QDomElement el = dnList.item( i ).toElement();
      CYData *d = core->db.value( el.attribute( "dataName" ) );
      if ( !d )
      {
        CYWARNINGTEXT( el.attribute( "dataName" ) )
            mCreatingFromDOM = false;
        return false;
      }
      else
      {
        QwtPlot::Axis yAxis = ( QwtPlot::Axis )el.attribute( "yAxis", QString::number( QwtPlot::yLeft ) ).toInt();
        QPen pen = QPen( restoreColorFromDOM( el, "color", mStyle->getDataColor( i ) ), el.attribute( "width", "1" ).toUInt(), ( Qt::PenStyle )el.attribute( "style", QString( "%1" ).arg( Qt::SolidLine ) ).toUShort() );
        addDataY( d, el.attribute( "dataLabel", d->label() ), pen, el.attribute( "coefficient", "1.0" ).toDouble(), yAxis ,
                  (bool)el.attribute( "trigger", "0" ).toUInt());
      }
    }
  }

  // Prise en compte de l'intervalle de rafraîchissement.
  internCreateFromDOM( element );

  setMode( mPlot->modeVal() );
  setSamplingMode( mPlot->samplingModeVal() );

  internCreateFromDOM( element );
  mPlot->createFromDOM( element );
  internCreateFromDOM( element );

  if ( analyseSheet() && analyseSheet()->inherits( "CYAnalyseSheetCapture" ) )
  {
    ((CYScopeCapture *)mPlot)->captureCurves(mPlot);
  }

  mPlot->applySettings();

  // Charge la configuration d'acquisition.
  QDomNodeList acquisitionList = element.elementsByTagName( "Acquisition" );
  for ( int i = 0; i < acquisitionList.count(); ++i )
  {
    QDomElement el = acquisitionList.item( i ).toElement();
    acquisition()->loadSetup( el );
  }

  setModified( false );
  mCreatingFromDOM = false;
  return ( true );
}

bool CYScope::addToDOM( QDomDocument &doc, QDomElement &element, bool save )
{
  // Sauvegarde des propriétés des données mesurées.
  for ( uint i = 0; i < mNbYLeft; ++i )
  {
    QDomElement curve = doc.createElement( "curve" );
    element.appendChild( curve );
    curve.setAttribute( "dataName", datas.at( i )->objectName() );
    if ( datas.at( i )->label != datas.at( i )->data->label() )
      curve.setAttribute( "dataLabel", datas.at( i )->label );
    addColorToDOM( curve, "color", mPlot->ySignal.at( i )->pen.color() );
    curve.setAttribute( "width", mPlot->ySignal.at( i )->pen.width() );
    curve.setAttribute( "style", mPlot->ySignal.at( i )->pen.style() );
    curve.setAttribute( "coefficient", mPlot->ySignal.at( i )->coefficient );
    curve.setAttribute( "yAxis", mPlot->ySignal.at( i )->axis() );
    curve.setAttribute( "trigger", mPlot->ySignal.at( i )->isTriggerSource() );
  }
  mPlot->addToDOM( doc, element );

  // Sauvegarde l'intervalle de rafraîchissement.
  internAddToDOM( doc, element );

  // Enregistre la configuration d'acquisition.
  QDomElement el = doc.createElement( "Acquisition" );
  element.appendChild( el );
  acquisition()->CYAcquisition::saveSetup( doc, el );

  if ( save )
    setModified( false );

  return ( true );
}

void CYScope::removeDatas()
{
  while ( !datas.isEmpty() )
  {
    removeData( nbItem - 1 );
  }
}

int CYScope::showPopupMenu( QMenu *pm0, int index, bool )
{
  if (!mShowPopupMenu)
    return index;

  QMenu *pm;

  if ( pm0 )
  {
    if ( index == 0 )                                 // Création d'un sous-menu surgissant
    {
      pm = pm0->addMenu( tr( "Scope" ) ); index++;
    }
    else                                              // Ajout d'entrées dans un menu surgissant
      pm = pm0;
  }
  else                                                // Création d'un menu surgissant
  {
    pm = new QMenu( tr( "Scope" ) ); index++;
    pm->addSeparator();
  }

  index = CYDisplay::showPopupMenu( pm, index );

  pm->addSeparator();
  pm->addAction( tr( "&Analyse curves" ), this, SLOT( analyseCurve() ));
  if (core)
  {
    if (( mPlot->modeVal() == CYScope::XY ) && !tableBufferSize() )
      pm->addAction( tr( "Reset" )        , this, SLOT( resetCurves() ));
    pm->addAction( core->loadIconSet("cydocument-print.png", 22)  , tr( "Pr&int curves" )  , this, SLOT( printCurve()));
    pm->addAction( core->loadIconSet("cyapplication-pdf.png", 22)        , tr( "Export PDF" )     , this, SLOT( exportPDF() ));
    pm->addAction( core->loadIconSet("cytext-csv.png", 22), tr( "Export CSV" )     , this, SLOT( exportCSV() ));
    pm->addSeparator();
    pm->addAction( core->loadIconSet("trigger_setup.png", 22), tr( "Setup trigger" )  , this, SLOT( setupTrigger() ));
    pm->addAction( core->loadIconSet("trigger_reset.png", 22), tr( "Reset trigger" )  , this, SLOT( resetTrigger() ));
  }
  index = CYDisplay::showPopupMenu( pm, index, true );

  if ( !pm0 )
    pm->exec( QCursor::pos() );

  return index;
}

void CYScope::setMode( const CYScope::Mode mode )
{
  mPlot->setModeVal( mode );
}


CYScope::Mode CYScope::mode() const
{
  return mPlot->modeVal();
}

void CYScope::setSamplingMode( const CYScope::SamplingMode sampling )
{
  mPlot->setSamplingModeVal( sampling );
}

CYScope::SamplingMode CYScope::samplingMode() const
{
  return mPlot->samplingModeVal();
}

/*! Ajoute la donnée du signal d'un axe en X.
    \fn CYScope::addDataX(CYData *data, QString label, double coefficient , QwtPlot::Axis xAxis)
 */
CYScopeSignal *CYScope::addDataX( CYData *data, QString label, double coefficient, QwtPlot::Axis xAxis)
{
  if ( !data )
  {
    CYWARNING;
    return 0;
  }

  if ( label.isEmpty() )
    label = data->label();

  if ( !data->isNum() )
  {
    CYMessageBox::sorry( this, tr( "This display cannot treat this type of data !" ) );
    return 0;
  }

  return mPlot->addXSignal( data, label, coefficient, xAxis );
}


/*! Ajoute une donnée en Y.
    \fn CYScope::addDataY(CYData *data, QwtPlot::Axis yAxis, bool trigger)
 */
CYScopeSignal *CYScope::addDataY( CYData *data, QwtPlot::Axis yAxis, bool trigger )
{
  return addDataY( data, 0, mStyle->getDataColor( nbItem ), 1.0, yAxis, trigger );
}

/*! RAZ des courbes
    \fn CYScope::resetCurves()
 */
void CYScope::resetCurves()
{
  plot()->resetCurves();
}


/*! @return l'acquisition de l'oscilloscope.
    \fn CYScope::acquisition()
 */
CYScopeAcquisition * CYScope::acquisition()
{
  return plot()->acquisition();
}

/*! Exporter les courbes dans un fichier PDF
    @param saveAs Par défaut vaut à \a true pour avoir la boîte de sélection du fichier, pour avoir automatiquement un nom de fichier il suffit de le mettre à \a false.
    \fn CYScope::exportPDF()
 */
void CYScope::exportPDF()
{
  CYScopePrinter *printer = new CYScopePrinter( this, "CYScopePrinter" );
  Q_CHECK_PTR( printer );
  printer->printPDF();
}


/*! Exporter les courbes dans un fichier texte
    @param saveAs Par défaut vaut à \a true pour avoir la boîte de sélection du fichier, pour avoir automatiquement un nom de fichier il suffit de le mettre à \a false.
    \fn CYScope::exportCSV(bool saveAs)
 */
void CYScope::exportCSV( bool saveAs )
{
  exportCSV( saveAs, true, true );
}

/*! Exporter les courbes dans un fichier texte
 *  Enrichissement de \fn CYScope::exportCSV(bool saveAs) pour exporter l'acquisition déjà capturée.
    @param saveAs     Saisir \a true pour avoir la boîte de sélection du fichier et \a false pour avoir automatiquement un nom de fichier .
    @param newCapture Saisir \a true pour faire capturer avant d'exporter et \a false pour utiliser la dernière capture.
    @param open       Ouverture automatique.
    \fn CYScope::exportCSV(bool saveAs, bool newCapture)
 */
void CYScope::exportCSV( bool saveAs,  bool newCapture, bool open )
{
  if (plot()->capturing())
  {
    CYMessageBox::sorry( this, tr( "The previous export not finished!\n"
                                   "Renew the export.") );
  }
  else
    plot()->exportCurves(saveAs, newCapture, open);
}

/*! Mise à jour du format
    \fn CYDisplay::updateFormat()
 */
void CYScope::updateFormat()
{
  if (mUpdatingFormat)
    return;

  mUpdatingFormat = true;
  mPlot->updateFormat();
  mUpdatingFormat = false;
}

void CYScope::setYCoef( CYScopeSignal *signal, double coef )
{
  mPlot->setYCoef(signal, coef);
}

void CYScope::setYOffset( CYScopeSignal *signal, double offset )
{
  mPlot->setYOffset(signal, offset);
}

void CYScope::setTitle(const QString &t)
{
  CYDisplay::setTitle(t);
//  mPlot->setTitle(t);
}

bool CYScope::triggerIsEnabled()
{
  return mPlot->triggerIsEnabled();
}

void CYScope::setupTrigger()
{
  mPlot->setupTrigger();
}

void CYScope::resetTrigger()
{
  mPlot->enableTrigger(true);
}

int CYScope::tableBufferSize()
{
  if (datas.isEmpty())
    return -1;
  CYData * data = datas.first()->data;
  return data->tableBufferSize();
}

void CYScope::setEnableBuffering(bool val)
{
  mPlot->setEnableBuffering(val);
}

void CYScope::setDescription(const QString & txt)
{
  mDescription = txt;
  mPlot->createHelp();
}
