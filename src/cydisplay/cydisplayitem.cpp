/***************************************************************************
                          cydisplayitem.cpp  -  description
                             -------------------
    début                  : mer sep 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cydisplayitem.h"

// CYLIBS
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cypen.h"

CYDisplayItem::CYDisplayItem(QObject *parent, CYData* d, int i, QString l, double coef, double offset)
  : QObject(parent),
    id(i),
    host(d->hostsName()),
    link(d->linksName()),
    group(d->group()),
    desc(d->displayHelp()),
    unit(d->unit()),
    physical(d->physical()),
    coefficient(coef),
    offset(offset)
{
  setObjectName(d->dataName());
  data = d;
  mFormatUpdated = false;

  if ( !l.isEmpty() )
    label = l;
  else
    label = data->label();

  if ( label == data->label() )
    mDataLabel = true;
  else
    mDataLabel = false;

  connect( data, SIGNAL( formatUpdated()), this, SLOT( updateFormat() ) );
}

CYDisplayItem::~CYDisplayItem()
{
}

bool CYDisplayItem::isOk(bool testFlag)
{
  return data->isOk(testFlag);
}


/*! @return le status de la donnée
    \fn CYDisplayItem::status()
 */
QString CYDisplayItem::status()
{
  return (isOk()) ? tr("Ok") : tr("Error");
}


/*! Mise à jour du format
    \fn CYDisplayItem::updateFormat()
 */
void CYDisplayItem::updateFormat()
{
  if ( mDataLabel )
    label = data->label();
  unit = data->unit();
  emit formatUpdated();
}


/*! @return \a false si le libellé a été édité commme différent de celui de la donnée
    \fn CYDisplayItem::dataLabel()
 */
bool CYDisplayItem::dataLabel()
{
  return mDataLabel;
}

double CYDisplayItem::val()
{
  switch (data->type())
  {
    case Cy::VFL    :
                    {
                       return ((CYFlag *)data)->val()*coefficient+offset;
                    }
    case Cy::VS8    :
                    {
                       return ((CYS8 *)data)->val()*coefficient+offset;
                    }
    case Cy::VS16   :
                    {
                      return ((CYS16 *)data)->val()*coefficient+offset;
                    }
    case Cy::VS32   :
                    {
                      return ((CYS32 *)data)->val()*coefficient+offset;
                    }
    case Cy::VU8    :
                    {
                      return ((CYU8  *)data)->val()*coefficient+offset;
                    }
    case Cy::VU16   :
                    {
                      return ((CYU16 *)data)->val()*coefficient+offset;
                    }
    case Cy::VU32   :
                    {
                      return ((CYU32 *)data)->val()*coefficient+offset;
                    }
    case Cy::VF32   :
                    {
                      return ((CYF32 *)data)->val()*coefficient+offset;
                    }
    case Cy::VF64   :
                    {
                      return ((CYF64 *)data)->val()*coefficient+offset;
                    }
    case Cy::Time   :
                    {
                      return ((CYTime *)data)->val()*coefficient+offset;
                    }
    case Cy::Sec    :
                    {
                      return ((CYTSec *)data)->val()*coefficient+offset;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+data->objectName());
                      return 0.0;
  }
  return 0.0;
}


double CYDisplayItem::max()
{
  switch (data->type())
  {
    case Cy::VFL    :
                    {
                      return ((CYFlag *)data)->max()*coefficient+offset;
                    }
    case Cy::VS8    :
                    {
                      return ((CYS8 *)data)->max()*coefficient+offset;
                    }
    case Cy::VS16   :
                    {
                      return ((CYS16 *)data)->max()*coefficient+offset;
                    }
    case Cy::VS32   :
                    {
                      return ((CYS32 *)data)->max()*coefficient+offset;
                    }
    case Cy::VU8    :
                    {
                      return ((CYU8  *)data)->max()*coefficient+offset;
                    }
    case Cy::VU16   :
                    {
                      return ((CYU16 *)data)->max()*coefficient+offset;
                    }
    case Cy::VU32   :
                    {
                      return ((CYU32 *)data)->max()*coefficient+offset;
                    }
    case Cy::VF32   :
                    {
                      return ((CYF32 *)data)->max()*coefficient+offset;
                    }
    case Cy::VF64   :
                    {
                      return ((CYF64 *)data)->max()*coefficient+offset;
                    }
    case Cy::Time   :
                    {
                      return ((CYTime *)data)->max()*coefficient+offset;
                    }
    case Cy::Sec    :
                    {
                      return ((CYTSec *)data)->max()*coefficient+offset;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+data->objectName());
                      return 0.0;
  }
  return 0.0;
}


double CYDisplayItem::min()
{
  switch (data->type())
  {
    case Cy::VFL    :
                    {
                      return ((CYFlag *)data)->min()*coefficient+offset;
                    }
    case Cy::VS8    :
                    {
                      return ((CYS8 *)data)->min()*coefficient+offset;
                    }
    case Cy::VS16   :
                    {
                      return ((CYS16 *)data)->min()*coefficient+offset;
                    }
    case Cy::VS32   :
                    {
                      return ((CYS32 *)data)->min()*coefficient+offset;
                    }
    case Cy::VS64   :
                    {
                      return ((CYS64 *)data)->min()*coefficient+offset;
                    }
    case Cy::VU8    :
                    {
                      return ((CYU8  *)data)->min()*coefficient+offset;
                    }
    case Cy::VU16   :
                    {
                      return ((CYU16 *)data)->min()*coefficient+offset;
                    }
    case Cy::VU32   :
                    {
                      return ((CYU32 *)data)->min()*coefficient+offset;
                    }
    case Cy::VU64   :
                    {
                      return ((CYU64 *)data)->min()*coefficient+offset;
                    }
    case Cy::VF32   :
                    {
                      return ((CYF32 *)data)->min()*coefficient+offset;
                    }
    case Cy::VF64   :
                    {
                      return ((CYF64 *)data)->min()*coefficient+offset;
                    }
    case Cy::Time   :
                    {
                      return ((CYTime *)data)->min()*coefficient+offset;
                    }
    case Cy::Sec    :
                    {
                      return ((CYTSec *)data)->min()*coefficient+offset;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+data->objectName());
                      return 0.0;
  }
  return 0.0;
}

void CYDisplayItem::initDiscontBuffer()
{
  data->initDiscontBuffer();
}
