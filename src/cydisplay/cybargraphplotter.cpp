/***************************************************************************
                          cybargraphplotter.cpp  -  description
                             -------------------
    begin                : jeu déc 4 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cybargraphplotter.h"

#define NB_BAR 32

// QT
#include <QPaintEvent>
#include <QPixmap>
// CYLIBS
#include "cycore.h"
#include "cyf64.h"
#include "cyf32.h"
#include "cyflag.h"
#include "cycolor.h"
#include "cydisplaystyle.h"
#include "cybargraphsignal.h"
#include "cybargraph.h"
#include "cynethost.h"
#include "cymessagebox.h"

CYBargraphPlotter::CYBargraphPlotter(CYBargraph *bargraph, QWidget *parent, const QString &name)
  : QWidget(parent),
    mBargraph(bargraph)
{
  setObjectName(name);
  init();
}

CYBargraphPlotter::~CYBargraphPlotter()
{
}

CYBargraphSignal *CYBargraphPlotter::addBar(CYData *data, int id, QString label)
{
  if (items.count() >= NB_BAR)
  {
    if (isVisible())
      CYMessageBox::information(this, QString(tr("For a good display the maximum number of bars in the bargraph is %1!")).arg(NB_BAR));
    return 0;
  }

  // connexion d'une nouvelle données aux graphes
  CYBargraphSignal *signal =  new CYBargraphSignal(this, data, id, label);
  Q_CHECK_PTR(signal);

  if ( signal->max() > mMaxValue->val() )
    mMaxValue->setVal(signal->max());

  if ( signal->min() < mMinValue->val() )
    mMinValue->setVal(signal->min());

  items.insert(items.count(), signal);
  repaint();
  return (signal);
}

bool CYBargraphPlotter::deleteBar(uint idx)
{
  items.removeAt(idx);
  update();

  return (true);
}

void CYBargraphPlotter::changeRange(double min, double max)
{
  mMinValue->setVal(min);
  mMaxValue->setVal(max);
}

void CYBargraphPlotter::setLimits(double l, bool la, double u, bool ua)
{
  mAlarmLowerBound->setVal( l );
  mAlarmLowerEnable->setVal( la );
  mAlarmUpperBound->setVal( u );
  mAlarmUpperEnable->setVal( ua );
}

void CYBargraphPlotter::paintEvent(QPaintEvent *)
{
  int w = width();
  int h = height();

  QPixmap pm(w, h);
  QPainter p;
  p.begin(&pm);
  p.begin(this);

  p.setRenderHint(QPainter::Antialiasing);
  p.setFont(QFont(p.font().family(), mBargraph->fontSize() ));
  QFontMetrics fm(p.font());

  pm.fill(mBackgroundColor->val());

  /* Dessine une ligne blanche le long des côtés bas et droit du
   * widget pour créer un effet 3D. */
  p.setPen(palette().color(QPalette::Light));
  p.drawLine(0, h - 1, w - 1, h - 1);
  p.drawLine(w - 1, 0, w - 1, h - 1);

  p.setClipRect(1, 1, w - 2, h - 2);

  if (items.count() > 0)
  {
    int barWidth = (w - 2) / items.count();
    int b;
    /* Les légendes sont affichées uniquement en dessous des barres
     * si les légendes pour toutes les barres sont plus petites que
     * la largeur des barres. Si une simple légende n'est pas appropriée
     * elle n'est pas montrée. */
    bool showLabels = true;

    if (items.isEmpty())
      showLabels = false;
    else
      for (b = 0; b < items.count(); b++)
        if (fm.horizontalAdvance((items.at(b)->label).length()) > barWidth)
          showLabels = false;

    int barHeight;
    if (showLabels)
      barHeight = h - 2 - fm.lineSpacing() - 2;
    else
      barHeight = h - 2;

    for (int b = 0; b < items.count(); b++)
    {
      double val = items.at(b)->val();
      int topVal;
      if ( mSens->val() )
        topVal = (int) ( (float) ( ( barHeight / sqrt( pow( ( mMaxValue->val() - mMinValue->val() ), 2 ) ) ) * (val - mMinValue->val()) ) );
      else
        topVal = (int) ( (float) ( ( barHeight / sqrt( pow( ( mMaxValue->val() - mMinValue->val() ), 2 ) ) ) * (mMaxValue->val() - val) ) );

      for (int i = 0; i < barHeight && i < topVal; i += 2)
      {
        if (( mAlarmUpperEnable->val() && ( val > mAlarmUpperBound->val()) ) || ( mAlarmLowerEnable->val() && ( val < mAlarmLowerBound->val() )))
          p.setPen(mAlarmColor->val().lighter(static_cast<int>(30 + (70.0 / (barHeight + 1) * i))));
        else
          p.setPen(mNormalColor->val().lighter(static_cast<int>(30 + (70.0 / (barHeight + 1) * i))));

        if ( mSens->val() )
          p.drawLine(b * barWidth + 3, barHeight - i, (b + 1) * barWidth - 3, barHeight - i);
        else
          p.drawLine(b * barWidth + 3, i, (b + 1) * barWidth - 3, i);
      }

      if (( mAlarmUpperEnable->val() && ( val > mAlarmUpperBound->val()) ) || ( mAlarmLowerEnable->val() && ( val < mAlarmLowerBound->val() )))
        p.setPen(mAlarmColor->val());
      else
        p.setPen(mNormalColor->val());
      if (showLabels)
        p.drawText(b * barWidth + 3, h - fm.lineSpacing() - 2, barWidth - 2 * 3, fm.lineSpacing(), Qt::AlignCenter, items.at(b)->label);
      p.drawText(b * barWidth + 3, h - 2*fm.lineSpacing() - 2, barWidth - 2 * 3, fm.lineSpacing(), Qt::AlignCenter, items.at(b)->data->toString(true, !core->simulation()));
    }
  }

  p.end();

  p.begin(this);
  p.drawPixmap(0, 0, pm);
  p.end();
}

CYBargraphSignal *CYBargraphPlotter::item(uint i)
{ 
  return items.at(i);
}


/*!
    \fn CYBargraphPlotter::init()
 */
void CYBargraphPlotter::init()
{
  // paintEvent couvre entièrement le widget afin d'utiliser aucun arrière-plan pour éviter des clignotements
  setAttribute(Qt::WA_OpaquePaintEvent);

  initDB();

  // Quelque chose de plus petit que ça n'a aucun sens.
  setMinimumSize(16, 16);
  setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
}


/*! Saisie les couleurs
    \fn CYBargraphPlotter::setColors(QColor normal, QColor alarm, QColor background)
 */
void CYBargraphPlotter::setColors(QColor normal, QColor alarm, QColor background)
{
  mNormalColor->setVal( normal );
  mAlarmColor->setVal( alarm );
  mBackgroundColor->setVal( background );
}


/*! Initiallise la base de données de l'afficheur.
    \fn CYBargraphPlotter::initDB()
 */
void CYBargraphPlotter::initDB()
{
  if ( mBargraph )
    mDB = mBargraph->db();
  else
    CYFATAL

  mDB->setGroup(tr("Bargraph"));

  mDB->setUnderGroup(tr("Scale"));
  mMinValue = new CYF64(mDB, "MIN_VALUE" , new f64);
  mMinValue->setLabel(tr("Minimum value"));
  mMinValue->setNbDec(0);
  mMaxValue = new CYF64(mDB, "MAX_VALUE" , new f64);
  mMaxValue->setLabel(tr("Maximum value"));
  mMaxValue->setNbDec(0);
  mSens = new CYFlag(mDB, "SENS" , new flg, true);
  mSens->setLabel(tr("Sense"));
  mSens->setChoiceTrue(tr("Increasing"));
  mSens->setChoiceFalse(tr("Decreasing"));
  mDB->setUnderGroup(0);

  mDB->setUnderGroup(tr("Alarm"));
  mAlarmUpperBound = new CYF64(mDB, "ALARM_UPPER_BOUND" , new f64);
  mAlarmUpperBound->setLabel(tr("Upper bound value"));
  mAlarmUpperBound->setNbDec(0);
  mAlarmUpperEnable = new CYFlag(mDB, "ALARM_UPPER_ENABLE" , new flg);
  mAlarmUpperEnable->setLabel(tr("Upper alarm enable"));

  mAlarmLowerBound = new CYF64(mDB, "ALARM_LOWER_BOUND" , new f64);
  mAlarmLowerBound->setLabel(tr("Lower bound value"));
  mAlarmLowerBound->setNbDec(0);
  mAlarmLowerEnable = new CYFlag(mDB, "ALARM_LOWER_ENABLE" , new flg);
  mAlarmLowerEnable->setLabel(tr("Lower alarm enable"));
  mDB->setUnderGroup(0);

  mDB->setUnderGroup(tr("Style"));
  mNormalColor = new CYColor(mDB, "NORMAL_COLOR", new QColor(Qt::green));
  mNormalColor->setLabel(tr("Normal Bar Color"));
  mAlarmColor = new CYColor(mDB, "ALARM_COLOR", new QColor(Qt::red));
  mAlarmColor->setLabel(tr("Out-of-range Color"));
  mBackgroundColor = new CYColor(mDB, "BACKGROUND_COLOR", new QColor(Qt::black));
  mBackgroundColor->setLabel(tr("Background Color"));
  mDB->setUnderGroup(0);
}
