/***************************************************************************
                          cydatastablecol.h  -  description
                             -------------------
    begin                : ven nov 19 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDATASTABLECOL_H
#define CYDATASTABLECOL_H

// QT
#include <QString>
#include <QTableWidgetItem>

/** @short Colonne d'un table de données.
  * @author LE CLÉACH Gérald
  */

class CYDatasTableCol
{
public:
  /** Type de colonne. */
  enum Type
  {
    /** Affiche l'identifiant de la ligne. */
    Id,
    /** Affiche les valeurs. */
    Value,
    /** Propose la saisie des valeurs de forçage. */
    Forcing,
    /** Affiche les étiquettes.*/
    Label,
    /** Affiche les groupes. */
    Group,
    /** Affiche les connexions réseau. */
    Link,
    /** Affiche les hôtes. */
    Host,
    /** Affiche les noms. */
    Name,
    /** Affiche les aides.*/
    Help,
    /** Affiche le repère physique.*/
    Phys,
    /** Affiche le repère éléctrique.*/
    Elec,
    /** Affiche la valeur ADC.*/
    ADC,
    /** Affiche un afficheur CYDisplay.*/
    Display,
    /** Propose la saisie d'une donnée de RAZ. */
    Reset,
    /** Affiche la donnée partielle sur laquelle agit la donnée de RAZ. */
    Partial,
    /** Propose la saisie de la donnée note. */
    Note,
    /** Propose la sélection du type de pilotage en mode forçage . */
    Control,
    /** Affiche l'addresse.*/
    Addr,
    /** Seuil.*/
    Threshold,
  };

  CYDatasTableCol(Type, int id, bool shown, int width, QString label);
  ~CYDatasTableCol();

  /** @return le type colonne. */
  Type type() { return mType; }

//  /** @return le numéro de la colonne. */
//  int num() { return (int)mType; }

//  /** @return la position de la colonne. */
//  int pos() { return mPos; }
//  /** Saisie la position de la colonne. */
//  void setPos(int val) { mPos = val; }

  /** @return true si la colonne est affichée. */
  bool shown() { return mShown; }
  /** Saisir \a true si la colonne est affichée. */
  void setShown(bool state) { mShown = state; }

  /** @return la largeur de la colonne. */
  int width() { return mWidth; }
  /** Saisie la largeur de la colonne. */
  void setWidth(int val) { mWidth = val; }

  /** @return l'étiquette de la colonne. */
  QString label() { return mLabel; }
  /** Saisie l'étiquette de la colonne. */
  void setLabel(QString string);

  /** Widget d'en-tête. */
  QTableWidgetItem *header() { return mHeader; }

  /** Identifiant unique de la colonne */
  int id;

protected: // Protected attributes
  /** Type de colonne. */
  Type mType;
//  /** Position de la colonne. */
//  int mPos;
  /** Vaut \a true si la colonne est affichée. */
  bool mShown;
  /** Largeur de la colonne. */
  int mWidth;
  /** Etiquette de la colonne. */
  QString mLabel;
  /** Widget d'en-tête. */
  QTableWidgetItem *mHeader;
};

#endif
