/***************************************************************************
                          cydisplay.h  -  description
                             -------------------
    début                  : ven mar 14 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDISPLAY_H
#define CYDISPLAY_H

#define NONE -1

// QT
#include <QDomElement>
#include <QStringList>
#include <QList>
#include <QLabel>
#include <QGroupBox>
#include <QContextMenuEvent>
#include <QTimerEvent>
#include <QResizeEvent>
#include <QFocusEvent>
#include <QMouseEvent>
#include <QEvent>
#include <QLayout>
#include <QToolTip>
#include <QMenu>
// CYLIBS
#include "cyframe.h"
#include "cydisplayitem.h"

class CYDB;
class CYData;
class CYF64;
class CYS16;
class CYTime;
class CYFlag;
class CYString;
class CYAnalyseCell;
class CYAnalyseSheet;
class CYDisplayStyle;
class CYDisplayTimerSetup;
class CYDisplaySatusIndicator;
class QBoxLayout;

/** CYDisplay est la classe de base de des afficheurs de données. Un
  * afficheur est un type de widget qui peut afficher la valeur d'une ou
  * plusieurs données d'une manière propre au type d'afficheur.
  * @short Afficheur de données de base.
  * @author Gérald LE CLEACH.
  */

class CYDisplay : public CYFrame
{
    Q_OBJECT
    Q_PROPERTY(bool forcing READ forcing WRITE setForcing)

public:
    /** Type d'afficheur. */
    enum Type
    {
      /** Afficheur vide. */
      Dummy,
      /** Bargraph. */
      Bargraph,
      /** Multimètre. */
      Multimeter,
      /** Osciloscope. */
      Scope,
      /** Tableau de données. */
      DatasTable,
      /** Afficheur de courbe XY. */
      XYCurve,
    };

    /** Type de status possible. */
    enum Status
    {
      /** La connexion réseau est en place. */
      Connect,
      /** Pas de connexion réseau. */
      NoConnect,
      /** Afficheur en pause. */
      Pause,
      /** Tous les afficheurs en pause. */
      GlobalPause,
    };

    /** Construit un afficheur.
      * @param parent  Widget parent.
      * @param name    Nom de l'afficheur.
      * @param title   Titre de l'afficheur.
      * @param nf      Pas de trame existante. */
    CYDisplay(QWidget *parent=0, const QString &name="display", const QString &title=0, bool nf=false);

    virtual ~CYDisplay();

    /** @return le type d'afficheur. */
    Type type() { return mType; }

    /** Ajoute un groupe de données dans l'afficheur. */
    virtual bool addGroup(QString group);
    /** Ajoute une donnée dans l'afficheur. */
    virtual bool addData(const QString &dataName, QString label=0);
    /** Ajoute une donnée dans l'afficheur. */
    virtual bool addData(CYData *data, QString label=0);
    /** Supprime une donnée de l'afficheur. */
    virtual bool removeData(uint idx);

    /** Test s'il existe une boîte de dialogue de propriété pour cet afficheur. */
    virtual bool hasSettingsDialog() const { return false; }

    /** Change la tempo de rafraîchissement. */
    void changeRefreshTimer();

    virtual void updateToolTip();
    virtual QString additionalToolTip();

    /** Construit un afficheur à partir d'un DOM. */
    virtual bool createFromDOM(QDomElement &, QStringList *ErrorList=nullptr);
    /** Ajoute un afficheur dans un DOM. */
    virtual bool addToDOM(QDomDocument &, QDomElement &, bool=true);

    /** Liste les hôtes concernés. */
    void collectHosts(QStringList &list);

    /** Gestion du timer en fonction de la visibilité de l'afficheur. */
    void setIsOnTop(bool onTop);

    QString title();
    virtual void setTitle(const QString &title);

    QString boxTitle() const;
    void setBoxTitle(const QString &title);

    /** Charge le widget correspondant au type de l'afficheur. */
    void registerPlotterWidget(QWidget *plotter);

    /** @return la tempo de rafraîchissement. */
    CYTime *refreshTime();

    /** @return le nom du fichier de configuration. */
    QString settingsFile() { return mSettingsFile; }

    /** @return la base de données locale à cet afficheur. */
    CYDB *db() { return mDB; }

    /** @return \a true si le rafraîchissement de l'afficheur est à l'arrêt. */
    bool refreshIsStop() { return mRefreshStop; }

    /** @return Version de CYLIBS lors de l'enregistrement du fichier de sauvegarde */
    inline QString versionCYDOM() { return mVersionCYDOM; }

public: // Public methods
    /** @return \a true si la feuille d'analyse est en mode forçage. */
    virtual bool forcing() const;
    /** Saisir \a true pour activer le mode forçage. */
    virtual void setForcing(const bool val);
    /** Gestion du menu surgissant.
      * @param pm0    Menu appelant s'il existe.
      * @param index  Index de l'entrée du menu appelant.
      * @param end    Fin du menu. */
    virtual int showPopupMenu(QMenu *pm0=0, int index=0, bool end=false);
    /** Autorise ou non le menu surgissant.*/
    virtual void setShowPopupMenu(const bool &enable) { mShowPopupMenu = enable; }
    virtual bool connected();
    virtual void initTRDisplay(QString title);
    virtual int fontSize();
    virtual void setFontSize(int val);
    /**
     * @brief Compare la version de CYLIBS lors de l'enregistrement du fichier de sauvegarde avec la version \a than
     * Si l'afficher fait partie d'une feuille d'analyse graphique alors le fichier traîté est le .cyas relatif,
     * sinon c'est le .cywdg de l'afficheur.
     * @param than          Version de comparaison
     * @return
     *  - \a 1 si \a versionCYDOM > \a than.
     *  - \a 0 si \a versionCYDOM = \a than.
     *  - \a -1 si \a versionCYDOM < \a than.
     */
    int compareVersionCYDOM(QString than);
    virtual CYAnalyseSheet * analyseSheet();

    /** @return le style de l'afficheur. */
    CYDisplayStyle *style() { return mStyle; }

    /** @return le texte contenu par l'afficheur lorsque c'est possible.
        Les afficheurs ne pouvant pas retourner de texte simple tels que par
        exemple un oscilloscope, la valeur \a ok est alors à \a false. */
    virtual QString text(bool &ok) { ok=false; return 0; }

    /** Rafraichit mếme si l'objet est caché.
      * Utile par exmple en cas d'export automatique */
    void setRefreshNotVisible(bool val) { mRefreshNotVisible = val; }

    /** Active ou non le status de l'afficheur */
    void setStatusIndicatorEnabled(const bool &val) { mStatusIndicatorEnabled = val; }

    /** @return La forme autour des autres widgets.*/
    inline QGroupBox *frame() { return mFrame; }

public slots:
    /** Démarre le rafraîchissement. */
    virtual void startRefresh();
    /** Arrête le rafraîchissement. */
    virtual void stopRefresh();
    void timerToggled(bool);
    /** Configure le timer de raffraîchissement. */
    void setupTimer();
    /** Exécute la boîte de dialogue de propriété de cet afficheur. */
    virtual void settings() {}

    /** Appliquer les paramètres de configuration. */
    virtual void applySettings();
    /** Vérifie si l'afficheur a été modifié. */
    virtual void setModified(bool mfd);
    /** Rafraîchit l'afficheur. */
    virtual void refresh() {}
    /** @return \a true s'il n'y a acune donnée à visualiser. */
    bool isEmpty();

    /** Saisie le nom du fichier de configuration. */
    void setSettingsFile(const QString fileName);
    /** Charge la configuration. */
    int loadSettings();
    /** Charge la configuration du fichier \a fileName. */
    int loadSettings(QString &fileName);
    /** Enregistre la configuration. */
    int saveSettings();
    /** Enregistre la configuration dans le fichier \a fileName. */
    int saveSettings(QString &fileName);
    /** Valider les modifications. */
    void validate();
    /** Supprimer l'afficheur. */
    void remove();
    virtual void refreshStatus();
    virtual CYDisplay::Status status();
    virtual void removeDatas();
    virtual void updateFormat() {}
    virtual void contextMenu(const QPoint& pos);

    /** Initialise la bufferisation discontinue*/
    void initDiscontBuffer();

signals: // Signals
    /** Indique si l'afficheur a été modifié. */
    void displayModified(bool mfd);
    void showPopupMenu(CYDisplay *display);
    /** Emis à chaque changement de titre. */
    void titleChanged(const QString &);
    /** Donne l'ordre au widget parent si celui-ci est une cellule d'analyse (CYAnalyseCell) de se diviser. */
    void split(int orientation);
    void statusChanged(CYDisplay::Status status);

protected: // Protected methods
    virtual void timerEvent(QTimerEvent *);
    virtual void resizeEvent(QResizeEvent *);

    /** Gestion évènementielle de la souris. */
    virtual bool eventFilter(QObject *, QEvent *);
    /** Gestion du menu contextuel. */
    //virtual void contextMenuEvent ( QContextMenuEvent * e );

    virtual void focusInEvent(QFocusEvent *);
    virtual void focusOutEvent(QFocusEvent *);

    /** Charge un élément dans l'afficheur. */
    void registerItem(CYDisplayItem *item);
    /** Décharge un élément de l'afficheur. */
    void unregisterItem(uint idx);

    /** Retrouve une couleur à partir d'un DOM. */
    QColor restoreColorFromDOM(QDomElement &de, const QString &attr, const QColor &fallback);
    /** Ajoute une couleur dans un DOM. */
    void addColorToDOM(QDomElement &de, const QString &attr, const QColor &col);

    /** Charge les paramètres de l'afficheur à partir d'un DOM. */
    void internCreateFromDOM(QDomElement &display);
    /** Ajoute les paramètres de l'afficheur dans un DOM. */
    void internAddToDOM(QDomDocument &doc, QDomElement &display);

    virtual QLabel * popupMenuLabel(QString title);

public: // Public attributes
    QList<CYDisplayItem*> datas;

protected: // Protected attributes
    /** Style de l'afficheur. */
    CYDisplayStyle *mStyle;
    /** La forme autour des autres widgets.*/
    QGroupBox *mFrame;
    QBoxLayout *mBoxLayout;

    int nbItem;

    /** Titre de l'afficheur. */
    CYString *mTitle;
    /** Titre de l'afficheur automatique. */
    CYFlag *mTitleAuto;
    /** Titre de l'afficheur avec unité. */
    CYFlag *mTitleWithUnit;
    /** Unité de l'afficheur. */
    QString mUnit;

    /** Vaut \a true si les propriétés de l'afficheur ont été modifiés depuis la dernière sauvegarde. */
    bool mModified;

    bool pauseOnHide;
    bool pausedWhileHidden;

    /** Base de données locale à cet afficheur. */
    CYDB *mDB;
    /** Temps de rafraîchissement de l'afficheur. */
    CYTime *mRefreshTime;
    /** Timer de rafraîchissement de l'afficheur. */
    QTimer *mRefreshTimer;
    /** Taille de la police. */
    CYS16 *mFontSize;
    /** Vaut \a true si le rafraîchissement de l'afficheur est à l'arrêt. */
    bool mRefreshStop;
    QWidget *plotterWdg;
    /** Fichier de configuration. */
    QString mSettingsFile;
    /** Type d'afficheur. */
    Type mType;
    /** Cellule contenant cet afficheur. */
    CYAnalyseCell *mCell;
    /** Mode forçage. */
    bool mForcing;
    /** Vaut \a true si en cours de construction d'un afficheur à partir d'un DOM. */
    bool mCreatingFromDOM;
    /** Version de CYLIBS lors de l'enregistrement du fichier de sauvegarde */
    QString mVersionCYDOM;

    bool mDeleting;
    bool mCreating;

    Status mStatus;
    CYDisplaySatusIndicator *mStatusIndicator;
    bool mStatusIndicatorEnabled;
    bool mUpdatingFormat;

    bool mRefreshNotVisible;
    bool mShowPopupMenu;

private:
    void init(QString title);
    CYDisplayTimerSetup *ts;

};

#endif
