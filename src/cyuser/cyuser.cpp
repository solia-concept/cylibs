/***************************************************************************
                          cyuser.cpp  -  description
                             -------------------
    begin                : ven oct 31 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyuser.h"

// QT
#include <QDomElement>
#include <qapplication.h>
#include <qdir.h>
//Added by qt3to4:
#include <QTextStream>
#include <QString>
// CTYLIBS
#include "cycore.h"
#include "cyuseraction.h"
#include "cyusergroup.h"
#include "cydb.h"
#include "cystring.h"
#include "cyflag.h"
#include "cymessagebox.h"
#include "cypassdlg.h"
#include "cyglobal.h"
#include "cysimplecrypt.h"

CYUser::CYUser(QObject *parent, int id, QString title)
  : QObject(parent),
    mIndex( id )
{
  setObjectName(QString("user_%1").arg(id));
  mGroup = 0;
  mParentGroup    = (core->administrator()) ? core->administrator()->group() : 0;
  mParentUser     = 0;

  mDB = new CYDB( this );
  mDB->setSaveWithApply( false );
  mDB->setDOMTagDatas( "CYUser" );
  mDB->setGroup( tr("User") );

  mTitle = new CYString( mDB, "TITLE", new QString( title ), tr("Name") );

  mOSUser = new CYFlag( mDB, "LINUX_USER", new flg, false );
  mOSUser->setLabel(tr("LINUX user"));
  mOSUser->setHelp(tr("The LINUX user takes the user name of the current LINUX session."));

  mPassword = new CYString( mDB, "PASSWORD", new QString, tr("Password") );

  mHasPassword = new CYFlag( mDB, "HAS_PASSWORD", new flg, true );
  mHasPassword->setLabel(tr("Has a password"));
}

CYUser::~CYUser()
{
}

QString CYUser::title()
{
  return mTitle->val();
}

bool CYUser::isProtectedAccess()
{
  return ( this == core->protectedAccess() ) ? true : false;
}

QString CYUser::password()
{
  return mPassword->simpleDecrypt();
}

void CYUser::setPassword(QString pass)
{
  mPassword->setVal( QString(pass) );
}

bool CYUser::hasPassword()
{
//  CYDEBUGBOX return false;
  return mHasPassword->val();
}

void CYUser::setHasPassword(bool val)
{
  mHasPassword->setVal( val );
}

int CYUser::checkPassword(QWidget *parent, QString note)
{
  if ( !hasPassword() || designer() || simulation() )
    return 1;

  QString pass;

  CYPasswordDialog dlg( CYPasswordDialog::Password, false, 0, parent);
  dlg.setWindowTitle(tr("Password checking"));
  if (!note.isEmpty())
    dlg.setPrompt( tr("%1\nPassword of '%2'").arg(note).arg( title() ));
  else
    dlg.setPrompt( tr("Password of '%1'").arg( title() ));
  if( !dlg.exec() )
      return 0; //the user canceled
  pass = dlg.password();

  if ( pass == password() )
    return 1;
  else if (core->isDesignerPassword( pass ))
    return 2;
  else if (core->isSimulationPassword( pass ))
    return 3;
  else
  {
    CYMessageBox::error(parent, tr("Wrong password"));
    return checkPassword();
  }
  return 0;
}

void CYUser::changePassword()
{
  if (!hasPassword())
  {
    QString msg = tr("Access '%1' has no password.").arg( title() );
    CYMessageBox::information(0, msg, tr("Access password"));
    return;
  }

  if (designer() || simulation())
  {
    QString msg = tr("Access '%1' cannot change its password.").arg( title() );
    CYMessageBox::information(0, msg, tr("Access password"));
    return;
  }

  if (checkPassword()!=1)
    return;

  QString password;
  int result = CYPasswordDialog::getNewPassword(password, tr("New password for '%1'").arg( title() ));
  if (result == CYPasswordDialog::Accepted)
  {
    setPassword( password );
    save();
  }
}

/*! @return \a true si l'accès à l'environnement de bureau est protégé.
    \fn CYUser::desktopEnvironmentProtected()
 */
bool CYUser::desktopEnvironmentProtected()
{
  if (isProtectedAccess())
    return true;
  if (group())
    return group()->desktopEnvironmentProtected();
  return false;
}


void CYUser::setGroup(int id)
{
  CYUserGroup *group = core->userGroups().value( id );

  if ( group )
    setGroup( group );
  else
    CYWARNINGTEXT( tr("Can't find group with index %1").arg( id ) );
}


void CYUser::setGroup(CYUserGroup *g)
{
  if ( group() )
  {
    group()->removeUser( this );
  }

  if (core->pyramidalUserAdmin())
    setParentGroup(g);
  else
    mGroup = g;

  group()->addUser( this );
}

CYUserGroup *CYUser::group()
{
  CYUserGroup *group;

  if (core->pyramidalUserAdmin())
  {
    group=parentGroup();
    if (group)
    {
      return group;
    }
    CYUser *user=parentUser();
    if (user)
    {
      return user->group();
    }
  }
    return mGroup;
}

/*! @return la base de données propre à l'utilisateur.
    \fn CYUser::db()
 */
CYDB * CYUser::db()
{
  return mDB;
}

bool CYUser::designer()
{
  return group()->designer();
}

void CYUser::setDesigner(bool val)
{
  group()->setDesigner( val );
}

bool CYUser::simulation()
{
  return group()->simulation();
}

void CYUser::setSimulation(bool val)
{
  group()->setSimulation( val );
}

bool CYUser::administrator()
{
  return group()->administrator();
}

void CYUser::setAdministrator(bool val)
{
  if ( val )
  {
    mTitle->designer();
  }
  group()->setAdministrator( val );
}


/*! Mise à jour de l'interface graphique par rapport à toutes les collections d'actions.
    \fn CYUser::updateGUI()
 */
void CYUser::updateGUI()
{
  group()->updateGUI();
}


/*! Mise à jour de l'interface graphique par rapport à la collection d'actions ayant pour nom \a collection.
    \fn CYUser::updateGUI( QString collection )
 */
void CYUser::updateGUI( QString collection )
{
  group()->updateGUI( collection );
}


/*! Mise à jour de l'interface graphique par rapport à la collection d'actions \a collection.
    \fn CYUser::updateGUI( CYUserActionCollection *collection )
 */
void CYUser::updateGUI( CYUserActionCollection *collection )
{
  group()->updateGUI(collection);
}

/*! Chargement des paramètres de l'utilisateur.
    \fn CYUser::load()
 */
bool CYUser::load()
{
  QDir dir( core->appDataDir()+"/users" );
  if ( !dir.exists() )
    return false;
  return load( QString("%1/user_%2.cyusr").arg(dir.absolutePath()).arg( index()) );
}


/*! Chargement des paramètres de l'utilisateur.
    \fn CYUser::load(const QString &fileName)
 */
bool CYUser::load(const QString &fileName)
{
  QFile file(mFileName = fileName);

  if ( !file.exists() )
    return true;

  if (!file.open(QIODevice::ReadOnly))
  {
    CYMessageBox::sorry(0, tr("Can't open the file %1").arg(mFileName));
    return (false);
  }

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  QString errorMsg;
  int errorLine;
  int errorColumn;
  if (!doc.setContent(&file, &errorMsg, &errorLine, &errorColumn))
  {
    CYMessageBox::sorry(0, tr("The file %1 does not contain valid XML\n"
                                  "%2: line:%3 colomn:%4").arg(mFileName).arg(errorMsg).arg(errorLine).arg(errorColumn));
    return (false);
  }
  // Vérifie le type propre du document.
  if (doc.doctype().name() != "CYLIXUser")
  {
    CYMessageBox::sorry(0, QString(tr("The file %1 does not contain a valid"
                                          "definition, which must have a document type ").arg(mFileName)
                                          +"'CYLIXUser'"));
    return (false);
  }
  // Vérifie la taille propre.
  QDomElement el = doc.documentElement();

  // Version de la librairie lors de la sauvegarde du document.
  mVersionCYDOM = el.attribute("CYDOM");
  CYSimpleCrypt * simpleCrypt = new CYSimpleCrypt(Q_UINT64_C(0x0c2ad4a4acb9f023));
  if (compareVersion(mVersionCYDOM, "5.26")>=0)
  {
    // préparation pour lecture mot de passe crypté
    mPassword->setSimpleCrypt(simpleCrypt);
  }

  // pyramidale
  if (core->pyramidalUserAdmin())
  {
    if (el.attribute("PARENT_GROUP").isEmpty())
      setGroup( el.attribute("GROUP").toInt() );
    else
      setGroup( el.attribute("PARENT_GROUP").toInt() );

    setParentUser( el.attribute("PARENT_USER").toInt() );
  }
  else
    setGroup( el.attribute("GROUP").toInt() );


  db()->loadFromDOM(el);
  if (compareVersion(mVersionCYDOM, "5.26")<0)
  {
    // crypte le mot de passe
    QString txt = mPassword->val();
    mPassword->setSimpleCrypt(simpleCrypt);
    mPassword->setVal(txt);
    // enregistre le mot de passe crypté
    save(fileName);
  }

  setOSUser( mOSUser->val() );

  return (true);
}


bool CYUser::save()
{
  QDir dir( core->appDataDir()+"/users" );
  if ( !dir.exists() )
    core->mkdir( dir.absolutePath() );
  return save( QString("%1/user_%2.cyusr").arg(dir.absolutePath()).arg( index()) );
}


bool CYUser::save(const QString &fileName)
{
  QDomDocument doc("CYLIXUser");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  // enregistre les informations la feuille de données.
  QDomElement el = doc.createElement("CYUser");
  doc.appendChild(el);

  el.setAttribute("CYDOM", core->version());
  mVersionCYDOM = core->version();

  if (core->pyramidalUserAdmin())
  {
    if (parentGroup())
      el.setAttribute("PARENT_GROUP", parentGroup()->index());

    if (parentUser())
      el.setAttribute("PARENT_USER", parentUser()->index());
  }
  else
    el.setAttribute("GROUP", group()->index());

  setOSUser( mOSUser->val() );

  if ( !db()->hasOnlyDesignerValues() )
    db()->saveToDOM( doc, el );

  QFile file(mFileName = fileName);
  if (!file.open(QIODevice::WriteOnly))
  {
    CYWARNINGTEXT(tr("Can't save file %1!").arg(mFileName));
    return (false);
  }
  QTextStream s(&file);
  s.setCodec("UTF-8");
  s << doc;
  file.close();
  core->backupFile(mFileName);

//   setModified(false);
  return (true);
}


/*! Fixe ou non l'utilisateur comme utilisateur OS.
    \fn CYUser::setOSUser( bool val )
 */
void CYUser::setOSUser( bool val )
{
  mOSUser->setVal( val );
  if ( mOSUser->val() )
  {
    QString name = qgetenv("USER");
    if (name.isEmpty())
        name = qgetenv("USERNAME");
    if (name.isEmpty())
      CYWARNINGTEXT(tr("Cannot get system user name"))

    mTitle->setVolatile( true );
    mTitle->setVal( name );
  }
  else
  {
    mTitle->setVolatile( false );
  }

}


/*! @return \a true si l'utilisateur est l'utilisateur OS.
    \fn CYUser::isOSUser()
 */
bool CYUser::isOSUser()
{
  return mOSUser->val();
}

/*! Saisie l'utilisateur parent suivant son index \a id.
    \fn CYUser::setParentGroup( int id )
 */
void CYUser::setParentGroup( int id )
{
  if (id>0) // index 0 réservé à l'accès simulation
  {
    CYUserGroup *group= core->userGroups().value( id );
    if (group)
      setParentGroup(group);
  }
}


/*! Saisie le groupe parent.
    \fn CYUser::setParentGroup( CYUserGroup *group )
 */
void CYUser::setParentGroup( CYUserGroup *group )
{
  mParentGroup = group;
  mParentUser = 0;
}

/*! @return le groupe parent ou 0 s'il n'y en a pas.
    \fn CYUser::parentGroup()
 */
CYUserGroup *CYUser::parentGroup()
{
  return mParentGroup;
}

/*! Saisie l'utilisateur parent suivant son index \a id.
    \fn CYUser::setParentUser( int id )
 */
void CYUser::setParentUser( int id )
{
  if (id>0) // index 0 réservé à l'accès simulation
  {
    CYUser *user= core->users().value( id );
    if (user)
      setParentUser(user);
  }
}

/*! Saisie l'utilisateur parent.
    \fn CYUser::setParentUser( CYUser *user )
 */
void CYUser::setParentUser( CYUser *user )
{
  mParentUser = user;
  mParentGroup=0;
}

/*! @return le utilisateur parent ou 0 s'il n'y en a pas.
    \fn CYUser::parentUser()
 */
CYUser *CYUser::parentUser()
{
  return mParentUser;
}

/*! @return false si l'action est interdite par l'un de ces parents
    \fn CYUserGroup::actionUsed(CYUserAction *action, CYWin *win)
 */
bool CYUser::actionUsed(CYUserAction *action, CYWin *win)
{
  if (mParentGroup && !mParentGroup->actionUsed(action, win))
    return false;
  if (mParentUser && !mParentUser->actionUsed(action, win))
    return false;
  return true;
}


/*! @return true si l'utilisateur est parent au groupe \a group
    \fn CYUser::isParent(CYUserGroup *group)
 */
bool CYUser::isParent(CYUserGroup *group)
{
  if ((group->administrator()) || (group->designer()) || (group->simulation()))
    return false;

  if (group->parentUser())
  {
    if ((index()==group->parentUser()->index()) || isParent(group->parentUser()))
      return true;
  }
  if (group->parentGroup())
  {
    if (isParent(group->parentGroup()))
      return true;
  }
  return false;
}


/*! @return true si l'utilisateur est parent de \a user
    \fn CYUser::actionUsed(CYUser *user)
 */
bool CYUser::isParent(CYUser *user)
{
  if ((user==core->administrator()) || (user->designer()) || (user->simulation()))
    return false;

  if (user->parentUser())
  {
    if ((index()==user->parentUser()->index()) || isParent(user->parentUser()))
      return true;
  }
  if (user->parentGroup())
  {
    if (isParent(user->parentGroup()))
      return true;
  }

  return false;
}

/*! @return TRUE si l'utilisateur est au même niveau que \a user
    \fn CYUser::isSameLevel(CYUser *user)
 */
bool CYUser::isSameLevel(CYUser *user)
{
  if (user->parentUser())
  {
    if (parentUser() && (parentUser()==user->parentUser()))
      return true;
  }
  if (user->parentGroup())
  {
    if (parentGroup() && (parentGroup()==user->parentGroup()))
       return true;
  }

  return false;
}

/*! @return TRUE si l'utilisateur est au même niveau que \a group
    \fn CYUser::isSameLevel(CYUserGroup *group)
 */
bool CYUser::isSameLevel(CYUserGroup *group)
{
  if (group->parentUser())
  {
    if (parentUser() && (parentUser()==group->parentUser()))
      return true;
  }
  if (group->parentGroup())
  {
    if (parentGroup() && (parentGroup()==group->parentGroup()))
       return true;
  }

  return false;
}
