/***************************************************************************
                          cyusergroup.h  -  description
                             -------------------
    begin                : ven oct 31 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYUSERGROUP_H
#define CYUSERGROUP_H

// QT
#include <QObject>
#include <QList>
#include <QHash>

class CYUser;
class CYDB;
class CYString;
class CYFlag;
class CYU8;
class CYUserAction;
class CYUserActionCollection;
class CYWin;

/** Groupe d'utilisateurs donnant des droits d'utilisation de cylix.
  * Un utilisateur (CYUser) doit faire parti au moins d'un de ses groupes.
  * @short Groupe d'utilisateurs.
  * @author Gérald LE CLEACH
  */

class CYUserGroup : public QObject
{
  Q_OBJECT
public:

    /** Actions de protection de l'économiseur d'écran. */
    enum ScreenSaver
    {
      /** Force CYLIX en accès protégé. */
      PROTECT_CYLIX = 0,
      /** Arrête de la session LINUX. */
      LOGOUT_LINUX,
    };

    CYUserGroup(QObject *parent, int id, QString title);
    ~CYUserGroup();

    /** @return la base données propre au groupe d'utilisateurs */
    CYDB * db() { return mDB; }
    /** @return le titre. */
    QString title();

    /** Ajoute un nouvel utilisateur. */
    void addUser(CYUser *u);
    /** Supprime un nouvel utilisateur. */
    void removeUser(CYUser *u);

    /** @return \a true si c'est le groupe de l'utilisateur qui gère l'accès protégé. */
    bool isProtectedAccess();

    /** @return l'index utilisé dans le fichier XML *.rc. */
    uint index() { return mIndex; }
    /** Saisie l'index utilisé dans le fichier XML *.rc. */
    void setIndex(uint i) { mIndex = i; }

    /** @return \a true si ce groupe donne les droits de constructeur. */
    bool designer() { return mDesigner; }
    /** Saisir \a true pour que ce groupe ait les droits de constructeur. */
    void setDesigner(bool val) { mDesigner = val; }

    /** @return \a true si ce groupe donne les droits de simulation. */
    bool simulation() { return mSimulation; }
    /** Saisir \a true pour que ce groupe ait les droits de simulation. */
    void setSimulation(bool val) { mSimulation = val; }

    /** @return \a true si ce groupe donne les droits d'administrateur. */
    bool administrator() { return mAdministrator; }
    /** Saisir \a true pour que ce groupe ait les droits d'administarteur. */
    void setAdministrator(bool val) { mAdministrator = val; }

    /** @return \a true si ce groupe est authorisé à effectuer des actions de forçage de données en mode forçage. */
    bool enableForcing() { return mEnableForcing; }
    /** Authorise les actions de forçage de données en mode forçage si \a ok vaut \a true. */
    void setEnableForcing(bool ok) { mEnableForcing = ok; }
    QHash<QString, CYUserActionCollection*> & actionCollections();
    CYUserActionCollection *windowCollection(QString windowLabel);

    virtual void addWindow( CYWin *window );
    virtual bool load();
    virtual bool load(const QString &fileName);
    virtual bool save();
    virtual bool save(const QString &fileName);
    virtual bool protectStarting();
    virtual unsigned short screenSaverAction();
    virtual bool screenSaverProtection();
    virtual bool desktopEnvironmentProtected();
    virtual CYUserGroup *parentGroup();
    virtual CYUser *parentUser();
    virtual bool actionUsed(CYUserAction *action, CYWin *win);
    virtual bool isParent(CYUserGroup *group);
    virtual bool isParent(CYUser *user);

public slots:
    virtual void updateGUI();
    virtual void updateGUI( QString collection );
    virtual void updateGUI( CYUserActionCollection *collection );
    virtual void setProtectStarting( bool val );
    virtual void setScreenSaverProtection( bool val );
    virtual void setProtectDesktopEnvironment( bool val );
    virtual void setParentGroup( int index );
    virtual void setParentGroup( CYUserGroup *group );
    virtual void setParentUser( int index );
    virtual void setParentUser( CYUser *user );

protected: // Protected attributes
    /** Liste des utilisateurs qu'il regroupe. */
    QList<CYUser*> mUsers;

    CYDB * mDB;
    CYUserGroup *mParentGroup;
    CYUser *mParentUser;

    /** Titre. */
    CYString *mTitle;

    /** Index utilisé dans le fichier XML *.rc. */
    uint mIndex;

    /** Vaut \a true si ce groupe donne les droits de constructeur. */
    bool mDesigner;
    /** Vaut \a true si ce groupe donne les droits de simulation. */
    bool mSimulation;
    /** Vaut \a true si ce groupe donne les droits d'administrateur. */
    bool mAdministrator;

    /** Authorise ou non les actions de forçage de données en mode forçage. */
    bool mEnableForcing;

    CYFlag *mStarting;

    CYFlag *mScreensaver;
    CYU8   *mScreensaverAction;

    CYFlag *mDesktopEnvironmentProtected;

    QHash<QString, CYUserActionCollection*> mActionCollections;

    QString mFileName;
    /** Version de CYLIBS lors de l'enregistrement du fichier de sauvegarde */
    QString mVersionCYDOM;
};

#endif
