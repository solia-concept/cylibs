//
// C++ Interface: cyusergroupedit
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYUSERGROUPEDIT_H
#define CYUSERGROUPEDIT_H

// QT
#include <QTreeWidgetItem>
#include <QHash>
// CYLIBS
#include <cydialog.h>

class CYUserGroup;
class CYUserActionCollection;

namespace Ui {
		class CYUserGroupEdit;
}

/**
@short Boîte d'édition d'un groupe d'utilisateurs

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYUserGroupEdit : public CYDialog
{
  Q_OBJECT
public:
    CYUserGroupEdit( CYUserGroup *group, QWidget* parent = 0, const char* name = 0 );

    ~CYUserGroupEdit();

public slots:
    virtual void apply();
    virtual void update();

protected slots:
    virtual void setCollection( const QString &key );

protected:
    void change( QTreeWidgetItem *item, bool used );
    virtual void insertActive( QTreeWidgetItem *item );
    virtual void removeActive( QTreeWidgetItem *item );

    CYUserGroup * mGroup;
    QHash<QString, CYUserActionCollection*> mCollections;

protected slots:
    virtual void slotInactiveSelected(QTreeWidgetItem *item, QTreeWidgetItem *previous);
    virtual void slotActiveSelected(QTreeWidgetItem *item, QTreeWidgetItem *previous);
    virtual void slotInsertButton(QTreeWidgetItem * item, int column);
    virtual void slotRemoveButton(QTreeWidgetItem * item, int column);
    virtual void slotInsertButton();
    virtual void slotRemoveButton();
    virtual void accept();
    virtual void reject();
    virtual void help();

private:
    Ui::CYUserGroupEdit *ui;
};

#endif
