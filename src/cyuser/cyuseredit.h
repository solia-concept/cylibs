//
// C++ Interface: cyuseredit
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYUSEREDIT_H
#define CYUSEREDIT_H

#include <cydialog.h>

class CYUser;

namespace Ui {
		class CYUserEdit;
}

/**
@short Boîte d'édition d'un utilisateur.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYUserEdit : public CYDialog
{
public:
    CYUserEdit(CYUser *user, QWidget *parent = 0, const QString &name = 0);

    ~CYUserEdit();

protected:
    CYUser * mUser;
public slots:
    virtual void apply();
    void accept();
    virtual void reject();
protected slots:
    virtual void help();


private:
    Ui::CYUserEdit *ui;
};

#endif
