//
// C++ Interface: cyuseradminedit2
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYUSERADMINEDIT2_H
#define CYUSERADMINEDIT2_H

// QT
#include <QPixmap>
#include <QTreeWidget>
// CYLIBS
#include <cydialog.h>

#define COL_NAME  0
#define COL_GROUP 1
#define COL_USER  2

class CYUserGroup;
class CYUser;

namespace Ui {
		class CYUserAdminEdit2;
}

/**
@short Boîte d'administration utilisateur.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYUserAdminEdit2 : public CYDialog
{
  Q_OBJECT

public:
    CYUserAdminEdit2(QWidget *parent = 0, const QString &name = 0);

    ~CYUserAdminEdit2();
    void init();

    virtual QTreeWidgetItem *addGroupItemList(CYUserGroup *group);
    virtual QTreeWidgetItem *addUserItemList(CYUser *user);
    CYUserGroup * userGroup(QTreeWidgetItem *item);
    CYUser * user(QTreeWidgetItem *item);
    bool overAccess();

signals:
//     void itemRenamed(const QString &);
    void listModified(bool state);

public slots:
    virtual void updateList();
    virtual void setListModified();
    virtual void setListModified(bool val);
    virtual void itemSwap(bool up);
    virtual void edit( QTreeWidgetItem *item, int column );

protected slots:
    virtual void edit( QTreeWidgetItem *item );
    virtual bool remove( QTreeWidgetItem *item, bool force=false);
    virtual void help();

    virtual void edit();
    virtual void remove();

    virtual void addUser();
    virtual void addOSUser();
    virtual void addUser( bool linux_user );
    virtual void removeUser(CYUser *user);

    virtual void addGroup();
    virtual void removeGroup(CYUserGroup *group);
    virtual void currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);

protected slots:
    void applyClicked();
    void itemDeleteClicked();
    void itemNewClicked();
    void itemNewSubClicked();
    void itemDownClicked();
    void itemUpClicked();
    void itemLeftClicked();
    void itemRightClicked();
    void okClicked();

private:
    void transferItems( QTreeWidget *from, QTreeWidget *to );
    void updateChild(QTreeWidgetItem *parent);

private:
    QTreeWidget *listview;
    int numColumns;
    QPixmap mManPixmap;
    QPixmap mWomanPixmap;
    QPixmap mGroupPixmap;
    bool mNotOSUser;

private:
    Ui::CYUserAdminEdit2 *ui;
};

#endif
