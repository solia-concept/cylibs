/***************************************************************************
                          cyusergroup.cpp  -  description
                             -------------------
    begin                : ven oct 31 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// QT
#include <QDomElement>
#include <qapplication.h>
#include <qdir.h>
#include <qlocale.h>
//Added by qt3to4:
#include <QTextStream>
// CYLIBS
#include "cyusergroup.h"
#include "cyuser.h"
#include "cycore.h"
#include "cydb.h"
#include "cystring.h"
#include "cyflag.h"
#include "cyu8.h"
#include "cyuseractioncollection.h"
#include "cywin.h"
#include "cyuseraction.h"
#include "cymessagebox.h"
#include "cyglobal.h"

CYUserGroup::CYUserGroup(QObject *parent, int id, QString title)
  : QObject(parent),
    mIndex( id )
{
  setObjectName(QString("usergroup_%1").arg(id));
  mDB = new CYDB( this );
  mDB->setSaveWithApply( false );
  mDB->setDOMTagDatas( "CYUserGroup" );
  mDB->setGroup( tr("Users group") );

  mTitle = new CYString( mDB, "TITLE", new QString( title ), tr("Name"));

  mStarting = new CYFlag( mDB, "STARTING", new flg, true );
  mStarting->setLabel( tr("CYLIX starting protected") );
  mStarting->setHelp( tr("CYLIX can start automatically with the current user at the time of the preceding stop if the group of this one allows it."), tr("CYLIX starts in protected access."), 0);

  mScreensaver = new CYFlag( mDB, "SCREENSAVER", new flg, false );
  mScreensaver->setLabel( tr("Screensaver protection") );

  mScreensaverAction = new CYU8( mDB, "SCREENSAVER_ACTION", new u8 );
  mScreensaverAction->setLabel( tr("Screensaver protection action") );
  mScreensaverAction->setChoiceLabel( PROTECT_CYLIX, tr("Force CYLIX in protected access") );
  mScreensaverAction->setChoiceLabel( LOGOUT_LINUX, tr("Stop the LINUX session") );
  mScreensaverAction->setDef( PROTECT_CYLIX );
  mScreensaverAction->designer();

  mDesktopEnvironmentProtected = new CYFlag( mDB, "PROTECT_OS", new flg, false );
  mDesktopEnvironmentProtected->setLabel( tr("Protect access to desktop environment") );
  mDesktopEnvironmentProtected->setHelp( tr("Restricts access to the desktop environment by disabling its panel.") );
  mDesktopEnvironmentProtected->addNote(tr("Configuration is not possible if one of the parents has enabled this protection.") );

  QHashIterator<QString, CYWin *> it(core->windows());
  while (it.hasNext())
  {
    it.next();                   // must come first
    CYWin *win = it.value();
    addWindow( win );
    win->initHelpMenu();
  }

  mDesigner       = false;
  mSimulation     = false;
  mAdministrator  = false;
  mParentGroup    = (core->administrator()) ? core->administrator()->group() : 0;
  mParentUser     = 0;
}

CYUserGroup::~CYUserGroup()
{
}

QString CYUserGroup::title()
{
  return mTitle->val();
}


void CYUserGroup::addUser(CYUser *u)
{
//  u->addUserGroup(this);
  mUsers.append(u);
}

void CYUserGroup::removeUser(CYUser *u)
{
//  u->removeUserGroup(this);
  mUsers.removeAll(u);
}


bool CYUserGroup::isProtectedAccess()
{
  QListIterator<CYUser *> i(mUsers);
  while (i.hasNext())
  {
    CYUser *u=i.next();
    if ( u->isProtectedAccess() )
      return true;
  }

  return false;
}


/*! @return les collections d'actions.
    \fn CYUserGroup::actionCollections()
 */
QHash<QString, CYUserActionCollection*> & CYUserGroup::actionCollections()
{
  return mActionCollections;
}

/*! @return la collection d'actions de la fenêtre d'étiquette \a windowLabel.
    \fn CYUserGroup::windowCollection(QString windowLabel)
 */
CYUserActionCollection *CYUserGroup::windowCollection(QString windowLabel)
{
  QHashIterator<QString, CYUserActionCollection *> i(mActionCollections);
  while (i.hasNext())
  {
    i.next();
    CYWin *win = i.value()->window();
    QString tmp=QString("%1: %2").arg(win->index()).arg(win->label());
    if (tmp==windowLabel)
    {
      return i.value();
    }
  }

  return 0;
}


/*! Ajoute une fenêtre à gérer
    \fn CYUserGroup::addWindow( CYWin *window )
 */
void CYUserGroup::addWindow( CYWin *window )
{
  mActionCollections.insert( window->metaObject()->className(), new CYUserActionCollection( this, window) );
}


/*! Chargement des paramètres du groupe utilisateur.
    \fn CYUserGroup::load()
 */
bool CYUserGroup::load()
{
  QDir dir( core->appDataDir()+"/users" );
  if ( !dir.exists() )
    return false;
  return load( QString("%1/group_%2.cyusr").arg(dir.absolutePath()).arg( index()) );
}


/*! Chargement des paramètres du groupe utilisateur.
    \fn CYUserGroup::load(const QString &fileName)
 */
bool CYUserGroup::load(const QString &fileName)
{
  QFile file(mFileName = fileName);

  if ( !file.exists() )
    return true;

  if (!file.open(QIODevice::ReadOnly))
  {
    CYMessageBox::sorry(0, tr("Can't open the file %1").arg(mFileName));
    return (false);
  }

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  QString errorMsg;
  int errorLine;
  int errorColumn;
  if (!doc.setContent(&file, &errorMsg, &errorLine, &errorColumn))
  {
    CYMessageBox::sorry(0, tr("The file %1 does not contain valid XML\n"
                                  "%2: line:%3 colomn:%4").arg(mFileName).arg(errorMsg).arg(errorLine).arg(errorColumn));
    return (false);
  }
  // Vérifie le type propre du document.
  if (doc.doctype().name() != "CYLIXUserGroup")
  {
    CYMessageBox::sorry(0, QString(tr("The file %1 does not contain a valid"
                                          "definition, which must have a document type ").arg(mFileName)
                                          +"'CYLIXUserGroup'"));
    return (false);
  }
  // Vérifie la taille propre.
  QDomElement el = doc.documentElement();

  // Version de la librairie lors de la sauvegarde du document.
  mVersionCYDOM = el.attribute("CYDOM");

  db()->loadFromDOM(el);

  // pyramidale
  setParentGroup( el.attribute("PARENT_GROUP").toInt() );
  setParentUser( el.attribute("PARENT_USER").toInt() );

//   ws.setAttribute("designer", mDesigner);
//   ws.setAttribute("simulation", mSimulation);
//   ws.setAttribute("administrator", mAdministrator);
//   ws.setAttribute("enableForcing", mEnableForcing);

  if (!core->pyramidalUserAdmin())
  {
    /* Charge les utilisateurs appartenant au groupe. */
    QDomNodeList users = el.elementsByTagName("user");
    for (int i = 0; i < users.count(); ++i)
    {
      QDomElement item = users.item(i).toElement();
      CYUser *u = new CYUser( this, item.attribute("id").toInt(), item.attribute("title") );
      u->db()->loadFromDOM(item);
    }
  }

  /* Charge les collections d'actions appartenant au groupe. */
  QDomNodeList collections = el.elementsByTagName("collection");
  for (int i = 0; i < collections.count(); ++i)
  {
    QDomElement item = collections.item(i).toElement();

    CYUserActionCollection *collection;
    QString key = item.attribute("name");
    bool ok;
    QLocale local(QLocale::C);
    float version = local.toFloat(mVersionCYDOM, &ok);
    if (ok && (version<2.89)) // Compatibilté ascendante
      collection = windowCollection(key);
    else
      collection = mActionCollections.value(key);

    if (collection)
    {
      QDomNodeList actions = item.elementsByTagName("action");
      for (int j = 0; j < actions.count(); ++j)
      {
        QDomElement item2 = actions.item(j).toElement();
        CYUserAction *action = collection->actions().value(item2.attribute("name"));
        if (action)
        {
          action->db()->loadFromDOM(item2);
        }
      }
    }
  }
  if (compareVersion(mVersionCYDOM, "5.26")<0)
  {
    // supprime les données de chaque utilisateur car celles-ci sont déjà sauvegardées dans son fichier associé.
    save(fileName);
  }
  return (true);
}


bool CYUserGroup::save()
{
  QDir dir( core->appDataDir()+"/users" );
  if ( !dir.exists() )
    core->mkdir( dir.absolutePath() );
  return save( QString("%1/group_%2.cyusr").arg(dir.absolutePath()).arg( index()) );
}


bool CYUserGroup::save(const QString &fileName)
{
  QDomDocument doc("CYLIXUserGroup");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  // enregistre les informations la feuille de données.
  QDomElement el_group = doc.createElement("CYUserGroup");
  doc.appendChild(el_group);

  el_group.setAttribute("CYDOM", core->version());
  mVersionCYDOM = core->version();

  if (core->pyramidalUserAdmin())
  {
    if (parentGroup())
    {
      el_group.setAttribute("PARENT_GROUP", parentGroup()->index());
    }

    if (parentUser())
    {
      el_group.setAttribute("PARENT_USER", parentUser()->index());
    }
  }

  if ( !db()->hasOnlyDesignerValues() )
    db()->saveToDOM( doc, el_group );

/*  ws.setAttribute("designer", mDesigner);
  ws.setAttribute("simulation", mSimulation);
  ws.setAttribute("administrator", mAdministrator);
  ws.setAttribute("enableForcing", mEnableForcing);
*/
  QDomElement el_users = doc.createElement("users");
  el_group.appendChild(el_users);
  // enregistre les utilisateurs appartenant au groupe. */
  QListIterator<CYUser *> iu(mUsers);
  while (iu.hasNext())
  {
    CYUser *u=iu.next();
    QDomElement el_user = doc.createElement("user");
    el_users.appendChild(el_user);
    el_user.setAttribute("id", u->index());
    if ( !u->db()->hasOnlyDesignerValues() )
      u->db()->saveToDOM( doc, el_user );
  }

  /* enregistre les collections d'actions appartenant au groupe. */
  QDomElement el_collections = doc.createElement("collections");
  el_group.appendChild(el_collections);
  QHashIterator<QString, CYUserActionCollection *> ic(mActionCollections);
  while (ic.hasNext())
  {
    ic.next();
    CYUserActionCollection *collection = ic.value();
    QDomElement el_collection = doc.createElement("collection");
    el_collections.appendChild(el_collection);
    el_collection.setAttribute("name", collection->objectName());

    QHashIterator<QString, CYUserAction *> ia(collection->actions());
    while (ia.hasNext())
    {
      ia.next();
      CYUserAction *action = ia.value();
      if ( !action->db()->hasOnlyDesignerValues() )
      {
        QDomElement el_action = doc.createElement("action");
        el_collection.appendChild(el_action);
        el_action.setAttribute("name", action->objectName());
        action->db()->saveToDOM( doc, el_action );
      }
    }
  }

  QFile file(mFileName = fileName);
  if (!file.open(QIODevice::WriteOnly))
  {
    CYWARNINGTEXT(tr("Can't save file %1!").arg(mFileName));
    return (false);
  }
  QTextStream s(&file);
  s.setCodec("UTF-8");
  s << doc;
  file.close();
  core->backupFile(mFileName);

//   setModified(false);
  return (true);
}



/*! Mise à jour de l'interface graphique par rapport à toutes les collections d'actions.
    \fn CYUserGroup::updateGUI()
 */
void CYUserGroup::updateGUI()
{
  QHashIterator<QString, CYUserActionCollection *> ic(actionCollections());
  while (ic.hasNext())
  {
    ic.next();
    updateGUI( ic.value() );
  }
}


/*! Mise à jour de l'interface graphique par rapport à la collection d'actions ayant pour nom \a collection.
    \fn CYUserGroup::updateGUI( QString collection )
 */
void CYUserGroup::updateGUI( QString collection )
{
  updateGUI( actionCollections().value( collection ) );
}


/*! Mise à jour de l'interface graphique par rapport à la collection d'actions \a collection.
    \fn CYUserGroup::updateGUI( CYUserActionCollection *collection )
 */
void CYUserGroup::updateGUI( CYUserActionCollection *collection )
{
  if ( collection)
  {
    CYWin *win = collection->window();

    if (!win || !win->actionCollection())
      return;

    win->createGUI(win->actionCollection()->xmlFile());
    QHashIterator<QString, CYUserAction *> ia(collection->actions());
    while (ia.hasNext())
    {
      ia.next();
      CYUserAction *userAction = ia.value();
      userAction->action()->setVisible(true);
      if ( isProtectedAccess() )
      {
        if ( userAction->actionEdit() )
        {
          userAction->setEnabled( false );
          userAction->action()->setUsed( false );
        }
      }
      else if ( simulation() )
      {
        userAction->setEnabled( true );
        userAction->action()->setUsed( true );
      }
      else
      {
//        if ( !userAction->isUsed() || (userAction->objectName()=="help_contents") || (userAction->actionDesigner() && !core->designer())
          if ( (userAction->objectName()=="help_contents") || (userAction->actionDesigner() && !core->designer())
             || !actionUsed(userAction, win))
          userAction->unplugAll();

        if ( userAction->actionEdit() )
        {
          userAction->setDisabled( false );
        }
      }
    }
    win->initHelpMenu();
  }
}


/*! Démarrage de CYLIX en accès protégé.
    \fn CYUserGroup::protectStarting()
 */
bool CYUserGroup::protectStarting()
{
  return mStarting->val();
}


/*! Force le démarrage de CYLIX en accès protégé.
    \fn CYUserGroup::setProtectStarting( bool val )
 */
void CYUserGroup::setProtectStarting( bool val )
{
  mStarting->setVal( val );
}


/*! @return \a true si l'économiseur d'écran génère une protection.
    \fn CYUserGroup::screenSaverProtection()
 */
bool CYUserGroup::screenSaverProtection()
{
  return mScreensaver->val();
}


/*! Force la protection de l'économiseur d'écran.
    \fn CYUserGroup::setScreenSaverProtection( bool val )
 */
void CYUserGroup::setScreenSaverProtection( bool val )
{
  // TODO QT5
  CYWARNINGTEXT(tr("This does not work and will be corrected soon."));
  mScreensaver->setVal( val );
}


/*! @return le type d'action de protection que génère l'économiseur d'écran.
    \fn CYUserGroup::screenSaverAction()
 */
unsigned short CYUserGroup::screenSaverAction()
{
  return mScreensaverAction->val();
}


/*! @return \a true si l'accès à l'environnement de bureau est protégé.
    \fn CYUserGroup::desktopEnvironmentProtected()
 */
bool CYUserGroup::desktopEnvironmentProtected()
{
  if (mParentGroup && mParentGroup->desktopEnvironmentProtected())
  {
    return true;
  }
  else
  {
    return mDesktopEnvironmentProtected->val();
  }
}


/*! Force la protection de l'accès à l'environnement de bureau .
    \fn CYUserGroup::setProtectDesktopEnvironment( bool val )
 */
void CYUserGroup::setProtectDesktopEnvironment( bool val )
{
  mDesktopEnvironmentProtected->setVal( val );
}


/*! Saisie l'utilisateur parent suivant son index \a id.
    \fn CYUserGroup::setParentGroup( int id )
 */
void CYUserGroup::setParentGroup( int id )
{
  if (id>0) // index 0 réservé à l'accès simulation
  {
    CYUserGroup *group= core->userGroups().value( id );
    if (group)
      setParentGroup(group);
  }
}


/*! Saisie le groupe parent.
    \fn CYUserGroup::setParentGroup( CYUserGroup *group )
 */
void CYUserGroup::setParentGroup( CYUserGroup *group )
{
  mParentGroup = group;
  mParentUser = 0;
}

/*! @return le groupe parent ou 0 s'il n'y en a pas.
    \fn CYUserGroup::parentGroup()
 */
CYUserGroup *CYUserGroup::parentGroup()
{
  return mParentGroup;
}

/*! Saisie l'utilisateur parent suivant son index \a id.
    \fn CYUserGroup::setParentUser( int id )
 */
void CYUserGroup::setParentUser( int id )
{
  if (id>0) // index 0 réservé à l'accès simulation
  {
    CYUser *user= core->users().value( id );
    if (user)
      setParentUser(user);
  }
}


/*! Saisie l'utilisateur parent.
    \fn CYUserGroup::setParentUser( CYUser *user )
 */
void CYUserGroup::setParentUser( CYUser *user )
{
  mParentUser = user;
  mParentGroup = nullptr;
}

/*! @return le utilisateur parent ou 0 s'il n'y en a pas.
    \fn CYUserGroup::parentUser()
 */
CYUser *CYUserGroup::parentUser()
{
  return mParentUser;
}

/*! @return false si l'action est interdite par ce groupe ou l'un de ces parents
    \fn CYUserGroup::actionUsed(CYUserAction *action, CYWin *win)
 */
bool CYUserGroup::actionUsed(CYUserAction *action, CYWin *win)
{
  CYUserActionCollection *local_collection = actionCollections().value(win->metaObject()->className());
  if (!local_collection)
  {
    CYWARNINGTEXT(tr("Unable to find the collection of action %1 in user group %2").arg(action->plainText()).arg(title()));
    return false;
  }

  CYUserAction *local_action = local_collection->actions().value(action->objectName());
  if (!action)
  {
    CYWARNINGTEXT(tr("Unable to find action %1 in user group %2").arg(action->plainText()).arg(title()));
    return false;
  }

  if (!local_action->isUsed(true))
    return false;

  if (mParentGroup && !mParentGroup->actionUsed(action, win))
    return false;
  if (mParentUser && !mParentUser->actionUsed(action, win))
    return false;
  return true;
}


/*! @return true si le groupe est parent au groupe \a group
    \fn CYUserGroup::isParent(CYUserGroup *group)
 */
bool CYUserGroup::isParent(CYUserGroup *group)
{
  if (group->parentGroup())
  {
    if ((index()==group->parentGroup()->index()) || isParent(group->parentGroup()))
      return true;
  }
  if (group->parentUser())
  {
    if (isParent(group->parentUser()))
      return true;
  }
  return false;
}


/*! @return true si le groupe est parent de l'utilisateur \a user
    \fn CYUserGroup::isParent(CYUser *user)
 */
bool CYUserGroup::isParent(CYUser *user)
{
  if (user->parentGroup())
  {
    if ((index()==user->parentGroup()->index()) || isParent(user->parentGroup()))
      return true;
  }
  if (user->parentUser())
  {
    if (isParent(user->parentUser()))
      return true;
  }
  return false;
}
