//
// C++ Interface: cyuseraction
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYUSERACTION_H
#define CYUSERACTION_H

// QT
#include <QObject>
#include <QList>

class CYAction;
class CYDB;
class CYUserActionCollection;
class CYFlag;

/**
@short Action utilisateur paramètrable par le groupe d'utilisateurs auquel il appartient et ayant une CYAction associée.

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYUserAction : public QObject
{
  Q_OBJECT

public:
  CYUserAction( CYUserActionCollection *collection, CYAction *action );

  ~CYUserAction();

  /** @return la base données propre à l'action */
  CYDB * db() { return mDB; }

  CYAction * action();
  bool isUsed(bool notify=false);
  virtual QString plainText();
  CYFlag * used();
  virtual void addDisableFlag( CYFlag *flag );
  virtual void addDisableFlag( QString flagName );
  virtual bool actionEdit();
  virtual bool actionDesigner();

public slots:
  virtual void unplugAll();
  virtual void setDisabled( bool rising );
  virtual void setEnabled( bool rising );

protected:
  CYDB * mDB;

  CYUserActionCollection *mCollection;
  CYAction * mAction;
  CYFlag * mUsed;
  QList<CYFlag*> mDisableFlags;
  QList<CYFlag*> mEnableFlags;
};

#endif
