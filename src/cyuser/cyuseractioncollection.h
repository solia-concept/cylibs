//
// C++ Interface: cyuseractioncollection
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYUSERACTIONCOLLECTION_H
#define CYUSERACTIONCOLLECTION_H

// QT
#include <QObject>
#include <QHash>

class CYWin;
class CYUserGroup;
class CYUserAction;

/**
@short Collection d'actions utilisateurs CYUserACtion par fenêtre.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYUserActionCollection : public QObject
{
public:
    CYUserActionCollection( CYUserGroup *group, CYWin *win );

    ~CYUserActionCollection();
    QHash<QString, CYUserAction*> &actions();
    CYWin * window();

protected:
    CYUserGroup * mGroup;
    CYWin *mWindow;
    QHash<QString, CYUserAction*> mActions;
};

#endif
