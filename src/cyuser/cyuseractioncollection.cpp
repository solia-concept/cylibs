//
// C++ Implementation: cyuseractioncollection
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
// CYLIBS
#include "cy.h"
#include "cyuseractioncollection.h"
#include "cyusergroup.h"
#include "cyuseraction.h"
#include "cywin.h"
#include "cycore.h"

CYUserActionCollection::CYUserActionCollection( CYUserGroup *group, CYWin *window )
 : QObject( group ),
   mGroup( group ),
   mWindow( window )
{
  setObjectName(window->metaObject()->className());
  CYActionPtrList list = window->actionCollection()->actions();
  CYActionPtrList::iterator it;
  for ( it = list.begin(); it != list.end(); ++it )
  {
    CYAction *action = *it;
    CYUserAction * ua = new CYUserAction( this, action );
    mActions.insert( action->objectName(), ua );

    QString *flagList = window->actionDisable().value( action->objectName() );
    if ( flagList && !flagList->isEmpty() )
    {
      int nb = flagList->count(',');
      if ( nb > 0 )
      {
        for (int i=0; i<=nb; i++)
        {
          ua->addDisableFlag( flagList->section(',', i, i) );
        }
      }
      else
        ua->addDisableFlag( *flagList );
    }
  }
}


CYUserActionCollection::~CYUserActionCollection()
{
}


/*! @return les actions utilisateurs.
    \fn CYUserActionCollection::actions()
 */
QHash<QString, CYUserAction*> & CYUserActionCollection::actions()
{
  return mActions;
}


/*! Fenêtre dans laquelle s'applique cette collection.
    \fn CYUserActionCollection::window()
 */
CYWin * CYUserActionCollection::window()
{
  return mWindow;
}
