/***************************************************************************
                          cyuser.h  -  description
                             -------------------
    begin                : ven oct 31 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYUSER_H
#define CYUSER_H

// QT
#include <QObject>

class CYDB;
class CYString;
class CYFlag;
class CYWin;
class CYUserGroup;
class CYUserAction;
class CYUserActionCollection;

/** Un utilisateur a des droits d'accès dans le superviseur en fonction
  * du ou des groupes (CYUserGroup) auquel il appartient.
  * @short Utilisateur.
  * @author Gérald LE CLEACH
  */

class CYUser : public QObject
{
  Q_OBJECT
public:
  CYUser(QObject *parent, int id, QString desc);
  ~CYUser();

  /** @return le titre. */
  QString title();

  /** @return \a true si c'est l'utilisateur qui gère l'accès protégé. */
  bool isProtectedAccess();

  /** @return le mot de passe. */
  QString password();
  /** Saisie le mot de passe. */
  void setPassword(QString pass);

  /** @return \a true si l'utilisateur a un mot de passe. */
  bool hasPassword();
  /** Saisir \a true pour que l'utilisateur ait un mot de passe. */
  void setHasPassword(bool val);

  /** Ajoute un nouveau groupe d'utilisateurs. */
  void addUserGroup(CYUserGroup *g);
  /** Supprime un nouveau groupe d'utilisateurs. */
  void removeUserGroup(CYUserGroup *g);
  /** Vérifie le code d'accès de l'utilisateur courant.
    * @param parent Sort sur ce widget plutôt que la fenêtre principale de Cylix.
    * @parma note Ajoute un texte explicatif.
    * @return 0 en cas d'échec, 1 si ok, 2 si mot de passe constructeur et 3 si mot de passe simulation, */
  int checkPassword(QWidget *parent = 0, QString note=0);

  /** Changement de mot de passe. */
  void changePassword();
  virtual bool desktopEnvironmentProtected();

  /** @return l'index utilisé dans le fichier XML *.rc. */
  uint index() { return mIndex; }
  /** Saisie l'index utilisé dans le fichier XML *.rc. */
  void setIndex(uint i) { mIndex = i; }

  /** Saisie le groupe d'utilisateurs ayant pour index \a id auquel il appartient. */
  void setGroup(int id);
  /** Saisie le groupe d'utilisateurs auquel il appartient. */
  void setGroup(CYUserGroup *group);
  /** @return le groupe d'utilisateurs auquel il appartient. */
  CYUserGroup *group();
  CYDB * db();

  /** @return \a true si cet utilisateur a les droits de constructeur. */
  bool designer();
  /** Saisir \a true pour que cet utilisateur ait les droits de constructeur. */
  void setDesigner(bool val);

  /** @return \a true si cet utilisateur a les droits de simulation. */
  bool simulation();
  /** Saisir \a true pour que cet utilisateur ait les droits de simulation. */
  void setSimulation(bool val);

  /** @return \a true si cet utilisateur a les droits d'administrateur. */
  bool administrator();
  /** Saisir \a true pour que cet utilisateur ait les droits d'administarteur. */
  void setAdministrator(bool val);

  virtual bool load();
  virtual bool load(const QString &fileName);
  virtual bool save();
  virtual bool save(const QString &fileName);
  virtual bool isOSUser();
  virtual CYUserGroup *parentGroup();
  virtual CYUser *parentUser();
  virtual bool actionUsed(CYUserAction *action, CYWin *win);
  virtual bool isParent(CYUserGroup *group);
  virtual bool isParent(CYUser *user);
  virtual bool isSameLevel(CYUser *user);
  virtual bool isSameLevel(CYUserGroup *group);

public slots:
  virtual void updateGUI();
  virtual void updateGUI( QString collection );
  virtual void updateGUI( CYUserActionCollection *collection );
  virtual void setOSUser( bool val );
  virtual void setParentGroup( int index );
  virtual void setParentGroup( CYUserGroup *group );
  virtual void setParentUser( int index );
  virtual void setParentUser( CYUser *user );

protected: // Protected attributes
  CYDB * mDB;

  /** Titre. */
  CYString *mTitle;

  /** Utilisateur CYLIX. */
  CYFlag * mOSUser;

  /** Mot de passe. */
  CYString * mPassword;
  CYFlag * mHasPassword;

  /** Index utilisé dans le fichier XML *.rc. */
  uint mIndex;
  /** Groupe d'utilisateurs auquel il appartient. */
  CYUserGroup *mGroup;

  CYUserGroup *mParentGroup;
  CYUser *mParentUser;

  QString mFileName;
  /** Version de CYLIBS lors de l'enregistrement du fichier de sauvegarde */
  QString mVersionCYDOM;
};

#endif
