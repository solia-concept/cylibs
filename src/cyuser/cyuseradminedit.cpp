//
// C++ Implementation: cyuseradminedit
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

// QT
#include <QTreeWidget>
#include <QPushButton>
// CYLIBS
#include "cyuseradminedit.h"
#include "ui_cyuseradminedit.h"
#include "cyuser.h"
#include "cyusergroup.h"
#include "cycore.h"
#include "cyuseredit.h"
#include "cyusergroupedit.h"
#include "cymessagebox.h"

CYUserAdminEdit::CYUserAdminEdit(QWidget *parent, const QString &name )
  : CYDialog(parent,name), ui(new Ui::CYUserAdminEdit)
{
  ui->setupUi(this);
  init();
}


CYUserAdminEdit::~CYUserAdminEdit()
{
  delete ui;
}


/*!
    \fn CYUserAdminEdit::init()
 */
void CYUserAdminEdit::init()
{
  updateUserList();
  updateGroupList();
}


/*! Edite l'utilisateur sélectioné dans la liste d'utilisateurs
    \fn CYUserAdminEdit::editUser( QTreeWidgetItem *item )
 */
void CYUserAdminEdit::editUser( QTreeWidgetItem *item )
{
  if ( item )
  {
    CYUser *u = core->users().value( item->text(0).toInt() );
    if ( u )
    {
      CYUserEdit *dlg = new CYUserEdit( u, this );
      dlg->exec();
      updateUserList();
    }
    else
      CYWARNINGTEXT( item->text(0) );
  }
}


/*! Edite de groupe d'utilisateurs sélectioné dans la liste des groupes.
    \fn CYUserAdminEdit::editUserGroup( QTreeWidgetItem *item )
 */
void CYUserAdminEdit::editUserGroup( QTreeWidgetItem *item )
{
  if ( item )
  {
    CYUserGroup *g = core->userGroups().value( item->text(0).toInt() );
    if ( g )
    {
      CYUserGroupEdit *dlg = new CYUserGroupEdit( g, this );
      dlg->exec();
      updateGroupList();
      updateUserList();
    }
    else
      CYWARNINGTEXT(item->text(0));
  }
}


/*! Mise à jour de la liste d'utilisateurs
    \fn CYUserAdminEdit::updateUserList()
 */
void CYUserAdminEdit::updateUserList()
{
  bool noOSUser = true;
  ui->userList->clear();
  QHashIterator<int, CYUser *> it(core->users());
  while (it.hasNext())
  {
    it.next();                   // must come first
    CYUser *user = it.value();
    if ( !user->designer() && !user->simulation() && !user->isProtectedAccess() && (core->administrator()!=user) )
    {
      QStringList list;
      list<<QString::number(user->index())<<user->title()<<user->group()->title();
      QTreeWidgetItem *lvi = new QTreeWidgetItem( ui->userList, list);
      ui->userList->addTopLevelItem(lvi);
    }
    if ( user->isOSUser() )
      noOSUser = false;
  }
  ui->addOSUserButton->setEnabled( noOSUser );
}


/*! Mise à jour de la liste des groupes utilisateurs
    \fn CYUserAdminEdit::updateGroupList()
 */
void CYUserAdminEdit::updateGroupList()
{
  ui->groupList->clear();
  QHashIterator<int, CYUserGroup *> it(core->userGroups());
  while (it.hasNext())
  {
    it.next();                   // must come first
    CYUserGroup *group = it.value();
    if ( !group->designer() && !group->simulation() && !group->isProtectedAccess() && !group->administrator() )
    {
      QStringList list;
      list<<QString::number(group->index())<<group->title();
      QTreeWidgetItem *lvi = new QTreeWidgetItem( ui->groupList, list);
      ui->groupList->addTopLevelItem(lvi);
    }
  }
}


/*! Affiche l'aide relative du manuel.
    \fn CYUserAdminEdit::help()
 */
void CYUserAdminEdit::help()
{
  core->invokeHelp( "cydoc", "user" );
}


/*! Ajoute un utilisateur
    \fn CYUserAdminEdit::addUser()
 */
void CYUserAdminEdit::addUser()
{
  addUser( false );
}


/*! Ajoute un utilisateur Linux
    \fn CYUserAdminEdit::addOSUser()
 */
void CYUserAdminEdit::addOSUser()
{
  addUser( true );
}


/*! Ajoute un utilisateur Linux ou non selon \a linux_user.
    \fn CYUserAdminEdit::addUser( bool linux_user )
 */
void CYUserAdminEdit::addUser( bool linux_user )
{
  int id = core->users().count();
  while ( core->users().value(id) )
  {
    id++;
  }

  CYUser * u = new CYUser( core, id, "" );
  u->setOSUser( linux_user );
  u->setGroup( core->userGroup( tr("Administrator") ) );
  CYUserEdit *dlg = new CYUserEdit( u, this );
  if ( dlg->exec() )
  {
    core->users().insert(id, u);
    updateUserList();
  }
}

/*! Suppression de l'utilisateur sélectionné dans la liste.
    \fn CYUserAdminEdit::removeUser()
 */
void CYUserAdminEdit::removeUser()
{
  QTreeWidgetItem *item = ui->userList->currentItem();
  if ( item )
  {
    int index = item->text(0).toInt();
    CYUser * u = core->users().value( index );
    if ( u )
    {
      if ( core->removeUser( u ) )
      {
        if (CYMessageBox::Yes !=
            CYMessageBox::warningYesNo(this, "<p align=""center"">"+tr("Are you sure to remove the user %1 ?").arg(item->text(1))+"</p>"))
          return ;

        QFile file( core->appDataDir()+QString("/users/user_%1.cyusr").arg( index ) );
        file.remove();
        updateUserList();
      }
    }
  }
}


/*! Edition de l'utilisateur sélectionné dans la liste.
    \fn CYUserAdminEdit::editUser()
 */
void CYUserAdminEdit::editUser()
{
  editUser( ui->userList->currentItem() );
}


/*! Ajoute un groupe d'utilisateurs
    \fn CYUserAdminEdit::addGroup()
 */
void CYUserAdminEdit::addGroup()
{
  int id = 0;
  while ( core->userGroups().value(id) )
    id++;

  CYUserGroup * g = new CYUserGroup( core, id, "" );
  CYUserGroupEdit *dlg = new CYUserGroupEdit( g, this );
  if ( dlg->exec() )
  {
    core->userGroups().insert(id, g);
    updateGroupList();
    updateUserList();
  }
}


/*! Suppression du groupe d'utilisateurs sélectionné dans la liste.
    \fn CYUserAdminEdit::removeGroup()
 */
void CYUserAdminEdit::removeGroup()
{
  QTreeWidgetItem *item = ui->groupList->currentItem();
  if ( item )
  {
    int index = item->text(0).toInt();
    if (CYMessageBox::Yes !=
        CYMessageBox::warningYesNo(this, "<p align=""center"">"+tr("Are you sure to remove the users group %1 ?").arg(item->text(1))+"</p>"))
      return ;
    CYUserGroup * g = core->userGroups().value( index );
    if ( g )
    {
      if ( core->removeUserGroup( g ) )
      {
        QFile file( core->appDataDir()+QString("/users/group_%1.cyusr").arg( index ) );
        file.remove();
        updateGroupList();
      }
    }
  }
}


/*! Edition du groupe d'utilisateurs sélectionné dans la liste.
    \fn CYUserAdminEdit::editGroup()
 */
void CYUserAdminEdit::editGroup()
{
  editUserGroup( ui->groupList->currentItem() );
}
