//
// C++ Implementation: cyusergroupedit
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <QToolButton>
#include <QHeaderView>
#include <QTreeWidget>
#include <QApplication>
// CYLIBS
#include "cyusergroupedit.h"
#include "ui_cyusergroupedit.h"
#include "cyusergroup.h"
#include "cycore.h"
#include "cywin.h"
#include "cyuseractioncollection.h"
#include "cyuseraction.h"
#include "cyflag.h"
#include "cyuser.h"

CYUserGroupEdit::CYUserGroupEdit( CYUserGroup *group, QWidget* parent,  const char* name )
  : CYDialog(parent,name),
    mGroup( group )
{
  ui=new Ui::CYUserGroupEdit;
  ui->setupUi(this);
  setLocaleDB( mGroup->db() );
  ui->insertAction->setEnabled(false);
  ui->removeAction->setEnabled(false);

  if (!core->desktopEnvironmentProtectable())
  {
    ui->desktopEnvironmentProtected->hide();
    ui->desktopEnvironmentProtectedLabel->hide();
  }
  else
  {
    if (group->parentGroup() &&
        group->parentGroup()->desktopEnvironmentProtected())
    {
      // héritage du paramétrage parent(s)
      ui->desktopEnvironmentProtected->setEnabled(false);
      ui->desktopEnvironmentProtectedLabel->setEnabled(false);
    }
    else if (group->parentUser() &&
             group->parentUser()->group() &&
             group->parentUser()->group()->desktopEnvironmentProtected())
    {
      // héritage du paramétrage parent(s)
      ui->desktopEnvironmentProtected->setEnabled(false);
      ui->desktopEnvironmentProtectedLabel->setEnabled(false);
    }
    else
    {
      ui->desktopEnvironmentProtected->setEnabled(true);
      ui->desktopEnvironmentProtectedLabel->setEnabled(true);
    }
  }

// our list of inactive actions
  ui->inactiveList->header()->hide();
  QStringList list;
  list<<""<<"";// icon, text
  ui->inactiveList->setHeaderLabels(list);
  ui->inactiveList->sortByColumn ( 0, Qt::AscendingOrder );
  ui->inactiveList->hideColumn( 1 );
  ui->inactive_label->setBuddy(ui->inactiveList);
  connect(ui->inactiveList, SIGNAL(dropped(QTreeWidget*,QDropEvent*,QTreeWidgetItem*))       , this, SLOT(slotDropped(QTreeWidget*,QDropEvent*,QTreeWidgetItem*)));
  connect(ui->inactiveList, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(slotInactiveSelected(QTreeWidgetItem*,QTreeWidgetItem*)));
  connect(ui->inactiveList, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(slotInsertButton(QTreeWidgetItem*,int)));

  // our list of active actions
  ui->activeList->setAcceptDrops(false);
  ui->activeList->setAllColumnsShowFocus(true);
  ui->activeList->header()->hide();
  list.clear();
  list<<""<<"";// icon, text
  ui->activeList->setHeaderLabels(list);
  ui->activeList->hideColumn( 1 );
  ui->active_label->setBuddy(ui->activeList);
  connect(ui->activeList, SIGNAL(dropped(QTreeWidget*,QDropEvent*,QTreeWidgetItem*))     , this, SLOT(slotDropped(QTreeWidget*,QDropEvent*,QTreeWidgetItem*)));
  connect(ui->activeList, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*))  , this, SLOT(slotActiveSelected(QTreeWidgetItem*,QTreeWidgetItem*)));
  connect(ui->activeList, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(slotRemoveButton(QTreeWidgetItem*,int)));

  QStringList winList;
  QHashIterator<QString, CYUserActionCollection *> ic(group->actionCollections());
  while (ic.hasNext())
  {
    ic.next();
    CYUserActionCollection *collection = ic.value();
    CYWin *win = collection->window();
    QString text=QString::fromUtf8(win->label().toUtf8());
    text.prepend(QString("%1: ").arg(win->index()));
    mCollections.insert( text, collection );
    winList.append(text);
  }

  winList.sort();
  for ( QStringList::Iterator it = winList.begin(); it != winList.end(); ++it )
    ui->winBox->addItem(*it);

  ui->winBox->setCurrentIndex( 0 );
  setCollection( ui->winBox->currentText() );
  ui->activeList->sortByColumn ( 0, Qt::AscendingOrder );
  ui->inactiveList->sortByColumn ( 0, Qt::AscendingOrder );
}


CYUserGroupEdit::~CYUserGroupEdit()
{
}


/*! Saisie la collection d'action courante
    \fn CYUserGroupEdit::setCollection( const QString &key )
 */
void CYUserGroupEdit::setCollection( const QString &key )
{
  ui->activeList->clear();
  ui->inactiveList->clear();
  CYUserActionCollection *collection = mCollections.value(key);
  if ( collection )
  {
    QHashIterator<QString, CYUserAction *> ia(collection->actions());
    while (ia.hasNext())
    {
      ia.next();
      CYUserAction *ua= ia.value();
      if ( !ua->action()->inherits("CYActionMenu") && ua->actionEdit() )
      {
        CYFlag * flag = ua->used();
        flag->setTmp( flag->val() );

        QTreeWidgetItem *item;
        QStringList list;
        list<<ua->plainText()<<ua->objectName();
        if ( ua->isUsed() )
          item = new QTreeWidgetItem( ui->activeList, list);
        else
          item = new QTreeWidgetItem( ui->inactiveList, list);

        if (ua->action()->hasIcon())
        {
          item->setIcon( 0, ua->action()->icon() );
        }
      }
    }
  }
}


/*! Applique les données et enregistre le fichier du groupe.
    \fn CYUserGroupEdit::apply()
 */
void CYUserGroupEdit::apply()
{
  CYDialog::apply();

  QHashIterator<QString, CYUserActionCollection *> ic( mGroup->actionCollections());
  while (ic.hasNext())
  {
    ic.next();
    CYUserActionCollection *collection = ic.value();
    if ( collection )
    {
      QHashIterator<QString, CYUserAction *> ia(collection->actions());
      while (ia.hasNext())
      {
        ia.next();
        CYUserAction *action = ia.value();
        CYFlag * flag = action->used();
        flag->setVal( flag->tmp() );
      }
    }
  }
  if (!core->pyramidalUserAdmin())
  {
    if ( core->user()->group() == mGroup )
      mGroup->updateGUI();
  }
}


/*! Mise à jour des données.
    \fn CYUserGroupEdit::update()
 */
void CYUserGroupEdit::update()
{
  CYDialog::update();
}


/*! Change la valeur temporaire du flag d'utilisation ou non d'une action par le groupe d'utilisateur.
    \fn CYUserGroupEdit::change( QListViewItem *item, bool used )
 */
void CYUserGroupEdit::change( QTreeWidgetItem *item, bool used )
{
  QString text=ui->winBox->currentText().toUtf8();
  CYUserActionCollection *collection = mGroup->windowCollection(text);
  if ( collection )
  {
    CYUserAction * action = collection->actions().value( item->text(1) );
    if ( used != action->isUsed() )
    {
      action->used()->setTmp( used );
      emit modified( true );
    }
  }
  ui->activeList->sortByColumn ( 0, Qt::AscendingOrder );
  ui->inactiveList->sortByColumn ( 0, Qt::AscendingOrder );
}


/*!
    \fn CYUserGroupEdit::slotInactiveSelected(QTreeWidgetItem *item )
 */
void CYUserGroupEdit::slotInactiveSelected(QTreeWidgetItem *item, QTreeWidgetItem *previous)
{
  Q_UNUSED(previous)

  if (item)
  {
    ui->insertAction->setEnabled(true);
  }
  else
  {
    ui->insertAction->setEnabled(false);
  }
}


/*!
    \fn CYUserGroupEdit::slotActiveSelected(QTreeWidgetItem *item, QTreeWidgetItem *previous)
 */
void CYUserGroupEdit::slotActiveSelected(QTreeWidgetItem *item, QTreeWidgetItem *previous)
{
  Q_UNUSED(previous)

  if (item)
  {
    ui->removeAction->setEnabled(true);
  }
  else
  {
    ui->removeAction->setEnabled(false);
  }
}


/*! Ajoute une action à la liste d'actions possibles pour les utilisateurs du groupe.
    \fn CYUserGroupEdit::insertActive(QListViewItem *item )
 */
void CYUserGroupEdit::insertActive(QTreeWidgetItem *item)
{
  if (!item)
    return;

  QTreeWidgetItem *newItem = item->clone();
  ui->activeList->addTopLevelItem( newItem );
  int index = ui->inactiveList->indexOfTopLevelItem(item);
  ui->inactiveList->takeTopLevelItem(index);
  change( item, true );
}


/*! Supprime une action à la liste d'actions possibles pour les utilisateurs du groupe.
    \fn CYUserGroupEdit::removeActive( QListViewItem *item )
 */
void CYUserGroupEdit::removeActive( QTreeWidgetItem *item )
{
  if (!item)
    return;

  QTreeWidgetItem *newItem = item->clone();
  ui->inactiveList->addTopLevelItem( newItem );
  int index = ui->activeList->indexOfTopLevelItem(item);
  ui->activeList->takeTopLevelItem(index);
  change( item, false );
}


/*!
    \fn CYUserGroupEdit::slotInsertButton(QTreeWidgetItem * item, int column)
 */
void CYUserGroupEdit::slotInsertButton(QTreeWidgetItem * item, int column)
{
  Q_UNUSED(column)

  insertActive( item );
}


/*!
    \fn CYUserGroupEdit::slotRemoveButton(QTreeWidgetItem * item, int column)
 */
void CYUserGroupEdit::slotRemoveButton(QTreeWidgetItem * item, int column)
{
  Q_UNUSED(column)

  removeActive( item );
}


/*!
    \fn CYUserGroupEdit::slotInsertButton()
 */
void CYUserGroupEdit::slotInsertButton()
{
  insertActive( ui->inactiveList->currentItem() );
}


/*!
    \fn CYUserGroupEdit::slotRemoveButton()
 */
void CYUserGroupEdit::slotRemoveButton()
{
  removeActive( ui->activeList->currentItem() );
}


/*! Applique les modifications et ferme la boîte de dialogue.
    \fn CYUserGroupEdit::accept()
 */
void CYUserGroupEdit::accept()
{
  CYDialog::accept();
  mGroup->save();
}


/*! Annule les modifications et ferme la boîte de dialogue.
    \fn CYUserGroupEdit::reject()
 */
void CYUserGroupEdit::reject()
{
  CYDialog::reject();
  mGroup->load();

  QHashIterator<QString, CYUserActionCollection *> ic(mGroup->actionCollections());
  while (ic.hasNext())
  {
    ic.next();
    CYUserActionCollection *collection = ic.value();
    if ( collection )
    {
      QHashIterator<QString, CYUserAction *> ia(collection->actions());
      while (ia.hasNext())
      {
        ia.next();
        CYUserAction *action = ia.value();
        CYFlag * flag = action->used();
        flag->setTmp( flag->val() );
      }
    }
  }
  if (!core->pyramidalUserAdmin())
  {
    if ( core->user()->group() == mGroup )
      mGroup->updateGUI();
  }
}


/*! Affiche l'aide relative du manuel.
    \fn CYUserGroupEdit::help()
 */
void CYUserGroupEdit::help()
{
  core->invokeHelp( "cydoc", "user_group_edit" );
}
