//
// C++ Implementation: cyuseredit
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
// CYLIBS
#include "cyuseredit.h"
#include "ui_cyuseredit.h"
#include "cyuser.h"
#include "cyusergroup.h"
#include "cycombobox.h"
#include "cycore.h"

CYUserEdit::CYUserEdit(CYUser *user, QWidget *parent, const QString &name)
 : CYDialog( parent, name ),
   mUser( user )
{
  ui=new Ui::CYUserEdit;
  ui->setupUi(this);
  setLocaleDB( mUser->db() );

  if (core->pyramidalUserAdmin())
  {
    ui->groupBox->hide();
    ui->groupLabel->hide();
  }
  else
  {
    ui->groupBox->show();
    ui->groupLabel->show();
    QHashIterator<int, CYUserGroup *> it(core->userGroups());
    while (it.hasNext())
    {
      it.next();                   // must come first
      CYUserGroup *group = it.value();
      if ( !group->designer() && !group->simulation() && !group->isProtectedAccess() )
      {
        ui->groupBox->addItem( QString("%1: %2").arg(group->index()).arg(group->title()));
      }
    }
    for (int i=0; i<ui->groupBox->count(); i++)
    {
      if (ui->groupBox->itemText(i).startsWith(QString("%1:").arg(mUser->group()->index())))
      {
        ui->groupBox->setCurrentIndex( i );
        break;
      }
    }
  }
}


CYUserEdit::~CYUserEdit()
{
  delete ui;
}


/*!
    \fn CYUserEdit::apply()
 */
void CYUserEdit::apply()
{
  CYDialog::apply();
/*  CYUserGroup *group = core->userGroups().find(ui->groupBox->currentItem()+mOffset);*/
  if (!core->pyramidalUserAdmin())
  {
    int index = ui->groupBox->currentText().section(":", 0, 0).toInt();
    CYUserGroup *group = core->userGroups().value(index);

    if ( group )
      mUser->setGroup( group );
    else
      CYWARNINGTEXT( ui->groupBox->currentText() );

    if ( core->user() == mUser )
      mUser->updateGUI();
  }
}


/*! Applique les modifications et ferme la boîte de dialogue.
    \fn CYUserEdit::accept()
 */
void CYUserEdit::accept()
{
  CYDialog::accept();
  mUser->save();
}


/*! Annule les modifications et ferme la boîte de dialogue.
    \fn CYUserEdit::reject()
 */
void CYUserEdit::reject()
{
  CYDialog::reject();
  mUser->load();
  if (!core->pyramidalUserAdmin())
  {
    if ( core->user() == mUser )
      mUser->updateGUI();
  }
}


/*! Affiche l'aide relative du manuel.
    \fn CYUserEdit::help()
 */
void CYUserEdit::help()
{
  core->invokeHelp( "cydoc", "user_edit" );
}
