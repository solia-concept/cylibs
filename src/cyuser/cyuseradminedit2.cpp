//
// C++ Implementation: cyuseradminedit2
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

// QT
#include <QPushButton>
#include <QAbstractItemView>
// CYLIBS
#include "cyuseradminedit2.h"
#include "ui_cyuseradminedit2.h"
#include "cyuser.h"
#include "cyusergroup.h"
#include "cycore.h"
#include "cyuseredit.h"
#include "cyusergroupedit.h"
//#include "cyuserlistviewdnd.h"
#include "cymessagebox.h"
#include "cytreewidget.h"

CYUserAdminEdit2::CYUserAdminEdit2(QWidget *parent, const QString &name )
	: CYDialog(parent,name), ui(new Ui::CYUserAdminEdit2)
{
	ui->setupUi(this);

	if (core)
	{
		mManPixmap   = QPixmap(":/icons/cyman.png");
		mWomanPixmap = QPixmap(":/icons/cywoman.png");
		mGroupPixmap = QPixmap(":/icons/cyusergroup.png");
	}

  mNotOSUser = true;

#if defined(Q_OS_WIN)
  ui->addLinuxUserButton->hide();
#elif defined(Q_OS_LINUX)
  ui->addWindowsUserButton->hide();
#endif

//   ui->list->setSorting(-1);
  ui->upButton->hide();
  ui->downButton->hide();
  ui->leftButton->hide();
  ui->rightButton->hide();

  init();

  ui->list->hideColumn( COL_GROUP );
  ui->list->hideColumn( COL_USER );

  connect(ui->helpButton, SIGNAL(clicked()), this, SLOT(help()));
//  connect(ui->okButton, SIGNAL(clicked()), this, SLOT(accept()));
  connect(ui->addUserButton, SIGNAL(clicked()), this, SLOT(addUser()));
  connect(ui->addGroupButton, SIGNAL(clicked()), this, SLOT(addGroup()));
  connect(ui->editButton, SIGNAL(clicked()), this, SLOT(edit()));
  connect(ui->addLinuxUserButton, SIGNAL(clicked()), this, SLOT(addOSUser()));
  connect(ui->addWindowsUserButton, SIGNAL(clicked()), this, SLOT(addOSUser()));
  connect(ui->upButton, SIGNAL(clicked()), this, SLOT(itemUpClicked()));
  connect(ui->downButton, SIGNAL(clicked()), this, SLOT(itemDownClicked()));
  connect(ui->leftButton, SIGNAL(clicked()), this, SLOT(itemLeftClicked()));
  connect(ui->rightButton, SIGNAL(clicked()), this, SLOT(itemRightClicked()));
  connect(ui->okButton, SIGNAL(clicked()), this, SLOT(okClicked()));
  connect(ui->applyButton, SIGNAL(clicked()), this, SLOT(applyClicked()));
  connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(reject()));
  connect(ui->list, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(edit(QTreeWidgetItem*,int)));
  connect(ui->list, SIGNAL(modifie()), this, SLOT(setListModified()));
  connect(ui->removeButton, SIGNAL(clicked()), this, SLOT(remove()));
  connect(ui->list, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)));
}


CYUserAdminEdit2::~CYUserAdminEdit2()
{
  delete ui;
}


/*!
    \fn CYUserAdminEdit2::init()
 */
void CYUserAdminEdit2::init()
{
  updateList();
  setListModified(false);
}

/*! Edite l'utilisateur ou groupe sélectioné dans la liste.
    \fn CYUserAdminEdit2::edit( QListViewItem *item, int column )
 */
void CYUserAdminEdit2::edit( QTreeWidgetItem *item, int column )
{
  Q_UNUSED(item)
  Q_UNUSED(column)
}

/*! Edite l'utilisateur ou groupe sélectioné dans la liste.
    \fn CYUserAdminEdit2::edit( QListViewItem *item )
 */
void CYUserAdminEdit2::edit( QTreeWidgetItem *item )
{
  if ( item )
  {
    CYUser *u = user( item );
    CYUserGroup *g = userGroup( item );
    if ( u && (item->text(COL_USER).toInt()>0))
    {
      CYUserEdit *dlg = new CYUserEdit( u, this );
      dlg->exec();
      updateList();
    }
    else if ( g && (item->text(COL_GROUP).toInt()>0))
    {
      CYUserGroupEdit *dlg = new CYUserGroupEdit( g, this );
      dlg->exec();
      updateList();
    }
    else
      CYWARNINGTEXT( item->text(COL_NAME) );
  }
}


/*! Supprime l'utilisateur ou groupe sélectioné dans la liste.
    \fn CYUserAdminEdit2::remove( QListViewItem *item, bool force )
 */
bool CYUserAdminEdit2::remove( QTreeWidgetItem *item, bool force )
{
  if ( item )
  {
    CYUser *u = user( item );
    CYUserGroup *g = userGroup( item );
    if ( u )
    {
      if ( !force )
      {
        if (CYMessageBox::Yes !=
            CYMessageBox::warningYesNo(this, "<p align=""center"">"+tr("Are you sure to remove the user %1 ?").arg(u->title())+"</p>"))
          return false;
      }

      QTreeWidgetItemIterator it(item);//, QTreeWidgetItemIterator::HasChildren);
      while (*it)
      {
        QTreeWidgetItem *item2 =*it;

        // Supprime uniquement si c'est un enfant
        if (item2->parent()==item)
          if (!remove( item2, false))
            return false;
        ++it;
      }

      if ( core->removeUser( u ) )
      {
        QFile file( core->appDataDir()+QString("/users/user_%1.cyusr").arg( u->index() ) );
        file.remove();
      }
    }
    else if ( g )
    {
      if ( !force )
      {
        if (CYMessageBox::Yes !=
            CYMessageBox::warningYesNo(this, "<p align=""center"">"+tr("Are you sure to remove the users group %1 ?").arg(g->title())+"</p>"))
          return false;
      }

      QTreeWidgetItemIterator it(item);//, QTreeWidgetItemIterator::HasChildren);
      while (*it)
      {
        QTreeWidgetItem *item2 =*it;
        // Supprime uniquement si c'est un enfant
        if (item2->parent()==item)
          if (!remove( item2, false))
            return false;
        ++it;
      }

      if ( core->removeUserGroup( g ) )
      {
        QFile file( core->appDataDir()+QString("/users/group_%1.cyusr").arg( g->index() ) );
        file.remove();
      }
    }
  }

  return true;
}

/*! Mise à jour de la liste
    \fn CYUserAdminEdit2::updateList()
 */
void CYUserAdminEdit2::updateList()
{
  bool error = false;
  mNotOSUser = true;
  ui->list->clear();

  QHashIterator<int, CYUserGroup *> itg(core->userGroups());
  while (itg.hasNext())
  {
    itg.next();                   // must come first
    CYUserGroup *group = itg.value();

    if ( !group->designer() && !group->simulation() && !group->isProtectedAccess() && !group->administrator()
           && (overAccess() || !group->isParent(core->user())))
    {
      addGroupItemList(group);
    }
  }

  QHashIterator<int, CYUser *> itu(core->users());
  while (itu.hasNext())
  {
    itu.next();                   // must come first
    CYUser *user = itu.value();

    if ( (user->index()!=core->user()->index()) && !user->designer()
         && !user->simulation() && !user->isProtectedAccess()
         && (core->administrator()!=user) )
    {
      addUserItemList(user);
    }

    if ( user->isOSUser() )
      mNotOSUser = false;
  }
  ui->addLinuxUserButton->setEnabled( mNotOSUser );
  ui->addWindowsUserButton->setEnabled( mNotOSUser );

  if (error)
    updateList();

  // "ouvrir" l'arborescence au premier demarrage
  QTreeWidgetItemIterator it(ui->list);
  while ((*it))
  {
    QTreeWidgetItem *item = (*it);
    if (item)
      item->setExpanded(true);
    it++;
  }
}


/*! Ajoute un élément groupe utilisateurs dans la liste
    \fn CYUserAdminEdit2::addGroupItemList(CYUserGroup *group)
 */
QTreeWidgetItem *CYUserAdminEdit2::addGroupItemList(CYUserGroup *group)
{
  if (core->user()->isSameLevel(group))
    return 0;

  QTreeWidgetItem *lvi,*lvi0;

  lvi = ui->list->findItem(QString::number(group->index()), COL_GROUP );
  if (lvi)
    return lvi;

  CYUserGroup *parent_group = group->parentGroup();
  CYUser      *parent_user  = group->parentUser();

  QStringList stringList;
  stringList<<group->title()<<QString::number(group->index());
  if ( !parent_group && !parent_user ) // groupe à la racine
  {
    lvi = new QTreeWidgetItem( ui->list, stringList );
  }  
  else if ( parent_group && !parent_group->isParent(core->user()))
  {
    // récupère ou création s'il n'existe pas encore de l'élément du groupe parent
    if (!parent_group->administrator())
    {
      lvi0 = addGroupItemList(parent_group);
      if (lvi0)
        lvi = new QTreeWidgetItem( lvi0, stringList);
    }
    else
      lvi = new QTreeWidgetItem( ui->list, stringList);
  }
  else if ( parent_user && !parent_user->isParent(core->user()) && (parent_user->index()!=core->user()->index()))
  {
    // récupère ou création s'il n'existe pas encore de l'élément de l'utilisateur parent
    lvi0 = addUserItemList(parent_user);
    if (lvi0)
      lvi = new QTreeWidgetItem( lvi0, stringList);
  }
  else if (core->user()->isParent(group))
    lvi = new QTreeWidgetItem( ui->list, stringList);

  if (!lvi)
    return 0;
  lvi->setIcon(COL_NAME, QIcon(mGroupPixmap));
  lvi->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable|Qt::ItemIsDragEnabled|Qt::ItemIsDropEnabled|Qt::ItemIsUserCheckable);


  return lvi;
}

/*! Ajoute un élément utilisateur dans la liste
    \fn CYUserAdminEdit2::addUserItemList(CYUser *user)
 */
QTreeWidgetItem *CYUserAdminEdit2::addUserItemList(CYUser *user)
{
  if (user->isSameLevel(core->user()))
    return 0;

  QTreeWidgetItem *lvi,*lvi0;

  lvi = ui->list->findItem(QString::number(user->index()), COL_USER );
  if (lvi)
    return lvi;

  CYUserGroup *group        = user->group();
  CYUserGroup *parent_group = user->parentGroup();
  CYUser      *parent_user  = user->parentUser();

  QStringList stringList;
  stringList<<user->title()<<""<<QString::number(user->index());
  if ( !parent_group && !parent_user && !group) // groupe à la racine
  {
    lvi = new QTreeWidgetItem( ui->list, stringList );
  }
  else if ( parent_group && !parent_group->isParent(core->user()) && !parent_group->designer() && !parent_group->simulation() && !parent_group->isProtectedAccess() && !parent_group->administrator() )
  {
    if (!parent_group->isParent(core->user()))
    {
      // récupère ou création s'il n'existe pas encore de l'élément du groupe parent
      lvi0 = addGroupItemList(parent_group);
      if (lvi0)
        lvi = new QTreeWidgetItem( lvi0, stringList );
    }
    else
      lvi = new QTreeWidgetItem( ui->list, stringList );
  }
  else if ( parent_user && !parent_user->designer() && !parent_user->simulation() && !parent_user->isProtectedAccess() && (core->administrator()!=parent_user) )
  {
    if ((parent_user->index()!=core->user()->index()))
    {
      // récupère ou création s'il n'existe pas encore de l'élément de l'utilisateur parent
      lvi0 = addUserItemList(parent_user);
      if (lvi0)
        lvi = new QTreeWidgetItem( lvi0, stringList );
    }
    else
      lvi = new QTreeWidgetItem( ui->list, stringList );
  }
  else if ( group && !group->designer() && !group->simulation() && !group->isProtectedAccess() && !group->administrator() )
  {
    // compatibilté avec ancienne config
    lvi0 = addGroupItemList(group);
    if (lvi0)
      lvi = new QTreeWidgetItem( lvi0, stringList );
  }
  else if ( core->user()->designer() || core->user()->simulation() )
    lvi = new QTreeWidgetItem( ui->list, stringList );

  if (!lvi)
    return 0;

  lvi->setIcon(COL_NAME, QIcon(mManPixmap));
  lvi->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable|Qt::ItemIsDragEnabled|Qt::ItemIsDropEnabled|Qt::ItemIsUserCheckable);
  return lvi;
}

/*! Affiche l'aide relative du manuel.
    \fn CYUserAdminEdit2::help()
 */
void CYUserAdminEdit2::help()
{
  core->invokeHelp( "cydoc", "user" );
}


/*! Ajoute un utilisateur
    \fn CYUserAdminEdit2::addUser()
 */
void CYUserAdminEdit2::addUser()
{
  addUser( false );
}


/*! Ajoute un utilisateur Linux
    \fn CYUserAdminEdit2::addOSUser()
 */
void CYUserAdminEdit2::addOSUser()
{
  addUser( true );
}


/*! Ajoute un utilisateur Linux ou non selon \a linux_user.
    \fn CYUserAdminEdit2::addUser( bool linux_user )
 */
void CYUserAdminEdit2::addUser( bool linux_user )
{
  int id = core->users().count();
  while ( core->users().value(id) )
  {
    id++;
  }

  CYUser * u = new CYUser( core, id, "" );
  u->setOSUser( linux_user );

  QTreeWidgetItem *item = ui->list->currentItem();
  CYUser *u0 = user( item );
  CYUserGroup *g0 = userGroup( item );

  CYUserEdit *dlg = new CYUserEdit( u, this );
  if ( dlg->exec() )
  {
    if (u0)
      u->setParentUser( u0 );
    if (g0)
      u->setParentGroup( g0 );

    core->users().insert(id, u);
    updateList();
  }
}

/*! Suppression de l'utilisateur \a user.
    \fn CYUserAdminEdit2::removeUser(CYUser * user)
 */
void CYUserAdminEdit2::removeUser(CYUser * user)
{
  if ( core->removeUser( user ) )
  {
    if (CYMessageBox::Yes !=
        CYMessageBox::warningYesNo(this, "<p align=""center"">"+tr("Are you sure to remove the user %1 ?").arg(user->title())+"</p>"))
      return ;

    QFile file( core->appDataDir()+QString("/users/user_%1.cyusr").arg( user->index() ) );
    file.remove();
    updateList();
  }
}


/*! Edition de l'utilisateur ou le groupe sélectionné dans la liste.
    \fn CYUserAdminEdit2::editUser()
 */
void CYUserAdminEdit2::edit()
{
  edit( ui->list->currentItem() );
}

/*! Suppression de l'utilisateur ou le groupe sélectionné dans la liste.
    \fn CYUserAdminEdit2::remove()
 */
void CYUserAdminEdit2::remove()
{
  remove( ui->list->currentItem() );
  updateList();
}


/*! Ajoute un groupe d'utilisateurs
    \fn CYUserAdminEdit2::addGroup()
 */
void CYUserAdminEdit2::addGroup()
{
  int id = core->userGroups().count();
  while ( core->userGroups().value(id) )
    id++;

  CYUserGroup * g = new CYUserGroup( core, id, "" );

  QTreeWidgetItem *item = ui->list->currentItem();
  CYUser *u0 = user( item );
  CYUserGroup *g0 = userGroup( item );

  CYUserGroupEdit *dlg = new CYUserGroupEdit( g, this );
  if ( dlg->exec() )
  {
    if (u0)       // si un utilisateur sélectionné
      g->setParentUser( u0 );
    else if (g0)  // ou si un groupe sélectionné
      g->setParentGroup( g0 );
    else          // ou si pas de sélection alors utlisateur courant
      g->setParentUser( core->user() );

    core->userGroups().insert(id, g);

    updateList();
  }
}


/*! Suppression du groupe d'utilisateurs \a group.
    \fn CYUserAdminEdit2::removeGroup(CYUserGroup * group)
 */
void CYUserAdminEdit2::removeGroup(CYUserGroup * group)
{
  if ( group )
  {
    if (CYMessageBox::Yes !=
        CYMessageBox::warningYesNo(this, "<p align=""center"">"+tr("Are you sure to remove the users group %1 ?").arg(group->title())+"</p>"))
      return ;
    if ( core->removeUserGroup( group ) )
    {
      QFile file( core->appDataDir()+QString("/users/group_%1.cyusr").arg( group->index() ) );
      file.remove();
      updateList();
    }
  }
}


/*! Affecte les enfants de l'utilisateur ou groupe suivant son élement \a parent relatif dans la liste
    \fn CYUserAdminEdit2::updateChild(QListViewItem *parent)
 */
void CYUserAdminEdit2::updateChild(QTreeWidgetItem *parent)
{
  CYUser *u0 = user(parent);
  CYUserGroup *g0 = userGroup(parent);

  QTreeWidgetItemIterator it(parent);//, QTreeWidgetItemIterator::HasChildren);
  while (*it)
  {
    QTreeWidgetItem *item =*it;

    // Mise à jour uniquement si c'est un enfant
    if (item->parent()==parent)
    {
      CYUser *u = user(item);
      CYUserGroup *g = userGroup(item);

      if (u && u0)
      {
        u->setParentUser(u0);
      }
      else if (u && g0)
      {
        u->setParentGroup(g0);
      }
      else if (g && u0)
      {
        g->setParentUser(u0);
      }
      else if (g && g0)
      {
        g->setParentGroup(g0);
      }

      if (item->childCount())
      {
        updateChild(item);
      }
    }
    ++it;
  }
}


void CYUserAdminEdit2::applyClicked()
{
  if (core->isProtectedAccess())
    return;

  // affecte les enfants à chaque parent
  QTreeWidgetItemIterator it(ui->list);//, QTreeWidgetItemIterator::HasChildren);
  while (*it)
  {
    QTreeWidgetItem *item =*it;
    if (!item->parent()) // élément à la racine
    {
      if (!core->designer() && !core->simulation())
      {
        CYUser *u0 = core->user();

        CYUser *u = user(item);
        CYUserGroup *g = userGroup(item);

        if (u)
          u->setParentUser(u0);
        else if (g)
          g->setParentUser(u0);
      }
    }
    if (item->childCount())
      updateChild(item);
    ++it;
  }
  setListModified(false);
}

void CYUserAdminEdit2::okClicked()
{
  applyClicked();

  {
    QHashIterator<int, CYUserGroup *> it(core->userGroups());
    while (it.hasNext())
    {
      it.next();                   // must come first
      CYUserGroup *group = it.value();
      group->save();
    }
  }

  {
    QHashIterator<int, CYUser *> it(core->users());
    while (it.hasNext())
    {
      it.next();                   // must come first
      CYUser *user = it.value();
      user->save();
    }
  }

  accept();
}

void CYUserAdminEdit2::itemDeleteClicked()
{
  QTreeWidgetItem *i = ui->list->currentItem();
  if ( !i )
    return;

  delete i;

  QTreeWidgetItemIterator it(ui->list);
  if (*it)
  {
    ui->list->setCurrentItem( *it );
    (*it)->setSelected(true);
  }
}

void CYUserAdminEdit2::itemDownClicked()
{
  itemSwap(true);
  setListModified(true);
}

void CYUserAdminEdit2::itemNewClicked()
{
    QTreeWidgetItem *item = new QTreeWidgetItem( ui->list );
    item->setText( 0, "Item" );
    item->setFlags(Qt::ItemIsEnabled|Qt::ItemIsEditable|Qt::ItemIsSelectable|Qt::ItemIsDragEnabled|Qt::ItemIsDropEnabled|Qt::ItemIsUserCheckable);
    ui->list->setCurrentItem( item );
    item->setSelected( true );
/*    itemText->setFocus();
    itemText->selectAll();*/
}

void CYUserAdminEdit2::itemNewSubClicked()
{
  QTreeWidgetItem *parent = ui->list->currentItem();
  QTreeWidgetItem *item = 0;
  if ( parent )
  {
    item = new QTreeWidgetItem( parent );
  }
  else
  {
    item = new QTreeWidgetItem( ui->list );
  }
  item->setText( 0, "Subitem" );
  item->setFlags(Qt::ItemIsEnabled|Qt::ItemIsEditable|Qt::ItemIsSelectable|Qt::ItemIsDragEnabled|Qt::ItemIsDropEnabled|Qt::ItemIsUserCheckable);
  ui->list->setCurrentItem( item );
  item->setSelected( true );
}

void CYUserAdminEdit2::itemUpClicked()
{
  itemSwap(true);
  setListModified(true);
}

void CYUserAdminEdit2::itemSwap(bool up)
{
  QTreeWidgetItem* item = ui->list->currentItem();
  int              row  = ui->list->currentIndex().row();

  if (item && row > 0)
  {
    ui->list->takeTopLevelItem(row);
    if (up)
      ui->list->insertTopLevelItem(row + 1, item);
    else
      ui->list->insertTopLevelItem(row - 1, item);
    ui->list->setCurrentItem(item);
  }
}

void CYUserAdminEdit2::itemRightClicked()
{
  QTreeWidgetItem *i = ui->list->currentItem();
  if ( !i )
    return;

  QTreeWidgetItemIterator it( i );
  QTreeWidgetItem *parent = i->parent();
  parent = parent ? parent->child(0) : ui->list->topLevelItem(0);
  if ( !parent )
    return;
  QString txt = parent->text(0);
  it++;
  while ( *it ) {
    if ( (*it)->parent() == parent )
      break;
    it++;
  }

  if ( !(*it) )
    return;
  QTreeWidgetItem *other = (*it);

  for ( int c = 0; c < ui->list->columnCount(); ++c ) {
    QString s = i->text( c );
    i->setText( c, other->text( c ) );
    other->setText( c, s );
    QIcon icon;
    if ( !i->icon( c ).isNull() )
      icon = i->icon( c );
    if ( !other->icon( c ).isNull() )
      i->setIcon( c, other->icon( c ) );
    else
      i->setIcon( c, QIcon() );
    other->setIcon( c, icon );
  }

  ui->list->setCurrentItem( other );
  other->setSelected( true );
  setListModified(true);
}

void CYUserAdminEdit2::itemLeftClicked()
{
  QTreeWidgetItem *i = ui->list->currentItem();
  if ( !i )
    return;

  QTreeWidgetItemIterator it( i );
  QTreeWidgetItem *parent = i->parent();
  if ( !parent )
    return;

  parent = parent->parent();
  --it;
  while ( *it )
  {
    if ( (*it)->parent() == parent )
      break;
    --it;
  }

  if ( !(*it) )
    return;
  QTreeWidgetItem *other = (*it);

  for ( int c = 0; c < ui->list->columnCount(); ++c )
  {
    QString s = i->text( c );
    i->setText( c, other->text( c ) );
    other->setText( c, s );
    QIcon icon;
    if ( !i->icon( c ).isNull() )
      icon = i->icon( c );
    if ( !other->icon( c ).isNull() )
      i->setIcon( c, other->icon( c ) );
    else
      i->setIcon( c, QIcon() );
    other->setIcon( c, icon );
  }

  ui->list->setCurrentItem( other );
  other->setSelected( true );
  setListModified(true);
}

CYUser * CYUserAdminEdit2::user(QTreeWidgetItem *item)
{
  CYUser *user=nullptr;
  if ( item && !item->text(COL_USER).isEmpty())
    user = core->users().value( item->text(COL_USER).toInt() );
  return user;
}


/*! @return le groupe utilisateur s'il existe de l'élement de la liste \a item
    \fn CYUserAdminEdit2::userGroup(QListViewItem *item)
 */
CYUserGroup * CYUserAdminEdit2::userGroup(QTreeWidgetItem *item)
{
  CYUserGroup *group=nullptr;

  if ( item && !item->text(COL_GROUP).isEmpty())
    group = core->userGroups().value( item->text(COL_GROUP).toInt() );

  return group;
}


/*! @return \a true si l'utilisateur courant est l'administrateur, le constructeur ou la simulation
    \fn CYUserAdminEdit2::overAccess()
 */
bool CYUserAdminEdit2::overAccess()
{
  if (!core->user())
    return false;

  if (core->user()->administrator() || core->user()->designer() || core->user()->simulation())
    return true;

  return false;
}


void CYUserAdminEdit2::setListModified()
{
  setListModified(true);
}

  /*!
    \fn CYUserAdminEdit2::setListModified(bool val)
 */
void CYUserAdminEdit2::setListModified(bool val)
{
//  ui->addUserButton->setDisabled(val);
//  ui->addLinuxUserButton->setDisabled(val || (!val && !mNotOSUser));
//  ui->addWindowsUserButton->setDisabled(val || (!val && !mNotOSUser));
//  ui->addGroupButton->setDisabled(val);
//  ui->editButton->setDisabled(val);
//  ui->removeButton->setDisabled(val);
  ui->applyButton->setEnabled(val);
}


void CYUserAdminEdit2::currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
  Q_UNUSED(current)
  Q_UNUSED(previous)
  setListModified(true);
}
