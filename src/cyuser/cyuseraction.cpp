//
// C++ Implementation: cyuseraction
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

// CYLIBS
#include "cyuseraction.h"
#include "cyuseractioncollection.h"
#include "cyusergroup.h"
#include "cydb.h"
#include "cyflag.h"
#include "cycore.h"
#include "cywin.h"
#include "cyaction.h"

CYUserAction::CYUserAction( CYUserActionCollection *collection, CYAction *action )
 : QObject( collection ),
   mCollection( collection ),
   mAction( action )
{
  setObjectName(action->objectName());
  mDB = new CYDB( this );
  mDB->setDOMTagDatas( "CYUserAction" );
  mDB->setGroup( tr("User action") );

  mUsed = new CYFlag( mDB, "USED", new flg, true );
}


CYUserAction::~CYUserAction()
{
}


/*! @return la CYAction associée.
    \fn CYUserAction::action()
 */
CYAction * CYUserAction::action()
{
  return mAction;
}


/*! @return \a true si l'action est utilisée par le groupe d'utilisateur.
    \fn CYUserAction::isUsed()
 */
bool CYUserAction::isUsed(bool notify)
{
  bool res = mUsed->val();
  mAction->setUsed(res, notify);
  return mUsed->val();
}


/*! @return le texte de l'action sans les raccourcis clavier '&'
    \fn CYUserAction::plainText()
 */
QString CYUserAction::plainText()
{
  return mAction->text();
}


/*! @return le flag qui indique que l'action est utilisée ou non par le groupe d'utilisateur.
    \fn CYUserAction::used()
 */
CYFlag * CYUserAction::used()
{
  return mUsed;
}


/*! Enlève l'action de l'interface.
    \fn CYUserAction::unplugAll()
 */
void CYUserAction::unplugAll()
{
  setEnabled(false);
  mAction->setVisible(false);
}


/*! Ajoute un flag d'inhibition.
    \fn CYUserAction::addDisableFlag( CYFlag *flag )
 */
void CYUserAction::addDisableFlag( CYFlag *flag )
{
  mDisableFlags.append( flag );
}


/*! Ajoute un flag d'inhibition ayant pour nom \a flagName.
    @param flagName Commence par un "!" pour inverser le sens d'inhibition
    \fn CYUserAction::addDisableFlag( QByteArray flagName )
 */
void CYUserAction::addDisableFlag( QString flagName )
{
  bool inverse=false;
  if (flagName.startsWith("!"))
  {
    inverse=true;
    flagName=flagName.remove("!");
  }

  CYFlag * flag = (CYFlag * )core->findData( flagName );
  if ( !flag )
    CYFATALTEXT( QString("%1 -> %2").arg(objectName()).arg(flagName) );

  if (inverse)
  {
    mEnableFlags.append( flag );
    connect( flag, SIGNAL(valueRising(bool)), this, SLOT(setEnabled(bool) ) );
  }
  else
  {
    mDisableFlags.append( flag );
    connect( flag, SIGNAL(valueRising(bool)), this, SLOT(setDisabled(bool) ) );
  }
}


/*! Inhibe l'action si aucun des flags d'autorisation ne vaut \a false et qu'aucun autre flag d'inhibition ne vaut \a true ou si elle n'est pas accessible pour l'utilisateur.
    \fn CYUserAction::setDisabled( bool val )
 */
void CYUserAction::setDisabled( bool val )
{
  if ( val || core->isProtectedAccess() || !mAction->isVisible())
  {
    mAction->setEnabled( false );
  }
  else
  {
    mAction->setEnabled( true );
    QListIterator<CYFlag *> it1(mEnableFlags);
    while (it1.hasNext())
    {
      CYFlag *flag = it1.next();
      if ( !flag->val() )
      {
        mAction->setEnabled( false );
        return;
      }
    }
    QListIterator<CYFlag *> it2(mDisableFlags);
    while (it2.hasNext())
    {
      CYFlag *flag = it2.next();
      if ( flag->val() )
      {
        mAction->setEnabled( false );
        return;
      }
    }
  }
}


/*! Autorise l'action si aucun des flags d'inibition ne vaut \a true et qu'aucun autre flag d'authorisation ne vaut \a false ou si elle n'est pas accessible pour l'utilisateur.
    \fn CYUserAction::setEnabled( bool val )
 */
void CYUserAction::setEnabled( bool val )
{
  if ( !val || core->isProtectedAccess() || !mAction->isVisible())
  {
    mAction->setEnabled( false );
  }
  else
  {
    mAction->setEnabled( val );
    QListIterator<CYFlag *> it1(mEnableFlags);
    while (it1.hasNext())
    {
      CYFlag *flag = it1.next();
      if ( !flag->val() )
      {
        mAction->setEnabled( false );
        return;
      }
    }
    QListIterator<CYFlag *> it2(mDisableFlags);
    while (it2.hasNext())
    {
      CYFlag *flag = it2.next();
      if ( flag->val() )
      {
        mAction->setEnabled( false );
        return;
      }
    }
  }
}


/*! @return \a true si l'action est éditable
    \fn CYUserAction::actionEdit()
 */
bool CYUserAction::actionEdit()
{
  return mCollection->window()->actionEdit( objectName() );
}


/*! @return \a true si l'action est accesible uniquement en accès constructeur
    \fn CYUserAction::actionDesigner()
 */
bool CYUserAction::actionDesigner()
{
  return mCollection->window()->actionDesigner( objectName() );
}
