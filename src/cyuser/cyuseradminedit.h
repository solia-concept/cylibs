//
// C++ Interface: cyuseradminedit
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYUSERADMINEDIT_H
#define CYUSERADMINEDIT_H

// QT
#include <QTreeWidgetItem>
// CYLIBS
#include <cydialog.h>

namespace Ui {
		class CYUserAdminEdit;
}

/**
@short Boîte d'administration utilisateur.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYUserAdminEdit : public CYDialog
{
  Q_OBJECT
public:
    CYUserAdminEdit(QWidget *parent = 0, const QString &name = 0);

    ~CYUserAdminEdit();
    void init();

public slots:
    virtual void updateGroupList();
    virtual void updateUserList();

protected slots:
    virtual void editUser( QTreeWidgetItem *item );
    virtual void editUserGroup( QTreeWidgetItem *item );
    virtual void help();

    virtual void addUser();
    virtual void addOSUser();
    virtual void addUser( bool linux_user );
    virtual void removeUser();
    virtual void editUser();

    virtual void addGroup();
    virtual void removeGroup();
    virtual void editGroup();

private:
    Ui::CYUserAdminEdit *ui;
};

#endif
