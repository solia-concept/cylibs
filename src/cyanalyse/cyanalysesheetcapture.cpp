//
// C++ Implementation: cyanalysesheetcapture
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
// CYLIBS
#include "cyanalysesheetcapture.h"
#include "cyanalysecell.h"

CYAnalyseSheetCapture::CYAnalyseSheetCapture( QWidget* parent, const char* name, Qt::Orientation orientation )
  : CYAnalyseSheet( parent, name, orientation )
{
  mCaptureDateTime = QDateTime::currentDateTime();
  disconnect( mRefreshTimer, SIGNAL( timeout() ), this, SIGNAL( refreshing() ) );
}


CYAnalyseSheetCapture::~CYAnalyseSheetCapture()
{
}


QDateTime CYAnalyseSheetCapture::captureDateTime()
{
  return mCaptureDateTime;
}
