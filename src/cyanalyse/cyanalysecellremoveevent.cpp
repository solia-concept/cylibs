/***************************************************************************
                          cyanalysecellremoveevent.cpp  -  description
                             -------------------
    begin                : mer fév 2 2005
    copyright            : (C) 2005 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyanalysecellremoveevent.h"

// CYLIBS
#include "cyanalysecell.h"
#include "cy.h"

CYAnalyseCellRemoveEvent::CYAnalyseCellRemoveEvent(CYAnalyseCell *parent, CYAnalyseCell *child)
  : QEvent((QEvent::Type) RemoveAnalyseCell)
{
   mCellParent = parent;
   mCellChild  = child;
}

CYAnalyseCellRemoveEvent::~CYAnalyseCellRemoveEvent()
{
}

void CYAnalyseCellRemoveEvent::remove()
{
  mCellParent->removeChild( mCellChild );
}
