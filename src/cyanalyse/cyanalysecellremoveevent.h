/***************************************************************************
                          cyanalysecellremoveevent.h  -  description
                             -------------------
    begin                : mer fév 2 2005
    copyright            : (C) 2005 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYANALYSECELLREMOVEEVENT_H
#define CYANALYSECELLREMOVEEVENT_H

// QT
#include <qevent.h>
//Added by qt3to4:
#include <QEvent>

class CYAnalyseCell;

/** @short Evènement de suppression d'une cellule d'analyse.
  * @author LE CLÉACH Gérald
  */

class CYAnalyseCellRemoveEvent : public QEvent
{
public: 
  CYAnalyseCellRemoveEvent(CYAnalyseCell *parent, CYAnalyseCell *child);
  ~CYAnalyseCellRemoveEvent();
  
  /** Supprimer la cellule. */
  void remove();

protected: // Protectd attributes
  /** Cellule d'analyse contenant la cellule à supprimer. */
  CYAnalyseCell * mCellParent;
  /** Cellule d'analyse à supprimer. */
  CYAnalyseCell * mCellChild;
};

#endif
