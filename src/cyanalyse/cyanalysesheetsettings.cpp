#include "cyanalysesheetsettings.h"
#include "ui_cyanalysesheetsettings.h"

CYAnalyseSheetSettings::CYAnalyseSheetSettings(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYAnalyseSheetSettings)
{
  ui->setupUi(this);
}

CYAnalyseSheetSettings::~CYAnalyseSheetSettings()
{
  delete ui;
}

void CYAnalyseSheetSettings::init(QString t, bool newSheet, int rows, int cols)
{
  ui->title->setText( t );
  if ( newSheet )
  {
    ui->rows->setValue( rows );
    ui->columns->setValue( cols);
  }
  else
  {
    ui->rows->hide();
    ui->columns->hide();
    ui->rowsLabel->hide();
    ui->columnsLabel->hide();
  }
}

uint CYAnalyseSheetSettings::rows()
{
  return ui->rows->text().toUInt();
}

uint CYAnalyseSheetSettings::columns()
{
  return ui->columns->text().toUInt();
}

QString CYAnalyseSheetSettings::title()
{
  return ui->title->text();
}
