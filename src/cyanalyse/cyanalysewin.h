/***************************************************************************
                          cyanalysewin.h  -  description
                             -------------------
    début                  : mar fév 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYANALYSEWIN_H
#define CYANALYSEWIN_H

// QT
#include <QSplitter>
#include <QHash>
// CYLIBS
#include "cywin.h"
#include "cyanalysesheettemplate.h"

class CYAnalyseSpace;
class CYDatasBrowser;
class CYAction;
class CYActionMenu;

/**
 * Cette classe constitue la fenêtre d'analyses. Celle-ci possède comme la
 * fenêtre principale de menus, de barre d'outils et d'états. Sa vue centrale
 * est composé d'une vue en listes de toutes les variables disponibles
 * (CYDatasBrowser) et d'un espace d'analyses (CYAnalyseSpace) comparable à
 * un classeur. Il est possible d'y charger, d'y créer ou d'y sauvegarder des
 * feuilles d'analyses (CYAnalyseSheet) pouvant accueillir différents afficheurs.
 * Il suffit alors par un simple "glisser-déposer" d'associer à ces derniers les
 * variables à visualiser.
 * @short Fenêtre d'analyses.
 * @author Gérald LE CLEACH
 */

class CYAnalyseWin : public CYWin
{
  Q_OBJECT

public:
  /** Construction de la fenêtre d'analyses de CYLIX.
    * @param name     Nom de la fenêtre.
    * @param index    Index de la fenêtre.
    * @param label    Libellé de la fenêtre. */
  CYAnalyseWin(const QString &name, int index, const QString &label);
  ~CYAnalyseWin();

  virtual void createGUI(const QString &xmlfile=0);

  virtual void saveProperties(QSettings *);
  virtual void readProperties(QSettings *);

  void loadAnalyseSheet(const QString &fileName);
  void removeAnalyseSheet(const QString &fileName);

  virtual void customEvent(QEvent *e);
  virtual void timerEvent(QTimerEvent *);
  virtual bool queryClose();

  /** Ajoute un modèle de feuille d'analyse .*/
  CYAnalyseSheetTemplate *addTemplate(QString title, QString file);

public slots:
  void editStyle();
  /** Sauvegarde toutes les feuille ouvertes ainsi que les propriétés de la fenêtre. */
  bool saveAll(bool quit=false);
  /** Appelée à chaque modification ou enregistrement de feuille. */
  void setModified(bool val);
  /** Autorise ou non le rafraîchissement des objets d'analyse graphique:
    * oscilloscope, hystogramme, multimètre...
    * Si le rafraîchissement était autorisé alors il ne l'est plus ou inversement. */
  void enableAnalyseRefresh();

  virtual void showStatusBar();

  /** Active ou désactive les connexions de modifications. */
  void connectModifie(bool enable);
    void capture();

private:
  QSplitter *splitter;
  CYDatasBrowser *mBrowser;
  CYAnalyseSpace *as;

protected: // Protected attributes
  /** Action enregistrer. */
  CYAction *mSaveAction;
  /** Action enregistrer sous. */
  CYAction *mSaveAsAction;
  /** Sous-menu modèle. */
  CYActionMenu *mTemplateAction;
  /** Action mise en pause. */
  CYAction *mPauseAction;
  QHash<int, CYAnalyseSheetTemplate*> mTemplates;
};

#endif
