/***************************************************************************
                          cyanalysesheet.cpp  -  description
                             -------------------
    début                  : mar fév 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyanalysesheet.h"

// QT
#include <QFile>
#include <QClipboard>
#include <QCursor>
#include <QApplication>
#include <QSpinBox>
#include <QTextStream>
#include <QEvent>
#include <QString>
// CYLIBS
#include "cycore.h"
#include "cydb.h"
#include "cynethost.h"
#include "cytime.h"
#include "cynuminput.h"
#include "cydisplay.h"
#include "cydisplaydummy.h"
#include "cymultimeter.h"
#include "cyscope.h"
#include "cybargraph.h"
#include "cydatastable.h"
//#include "cytreewidget.h"
//#include "cylogfile.h"
//#include "cyprocesscontroller.h"
//#include "cysensorlogger.h"
#include "cyanalysesheetsettings.h"
#include "cyanalysecell.h"
#include "cyanalysecellremoveevent.h"
#include "cymessagebox.h"
#include "cyglobal.h"

CYAnalyseSheet::CYAnalyseSheet( QWidget *parent, const QString &name, Qt::Orientation orientation )
    : QFrame( parent ),
    mRows( 1 ),
    mColumns( 1 ),
    mOrientation( orientation )
{
  setObjectName(name);
  init();
}

CYAnalyseSheet::CYAnalyseSheet( QWidget *parent, const QString &name, uint rows, uint columns, uint i, Qt::Orientation orientation )
    : QFrame( parent ),
    mRows( rows ),
    mColumns( columns ),
    mOrientation( orientation )
{
  setObjectName(name);
  init();
  mRefreshTime->setVal( i );
  mRefreshTimer->start( mRefreshTime->val() );
}

CYAnalyseSheet::~CYAnalyseSheet()
{
}

void CYAnalyseSheet::init()
{
  mGrid = 0;
  mCell = 0;
  mModified = false;
  mForcing  = false;
  mProteted = false;
  mVersionCYDOM = QString(core->version()).section( '.', 0, 1 );

  setAcceptDrops( true );

  if ( !core )
    mDB = new CYDB( new CYNetHost( this, QString( "Host_%1" ).arg( objectName() ) ), QString( "DB_%1" ).arg( objectName() ), -1, 0, false );
  else
    mDB = new CYDB( core->localHost(), QString( "DB_%1" ).arg( objectName() ), -1, 0, false );

  mDB->setGroup( tr( "Analyse sheet" ) );

  mRefreshTime = new CYTime(mDB, "REFRESH_TIME", new u32, "0h0m0s200ms", "0h0m0s40ms", "1h0m0s0ms");
//   mRefreshTime = new CYTime( mDB, "REFRESH_TIME", new u32, "0s200ms", "0s40ms", "3600s0ms");
  mRefreshTime->setLabel( tr( "Refresh time" ) );
  mRefreshTimer = new QTimer();
  connect( mRefreshTimer, SIGNAL( timeout() ), this, SIGNAL( refreshing() ) );
  mRefreshTimer->start( mRefreshTime->val() );
}

bool CYAnalyseSheet::load()
{
  return load( mFileName );
}

bool CYAnalyseSheet::load( const QString &fN )
{
  if ( !core )
    return false;

  QFile file( mFileName = fN );
  if ( !file.open( QIODevice::ReadOnly ) )
  {
    CYWARNINGTEXT( tr( "Can't open the file %1" ).arg( mFileName ) );
    return ( false );
  }

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  QString errorMsg;
  int errorLine;
  int errorColumn;
  if ( !doc.setContent( &file, &errorMsg, &errorLine, &errorColumn ) )
  {
    CYMessageBox::sorry( this, tr( "The file %1 does not contain valid XML\n"
                                    "%2: line:%3 colomn:%4" ).arg( mFileName ).arg( errorMsg ).arg( errorLine ).arg( errorColumn ) );
    return ( false );
  }
  // Vérifie le type propre du document.
  if ( doc.doctype().name() != "CYLIXAnalyseSheet" )
  {
    CYMessageBox::sorry( this, QString( tr( "The file %1 does not contain a valid"
                                       "definition, which must have a document type " ).arg( mFileName )
                                       + "'CYLIXAnalyseSheet'" ) );
    return ( false );
  }
  // Vérifie la taille propre.
  QDomElement el = doc.documentElement();

  // Version de la librairie lors de la sauvegarde du document.
  mVersionCYDOM = el.attribute( "CYDOM" );

  setTitle( el.attribute( "title" ) );

  bool timerOk;
  mRefreshTime->setVal( el.attribute( "refreshTime" ).toUInt( &timerOk ) );
  mRefreshTimer->start( mRefreshTime->val() );
  if ( !timerOk )
  {
    CYMessageBox::sorry( this, tr( "The file %1 has an invalid refresh time." ).arg( mFileName ) );
    return ( false );
  }

  int i;

  /* Charge les listes d'hôtes qui sont nécessaires pour la feuille de
   * données et essaye d'établir une connexion. */
  QDomNodeList dnList = el.elementsByTagName( "host" );
  for ( i = 0; i < dnList.count(); ++i )
  {
    QDomElement el = dnList.item( i ).toElement();
    core->engage( el.attribute( "name" ) );
  }

  QDomNodeList cellList = el.elementsByTagName( "cell0" );

  if ( cellList.count() )// Charge la cellue principale d'un nouveau fichier ainsi que toutes celles qui en découlent
  {
    createGrid( 1, 1, Qt::Horizontal );
    for ( int num = 0; num < cellList.count(); num++ )
    {
      QDomElement el = cellList.item( num ).toElement();
      mCell->createFromDOM( this, this, el, &mErrorList );
    }
    mCell->show();
  }
  else // Charge les afficheurs d'un ancien fichier et les place à l'intérieur de la feuille d'analyse.
  {
    bool rowsOk;
    uint r = el.attribute( "rows" ).toUInt( &rowsOk );
    bool columnsOk;
    uint c = el.attribute( "columns" ).toUInt( &columnsOk );
    if ( !( rowsOk && columnsOk ) )
    {
      CYMessageBox::sorry( this, tr( "The file %1 has an invalid work sheet size." ).arg( mFileName ) );
      return ( false );
    }
    createGrid( r, c, Qt::Horizontal );
    QDomNodeList displayList = el.elementsByTagName( "display" );
    for ( i = 0; i < displayList.count(); ++i )
    {
      QDomElement el = displayList.item( i ).toElement();
      uint row = el.attribute( "row" ).toUInt();
      uint column = el.attribute( "column" ).toUInt();
      if ( row >= mRows || column >= mColumns )
      {
        CYWARNINGTEXT(tr("Row or Column out of range (%1, %2)").arg(row).arg(column));
        return ( false );
      }
      CYAnalyseCell *cell = mGridCells.value( QString( "Cell_%1_%2" ).arg( row ).arg( column ) );
      if ( cell )
        cell->createDisplayFromDOM( el );
    }
  }

  mGrid->activate();

  mModified = false;

  return ( true );
}

bool CYAnalyseSheet::save()
{
  return save( mFileName );
}

bool CYAnalyseSheet::save( const QString &fN )
{
  if ( !core )
    return false;

  QDomDocument doc( "CYLIXAnalyseSheet" );
  doc.appendChild( doc.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"UTF-8\"" ) );

  // enregistre les informations la feuille de données.
  QDomElement ws = doc.createElement( "CYAnalyseSheet" );
  doc.appendChild( ws );

  ws.setAttribute( "CYDOM", core->version() );
  mVersionCYDOM = core->version();

  ws.setAttribute( "title", mTitle );
  ws.setAttribute( "rows", mRows );
  ws.setAttribute( "columns", mColumns );
  ws.setAttribute( "refreshTime", mRefreshTime->val() );

  QStringList hosts;
  collectHosts( hosts );

  // enregistre les informations sur l'hôte (name, shell, etc.)
  QStringList::Iterator it;
  for ( it = hosts.begin(); it != hosts.end(); ++it )
  {
    QString shell, command;
    int port;

    if ( core->getHostInfo( *it, shell, command, port ) )
    {
      QDomElement host = doc.createElement( "host" );
      ws.appendChild( host );
      host.setAttribute( "name", *it );
    }
  }

  // Enregistrement de la cellue principale ainsi que toutes celles qui en découlent
  mCell->addToDOM( doc, ws );

  QFile file( mFileName = fN );
  if ( !file.open( QIODevice::WriteOnly ) )
  {
    CYWARNINGTEXT(tr("Can't save file %1!").arg(mFileName));
    return ( false );
  }
  QTextStream s( &file );
  s.setCodec( "UTF-8" );
  s << doc;
  file.close();
  core->backupFile( mFileName );

  setModified( false );
  return ( true );
}

void CYAnalyseSheet::cut()
{
  CYAnalyseCell *cell = mCell->cellHasFocus();
  if ( cell )
    cell->cutDisplay();
}

void CYAnalyseSheet::copy()
{
  CYAnalyseCell *cell = mCell->cellHasFocus();
  if ( cell )
    cell->copyDisplay();
}

void CYAnalyseSheet::paste()
{
  CYAnalyseCell *cell = mCell->cellHasFocus();
  if ( cell )
    cell->pasteDisplay();
}

void CYAnalyseSheet::setTitle( const QString &title )
{
  mTitle = title;
}

bool CYAnalyseSheet::settings( bool init )
{
  CYAnalyseSheetSettings* wss = new CYAnalyseSheetSettings( this, "CYAnalyseSheetSettings" );
  Q_CHECK_PTR( wss );

  wss->setLocaleDB( mDB );

  /* Comme le nom de la feuille doit pouvoir être changé par la fonction
   * "Enregistrer sous..." nous n'avons pas à afficher le titre de la structure. */
//  wss->titleFrame->hide();
//  wss->resize( wss->sizeHint() );
  wss->init(mTitle, init, mRows, mColumns );
  if ( wss->exec() )
  {
    if ( init )
      createGrid( wss->rows(), wss->columns(), Qt::Horizontal );
    setTitle( wss->title() );
    mRefreshTimer->stop();
    disconnect( mRefreshTimer, SIGNAL( timeout() ), this, SIGNAL( refreshing() ) );
    connect( mRefreshTimer, SIGNAL( timeout() ), this, SIGNAL( refreshing() ) );
    mRefreshTimer->start( mRefreshTime->val() );

    setModified( true );

    delete wss;
    return true;
  }
  else
  {
    delete wss;
    return false;
  }
}

void CYAnalyseSheet::showPopupMenu( CYDisplay* display )
{
  display->settings();
}

void CYAnalyseSheet::setModified( bool mfd )
{
  mModified = mfd;
  emit sheetModified( this );
}

void CYAnalyseSheet::collectHosts( QStringList& list )
{
  Q_UNUSED(list);
//  for (uint r = 0; r < mRows; ++r)
//    for (uint c = 0; c < mColumns; ++c)
//      if (!displays[r][c]->isA("CYDisplayDummy"))
//        ((CYDisplay*) displays[r][c])->collectHosts(list);
}

void CYAnalyseSheet::createGrid( uint row, uint col, Qt::Orientation orientation )
{
  mRows    = row;
  mColumns = col;

  if ( mGrid )
    delete mGrid;

  // crée une grille de position avec des dimensions spécifiques
  mGrid = new QGridLayout( this );
  Q_CHECK_PTR( mGrid );
  mGrid->setContentsMargins(0,0,0,0);
  mGrid->setMargin(0);

  if ( mCell )
    delete mCell;

  if (( mRows == 1 ) && ( mColumns == 1 ) )
  {
    // Une seule cellule contenant un afficheur
    mCell = new CYAnalyseCell( this, this, "Cell0_0", CYAnalyseCell::Display );
  }
  else if ( orientation == Qt::Horizontal )
  {
    // Feuille divisée horizontalement
    mCell = new CYAnalyseCell( this, this, "Cell0_0", CYAnalyseCell::Horizontal );
    CYAnalyseCell *cell;
    for ( uint c = 0; c < mColumns; c++ )
    {
      if ( mRows == 1 )
      {
        // Une seule ligne
        cell = mCell->newCell( CYAnalyseCell::Display, mCell->level() + 1 );
      }
      else
      {
        cell = mCell->newCell( CYAnalyseCell::Vertical, mCell->level() + 1 );
        for ( uint r = 0; r < mRows; r++ )
          mGridCells.insert(QString( "Cell_%1_%2" ).arg( r ).arg( c ), cell->newCell( CYAnalyseCell::Display, cell->level() + 1 ) );
      }
    }
  }
  else if ( orientation == Qt::Vertical )
  {
    // Feuille divisée verticalement
    mCell = new CYAnalyseCell( this, this, "Cell0_0", CYAnalyseCell::Vertical, mCell->level() + 1 );
    CYAnalyseCell *cell;
    for ( uint r = 0; r < mRows; r++ )
    {
      if ( mColumns == 1 )
      {
        // Une seule colonne
        cell = mCell->newCell( CYAnalyseCell::Display, mCell->level() + 1 );
      }
      else
      {
        cell = mCell->newCell( CYAnalyseCell::Horizontal, mCell->level() + 1 );
        for ( uint c = 0; c < mColumns; c++ )
          mGridCells.insert( QString( "Cell_%1_%2" ).arg( r ).arg( c ), cell->newCell( CYAnalyseCell::Display, cell->level() + 1 ) );
      }
    }
  }

  mGrid->addWidget( mCell, 0, 0 );
  mGrid->activate();
}

void CYAnalyseSheet::fixTabOrder()
{
//  for (uint r = 0; r < mRows; ++r)
//    for (uint c = 0; c < mColumns; ++c)
//    {
//      if (c + 1 < mColumns)
//        setTabOrder(displays[r][c], displays[r][c + 1]);
//      else if (r + 1 < mRows)
//        setTabOrder(displays[r][c], displays[r + 1][0]);
//    }
}

void CYAnalyseSheet::setIsOnTop( bool )
{
//  for (uint i = 0; i < mRows; ++i)
//    for (uint j = 0; j < mColumns; ++j)
//      displays[i][j]->setIsOnTop(onTop);
}

CYTime *CYAnalyseSheet::refreshTime()
{
  return mRefreshTime;
}

void CYAnalyseSheet::connectModifie( bool enable )
{
  if ( mCell )
    mCell->connectModifie( enable );
}

void CYAnalyseSheet::setFileName(const QString fN)
{
  mFileName = findResource(fN);
  load(mFileName);
  if (!mErrorList.isEmpty())
      CYWARNINGLIST(mErrorList, mFileName);
}

bool CYAnalyseSheet::hasBeenModified()
{
  return mModified;
}

bool CYAnalyseSheet::forcing() const
{
  return mForcing;
}

void CYAnalyseSheet::setForcing( const bool val )
{
  mForcing = val;
  if ( mCell )
  {
    mCell->setForcing( val );
  }
}

int CYAnalyseSheet::showPopupMenu( QMenu *pm0, int index )
{
  QMenu *pm;
  if ( pm0 )
  {
    if ( index == 0 )                                 // Création d'un sous-menu surgissant
    {
      pm = new QMenu( pm0 );
      pm = pm0->addMenu( tr( "Analyse sheet" ) ); index++;
    }
    else                                              // Ajout d'entrées dans un menu surgissant
      pm = pm0;
  }
  else                                                // Création d'un menu surgissant
  {
    pm = new QMenu( this );
    pm->addMenu( tr( "Analyse sheet" ) ); index++;
    pm->addSeparator();
  }

  if (core)
  {
      pm->addAction( core->loadIconSet( "cyconfigure.png", 22 ), tr( "&Properties" ), this, SLOT( settings() ), 0); index++;
  }

  if ( !pm0 )
    pm->exec( QCursor::pos() );

  return index;
}

void CYAnalyseSheet::customEvent( QEvent* ev )
{
  if ( ev->type() == RemoveAnalyseCell )
  {
    CYAnalyseCellRemoveEvent *e = ( CYAnalyseCellRemoveEvent * )ev;
    e->remove();
  }
}


/*! @return la cellule correspondant à \a position ou 0 si elle n'existe pas.
    @param position Permet de retrouver la bonne cellule.
                    Le position est constituée de divisions horizontales et verticales. Chacune d'elles part d'une cellule qui est soit la cellule initiale (Cell0) soit une des 2 cellules résultant de la division d'une autre cellule. Dans ce cas il faut indiquer laquelle de celles-ci nous intéresse.
                    Nous avons alors une chaîne de caractères formées de sections correspondant à des divisions de cellule avec la cellule fille de départ dans chaque section.
                      - Par exemple pour atteindre la cellule à la 1ère colonne de la 2ème ligne de la grille nous avons:
                        "2/1" (Cellule fille n°2 de la première division (Horizontale) puis cellule fille n°1 (Verticale) de la deuxième division)
                      - Par exemple pour atteindre la cellule à la 1ère ligne de la 2ème colonne de la grille nous avons:
                        "2/1" (Cellule fille n°2 de la première division (Verticale) puis cellule fille n°1 (Horizontale) de la deuxième division)
                    Lorsque la feuille n'est pas divisée il y a donc qu'une seule cellule et la position est donc "0".
    \fn CYAnalyseSheet::findCell(QString position)
 */
CYAnalyseCell * CYAnalyseSheet::findCell( QString position )
{
  return mCell->findCell( position );
}

void CYAnalyseSheet::setDisplayTitle( QString position, QString title )
{
  CYAnalyseCell *cell = findCell( position );
  if ( cell )
  {
    if ( cell->type() == CYAnalyseCell::Display )
    {
      CYDisplay *display = cell->display();
      if ( display )
      {
        display->setBoxTitle( title );
      }
    }
    else
      CYWARNINGTEXT( QString( "%1: position %2 => %3 ne correspond pas à un type Display , vérifier la position" ).arg( fileName() ).arg( position ).arg( title ) );
  }
}


/*! Une feuille d'analyse protégée inhibe certaines actions telles que l'ajout ou
    la suppression de cellule ainsi que la supression de CYDisplay qu'elle contient.
    Cette protection est utile pour les feuilles d'analyse système qui sont insérées
    lors du développement dans l'interface utilisateur et installées sur le système.
    Cette protection permet à l'utilisateur de modifier la configuration et de la
    sauvegarder dans un autre fichier tout en gardant l'indispensable au bon
    fonctionnement de l'application.
    Cette protection peut être également activer ou désactiver par @see setProtected(bool)
    \fn CYAnalyseSheet::isProtected() const
 */
bool CYAnalyseSheet::isProtected() const
{
  return mProteted;
}


/*! Active/désactive la protection d'une feuille d'analyse système.
    @see isProtected()
    \fn CYAnalyseSheet::setProtected(const bool val)
 */
void CYAnalyseSheet::setProtected( const bool val )
{
  mProteted = val;
}


/*! Initialise les textes à traduire de l'afficheur à la position \a position de la feuille.
    Ceci est utile par exemple pour les feuilles d'analyses système.
    @param pos    @see CYAnalyseSheet::findCell(QString position)
    @param title  Titre de l'afficheur.
    \fn CYAnalyseSheet::initTRDisplay(QString pos, QString title)
 */
void CYAnalyseSheet::initTRDisplay( QString pos, QString title )
{
  CYAnalyseCell *cell = findCell( pos );
  if ( cell && cell->display() )
    cell->display()->initTRDisplay( title );
}


/*! @return la cellule d'analyse initiale
    \fn CYAnalyseSheet::cell0()
 */
CYAnalyseCell * CYAnalyseSheet::cell0()
{
  return mCell;
}


void CYAnalyseSheet::capture()
{
//   CYAnalyseSheet *sheetCYAnalyseSheet(
}

void CYAnalyseSheet::showEvent(QShowEvent *e)
{
  QFrame::showEvent(e);
}
