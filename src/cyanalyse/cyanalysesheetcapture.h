//
// C++ Interface: cyanalysesheetcapture
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYANALYSESHEETCAPTURE_H
#define CYANALYSESHEETCAPTURE_H

// QT
#include <qdatetime.h>
// CYLIBS
#include <cyanalysesheet.h>

/**
@short Capture d'une feuille d'analyse

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYAnalyseSheetCapture : public CYAnalyseSheet
{
  Q_OBJECT
public:
  CYAnalyseSheetCapture( QWidget* parent, const char* name=0, Qt::Orientation orientation=Qt::Horizontal);

  ~CYAnalyseSheetCapture();

  QDateTime captureDateTime();

protected:
  QDateTime mCaptureDateTime;
};

#endif
