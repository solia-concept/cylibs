/***************************************************************************
                          cyanalysespace.cpp  -  description
                             -------------------
    début                  : mar fév 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyanalysespace.h"

// ANSI
#include <stdlib.h>
// QT
#include <QLineEdit>
#include <QSpinBox>
#include <QFileInfo>
#include <QFileDialog>
#include <QShowEvent>
#include <QToolTip>
// CYLIBS
#include "cyanalysewin.h"
#include "cyanalysesheet.h"
#include "cyanalysesheetsettings.h"
#include "cyanalysesheetcapture.h"
#include "cycore.h"
#include "cymessagebox.h"

CYAnalyseSpace::CYAnalyseSpace( CYAnalyseWin *win, QWidget *parent, const QString &name )
    : QTabWidget( parent ),
    mWin( win )
{
  setObjectName(name);
  workDir = tr("%1/Graphic_sheet").arg(core->baseDirectory());
  setFocusPolicy( Qt::StrongFocus );
  autoSave = false;

  connect( this, SIGNAL( currentChanged( int ) ), this, SLOT( updateCaption( int ) ) );

  setToolTip( "<p>"+
              tr( "This is your analyse space. It holds your analyse sheets. You need "
                  "to create a new analyse sheet (Menu File->New) before "
                  "you can drag datas here.")
              +"</p>" );
}

CYAnalyseSpace::~CYAnalyseSpace()
{
  /* Cet inventaire est nécessaire pour éviter un plantage lorsque la
   * dernière page n'est pas la page courante. Même si la gestion de données
   * par signal/slot semble supprimée des slots peuvent toujours être actifs. */
  disconnect( this, SIGNAL( currentChanged( int ) ), this, SLOT( updateCaption( int ) ) );
}

void CYAnalyseSpace::saveProperties( QSettings *cfg )
{
  cfg->setValue("WorkDir", workDir );
  cfg->setValue("CurrentSheet",  tabText(currentIndex()));

  QStringList list;
  QListIterator<CYAnalyseSheet*> i(sheets);
  while (i.hasNext())
  {
    CYAnalyseSheet *as=i.next();
    if (as->fileName() != "" )
    {
      list.append(as->fileName());
    }
  }
  cfg->setValue( "Sheets", list );
}

void CYAnalyseSpace::readProperties( QSettings *cfg )
{
  QString currentSheet;

  workDir = cfg->value( "WorkDir", workDir ).toString();

  if ( !workDir.isEmpty() )
  {
    currentSheet = cfg->value( "CurrentSheet" ).toString();
    QStringList list = cfg->value( "Sheets" ).toStringList();
    for ( QStringList::Iterator it = list.begin(); it != list.end(); ++it )
      restoreAnalyseSheet( *it );
  }

  // Détermine la feuille visible.
  QListIterator<CYAnalyseSheet*> i(sheets);
  while (i.hasNext())
  {
    CYAnalyseSheet *as=i.next();
    if ( currentSheet == tabText(indexOf(as)) )
    {
      setCurrentIndex(indexOf(as));
      break;
    }
  }
//   updateCaption(currentWidget());
}

void CYAnalyseSpace::newAnalyseSheet()
{
  /* Trouve un nom de la forme "Sheet %d" qui ne soit pas encore utilisé par aucune feuille de données existante. */
  int     i = 1;
  bool    found;
  QString sheetName;
  do
  {
    sheetName = QString( tr( "Sheet %1" ) ).arg( i++ );
    found = false;
    QListIterator<CYAnalyseSheet*> i(sheets);
    while (i.hasNext())
    {
      CYAnalyseSheet *as=i.next();
      if ( sheetName == tabText(indexOf(as)) )
        found = true;
    }
  }
  while ( found );

  CYAnalyseSheet *sheet = new CYAnalyseSheet( this, "sheet" );
  Q_CHECK_PTR( sheet );
  if ( sheet->settings( true ) )
  {
    addTab( sheet, sheet->title() );
    sheets.append( sheet );
    setCurrentIndex(indexOf(sheet));
    connect( sheet, SIGNAL( sheetModified( QWidget * ) ), this, SLOT( updateCaption( QWidget * ) ) );
    sheet->connectModifie( true );
  }
  else
    delete sheet;
}

bool CYAnalyseSpace::saveAllSheets( bool withWarning )
{
  QListIterator<CYAnalyseSheet*> i(sheets);
  while (i.hasNext())
  {
    CYAnalyseSheet *sheet = i.next();
    if ( sheet->hasBeenModified() )
    {
      if ( withWarning && ( !autoSave || sheet->fileName().isEmpty() ) )
      {
        QString msg = QString( tr( "The analyse sheet '%1' has been modified.\n"
                                   "Do you want to save this analyse sheet?" ) )
            .arg( tabText(indexOf(sheet)) );
        int res = CYMessageBox::warningYesNoCancel( mWin, msg );


        if ( res == CYMessageBox::Yes )
          saveAnalyseSheet( sheet );
        if (( res == CYMessageBox::No ) && !sheet->fileName().isEmpty() )
        {
          QString fileName = sheet->fileName();
          deleteAnalyseSheet( fileName );
          loadAnalyseSheet( fileName );
        }
        else if ( res == CYMessageBox::Cancel )
          return false; // annuler quitter
      }
      else
        saveAnalyseSheet( sheet );
    }
  }

  return ( true );
}

void CYAnalyseSpace::loadAnalyseSheet()
{
  QString fileName;
  fileName = QFileDialog::getOpenFileName(
        mWin,
        tr( "Select a work sheet to load" ),
        workDir, "*.cyas" );
  loadAnalyseSheet( fileName );
}

CYAnalyseSheet *CYAnalyseSpace::loadAnalyseSheet( const QString& fileName )
{
  if (fileName.isEmpty())
    return 0;

//  TODO KDE3->QT3
//  QString tmpFile;
//  KIO::NetAccess::download( url, tmpFile );
//  workDir = tmpFile.left( tmpFile.findRev( '/' ) );
//  // Charge la feuille à partir du fichier.
//  CYAnalyseSheet *sheet = restoreAnalyseSheet( tmpFile );
//  /* Si nous avons chargé un fichier non-local nous effaçons le nom
//  du fichier afin que l'utilisateur soit invité à enregister le fichier. */
//  KURL tmpFileUrl;
//  tmpFileUrl.setPath( tmpFile );
//  if ( tmpFileUrl != url.url() )
//    sheets.last()->setFileName( 0 );
//  KIO::NetAccess::removeTempFile( tmpFile );

  // Charge la feuille à partir du fichier.
  CYAnalyseSheet *sheet = restoreAnalyseSheet( fileName );
  updateCaption( currentWidget() );
  emit announceRecentURL( QUrl( fileName ) );
  connectModifie( true );
  return sheet;
}

void CYAnalyseSpace::saveAnalyseSheet()
{
  saveAnalyseSheet((CYAnalyseSheet *)currentWidget());
}

void CYAnalyseSpace::saveAnalyseSheet( CYAnalyseSheet *sheet )
{
  if ( !sheet )
  {
    CYMessageBox::sorry( mWin, tr( "You don't have an analyse sheet that could be saved!" ) );
    return;
  }

  QString fileName = sheet->fileName();

  if ( fileName.isEmpty() )
  {
    fileName = QFileDialog::getSaveFileName( mWin, tr( "Save current work sheet as" ),  workDir + "/" + sheet->title() + ".cyas", "*.cyas");
    if ( fileName.isEmpty() )
      return;

    workDir = fileName.left( fileName.lastIndexOf( '/' ) );
    // extraction du nom de fichier sans le chemin
    QString baseName = fileName.right( fileName.length() - fileName.lastIndexOf( '/' ) - 1 );
    // suppression de l'extension (habituellement '.cyas')
    baseName = baseName.left( baseName.lastIndexOf( '.' ) );
  }
  else
  {
    workDir = fileName.left( fileName.lastIndexOf( '/' ) );
    QFileInfo infoDir(workDir);
    if (!infoDir.isWritable())
    {
      saveAnalyseSheetAs(sheet);
      return;
    }
  }
  /* Si nous ne pouvons pas enregistrer le fichier il est probablement protég
  en écriture. Nous avons donc besoin de demander un nouveau nom à l'utilisateur. */
  if ( !sheet->save( fileName ) )
  {
    saveAnalyseSheetAs( sheet );
    return;
  }

  /* Ajoute le fichier au menu des documents récents. */
  QUrl url;
  url.setPath( fileName );
  emit announceRecentURL( url );
}

void CYAnalyseSpace::saveAnalyseSheetAs()
{
  saveAnalyseSheetAs((CYAnalyseSheet*) currentWidget());
}

void CYAnalyseSpace::saveAnalyseSheetAs( CYAnalyseSheet *sheet )
{
  if ( !sheet )
  {
    CYMessageBox::sorry( mWin, tr( "You don't have an analyse sheet that could be saved!" ) );
    return;
  }

  QString fileName;
  do
  {
    if (workDir.startsWith(":"))
      workDir=core->baseDirectory();
    while (sheet->title().isEmpty())
    {
      configure();
      if (sheet->title().isEmpty())
        CYMessageBox::sorry( mWin, tr( "You have to enter a title for the analyse sheet !" ) );
    }
    fileName = QFileDialog::getSaveFileName( mWin, tr( "Save current work sheet as" ),  workDir + "/" + sheet->title() + ".cyas", "*.cyas");
    if ( fileName.isEmpty() )
      return;
    workDir = fileName.left( fileName.lastIndexOf( '/' ) );
    // extraction du nom de fichier sans le chemin
    QString baseName = fileName.right( fileName.length() - fileName.lastIndexOf( '/' ) - 1 );
    // suppression de l'extension (habituellement '.cyas')
    baseName = baseName.left( baseName.lastIndexOf( '.' ) );
  }
  while ( !sheet->save( fileName ) );

  /* Ajoute le fichier au menu des documents récents. */
  QUrl url;
  url.setPath( fileName );
  emit announceRecentURL( url );
}

void CYAnalyseSpace::captureAnalyseSheet()
{
  captureAnalyseSheet((CYAnalyseSheet*) currentWidget());
}

void CYAnalyseSpace::captureAnalyseSheet( CYAnalyseSheet *sheet )
{
  if ( !sheet )
  {
    CYMessageBox::sorry( mWin, tr( "You don't have an analyse sheet that could be captured!" ) );
    return;
  }

  QString fileName = sheet->fileName();
  CYAnalyseSheetCapture *capture = new CYAnalyseSheetCapture( this, QString( "%1_capture" ).arg( sheet->objectName() ).toUtf8() );
  Q_CHECK_PTR( capture );
  if ( !capture->load( fileName ) )
  {
    delete capture;
    return;
  }
  QString title = capture->title();
  title.append( "-" + capture->captureDateTime().toString( "yyMMdd_hhmmss" ) );
  capture->setTitle( title );
  capture->setFileName( 0 );

  if (core)
    addTab( capture, core->loadIconSet( "camera.png", 22 ), capture->title() );
  setCurrentWidget( capture );
  sheets.append( capture );
  connect( capture, SIGNAL( sheetModified( QWidget * ) ), this, SLOT( updateCaption( QWidget * ) ) );

  /* Ajoute le fichier au menu des documents récents. */
  QUrl url;
  url.setPath(fileName);
  emit announceRecentURL(url);

  capture->setModified( true );
}

void CYAnalyseSpace::deleteAnalyseSheet()
{
  QString msg;

  CYAnalyseSheet *current = ( CYAnalyseSheet * ) currentWidget();

  if ( current )
  {
    if ( current->hasBeenModified() )
    {
      if ( !autoSave || current->fileName().isEmpty() )
      {
        msg = QString( tr( "The analyse sheet '%1' has been modified.\n"
                           "Do you want to save this analyse sheet?" ) )
            .arg( tabText( indexOf(current) ) );
        int res = CYMessageBox::warningYesNoCancel( mWin, msg );
        if ( res == CYMessageBox::Yes )
          saveAnalyseSheet( current );
      }
      else
        saveAnalyseSheet( current );
    }

    removeTab(indexOf(current));
    int i = sheets.indexOf(current);
    if (i != -1)
        delete sheets.takeAt(i);
    updateCaption( currentWidget() );
  }
  else
  {
    msg = QString( tr( "There are no analyse sheets that could be deleted!" ) );
    CYMessageBox::error( mWin, msg );
  }
}

void CYAnalyseSpace::deleteAnalyseSheet( const QString &fileName )
{
  QListIterator<CYAnalyseSheet*> i(sheets);
  while (i.hasNext())
  {
    CYAnalyseSheet *as=i.next();
    if ( QString(as->fileName() ) == fileName )
    {
      removeTab(indexOf(as));
      int j = sheets.indexOf(as);
      if (j != -1)
          delete sheets.takeAt(j);
      return;
    }
  }
}

CYAnalyseSheet *CYAnalyseSpace::restoreAnalyseSheet( const QString &fileName, const QString &newName )
{
  if (fileName.isEmpty())
    return 0;

  /* Il est possible que nous voulions enregistrer plus tard la feuille d'analyse
  sous un autre nom. Ce nom peut être défini comme newName. Si newName est vide nous
  utilisons le nom d'origine pour enregistrer la feuille d'analyse. */
  QString fName;
  if ( newName.isEmpty() )
    fName = fileName;
  else
    fName = newName;

  CYAnalyseSheet *sheet = new CYAnalyseSheet( this, "sheet" );
  Q_CHECK_PTR( sheet );
  if ( !sheet->load( fileName ) )
  {
    delete sheet;
    return 0;
  }
  addTab( sheet, sheet->title() );
  setCurrentWidget( sheet );
  sheets.append( sheet );
  connect( sheet, SIGNAL( sheetModified( QWidget * ) ), this, SLOT( updateCaption( QWidget * ) ) );

  /* Force le nom du fichier à être le nouveau nom. Ceci place également
  le flag modifié afin que le fichier soit sauvé en sortant .*/
  if ( !newName.isEmpty() )
    sheet->setFileName( newName.toUtf8() );
  sheet->setModified( false );
  return sheet;
}

void CYAnalyseSpace::cut()
{
  CYAnalyseSheet *current = ( CYAnalyseSheet * ) currentWidget();

  if ( current )
    current->cut();
}

void CYAnalyseSpace::copy()
{
  CYAnalyseSheet *current = ( CYAnalyseSheet * ) currentWidget();

  if ( current )
    current->copy();
}

void CYAnalyseSpace::paste()
{
  CYAnalyseSheet *current = ( CYAnalyseSheet * ) currentWidget();

  if ( current )
    current->paste();
}

void CYAnalyseSpace::configure()
{
  CYAnalyseSheet *current = ( CYAnalyseSheet * ) currentWidget();

  if ( !current )
    return;

  current->settings();
}

void CYAnalyseSpace::updateCaption( int index )
{
  QWidget *ws=widget(index);
  if (ws)
    updateCaption(ws);
}

void CYAnalyseSpace::updateCaption( QWidget *ws )
{
  QString caption = tr( "Graphic analyse" );

  if ( ws )
  {
    CYAnalyseSheet *sheet = ( CYAnalyseSheet * ) ws;
    if ( core && sheet->inherits( "CYAnalyseSheetCapture" ) )
    {
      setTabText( indexOf(sheet), sheet->title() );
      setTabIcon( indexOf(sheet), core->loadIconSet( "camera.png", 22 ) );
    }
    else
    {
      setTabText( indexOf(sheet), sheet->title() );
    }
    caption.append( ": " );
    caption.append( sheet->title() );
    emit newCaption( caption, (( CYAnalyseSheet * ) ws )->hasBeenModified() );
  }
  else
  {
    emit newCaption( caption, false );
  }
  bool allNotModified = true;

  QListIterator<CYAnalyseSheet*> i(sheets);
  while (i.hasNext())
  {
    CYAnalyseSheet *as=i.next();
    if ( as->hasBeenModified() )
      allNotModified = false;
    as->setIsOnTop( as == ws );
  }
  mWin->setModified( !allNotModified );
}

void CYAnalyseSpace::connectModifie( bool enable )
{
  QListIterator<CYAnalyseSheet*> i(sheets);
  while (i.hasNext())
  {
    CYAnalyseSheet *as=i.next();
    as->connectModifie( enable );
  }
}

void CYAnalyseSpace::showEvent( QShowEvent *e )
{
  QTabWidget::showEvent( e );
  connectModifie( true );
}
