#ifndef CYANALYSESHEETSETTINGS_H
#define CYANALYSESHEETSETTINGS_H

// CYLIBS
#include <cydialog.h>

namespace Ui {
    class CYAnalyseSheetSettings;
}


class CYAnalyseSheetSettings : public CYDialog
{
public:
  CYAnalyseSheetSettings(QWidget *parent=0, const QString &name=0);
  ~CYAnalyseSheetSettings();

  virtual void init(QString title, bool newSheet, int rows, int cols);
  uint rows();
  uint columns();
  QString title();

private:
    Ui::CYAnalyseSheetSettings *ui;
};

#endif // CYANALYSESHEETSETTINGS_H
