/***************************************************************************
                          cyanalysespace.h  -  description
                             -------------------
    début                  : mar fév 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYANALYSESPACE_H
#define CYANALYSESPACE_H

// QT
#include <QList>
#include <QString>
#include <QTabWidget>
#include <QShowEvent>

class CYCore;
class CYAnalyseWin;
class CYAnalyseSheet;
class QSettings;

/**
 * CYAnalyseSpace est widget de type classeur dans lequel on peut insérer différentes
 * feuilles d'analyses. Celles-ci peuvent contenir différents types d'afficheur de
 * données. Ce classeur communique avec le gestionnaire de données CYDM par
 * l'intermédiaire de l'explorateur de données CYDatasBrowser au moyen d'un système de
 * glisser-déposser.
 * @short Espace d'analyses.
 * @author Gérald LE CLEACH
 */

class CYAnalyseSpace : public QTabWidget
{
  Q_OBJECT

public:
  /** Construit l'espace d'analyse.
    * @param win     Fenêtre d'analyse.
    * @param parent  Widget parent.
    * @param name    Nom du classeur. */
  CYAnalyseSpace(CYAnalyseWin *win, QWidget *parent, const QString &name=0);
  /** Destructeur. */
  ~CYAnalyseSpace();

  /** Enregistre les propriétés du classeur dans le fichier de config cfg. */
  void saveProperties(QSettings *cfg);
  /** Lit les propriétés du classeur contenues dans le fichier de config cfg. */
  void readProperties(QSettings *cfg);
  /** Enregistre toutes les feuilles ouvertes. */
  bool saveAllSheets(bool withWarning=true);
  /** Reconstruction d'une feuille d'analyses. */
  CYAnalyseSheet *restoreAnalyseSheet(const QString &fileName, const QString &newName=QString());
  /** Supprime une feuille d'analyses du classeur. */
  void deleteAnalyseSheet(const QString &fileName);

public slots: // Public slots
  /** Création d'une nouvelle feuille d'analyses. */
  void newAnalyseSheet();
  /** Chargement d'une feuille d'analyses. */
  void loadAnalyseSheet();
  /**
   * Chargement d'une feuille d'analyses par lien url.
   * @param fileName  : fichier de cette feuille d'analyses.
   */
  CYAnalyseSheet *loadAnalyseSheet(const QString& fileName);
  /** Enregistre de la feuille d'analyses courante. */
  void saveAnalyseSheet();
  /**
   * Enregistre une feuille d'analyses.
   * @param sheet    : feuille d'analyses à sauvegarder.
   */
  void saveAnalyseSheet(CYAnalyseSheet* sheet);
  /** Enregistre la feuille d'analyses courante dans un fichier ayant pour nom le nom de cette feuille. */
  void saveAnalyseSheetAs();
  /**
   * Enregistre une feuille d'analyses dans un fichier ayant pour nom le nom de cette feuille.
   * @param sheet    : feuille d'analyses à sauvegarder.
   */
  void saveAnalyseSheetAs(CYAnalyseSheet *sheet);

  /** Capture la feuille d'analyses courante dans un fichier ayant le nom du fichier de cette feuille associé à la date et l'heure de la capture. */
  void captureAnalyseSheet();
  /**
   * Capture une feuille d'analyses dans un fichier ayant pour nom le nom du fichier de cette feuille associé à la date et l'heure de la capture.
   * @param sheet    : feuille d'analyses à capturer.
   */
  void captureAnalyseSheet(CYAnalyseSheet *sheet);


  /** Supprime la feuille d'analyses courante. */
  void deleteAnalyseSheet();
  /** Coupe l'objet sélectionné dans la feuille d'analyses courante. */
  void cut();
  /** Copie l'objet sélectionné dans la feuille d'analyses courante. */
  void copy();
  /** Colle l'objet copié dans l'emplacement sélectionné de la feuille d'analyses courante. */
  void paste();
  /** Configure la feuille d'analyses. */
  void configure();
  /** Mise à jour du titre par rapport à la feuille active relative à index. */
  void updateCaption(int index);
  /** Mise à jour du titre par rapport à la feuille active. */
  void updateCaption(QWidget *);

  /** Active ou désactive les connexions de modifications. */
  void connectModifie(bool enable);

signals:
  /** Place l'url passé en argument comme récent. */
  void announceRecentURL(const QUrl &url);
  /** Indique que la feuille d'analyses a un nouveau titre. */
  void newCaption(const QString &text, bool modified);

protected: // Protected methods
  /** Fonction appelée à l'affichage du widget. */
  virtual void showEvent(QShowEvent *e);

private: // Private attributes
  /** Fenêtre d'analyse. */
  CYAnalyseWin *mWin;
  /** Liste des feuilles d'analyses ouvertes. */
  QList<CYAnalyseSheet*> sheets;
  /** Répertoire servant à ouvrir/enregistrer. */
  QString workDir;
  /** Flag d'enregistrement automatique. */
  bool autoSave;
};

#endif
