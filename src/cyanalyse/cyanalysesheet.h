/***************************************************************************
                          cyanalysesheet.h  -  description
                             -------------------
    début                  : mar fév 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYANALYSESHEET_H
#define CYANALYSESHEET_H

// QT
#include <QFrame>
#include <QEvent>
#include <QLayout>
#include <QDomElement>
#include <QMap>
#include <QMenu>
#include <QDateTime>

class CYDB;
class CYTime;
class CYDisplay;
class CYAnalyseCell;

/** CYAnalyseSheet est une feuille d'analyses contenant les afficheurs pour visualiser les
  * valeurs des données. Lors de la création d'une feuille d'analyses il faut spécifier
  * le nombre de lignes et de colonnes. Des afficheurs peuvent être ajoutés et enlevés
  * la volée. La grille de position s'occupe du positionnement.
  * Les afficheurs sont ajoutés en trainant une donnée de l'explorateur de données,
  * CYDatasBrowser, vers un espace vide de la feuille d'analyses.
  * @short Feuille d'analyses.
  * @author Gérald LE CLEACH
  */

class CYAnalyseSheet : public QFrame
{
  Q_OBJECT
  Q_PROPERTY(QString fileName READ fileName WRITE setFileName)
  Q_PROPERTY(bool forcing READ forcing WRITE setForcing)
  Q_PROPERTY(bool isProtected READ isProtected WRITE setProtected)

public:
  /** Construit une feuille d'analyses vide.
      * @param  parent      Widget parent.
      * @param  name        Nom de la feuille.
      * @param  orientation Orientation de la grille. */
  CYAnalyseSheet(QWidget *parent, const QString &name=0, Qt::Orientation orientation=Qt::Horizontal);
  /** Construit une feuille d'analyses paramétrée.
      * @param  parent      Widget parent.
      * @param  name        Nom de la feuille.
      * @param  rows        Nombre de lignes.
      * @param  rows        Nombre de colonnes.
      * @param  interval    Intervalle de rafraichissement en ms.
      * @param  orientation Orientation de la grille. */
  CYAnalyseSheet(QWidget *parent, const QString &name, uint rows, uint columns, uint interval, Qt::Orientation orientation=Qt::Horizontal);
  ~CYAnalyseSheet();

  /** Charge une feuille d'analyses à partir d'un fichier. */
  bool load(const QString &fN);
  /** Enregistre une feuille d'analyses dans un fichier. */
  bool save(const QString &fN);

  /** Coupe un afficheur. */
  void cut();
  /** Copie un afficheur. */
  void copy();
  /** Colle un afficheur. */
  void paste();

  /** @return le nom du fichier. */
  QString fileName() const { return mFileName.toUtf8(); }
  /** Saisie le nom du fichier. */
  void setFileName(const QString fN);

  /** @return 'true' si la feuille a été modifiée. */
  bool hasBeenModified();

  /** Saisie le titre de la feuille. */
  void setTitle(const QString &title);
  /** @return le titre de la feuille. */
  QString title() const { return mTitle; }


  void setIsOnTop(bool onTop);

  /** @return la tempo de rafraîchissement. */
  CYTime *refreshTime();

  CYAnalyseCell * findCell(QString position);
  /** Saisie le titre de l'afficheur de la cellule correspondant à \a position.
        Ceci est utile pour la traduction après chargement d'une feuille système
        @param position La position est donnée par une chaîne de caractères composée du numéro de ligne et du numéro de colonne de la cellule sépararés par un '/'.
        @see CYAnalyseSheet::findCell(QString position) */
  virtual void setDisplayTitle(QString position, QString title);

  /** @return Version de CYLIBS lors de l'enregistrement du fichier de sauvegarde */
  QString versionCYDOM() { return mVersionCYDOM; }

public: // Public methods
  /** @return \a true si la feuille d'analyse est en mode forçage. */
  bool forcing() const;
  /** Saisir \a true pour activer le mode forçage. */
  void setForcing(const bool val);
  virtual bool isProtected() const;
  void initTRDisplay(QString pos, QString title);
  CYAnalyseCell * cell0();

  /** Construit la grille de positionnement des afficheurs à partir d'une cellule d'analyse.
      * @param row         Nombre de lignes.
      * @param col         Nombre de colonnes.
      * @param orientation Orientation de la grille. */
  void createGrid(uint row, uint col, Qt::Orientation orientation);

public slots: // Public slots
  /** Charge la feuille d'analyses à partir du nom de fichier courant. */
  virtual bool load();
  /** Sauvegarde la feuille d'analyses à partir du nom de fichier courant. */
  bool save();
  void showPopupMenu(CYDisplay *display);
  void setModified(bool mfd);
  /** Active ou désactive les connexions de modifications. */
  void connectModifie(bool enable);
  /** Gestion du menu surgissant.
      * @param pm0    Menu appelant s'il existe.
      * @param index  Index de l'entrée du menu appelant. */
  virtual int showPopupMenu(QMenu *pm0=0, int index=0);
  /** Configure la feuille d'analyses. @return \a false lorsque cette le changement de configuration est annulé.
      * @param init Boîte de configuration lancée pour la création de la feuille. */
  bool settings(bool init=false);
  virtual void setProtected(const bool val);
  /** Capture une feuille d'analyses dans un fichier. */
  void capture();

signals:
  /** Emis à chaque modification de la feuille. */
  void sheetModified(QWidget *sheet);
  /** Emis à chaque rafraîchissement. */
  void refreshing();

protected: // Protected methods
  /** Gestion d'un QCustomEvent. */
  virtual void customEvent(QEvent* ev);
  /** Initialise la feuille d'analyses. */
  virtual void init();
  /** Fonction appelée à l'affichage du widget. */
  virtual void showEvent(QShowEvent *e);

private: // Private methods
  /** Liste tous les hôtes de la feuille. */
  void collectHosts(QStringList &l);
  /** Redimensionne la grille de positionnement des afficheurs. */
  void resizeGrid(uint r, uint c);
  /** Donne l'ordre de déplacement du focus dans la grille par la touche de tabulation. */
  void fixTabOrder();

protected: // Protected attributes
  /** Nom du fichier d'enregistrement. */
  QString mFileName;
  /** Nom de la feuille. */
  QString mTitle;
  /** Flag de modif de la feuille. */
  bool mModified;
  /** Flag d'information de nouvelle configuration. */
  bool mNewSettings;
  /** Nombre de lignes de la grille. */
  uint mRows;
  /** Nombre de colonnes de la grille. */
  uint mColumns;
  /** Base de données locale à cet afficheur. */
  CYDB *mDB;
  /** Temps de rafraîchissement de la feuille. */
  CYTime *mRefreshTime;
  /** Timer de rafraîchissement de la feuille. */
  QTimer *mRefreshTimer;
  /** Grille de positionnement. */
  QGridLayout *mGrid;
  /** Cellule d'analyse initiale. */
  CYAnalyseCell *mCell;
  /** Protection. */
  bool mProteted;
  /** Orientation de la grille. */
  Qt::Orientation mOrientation;
  /** Cellules initiales contenant des afficheurs. */
  QMap<QString, CYAnalyseCell*> mGridCells;
  /** Mode forçage. */
  bool mForcing;
  /** Version de CYLIBS lors de l'enregistrement du fichier de sauvegarde */
  QString mVersionCYDOM;

  QStringList mErrorList;
};

#endif
