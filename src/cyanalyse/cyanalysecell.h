/***************************************************************************
                          cyanalysecell.h  -  description
                             -------------------
    begin                : mar déc 7 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYANALYSECELL_H
#define CYANALYSECELL_H

// QT
#include <QLayout>
#include <QList>
#include <QSplitter>
#include <QMenu>
// CYLIBS
#include "cydisplaycell.h"

class CYAnalyseSheet;
class CYDisplay;

/** @short Cellule d'une feuille d'analyses.
  * Une cellule contient soit un afficheur CYDisplay soit
  * une division QSplitter qui peut être horizontal ou vertical.
  * Dans ce cas elle est divisée en d'autres cellules qui peuvent
  * contenir à leur tour un afficheur ou être divisées.
  * @author LE CLÉACH Gérald
  */

class CYAnalyseCell : public CYDisplayCell
{
  Q_OBJECT
public:
  /** Type de cellule. */
  enum Type
  {
    /** Contient un afficheur CYDisplay. */
    Display,
    /** Contient un QSplitter horizontal. */
    Horizontal,
    /** Contient un QSplitter vertical. */
    Vertical,
  };

  /** Constructeur de la cellule principale.
    * @param sheet  feuille d'analyse.
    * @param parent parent de la cellule.
    * @param name   nom de la cellule.
    * @param type   type de cellule.
    * @param level  niveau de cellule. */
  CYAnalyseCell(CYAnalyseSheet *sheet, QWidget *parent, const QString &name, Type type, int level=0);

  /** Constructeur d'une cellule.
    * @param sheet  feuille d'analyse.
    * @param cell   cellule d'analyse parent (contenant cette cellule).
    * @param parent parent de la cellule.
    * @param name   nom de la cellule.
    * @param type   type de cellule.
    * @param level  niveau de cellule. */
  CYAnalyseCell(CYAnalyseSheet *sheet, CYAnalyseCell *cell, QWidget *parent, const QString &name, Type type, int level=0);

  ~CYAnalyseCell();

  /** @return le type de cellule. */
  Type type() { return mType; }

  /** @return une nouvelle cellule contenu dans celle-ci.
    * @param type   type de cellule.
    * @param level  niveau de cellule. */
  CYAnalyseCell *newCell(Type type, int level);
  /** Place l'afficheur dans la cellule.
    * @param  name  Nom de la donnée / ou du groupe de données. */
  void setDisplay(const QString& name);
  /** Place l'afficheur dans la cellule. */
  void setDisplay(CYDisplay *display=0);

  /** Construit une cellule à partir d'un DOM. */
  bool createFromDOM(CYAnalyseSheet *sheet, QWidget *parent, QDomElement &element, QStringList *ErrorList=nullptr);
  /** Ajoute une cellule dans un DOM. */
  bool addToDOM(QDomDocument &doc, QDomElement &element, bool save=true, int size=100);
  /** Construit un afficheur dans la cellule à partir d'un élément DOM. */
  bool createDisplayFromDOM(QDomElement &el, QStringList *ErrorList=nullptr);
//  /** Ajoute un afficheur dans la cellule à partir d'un élément DOM. */
  bool addDisplayToDOM(QDomDocument &doc, QDomElement &element, bool save=true);
  /** Saisie le niveau de la cellule. */
  void setLevel(int val);
  /** @return le niveau de la cellule. */
  int level();
  /** @return la cellule parent de différence de niveau \a dl
    * dl=1 => parent,
    * dl=2 => grand parent, */
  CYAnalyseCell *cellParent(int dl);
  /** Active ou désactive les connexions de modifications. */
  void connectModifie(bool enable);
  /** @return la cellule qui a le focus. */
  CYAnalyseCell *cellHasFocus();

  /** @return la feuille d'analyse de ratachement. */
  CYAnalyseSheet *sheet() { return mSheet; }

  /** @return la cellule correspondant à \a position ou 0 si elle n'existe pas.
      @param position Permet de retrouver la bonne cellule.
                      La position est constituée de divisions horizontales et verticales. Chacune d'elles part d'une cellule qui est soit la cellule initiale (Cell0) soit une des 2 cellules résultant de la division d'une autre cellule. Dans ce cas il faut indiquer laquelle de celles-ci nous intéresse.
                      Nous avons alors une chaîne de caractères formées de sections correspondant à des divisions de cellule avec la cellule fille de départ dans chaque section.
                        - Par exemple pour atteindre la cellule à la 1ère colonne de la 2ème ligne de la grille nous avons:
                          "2/1" (Cellule fille n°2 de la première division (Horizontale) puis cellule fille n°1 (Verticale) de la deuxième division)
                        - Par exemple pour atteindre la cellule à la 1ère ligne de la 2ème colonne de la grille nous avons:
                          "2/1" (Cellule fille n°2 de la première division (Verticale) puis cellule fille n°1 (Horizontale) de la deuxième division)
      \fn CYAnalyseCell::findCell(QString position)
  */
  CYAnalyseCell * findCell(QString position);
  CYAnalyseCell * cellChild(int id);

public: // Public methods
  /** Saisir \a true pour activer le mode forçage. */
  void setForcing(const bool val);
  /** Gestion du menu surgissant.
    * @param pm0    Menu appelant s'il existe.
    * @param index  Index de l'entrée du menu appelant. */
  virtual int showPopupMenu(QMenu *pm0=0, int index=0);
    virtual bool forcing();

public slots: // Public slots
  /** Démarre le rafraîchissement. */
  virtual void startRefresh();
  /** Donne l'ordre de diviser la cellule horizontalement. */
  void horizontalSplit();
  /** Donne l'ordre de diviser la cellule verticalement. */
  void verticalSplit();
  /** Donne l'ordre de diviser la cellule. */
  void split(int orientation);
  /* Construit une description XML de la cellule. */
  QString asXML();
  /* Construit une description XML de l'afficheur. */
  QString displayAsXML();
  /** Supprime cette cellule. */
  void remove();
  /** Supprime une cellule enfant. */
  void removeChild(CYAnalyseCell *cell);

    virtual void newDisplaySimple();
    virtual void newLCDMultimeter();
    virtual void newAnalogMultimeter();
    virtual void newScope();
    virtual void newScopeMultiple();
    virtual void newBargraph();
    virtual void newDatasTable();

private: // Private methods
  /** Fonction d'initialisation. */
  void init();

protected: // Protected attributes
  /** Type de cellule. */
  Type mType;
  /** Division. */
  QSplitter *mSplitter;
  /** Feuille d'analyse de ratachement. */
  CYAnalyseSheet *mSheet;
  /** Cellule d'analyse parent (contenant cette cellule). */
  CYAnalyseCell *mCellParent;
  /** Liste des cellules enfant dans le cas d'une division. */
  QList<CYAnalyseCell*> mCellsChild;
  /** Niveau de la cellule par rapport aux autres cellules. */
  int mLevel;
};

#endif
