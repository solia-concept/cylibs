/***************************************************************************
                          cyanalysecell.cpp  -  description
                             -------------------
    begin                : mar déc 7 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyanalysecell.h"

// QT
#include <QClipboard>
#include <QList>
#include <QMenu>
// CYLIBS
#include "cyanalysesheet.h"
#include "cyanalysecellremoveevent.h"
#include "cydisplaydummy.h"
#include "cydisplaysimple.h"
#include "cymultimeter.h"
#include "cyscope.h"
#include "cyscopemultiple.h"
#include "cybargraph.h"
#include "cydatastable.h"
#include "cycore.h"
#include "cydata.h"
#include "cymessagebox.h"
#include "cyapplication.h"

CYAnalyseCell::CYAnalyseCell(CYAnalyseSheet *sheet, QWidget *parent, const QString &name, Type type, int level)
: CYDisplayCell(parent,name)
{
  mSheet = sheet;
  mType  = type;
  mLevel = level;
  mCellParent = 0;
  init();
}

CYAnalyseCell::CYAnalyseCell(CYAnalyseSheet *sheet, CYAnalyseCell *cell, QWidget *parent, const QString &name, Type type, int level)
: CYDisplayCell(parent,name)
{
  mSheet = sheet;
  mType  = type;
  mLevel = level;
  mCellParent = cell;
  init();
}

CYAnalyseCell::~CYAnalyseCell()
{
}

void CYAnalyseCell::init()
{
  mSplitter = 0;

  if ( mType==Display )
  {
    setDisplay();
    mGrid->addWidget( mDisplay, 0, 0 );
  }
  else if ( mType==Horizontal )
  {
    mSplitter = new QSplitter(Qt::Horizontal, this);
    //mCellLayout->addWidget(mSplitter);
    mGrid->addWidget( mSplitter, 0, 0 );
  }
  else if ( mType==Vertical )
  {
    mSplitter = new QSplitter(Qt::Vertical, this);
    //mCellLayout->addWidget(mSplitter);
    mGrid->addWidget( mSplitter, 0, 0 );
  }
  else
    CYFATAL;
}

CYAnalyseCell *CYAnalyseCell::newCell(Type type, int level)
{
  CYAnalyseCell *cell = new CYAnalyseCell( mSheet, this, mSplitter, QString("Cell%1_%2").arg(level).arg(mCellsChild.count()), type, level);
  cell->connectModifie( mConnectModifie );

  mCellsChild.append(cell);
  return cell;
}

void CYAnalyseCell::setDisplay(const QString& name)
{
  if (!core)
    return;

  bool group = false;
  if (name.contains(tr("Group")))
    group = true;

  if (mDisplay)
  {
    /* Si l'afficheur existant est un emplacement vide
    * nous remplaçons le widget. Sinon nous essayons juste d'ajouter
    * la nouvelle donnée à l'afficheur existant. */
    if ( QString(mDisplay->metaObject()->className())=="CYDisplayDummy" )
    {
      CYDisplay* newDisplay = 0;

      CYData *data=0;
      if (!group)
      {
        if ((data = core->findData(name, false, false)) == 0)
          return;
      }

      /* Si le type de donnée est supporté par plus d'un type d'afficheur
      * nous faisons apparaître un menu afin que l'utilisateur puisse
      * sélectionner quel afficheur il veut. */
      if (group || data)
      {
        QMenu pm(tr("Select a display type"));
        pm.addSeparator();

        QAction *simpleAct = pm.addAction(tr("&Simple"));
        simpleAct->setEnabled( !group );

        QMenu *pm2 = pm.addMenu(tr("&Multimeter"));
        QAction *analogAct = pm2->addAction(tr("&Analog"));
        analogAct->setEnabled( !group && data->isNum() && !data->isFlag() );
        QAction *digitalAct = pm2->addAction(tr("&Digital"));
        digitalAct->setEnabled( !group && data->isNum() && !data->isFlag() );
        pm2->setEnabled( !group && data->isNum() && !data->isFlag() );

        QAction *classicScopeAct = pm.addAction(tr("&Classical oscilloscope"));
        classicScopeAct->setEnabled( !group && data->isNum() && !data->isFlag() && (data->mode()==Cy::User) );
        classicScopeAct->setWhatsThis(tr("Oscilloscope may have two ordinate axes, each with its own format."));

        QAction *multiScopeMAct = pm.addAction(tr("Oscilloscope multiple formats"));
        multiScopeMAct->setEnabled( !group && data->isNum() && !data->isFlag() && (data->mode()==Cy::User) );
        multiScopeMAct->setWhatsThis(tr("Oscilloscope may have several different data formats on a single axis of ordinates. The label of each measurement is displayed with his unit in brackets. The configurable display coefficient, if it differs from 1, also appears in brackets."));

        QAction *bargraphAct = pm.addAction(tr("&BarGraph"));
        bargraphAct->setEnabled( !group && data->isNum() && !data->isFlag() );

        QAction *dataTableAct = pm.addAction(tr("Datas &table"));

        QAction *action = pm.exec(QCursor::pos());
        if (action==simpleAct)
        {
          CYDisplaySimple *display = new CYDisplaySimple(this, "CYDisplaySimple");
          display->addData(data, 0);
          newDisplay = display;
        }
        else if (action==analogAct)
        {
          CYMultimeter *display = new CYMultimeter(this, "CYMultiMeter", 0, false, true, false);
          display->addData(data, 0);
          newDisplay = display;
        }
        else if (action==digitalAct)
        {
          CYMultimeter *display = new CYMultimeter(this, "CYMultiMeter", 0, false, false, true);
          display->addData(data);
          newDisplay = display;
        }
        else if (action==classicScopeAct)
        {
          CYScope *display = new CYScope(this, "CYScope", 0, false, CYScope::Time, CYScope::Continuous);
          display->addData(data);
          newDisplay = display;
        }
        else if (action==multiScopeMAct)
        {
          CYScopeMultiple *display = new CYScopeMultiple(this, "CYScope", 0, false, CYScope::Time, CYScope::Continuous);
          display->addData(data);
          newDisplay = display;
        }
        else if (action==bargraphAct)
        {
          CYBargraph *display = new CYBargraph(this, "CYBarGraph");
          display->addData(data);
          newDisplay = display;
        }
        else if (action==dataTableAct)
        {
          CYDatasTable *display = new CYDatasTable(this, "DatasTable");
          newDisplay = display;
          if (group)
            display->addGroup(name);
          else
            display->addData(data);
        }
        else if (action==analogAct)
        {
          CYMultimeter *display = new CYMultimeter(this, "CYMultiMeter", 0, false, true, false);
          display->addData(data);
          newDisplay = display;
        }
        else
          return;
      }
      else
      {
        if (!group && !data->isNum())
          CYMessageBox::sorry(this, tr("No display for this type of data"));
        return;
      }
      setDisplay(newDisplay);
    }
    else if (group)
    {
      mDisplay->addGroup(name);
    }
    else
    {
      CYData *data;
      if ((data = core->findData(name, false, false)) == 0)
        return;
      mDisplay->addData(data);
    }
  }

  setModifieOnResize(true);
  setModified(true);
}

void CYAnalyseCell::setDisplay(CYDisplay *display)
{
  if (mDisplay)
  {
   // mCellLayout->removeWidget(mDisplay);
  //  disconnect(mDisplay, SIGNAL(showPopupMenu(CYDisplay*)), mSheet, SLOT(showPopupMenu(CYDisplay*)));
  //  disconnect(mDisplay, SIGNAL(displayModified(bool)), mSheet, SLOT(setModified(bool)));
    delete mDisplay;
  }

  if (!display)
  {
    // insert un nouvel afficheur
    mDisplay = new CYDisplayDummy(this, "CYDisplayDummy");
  }
  else
  {
    mDisplay = display;

    connect(mDisplay, SIGNAL(showPopupMenu(CYDisplay*)), mSheet, SLOT(showPopupMenu(CYDisplay*)));
    connect(mDisplay, SIGNAL(displayModified(bool)), mSheet, SLOT(setModified(bool)));
  }

  //mCellLayout->addWidget( mDisplay );
  mGrid->addWidget( mDisplay, 0, 0 );

  if (isVisible())
  {
    mDisplay->show();
    mDisplay->refresh();
  }
  mGrid->activate();

  setModified(true);
}

bool CYAnalyseCell::createFromDOM(CYAnalyseSheet *sheet, QWidget *, QDomElement &element, QStringList *ErrorList)
{
  mCellsChild.clear();
  // Chargement des cellules enfants
  mType = (CYAnalyseCell::Type)element.attribute("type").toInt();
  if ( mType==Display )
  {
    QDomNodeList list;
    if ( mDisplayPasting )
      list = element.elementsByTagName("display");
    else
      list = element.elementsByTagName(QString("display%1").arg(mLevel));
    for (int num=0; num<list.count(); num++)
    {
      QDomElement el = list.item(num).toElement();
      createDisplayFromDOM(el, ErrorList);
    }
  }
  else
  {
    if ( mType==CYAnalyseCell::Horizontal )
    {
      mSplitter = new QSplitter(Qt::Horizontal, this);
      mGrid->addWidget( mSplitter, 0, 0 );
    }
    else if ( mType==CYAnalyseCell::Vertical )
    {
      mSplitter = new QSplitter(Qt::Vertical, this);
      mGrid->addWidget( mSplitter, 0, 0 );
    }
    QList<int> listSizes;
    QDomNodeList list = element.elementsByTagName(QString("cell%1").arg(mLevel+1));
    for (int num=0; num<list.count(); num++)
    {
      QDomElement el = list.item(num).toElement();
      listSizes.append(el.attribute("size").toInt());
      CYAnalyseCell *cell = newCell((CYAnalyseCell::Type)el.attribute("type").toInt(), mLevel+1);
      cell->createFromDOM(sheet, this, el, ErrorList);
    }
    mSplitter->setSizes(listSizes);
  }

  setModified(false);

  return true;
}

bool CYAnalyseCell::addToDOM(QDomDocument &doc, QDomElement &element, bool save, int size)
{
  QDomElement el = doc.createElement(QString("cell%1").arg(mLevel));
  element.appendChild(el);
  el.setAttribute("type", mType);
  el.setAttribute("size", size);

  if ( mType==Display )
  {
    addDisplayToDOM(doc, el, save);
  }
  else
  {
    QList<int> list = mSplitter->sizes();
    QList<int>::Iterator it = list.begin();
    for (int num=0; num<mCellsChild.count(); num++)
    {

      CYAnalyseCell *cell=mCellsChild.at(num);
      cell->addToDOM(doc, el, save, *it);
      ++it;
    }
  }

  if (save)
    setModified(false);

  return true;
}

bool CYAnalyseCell::createDisplayFromDOM(QDomElement &el, QStringList *ErrorList)
{
  QString classType = el.attribute("class");

  CYDisplay* newDisplay;
  if (classType == "CYDisplaySimple")
    newDisplay = new CYDisplaySimple(this, "CYDisplaySimple", 0, false);
  else if (classType == "CYMultimeter")
    newDisplay = new CYMultimeter(this, "CYMultimeter", 0, false);
  else if (classType == "CYScope")
    newDisplay = new CYScope(this, "CYScope", 0, false);
  else if (classType == "CYScopeMultiple")
    newDisplay = new CYScopeMultiple(this, "CYScopeMultiple", 0, false);
  else if (classType == "CYBargraph")
    newDisplay = new CYBargraph(this, "CYBargraph", 0, false);
  else if (classType == "CYDatasTable")
    newDisplay = new CYDatasTable(this, "CYDatasTable", 0, false);
  else
    newDisplay = new CYDisplayDummy(this, "CYDisplayDummy");
  Q_CHECK_PTR(newDisplay);

//  if (newDisplay->globalUpdateInterval)
//    newDisplay->setUpdateInterval(updateIntervalSec() * 1000 + updateIntervalMSec());

  // charge les paramètres spécifiques d'affichage
  if (!newDisplay->createFromDOM(el, ErrorList))
    return false;

  setDisplay(newDisplay);
  return true;
}

bool CYAnalyseCell::addDisplayToDOM(QDomDocument &doc, QDomElement &element, bool save)
{
  QDomElement el = doc.createElement(QString("display%1").arg(mLevel));
  element.appendChild(el);
  if (mDisplay)
  {
    el.setAttribute("class", mDisplay->metaObject()->className());
    mDisplay->addToDOM(doc, el, save);
  }
  return true;
}

void CYAnalyseCell::setLevel(int val)
{
  mLevel = val;
  for (int i = 0; i < mCellsChild.size(); ++i)
    mCellsChild.at(i)->setLevel(mLevel+1);
}

int CYAnalyseCell::level()
{
  return mLevel;
}

void CYAnalyseCell::startRefresh()
{
  if (sheet()->inherits("CYAnalyseSheetCapture"))
    return;

  if (mDisplay)
    mDisplay->startRefresh();
}

void CYAnalyseCell::horizontalSplit()
{
  split(Horizontal);
}

void CYAnalyseCell::verticalSplit()
{
  split(Vertical);
}

void CYAnalyseCell::split(int orientation)
{
  mType = (Type)orientation;

  hide();

  if ( mType==Horizontal )
  {
    mSplitter = new QSplitter(Qt::Horizontal, this);
//    mCellLayout->addWidget( mSplitter );
    mGrid->addWidget( mSplitter, 0, 0 );

  }
  else if ( mType==Vertical )
  {
    mSplitter = new QSplitter(Qt::Vertical, this);
  //  mCellLayout->addWidget( mSplitter );
    mGrid->addWidget( mSplitter, 0, 0 );
  }

  //nouvelle cellule 1 : remplacement de l'ancienne
  CYAnalyseCell *cell = newCell(Display, mLevel+1);

  //remplacement du Display de la nouvelle cellule par l'ancien
  cell->setDisplay(mDisplay);
  bool refreshWasStop=0;
  if ( mDisplay )
    refreshWasStop = mDisplay->refreshIsStop();
  stopRefresh();
  cell->setModifieOnResize(true);
  //nouvelle cellule 2
  cell = newCell(Display, mLevel+1);
  cell->setModifieOnResize(true);

  if (!refreshWasStop)
    startRefresh();

  mGrid->activate();
  show();
  setModified(true);
}

QString CYAnalyseCell::asXML()
{
  /* Nous construisons une description XML de la cellule. */
  QDomDocument doc("CYLIXCell");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  QDomElement el = doc.createElement(QString("cell%1").arg(mLevel));
  doc.appendChild(el);
  el.setAttribute("type", mType);
  el.setAttribute("size", 100);

  if ( mType==Display )
  {
    addDisplayToDOM(doc, el, false);
  }
  else if ( ( mType==Horizontal ) || ( mType==Vertical ) )
  {
    QList<int> list = mSplitter->sizes();
    QList<int>::Iterator it = list.begin();
    for (int i = 0; i < mCellsChild.size(); ++i)
    {
      mCellsChild.at(i)->addToDOM(doc, el, false, *it);
      ++it;
    }
  }

  return doc.toString();
}

QString CYAnalyseCell::displayAsXML()
{
  if (!mDisplay)
    return QString();

  /* Nous construisons une description XML de l'afficheur. */
  QDomDocument doc("CYLIXDisplay");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  QDomElement el;
  if (mDisplayCopying)
    el = doc.createElement("display");
  else
    el = doc.createElement(QString("display%1").arg(mLevel));
  doc.appendChild(el);
  el.setAttribute("class", mDisplay->metaObject()->className());
  mDisplay->addToDOM(doc, el);

  return doc.toString();
}

CYAnalyseCell *CYAnalyseCell::cellParent(int dl)
{
  if (dl<=1)
    return mCellParent;
  else if (mCellParent)
    return mCellParent->cellParent(dl-1);
  else
    return 0;
}

void CYAnalyseCell::remove()
{
  CYAnalyseCellRemoveEvent *ev = new CYAnalyseCellRemoveEvent(mCellParent, this);
  cyapp->postEvent(mSheet, ev);
}

void CYAnalyseCell::removeChild(CYAnalyseCell *cell)
{
  CYAnalyseCell *c;
  if (!cell)
    return;

  cell->stopRefresh();

  if (mCellsChild.size()>2)
  {
    CYWARNINGTEXT(tr("The parent of cell %1 has more than 2 children!").arg(cell->objectName()));
    return;
  }

  QListIterator<CYAnalyseCell*> i(mCellsChild);
  while (i.hasNext())
  {
    c=i.next();
    if (c && (c!=cell))
    {
      c->setLevel( mLevel );

      QDomDocument doc;
      /* Récupère le texte dans le presse-papiers et vérifie la valididé de
       * l'en-tête XML et du type propre du document. */
      if (!doc.setContent(c->asXML()) || doc.doctype().name() != "CYLIXCell")
      {
        CYMessageBox::sorry(this, tr("The clipboard does not contain a valid display description."));
        return;
      }
      c->stopRefresh();

      QDomElement el = doc.documentElement();
      createFromDOM( mSheet, (QWidget *)parent(), el);
    }
  }
  delete cell;

  setModified(true);
}

void CYAnalyseCell::connectModifie(bool enable)
{
  mConnectModifie = enable;
  for (int i = 0; i < mCellsChild.size(); ++i)
  {
    mCellsChild.at(i)->connectModifie(enable);
  }

  if (enable)
    connect(this, SIGNAL(modified(bool)), mSheet, SLOT(setModified(bool)));
  else
    disconnect(this, SIGNAL(modified(bool)), mSheet, SLOT(setModified(bool)));
}

CYAnalyseCell *CYAnalyseCell::cellHasFocus()
{
  if ( mDisplay && (hasFocus() || mDisplay->hasFocus()) )
    return this;

  for (int i = 0; i < mCellsChild.size(); ++i)
  {
    CYAnalyseCell *c = mCellsChild.at(i)->cellHasFocus();
    if (c)
      return c;
  }

  return 0;
}

void CYAnalyseCell::setForcing(const bool val)
{
  CYDisplayCell::setForcing(val);

  for (int i = 0; i < mCellsChild.size(); ++i)
  {
    mCellsChild.at(i)->setForcing(val);
  }
}

int CYAnalyseCell::showPopupMenu(QMenu *pm0, int index)
{
  QMenu *pm;
  if (pm0)
  {
    if (index==0)                                     // Création d'un sous-menu surgissant
    {
      pm = pm0->addMenu(tr("Analyse cell")); index++;
    }
    else                                              // Ajout d'entrées dans un menu surgissant
      pm = pm0;
  }
  else                                                // Création d'un menu surgissant
  {
    pm = new QMenu(tr("Analyse cell")); index++;
    pm->addSeparator();
  }

  if (core && sheet() && !sheet()->isProtected())
  {
    pm->addAction(core->loadIconSet("cyedit-copy.png"      , 22), tr("Copy" )         , this, SLOT( copyDisplay()));
    pm->addAction(core->loadIconSet("cyedit-paste.png"     , 22), tr("Paste")         , this, SLOT( pasteDisplay()));
    pm->addSeparator();

    pm->addAction(core->loadIconSet("cyview_right_new.png", 22), tr("Add a cell on the right" ), this, SLOT( horizontalSplit()));
    pm->addAction(core->loadIconSet("cyview_bottom_new.png", 22), tr("Add a cell below"        ), this, SLOT( verticalSplit()  ));
    if ( mCellParent )
      pm->addAction(core->loadIconSet("cyedit-delete.png", 22), tr("Remove this cell"      ), this, SLOT( remove()         ));
    pm->addSeparator();
  }

  if (mSheet)
  {
    index = mSheet->showPopupMenu(pm, 0);
  }

  if (!pm0)
    pm->exec(QCursor::pos());

  return index;
}

CYAnalyseCell * CYAnalyseCell::findCell(QString position)
{
  CYAnalyseCell *cell = this;

  if (!cell)
    return 0;

  if (position.isEmpty())
    return cell;
  QString section = position.section('/', 0, 0);
  if (section.isEmpty())
  {
    return cell;
  }
  else
  {
    int num = section.toInt();
    cell = cell->cellChild(num-1);
    position = position.remove(0, section.length()+1);
  }

  if (cell && !position.isEmpty())
  {
    return cell->findCell(position);
  }
  else
    return cell;
}

/*! @return la cellule enfant correspondant à l'index \a id dans la liste.
    \fn CYAnalyseCell::cellChild(int id)
 */
CYAnalyseCell * CYAnalyseCell::cellChild(int id)
{
  if (mCellsChild.count()==0)
    return 0;
  return mCellsChild.at(id);
}


/*! Création d'un nouveau CYDisplaySimple sans donnée
    \fn CYAnalyseCell::newDisplaySimple()
 */
void CYAnalyseCell::newDisplaySimple()
{
  setDisplay(new CYDisplaySimple(this, "CYDisplaySimple"));
}


/*! Création d'un nouveau CYMultimeter analogique sans donnée
    \fn CYAnalyseCell::newAnalogMultimeter()
 */
void CYAnalyseCell::newAnalogMultimeter()
{
  setDisplay(new CYMultimeter(this, "CYMultiMeter", 0, false, true, false));
}


/*! Création d'un nouveau CYMultimeter LCD sans donnée
    \fn CYAnalyseCell::newLCDMultimeter()
 */
void CYAnalyseCell::newLCDMultimeter()
{
  setDisplay(new CYMultimeter(this, "CYMultiMeter", 0, false, false, true));
}


/*! Création d'un nouveau CYScope sans donnée
    \fn CYAnalyseCell::newScope()
 */
void CYAnalyseCell::newScope()
{
  setDisplay(new CYScope(this, "CYScope"));
}


/*! Création d'un nouveau CYScopeMultiple sans donnée
    \fn CYAnalyseCell::newScopeMultiple()
 */
void CYAnalyseCell::newScopeMultiple()
{
  setDisplay(new CYScopeMultiple(this, "CYScopeMultiple"));
}


/*! Création d'un nouveau CYBargraph sans donnée
    \fn CYAnalyseCell::newBargraph()
 */
void CYAnalyseCell::newBargraph()
{
  setDisplay(new CYBargraph(this, "CYBarGraph"));
}


/*! Création d'un nouveau CYDatasTable sans donnée
    \fn CYAnalyseCell::newDatasTable()
 */
void CYAnalyseCell::newDatasTable()
{
  setDisplay(new CYDatasTable(this, "CYDatasTable"));
}


/*! @return \a true si la cellule est en mode forçage.
    \fn CYAnalyseCell::forcing()
 */
bool CYAnalyseCell::forcing()
{
/*  if (mSheet)
    return mSheet->forcing();*/
  return CYDisplayCell::forcing();
}
