//
// C++ Implementation: cyanalysesheettemplate
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyanalysesheettemplate.h"

// CYLIBS
#include "cy.h"
#include "cyanalysespace.h"

CYAnalyseSheetTemplate::CYAnalyseSheetTemplate(QWidget *parent, const QString &name)
 : CYAnalyseSheet(parent, name)
{
  hide();
}


CYAnalyseSheetTemplate::~CYAnalyseSheetTemplate()
{
}

void CYAnalyseSheetTemplate::loadAnalyseSheet()
{
  CYAnalyseSpace *as = (CYAnalyseSpace *)parent();
  CYAnalyseSheet *newSheet = as->loadAnalyseSheet(mFileName);
  QHashIterator<QString, QString*> it(mDisplayTitle);
  it.toFront();
  while (it.hasNext())
  {
    it.next();
    newSheet->setDisplayTitle(it.key(), *(it.value()));
  }
}

void CYAnalyseSheetTemplate::setDisplayTitle(QString position, QString title)
{
  mDisplayTitle.insert( position, new QString(title) );
}

