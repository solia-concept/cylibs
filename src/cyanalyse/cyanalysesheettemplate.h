//
// C++ Interface: cyanalysesheettemplate
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYANALYSESHEETTEMPLATE_H
#define CYANALYSESHEETTEMPLATE_H

// QT
#include <QHash>
// CYLIBS
#include <cyanalysesheet.h>

/**
@short Modèle de feuille d'analyse.
Un modèle de feuille d'analyse est une feuille d'analyse installée sur le système et accessible en lecture à partir de l'outil d'analyse en tant que modèle. Une fois modifiée, l'utilisateur s'il le veut peut l'enregistrée sous un autre nom de fichier.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYAnalyseSheetTemplate : public CYAnalyseSheet
{
Q_OBJECT
public:
    CYAnalyseSheetTemplate(QWidget *parent = 0, const QString &name = 0);

    ~CYAnalyseSheetTemplate();

    /** Saisie le titre de l'afficheur de la cellule correspondant à \a position.
        Ceci est utile pour la traduction après chargement d'une feuille système
        @param position La position est donnée par une chaîne de caractères composée du numéro de ligne et du numéro de colonne de la cellule sépararés par un '/'.
        @see CYAnalyseSheet::findCell(QString position) */
    void setDisplayTitle(QString position, QString title);

public slots: // Public slots
  /** Chargement d'une feuille d'analyses. */
  void loadAnalyseSheet();

protected:
    QHash<QString, QString*> mDisplayTitle;
};

#endif
