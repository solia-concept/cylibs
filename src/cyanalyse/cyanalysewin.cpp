/***************************************************************************
                          cyanalysewin.cpp  -  description
                             -------------------
    début                  : mar fév 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyanalysewin.h"

// QT
#include <QMenuBar>
#include <QIcon>
#include <QList>
#include <QTimerEvent>
#include <QMenu>
#include <QToolBar>
// CYLIBS
#include "cy.h"
#include "cycore.h"
#include "cydisplaystyle.h"
#include "cydatasbrowser.h"
#include "cyanalysespace.h"
#include "cyeventspanel.h"
#include "cyaction.h"
#include "cyactionmenu.h"

CYAnalyseWin::CYAnalyseWin(const QString &name, int index, const QString &label)
  : CYWin(0, name, index, label)
{
//   QBoxLayout * l = new QVBoxLayout( this );

  splitter = new QSplitter(this);
  Q_CHECK_PTR(splitter);
  splitter->setOrientation(Qt::Horizontal);
  splitter->setOpaqueResize(true);
  setCentralWidget(splitter);
//   l->addWidget( splitter );

  mBrowser = new CYDatasBrowser(splitter, "CYDatasBrowser");
  Q_CHECK_PTR(mBrowser);

  as = new CYAnalyseSpace(this, splitter, "CYAnalyseSpace");
  Q_CHECK_PTR(as);

  connect(as, SIGNAL(announceRecentURL(const QUrl&)), this, SLOT(registerRecentURL(const QUrl&)));
  connect(as, SIGNAL(newCaption(const QString&, bool)), this, SLOT(setWindowTitle(const QString&, bool)));

  new CYActionMenu( tr("&File"), 0, actionCollection(), "file");
  new CYAction(tr("&New"        ), "cydocument-new"        , QKeySequence::New   , as     , SLOT(newAnalyseSheet())            , actionCollection(), "file_new");
  new CYAction(tr("&Open"       ), "cydocument-open"       , QKeySequence::Open  , as     , SLOT(loadAnalyseSheet())           , actionCollection(), "file_open");
//  new CYAction(tr("Open &Recent"), "cydocument-open-recent", 0                   , as     , SLOT(loadAnalyseSheet(const QString& )), actionCollection(), "file_open_recent");
  mSaveAction =
      new CYAction(tr("Save"           ), "cydocument-save"    , QKeySequence::Save   , this   , SLOT(saveAll())            , actionCollection(), "file_save");
  mSaveAsAction =
      new CYAction(tr("Save &As..."    ), "cydocument-save-as" , QKeySequence::SaveAs, as     , SLOT(saveAnalyseSheetAs()) , actionCollection(), "file_save_as");
  new CYAction(tr("&Close"      ), "cydocument-close"      , QKeySequence::Close , as     , SLOT(deleteAnalyseSheet())         , actionCollection(), "file_close");
  new CYAction(tr("&Quit"       ), "cywindow-close"        , QKeySequence::Quit  , this   , SLOT(close())                      , actionCollection(), "quit");
  mTemplateAction = new CYActionMenu( tr("Template..."), "cydocument-open-folder", actionCollection(), "template");
  mPauseAction = new CYAction(tr("Pause"), "cyplayer_pause" , 0, this, SLOT(enableAnalyseRefresh()), actionCollection(), "pause");

  new CYActionMenu( tr("&Edit"), 0, actionCollection(), "edit");
  new CYAction(tr("Cu&t"        ), "cyedit-cut"    , QKeySequence::Cut   , as     , SLOT(cut())                 , actionCollection(), "edit_cut");
  new CYAction(tr("&Copy"       ), "cyedit-copy"   , QKeySequence::Copy  , as     , SLOT(copy())                , actionCollection(), "edit_copy");
  new CYAction(tr("&Paste"      ), "cyedit-paste"  , QKeySequence::Paste , as     , SLOT(paste())               , actionCollection(), "edit_paste");
  new CYAction(tr("&Analyse sheet properties..."), "cyconfigure", 0, as  , SLOT(configure()), actionCollection(), "configure_sheet");

  new CYActionMenu( tr("&Settings"), 0, actionCollection(), "settings");
  new CYAction(tr("Configure &Style...")         , "cyformat-stroke-color" , 0, this, SLOT(editStyle()), actionCollection(), "configure_style");

  new CYAction(tr("&Capture"), "camera" , 0, as, SLOT(captureAnalyseSheet()), actionCollection(), "capture");

}

CYAnalyseWin::~CYAnalyseWin()
{
}

void CYAnalyseWin::createGUI(const QString &xmlfile)
{
  CYWin::createGUI(xmlfile);
}

void CYAnalyseWin::loadAnalyseSheet(const QString& fileName)
{
  as->loadAnalyseSheet(fileName);
}

void CYAnalyseWin::removeAnalyseSheet(const QString& fileName)
{
  as->deleteAnalyseSheet(fileName);
}

void CYAnalyseWin::showStatusBar()
{
//  if (mStatusBarTog->isChecked())
//  {
//    statusBar()->show();
//    if (timerId == -1)
//    {
//      timerId = startTimer(2000);
//    }
//    // appel de timerEvent pour remplir la barre d'état avec les valeurs réelles
//    timerEvent(0);
//  }
//  else
//  {
//    statusBar()->hide();
//    if (timerId != -1)
//    {
//      killTimer(timerId);
//      timerId = -1;
//    }
//  }
}

void CYAnalyseWin::editStyle()
{
  if (!core)
    return;
  core->style->configure(this);
}

void CYAnalyseWin::customEvent(QEvent *)
{
//  if (ev->type() == QEvent::User)
//  {
//    /* Due à la communication asynchrone entre cylix et ses correspondants,
//     * nous devons parfois montrer des boîtes de message qui sont activées
//     * par des objets qui sont déjà morts. */
//    CYMessageBox::error(this, *((QString*) ev->data()));
//    delete (QString*) ev->data();
//  }
}

void CYAnalyseWin::timerEvent(QTimerEvent*)
{
}

bool CYAnalyseWin::queryClose()
{
  return saveAll(true);
}

void CYAnalyseWin::readProperties(QSettings* cfg)
{
  CYWin::readProperties(cfg);

  QList<QVariant> varList= cfg->value("SplitterSizeList").toList();
  QList<int> sizes;
  foreach(QVariant var, varList)
    sizes << var.toInt();

  if (sizes.isEmpty())
  {
    // commence avec un rapport de 30/70
    sizes.append(30);
    sizes.append(70);
  }
  splitter->setSizes(sizes);

  as->readProperties(cfg);

  // TODO KDE3->QT3
//  openRecent->loadEntries(cfg);

//  as->connectModifie(true);
}

void CYAnalyseWin::saveProperties(QSettings* cfg)
{
  CYWin::saveProperties(cfg);

  QList<QVariant> varList;
  QList<int> sizes;
  foreach(int size, splitter->sizes())
    varList << size;
  cfg->setValue("SplitterSizeList", varList);

  as->saveProperties(cfg);
}

bool CYAnalyseWin::saveAll(bool quit)
{
  if (!dontSaveSession)
  {
    if (!as->saveAllSheets(quit))
      return false;
    saveProperties(core->OSUserConfig());
  }
  return true;
}

void CYAnalyseWin::setModified(bool val)
{
  mSaveAction->setEnabled(val);
}

void CYAnalyseWin::connectModifie(bool )
{
  as->connectModifie(true);
}


CYAnalyseSheetTemplate *CYAnalyseWin::addTemplate(QString title, QString file)
{
  CYAnalyseSheetTemplate *ast = new CYAnalyseSheetTemplate(as, title);
  ast->setFileName(file.toUtf8());
  mTemplates.insert(mTemplates.count()+1, ast);
  mTemplateAction->insert( new CYAction(title, 0, 0, ast, SLOT(loadAnalyseSheet()), actionCollection(), QString("template_%1").arg(mTemplates.count()) ) );
  return ast;
}

/*!
    \fn CYAnalyseWin::capture()
 */
void CYAnalyseWin::capture()
{

}

void CYAnalyseWin::enableAnalyseRefresh()
{
  core->enableAnalyseRefresh();
  if (core->analyseRefreshEnabled())
  {
    mPauseAction->setText(tr("Pause"));
    // TODO KDE3->QT3
//    mPauseAction->setIcon("cyplayer_pause");
  }
  else
  {
    mPauseAction->setText(tr("Refresh"));
    // TODO KDE3->QT3
//    mPauseAction->setIcon("cystart");
  }
}

