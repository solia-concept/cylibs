//
// C++ Interface: cyeventspreferences
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYEVENTSPREFERENCES_H
#define CYEVENTSPREFERENCES_H

// CYLIBS
#include "cysettingsdialog.h"

namespace Ui {
    class CYEventsPreferences;
}

/**
@sort Boîte de configuration du gestionnaire d'événements.

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYEventsPreferences : public CYSettingsDialog
{
public:
  CYEventsPreferences(QWidget *parent=0, const QString &name=0);

  ~CYEventsPreferences();

  virtual void init();

private:
  Ui::CYEventsPreferences *ui;
};

#endif
