//
// C++ Implementation: cyeventspreferences
//
// Description: 
//
//
// Author: Géald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyeventspreferences.h"
#include "ui_cyeventspreferences.h"

// CYLIBS
#include "cycore.h"
#include "cyframe.h"
#include "cyeventsmanager.h"
#include "cypushbutton.h"
#include "cybuttongroup.h"
#include "cyuser.h"

CYEventsPreferences::CYEventsPreferences(QWidget *parent, const QString &name)
  : CYSettingsDialog(parent,name), ui(new Ui::CYEventsPreferences)
{
  ui->setupUi(this);

  init();
}


CYEventsPreferences::~CYEventsPreferences()
{
  delete ui;
}

void CYEventsPreferences::init()
{
  mButtonDefaults = ui->buttonDefaults;
  mButtonApply    = ui->buttonApply;
  mButtonCancel   = ui->buttonCancel;
  mListView       = ui->listView;
  mWidgetStack    = ui->widgetStack;

  CYSettingsDialog::init();
}

