/***************************************************************************
                          cyevent.cpp  -  description
                             -------------------
    début                  : mer aoû 20 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyevent.h"

// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cystring.h"
#include "cycolor.h"
#include "cyeventsmanager.h"
#include "cyeventsgenerator.h"
#include "cyeventreport.h"
#include "cyeventslist.h"
#include "cycommand.h"
#include "cyuser.h"

CYEvent::CYEvent(CYEventsGenerator *generator, const QString &name, QString desc, QColor color, flg *val, QString help)
  : CYFlag(generator->db(), name, val, *val),
    mEnableState(true),
    mProcessing(SCR),
    mDesc(desc),
    mSender("")
{
  mGenerator = generator;
  mEnableColor = color;
  setHelp(help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, const QString &name, flg *val, bool enable, int proc, QString desc, QString from, QString refs, QString help, QColor color)
  : CYFlag(generator->db(), name, val, *val),
    mEnableState(enable),
    mProcessing(proc),
    mDesc(desc),
    mSender(from)
{
  mGenerator = generator;
  mEnableColor = color;

  if (!core)
    return;
  setRefs(refs);
  setHelp(help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, Attr1 *attr, flg *val)
  : CYFlag(generator->db(), attr->name, val),
    mEnableState(true),
    mProcessing(SCR),
    mDesc(attr->desc),
    mSender("")
{
  mGenerator = generator;
  mEnableColor = attr->color;
  if (!core)
    return;
  setRefs(attr->refs);
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, const QString &name, Attr1 *attr, flg *val)
  : CYFlag(generator->db(), name, val),
    mEnableState(true),
    mProcessing(SCR),
    mDesc(attr->desc),
    mSender("")
{
  mGenerator = generator;
  mEnableColor = attr->color;
  if (!core)
    return;
  setRefs(attr->refs);
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, Attr2 *attr)
  : CYFlag(generator->db(), attr->name, attr->val, *attr->val),
    mEnableState(attr->enable),
    mProcessing(attr->proc),
    mDesc(attr->desc),
    mSender(attr->from)
{
  mGenerator = generator;
  mEnableColor = Qt::white;
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, const QString &name, Attr2 *attr)
  : CYFlag(generator->db(), name, attr->val, *attr->val),
    mEnableState(attr->enable),
    mProcessing(attr->proc),
    mDesc(attr->desc),
    mSender(attr->from)
{
  mGenerator = generator;
  mEnableColor = Qt::white;
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, Attr3 *attr)
  : CYFlag(generator->db(), attr->name, attr->val, *attr->val),
    mEnableState(attr->enable),
    mProcessing(attr->proc),
    mDesc(attr->desc),
    mSender(attr->from)
{
  mGenerator = generator;
  mEnableColor = Qt::white;
  if (!core)
    return;
  setRefs(attr->refs);
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, const QString &name, Attr3 *attr)
  : CYFlag(generator->db(), name, attr->val, *attr->val),
    mEnableState(attr->enable),
    mProcessing(attr->proc),
    mDesc(attr->desc),
    mSender(attr->from)
{
  mGenerator = generator;
  mEnableColor = Qt::white;
  if (!core)
    return;
  setRefs(attr->refs);
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, Attr4 *attr)
  : CYFlag(generator->db(), attr->name, attr->val, *attr->val),
    mEnableState(attr->enable),
    mProcessing(attr->proc),
    mDesc(attr->desc),
    mSender(attr->from)
{
  mGenerator = generator;
  mEnableColor = attr->color;
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, const QString &name, Attr4 *attr)
  : CYFlag(generator->db(), name, attr->val, *attr->val),
    mEnableState(attr->enable),
    mProcessing(attr->proc),
    mDesc(attr->desc),
    mSender(attr->from)
{
  mGenerator = generator;
  mEnableColor = attr->color;
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, Attr5 *attr)
  : CYFlag(generator->db(), attr->name, attr->val, *attr->val),
    mEnableState(attr->enable),
    mProcessing(attr->proc),
    mDesc(attr->desc),
    mSender(attr->from)
{
  mGenerator = generator;
  mEnableColor = attr->color;
  if (!core)
    return;
  setRefs(attr->refs);
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(CYEventsGenerator *generator, const QString &name, Attr5 *attr)
  : CYFlag(generator->db(), name, attr->val, *attr->val),
    mEnableState(attr->enable),
    mProcessing(attr->proc),
    mDesc(attr->desc),
    mSender(attr->from)
{
  mGenerator = generator;
  mEnableColor = attr->color;
  if (!core)
    return;
  setRefs(attr->refs);
  setHelp(attr->help);
  init();
}

CYEvent::CYEvent(const QString &name, flg *val, bool enable, int proc, QString desc, QString from, QString refs, QString help)
  : CYFlag(core->events->db(), name, val, *val),
    mEnableState(enable),
    mProcessing(proc),
    mDesc(desc),
    mSender(from)
{
  mEnableColor = Qt::white;
  mGenerator = 0;
  if (!core)
    return;
  setRefs(refs);
  setHelp(help);
  init();
}

CYEvent::CYEvent(CYCommand *command,const QString &name, flg *val, bool enable, int proc, QString desc, QString from, QString refs, QString help, QColor color)
  : CYFlag(core->events->db(), name, val, *val),
    mEnableState(enable),
    mProcessing(proc),
    mDesc(desc),
    mSender(from)
{
  mEnableColor = color;
  mGenerator = 0;
  command->setEvent( this );
  if (!core)
    return;
  setRefs(refs);
  setHelp(help);
  init();
}

CYEvent::~CYEvent()
{
}

void CYEvent::init()
{
  mEnable       = false;
  mState        = false;
  mOldState     = false;
  mLocal        = false;
  mPrefix       = "";
  mSuffix       = "";
  mCydocEnable     = true;
  setEnableColor(mEnableColor);

  if (mDesc.isEmpty())
    mDesc = QString(QString("%1").arg(objectName()));

  mReportUp     = tr("Event");
  mReportEnd    = tr("End");
  mReportAcknow = tr("Acknow.");
  mGroup        = mReportUp;
  mLabel = mDesc;
  mAuthorizationFlag = 0;
  mIndexStation = -1;
}

void CYEvent::setEnableColor(QColor color)
{
  mEnableColor  = color;
  mDisableColor = mEnableColor.darker(150);
}

QString CYEvent::desc()
{
  QString res = mDesc;
  int nbRefs = mDatasRef.count();
  if (nbRefs>0)
  {
    for ( int i=1; i<=nbRefs; i++ )
    {
      int nb_DATAi = res.indexOf(QString("DATA%1").arg(i));
      int nb_DATA_RECi = res.indexOf(QString("DATA_REC%1").arg(i));

      if ( (nb_DATAi==-1) && (nb_DATA_RECi==-1) )
      {
        CYFATALTEXT(QString("CYEvent::desc(): %1 => impossible de trouver DATA%2 pour %3 dans la description").arg(objectName()).arg(i).arg(mDatasRef[i-1]->objectName()));
      }

      if (nb_DATAi>-1)
      {
        res.replace(QString("DATA%1").arg(i), stringRef(mDatasRef[i-1]));
      }
      else
      {
        QString *txt = mStringsRef[i-1];
        if (!txt) // Sauvegarde de la chaîne de caractères
        {
          txt = new QString(stringRef(mDatasRef[i-1]));
          mStringsRef.insert(i-1, txt);
        }
        res.replace(QString("DATA_REC%1").arg(i), *txt);
      }
    }
  }

  return res;
}

void CYEvent::read()
{
  if (mVal==0)
    return;

  if (mAuthorizationFlag && !mAuthorizationFlag->val())
    return;

  if (!mGenerator->netIsOk() && !core->simulation() && !mLocal)
    return;

  if (((*mVal) && mEnableState) || (!(*mVal) && !mEnableState))
  {
    mState = true;
  }
  else
  {
    mState = false;
  }

  if (mState && (mProcessing & SCR))
  {
    mEnable = true;
  }
  else
    mEnable = false;

  if (!(mProcessing & REC))
    return;

  if (mState == mOldState)
    return;

  mOldState = mState;

  if (mState) // Activation de l'évènment
    mStringsRef.clear(); // RAZ des anciennes chaînes de caractères DATAi_REC.

  if (!mState && !(mProcessing & BST))
    return;

  if (mProcessing & RAZ)
    *mVal = !(flg)mEnableState;

  addToEventsReport();
}

void CYEvent::addToEventsReport()
{
  CYEventReport *report=0;

  if (mState)
  {
    mReportUpDateTime = QDateTime::currentDateTime();
    report = new CYEventReport(this, objectName(), mEnableColor, mReportUpDateTime, QDateTime(), mReportUp, sender(), message(true));
  }
  else if (message(true).contains('!'))
  {
    report = new CYEventReport(this, objectName(), mDisableColor, QDateTime::currentDateTime(), mReportUpDateTime, mReportAcknow, sender(), message(true));
  }
  else
  {
    report = new CYEventReport(this, objectName(), mDisableColor, QDateTime::currentDateTime(), mReportUpDateTime, mReportEnd, sender(), message(true));
  }

  report->initIndexStation(mIndexStation);
  core->events->addReport(report);

  if ( mustToBeMailing() )
    core->events->addReportToMail( report );
}

QString CYEvent::stringRef(CYData *data)
{
  switch (data->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *tmp = (CYFlag *)data;
                      return tmp->toString();
                    }
    case Cy::VS8    :
                    {
                      CYS8 *tmp = (CYS8 *)data;
                      return tmp->toString();
                    }
    case Cy::VS16   :
                    {
                      CYS16 *tmp = (CYS16 *)data;
                      return tmp->toString();
                    }
    case Cy::VS32   :
                    {
                      CYS32 *tmp = (CYS32 *)data;
                      return tmp->toString();
                    }
    case Cy::VS64   :
                    {
                      CYS64 *tmp = (CYS64 *)data;
                      return tmp->toString();
                    }
    case Cy::VU8    :
                    {
                      CYU8  *tmp = (CYU8  *)data;
                      return tmp->toString();
                    }
    case Cy::VU16   :
                    {
                      CYU16 *tmp = (CYU16 *)data;
                      return tmp->toString();
                    }
    case Cy::VU32   :
                    {
                      CYU32 *tmp = (CYU32 *)data;
                      return tmp->toString();
                    }
    case Cy::VU64   :
                    {
                      CYU64 *tmp = (CYU64 *)data;
                      return tmp->toString();
                    }
    case Cy::VF32   :
                    {
                      CYF32 *tmp = (CYF32 *)data;
                      return tmp->toString();
                    }
    case Cy::VF64   :
                    {
                      CYF64 *tmp = (CYF64 *)data;
                      return tmp->toString();
                    }
    case Cy::Time   :
                    {
                      CYTime *tmp = (CYTime *)data;
                      return tmp->toString();
                    }
    case Cy::Sec    :
                    {
                      CYTSec *tmp = (CYTSec *)data;
                      return tmp->toString();
                    }
    case Cy::String :
                    {
                      CYString *tmp = (CYString *)data;
                      return tmp->toString();
                    }
    case Cy::Color :
                    {
                      CYColor *tmp = (CYColor *)data;
                      return tmp->toString();
                    }
    default         : CYWARNINGTEXT(objectName()+", "+data->objectName());
  }
  return 0;
}

QString CYEvent::message(bool oneLine)
{
  QString msg = prefix()+desc()+suffix();
  if (oneLine)
    msg = msg.replace("\n", " ");
  return msg;
}


/*! Saisie les noms des données de référence séparés par une virgule
    \fn CYEvent::setRefs(QString refs)
 */
void CYEvent::setRefs(QString refs)
{
  if (refs.isEmpty())
    return;
  refs = refs.remove(' ');
  int nb = refs.count(',');
  if (nb>8)
    CYFATALTEXT(QString("%1 DONNEE(S) DE REFERENCE POUR L'EVENEMENT %2 EN TROP").arg(nb-9).arg(objectName()));
  for ( int i=0; i<=nb; i++ )
  {
    QString dataName = refs.section(',', i, i);
    CYData *data = core->findData(dataName, false);
    if (!data)
      CYFATALTEXT(QString("IMPOSSIBLE DE TROUVER LA DONNEE DE REFERENCE %1 POUR L'EVENEMENT %2").arg(dataName).arg(objectName()));
    mDatasRef.insert(i, data);
  }
}


/*!
    \fn CYEvent::mustToBeMailing()
 */
bool CYEvent::mustToBeMailing()
{
  if ( mList->enableMailing->val() )
    return true;
  return false;
}


/*!
    \fn CYEvent::setList( CYEventsList * list )
 */
void CYEvent::setList( CYEventsList * list )
{
  mList = list;
}


/*! Saisie le générateur d'évènement de cet évènement.
    \fn CYEvent::setGenerator( CYEventsGenerator *generator )
 */
void CYEvent::setGenerator( CYEventsGenerator *generator )
{
  mGenerator = generator;
}


/*! @erturn la provenance de l'évènement. Si celui-ci vaut tr("Operator") ou tr("User") alors c'est le nom de l'utilisateur courant qui est retourné.
    \fn CYEvent::sender()
 */
QString CYEvent::sender()
{
  if ( (mSender==tr("Operator")) || (mSender==tr("User")) )
  {
    if (core->user())
      return core->user()->title();
  }
  return mSender;
}


void CYEvent::setSender(QString from)
{
  mSender = from;
}


void CYEvent::setAuthorizationFlag(CYFlag *flag)
{
  mAuthorizationFlag = flag;
}

void CYEvent::setCydoc(int index, int index_ref, QString note)
{
  addNote(note);
  mCydocEnable = (index==index_ref) ? true : false;
}
