/***************************************************************************
                          cyeventsreport.h  -  description
                             -------------------
    début                  : jeu aoû 21 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYEVENTSREPORT_H
#define CYEVENTSREPORT_H

// QT
#include <QObject>
#include <QList>
#include <QDir>

class CYEventReport;

/** @short Journal d'évènements.
  * @author Gérald LE CLEACH
  */

class CYEventsReport : public QObject
{
  Q_OBJECT

public: 
  CYEventsReport(QObject *parent=0, const QString &name=0, QString path=0, bool useNewFile=false, int projectId=0);
  ~CYEventsReport();

  /** Journalisation d'un évènement. */
  void addReport(CYEventReport *report=0);

  /** @return le fichier de journalisation. 
      @param newFile Force la création d'un nouveau fichier de journalisation*/
  QFile *findFile(bool newFile=false);
  /** @return le nouveau fichier de journalisation. */
  QFile *newFile(QString filename);

  /** @return le nom du fichier de journalisation. */
  QString fileName() { return mFileName; }
  /** Saisie le nom du fichier de journalisation. */
  void setFileName(const QString &filename) { mFileName = filename;}

  /** @return le chemin du répertoire d'archivage. */
  QString path();
  /** Saisie le chemin du répertoire d'archivage. */
  void setPath(const QString &path);

  /** @return la date et heure de chargement du projet en texte. */
  QString loadTimeToString();

//  /** @return la date et heure de chargement du projet . */
//  QDateTime loadTime() { return mLoadTime; }
//  /** Saisie la date et heure de chargement du projet . */
//  void setLoadTime(QDateTime time) { mLoadTime = time; }
//  /** @return la date et heure de chargement du projet en texte. */
//  QString loadTimeToString() { return mLoadTime.toString(Qt::ISODate).replace(QChar('T'), " / " ); }
//  /** Saisie la date et heure de chargement du projet en texte. */
//  void loadTimeFromString(const QString &time) { mLoadTime = QDateTime::fromString(time, Qt::ISODate); }
//
//  /** @return la date et heure de fermeture du projet . */
//  QDateTime closeTime() { return mCloseTime; }
//  /** Saisie la date et heure de fermeture du projet . */
//  void setCloseTime(QDateTime time) { mCloseTime = time; }
//  /** @return la date et heure de fermeture du projet en texte. */
//  QString closeTimeToString() { return mCloseTime.toString(Qt::ISODate).replace(QChar('T'), " / " ); }
//  /** Saisie la date et heure de fermeture du projet en texte. */
//  void closeTimeFromString(const QString &time) { mCloseTime = QDateTime::fromString(time, Qt::ISODate); }

  /** Met à jour le répertoire d'archivage. */
  void updateDir();
  /** Charge le journal d'évènements. */
  void load(QString filename);
  /** Charge le journal d'évènements. */
  void load();

signals: // Signals
  /** Emis dès la création d'un nouveau fichier d'évènements.
    * @param projectId  Index de projet.*/
  void newEventsReportFile(QString fileName, int projectId);

public: // Public attributes
  /** Liste de journalisations. */
  QList<CYEventReport*> reports;

protected: // Protected attributes
  /** Index de projet.*/
  int mProjectId;
  /** Répertoire d'archivage. */
  QDir *mDir;  
  /** Chemin du répertoire d'archivage. */
  QString mPath;  
  /** Nom du fichier de journalisation courrant. */
  QString mFileName;
  /** Numéro du fichier de journalisation courrant. */
  uint mFileNum;
  /** Indique s'il faut utiliser un nouveau fichier de journalisation. */
  bool mUseNewFile;
//  /** Date et heure de chargement du projet . */
//  QDateTime mLoadTime;
//  /** Date et heure de fermeture du projet . */
//  QDateTime mCloseTime;

};

#endif
