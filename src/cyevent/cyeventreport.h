/***************************************************************************
                          cyeventreport.h  -  description
                             -------------------
    begin                : ven nov 21 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYEVENTREPORT_H
#define CYEVENTREPORT_H

// QT
#include <qobject.h>
#include <qcolor.h>
#include <qdatetime.h>

/** @short Journalisation d'évènement.
  * @author Gérald LE CLEACH
  */

class CYEventReport : public QObject
{
  Q_OBJECT
public:
  /** Construction d'une journalisation d'évènement.
    * @param parent     parent.
    * @param name       name.
    * @param color      couleur.
    * @param time       date et heure de journalisation.
    * @param since      date et heure de début.
    * @param type       type.
    * @param from       provenance.
    * @param desc       desc.
    */
  CYEventReport(QObject *parent=0, const QString &name=0, QColor color=Qt::white, QDateTime time=QDateTime(), QDateTime since=QDateTime(), QString type=0, QString from=0, QString desc=0);

  ~CYEventReport();

  /** @return la couleur d'affichage. */
  QColor color() { return mColor; }
  /** @return la date et heure de journalisation de l'évènement. */
  QDateTime dateTime() { return mDateTime; }
  /** @return la date et heure de début de l'évènement. */
  QDateTime sinceDateTime() { return mSinceDateTime; }
  /** @return le type de journalisation. */
  QString type() { return mType; }
  /** @return la provenance. */
  QString from() { return mSender; }
  /** @return la description. */
  QString desc() { return mDesc; }
  /** @return la chaîne de caractères à sauvegarder dans le journal d'évènements. */
  QString record( bool withColor = true );

  /**
   * @brief Réserve l'exclusivité de l'évènement à un poste.
   * Par défaut les évènements ne sont pas rattachés à un poste et leur journalisation se fait alors
   * dans les journaux d'évènements de chaque projet chargé par poste.
   * @param index Index de poste pour lequel l'événement doit être réservé.
   */
  inline void initIndexStation(int index) { mIndexStation = index; }
  /**
   * @return l'index du poste pour lequel l'évènement est réservé.
   * Par défaut vaut -1. L'événement n'est alors réservé à aucun poste et sa journalisation se fait alors
   * dans les journaux d'évènements de chaque projet chargé par poste.
   */
  inline int indexStation() { return mIndexStation; }

protected: // Protected attributes
  /** Couleur d'affichage. */
  QColor mColor;
  /** Date et heure de début de journalisation de l'évènement. */
  QDateTime mDateTime;
  /** Date et heure de début de l'évènement. */
  QDateTime mSinceDateTime;
  /** Type de journalisation. */
  QString mType;
  /** Provenance. */
  QString mSender;
  /** Description. */
  QString mDesc;
  /** Index de poste associé à cet évènement. */
  int mIndexStation;
};

#endif
