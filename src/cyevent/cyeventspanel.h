//
// C++ Interface: cyeventspanel
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYEVENTSPANEL_H
#define CYEVENTSPANEL_H

#include <cyframe.h>

/** Bandeau de messagerie
  * Pour refaire un bandeau personnalisé il suffit d'enrichir cette fonction dans la classe fille (Core)
  * en appelant un autre bandeau créé dans l'affaire à partir du modèle CYEventsPanel.ui.
	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYEventsPanel : public CYFrame
{
Q_OBJECT
public:
    CYEventsPanel(QWidget *parent = 0, const QString &name = 0);

    ~CYEventsPanel();

public slots:
    virtual void acknowledge();
    virtual void postMortemBackup();
};

#endif
