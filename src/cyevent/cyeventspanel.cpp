//
// C++ Implementation: cyeventspanel
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyeventspanel.h"
#include "cycore.h"

CYEventsPanel::CYEventsPanel(QWidget *parent, const QString &name)
 : CYFrame(parent, name)
{
  mTimer->start(250);
}


CYEventsPanel::~CYEventsPanel()
{
}

/*! Acquitement des défauts et alertes à acquiter.
    \fn CYEventsPanel::acknowledge()
 */
void CYEventsPanel::acknowledge()
{
  core->acknowledge();
}

/*!  Archivage du post-mortem.
    \fn CYEventsPanel::acknowledge()
 */
void CYEventsPanel::postMortemBackup()
{
  core->postMortemBackup();
}
