/***************************************************************************
                          cyeventsfileinfo.h  -  description
                             -------------------
    begin                : ven mar 5 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYEVENTSFILEINFO_H
#define CYEVENTSFILEINFO_H

// QT
#include <qfileinfo.h>
#include <QDateTime>

/** @short Information sur un fichier de journal d'évènements.
  * @author LE CLÉACH Gérald
  */

class CYEventsFileInfo : public QFileInfo
{
public:
  CYEventsFileInfo();
  CYEventsFileInfo(const QString &file);
  CYEventsFileInfo(const QFile &file);
  CYEventsFileInfo(const QDir &d, const QString &fileName);
  CYEventsFileInfo(const QFileInfo &fi);
  ~CYEventsFileInfo();

  /** Saisie l'index du projet auquel il appartient. */
  void setProjectId(int id) { mProjectId = id; }
  /** @retur l'index du projet auquel il appartient. */
  int projectId() { return mProjectId; }
  /** Saisie la date et l'heure de création. */
  void setCreatedTime(QDateTime time) { mCreatedTime = time; }
  /** Saisie la date et l'heure de création. */
  void setCreatedTime(const QString &time) { mCreatedTime = QDateTime::fromString(time, Qt::ISODate); }
  /** @return la date et l'heure de création. */
  QDateTime createdTime();
  /** @return la date et l'heure de création du fichier sous forme
    * de caractères lisible pour l'utilisateur. */
  QString createdTimeToString();

  /** @return \a true si le fichier est archivé. */
  bool backup() { return mBackup; }
  /** Saisir \a true pour archiver le fichier. */
  void setBackup(bool val) { mBackup = val; }

private: // Private methods
  /** Initialisation des informations. */
  void init();
  
private: // Private attributes
  /** Index de projet.*/
  int mProjectId;
  /** Date et heure de création du fichier. */
  QDateTime mCreatedTime;
  /** Vaut \a true si le fichier est archivé. */
  bool mBackup;
};

#endif
