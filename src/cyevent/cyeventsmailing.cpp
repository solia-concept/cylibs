//
// C++ Implementation: cyeventsmailing
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyeventsmailing.h"

// CYLIBS
#include "cycore.h"
#include "cyeventreport.h"
#include "cydblibs.h"
#include "cystring.h"
#include "cyu8.h"
#include "cysmtp.h"

CYEventsMailing::CYEventsMailing(QObject *parent, const QString &name)
  : QObject(parent)
{
  setObjectName(name);
  mRecFlag = 0;

  mTimer = 0;
  connect( core, SIGNAL( testEventsMail() ), SLOT( test() ) );
}


CYEventsMailing::~CYEventsMailing()
{
}

/*! Saisie l'intervalle de temps minimum en (ms) entre chaque envoi.
    \fn CYEventsMailing::setTimer( int ms )
 */
void CYEventsMailing::setTimer( int ms )
{
  if (ms==0)
  {
    if (mTimer)
      disconnect(mTimer, SIGNAL(timeout()), this, SLOT(send()));
    delete mTimer;
    mTimer = 0;
  }
  mTimer = new QTimer(this);
  connect(mTimer, SIGNAL(timeout()), this, SLOT(send()));
  mTimer->start(ms);
}


/*! Saisie du flag activant l'enregistrement de la liste de messages d'évènements à envoyer.
    \fn CYEventsMailing::setRecFlag( CYFlag * flag )
 */
void CYEventsMailing::setRecFlag( CYFlag * flag )
{
  mRecFlag = flag;
  connect( mRecFlag, SIGNAL( valueRising(bool) ), SLOT( recFlagChanged(bool) ) );
}


/*! Ajoute la journalisation d'un évènement à la liste à envoyer par mail.
    \fn CYEventsMailing::addReport(CYEventReport *report)
 */
void CYEventsMailing::addReport(CYEventReport *report)
{
  if ( mTimer || (mRecFlag && mRecFlag->val()) )
    mList.append( report );
}


/*! Envoi la liste de messages par mail.
    \fn CYEventsMailing::send()
 */
void CYEventsMailing::send()
{
  CYString * sender   = (CYString *)core->findData( "CY_EVENTS_MAIL_FROM");
  CYString * receiver = (CYString *)core->findData( "CY_EVENTS_MAIL_TO");
  CYString * subject  = (CYString *)core->findData( "CY_EVENTS_MAIL_SUBJECT");
  CYString * body     = (CYString *)core->findData( "CY_EVENTS_MAIL_BODY");

  CYFlag * flag = (CYFlag * )core->findData( "CY_EVENTS_MAIL_ENABLE" );
  if ( flag->val() && !mList.isEmpty())
  {
    QString txt;
    foreach ( CYEventReport *report, mList)
      txt.append( report->record( false ) );
    body->setVal( txt );
    new CYSMTP( sender->val(), receiver->val(), subject->val(), body->val() );
    mList.clear();
  }
}


/*! Test la configuration faite par l'utilisateur. 
    \fn CYEventsMailing::test()
 */
void CYEventsMailing::test()
{
  CYString * sender   = (CYString *)core->findData( "CY_EVENTS_MAIL_FROM");
  CYString * receiver = (CYString *)core->findData( "CY_EVENTS_MAIL_TO");
  CYString * subject  = (CYString *)core->findData( "CY_EVENTS_MAIL_SUBJECT");
  CYString * body     = (CYString *)core->findData( "CY_EVENTS_MAIL_BODY");

  body->setVal( QDateTime::currentDateTime().toString(Qt::ISODate) );
  new CYSMTP( sender->val(), receiver->val(), subject->val(), body->val() );
}


/*! Traîtement à chaque changement d'état du flag d'enregistrement.
    \fn CYEventsMailing::recFlagChanged( bool risign )
 */
void CYEventsMailing::recFlagChanged( bool risign )
{
  if ( risign )  
  {
    // début de l'enregistrement
    mList.clear();
  }
  else
  {
    // fin de l'enregistrement
    send();
  }
}
