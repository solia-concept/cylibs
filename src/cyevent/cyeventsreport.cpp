/***************************************************************************
                          cyeventsreport.cpp  -  description
                             -------------------
    début                  : jeu aoû 21 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyeventsreport.h"

// QT
#include <qfile.h>
#include <QTextStream>
#include <QTextCodec>
// CYLIBS
#include "cycore.h"
#include "cyeventreport.h"
#include "cystring.h"

CYEventsReport::CYEventsReport(QObject *parent, const QString &name, QString path, bool useNewFile, int projectId)
  : QObject(parent),
    mProjectId(projectId),
    mPath(path)
{
  setObjectName(name);

  mFileNum = 1;
  mUseNewFile = useNewFile;
  mDir = new QDir(path, "*.cyevt");
  setPath(path);
}

CYEventsReport::~CYEventsReport()
{
}

QString CYEventsReport::path()
{
  return mPath;
}

void CYEventsReport::setPath(const QString &path)
{
  mPath = path;
  updateDir();
}

void CYEventsReport::updateDir()
{
  if (!mDir->exists())
    mDir->mkdir(mPath);
}

QFile *CYEventsReport::newFile(QString filename)
{
  mUseNewFile = false;
  mFileName = filename;
  QFile *file = new QFile(mFileName);
  if (file->open(QIODevice::ReadWrite | QIODevice::Append))
  {
    QTextStream stream(file);
    stream << QString("%1:%2\n").arg(core->customerName()).arg(core->designerRef());
    QDate date = QDateTime::currentDateTime().date();
    QTime time = QDateTime::currentDateTime().time();
    stream << tr("Events report (%1) of %2 since %3\n\n").arg(mFileNum).arg(date.toString()).arg(time.toString());
    stream << tr(" COLOR\tTIME\tSINCE\tTYPE\tFROM\tDESCRIPTION\tNAME\n");
    file->close();
    emit newEventsReportFile(mFileName, mProjectId);
    core->backupFile(mFileName);
    return file;
  }
  else
    return 0;
}

QFile *CYEventsReport::findFile(bool nf)
{
  int fileNum = mFileNum;
  mFileName = QString("%1/events_%2.cyevt").arg(mDir->path()).arg(fileNum);

  while (mDir->exists(mFileName))
  {
    QFileInfo fi(mFileName);
    if (!nf && fi.exists() && (fi.size() < 102400)) // 100kO
    {
      QFileInfo fi2(QString("%1/events_%2.cyevt").arg(mDir->path()).arg(fileNum+1));
      if (!fi2.exists())
      {
        mFileNum = fileNum;
        return (new QFile(mFileName));
      }
    }
    fileNum++;
    mFileName = QString("%1/events_%2.cyevt").arg(mDir->path()).arg(fileNum);
  }
  mFileNum = fileNum;
  // Creation d'un nouveau fichier de journalisation
  return (newFile(mFileName));
}

void CYEventsReport::addReport(CYEventReport *report)
{
  if (report==0)
    return;

  reports.append(report);
  QFile *file = findFile( mUseNewFile );
  if (file && (file->open(QIODevice::ReadWrite | QIODevice::Append)))
  {
    QTextStream stream(file);
    stream << report->record();
    file->close();
    core->backupFile(mFileName);
    return;
  }
  else
    qDebug("void CYEventsReport::addReport(CYEventReport *report) 2");
}

void CYEventsReport::load(QString filename)
{
  mFileName = filename;
  load();
}

void CYEventsReport::load()
{
  QFile *file = new QFile(mFileName);
  if (!file->open(QIODevice::ReadOnly))
  {
    CYMESSAGETEXT(mFileName);
    return;
  }

  // Décodage automatique
  QTextCodec::ConverterState state;
  QTextCodec *codec = QTextCodec::codecForName("UTF-8");
  const QByteArray data(file->readAll());
  const QString text = codec->toUnicode(data.constData(), data.size(), &state);
  if (state.invalidChars > 0)
  {
      // Text non UTF-8 - utilisation localisation par défaut du système
      codec = QTextCodec::codecForLocale();
      if (!codec)
         return;
  }

  file->reset();
  QTextStream stream(file);
  stream.setCodec(codec);
  QString line;
  stream.reset();
  reports.clear();
  while (!stream.atEnd())
  {
    line = stream.readLine();
    if (!line.isEmpty() && (line.at(0)=='#'))
    {
      if (line.count('\t')<6) // ancien fichier < 2.67
      {
        QColor color(  line.section('\t', 0, 0)); // COULEUR
        QString time = line.section('\t', 1, 1);  // DATE HEURE
        QString type = line.section('\t', 2, 2);  // TYPE
        QString from = line.section('\t', 3, 3);  // PROVENANCE
        QString desc = line.section('\t', 4, 4);  // DESCRIPTION
        QString name = line.section('\t', 5, 5);  // NOM
        reports.append(new CYEventReport(this, name, color, QDateTime::fromString(time, Qt::ISODate), QDateTime(), type, from, desc));
      }
      else  // nouveau fichier >= 2.67
      {
        QColor color(  line.section('\t', 0, 0)); // COULEUR
        QString time = line.section('\t', 1, 1);  // DATE HEURE
        QString since= line.section('\t', 2, 2);  // DATE HEURE DE DEBUT
        QString type = line.section('\t', 3, 3);  // TYPE
        QString from = line.section('\t', 4, 4);  // PROVENANCE
        QString desc = line.section('\t', 5, 5);  // DESCRIPTION
        QString name = line.section('\t', 6, 6);  // NOM

        if (since.isEmpty())
          reports.append(new CYEventReport(this, name, color, QDateTime::fromString(time, Qt::ISODate), QDateTime(), type, from, desc));
        else
          reports.append(new CYEventReport(this, name, color, QDateTime::fromString(time, Qt::ISODate), QDateTime::fromString(since, Qt::ISODate), type, from, desc));
      }
    }
  }
}

QString CYEventsReport::loadTimeToString()
{
  QFileInfo fi = QFileInfo(mFileName);
  return fi.created().toString(Qt::ISODate).replace(QChar('T'), " / " );
}
