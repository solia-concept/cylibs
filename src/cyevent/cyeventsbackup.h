/***************************************************************************
                          cyeventsbackup.h  -  description
                             -------------------
    begin                : mar aoû 3 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYEVENTSBACKUP_H
#define CYEVENTSBACKUP_H

// CYLIBS
#include "cydialog.h"

namespace Ui {
    class CYEventsBackup;
}

/** @short Boîte d'archivage des évènements.
  * @author LE CLÉACH Gérald
  */

class CYEventsBackup : public CYDialog
{
   Q_OBJECT
public:
  CYEventsBackup(QWidget *parent=0, const QString &name=0);
  ~CYEventsBackup();

public slots: // Public slots
  /** Applique les modifications et ferme la boîte de dialogue. */
  virtual void accept();

private:
  Ui::CYEventsBackup *ui;
};

#endif
