/***************************************************************************
                          cyevent.h  -  description
                             -------------------
    début                  : mer aoû 20 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYEVENT_H
#define CYEVENT_H

// QT
#include <QColor>
#include <QHash>
#include <QDateTime>
// CYLIBS
#include "cytypes.h"
#include "cyflag.h"

class CYData;
class CYEventsManager;
class CYEventsGenerator;
class CYEventsList;
class CYCommand;
class CYEventReport;

/** CYEvent est la classe de base pour gérer un évènemet.
  * Grâce à elle il est possible de traîter les défauts, alarmes ou
  * autres états du process qui méritent d'être signalés à l'opérateur.
  * Ce signalement peut se faire par la messagerie ainsi que par le
  * journal d'évènements.
  * @short Evènement.
  * @author Gérald LE CLEACH
  */

class CYEvent : public CYFlag
{
   Q_OBJECT
public:
    /** Attr1 est une structure de saisie simple des attributs d'un évènement généré par les états du grafcet avec des données de référence. */
    struct Attr1
    {
      /** Nom. */
      const QString &name;
      /** Description. */
      QString desc;
      /** Couleur en état actif. */
      QColor color;
      /** Noms des données de référence séparés par une virgule.*/
      QString refs;
      /** Aide expliquant les actions à mener.*/
      QString help;
    };

    /** Attr2 est une structure de saisie simple des attributs d'un évènement généré par un flag. */
    struct Attr2
    {
      /** Nom. */
        const QString &name;
      /** Valeur de la l'évènement. */
      flg *val;
      /** Etat actif. */
      bool enable;
      /** Type de traîtement à effectuer. */
      int proc;
      /** Description. */
      QString desc;
      /** Provenance. */
      QString from;
      /** Aide expliquant les actions à mener.*/
      QString help;
    };

    /** Attr3 est une structure de saisie simple des attributs d'un évènement généré par un flag
      * avec des données de référence. */
    struct Attr3
    {
      /** Nom. */
      const QString &name;
      /** Valeur de la l'évènement. */
      flg *val;
      /** Etat actif. */
      bool enable;
      /** Type de traîtement à effectuer. */
      int proc;
      /** Description. */
      QString desc;
      /** Provenance. */
      QString from;
      /** Noms des données de référence séparés par une virgule.*/
      QString refs;
      /** Aide expliquant les actions à mener.*/
      QString help;
    };

    /** Attr4 est une structure de saisie simple des attributs d'un évènement généré par un flag
      * avec une couleur spécifique en état actif. */
    struct Attr4
    {
      /** Nom. */
      const QString &name;
      /** Valeur de la l'évènement. */
      flg *val;
      /** Etat actif. */
      bool enable;
      /** Type de traîtement à effectuer. */
      int proc;
      /** Description. */
      QString desc;
      /** Provenance. */
      QString from;
      /** Couleur en état actif. */
      QColor color;
      /** Aide expliquant les actions à mener.*/
      QString help;
    };

    /** Attr5 est une structure de saisie simple des attributs d'un évènement généré par un flag
      * avec des données de référence et une couleur spécifique en état actif. */
    struct Attr5
    {
      /** Nom. */
      const QString &name;
      /** Valeur de la l'évènement. */
      flg *val;
      /** Etat actif. */
      bool enable;
      /** Type de traîtement à effectuer. */
      int proc;
      /** Description. */
      QString desc;
      /** Provenance. */
      QString from;
      /** Noms des données de référence séparés par une virgule.*/
      QString refs;
      /** Couleur en état actif. */
      QColor color;
      /** Aide expliquant les actions à mener.*/
      QString help;
    };

    /** Création d'un évènement à partir des étapes grafcet.
      * @param parent Parent.
      * @param name   Nom.
      * @param desc   Description.
      * @param color  Couleur en état actif.
      * @param val    Pointeur sur un flg si lecture automatique (appelé par le readData de la base de données).
      * @param help   Aide expliquant les actions à mener. */
    CYEvent(CYEventsGenerator *generator, const QString &name, QString desc=0, QColor color=Qt::white, flg *val=0, QString help=0);

    /** Création d'un évènement à partir d'un flag.
      * @param parent Parent.
      * @param name   Nom.
      * @param val    Valeur du flag.
      * @param enable Etat actif.
      * @param proc   Type de traîtement à effectuer.
      * @param desc   Description.
      * @param from   Provenance.
      * @param refs   Noms des données de référence séparés par une virgule.
      * @param help   Aide expliquant les actions à mener.
      * @param color  Couleur en état actif. */
    CYEvent(CYEventsGenerator *generator, const QString &name, flg *val=0, bool enable=SI_1, int proc=0, QString desc=0, QString from=0, QString refs=0, QString help=0, QColor color=Qt::white);

    /** Création d'un évènement à partir des états du grafcet avec les attributs Attr1.
      * @param parent Parent.
      * @param attr   Attributs.
      * @param val    Pointeur sur un flg si lecture automatique (appelé par le readData de la base de données). */
    CYEvent(CYEventsGenerator *generator, Attr1 *attr, flg *val=new flg);

    /** Création d'un évènement à partir des états du grafcet avec les attributs Attr1.
      * @param parent Parent.
      * @param name   Nom.
      * @param attr   Attributs.
      * @param val    Pointeur sur un flg si lecture automatique (appelé par le readData de la base de données). */
    CYEvent(CYEventsGenerator *generator, const QString &name, Attr1 *attr, flg *val=new flg);

    /** Création d'un évènement à partir d'un flag avec les attributs Attr2.
      * @param parent Parent.
      * @param attr   Attributs. */
    CYEvent(CYEventsGenerator *generator, Attr2 *attr);

    /** Création d'un évènement à partir d'un flag avec les attributs Attr2.
      * @param parent Parent.
      * @param name   Nom.
      * @param attr   Attributs. */
    CYEvent(CYEventsGenerator *generator, const QString &name, Attr2 *attr);

    /** Création d'un évènement à partir d'un flag avec les attributs Attr3.
      * @param parent Parent.
      * @param attr   Attributs. */
    CYEvent(CYEventsGenerator *generator, Attr3 *attr);

    /** Création d'un évènement à partir d'un flag avec les attributs Attr3.
      * @param parent Parent.
      * @param name   Nom.
      * @param attr   Attributs. */
    CYEvent(CYEventsGenerator *generator, const QString &name, Attr3 *attr);

    /** Création d'un évènement à partir d'un flag avec les attributs Attr4.
      * @param parent Parent.
      * @param attr   Attributs. */
    CYEvent(CYEventsGenerator *generator, Attr4 *attr);

    /** Création d'un évènement à partir d'un flag avec les attributs Attr4.
      * @param parent Parent.
      * @param name   Nom.
      * @param attr   Attributs. */
    CYEvent(CYEventsGenerator *generator, const QString &name, Attr4 *attr);

    /** Création d'un évènement à partir d'un flag avec les attributs Attr5.
      * @param parent Parent.
      * @param attr   Attributs. */
    CYEvent(CYEventsGenerator *generator, Attr5 *attr);

    /** Création d'un évènement à partir d'un flag avec les attributs Attr5.
      * @param parent Parent.
      * @param name   Nom.
      * @param attr   Attributs. */
    CYEvent(CYEventsGenerator *generator, const QString &name, Attr5 *attr);

    /** Création d'un évènement à partir d'un flag avec connexion au générateur ultérieurement (@see CYCT).
      * @param name   Nom.
      * @param val    Valeur du flag.
      * @param enable Etat actif.
      * @param proc   Type de traîtement à effectuer.
      * @param desc   Description.
      * @param from   Provenance.
      * @param refs   Noms des données de référence séparés par une virgule.
      * @param help   Aide expliquant les actions à mener. */
    CYEvent(const QString &name, flg *val=0, bool enable=SI_1, int proc=0, QString desc=0, QString from=0, QString refs=0, QString help=0);

    /** Création d'un évènement à partir d'une commande.
      * @param parent Parent.
      * @param name   Nom.
      * @param val    Valeur du flag.
      * @param enable Etat actif.
      * @param proc   Type de traîtement à effectuer.
      * @param desc   Description.
      * @param from   Provenance.
      * @param refs   Noms des données de référence séparés par une virgule.
      * @param help   Aide expliquant les actions à mener. */
    CYEvent(CYCommand *command, const QString &name, flg *val=0, bool enable=SI_1, int proc=0, QString desc=0, QString from=0, QString refs=0, QString help=0, QColor color=Qt::white);

    ~CYEvent();

    /**
     * @brief Réserve l'exclusivité de l'évènement à un poste.
     * Par défaut les évènements ne sont pas rattachés à un poste et leur journalisation se fait alors
     * dans les journaux d'évènements de chaque projet chargé par poste.
     * @param index Index de poste pour lequel l'événement doit être réservé.
     */
    inline void initIndexStation(int index) { mIndexStation = index; }
    /**
     * @return l'index de poste pour lequel l'évènement est réservé.
     * Par défaut vaut -1. L'événement n'est alors réservé à aucun poste et sa journalisation se fait alors
     * dans les journaux d'évènements de chaque projet chargé par poste.
     */
    inline int indexStation() { return mIndexStation; }

    /** Lit la valeur et en déduit si l'évènement est actif. */
    virtual void read();
    /** @return \true si l'évènement est actif. */
    bool isEnable() { return mEnable; }
    /** Saisir \a true pour que l'évènement soit actif à true. */
    virtual void setEnableState(const bool mode)  { mEnableState = mode; }

    /** @return la  description. */
    virtual QString desc();

    /** @return le préfix du message d'évènement. */
    virtual QString prefix() { return mPrefix; }
    /** Saisie le préfix du message d'évènement. */
    virtual void setPrefix(QString string) { mPrefix = string; }

    /** @return le suffix du message d'évènement. */
    virtual QString suffix() { return mSuffix; }
    /** Saisie le suffix du message d'évènement. */
    virtual void setSuffix(QString string) { mSuffix = string; }

    /** @return le message d'apparation de journalisation. */
    QString reportUp() { return mReportUp; }
    /** @return le message de disparition de journalisation. */
    QString reportEnd() { return mReportEnd; }
    /** @return le message d'acquittemement de journalisation. */
    QString reportAcknow() { return mReportAcknow; }
    /** @return la chaîne de caractère correspondant à la valeur de \a data. */
    QString stringRef(CYData *data);
    /** Rend l'évènement local ou non. Si \a state vaut \a true l'évènement n'est
      * alors pas soumis à un test de communications. */
    void setLocal(bool state) { mLocal = state; }
    /** @return le message de l'évènement.
      * @param oneLine Si \a true alors les passages à la lignes sont supprimés. */
    virtual QString message(bool oneLine=false);
    virtual void setRefs(QString);
    virtual bool mustToBeMailing();
    virtual void setList( CYEventsList * list );
    void setGenerator( CYEventsGenerator *generator );

    QString sender();

    /** @return le type de traîtement à effectuer. */
    int processing() { return mProcessing; }
    /** Saisie la provenance */
    void setSender(QString from);
    /** Saisie le flag d'autorisation de l'évènement. Si celui-ci est à faut alors l'évènement n'est pas géré. */
    void setAuthorizationFlag(CYFlag *flag);

    /*!
     * \brief setDoc Active ou non l'ajout de cet événement en manuel en ligne.
     * Evite de dupliquer inutilement des événements similaires.
     * Si \a index est égale à \a index_ref, alors c'est cet événement est présent dans le manuel en ligne.
     * \param index     Index de l'événement.
     * \param index_ref Index de l'événement similaire de référence.
     * \param note      Note ajoutée à l'aide (ex. Idem pour les autres entrées analogiques.)
     */
    void setCydoc(int index, int index_ref, QString note);
    /** @retur si cet événement est expliqué en manuel en ligne.*/
    bool cydocIsEnabled() { return mCydocEnable; }

public slots: // Public slots
    /** Saisie la couleur en état actif.\n
      * Note: la couleur en état inactif en est automatiquement déduite. */
    void setEnableColor(QColor color);
    /** Saisie la couleur en état inactif. */
    void setDisableColor(QColor color) { mDisableColor = color; }

    /** Saisie le message d'apparation de journalisation. */
    void setReportUp(QString report) { mReportUp = report; setGroup(mReportUp); }
    /** Saisie le message de disparition de journalisation. */
    void setReportEnd(QString report) { mReportEnd = report; }
    /** Saisie le message d'acquittemement de journalisation. */
    void setReportAcknow(QString report) { mReportAcknow = report; }

protected: // Protected methods
    /** Ajoute un nouveau message dans le journal d'évènements. */
    virtual void addToEventsReport();

private: // Private methods
    /** No descriptions */
    void init();

public: // Public attributes
    /** Note en doc pour cet évènement. (ex. Idem pour 2 à 6 postes)*/
    QString docNote;

protected: // Protected attributes
    /** Gestionnaire d'évènemments. */
    CYEventsManager *mManager;
    /** Etat actif. */
    bool mEnableState;
    /** Type de traîtement à effectuer. */
    int mProcessing;
    /** Description. */
    QString mDesc;
    /** Provenance. */
    QString mSender;
    /** Dictionnaire des données de référence. */
    QHash<int, CYData*> mDatasRef;
    /** Chaînes de caractères sauvegardées correspondant aux valeurs des données de référence de type DATAi_REC. */
    QHash<int, QString*> mStringsRef;

    /** Etat actif de l'évènement à l'affichage. */
    bool mEnable;
    /** Etat de l'évènement. */
    bool mState;
    /** Etat précédent de l'évènement. */
    bool mOldState;
    /** Etat de l'évènement en simulation (si aqcuittement prise en compte de l'acquittement et fin de sa simulation). */
    bool mSimulState;

    /** Message d'apparation de journalisation. */
    QString mReportUp;
    /** Date et heure du message de détection de journalisation. */
    QDateTime mReportUpDateTime;
    /** Message de disparition de journalisation. */
    QString mReportEnd;
    /** Message d'acquittemement de journalisation. */
    QString mReportAcknow;

    /** Un évènement local n'est pas soumis à un test de communications. */
    bool mLocal;
    /** Préfix du message d'évènement. */
    QString mPrefix;
    /** Suffix du message d'évènement. */
    QString mSuffix;
    /** Générateur de cet évènement. */
    CYEventsGenerator * mGenerator;
    CYEventsList * mList;

    CYFlag *mAuthorizationFlag;

    /** Active/désactive la doc pour cet évènement.*/
    bool mCydocEnable;

    /** Index du poste associé à cet évènement. */
    int mIndexStation;
};

#endif
