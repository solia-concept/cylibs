/***************************************************************************
                          cyeventsviewitem.h  -  description
                             -------------------
    begin                : lun nov 24 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYEVENTSVIEWITEM_H
#define CYEVENTSVIEWITEM_H

// QT
 #include <QTreeWidget>

/** @short Ligne de visualisation d'un évènement.
  * @author Gérald LE CLEACH
  */

class CYEventsViewItem : public QTreeWidgetItem
{
public:
  CYEventsViewItem(QTreeWidget *parent, QStringList list);
  virtual ~CYEventsViewItem() {}

  void setTextColor(const QColor& color);

private: // Private attributes
  /** Couleur du text. */
  QColor mTextColor;
};

#endif
