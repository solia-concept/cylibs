/***************************************************************************
                          cyeventsbackup.cpp  -  description
                             -------------------
    begin                : mar aoû 3 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyeventsbackup.h"
#include "ui_cyeventsbackup.h"

// QT
#include <QDateTimeEdit>
// CYLIBS
#include "cycore.h"
#include "cyeventsmanager.h"
#include "cymessagebox.h"

CYEventsBackup::CYEventsBackup(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYEventsBackup)
{
  ui->setupUi(this);
  ui->qDateTimeEdit->setDate(core->events->backupHistoryInitDate());
}

CYEventsBackup::~CYEventsBackup()
{
  delete ui;
}

void CYEventsBackup::accept()
{
  QDate date = ui->qDateTimeEdit->date();
  if (date >= QDate::currentDate())
  {
    CYMessageBox::sorry(this, tr("Select a date lower than today's."));
    return;
  }
  core->events->backupHistory(ui->qDateTimeEdit->date());
  CYDialog::accept();
}
