/***************************************************************************
                          cyeventsgenerator.h  -  description
                             -------------------
    begin                : mer nov 10 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYEVENTSGENERATOR_H
#define CYEVENTSGENERATOR_H

// QT
#include <qobject.h>

class CYEventsList;
class CYEventsGenerator;
class CYNetHost;
class CYNetLink;
class CYDB;

/** @short Générateur d'évènements.
  * Cette classe permet de déclarer les différents évènements généré par un PC.
  * @author LE CLÉACH Gérald
  */

class CYEventsGenerator : public QObject
{
  Q_OBJECT
public:
  CYEventsGenerator(QObject *parent=0, const QString &name=0, int host=-1, int com=-1);
  ~CYEventsGenerator();

  /** @return la liste des alarmes. */
  CYEventsList *alerts();
  /** @return la liste des défauts. */
  CYEventsList *faults();
  /** Ajoute une liste d'évènements au gestionnaire d'évènements. */
  virtual void addEventsList(CYEventsList *list);
  /** Ajoute un générateur d'évènements. */
  virtual void addEventsGenerator(CYEventsGenerator *generator);

  /** @return \a true si les communications qui retournent les infos évènementielles sont bonnes. */
  virtual bool netIsOk() { return mNetOk; }

  /** @return la base de donnée de ses évènements. */
  CYDB *db() { return mDB; }

public slots: // Public slots
  /** Rafraîchit les messageries. */
  virtual void messaging() {}
  
protected: // Protected attributes
  /** Vaut \a true si les communications qui retournent les infos évènementielles sont bonnes. */
  bool mNetOk;
  /** Si le générateur d'évènements est rattaché à un hôte distant dans son constructeur alors les évènenemnts sont insérés dans la bas de données globale et sont donc accessibles de n'importe où dans l'application. */
  CYNetHost *mHost;
  /** Si le générateur d'évènements est rattaché à un hôte distant et à une connexion réseau dans son constructeur alors les évènenemnts sont insérés dans la bas de données globale et sont donc accessibles par exemple dans l'outil graphique ou l'outil d'acquisition. */
  CYNetLink *mLink;
  CYDB *mDB;
};

#endif
