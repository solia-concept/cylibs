//
// C++ Interface: cyeventsmailing
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYEVENTSMAILING_H
#define CYEVENTSMAILING_H

// QT
#include <QObject>
#include <QList>
// CYLIBS
#include <cyflag.h>

class CYEventReport;
class CYString;
class CYU8;

/**
@short Gestion d'envoi par mail de message d'évènements.

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYEventsMailing : public QObject
{
  Q_OBJECT

public:
    CYEventsMailing(QObject *parent=0, const QString &name=0);

    ~CYEventsMailing();
    void setTimer( int ms );
    void setRecFlag( CYFlag * flag );
    virtual void addReport(CYEventReport *report);

public slots:
    void send();
    void test();

protected:
    /** Active l'enregistrement de la liste de messages d'évènements à envoyer.*/
    CYFlag * mRecFlag;
    QList<CYEventReport*> mList;
    QTimer *mTimer;

protected slots:
    virtual void recFlagChanged( bool risign );
};

#endif
