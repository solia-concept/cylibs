/***************************************************************************
                          cyeventsgenerator.cpp  -  description
                             -------------------
    begin                : mer nov 10 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyeventsgenerator.h"

// CYLIBS
#include "cy.h"
#include "cycore.h"
#include "cyeventsmanager.h"
#include "cynetlink.h"

CYEventsGenerator::CYEventsGenerator(QObject *parent, const QString &name, int host, int com)
: QObject(parent)
{
  setObjectName(name);
  mNetOk = false;
  mHost=core->host(host);
  if (mHost)
  {
    mDB = new CYDB(mHost, QString("%1_DB").arg(name), -1, 0, true);
    mDB->setGroup(tr("Events"));
    mLink = core->link(com);
    if (mLink)
    {
      mLink->addDB(mDB);
    }
  }
  else
    mDB = core->events->db();
}

CYEventsGenerator::~CYEventsGenerator()
{
}

CYEventsList *CYEventsGenerator::alerts()
{
  return core->events->alerts();
}

CYEventsList *CYEventsGenerator::faults()
{
  return core->events->faults();
}

void CYEventsGenerator::addEventsList(CYEventsList *list)
{
  core->events->addEventsList(list);
}

void CYEventsGenerator::addEventsGenerator(CYEventsGenerator *generator)
{
  core->events->addEventsGenerator(generator);
}
