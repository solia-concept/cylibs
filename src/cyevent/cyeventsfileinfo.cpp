/***************************************************************************
                          cyeventsfileinfo.cpp  -  description
                             -------------------
    begin                : ven mar 5 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyeventsfileinfo.h"

CYEventsFileInfo::CYEventsFileInfo()
{
  init();
}

CYEventsFileInfo::CYEventsFileInfo(const QString &file)
: QFileInfo(file)
{
  init();
}

CYEventsFileInfo::CYEventsFileInfo(const QFile &file)
: QFileInfo(file)
{
  init();
}

CYEventsFileInfo::CYEventsFileInfo(const QDir &d,const QString &fileName)
: QFileInfo(d, fileName)
{
  init();
}

CYEventsFileInfo::CYEventsFileInfo(const QFileInfo &fi)
: QFileInfo(fi)
{
  init();
}

CYEventsFileInfo::~CYEventsFileInfo()
{
}

void CYEventsFileInfo::init()
{
  mCreatedTime = created();
  mBackup      = false;
}

QDateTime CYEventsFileInfo::createdTime()
{
  return mCreatedTime;
}

QString CYEventsFileInfo::createdTimeToString()
{
  return mCreatedTime.toString(Qt::ISODate).replace(QChar('T'), " / ");
}
