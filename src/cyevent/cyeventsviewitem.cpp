/***************************************************************************
                          cyeventsviewitem.cpp  -  description
                             -------------------
    begin                : lun nov 24 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyeventsviewitem.h"

CYEventsViewItem::CYEventsViewItem(QTreeWidget *parent, QStringList list)
: QTreeWidgetItem(parent, list)
{
}

void CYEventsViewItem::setTextColor(const QColor& color)
{
  mTextColor = color;
  for (int col=0; col<columnCount(); col++)
    setForeground(col, QBrush(color));
}
