/***************************************************************************
                          cyeventslist.h  -  description
                             -------------------
    début                  : lun aoû 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYEVENTSLIST_H
#define CYEVENTSLIST_H

// QT
#include <QObject>
#include <QList>
#include <QColor>
// CYLIBS
#include "cytypes.h"

class CYEvent;
class CYDB;
class CYString;
class CYFlag;

/** Cette classe permet de gérer la messagerie et la
  * journalisation d'évènements.
  * @short Gestion d'une liste d'évènements.
  * @author Gérald LE CLEACH
  */

class CYEventsList : public QObject
{
  Q_OBJECT
public:
  CYEventsList(QObject *parent=0, const QString &name=0);
  ~CYEventsList();

  /** Ajoute un évènement. */
  void add(CYEvent *event);
  /** Ajoute une liste d'évènements. */
  void addList(QList<CYEvent*> events);

  /** @return une chaîne de caractères destinée au tableau de bord sous KDE3
    * donnant les informations relatives à l'évènement actif.
    * Celle-ci est composée de différents champs séparés par le carcatère "~"
    * donnant les 3 valeurs RVB correspondant à la couleur
    * de l'évènement actif ainsi que sa description.
    */
  virtual QString message();

  /** @return une chaîne de caractères destinée au tableau de bord
    * donnant les informations relatives à l'évènement d'indice \a i.
    * Celle-ci est composée de différents champs séparés par le carcatère "~"
    * donnant les 3 valeurs RVB correspondant à la couleur
    * de l'évènement d'indice \a i ainsi que sa description.
    * @param i      Indice de l'évènement dans sa liste (CYEventsList)
    */
  virtual QString message(int i);

  /** @return une chaîne de caractères destinée au tableau de bord sous KDE3
    * donnant les informations relatives à l'évènement actif.
    * Celle-ci est composée de différents champs séparés par le carcatère "~"
    * donnant les 3 valeurs RVB correspondant à la couleur
    * de l'évènement actif ainsi que sa description.
    * Donne en paramètre les informations relatives à l'évènement actif pour
    * pour le plasamoïde sous KDE4.
    * @param msg    Message courant de l'évènement.
    * @param color  Couleur de l'évènement.
    * @param help   Aide de l'évènement.
    */
  virtual QString message(QString &msg, QColor &color, QString &help);

  /** @return une chaîne de caractères destinée au tableau de bord sous KDE3
    * donnant les informations relatives à l'évènement actif.
    * Celle-ci est composée de différents champs séparés par le carcatère "~"
    * donnant les 3 valeurs RVB correspondant à la couleur
    * de l'évènement actif ainsi que sa description.
    * Donne en paramètre les informations relatives à l'évènement actif pour
    * pour le plasamoïde sous KDE4.
    * @param msg    Message courant de l'évènement.
    * @param color  Couleur de l'évènement.
    * @param help   Aide de l'évènement.
    * @param name   Nom l'évènement.
    */
  virtual QString message(QString &msg, QColor &color, QString &help, QString &name);

  /** @return une chaîne de caractères destinée au tableau de bord
    * donnant les informations relatives à l'évènement d'indice \a i.
    * Celle-ci est composée de différents champs séparés par le carcatère "~"
    * donnant les 3 valeurs RVB correspondant à la couleur
    * de l'évènement actif ainsi que sa description.
    * Donne en paramètre les informations relatives à l'évènement d'indice \a i pour
    * pour le plasamoïde sous KDE4.
    * @param msg    Message courant de l'évènement.
    * @param color  Couleur de l'évènement.
    * @param help   Aide de l'évènement.
    */
  virtual QString message(int i, QString &msg, QColor &color, QString &help);

  /** @return le texte du dernier message traîté. */
  QString text() { return mText; }
  /** @return le texte d'un message de la liste.
    * @param i        Indice de l'évènement dans sa liste (CYEventsList)
    * @param oneLine  Si \a true alors les passages à la lignes sont supprimés. */
  QString text(int i, bool oneLine=false);
  /** @return la couleur du dernier message traîté. */
  QColor color() { return mColor; }

  /** @return la donnée contenant le dernier message traîté. */
  CYString *dataMessage() { return mDataMessage; }
  /** Saisie la donnée contenant le dernier message traîté. */
  void setDataMessage(CYString *data);
  /** Saisie le nom de la donnée contenant le dernier message traîté. */
  void setDataMessage(QString dataName);
  /** @return true si un évènement ayant pour nom \a eventName existe dans cette liste. */
  bool exists(QString eventName);
  /** @return l'évènement ayant pour indice \a i dans la liste d'évènements. */
  CYEvent *event(int i);

  /** Saisir \a true pour definir cette liste comme liste d'évènements pouvant être simultatés. */
  void setSimultaneousEvents(bool val) { mSimultaneousEvents = val; }
  /** @return \a true si cette liste est définie comme liste d'évènements pouvant être simultatés. */
  bool simultaneousEvents() { return mSimultaneousEvents; }

  /** Saisir \a true pour autoriser la lecteur des èvènements. */
  void setEnableRead(bool val) { mEnableRead = val; }
  /** @return \a true si la lecteur des èvènements est autorisée. */
  bool enableRead() { return mEnableRead; }

  /** @return le nombre d'évènements actifs. */
  int nbEventsEnable();

  /*!
   * \brief setDoc Active ou non l'ajout de cette liste d'évènements en manuel en ligne.
   * Evite de dupliquer inutilement des listes d'évènements similaires.
   * Si \a index est égale à \a index_ref, alors c'est cette liste d'évènements est présente dans le manuel en ligne.
   * \param index     Index de la lsite d'événements.
   * \param index_ref Index de la liste d'événements similaire de référence.
   * \param note      Note ajoutée à l'aide (ex. Idem pour les autres postes.)
   */
  void setCydoc(int index, int index_ref, QString note);
  /** @return la note en manuel en ligne pour cet liste d'évènements.*/
  QString cydocNote() { return mCydocNote; }
  /** @return si cette liste d'événements est expliquée en manuel en ligne.*/
  bool cydocIsEnabled() { return mCydocEnable; }
  /** Saisie si cette liste d'événements est expliquée en manuel en ligne.*/
  void setCydocIsEnabled(bool val) { mCydocEnable = val; }

public slots: // Public slots
  /** Lecture des évènements. */
  void read();
  void setEventVal(int id_event, flg val);

signals: // Signals
  /** Donne l'ordre de rafraîchir la messagerie. */
  void messaging();

public: // Public attributes
  /** Liste de tous les évènements gérés par cette messagerie. */
  QList<CYEvent*> mList;
  CYFlag * enableMailing;

protected: // Protected attributes
  /** Texte du dernier message traîté. */
  QString mText;
  /** Couleur du dernier message traîté. */
  QColor mColor;
  /** Donnée contenant le dernier message traîté. */
  CYString *mDataMessage;

  CYDB   * mDB;

private: // Private attributes
  /** Indice de l'évènement courrant. */
  int mCurrentEvent;
  /** Indice de l'évènement précédent. */
  uint mOldEvent;
  /** Etat de l'évènement précédent. */
  bool mOldEnable;
  /** Liste d'évènements pouvant être simultatés. */
  bool mSimultaneousEvents;
  /** Autorise ou non la lecteur des èvènements. */
  bool mEnableRead;

  /** Note en doc pour cet évènement. (ex. Idem pour 2 à 6 postes)*/
  QString mCydocNote;
  /** Active/désactive la doc pour cet évènement.*/
  bool mCydocEnable;
};

#endif
