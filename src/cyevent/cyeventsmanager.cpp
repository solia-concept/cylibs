/***************************************************************************
                          cyeventsmanager.cpp  -  description
                             -------------------
    début                  : lun aoû 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyeventsmanager.h"

// QT
#include <QTextStream>
// CYLIBS
#include "cy.h"
#include "cycore.h"
#include "cyproject.h"
#include "cyevent.h"
#include "cyeventsreport.h"
#include "cyeventreport.h"
#include "cyeventslist.h"
#include "cyeventsgenerator.h"
#include "cyeventsfileinfo.h"
#include "cyeventsmailing.h"
#include "cyflag.h"
#include "cyuser.h"
#include "cymessagebox.h"
#include "cyapplication.h"
#include "cyaboutdata.h"

CYEventsManager::CYEventsManager(QObject *parent, const QString &name)
  : QObject(parent)
{
  setObjectName(name);

  core->events=this;

  mEnabled = false;
//   mReadTimerVal      =  250; // ms
  mReadTimerVal      = 1000; // ms
  mMessagingTimerVal = 1500;

  mProjetChanged = false;
  mExtractMessagesOk = false;

  mMailing = new CYEventsMailing(this, "EventsMailing");

  mDB = new CYDB(this, "EVENTS_MANAGER_DB", -1, 0, false);

  connect(this, SIGNAL(refreshingMessagingValues()), core, SIGNAL(refreshingMessagingValues()));
}

CYEventsManager::~CYEventsManager()
{
}

CYEventsReport *CYEventsManager::eventsReport(int projectId)
{
  if (!core)
    CYFATAL;
  CYProject *prj = core->project(projectId);
  if (!prj || (!prj->eventsReport()))
    return 0;
  return prj->eventsReport();
}

void CYEventsManager::addReport(CYEventReport *report)
{
  bool first = true;

  for (int i=0; i<core->projectCount(); i++)
  {
    if (report->indexStation()>-1 && report->indexStation()!=i)
      continue; // événement réservé à un autre projet

    historyExists(i);
    CYEventsReport *er = eventsReport(i);
    if (er)
    {
      er->addReport(report);
      if (first)
      {
//        first = false;
        emit newEvent(report);
      }
    }
  }
}

QString CYEventsManager::reportfileName()
{
  return eventsReport()->fileName();
}

void CYEventsManager::setReportFileName(const QString &filename)
{
  eventsReport()->setFileName(filename);
}

QString CYEventsManager::dirPath()
{
  return eventsReport()->path();
}

void CYEventsManager::setDirPath(const QString &path)
{
  eventsReport()->setPath(path);
}

QFile *CYEventsManager::findFile()
{
  mFileNum = 1;

  mFileName = QString("%1/history_%2.cyhist").arg(mDir->path()).arg(mFileNum);
  CYEventsFileInfo fi(mFileName);
  if (fi.exists() && fi.size() < 2000000)
    return (new QFile(mFileName));
  while (mDir->exists(mFileName))
  {
    mFileNum++;
    mFileName = QString("%1/history_%2.cyhist").arg(mDir->path()).arg(mFileNum);
    CYEventsFileInfo fi(mFileName);
    if (fi.exists() && fi.size() < 2000000)
      return (new QFile(mFileName));
  }
  // Creation d'un nouveau fichier de journalisation
  return (new QFile(mFileName));
}

void CYEventsManager::loading()
{
  QFileInfoList list = mDir->entryInfoList();
  if ( list.isEmpty() )
    return;
  QListIterator<QFileInfo> it(list);

  history.clear();
  while (it.hasNext())
  {
    CYEventsFileInfo fi=(CYEventsFileInfo)it.next();
    QString fileName = fi.absoluteFilePath();
    load(fileName);
  }
}

bool CYEventsManager::load(const QString &fN)
{
  if (!core)
    return false;

  QFile file(mFileName = fN);
  if (!file.open(QIODevice::ReadOnly))
  {
    CYMessageBox::sorry(0, tr("Can't open the file %1").arg(mFileName));
    return (false);
  }

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  if (!doc.setContent(&file))
  {
    CYMessageBox::sorry(0, tr("The file %1 does not contain valid XML").arg(mFileName));
    return (false);
  }
  // Vérifie le type propre du document.
  if (doc.doctype().name() != "CYLIXEventsManager")
  {
    CYMessageBox::sorry(0, QString(tr("The file %1 does not contain a valid"
                                  "definition, which must have a document type ").arg(mFileName)
                                  +"'CYLIXEventsManager'"));
    return (false);
  }

  QDomElement el = doc.documentElement();
  mFileNum = el.attribute("numFile").toUInt();

  /* Charge les listes des journaux d'évènements par projet. */
  QDomNodeList list = el.elementsByTagName("project");
  for (int i = 0; i < list.count(); ++i)
  {
    QDomElement item = list.item(i).toElement();
    QString created  = item.attribute("created");
    QString file     = item.attribute("file");
    bool backup      = (bool)item.attribute("backup").toShort();
    int projectId    =  item.attribute("projectId").toInt();
    CYEventsFileInfo *efi = new CYEventsFileInfo(file);
    efi->setCreatedTime(created);
    efi->setBackup(backup);
    efi->setProjectId(projectId);
    if (!efi->backup())
      history.append(efi);
  }

  return (true);
}

bool CYEventsManager::save(const QString &)
{
  if (!core)
    return false;

  QDomDocument doc("CYLIXEventsManager");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  QDomElement el = doc.createElement("Projects");
  doc.appendChild(el);
  el.setAttribute("numFile" , mFileNum);
  el.setAttribute("dateTime", QDateTime::currentDateTime().toString(Qt::ISODate));

  // Enregistre les listes des journaux d'évènements par projet. */
  foreach (CYEventsFileInfo *file, history)
  {
    QDomElement item = doc.createElement("project");
    el.appendChild(item);
    item.setAttribute("created", file->createdTime().toString(Qt::ISODate));
    item.setAttribute("file", file->filePath());
    item.setAttribute("backup", file->backup());
    item.setAttribute("projectId", file->projectId());
  }

  QFile *file = findFile();
  if (!file->open(QIODevice::WriteOnly))
  {
    CYWARNINGTEXT(tr("Can't save file %1!").arg(mFileName));
    return (false);
  }
  QTextStream s(file);
  s.setCodec("UTF-8");
  s << doc;
  file->close();

  return (true);
}

void CYEventsManager::projectChanged()
{
  projectChanged(0);
}

void CYEventsManager::projectChanged(int poste)
{
  Q_UNUSED(poste)
  if (!mProjetChanged || (core && core->projectLack()))
  {
    mProjetChanged = true;
    core->setProjectLack(false);
    cylixStart();
  }
}

void CYEventsManager::testChanged(int projectId)
{
  CYEventsReport *report = core->project(projectId)->eventsReport();
  connect(report, SIGNAL(newEventsReportFile(QString, int)), SLOT(newEventsReportFile(QString, int)));
  if (mProjetChanged || core->projectLack())
    report->findFile();
}

void CYEventsManager::enable()
{
  if (mEnabled)
    return;

  mEnabled = true;
  QString path = core->appDataDir()+"/history/";
  mDir = new QDir( path, "*.cyhist");
  if ( !mDir->exists() )
    mDir->mkdir( path );
  loading();

  QTimer *readTimer = new QTimer(this);
  connect(readTimer, SIGNAL(timeout()), SLOT(read()));
  readTimer->start(mReadTimerVal);

  QTimer *messagingTimer = new QTimer(this);
  connect(messagingTimer, SIGNAL(timeout()), SLOT(messaging()));
  messagingTimer->start(mMessagingTimerVal);

  extractMessages();
}

void CYEventsManager::read()
{
  foreach (CYEventsList *l, mEventsLists)
    l->read();
}

void CYEventsManager::newEventsReportFile(QString fileName, int projectId)
{
  if (fileName.isEmpty())
    return;
  CYEventsFileInfo *efi = new CYEventsFileInfo(fileName);
  efi->setProjectId(projectId);
  history.append(efi);
  save(mFileName);
  core->backupFile(mFileName);
  emit historyChanged();
}

void CYEventsManager::backupHistory(QDate date)
{
  QFileInfoList list = mDir->entryInfoList();
  if ( list.isEmpty() )
    return;
  QListIterator<QFileInfo> it(list);
  CYEventsFileInfo fi;

  history.clear();
  while (it.hasNext())
  {
    fi=(CYEventsFileInfo)it.next();
    history.clear();
    QString fileName = fi.absoluteFilePath();
    load(fileName);
    foreach (CYEventsFileInfo *file, history)
    {
      if (file->createdTime().date() <= date)
        file->setBackup(true);
    }
    save(fileName);
  }
  loading();
}

QDate CYEventsManager::backupHistoryInitDate()
{
  QDate date = QDate::currentDate();
  foreach (CYEventsFileInfo *file, history)
  {
    if (file->createdTime().date() < date)
      date = file->createdTime().date();
  }
  return date;
}

bool CYEventsManager::historyExists(int projectId)
{
  QString fileName;
  CYProject *prj = core->project(projectId);
  if (prj && prj->eventsReport())
  {
    fileName = prj->eventsReport()->fileName();
    if (fileName.isEmpty())
    {
      fileName = prj->eventsReport()->findFile(true)->fileName();
      prj->eventsReport()->setFileName(fileName);
    }
  }
  if (history.isEmpty() || !QFile::exists(fileName))
  {
    newEventsReportFile(fileName, projectId);
    return false;
  }
  return true;
}

void CYEventsManager::addEventsList(CYEventsList *list)
{
  mEventsLists.append(list);
}

void CYEventsManager::addEventsGenerator(CYEventsGenerator *generator)
{
  mEventsGenerators.append(generator);
}

CYEventsGenerator *CYEventsManager::eventsGenerator(QString name)
{
  foreach (CYEventsGenerator *generator, mEventsGenerators)
  {
    if (QString(generator->objectName())==name)
      return generator;
  }
  CYFATALTEXT(tr("Cannot find the events generator %1").arg(name));
}

void CYEventsManager::extractMessages()
{
  if (mExtractMessagesOk)
    return;

  QString languageName = core->currentLanguage();
  QString fileName = QString(core->appDataDir()+"messages_%1.txt").arg(languageName);
  QFile file(fileName);
  if (file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QTextStream stream(&file);
    foreach (CYEventsList *list, mEventsLists)
    {
      if (list->cydocIsEnabled())
      {
        for (int i = 0; i < list->mList.count(); ++i)
        {
          CYEvent *event = list->event(i);
          QString help = list->event(i)->help();
          stream << QString("%1\t%2\t%3\t%4\t%5\n").arg(list->objectName()).arg(i+1).arg(event->objectName()).arg(list->text(i, true)).arg(help);
          stream << "\n";
        }
      }
    }
    file.close();
  }

  core->createDocBook();
  mExtractMessagesOk = true;
}

void CYEventsManager::messaging()
{
  foreach (CYEventsGenerator *generator, mEventsGenerators)
  {
    generator->messaging();
  }
}


/*!
    \fn CYEventsManager::setMailingRecFlag( CYFlag * flag )
 */
void CYEventsManager::setMailingRecFlag( CYFlag * flag )
{
  mMailing->setRecFlag( flag );
}


/*!
    \fn CYEventsManager::addReportToMail(CYEventReport *report)
 */
void CYEventsManager::addReportToMail(CYEventReport *report)
{
  mMailing->addReport( report );
}


/*! Créer la journalisation de l'évènement de démarrage de CYLIX
    \fn CYEventsManager::cylixStart()
 */
void CYEventsManager::cylixStart()
{
  core->chekDefDatas();
  CYFlag *flag = (CYFlag *)core->findData("CY_EVENT_SPV_START");
  flag->setVal(true);
  core->setStarted();
}

/*! Créer la journalisation de l'évènement d'arrêt de CYLIX
    \fn CYEventsManager::cylixStop()
 */
void CYEventsManager::cylixStop()
{
  CYFlag *flag = (CYFlag *)core->findData("CY_EVENT_SPV_STOP");
  flag->setVal(true);
}


void CYEventsManager::createEventsDocBook(QString fileName)
{
  QFile file(fileName);
  core->mkdir(QFileInfo(file).absolutePath());
  CYAboutData *aboutData = cyapp->aboutData();
  if (file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    stream << "<appendix id=\"annexe_events\">";
    stream << "\n\t" << "<title>" << tr("Events list") + QString(" (%1-%2)").arg(aboutData->programName()).arg(aboutData->version())<< "</title>";
    int sect1 = 0;
    foreach (CYEventsList *list, mEventsLists)
    {
      if ((list->mList.count()>0) && list->cydocIsEnabled())
      {
        sect1++;
        stream << "\n\t\t" << QString("<sect1 id=\"annexe_events_%1\">").arg(sect1);
        stream << "\n\t\t\t" << "<title>" << list->objectName() << "</title>";
        stream << "\n\t\t\t\t" << "<para>";
        stream << list->cydocNote();
        stream << "\n\t\t\t\t\t" << "<table pgwide='1' frame='all'>";
        stream << "\n\t\t\t\t\t\t" << "<title>" << list->objectName() << "</title>";
        stream << "\n\t\t\t\t\t\t" << "<tgroup cols='1' align='left' colsep='1' rowsep='1'><colspec colwidth='1*'/>";
        stream << "\n\t\t\t\t\t\t\t" << "<thead>";
        stream << "\n\t\t\t\t\t\t\t\t" << "<row>";
        stream << "\n\t\t\t\t\t\t\t\t\t" << "<entry>" << tr("Name, message and help") << "</entry>";
        stream << "\n\t\t\t\t\t\t\t\t" << "</row>";
        stream << "\n\t\t\t\t\t\t\t" << "</thead>";
        stream << "\n\t\t\t\t\t\t\t" << "<tbody>";
        for (int i = 0; i < list->mList.count(); ++i)
        {
          CYEvent *event = list->event(i);
          if (event->cydocIsEnabled())
          {
            stream << "\n\t\t\t\t\t\t\t\t" << "<row>";
            stream << "\n\t\t\t\t\t\t\t\t\t" << "<entry>"
                   << event->objectName() << ": <emphasis role=\"bold\">"
                   << event->message(true).replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
                   << "</emphasis>";
            if (!event->help().isEmpty())
              stream << "\n" << CYCore::html2DocBook(event->help(), false);
            if (!event->cydocNote().isEmpty())
              stream << "\n" << event->cydocNote();
            stream << "</entry>";
            stream << "\n\t\t\t\t\t\t\t\t" << "</row>";
          }
        }
        stream << "\n\t\t\t\t\t\t\t" << "</tbody>";
        stream << "\n\t\t\t\t\t\t" << "</tgroup>";
        stream << "\n\t\t\t\t\t" << "</table>";
        stream << "\n\t\t\t\t" << "</para>";
        stream << "\n\t\t" << "</sect1>";
      }
    }
    stream << "\n" << "</appendix>";
    file.close();
  }
}
