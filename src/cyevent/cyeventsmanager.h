/***************************************************************************
                          cyeventsmanager.h  -  description
                             -------------------
    début                  : lun août 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYEVENTSMANAGER_H
#define CYEVENTSMANAGER_H

// QT
#include <QObject>
#include <QHash>
#include <QList>
#include <QDate>
#include <QDir>
// CYLIBS
#include "cytypes.h"

class CYEventsReport;
class CYEventReport;
class CYEventsFileInfo;
class CYEventsList;
class CYEventsGenerator;
class CYEventsMailing;
class CYDB;
class CYFlag;

/** @short Gestionnaire d'évènements.
  * @author Gérald LE CLEACH
  */

class CYEventsManager : public QObject
{
  Q_OBJECT
public:
  CYEventsManager(QObject *parent=0, const QString &name=0);
  ~CYEventsManager();

  /** Initialise le gestionnaire d'événements. */
  virtual void init() {}

  /** @return \a true si le gestionnaire d'évènements est activé. */
  bool isEnabled() { return mEnabled;}

  /** @return le journal d'évènements. */
  CYEventsReport *eventsReport(int projectId=0);

  /** Journalisation d'un évènement. */
  virtual void addReport(CYEventReport *report=0);

  /** @return le nom du fichier de journalisation. */
  QString reportfileName();
  /** Saisie le nom du fichier de journalisation. */
  void setReportFileName(const QString &filename);

  /** @return le répertoire d'archivages du journal d'évènements. */
  QString dirPath();
  /** Saisie le répertoire d'archivages du journal d'évènements. */
  void setDirPath(const QString &path);

  /** @return le fichier de traçabilité. */
  QFile *findFile();

  /** Charge l'historique des journaux d'évènements du fichier \a fN. */
  bool load(const QString &fN);
  /** Enregistre l'historique des journaux d'évènements dans le fichier \a fN. */
  bool save(const QString &fN);

  /** @return l'état du process. */
  virtual QString processState() { return 0; }
  /** Active ce gestionnaire d'évènements. */
  virtual void enable();
  /** Saisie du flag indiquant le changement de projet. */
  void setProjectChanged(bool val) { mProjetChanged = val; }

  /** Archiver l'historique à partir de la date \a date.*/
  void backupHistory(QDate date);
  /** @return \a true si l'historique existe. Sinon @return false et en créé un. */
  bool historyExists(int projectId=0);

  /** Ajoute une liste d'évènements. */
  virtual void addEventsList(CYEventsList *list);
  /** Ajoute un générateur d'évènements. */
  virtual void addEventsGenerator(CYEventsGenerator *generator);
  /** @return le générateur d'évènements ayant pour nom \a name. */
  virtual CYEventsGenerator *eventsGenerator(QString name);

  /** @return la liste des alarmes. */
  CYEventsList *alerts() { return mAlerts; }
  /** @return la liste des défauts. */
  CYEventsList *faults() { return mFaults; }
    void setMailingRecFlag( CYFlag * flag );

  virtual void cylixStart();
  virtual void cylixStop();

  /** @return la base de données.
    * Chaque donnée est un évènement */
  CYDB *db() { return mDB; }

  /** Création du fichier "annexe_events.xml" relatif aux évènements
   * et à la langue en cours puis génère le manuel utilisateur de cette
   * langue en un fichier html dans le répertoire "/tmp" */
  virtual void createEventsDocBook(QString fileName);

signals: // Signals
  /** Emis à chaque nouvel évènement. */
  void newEvent(CYEventReport *report);
  /** Emis à chaque changement de l'historique. */
  void historyChanged();
  /** Donne l'ordre de rafraichir les valeurs de la barre de messagerie. */
  void refreshingMessagingValues();

public slots: // Public slots
  /** Chargement des journal d'évènements à partir de tous les fichiers du répertoire courrant. */
  void loading();
  /** Traitement pour chaque changement de projet.*/
  virtual void projectChanged();
  /** Traitement pour chaque changement de projet.
    * @param index  Index de projet (0 si un seul project actif à la fois).*/
  virtual void projectChanged(int poste);
  /** Traitement pour chaque changement d'essai.
    * @param projectId  Index de projet (0 si un seul project actif à la fois).*/
  void testChanged(int projectId);
  /** Traitement réalisé lors de la création d'un nouveau fichier de journal d'évènements.
    * @param projectId  Index de projet.*/
  void newEventsReportFile(QString fileName, int projectId);
  /** Date d'initialisation d'archivage de l'historique. */
  QDate backupHistoryInitDate();
  /** Extractions de tous les messages d'évènement. */
  void extractMessages();
  /** Lecture des évènements. */
  virtual void read();
  /** Rafraichit les messageries. */
  virtual void messaging();
    virtual void addReportToMail(CYEventReport *report);

public: // Public attributes
  /** Liste des journaux d'évènements constituant l'historique du banc. */
  QList<CYEventsFileInfo*> history;
  /** Dictionnaire d'affichages / type d'évènements. */
  QHash<QString, bool*> showTypes;
  /** Dictionnaire d'affichages / provenance d'évènements. */
  QHash<QString, bool*> showFroms;

protected: // Protected attributes
  /** Répertoire d'archivage. */
  QDir *mDir;
  /** Nom du fichier de traçabilité courrant. */
  QString mFileName;
  /** Numéro du fichier de traçabilité courrant. */
  uint mFileNum;
  /** Listes des listes d'évènements. */
  QList<CYEventsList*> mEventsLists;
  /** Listes des générateurs d'évènements. */
  QList<CYEventsGenerator*> mEventsGenerators;
  /** Valeur du timer de lecture des évènements. */
  int mReadTimerVal;
  /** Valeur du timer de rafraichissements des messageries. */
  int mMessagingTimerVal;
  /** Indique le les messages ont déja eté mis dans un fichier. */
  bool mExtractMessagesOk;

  /** Liste des alarmes. */
  CYEventsList *mAlerts;
  /** Liste des défauts. */
  CYEventsList *mFaults;
  /** Liste des évènements générées par l'application. */
  CYEventsList *mEvents;

  /** Gestionnaire d'évènements activé. */
  bool mEnabled;

  /** Gestion d'envoi par mail de message d'évènements. */
  CYEventsMailing * mMailing;

  CYDB *mDB;

  bool mProjetChanged;
};

#endif
