/***************************************************************************
                          cyeventreport.cpp  -  description
                             -------------------
    begin                : ven nov 21 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyeventreport.h"

CYEventReport::CYEventReport(QObject *parent, const QString &name, QColor color, QDateTime time, QDateTime since, QString type, QString from, QString desc)
  : QObject(parent),
    mColor(color),
    mDateTime(time),
    mSinceDateTime(since),
    mType(type),
    mSender(from),
    mDesc(desc)
{
  setObjectName(name);
  mIndexStation = -1;
}

CYEventReport::~CYEventReport()
{
}

QString CYEventReport::record(bool withColor)
{
  QString txt;
  if ( withColor )
    txt = QString("%1\t%2\t%3\t%4\t%5\t%6\t%7\n").arg(mColor.name()).arg(mDateTime.toString(Qt::ISODate)).arg(mSinceDateTime.toString(Qt::ISODate)).arg(mType).arg(mSender).arg(mDesc).arg(objectName());
  else
    txt = QString("%1\t%2\t%3\t%4\t%5\t%6\n").arg(mDateTime.toString(Qt::ISODate)).arg(mSinceDateTime.toString(Qt::ISODate)).arg(mType).arg(mSender).arg(mDesc).arg(objectName());
  return txt;
}
