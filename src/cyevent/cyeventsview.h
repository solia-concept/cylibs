/***************************************************************************
                          cyeventsview.h  -  description
                             -------------------
    begin                : ven oct 31 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYEVENTSVIEW_H
#define CYEVENTSVIEW_H

// QT
#include <QUrl>
#include <QShowEvent>
#include <QTreeWidget>
// CYLIBS
#include "cywidget.h"
#include "cyeventswindow.h"

class CYEventReport;
class CYEventsReport;
class CYEventHistory;
class CYEventsFileInfo;

namespace Ui {
    class CYEventsView;
}

/** @short Widget d'affichage d'un journal d'évènements.
  * @author Gérald LE CLEACH
  */

class CYEventsView : public CYWidget
{
  Q_OBJECT
public:
  CYEventsView(CYEventsWindow *parent=0, const QString &name=0);
  ~CYEventsView();

  void setProject(CYProject *prj) { mProject =prj; }

  /** Ajoute la journalisation d'un évènement. */
  void addReport(CYEventReport *report=0);

  /** Ajoute un fichier d'évènements d'un essai de projet, soit un élément de l'historique de la machine. */
  void addEventsReport(CYEventsFileInfo *file);
  /** Saisie le fichier du journal d'évènements à visualiser. */
  void setEventsReportFile(QString fileName);
  /** Saisie le fichier du journal d'évènements à visualiser. */
  void setEventsReportFile(CYEventsFileInfo file);

  QTreeWidget *eventsView();

signals:
  /** Place l'url passé en argument comme récent. */
  void announceRecentURL(const QUrl &url);
  /** Signale la fin du rafraîchissement de l'affichage des évènement. */
  void eventsRefreshed();

public slots: // Public slots
  /** Rafraîchit l'affichage des évènement. */
  void refreshEvents();
  /** Rafraîchit l'affichage d'un évènement. */
  void refreshEvent(CYEventReport *report);
  /** Rafraîchit l'affichage de l'historique. */
  void refreshHistory();

  /** Chargement d'un journal d'évènements. */
  void load();
  /** Chargement d'un journal d'évènements par lien url.
   * @param url url du fichier. */
  void load(const QUrl &url);
  /** Recharge le journal d'évènements courrant (celui du projet en cours). */
  void current();

  /** Met à jour la vue par rapport à l'historique. */
  void historyChanged();
  /** Traîtement à chaque changement de sélection du journal d'évènements. */
  void selectionChanged(QTreeWidgetItem *item,QTreeWidgetItem *previous);
  /** Traîtement à chaque changement de sélection d'évènement. */
  void selectionEventChanged(QTreeWidgetItem *item,QTreeWidgetItem *previous);

protected: // Protected methods
  /** Fonction appelée à l'affichage du widget. */
  virtual void showEvent(QShowEvent *e);

protected: // Protected attributes
  /** Journal d'évènements. */
  CYEventsReport *mEventsReport;
  CYEventsWindow *mWindow;
  CYProject *mProject;

private:
  Ui::CYEventsView *ui;
};

#endif
