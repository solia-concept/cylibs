//
// C++ Interface: cyeventsgeneratorspv
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYEVENTSGENERATORSPV_H
#define CYEVENTSGENERATORSPV_H

// CYLIBS
#include "cyeventsgenerator.h"
#include "cyevent.h"

/**
  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYEventsGeneratorSPV : public CYEventsGenerator
{
  Q_OBJECT
public:
  CYEventsGeneratorSPV(QObject *parent = 0, const QString &name = 0, int host=-1, int com=-1);
  ~CYEventsGeneratorSPV();

  virtual void init();
  /** Saisie l'évènement dans la liste des évènements superviseurs. */
  void addToEventsList(CYEvent *event);

public slots:
  /** Acquitement des défauts et alertes à acquiter.
   * @brief acknowledge
   */
  virtual void acknowledge();

protected: // Protected attributes
  /** Liste des évènements superviseurs. */
  CYEventsList *mEvents;

protected:
  virtual void init_CYFD();
  virtual void init_CYEV();
  virtual void init_CYAcquisition();
};

#endif
