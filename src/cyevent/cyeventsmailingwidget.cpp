#include "cyeventsmailingwidget.h"
#include "ui_cyeventsmailingwidget.h"

// CYLIBS
#include "cycore.h"
#include "cyuser.h"

CYEventsMailingWidget::CYEventsMailingWidget(QWidget *parent, const QString &name)
  : CYWidget(parent,name), ui(new Ui::CYEventsMailingWidget)
{
  ui->setupUi(this);
  connect( ui->testButton, SIGNAL( clicked() ), core, SIGNAL( testEventsMail() ) );
}

CYEventsMailingWidget::~CYEventsMailingWidget()
{
  delete ui;
}

/*! Fonction appelée à l'affichage du widget.
    \fn CYEventsMailingWidget::showEvent(QShowEvent *e)
 */
void CYEventsMailingWidget::showEvent(QShowEvent *e)
{
  CYWidget::showEvent( e );
  if ( isVisible() )
  {
    if ( core->user() && ( core->user()->administrator() || core->user()->designer() || core->user()->simulation() ) )
    {
      ui->box->setReadOnly(false);
    }
    else
    {
      QString msg = tr( "You must be in administrator access to modifie this values" );
      ui->box->setToolTip( msg );
      ui->box->setReadOnly(true);
    }
  }
}
