/***************************************************************************
                          cyeventnum.h  -  description
                             -------------------
    begin                : mar nov 9 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYEVENTNUM_H
#define CYEVENTNUM_H

// CYLIBS
#include "cyevent.h"

/** @short Evènement généré par l'égalité d'une valeur numérique et d'un numéro de référence.
  * La valeur numérique correspond au numéro de l'évènement courant.
  * @author LE CLÉACH Gérald
  */

class CYEventNum : public CYEvent
{
public: 
  /** Attr1 est une structure de saisie simple des attributs d'un évènement généré par
    * l'égalité d'une valeur numérique et d'un numéro de référence avec une couleur
    * spécifique en état actif. */
  struct Attr1
  {
    /** Numéro de référence. */
    s16 num;
    /** Type de traîtement à effectuer. */
    int proc;
    /** Description. */
    QString desc;
    /** Couleur en état actif. */
    QColor color;
  };

  /** Création d'un évènement à partir des états du grafcet avec les attributs Attr1.
    * @param parent Parent.
    * @param name   Nom.
    * @param val    Valeur numérique.
    * @param attr   Attributs. */
  CYEventNum(CYEventsGenerator *generator, const QString &name, s16 *val, Attr1 *attr);
  ~CYEventNum();
  
  /** Lit la valeur et en déduit si l'évènement est actif. */
  virtual void read();

protected: // Protected attributes
  /** Pointe sur le numéro d'évènement courant. */
  s16 *mNumVal;
  /** Numéro de référence. */
  s16 mNumRef;
};

#endif
