/***************************************************************************
                          cyeventswindow.h  -  description
                             -------------------
    begin                : ven oct 31 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYEVENTSWINDOW_H
#define CYEVENTSWINDOW_H

// CYLIBS
#include <cywin.h>

class CYEventsView;
class CYFlag;
class CYActionMenu;
class CYProject;

/** @short Fenêtre d'affichage des journaux d'évènements.
  * @author Gérald LE CLEACH
  */

class CYEventsWindow : public CYWin
{
  Q_OBJECT
public:
  /** Construction de la fenêtre d'affichage des journaux d'évènements.
    * @param name     Nom de la fenêtre.
    * @param index    Index de la fenêtre.
    * @param label    Libellé de la fenêtre. */
  CYEventsWindow(const QString &name, int index, const QString &label);
  ~CYEventsWindow();

  virtual void createGUI(const QString &xmlfile=0);

  /** @return l'index de la colonne ayant pour texte \a text.
      @return -1 si cette colonne n'existe pas. */
  int column(QString text);
  void showColumns(CYFlag *flag, CYAction *action);

  /** @return \a true pour affichée la retombée des évènements. */
  bool showEndAction();
  /** @return \a true pour affichée l'acquittement des évènements. */
  bool showAckAction();

public slots:
  virtual void show(int projectId);
  virtual void show();

  /** Archiver l'historique. */
  void backup();

  /** Met à jour l'affichage des colonnes. */
  virtual void showColumns();
  virtual void showColumnName();
  virtual void showColumnTime();
  virtual void showColumnSince();
  virtual void showColumnType();
  virtual void showColumnFrom();
  virtual void showColumnDesc();

protected: // Protected attributes
  /** Widget d'affichage du journal d'évènements. */
  CYEventsView *mView;

  CYActionMenu *mShowColums;
  CYActionMenu *mShowTypeMenu;
  CYActionMenu *mShowFromMenu;

  CYFlag *mShowNameFlag;
  CYFlag *mShowTimeFlag;
  CYFlag *mShowSinceFlag;
  CYFlag *mShowTypeFlag;
  CYFlag *mShowFromFlag;
  CYFlag *mShowDescFlag;

  CYAction *mShowNameAction;
  CYAction *mShowTimeAction;
  CYAction *mShowSinceAction;
  CYAction *mShowTypeAction;
  CYAction *mShowFromAction;
  CYAction *mShowDescAction;

  CYAction *mShowEndAction;
  CYAction *mShowAckAction;

  QHash<QString, CYAction*> mShowTypesActions;
  QHash<QString, CYAction*> mShowFromsActions;

  CYProject *mProject;

private slots:
  virtual void optionsPreferences();
  virtual void showAllTypes();
  virtual void showNoneType();
  virtual void updateShowTypes();
  virtual void showAllFroms();
  virtual void showNoneFrom();
  virtual void updateShowFroms();
};

#endif
