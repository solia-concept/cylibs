#ifndef CYEVENTSMAILINGWIDGET_H
#define CYEVENTSMAILINGWIDGET_H

// CYLIBS
#include "cywidget.h"

namespace Ui {
    class CYEventsMailingWidget;
}

class CYEventsMailingWidget : public CYWidget
{
  Q_OBJECT
public:
  CYEventsMailingWidget(QWidget *parent=0, const QString &name=0);

  ~CYEventsMailingWidget();

protected:
  virtual void showEvent(QShowEvent *e);

private:
  Ui::CYEventsMailingWidget *ui;
};

#endif // CYEVENTSMAILINGWIDGET_H
