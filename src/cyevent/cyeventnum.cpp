/***************************************************************************
                          cyeventnum.cpp  -  description
                             -------------------
    begin                : mar nov 9 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyeventnum.h"

// CYLIBS
#include "cyeventsgenerator.h"

CYEventNum::CYEventNum(CYEventsGenerator *generator, const QString &name, s16 *val, Attr1 *attr)
: CYEvent(generator, name, attr->desc, attr->color),
  mNumVal(val),
  mNumRef(attr->num)  
{
  mVal  = new flg;
  *mVal = 0;
  mDesc.prepend(QString("%1 :").arg(mNumRef));
  mProcessing = attr->proc;
}

CYEventNum::~CYEventNum()
{
}

void CYEventNum::read()
{
  if ((*mNumVal) == mNumRef)
    *mVal = 1;
  else
    *mVal = 0;
  CYEvent::read();
}
