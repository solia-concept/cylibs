/***************************************************************************
                          cyeventslist.cpp  -  description
                             -------------------
    début                  : lun aoû 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyeventslist.h"

// CYLIBS
#include "cycore.h"
#include "cyevent.h"
#include "cyeventsmanager.h"
#include "cydb.h"
#include "cystring.h"
#include "cyflag.h"

CYEventsList::CYEventsList(QObject *parent, const QString &name)
  : QObject(parent)
{
  setObjectName(name);

  mCurrentEvent = 0;
  mDataMessage  = 0;
  mOldEvent     = 0;
  mOldEnable    = false;
  mSimultaneousEvents = true;
  mEnableRead   = true;
  mCydocEnable  = true;

  mDB = new CYDB(this, QString("db_%1").arg(name));
  enableMailing = new CYFlag( mDB, "ENABLE_MAILING", new flg, false);

  connect(this, SIGNAL(messaging()), core->events, SLOT(messaging()));
}

CYEventsList::~CYEventsList()
{
}

void CYEventsList::add(CYEvent *event)
{
  event->setList( this );
  // Récupère par défaut la couleur de l'événement précédent.
  // La couleur de la première alerte Cylibs s'initialise ainsi par la même couleur que la dernière définie dans Cylix.
  mColor = event->enableColor();
  if ( event->processing() & REC )
  {
/*    if (!event->reportUp().isEmpty() && !core->events->showTypes.find(event->reportUp()))*/
    if (!core->events->showTypes.value(event->reportUp()))
    {
      bool *show = new bool;
      *show = true;
      core->events->showTypes.insert(event->reportUp().toUtf8(), show);
    }
/*    if (!core->events->showTypes.find(event->reportEnd()))
    {
      bool *show = new bool;
      *show = true;
      core->events->showTypes.insert(event->reportEnd().toUtf8(), show);
    }
    if (!core->events->showTypes.find(event->reportAcknow()))
    {
      bool *show = new bool;
      *show = true;
      core->events->showTypes.insert(event->reportAcknow().toUtf8(), show);
    }*/
    if (!core->events->showFroms.value(event->sender()))
    {
      bool *show = new bool;
      *show = true;
      core->events->showFroms.insert(event->sender().toUtf8(), show);
    }
  }
  mList.append(event);
}

void CYEventsList::addList(QList<CYEvent*> events)
{
  for (int i=0; i<events.count(); ++i)
    mList.append(events.at(i));
}

void CYEventsList::read()
{
  if (!mEnableRead || mList.count()==0)
    return;

  // lecture reseau
  for (int i=0; i<mList.count(); i++)
    mList.at(i)->read();

  if (!mSimultaneousEvents)
  {
    QString oldText = mText;
    message();
/*    if (mText!=oldText) a_voir
      emit messaging();*/
  }
  else
  {
    if ( mOldEnable && !mList.at( mOldEvent )->isEnable()) // disparition de l'evt du message actif
    {
      message();
      emit messaging();
    }
    else if ( !mOldEnable && mList.at( mCurrentEvent )->isEnable()) // Aucun message => Au moins un message
    {
      message();
      emit messaging();
    }
    else if (!mOldEnable)
    {
      mCurrentEvent++;
      if (mCurrentEvent>=mList.count())
        mCurrentEvent = 0;
      return;
    }

    mOldEnable = mList.at(mCurrentEvent)->isEnable();
    mOldEvent  = mCurrentEvent;
  }
}

QString CYEventsList::message()
{
  QString msg = QString("0~0~0~");

  if (mList.count()==0)
    return msg;

  int current = mCurrentEvent;
  int cpt = mList.count()-1;

  current++;
  if (current>=mList.count())
    current = 0;

  while (cpt--)
  {
    if (mList.at(current)->isEnable())  // message actif
      break;
    current++;
    if (current>=mList.count())
      current = 0;
  }

  mCurrentEvent = current;

  if (mList.at(current)->isEnable())  // Affiche le message actif
  {
		CYEvent * event = mList.at(current);
		mColor = event->enableColor();
    mText  = text(current);

    msg = QString("%1~%2~%3~%4").arg(mColor.red()).arg(mColor.green()).arg(mColor.blue()).arg(mText);
    if ( mDataMessage )
    {
      mDataMessage->setVal(mText);
			mDataMessage->setEnableColor(mColor);
			if (!event->help().isEmpty())
			{
				/* L'aide en bulle de la messagerie est enrichie par l'aide complète de l'événement en cours.
				Dans cette aide complémentaire peuvent se trouver une ou plusieurs données (mesures, consignes, paramètres...)
				permettant de visualiser directement les valeurs qui peuvent influencer le déclenchement de l'événement.
				Ces valeurs, en gras, sont rafraîchies lors de l'affichage de l'aide, soit à l'apparition de l'aide en bulle.*/
				static bool init_note = false;
				if (!init_note)
				{
					init_note = true;
					mDataMessage->addNote(tr("One or more data (measurements, setpoints, parameters...) allow to visualize directly values which can influence the event triggering. These values, in bold, are refreshed when the help is displayed."));
					mDataMessage->addNote(tr("One or more hyperlinks allow to open the Cylix manual directly at a precise place, as for example the part relating to the setting which can influence the event triggering.")+"<br>"+tr("These links, which are only visible in the tooltip, can be activated at the bottom of the window of events logs."));
				}
				mDataMessage->setHelp(QString("<b>%1 %2 : %3</b><br>%4<hr>").arg(event->reportUp()).arg(event->objectName()).arg(mText).arg(event->helpHTML()));
			}
		}
  }
  else
  {
    msg = QString("0~0~0~");
    if ( mDataMessage )
      mDataMessage->setVal("");
  }

  return msg;
}

QString CYEventsList::message(int i)
{
  mCurrentEvent = i;

  mColor = mList.at(i)->enableColor();
  mText  = text(i);
  if ( mDataMessage )
  {
    mDataMessage->setVal(mText);
    mDataMessage->setEnableColor(mColor);
  }

  return QString("%1~%2~%3~%4").arg(mColor.red()).arg(mColor.green()).arg(mColor.blue()).arg(mText);
}

QString CYEventsList::message(QString &msg, QColor &color, QString &help)
{
  QString name;
  return message(msg, color, help, name);
}

QString CYEventsList::message(QString &msg, QColor &color, QString &help, QString &name)
{
  QString txt = message();

  color = QColor();
  msg   = "";
  help  = "";
  name  = "";
  if (mList.at(mCurrentEvent) && mList.at(mCurrentEvent)->isEnable())  // Si dernier évènement actif toujours actif
  {
    color = mColor;
    msg   = mText;
    help  = mList.at(mCurrentEvent)->help();
    name  = mList.at(mCurrentEvent)->objectName();
  }
  return txt;
}

QString CYEventsList::message(int i, QString &msg, QColor &color, QString &help)
{
  QString txt = message(i);

  color = mColor;
  msg   = mText;
  help  = mList.at(mCurrentEvent)->help();

  return txt;
}

QString CYEventsList::text(int i, bool oneLine)
{
  return mList.at(i)->message(oneLine);
}

void CYEventsList::setDataMessage(CYString *data)
{
  mDataMessage = data;
}

void CYEventsList::setDataMessage(QString dataName)
{
  mDataMessage = (CYString *)core->findData(dataName);
  if (!mDataMessage)
    CYFATALOBJECTDATA(objectName(), dataName);
}

bool CYEventsList::exists(QString eventName)
{
  for (int i=0; i<mList.count(); ++i)
    if (mList.at(i)->objectName() == eventName)
      return true;
  return false;
}

CYEvent * CYEventsList::event(int i)
{
  return mList.at(i);
}


int CYEventsList::nbEventsEnable()
{
  int cpt=0;
  for (int i=0; i<mList.count(); ++i)
    if (mList.at(i)->isEnable())
      cpt++;
  return cpt;
}


/*! Force la valeur de l'évènement \a id_event dans la liste à \a val.
    Force les autres à l'état inverse.
    \fn CYEventsList::setEnable(int id_event)
 */
void CYEventsList::setEventVal(int id_event, flg val)
{
  for (int i=0; i<mList.count(); ++i)
  {
    if (id_event==(int)i)
      mList.at(i)->setVal(val);
    else
      mList.at(i)->setVal(!val);
  }
}

void CYEventsList::setCydoc(int index, int index_ref, QString note)
{
  mCydocNote = note;
  mCydocEnable = (index==index_ref) ? true : false;
}
