/***************************************************************************
                          cyeventsview.cpp  -  description
                             -------------------
    begin                : ven oct 31 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyeventsview.h"
#include "ui_cyeventsview.h"

// QT
#include <QStackedWidget>
#include <QFileDialog>
// CYLIBS
#include "cycore.h"
#include "cyproject.h"
#include "cyeventreport.h"
#include "cyeventsreport.h"
#include "cyeventsmanager.h"
#include "cyeventsfileinfo.h"
#include "cyeventsviewitem.h"
#include "cyevent.h"
#include "cystring.h"
#include "cymessagebox.h"

CYEventsView::CYEventsView(CYEventsWindow *parent, const QString &name )
  : CYWidget(parent,name),
    mWindow(parent)
{
  ui=new Ui::CYEventsView;
  ui->setupUi(this);

  ui->evtView->sortItems( 1, Qt::AscendingOrder );
  mEventsReport = 0;
//  refreshHistory();
  connect(core->events, SIGNAL(newEvent(CYEventReport *)), SLOT(refreshEvent(CYEventReport *)));
  connect(core->events, SIGNAL(historyChanged()), SLOT(historyChanged()));
  connect(ui->prjView, SIGNAL(currentItemChanged(QTreeWidgetItem *,QTreeWidgetItem *)), SLOT(selectionChanged(QTreeWidgetItem *,QTreeWidgetItem *)));
  connect(ui->evtView, SIGNAL(currentItemChanged(QTreeWidgetItem *,QTreeWidgetItem *)), SLOT(selectionEventChanged(QTreeWidgetItem *,QTreeWidgetItem *)));

  mProject = core->project(0);
  mTimer->start(250);
}

CYEventsView::~CYEventsView()
{
  delete ui;
}

void CYEventsView::addReport(CYEventReport *report)
{
  QStringList list;
  list<<report->objectName()
     <<report->dateTime().toString(Qt::ISODate).replace(QChar('T'), " " )
    <<report->sinceDateTime().toString(Qt::ISODate).replace(QChar('T'), " " )
   <<report->type()
  <<report->from()
  <<report->desc();
  CYEventsViewItem *item = new CYEventsViewItem(ui->evtView, list);
  item->setTextColor(report->color());
  ui->evtView->addTopLevelItem(item);
}

void CYEventsView::refreshEvents()
{
  if (!mEventsReport)
    return;

  ui->evtView->clear();
  foreach (CYEventReport *report, mEventsReport->reports)
    refreshEvent(report);

  emit eventsRefreshed();
}

void CYEventsView::refreshEvent(CYEventReport *report)
{
  bool hide = false;
  if (!report->sinceDateTime().isNull() && report->desc().endsWith("!")) // Si acquittement d'évènement
  {
    if (!mWindow->showAckAction()) // Si acquittement d'évènement à cacher
      hide = true;
  }
  else if (!report->sinceDateTime().isNull()) // Si fin d'évènement
  {
    if (!mWindow->showEndAction())  // Si fin d'évènement à cacher
      hide = true;
  }

  if (!hide)
  {
    bool *checkedTypes;
    checkedTypes = core->events->showTypes.value(report->type());
    bool *checkedFroms;
    checkedFroms = core->events->showFroms.value(report->from());
    if (checkedTypes && checkedFroms)
    {
      if (*checkedTypes && *checkedFroms)
        addReport(report);
    }
    else  // Dans le cas où le type et la provenance ne peuvent être trouvé dans la langue et l'encodage en cours
    {
      CYEvent *event = core->findEvent(report->objectName(), false, false);
      if (event)
      {
        checkedTypes = core->events->showTypes.value(event->reportUp());
        checkedFroms = core->events->showFroms.value(event->sender());
        if (checkedTypes && *checkedTypes && checkedFroms && *checkedFroms)
          addReport(report);
        else if (checkedTypes && *checkedTypes && !checkedFroms)
          addReport(report);
        else if (!checkedTypes && checkedFroms && *checkedFroms)
          addReport(report);
        else if (!checkedTypes && !checkedFroms)
          addReport(report);
      }
      else
        addReport(report);
    }
  }
}

void CYEventsView::addEventsReport(CYEventsFileInfo *file)
{
  QStringList list;
  list<<file->createdTimeToString()<<file->filePath();
  QTreeWidgetItem *item = new QTreeWidgetItem(ui->prjView, list);
  ui->prjView->addTopLevelItem(item);
}

void CYEventsView::refreshHistory()
{
  ui->prjView->clear();
  foreach (CYEventsFileInfo *file, core->events->history)
  {
    if (!mProject || (mProject->index()==file->projectId()))
      addEventsReport(file);
  }
  current();
}

void CYEventsView::current()
{
  int idPrj = (mProject) ? mProject->index() : 0;
  if (core && core->events && core->events->eventsReport(idPrj))
  {
    setEventsReportFile(core->events->eventsReport()->fileName());
    // TODO QT5
//    int idx = ui->prjView->topLevelItemCount();
//    QAbstractItemModel *model = ui->prjView->model();
//    QItemSelectionModel *selection = new QItemSelectionModel(model);
//    QModelIndex index = model->index(idx,1);
////    QItemSelectionModel *selection = ui->prjView->selectionModel();
//    selection->select(index, QItemSelectionModel::Select | QItemSelectionModel::Rows);
//    ui->prjView->setCurrentItem(ui->prjView->topLevelItemCount());
//    ui->prjView->setSelected(ui->prjView->lastItem(), true);
//    ui->prjView->ensureItemVisible(ui->prjView->lastItem());
  }
}

void CYEventsView::setEventsReportFile(QString fileName)
{
  setEventsReportFile(CYEventsFileInfo(fileName));
}

void CYEventsView::setEventsReportFile(CYEventsFileInfo file)
{
  if (file.exists() && file.dir().exists())
  {
    ui->widgetStack->setCurrentIndex(0);
    ui->evtView->clear();
    if (mProject && mProject->eventsReport() && (mProject->eventsReport()->fileName()==file.filePath()) )
    {
      mEventsReport = mProject->eventsReport();
    }
    else
    {
      mEventsReport = new CYEventsReport(this, "CYEventsReport", file.path());
    }
    mEventsReport->load(file.filePath());
    refreshEvents();
  }
  else if (!file.dir().exists())
  {
    ui->widgetStack->setCurrentIndex(1);
  }
  else
  {
    ui->widgetStack->setCurrentIndex(2);
  }
  for (int col=0; col<ui->prjView->columnCount(); col++)
    ui->prjView->resizeColumnToContents(col);
  for (int col=0; col<ui->evtView->columnCount(); col++)
    ui->evtView->resizeColumnToContents(col);
}

void CYEventsView::load()
{
  QString fileName = QFileDialog::getOpenFileName(this,
                                                  tr("Select a events report to load"),
                                                  mEventsReport->path(),"*.cyevt");
  load(QUrl(fileName));
}

void CYEventsView::load(const QUrl &url)
{
  if (url.toString().isEmpty())
    return;

  setEventsReportFile(url.toString());
  emit announceRecentURL(QUrl(url));
}

void CYEventsView::historyChanged()
{
  addEventsReport(core->events->history.last());
  if (isVisible() && CYMessageBox::warningYesNo(this, tr("The current events file changed!\nDo you want to view this one?")) == CYMessageBox::Yes)
    current();
}

void CYEventsView::selectionChanged(QTreeWidgetItem *item,QTreeWidgetItem *previous)
{
  Q_UNUSED(previous)
   if (!item)
    return;

  CYString *help = (CYString *)core->findData("CY_EVENT_VIEWER_HELP");
  help->setVal(0);

  foreach (CYEventsFileInfo *file, core->events->history)
  {
    if (file->createdTimeToString() == item->text(0) &&
       (!mProject || mProject->index()==file->projectId()))
    {
      setEventsReportFile(*file);
      return;
    }
  }
  CYMESSAGE;
}

void CYEventsView::selectionEventChanged(QTreeWidgetItem *item,QTreeWidgetItem *previous)
{
  Q_UNUSED(previous)
  if (!item)
    return;

  CYString *help = (CYString *)core->findData("CY_EVENT_VIEWER_HELP");

  ui->evtView->setToolTip(0);
  help->setVal(0);

  CYEvent *event = core->findEvent(item->text(0), false, false);
  if (event)
  {
    QString desc = item->text(5);

    QString txt = (!event->help().isEmpty()) ? event->help() : tr("No help provided !");
    txt = event->helpHTML();

		help->setVal(QString("<b><font size=+1>%1 <code>%2</code> : %3</font></b><br>%4").arg(event->reportUp()).arg(event->objectName()).arg(desc).arg(txt));

    ui->evtView->setToolTip(help->val());
  }
}

void CYEventsView::showEvent(QShowEvent *e)
{
  refreshHistory();
  CYWidget::showEvent(e);
}

QTreeWidget *CYEventsView::eventsView()
{
  return ui->evtView;
}
