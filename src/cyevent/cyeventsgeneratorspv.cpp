//
// C++ Implementation: cyeventsgeneratorspv
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
// CYLIBS
#include "cycore.h"
#include "cyflag.h"
#include "cyevent.h"
#include "cyeventslist.h"
#include "cyeventsmanager.h"
#include "cynetmodule.h"
#include "cynetlink.h"
#include "cynethost.h"
#include "cystring.h"
#include "cyf32.h"
#include "cyeventsgeneratorspv.h"
#include "cyacquisition.h"

CYEventsGeneratorSPV::CYEventsGeneratorSPV(QObject *parent, const QString &name, int host, int com)
 : CYEventsGenerator(parent, name, host, com)
{
  core->eventsGeneratorSPV = this;
  // EVNTS SUPERVISEUR
  mEvents = new CYEventsList(this, tr("Supervisor Events"));
  core->events->addEventsList(mEvents);
}


CYEventsGeneratorSPV::~CYEventsGeneratorSPV()
{
}


void CYEventsGeneratorSPV::init()
{
  core->setSplashMessage(tr("Initializing supervisor events..."));

  mNetOk = true;

  init_CYFD();
  init_CYEV();
  init_CYAcquisition();
}


void CYEventsGeneratorSPV::addToEventsList(CYEvent *event)
{
  if (event)
    mEvents->add(event);
}

void CYEventsGeneratorSPV::acknowledge()
{
  QHashIterator<QString, CYAcquisition *> it(core->acquisitions);
  while (it.hasNext())
  {
    it.next();                   // must come first
    CYAcquisition *acquisition = it.value();
    acquisition->backup2Error->setVal(false);
  }
}

/*!
    \fn CYEventsGeneratorSPV::init_CYFD()
 */
void CYEventsGeneratorSPV::init_CYFD()
{
  QString module_desc, module_help;

  if (!core->module->isModbus())
  {
    module_desc = tr("RS422 communication module not installed");
    module_help = tr("The RS422 communication module has not been correctly installed. Reinstall Cylix.");
  }
  else
  {
    module_desc = tr("Ethernet interprocess communication not installed");
    module_help = tr("The Ethernet interprocess communication software was not detected at startup Cylix. Reinstall Cylix.");
    module_help.append("<br>");
    module_help.append(tr("Please note that under Windows you have to install Cylix as administrator even if the current Windows session is already in administrator mode. To do this, right-click on the installation file to access this command."));
  }

  CYEvent::Attr5 attr[]=
  {
    { "CYFD_MODULE_INST", &core->module->installed, SI_0, SCR|REC, module_desc, QString("CYLIX"), 0, QColor(Qt::red), module_help},
    { "CYFD_MODULE_RUN" , &core->module->running  , SI_0, SCR|REC, tr("Ethernet interprocess communication"), QString("CYLIX"), 0, QColor(Qt::red), tr("Cylix cannot communicate with the Ethernet interprocess communication software. Restart PC.")},
  };
  int nb = sizeof(attr)/sizeof(attr[0]);
  for (int i=0; i<nb; i++)
  {
    if (!core->module->isModbus() && (attr[i].name=="CYFD_MODULE_RUN"))
      continue;
    CYEvent *event = new CYEvent(this, &attr[i]);
    event->setLocal(true);
    core->events->faults()->add(event);
  }
}

/*!
    \fn CYEventsGeneratorSPV::init_CYEV()
 */
void CYEventsGeneratorSPV::init_CYEV()
{
  CYEvent::Attr5 attr[]=
  {
    { "CYEV_SPV_START"   , ((CYFlag *)core->findData("CY_EVENT_SPV_START"))->ptrVal()   , SI_1, REC|RAZ, tr("Supervisor starting"), QString("CYLIX"), 0, QColor(Qt::yellow), tr("Time stamp of starting the cylix software.")},
    { "CYEV_SPV_STOP"    , ((CYFlag *)core->findData("CY_EVENT_SPV_STOP"))->ptrVal()    , SI_1, REC|RAZ, tr("Supervisor stoping") , QString("CYLIX"), 0, QColor(Qt::yellow), tr("Time stamp of stopping the cylix software.")},
    { "CYEV_METRO_CAL"   , ((CYFlag *)core->findData("CY_EVENT_METRO_CAL"))->ptrVal()   , SI_1, REC|RAZ, "DATA_REC1 : "+tr("New calibration") , QString(tr("Metrology")), "CY_EVENT_METRO_LAST", QColor(Qt::yellow), tr("Sensor calibration by the metrology tool.")},
    { "CYEV_SEND_PROJECT", ((CYFlag *)core->findData("CY_EVENT_SEND_PROJECT"))->ptrVal(), SI_1, REC|RAZ, tr("Send project")+": DATA_REC1"     , QString("CYLIX"), "CY_EVENT_PROJECT_SENT"      , QColor(Qt::yellow), tr("Notifies the sending of the project to the numerical regulator.")+"\n"+tr("This sending is also done automatically when the communication is detected.")},
  };
  int nb = sizeof(attr)/sizeof(attr[0]);
  for (int i=0; i<nb; i++)
  {
    if (attr[i].name.startsWith("CYEV_METRO") && !core->isBenchType("_METRO"))
      continue;

    CYEvent *event = new CYEvent(this, &attr[i]);
    event->setLocal(true);
    mEvents->add(event);
  }
}

void CYEventsGeneratorSPV::init_CYAcquisition()
{
  QHashIterator<QString, CYAcquisition *> it(core->acquisitions);
  while (it.hasNext())
  {
    it.next();                   // must come first
    CYAcquisition *acquisition = it.value();
    if (acquisition->postMortem())
    {
      CYEvent::Attr5 attr[]=
      {
        { 0, acquisition->backupEvent->ptrVal()      , SI_1, REC|RAZ, tr("New archive: DATA_REC1") , acquisition->label() , QString("%1_BACKUP_FILENAME").arg(acquisition->objectName())  , QColor(Qt::lightGray ), tr("Time stamp of new archive generated by the acquisition tool.")},
      };
      int nb = sizeof(attr)/sizeof(attr[0]);
      for (int i=0; i<nb; i++)
      {
        CYEvent *event = new CYEvent(this, QString("CYEV_%1_%2").arg(acquisition->objectName()).arg(i), &attr[i]);
        event->setLocal(true);
        mEvents->add(event);
      }

      CYEvent::Attr3 attrFA[]=
      {
        { "BACKUP2"   , acquisition->backup2Error->ptrVal(), SI_1, SCR|REC|BST, tr("Cannot create file : DATA_REC1"), acquisition->label() , acquisition->backup2FileName->objectName(), tr("Check access rights to the acquisition directory.")},
      };
      nb = sizeof(attr)/sizeof(attr[0]);
      for (int i=0; i<nb; i++)
      {
        CYEvent *event = new CYEvent(this, QString("CYFA_%1_%2").arg(acquisition->objectName()).arg(attrFA[i].name), &attrFA[i]);
        event->setEnableColor( core->events->alerts()->color());
        event->setSuffix(" !");
        event->setLocal(true);
        event->setReportUp(tr("Alert"));
        core->events->alerts()->add(event);
      }
    }
  }
}
