/***************************************************************************
                          cyeventswindow.cpp  -  description
                             -------------------
    begin                : ven oct 31 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyeventswindow.h"

// CYLIBS
#include "cyeventsview.h"
#include "cyeventsmanager.h"
#include "cyeventsbackup.h"
#include "cyeventspreferences.h"
#include "cycore.h"
#include "cyflag.h"
#include "cyactionmenu.h"
#include "cyproject.h"

CYEventsWindow::CYEventsWindow(const QString &name, int index, const QString &label)
  : CYWin(0, name, index, label)
{
  setWindowTitle(tr("Events reports"));
  mView = new CYEventsView(this, "CYEventsView");
  setCentralWidget(mView);

  connect(mView, SIGNAL(announceRecentURL(const QUrl&)), this, SLOT(registerRecentURL(const QUrl&)));

  new CYActionMenu( tr("&File"), 0, actionCollection(), "file");
  new CYAction(tr("&Quit"       ), "cywindow-close"        ,  QKeySequence::Quit , this      , SLOT(close())          , actionCollection(), "quit");
  new CYAction(tr("&Open"       ), "cydocument-open"       ,  QKeySequence::Open , mView     , SLOT(load())           , actionCollection(), "file_open");
  setActionDesigner("file_open");
  new CYAction(tr("Open &Recent"), "cydocument-open-recent",  0          , mView     , SLOT(load(const QUrl&)), actionCollection(), "file_open_recent");
  setActionDesigner("file_open_recent");
  new CYAction(tr("&Current")    , 0            , 0           , mView    , SLOT(current()), actionCollection(), "current");
  new CYAction(tr("&Backup")     , 0            , 0           , this     , SLOT(backup()) , actionCollection(), "backup");
  setActionDesigner("backup");

  new CYActionMenu( tr("&Settings"), 0, actionCollection(), "settings");
  new CYAction(tr( "Settings events..." ), "cyconfigure" ,  0   , this     , SLOT(optionsPreferences()), actionCollection(), "events_settings");

  //----------------------------------------------
  // Affichage colonnes
  //----------------------------------------------
  new CYActionMenu( tr("&View"), 0, actionCollection(), "view");

  mShowColums = new CYActionMenu(tr("&Columns"), 0, actionCollection(), "show_column_menu");

  mShowNameFlag = (CYFlag *)core->findData("CY_EVENT_VIEWER_NAME");
  mShowNameAction = new CYAction(mShowNameFlag->label(), 0, 0, actionCollection(), "show_name", true);
  mShowNameAction->setChecked(mShowNameFlag->val());
  connect(mShowNameAction, SIGNAL(toggled(bool)), this, SLOT(showColumnName()));

  mShowTimeFlag = (CYFlag *)core->findData("CY_EVENT_VIEWER_TIME");
  mShowTimeAction = new CYAction(mShowTimeFlag->label(), 0, 0, actionCollection(), "show_time", true);
  mShowTimeAction->setChecked(mShowTimeFlag->val());
  connect(mShowTimeAction, SIGNAL(toggled(bool)), this, SLOT(showColumnTime()));

  mShowSinceFlag = (CYFlag *)core->findData("CY_EVENT_VIEWER_SINCE");
  mShowSinceAction = new CYAction(mShowSinceFlag->label(), 0, 0, actionCollection(), "show_since", true);
  mShowSinceAction->setChecked(mShowSinceFlag->val());
  connect(mShowSinceAction, SIGNAL(toggled(bool)), this, SLOT(showColumnSince()));

  mShowTypeFlag = (CYFlag *)core->findData("CY_EVENT_VIEWER_TYPE");
  mShowTypeAction = new CYAction(mShowTypeFlag->label(), 0, 0, actionCollection(), "show_type", true);
  mShowTypeAction->setChecked(mShowTypeFlag->val());
  connect(mShowTypeAction, SIGNAL(toggled(bool)), this, SLOT(showColumnType()));
  mShowColums->insert(mShowTypeAction);

  mShowFromFlag = (CYFlag *)core->findData("CY_EVENT_VIEWER_FROM");
  mShowFromAction = new CYAction(mShowFromFlag->label(), 0, 0, actionCollection(), "show_from", true);
  mShowFromAction->setChecked(mShowFromFlag->val());
  connect(mShowFromAction, SIGNAL(toggled(bool)), this, SLOT(showColumnFrom()));
  mShowColums->insert(mShowFromAction);

//   mShowDescFlag = (CYFlag *)core->findData("CY_EVENT_VIEWER_DESC");
//   mShowDescAction = new CYAction(mShowDescFlag->label(), 0, 0, actionCollection(), "show_desc", true);
//   mShowDescAction->setChecked(mShowDescFlag->val());
//   connect(mShowDescAction, SIGNAL(toggled(bool)), this, SLOT(showColumnDesc()));
//   mShowColums->insert(mShowDescAction);

  connect(mView, SIGNAL(eventsRefreshed()), SLOT(showColumns()));

  //----------------------------------------------
  // Affichage types
  //----------------------------------------------
  mShowTypeMenu = new CYActionMenu(tr("&Types"), 0, actionCollection(), "show_type_menu");
  new CYAction(tr("All"), 0, 0, this, SLOT(showAllTypes()), actionCollection(), "show_type_all");
  new CYAction(tr("&None"), 0, 0, this, SLOT(showNoneType()), actionCollection(), "show_type_none");
  mShowTypeMenu->popupMenu()->addSeparator();

  QStringList typeList;
  QHashIterator<QString, bool *> it(core->events->showTypes);
  while (it.hasNext())
  {
    it.next();
    typeList<<it.key();
  }

  typeList.sort();
  for ( QStringList::Iterator it = typeList.begin(); it != typeList.end(); ++it )
  {
    CYAction *showTypeAction = new CYAction(*it, 0, 0, actionCollection(), 0, true);
    connect(showTypeAction, SIGNAL(toggled(bool)), this, SLOT(updateShowTypes()));
    showTypeAction->setChecked(true);
    mShowTypeMenu->insert(showTypeAction);
    mShowTypesActions.insert(*it, showTypeAction);
  }

  mShowTypeMenu->popupMenu()->addSeparator();

  mShowEndAction = new CYAction(tr("Their end"), 0, 0, actionCollection(), "show_type_end", true);
  mShowEndAction->setChecked(true);
//   mShowEndAction->setToolTip(tr("Enables display of the end of events enabled above."));
  connect(mShowEndAction, SIGNAL(toggled(bool)), mView, SLOT(refreshEvents()));

  mShowAckAction = new CYAction(tr("Their acknowledgement"), 0, 0, actionCollection(), "show_type_ack", true);
  mShowAckAction->setChecked(true);
//   mShowAckAction->setToolTip(tr("Enables display of the acknowledgement of events enabled above."));
  connect(mShowAckAction, SIGNAL(toggled(bool)), mView, SLOT(refreshEvents()));

  //----------------------------------------------
  // Affichage provenances
  //----------------------------------------------
  mShowFromMenu = new CYActionMenu(tr("&From"), 0, actionCollection(), "show_from_menu");

  new CYAction(tr("All"), 0, 0, this, SLOT(showAllFroms()), actionCollection(), "show_from_all");
  new CYAction(tr("&None"), 0, 0, this, SLOT(showNoneFrom()), actionCollection(), "show_from_none");
  mShowTypeMenu->popupMenu()->addSeparator();

  QStringList fromList;
  QHashIterator<QString, bool *> it2(core->events->showFroms);
  while (it2.hasNext())
  {
    it2.next();
    fromList<<it2.key();
  }

  fromList.sort();
  for ( QStringList::Iterator it = fromList.begin(); it != fromList.end(); ++it )
  {
    CYAction *showFromAction = new CYAction(*it, 0, 0, actionCollection(), 0, true);
    connect(showFromAction, SIGNAL(toggled(bool)), this, SLOT(updateShowFroms()));
    showFromAction->setChecked(true);
    mShowFromMenu->insert(showFromAction);
    mShowFromsActions.insert(*it, showFromAction);
  }

  mProject = core->project(0);

  readProperties(core->OSUserConfig());
}

CYEventsWindow::~CYEventsWindow()
{
}

void CYEventsWindow::createGUI(const QString &xmlfile)
{
  CYWin::createGUI(xmlfile);

//QT5  mFileMenu = new QMenu( this );
//QT5  mFileMenu->setTitle(tr("&File"));
//QT5  menuBar()->addMenu( mFileMenu );
//QT5  actionCollection()->insertTo("quit"            , mFileMenu);
//QT5  actionCollection()->insertTo("file_open"       , mFileMenu);
//QT5  actionCollection()->addActionTo("file_open_recent", mFileMenu);
//QT5
//QT5  mViewMenu = new QMenu( this );
//QT5  mViewMenu->setTitle(tr("&View"));
//QT5  menuBar()->addMenu( mViewMenu );
//QT5  mViewMenu->addMenu( mShowColums );
//QT5  mViewMenu->addMenu( mShowTypeMenu );
//QT5  mViewMenu->addMenu( mShowFromMenu );
//QT5
//QT5  mSettingMenu = new QMenu( this );
//QT5  mSettingMenu->setTitle(tr("&Settings"));
//QT5  menuBar()->addMenu( mSettingMenu );
//QT5  actionCollection()->addActionTo("events_settings"  , mSettingMenu);
//QT5
//QT5  QToolBar * mainTools = new QToolBar( this );
//QT5  mainTools->setWindowTitle( tr("Main Toolbar") );
//QT5  actionCollection()->addActionTo("file_open"      , mainTools);
//QT5  actionCollection()->addActionTo("events_settings", mainTools);
}

void CYEventsWindow::show(int projectId)
{
  mProject = core->project(projectId);
  mView->setProject(mProject);

  QString title = tr("Events reports");
  if (!mProject->label().isEmpty())
    title.prepend(QString("%1: ").arg(mProject->label()));
  setWindowTitle(title);

  show();
}

void CYEventsWindow::show()
{
  CYWin::show();
  showColumns();
}

void CYEventsWindow::backup()
{
  CYEventsBackup *dlg = new CYEventsBackup(this, "CYEventsBackup");
  dlg->exec();
  mView->refreshHistory();
}


/*!
    \fn CYEventsWindow::optionsPreferences()
 */
void CYEventsWindow::optionsPreferences()
{
  // popup some sort of preference dialog, here
  CYEventsPreferences * dlg = new CYEventsPreferences( this );
  dlg->show();
}


int CYEventsWindow::column(QString text)
{
  for (int col=0; col<mView->eventsView()->columnCount(); col++)
  {
    if (mView->eventsView()->headerItem()->text(col)==text)
      return col;
  }
  return -1;
}

void CYEventsWindow::showColumns()
{
  showColumnName();
  showColumnTime();
  showColumnSince();
  showColumnType();
  showColumnFrom();
//   showColumnDesc();
}


void CYEventsWindow::showColumns(CYFlag *flag, CYAction *action)
{
  flag->setVal(action->isChecked());
  if (flag->val()!=flag->oldVal())
    flag->save();

  int col = column(flag->label());
  if (col>-1)
  {
    // TODO QT5
//    mView->eventsView()->adjustColumn(col);
    if (flag->val())
      mView->eventsView()->showColumn(col);
    else
      mView->eventsView()->hideColumn(col);
  }
}

void CYEventsWindow::showColumnName()
{
  showColumns(mShowNameFlag, mShowNameAction);
}

void CYEventsWindow::showColumnTime()
{
  showColumns(mShowTimeFlag, mShowTimeAction);
}

void CYEventsWindow::showColumnSince()
{
  showColumns(mShowSinceFlag, mShowSinceAction);
}

void CYEventsWindow::showColumnType()
{
  showColumns(mShowTypeFlag, mShowTypeAction);
}

void CYEventsWindow::showColumnFrom()
{
  showColumns(mShowFromFlag, mShowFromAction);
}

void CYEventsWindow::showColumnDesc()
{
  showColumns(mShowDescFlag, mShowDescAction);
}

void CYEventsWindow::showAllTypes()
{
  QHashIterator<QString, CYAction *> it(mShowTypesActions);
  while (it.hasNext())
  {
    it.next();
    CYAction *action = it.value();
    action->setChecked(true);
    bool *checked = core->events->showTypes.value(action->text());
    *checked = true;
  }
  mView->refreshEvents();
}

void CYEventsWindow::showNoneType()
{
  QHashIterator<QString, CYAction *> it(mShowTypesActions);
  while (it.hasNext())
  {
    it.next();
    CYAction *action = it.value();
    action->setChecked(false);
    bool *checked = core->events->showTypes.value(action->text());
    *checked = false;
  }
  mView->refreshEvents();
}

void CYEventsWindow::updateShowTypes()
{
  QHashIterator<QString, CYAction *> it(mShowTypesActions);
  while (it.hasNext())
  {
    it.next();
    CYAction *action = it.value();
    bool *checked = core->events->showTypes.value(action->text());
    if (action->isChecked())
      *checked = true;
    else
      *checked = false;
  }
  mView->refreshEvents();
}

void CYEventsWindow::showAllFroms()
{
  QHashIterator<QString, CYAction *> it(mShowFromsActions);
  while (it.hasNext())
  {
    it.next();
    CYAction *action = it.value();
    action->setChecked(true);
    bool *checked = core->events->showFroms.value(action->text());
    *checked = true;
  }
  mView->refreshEvents();
}

void CYEventsWindow::showNoneFrom()
{
  QHashIterator<QString, CYAction *> it(mShowFromsActions);
  while (it.hasNext())
  {
    it.next();
    CYAction *action = it.value();
    action->setChecked(false);
    bool *checked = core->events->showFroms.value(action->text());
    *checked = false;
  }
  mView->refreshEvents();
}

void CYEventsWindow::updateShowFroms()
{
  QHashIterator<QString, CYAction *> it(mShowFromsActions);
  while (it.hasNext())
  {
    it.next();
    CYAction *action = it.value();
    bool *checked = core->events->showFroms.value(action->text());
    if (action->isChecked())
      *checked = true;
    else
      *checked = false;
  }
  mView->refreshEvents();
}

bool CYEventsWindow::showEndAction()
{
  return mShowEndAction->isChecked();
}

bool CYEventsWindow::showAckAction()
{
  return mShowAckAction->isChecked();
}
