#include "cynetconnect.h"
#include "ui_cynetconnect.h"
#include "cynethost.h"
#include "cynetlink.h"
#include "cycore.h"

CYNetConnect::CYNetConnect(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYNetConnect)
{
  ui->setupUi(this);
  connect(ui->bpHelp   , SIGNAL(clicked(bool)), core, SLOT(helpConnectHost()));
  connect(ui->bpConnect, SIGNAL(clicked(bool)), core, SLOT(connectHost()));
}

CYNetConnect::~CYNetConnect()
{
  delete ui;
}

void CYNetConnect::update( const QString &hostName )
{
  CYNetHost* host;

  ui->netListView->clear();
  if (!core)
    return;
  if ((host = core->hostList.value(hostName)) == 0)
    return;


  QHashIterator<QString, CYNetLink *> it(host->linkList);
  while (it.hasNext())
  {
    it.next();                   // must come first
    QTreeWidgetItem* lvi = new QTreeWidgetItem ( ui->netListView, 0 );
    lvi->setText(0, QString("%1").arg(it.value()->getCOM()));
    lvi->setCheckState(0, (it.value()->isEnable()) ? Qt::Checked : Qt::Unchecked);
    lvi->setText(1, it.value()->state());
    lvi->setText(2, it.value()->objectName());
  }
}

void CYNetConnect::newHost( const QString &hostName )
{
  ui->comboHost->addItem(hostName);
}

void CYNetConnect::enableCOM( QTreeWidgetItem *lvi , int )
{
  CYNetHost* host;

  if (!core)
    return;
  if ((host = core->hostList.value(ui->comboHost->currentText())) == 0)
    return;

  QHashIterator<QString, CYNetLink *> it(host->linkList);
  while (it.hasNext())
  {
    it.next();                   // must come first
    if (it.value()->getCOM() == (lvi->text(0)).toShort())
    {
      // TODO QT5
      //      if (lvi->rtti() == 1)
//        it.value()->setEnable(static_cast<QCheckListItem *>(lvi)->isOn());
    }
  }
}

void CYNetConnect::engageHost(const QString &hostName)
{
  CYNetHost *host;
  CYNetLink *net;

  if (hostName == "")
  {
    show();
    ui->comboHost->setFocus();
  }
  else
  {
    hide();
    if ((host = core->hostList.value(hostName)) == 0)
      CYFATALTEXT(hostName)
    ui->comboHost->setCurrentIndex(host->index);
  }

  update(ui->comboHost->currentText());

  QTreeWidgetItemIterator it(ui->netListView);
  while (*it)
  {
    if ((net = core->linkList.value((*it)->text(2))) == 0)
      CYFATALTEXT((*it)->text(0));
    // TODO QT5
//    if ((*it)->rtti() == 1)
//      static_cast<QCheckListItem *>(*it)->setOn(net->isEnable());
    ++it;
  }
}

bool CYNetConnect::connectHost()
{
  CYNetLink *net;

  QTreeWidgetItemIterator it(ui->netListView);
  while (*it)
  {
    if ((net = core->linkList.value((*it)->text(2))) == 0)
      CYFATALTEXT((*it)->text(0));

    // TODO QT5
//    if ((*it)->rtti() == 1)
//      static_cast<QCheckListItem *>(*it)->setOn(net->isEnable());
//    if ((*it)->rtti() == 1)
//    {
//      if (static_cast<QCheckListItem *>((*it))->isOn())
        net->setEnable(true);
//      else
//        net->setEnable(false);
//    }
    ++it;
  }

  return core->engage(ui->comboHost->currentText());
}
