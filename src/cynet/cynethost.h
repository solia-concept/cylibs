/***************************************************************************
                          cynethost.h  -  description
                             -------------------
    début                  : Sat Feb 8 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYNETHOST_H
#define CYNETHOST_H

// QT
#include <QHash>
#include <QDir>
#include <QString>
// CYLIBS
#include "cytypes.h"

class CYNetLink;

/** CYNetHost est un agent de communication d'hôte.
  * Un hôte peut être un banc d'essai, un régulateur du banc
  * ou un système externe.
  * @short Agent de communication d'hôte.
  * @author Gérald LE CLEACH
  */

class CYNetHost : public QObject
{
  Q_OBJECT

public:
  enum Type
  {
    RN_DOS = 0,
    RN_LINUX,
    SPV
  };

  /** Constructeur d'un agent de communication d'hôte.
    * @param parent Parent.
    * @param name   Nom de l'hôte.
    * @param id     Index de l'hôte.
    * @param desc   Description de l'hôte.
      @param r422f  Nom de la donnée autorisant la mise à jour du régulateur.
                    Un charactère '!' devant inverse le sens. */
  CYNetHost(QObject *parent, const QString &name=0, const short id=0, QString desc=0, QString r422f=0);
  /** Destructeur. */
  ~CYNetHost();

  /** Ajoute une connexion. */
  void addLink(CYNetLink *l);
  /** Active une connexion. */
  void setEnableConnect(const CYNetLink *c, const bool actived=true);
  /** Démarre la communication avec l'hôte. */
  bool start();
  /** Arrête la communication avec l'hôte. */
  bool stop();
  /** @return la description. */
  QString description() { return mDesc; }

  /** @return la connexion réseau pour le transfert de fichiers. */
  CYNetLink *R422FLink() { return mR422FLink; }
  /** @return le nom de la donnée autorisant le transfert de fichier. */
  QString enableR422F() { return mEnableR422F; }

  /** @return l'étiquette */
  QString label() { return mLabel; }

public: // Public attributes
  /** Dictionnaire des différentes connexions réseau associées. */
  QHash<QString, CYNetLink*> linkList;
  /** Index de l'hôte. */
  int index;
  /** Type. */
  Type type;

signals:
    void r422f_notify(const QString &txt);

protected: // Protected attributes
  /** Description. */
  QString mDesc;
  /** Connexion réseau pour le transfert de fichiers. */
  CYNetLink *mR422FLink;
  /** Nom de la donnée autorisant le transfert de fichier. */
  QString mEnableR422F;

private:
  /** Etat de la conexion.*/
  bool onLine;

  QString mLabel;
};

#endif
