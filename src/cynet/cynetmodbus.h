//
// C++ Interface: cynetmodbus
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYNETMODBUS_H
#define CYNETMODBUS_H

#include <cynetmodule.h>
#include <scmdb_t.h>

/** Intertface modbus de communication réseau
  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYNetModbus : public CYNetModule
{
  Q_OBJECT
public:
  CYNetModbus(QObject *parent = 0, const QString &name = 0);

  ~CYNetModbus();

  /*! \return le type de communication du serveur distant \a srv.*/
  virtual TypeCom typeCom(int srv) = 0;

protected:
  /** Initialise la communication inter-processus */
  virtual int init_com_itprc() { return 0; }
};
#endif

