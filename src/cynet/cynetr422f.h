//
// C++ Interface: cynetr422f
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYNETR422F_H
#define CYNETR422F_H

// QT
#include <QCloseEvent>
#include <QList>
#include <QElapsedTimer>
// CYLIBS
#include "cydialog.h"
#include "cynetr422file.h"
#include "cynethost.h"
#include "cynetlink.h"
#include "cytypes.h"

namespace Ui {
		class CYNetR422F;
}

/**
@short Boîte de transfert de fichiers via la connexion R422.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYNetR422F : public CYDialog
{
Q_OBJECT
public:
  CYNetR422F(CYNetHost *host, QString fileName, QWidget *parent = 0, const QString &name = 0);

  ~CYNetR422F();

  /** Affiche un message d'information dans la boîte de messagerie de transfert */
  void message(QString txt, QColor color="", QString style=0);
  /** Affiche un message d'erreur dans la boîte d'info de messagerie de transfert */
  void error(QString txt);

public slots:
  /** Ouverture du fichier \a fileName pour le transfert de fichiers.
      Celui-ci peut être le fichier d'initialisation "ini" ou un fichier "tar.gz"
      contenant ce fichier "ini" et le(s) fichiers à transférer. */
  virtual void open(QString fileName);
  /** Transfert en cours .*/
  virtual void run();

protected:
  /** Initialise le transfert à partir du fichier "ini". */
  virtual void init(QString fileName);

  /** Ajoute un/des fichier(s) à installer sur l'hôte distant.
      Le répertoire source est recherché de manière automatique à partir
      du répertoire contenant le fichier initialisation du transfert
      et le répertoire de destination.
      Si \a File se termine par *.*, alors tous les fichiers du répertoire source sont transférés.
      @param section Opération à réaliser.
      @param File Nom du fichier avec le chemin de destination.
      @param Linux   Répertoire Linux.
      @param Dos     Répertoire Dos. */
  virtual bool fileLine(QString section, QString File, QString Linux, QString Dos);
  /** Ajoute un fichier sur l'hôte distant.
      @param fileName Nom du fichier.
      @param src      Répertoire source (Linux).
      @param dest     Répertoire destination (Dos). */
  virtual bool fileToInstall(QString fileName, QString src, QString dest);

  /** Sauvegarde un fichier de l'hôte distant.
      @param fileName Nom du fichier.
      @param src      Répertoire source (Dos).
      @param dest     Répertoire destination (Linux). */
  virtual bool fileToSave(QString File, QString src, QString dest);


  /** Envoie d'une commande de transfert de fichiers @see s_r422f_cmd.
    * @param cmd        Index de la commande.
    * @param path_name  Chemin RN.
    * @param file_name  Nom du fichier.
    * @param file_size  Taille du fichier. */
  virtual bool cmd(s8 cmd, QString path_name=0, QString file_name=0, int file_size=0);
  /** Écrit un bloc de transfert de fichiers.
    * @param num_block  Numéro du block transféré
    * @param size_block Taille du block transféré en octets.
    * @param data       Données du block. */
  virtual bool write(s16 num_block, s16 size_block, s8 *data);

  /** Lit un bloc de transfert de fichiers. */
  virtual bool read();

  /** Enrichissement pour empêcher la fermeture lors d'un transfert. */
  virtual void closeEvent(QCloseEvent * e);

protected:
  CYNetHost *mHost;
  CYNetLink *mLink;
  /** Répertoire local du transfert de fichiers. */
  QDir mLocalDir;
  int mode, mem_mode;
  QElapsedTimer t, t2;
  int para_ok;
  bool reboot;
  bool disable_r422f;
  s32 cpt_size;
  s32 size_block;

  /** Liste des fichiers à transférer */
  QList<CYNetR422File*> files;
  /** Fichier en cours de transfert */
  CYNetR422File *currentFile;

  s8 data[R422F_SIZE_BLOCK];

  s_r422f_info *st_info;
  s_r422f_read *st_read;

  int cpt_sec;
  int cpt_file;
  int max_sec;
  QTimer *mTimer;
  QString version_r422f;

private:
  Ui::CYNetR422F *ui;
};

#endif
