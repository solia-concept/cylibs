#ifndef CYNETCONNECT_H
#define CYNETCONNECT_H

// QT
#include <QTreeWidget>
// CYLIBS
#include "cydialog.h"

namespace Ui {
    class CYNetConnect;
}

/** @short Boîte de connexion.
  * @author Gérald LE CLEACH
  */

class CYNetConnect : public CYDialog
{
  Q_OBJECT
public:
  CYNetConnect(QWidget *parent=0, const QString &name=0);
  ~CYNetConnect();

  bool connectHost();

public slots: // Public slots
  void update( const QString &hostName );
  void newHost( const QString &hostName );
  void enableCOM( QTreeWidgetItem * lvi , int );
  void engageHost(const QString &hostName);

private:
    Ui::CYNetConnect *ui;
};

#endif // CYNETCONNECT_H
