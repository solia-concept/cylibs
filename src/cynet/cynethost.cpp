/***************************************************************************
                          cynethost.cpp  -  description
                             -------------------
    début                  : Sat Feb 8 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cynethost.h"

// QT
#include <qfile.h>
#include <qdir.h>
#include <QTextStream>
//Added by qt3to4:
#include <QString>
// CYLIBS
#include "cycore.h"
#include "cynetlink.h"
#include "cynetmodule.h"
#include "cycommand.h"
#include "cymessagebox.h"

CYNetHost::CYNetHost(QObject *parent, const QString &name, const short id, QString desc, QString r422f)
  : QObject(parent),
    index(id),
    mDesc(desc)
{
  type = (!id) ? SPV : RN_LINUX;
  setObjectName(name.toUtf8());
  mLabel=name.toUtf8();
  onLine = false;
  mR422FLink = 0;
  if (!r422f.isEmpty())
  {
    mEnableR422F = r422f;
  }
}

CYNetHost::~CYNetHost()
{
  if (onLine)
    onLine = false;
}

void CYNetHost::addLink(CYNetLink *l)
{
  linkList.insert(l->objectName(), l);

  QHashIterator<QString, CYNetLink *> it(linkList);
  while (it.hasNext())
  {
    it.next();                   // must come first
    if (!mR422FLink || (mR422FLink->getCOM()>l->getCOM()))
    {
      mR422FLink = l;
    }
  }
}

void CYNetHost::setEnableConnect(const CYNetLink *c, const bool actived)
{
  QHashIterator<QString, CYNetLink *> it(linkList);
  while (it.hasNext())
  {
    it.next();                   // must come first
    if (it.value() == c)
    {
      it.value()->setEnable(actived);
      return;
    }
  }
}

bool CYNetHost::start()
{
  QHashIterator<QString, CYNetLink *> it(linkList);
  while (it.hasNext())
  {
    it.next();                   // must come first
    it.value()->start();
  }

  onLine = true;

  return (true);
}

bool CYNetHost::stop()
{
  QHashIterator<QString, CYNetLink *> it(linkList);
  while (it.hasNext())
  {
    it.next();                   // must come first
    it.value()->stop();
  }

  onLine = false;

  return (true);
}
