/***************************************************************************
                          cynetmodule.cpp  -  description
                             -------------------
    début                  : Sat Feb 8 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cynetmodule.h"

#include "cytypes.h"

// ANSI
#include <stdio.h>
// POSIX
#include <fcntl.h>
#include <unistd.h>
// QT
#include <qapplication.h>
// CYLIBS
#include <cy.h>
#include <cytypes.h>
#include <cycore.h>
#include "cyapplication.h"
#include "cyaboutdata.h"

CYNetModule::CYNetModule(QObject *parent, const QString &name)
  : QObject(parent)
{
  setObjectName(name);
  cm.fl_open = false;
  cm.fl_init = false;
  cm.fl_start= false;
  mR422F_state=false;
  mModbus     =false;
  mIoctlTime  = 50;
  installed=false;
  running=false;
  mReadingStr = -1;

  if (!mModbus && QFile::exists(QString("/usr/src/r422m_%1").arg(cyapp->aboutData()->appName())))
    installed=true;
}

CYNetModule::~CYNetModule()
{
  end();
}

void CYNetModule::init()
{
  init_stcym();
  open_cym();
  init_ioctl();
}

void CYNetModule::init_st_r422f(int i, s16 attr_com, void *ptrstruct, size_t size, s16 seq, s8 index_auto, u32 flags, s16 nb_read, s16 nb_write)
{
  str_r422f[i] = ptrstruct;
  init_st(i, attr_com, ptrstruct, size, seq, index_auto, flags, nb_read, nb_write);
}

int CYNetModule::readData(int str, u32 index, int com)
{
  Q_UNUSED(str)
  Q_UNUSED(index)
  Q_UNUSED(com)
  return 0;
}

bool CYNetModule::writeData(int str, u32 index, int com)
{
  Q_UNUSED(str)
  Q_UNUSED(index)
  Q_UNUSED(com)
  return false;
}

bool CYNetModule::cmdR422F(int com, s8 cmd, QString path_name, QString file_name, s32 file_size)
{
  int str = ST_R422F_CMD;
  s_r422f_cmd *pstr = (s_r422f_cmd *)ptrStruct(str);
  if (!pstr)
  {
    CYWARNING;
    return false;
  }
  pstr->cmd=cmd;
  if (!path_name.isEmpty())
    sprintf(pstr->path_name,"%s", path_name.toUtf8().data());
  if (!file_name.isEmpty())
    sprintf(pstr->file_name,"%s", file_name.toUtf8().data());
  pstr->file_size=file_size;

  return writeData(str, 0, com);
}


bool CYNetModule::writeR422F(int com, s16 num_block, s16 size_block, s8 *data)
{
  int str = ST_R422F_WRITE;
  s_r422f_write *pstr = (s_r422f_write *)ptrStruct(str);
  if (!pstr)
  {
    CYWARNING;
    return false;
  }

  pstr->num_block=num_block;
  pstr->size_block=size_block;
  memcpy(pstr->data, data, size_block);
  return writeData(str, 0, com);
}

bool CYNetModule::readR422F(int com)
{
  if (!readData(ST_R422F_READ, 0, com))
    return false;

  return true;
}


/*! @return \a true si le module a été démarré.
    \fn CYNetModule::started()
 */
bool CYNetModule::started()
{
  return cm.fl_start;
}


/*! @return \a true si modbus sinon R422M
    \fn CYNetModule::isModbus()
 */
bool CYNetModule::isModbus()
{
  return mModbus;
}
