//
// C++ Implementation: cynetmodbus
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cynetmodbus.h"


CYNetModbus::CYNetModbus(QObject *parent, const QString &name)
 : CYNetModule(parent, name)
{
  mModbus=true;
}


CYNetModbus::~CYNetModbus()
{
}

