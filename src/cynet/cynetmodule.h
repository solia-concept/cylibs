/***************************************************************************
                          cynetmodule.h  -  description
                             -------------------
    début                  : Sat Feb 8 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYNETMODULE_H
#define CYNETMODULE_H

// QT
#include <QObject>
#include <QHash>
// CYLIBS
#include <cytypes.h>

/** CYNetModule est la classe de communication réseau de CYLIX. Elle dialogue
  * avec le module de communication "cym". Celui-ci permet au superviseur
  * d'envoyer et de recevoir des données sur les différentes COM du PC.
  * @short Interface entre CYLIX et son module de communication réseau.
  * @author Gérald LE CLEACH
  */

class CYNetModule : public QObject
{
  Q_OBJECT

public:
  CYNetModule(QObject *parent=0, const QString &name=0);
  virtual ~CYNetModule();

  virtual void init();
  /** @return la valeur du compteur d'erreur \a err de la connexion  \a com. */
  virtual u16 cntCom(int com, int err) { Q_UNUSED(com); Q_UNUSED(err); return 0; }
  /** @return la valeur du compteur d'erreur \a err du serveur  \a srv. */
  virtual u16 cntSrv(int srv, int err) {  Q_UNUSED(srv); Q_UNUSED(err); return 0; }
  /** @return l'état des connexions du device ayant pour masque de communication \a mask. */
  virtual bool testMaskCom(s16 mask) { Q_UNUSED(mask); return false; }
  /** @return l'état de la connexion \a com. */
  virtual bool testCom(int com) { Q_UNUSED(com); return false; }

  virtual bool testCom_forc(int com) { Q_UNUSED(com); return false; }
  /** Vide fifo de la structure \a str */
  virtual void clearFifo(int str) {Q_UNUSED(str); }

  /** Lecture des valeurs actuelles et passées d'une structure.
    * La valeur retournée est le nombre de structures lues.
    * @param str    Index de la structure dans le tableau des devices.
    * @param index  Indice du premier échantillon à lire.
    * @param com    Numéro de com. */
  virtual int readData(int str, u32 index=0, int com=0);
  /** Ecriture d'une structure de données.
    * @param str    Index de la structure dans le tableau des devices.
     * @param index  Indice du premier échantillon à écrire.
     * @param com    Numéro de com. (-1 : toutes com possible / init str, sinon com: */
  virtual bool writeData(int str, u32 index=0, int com=-1);

  /** @return l'index de la structure en lecture. */
  int readingStr() { return mReadingStr; }

  void setR422F(bool state) { mR422F_state = state; }
  bool R422F_running() { return mR422F_state; }

  /** Envoie d'une commande de transfert de fichiers @see s_r422f_cmd.
    * @param com        Numéro de com.
    * @param cmd        Index de la commande.
    * @param path_name  Chemin RN.
    * @param file_name  Nom du fichier.
    * @param file_size  Taille du fichier. */
  virtual bool cmdR422F(int com, s8 cmd, QString path_name=0, QString file_name=0, s32 file_size=0);
  /** Écrit un bloc de transfert de fichiers.
    * @param com        Numéro de com.
    * @param num_block  Numéro du block transféré
    * @param size_block Taille du block transféré en octets.
    * @param data       Données du block. */
  virtual bool writeR422F(int com, s16 num_block, s16 size_block, s8 *data);
  /** Lit un bloc de transfert de fichiers.
    * @param com        Numéro de com. */
  virtual bool readR422F(int com);

  /** Contrôle l'état de fonctionement du module. */
  virtual bool ctrl() { return false; }

  /** @return le pointeur sur la structure ayant le numéro \a no. */
  virtual void *ptrStruct(int no) { Q_UNUSED(no); return 0; }
  /** @return la taille de la structure ayant le numéro \a no. */
  virtual int sizeStruct(int no) { Q_UNUSED(no); return 0; }

  /** @return la taille de la FIFO d'acquisition de la structure ayant le numéro \a no. */
  virtual short sizeReadFIFO(int no) { Q_UNUSED(no); return 0; }
  /** @return la taille de la FIFO d'émission de la structure ayant le numéro \a no. */
  virtual short sizeWriteFIFO(int no) { Q_UNUSED(no); return 0; }
  virtual bool started();

  /** @return le temps minimum en ms entre 2 appels ioctl en lecture / écriture (@see mIoctlTime). */
  int ioctlTime() { return mIoctlTime; }

  bool isModbus();

  /** @return le pointeur du buffer de la structure \a str. */
  void *buffer(int str) { return mBuffer[str]; }

  /** @return \a true si la structure \str est en mode lecture.*/
  virtual bool isReadMode(int str) { Q_UNUSED(str); return false; }
  /** @return \a true si la structure \str est en mode écriture.*/
  virtual bool isWriteMode(int str) { Q_UNUSED(str); return false; }

  /** Vaut \a true si le module de communication est installé.*/
  flg installed;
  /** Vaut \a true si le module de communication fonctionne bien.*/
  flg running;


public slots: // Public slots
  /** Remet à zéro les compteurs d'erreurs realtifs au connexions réseau. */
  virtual void razCounters() {}
  virtual void end() {}

  /** Démarre le module de communication réseau. */
  virtual void start() {}
  /** Arrête le module de communication réseau. */
  virtual void stop() {}
  /** @return le pointeur sur l'absence de com . */
  virtual s8 *absence(int com) { Q_UNUSED(com); return 0; }
  /** @return le pointeur sur la valididté de réceptions de com. */
  virtual s8 *reception(int com) { Q_UNUSED(com); return 0; }
  virtual void acknowledge() {};

  /** RAZ des paramètres de toutes les structures réseau possibles.
    * Ce RAZ est nécessaire avant de réinitialisation à la volée des structures réseau. */
  virtual void raz_NB_MAX_ST() {};
  /** Initialise les paramètres des structures réseau nécessaires au tranfert de fichiers par R422. */
  virtual void init_NB_ST_R422F(void) {};
  /** Initialise les paramètres des structures réseau de communication par R422. */
  virtual void init_NB_ST(void) {};

signals: // Signals
  /** Emis à chaque test de COM. */
  void stateCom(bool state, int com);
  /** Emis au démarrage du module. */
  void starting();

protected: // Protected methods
  virtual void init_st(int i, s16 attr_com, void *ptrstruct, size_t size, s16 seq, s8 index_auto, u32 flags, s16 nb_read, s16 nb_write)
  {
    Q_UNUSED(i); Q_UNUSED(attr_com); Q_UNUSED(ptrstruct);
    Q_UNUSED(size); Q_UNUSED(seq); Q_UNUSED(index_auto);
    Q_UNUSED(flags); Q_UNUSED(nb_read); Q_UNUSED(nb_write);
  }
  virtual void init_st_r422f(int i, s16 attr_com, void *ptrstruct, size_t size, s16 seq, s8 index_auto, u32 flags, s16 nb_read, s16 nb_write);
  virtual void init_stcym() {}
  virtual void open_cym() {}
  virtual void init_ioctl() {}

public: // Public attributes
  /** Nombre de structures. */
  s16 nb_st;
  /** Nombre de COM. */
  s16 nb_com;
  void *str_r422f[NB_ST_R422F];

protected: // Protected attributes
  /** Structure de contrôle du module. */
  struct s_cm
  {
    char fl_open;
    char fl_init;
    char fl_start;
  };
  struct s_cm cm;

  /** Temps minimum en ms entre 2 appels ioctl en lecture / écriture.
    * Cette valeur est initilisée à 50ms dans le constructeur CYNetModule.
    * Pour la changer il suffit de la modifier la classe fille utilisée NetModule */
  int  mIoctlTime;

  bool mR422F_state;
  bool mModbus;
  QHash<int, void*> mBuffer;
  /** Index de la structure en lecture.*/
  int mReadingStr;
};

#endif
