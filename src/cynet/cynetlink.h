/***************************************************************************
                          cynetlink.h  -  description
                             -------------------
    début                  : Sat Feb 8 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYNETLINK_H
#define CYNETLINK_H

// QT
#include <QObject>
#include <QHash>

class CYDB;
class CYNetModule;
class CYNetHost;

/** CYNetLink représente une connexion réseau. Cette classe s'occupe de la
  * communication assynchrone entre un hôte et une COM du superviseur au moyen
  * d'un CYNetHost.
  * @short Connexion physique du réseau.
  * @author Gérald LE CLEACH
  */

class CYNetLink : public QObject
{
  Q_OBJECT

public:
  /** Constructeur d'une connexion physique du réseau.
    * @param parent     Parent.
    * @param name       Nom de la connexion.
    * @param cym        Module de communication.
    * @param host       Hôte distant.
    * @param localHost  Hôte = supervieur */
  CYNetLink(QObject *parent, const QString &name=0, CYNetModule *cym=0, CYNetHost *host=0, bool localHost=false);
  /** Destructeur. */
  ~CYNetLink();

  /** Ajoute une base de données gérant une structure réseau. */
  void addDB(CYDB *db);

  /** Authorise/interdit l'utilisation de la connexion. */
  void setEnable(const bool actived=true) { enable = actived; }
  /** Retourne l'authorisation d'utilisation de la connexion. */
  bool isEnable() { return enable; }

  /** Démarre la communication sur cette connection. */
  virtual bool start();
  /** Arrête la communication sur cette connection. */
  virtual bool stop();
  /** Retourne l'état de la connection. */
  bool isOn() { return onLine; }
  /** Retourne l'état de la connection. */
  virtual QString state();

  /**
   * @brief Saisie le numéro de COM
   * @param com Numéro de COM
   * @param st Index de structure de détection communication @see detectStr(). */
  void setCOM(const short com, int st) { numCom = com; mDetectStr = st; }
  /** Retourne le numéro de COM. */
  short getCOM() const { return numCom; }
  /** @return Index de structure de détection communication @see setCOM().
   *  Index de la structure dans le tableau des devices qui permet de détecter la perte
   *  et reprise de communication. Celle-ci doit-être la plus lente.*/
  int detectStr() const { return mDetectStr; }
  /** @return l'hôte distant. */
  CYNetHost *host() { return mHost; }
  void setBTMS( float val );
  virtual QString frequency();

  /** @return \a true si l'hôte au quel est rattachée cette comme est le supervieur. */
  bool isLocalHost() { return mLocalHost; }

  /** Liste des bases de données gérées. */
  QHash<QString, CYDB*> dbList;

  /** @return l'étiquette */
  QString label() { return mLabel; }

public slots: // Public slots
  /** Réalise un traîtement en fonction de l'état de la COM. */
  void stateCom(bool state, int com);

signals: // Signals
  /** No descriptions */
  void update();

protected: // Protected attributes
  /** Hôte distant. */
  CYNetHost *mHost;
  /** Module de communication réseau. */
  CYNetModule *module;
  /** Authorisation d'utilisation de la connexion. */
  bool enable;
  /** Etat de la connexion. */
  bool onLine;
  /** Numéro de COM. */
  short numCom;
  /** Index de structure de détection communication. */
  int mDetectStr;
  /** true si Hôte = supervieur. */
  bool mLocalHost;
  float mBTMS;

  QString mLabel;
};

#endif
