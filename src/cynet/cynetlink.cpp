/***************************************************************************
                          cynetlink.cpp  -  description
                             -------------------
    début                  : Sat Feb 8 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cynetlink.h"

// CYLIBS
#include "cy.h"
#include "cycore.h"
#include "cynetmodule.h"
#include "cydb.h"
#include "cymessagebox.h"

CYNetLink::CYNetLink(QObject *parent, const QString &name, CYNetModule *cym, CYNetHost *host, bool localHost)
  : QObject(parent),
    mHost(host)
{
  setObjectName(name.toUtf8());
  mLabel=name.toUtf8();
  numCom = -1;
  mLocalHost = localHost;
  if (mLocalHost)
    onLine = true;
  else
    onLine = false;
  module = cym;
  if (module)
  {
    connect(module, SIGNAL(stateCom(bool, int)), SLOT(stateCom(bool, int)));
    connect(module, SIGNAL(starting()), core, SIGNAL(moduleStarting()));
  }
}

CYNetLink::~CYNetLink()
{
}

void CYNetLink::addDB(CYDB *db)
{
  db->setLink(this);
  dbList.insert(db->objectName(), db);
}

bool CYNetLink::start()
{
  if (!enable || !module || (numCom==-1))
    return (false);

  module->ctrl();
  if (!module->testCom(numCom))
  {
    CYMESSAGETEXT(QString("Impossible de démarrer COM n°%1").arg(numCom+1).toUtf8());
    return false;
  }
  return true;
}

bool CYNetLink::stop()
{
//  onLine = false;
  return (true);
}

QString CYNetLink::state()
{
  if (onLine)
    return (QString(tr("Connected")));
  else
    return (QString(tr("Disconnected")));
}

void CYNetLink::stateCom(bool state, int com)
{
  if ((com != numCom) || (numCom==-1) || !core->module->started())
    return;
  
  if (core->module->readingStr()<0)
    CYFATALTEXT(TRFR("Affecter mReadingStr dans NetModbus::readData"));

  // si pas la structure de détection communication
  if (core->module->readingStr()!=mDetectStr)
    return;

  // PERTE DE COM
  if (!state && onLine)
  {
    CYMESSAGETEXT(QString("%1: PERTE DE COM n°%2").arg(objectName()).arg(numCom+1));
    onLine = false;
    return;
  }

  // REPRISE DE COM
  if (state && !onLine)
  {
    CYMESSAGETEXT(QString("%1: REPRISE DE COM n°%2").arg(objectName()).arg(numCom+1));
    onLine = true;
    if (!core->module->R422F_running())
    {
      QHashIterator<QString, CYDB *> it(dbList);
      while (it.hasNext())
      {
        it.next();                   // must come first
        if (it.value()->autoLoad())
        {
          CYMESSAGETEXT(QString("CHARGEMENT DE %1").arg(it.value()->objectName()));
          it.value()->load();
          it.value()->writeData();
        }
      }
      // Envoie automatique du(des) projet(s) à la détection de la communication.
      core->sendProject();
    }
  }
}


/*! Saisie la base temps en ms
    \fn CYNetLink::setBTMS( float val )
 */
void CYNetLink::setBTMS( float val )
{ 
  mBTMS = val;
}


/*! @return la fréquence de la connexion
    \fn CYNetLink::frequency()
 */
QString CYNetLink::frequency()
{
  if ( (1.0/mBTMS) >= 1.0 )
    return tr("%1 kHz").arg(1.0/mBTMS);
  else  
    return tr("%1 Hz").arg(1000.0/mBTMS);
}
