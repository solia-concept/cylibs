//
// C++ Implementation: cynetr422file
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <qfileinfo.h>
#include <qdir.h>
// cylibs
#include "cynetr422file.h"
#include "cycore.h"

CYNetR422File::CYNetR422File( const QString & name, QString path )
 : QFile(name)
{
  if (name.contains("/"))  // Fichier local à installer
  {
    install = true;
    fileName = QString(name).section( "/", -1 );
    src = name;
    src.remove(fileName);
    dest = path;
  }
  else  // fichier distant à sauvegarder
  {
    install = false;
    fileName = QString(name).section( "\\", -1 );
    src = name;
    src = src.remove(fileName);
    dest = path;
    if (!QDir(dest).exists())
      core->mkdir(dest);
  }
  num_block  = 0;
  inum_block = 0;
}


CYNetR422File::~CYNetR422File()
{
}
