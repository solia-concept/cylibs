//
// C++ Interface: cynetr422file
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYNETR422FILE_H
#define CYNETR422FILE_H

// QT
#include <qfile.h>
// CYLIBS
#include "cytypes.h"

/**
@short Fichier transféré par R422F.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYNetR422File : public QFile
{

public:
    CYNetR422File( const QString & name, QString path);

    ~CYNetR422File();

    QString fileName;

    /** Si \a true fichier à installer sur régulateur sinon fichier à sauvegarder du régulateur. */
    bool install;
    QString src;
    QString dest;
    s16 num_block;
    s16 inum_block;
    s32 file_size;
};

#endif
