//
// C++ Implementation: cynetr422f
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cynetr422f.h"
#include "ui_cynetr422f.h"

// QT
#include <QTextStream>
#include <QProcess>
#include <QTextBrowser>
#include <QCloseEvent>
// CYLIBS
#include "cycore.h"
#include "cynetlink.h"
#include "cynetmodule.h"
#include "cypushbutton.h"
#include "cystring.h"
#include "cymessagebox.h"

#define R422F_INIT          0
#define R422F_START         1
#define R422F_FAULT         2
#define R422F_STOP          3
#define R422M_STOP_RN      10
#define R422M_START_RN     11
#define R422M_STOP_R422F   12
#define R422M_START_R422F  13
#define TEST_NO_COM_RN     14
#define TEST_COM_RN        15
#define TEST_COM_R422F     16
#define TEST_NO_COM_R422F  17
#define CMD_REBOOT_RN      20
#define CMD_WAIT           21
#define CMD_REBOOT_R422F   22
#define WRITE_FILE_INIT    30
#define WRITE_FILE         31
#define WRITE_FILE_ATT     32
#define WRITE_FILE_FIN     33
#define READ_FILE_INIT     40
#define READ_FILE          41
#define READ_FILE_ATT      42
#define READ_FILE_FIN      43
#define ATT_CLOSE_FILE     50
#define CLOSE_FILE         51
#define NEXT_FILE          52

CYNetR422F::CYNetR422F(CYNetHost *host, QString fileName, QWidget *parent, const QString &name)
  : CYDialog(parent,name),
   mHost(host)
{
  ui = new Ui::CYNetR422F;
  ui->setupUi(this);

  if (QString(name).isEmpty())
    setObjectName(QString("R422F_%1").arg(host->objectName()));

  mTimer = new QTimer(this);
  connect(mTimer, SIGNAL(timeout()), this, SLOT(run()));

  st_info = (s_r422f_info *)core->module->ptrStruct(ST_R422F_INFO);
  st_read = (s_r422f_read *)core->module->ptrStruct(ST_R422F_READ);
  if (st_info && st_read)
  {
    mLink=mHost->R422FLink();
    mode=R422F_INIT;
    run();

    if (fileName.endsWith(".tar.gz") || fileName.endsWith(".tar"))
      open(fileName);
    else
      init(fileName);
  }
}


CYNetR422F::~CYNetR422F()
{
  delete ui;
}

void CYNetR422F::open(QString fileName)
{
  if (!QFile(fileName).exists())
  {
    CYMessageBox::error(this, tr("Doesn't exist '%1' !").arg(fileName), tr("Updating of regulator"));
    return;
  }

  if (!fileName.endsWith(".tar.gz") && !fileName.endsWith(".tar"))
  {
    init(fileName);
    return;
  }

  QString name = fileName.section( '/', -1 );
  name.remove(".tar.gz");
  name.remove(".tar");

  if (name.contains('_'))
  {
    QString new_serie = name.section( '_', 0, 0);
    QString new_version = name.section( '_', -1);

    if ((core->module->nb_st>NB_ST_R422F) && core->module->testCom( mLink->getCOM()))
    {
      CYString *serie = (CYString *)core->findData(QString("INFOPC_N_SERIE_REQUIRED_%1").arg(mHost->index));
      if (serie->val()!=new_serie)
      {
        if (CYMessageBox::Yes !=
            CYMessageBox::warningYesNo(0, "<p align=""center"">"+tr("Are you sure to update this regulator !")+"<br>"+tr("Its serial number must be %1, while the software you want install has a as serial number %2 !").arg(serie->val()).arg(new_serie)+"</p>"))
          return ;
      }
      else
      {
        CYString *version = (CYString *)core->findData(QString("INFOPC_VERSION_%1").arg(mHost->index));
        if (CYMessageBox::Continue !=
            CYMessageBox::warningContinueCancel(0, "<p align=""center"">"+tr("Update of regulator from the version %1 to the version %2. ").arg(version->val()).arg(new_version)+"</p>"))
          return ;
      }
    }
    else
    {
      if (CYMessageBox::Continue !=
          CYMessageBox::warningContinueCancel(this, "<p align=""center"">"+tr("Are you sure to install on the regulator the version %1 of the software %2 !").arg(new_version).arg(new_serie)+"</p>"))
        return ;
    }
  }
  else
  {
    if (CYMessageBox::Continue !=
        CYMessageBox::warningContinueCancel(0, "<p align=""center"">"+tr("Install the content of this archive on this regulator !")+"</p>"))
      return ;
  }

  QString tmpPath;
  tmpPath=QString("%1/%2").arg(QDir::temp().absolutePath()).arg(name);
  QDir tmpDir(tmpPath);

  if (tmpDir.exists() && !tmpDir.removeRecursively())
  {
    CYMessageBox::error(this, tr("Cannot remove the old directory %1 !").arg(tmpPath), tr("Updating of regulator"));
    return;
  }

  // Décompresse le fichier de mise à jour
  QProcess proc;
  proc.setWorkingDirectory(QDir::temp().absolutePath());

#if defined(Q_OS_WIN)
  QFileInfo fileInfo(fileName);
  QString program = "tar";
  QStringList arguments;
  arguments << "-xvzf";
  arguments << fileName;
  arguments << "-C";
  arguments << QDir::temp().absolutePath();
  proc.start(program, arguments);
#elif defined(Q_OS_LINUX)
  QString program = "tar";
  QStringList arguments;
  arguments << "xvzf";
  arguments << fileName;
  proc.start(program, arguments);
#endif
  if (!proc.waitForStarted(2000))
  {
    CYMessageBox::error(this, tr("Cannot start uncompressing file of update (%1)!").arg(fileName), tr("Updating of regulator"));
    return;
  }
  if (!proc.waitForFinished(5000))
  {
    CYMessageBox::error(this, tr("Cannot finish uncompressing file of update (%1)!").arg(fileName), tr("Updating of regulator"));
    return;
  }

  // Ouvre le fichier d'initialisation de la mise à jour
  QString imajName = QString("%1/r422f.ini").arg(tmpPath);
  QFile imaj;
  imaj.setFileName(imajName);
  if (!imaj.exists())
  {
    imajName = QString("%1/R422F.INI").arg(name);
    imaj.setObjectName(imajName);
    if (!imaj.exists())
    {
      CYMessageBox::error(0, tr("Init file doesn't exist !")+"\n"+tmpPath, tr("Updating of regulator"));
      return;
    }
  }
  init(imajName);
}

void CYNetR422F::init(QString fileName)
{
  if (!QFile(fileName).exists())
  {
    CYMessageBox::error(this, tr("Doesn't exist '%1' !").arg(fileName), tr("Updating of regulator"));
    return;
  }

  mLocalDir = QFileInfo(fileName).dir();
  QFile file(fileName);
  bool ok=true;
  reboot=false;
  QString section, Linux, Dos;
  QString val;
  if (file.open(QIODevice::ReadOnly))
  {
    QTextStream stream( &file );
    while ( !stream.atEnd() && ok )
    {
      QString line = stream.readLine();
      if (line.startsWith("Reboot"))
      {
        val = line.remove("Reboot=");
        if (val=="true")
          reboot=true;
      }
      if (line.startsWith("[") && line.endsWith("]"))
      {
        // Démarre une nouvelle section
        section=line;
        Linux="";
        Dos="";
      }
      if (line.startsWith("Linux"))
      {
        Linux=line.remove("Linux=");
      }
      if (line.startsWith("Dos"))
      {
        Dos=line.remove("Dos=");
      }
      if (line.startsWith("File"))
      {
        if ((section=="[INSTALL]") || (section=="[SAVE]"))
        {
          ok = fileLine(section, line.remove("File="), Linux, Dos);
        }
        else
        {
          CYMessageBox::error(this, tr("Section %1 unknown in the init file !").arg(section));
          ok = false;
        }
      }
    }
    file.close();
  }
  else
  {
    CYMessageBox::error(this, tr("Cannot open the init file !").arg(section));
    return;
  }

  if (!ok)
    return;

  mode=R422F_START;
  run();
}

bool CYNetR422F::fileLine(QString section, QString File, QString Linux, QString Dos)
{
  QString fileName, dest, src;

  if (section=="[INSTALL]")
  {
    if (Dos=="")
    {
      fileName = File.section( '\\', -1 );

      src = mLocalDir.path();
      dest = File.remove( fileName );

      int nb = File.count("\\");
      for (int i=1; i<nb; i++)
      {
        QString dirName = src+"/"+File.section( '\\', i, i );
        if (QDir(dirName).exists())
        src = dirName;
      }
    }
    else
    {
      fileName = File;
      src=(Linux=="") ? mLocalDir.path() : Linux;
      dest=Dos;
    }

    if (!dest.endsWith("\\"))
      dest.append("\\");

    if (dest.length()>R422F_PATH_L)
    {
      CYMessageBox::error(this, tr("The destination directory path '%1' is too long !").arg(dest));
      return false;
    }


    if (fileName.contains("*.*"))
    {
      QDir dir(src);
      QString nameFilter = fileName.section('.', -1);
      QStringList fileList = dir.entryList(QStringList(nameFilter), QDir::Files);
      for ( QStringList::Iterator it = fileList.begin(); it != fileList.end(); ++it )
      {
        if (!fileToInstall( *it, src, dest ))
        {
          return false;
        }
      }
    }
    else if (!fileToInstall( fileName, src, dest ))
      return false;
  }
  else
  {
    fileName = File;

    dest=(Linux=="") ? mLocalDir.path() : Linux;
    src=Dos;

    if (!src.endsWith("\\"))
      src.append("\\");

    if (!dest.endsWith("/"))
      dest.append("/");

    if (dest.length()>R422F_PATH_L)
    {
      CYMessageBox::error(this, tr("The source directory path '%1' is too long !").arg(src));
      return false;
    }

    if (!fileToSave( fileName, src, dest ))
      return false;
  }
  return true;
}

bool CYNetR422F::fileToInstall(QString fileName, QString src, QString dest)
{
  if (fileName=="*.*")
  {
    CYMessageBox::error(this, tr("The file name '%1' is not correct for transfert file !").arg(fileName));
    return false;
  }

  if (fileName.length()>12)
  {
    CYMessageBox::error(this, tr("The file name '%1' is too long !").arg(fileName));
    return false;
  }

  CYNetR422File *file = new CYNetR422File(src+"/"+fileName, dest);

  if (!file->exists())
  {
    fileName = fileName.toUpper();
    CYNetR422File *file2 =new CYNetR422File(src+"/"+fileName, dest);
    if (file2->exists())
    {
      file=file2;
    }
    else
    {
      fileName = fileName.toLower();
      file2 =new CYNetR422File(src+"/"+fileName, dest);
    }

    if (file2->exists())
    {
      file=file2;
    }
    else
    {
      CYMessageBox::error(this, tr("'%1' doesn't exist !").arg(file->objectName()));
      return false;
    }
  }

  if (!file->open( QIODevice::ReadOnly ))
  {
    CYMessageBox::error(this, tr("Cannot open the file '%1' !").arg(file->objectName()));
    return false;
  }

  file->file_size = file->size();
  if (!file->file_size)
  {
    if (core->designer())
    {
      error(tr("File %1 is empty!").arg(fileName));
    }
  }
  else
  {
    files.append(file);
    if (core->designer())
    {
      message(tr("Install %1 in %2").arg(fileName).arg(dest), Qt::gray, "tt");
    }
  }
  return true;
}


bool CYNetR422F::fileToSave(QString fileName, QString src, QString dest)
{
  if (fileName.length()>12)
  {
    CYMessageBox::error(this, tr("The file name '%1' is too long !").arg(fileName));
    return false;
  }

  CYNetR422File *file = new CYNetR422File(src+fileName, dest);

  files.append(file);
  if (core->designer())
  {
    message(tr("Save %1 in %2").arg(src+fileName).arg(dest), Qt::gray, "tt");
  }
  return true;
}

void CYNetR422F::run()
{
  mem_mode = mode;

  switch(mode)
  {
    case R422F_INIT:
      mTimer->stop();
      reboot = false;
      cpt_sec=0;
      max_sec=60;
      disable_r422f=false;
      break;
    case R422F_START:
      ui->okButton->setEnabled(false);
      mTimer->start(1);
      t.restart();
      mode = R422M_STOP_RN;
      break;
    case R422F_FAULT:
      reboot = true;
      disable_r422f=true;
      t.restart();
      mode = CMD_REBOOT_R422F;
      break;
    case R422F_STOP:
      core->module->readData(ST_R422F_INFO, 0, mLink->getCOM());
      version_r422f = st_info->version;
      if (!version_r422f.isEmpty())
      {
        error(tr("Regulator is blocked in file transfert mode !"));
        error(tr("YOU MUST REBOOT MANUALLY THE REGULATOR !"));
      }
      else
        message(tr("Regulator ok !"), "black", "b");
      ui->okButton->setEnabled(true);
      mode = R422F_INIT;
      break;
    //================================================
    case R422M_STOP_RN:
      // Attente avant arrêt du module
      if (t.elapsed()>1000)
      {
        message(tr("Stop of communication driver in normal mode."), Qt::blue);
        core->module->end();
        t.restart();
        mode = R422M_START_R422F;
      }
      break;
    case R422M_STOP_R422F:
      // Attente avant arrêt du module
      if (t.elapsed()>1000)
      {
        message(tr("Stop of communication driver in file transfert mode."), Qt::magenta);
        core->module->end();
        t.restart();
        mode = R422M_START_RN;
      }
      break;
    case TEST_NO_COM_RN:
      // Attente avant test perte com
      if (t.elapsed()>2000)
      {
        // Test perte de la COM
        bool etat_com;
        if(core->module->isModbus())
           etat_com = core->module->testCom_forc( mLink->getCOM() );
         else
           etat_com = core->module->testCom( mLink->getCOM() );

         if(!etat_com)
         {
          message(tr("Regulator disconnected."));
          para_ok = ui->textBrowser->document()->blockCount();
          t.restart();
          mode = TEST_COM_R422F;
        }
        else
        {
          error(tr("Cannot reboot the regulator !"));
          t.restart();
          mode = R422M_STOP_R422F;
        }
      }
      break;
    case TEST_NO_COM_R422F: //
     // bool etat_com;
      // Attente avant test perte com
      if (t.elapsed()>1000) // Test toutes les secondes
      {
        cpt_sec++;
        // Test perte de la COM
       bool etat_com;
       if(core->module->isModbus())
          etat_com = core->module->testCom_forc( mLink->getCOM() );
        else
          etat_com = core->module->testCom( mLink->getCOM() );

        if(!etat_com)
        {
          message(tr("Regulator disconnected."));
          para_ok = ui->textBrowser->document()->blockCount();
          mode = TEST_COM_R422F;
        }
        else
        {
          // TODO QT5 ui->textBrowser->removeParagraph(para_ok);
          message(tr("Waiting for disconnection (Maximum time %1 sec)...").arg(max_sec-cpt_sec));
        }
        t.restart();
      }
      else if (cpt_sec>=max_sec) // Temps maxi pour déconnexion atteint
      {
        error(tr("Cannot reboot the regulator !"));
        mode = R422M_STOP_R422F;
      }
      if (mode!=TEST_NO_COM_R422F)
        cpt_sec=0;
      break;
    case R422M_START_R422F:
      // Attente avant redémarrage du module
      if (t.elapsed()>2000)
      {
        message(tr("Restart of communication driver in file transfert mode."), Qt::magenta);
        core->module->setR422F(true);
        core->module->raz_NB_MAX_ST();
        core->module->init_NB_ST_R422F();
        core->module->init();
        core->module->start();
        core->module->razCounters();

        t.restart();
        mode = CMD_REBOOT_R422F;
      }
      break;
    case R422M_START_RN:
      // Attente avant redémarrage du module
      if (t.elapsed()>2000)
      {
        message(tr("Restart of communication driver in normal mode."), Qt::blue);
        core->module->raz_NB_MAX_ST();
        core->module->init_NB_ST_R422F();
        core->module->init_NB_ST();
        core->module->init();
        core->module->start();
        core->module->razCounters();
        core->module->setR422F(false);
        para_ok = ui->textBrowser->document()->blockCount();
        t.restart();
        mode = TEST_COM_RN;
      }
      break;
    case TEST_COM_R422F:
      if (t.elapsed()>1000) // Test toutes les secondes
      {
        cpt_sec++;
        // Test retour de la COM
       bool etat_com;
       if(core->module->isModbus())
          etat_com = core->module->testCom_forc( mLink->getCOM() );
        else
          etat_com = core->module->testCom( mLink->getCOM() );


        if(etat_com)
        {
          if (!core->module->readData(ST_R422F_INFO, 0, mLink->getCOM()) && (cpt_sec>=max_sec))
          {
            error(tr("Cannot read info datas from regulator !"));
            mode = R422F_FAULT;
          }
          else
          {
            version_r422f = st_info->version;
            if (version_r422f.isEmpty())
            {
              error(tr("Regulator reconnected in normal mode instead of file transfert mode !"));
              mode = R422F_FAULT;
            }
            else
            {
              message(tr("Regulator reconnected in file transfert mode (R422F %1).").arg(version_r422f), Qt::magenta);
              if (disable_r422f)
                mode = R422M_STOP_R422F;
              else
                mode = CMD_WAIT;
            }
          }
          para_ok = ui->textBrowser->document()->blockCount();
        }
        else
        {
          // TODO QT5 ui->textBrowser->removeParagraph(para_ok);
          message(tr("Waiting for reconnection (Maximum time %1 sec)...").arg(max_sec-cpt_sec));
        }
        t.restart();
      }
      else if (cpt_sec>=max_sec) // Temps maxi pour reconnexion atteint
      {
        error(tr("Cannot reconnect to the regulator !"));
        mode = CMD_REBOOT_R422F;
      }
      if (mode!=TEST_COM_R422F)
        cpt_sec=0;
      break;
    case TEST_COM_RN:
      if (t.elapsed()>2000) // Test toutes les 2 secondes
      {
        cpt_sec++;
        // Test retour de la COM
        if(core->module->testCom( mLink->getCOM() ))
        {
          core->module->readData(ST_R422F_INFO, 0, mLink->getCOM());
          version_r422f = st_info->version;
          if (!version_r422f.isEmpty())
          {
            error(tr("Regulator reconnected in file transfert mode (R422F %1) instead of normal mode !").arg(version_r422f));
            mode = R422F_STOP;
          }
          else
          {
            message(tr("Regulator reconnected in normal mode."), Qt::blue);
            mode = R422F_STOP;
          }
          para_ok = ui->textBrowser->document()->blockCount();
        }
        else
        {
          // TODO QT5 ui->textBrowser->removeParagraph(para_ok);
          message(tr("Waiting for reconnection (Maximum time %1 sec)...").arg(max_sec-cpt_sec));
        }
        t.restart();
      }
      else if (cpt_sec>=max_sec) // Temps maxi pour reconnexion atteint
      {
        error(tr("Cannot reconnect to the regulator !"));
        core->module->end();
        t.restart();
        mode = R422F_STOP;
      }
      if (mode!=TEST_COM_RN)
        cpt_sec=0;
      break;
    //================================================
    case CMD_REBOOT_RN:
      cmd(R422FC_REBOOT);
      message(tr("Regulator rebooting..."));
      t.restart();
      mode = R422M_STOP_RN;
      break;
    case CMD_REBOOT_R422F:
      cmd(R422FC_REBOOT);
      message(tr("Regulator rebooting..."));
      para_ok = ui->textBrowser->document()->blockCount();
      t.restart();
      mode = TEST_NO_COM_R422F;
      break;
    case CMD_WAIT:
      cmd(R422FC_WAIT);
      message(tr("Starting file transfert..."), Qt::magenta);
      t.restart();
      cpt_file=0;
      currentFile = files.value(cpt_file++);
      if (currentFile)
      {
        if(core->module->isModbus())
          core->module->clearFifo( ST_R422F_INFO );

        if (currentFile->install)
        {
          mode = WRITE_FILE_INIT;
        }
        else
        {
          mode = READ_FILE_INIT;
        }
      }
      else
      {
        error(tr("No file to transfert !"));
        t.restart();
        mode = CMD_REBOOT_R422F;
      }
      break;
    //================================================
    case WRITE_FILE_INIT:
      //Demande ecriture de 1 fichier
      cmd(R422FC_WRITE, currentFile->dest, currentFile->fileName, currentFile->file_size);
      cpt_size =currentFile->file_size;
      size_block=0;
      para_ok = ui->textBrowser->document()->blockCount();
      message(tr("Install : %1 (%2 %)").arg(currentFile->fileName).arg(0), Qt::magenta);
      t.restart();
      t2.restart();
      mode = WRITE_FILE;
      break;
    case WRITE_FILE:
      // Écriture du fichier par trame
      size_block = (cpt_size < R422F_SIZE_BLOCK) ? cpt_size : R422F_SIZE_BLOCK;
      currentFile->seek(currentFile->num_block*R422F_SIZE_BLOCK);
      currentFile->read(data, size_block);
      write(currentFile->num_block, size_block, data);
      // calcul taille restante à transférer
      cpt_size = cpt_size-size_block;
      t.restart();
      mode = WRITE_FILE_ATT;
      break;
    case WRITE_FILE_ATT: // Attente retour d'écriture trame régulateur

      //Mise a jour de la structure INFO
      core->module->readData(ST_R422F_INFO, 0, mLink->getCOM());

      switch (st_info->info)
      {
        case R422FI_WAIT     :
          if (t.elapsed()>5000)
          {
            error(tr("No answer from the regulator !"));
            mode = R422F_FAULT;
            break;
          }
          break;

        case R422FI_TRAME_OK :
          currentFile->inum_block=st_info->num_block;
          if (currentFile->inum_block==currentFile->num_block) // Controle si nouvelle trame
          {
            if (cpt_size)
            {
              if (t2.elapsed()>250) // Empêche les scintillements
              {
                // TODO QT5 ui->textBrowser->removeParagraph(para_ok);
                int pc = ((currentFile->file_size-cpt_size)*100)/currentFile->file_size;
                message(tr("Install : %1 (%2 %)").arg(currentFile->fileName).arg(pc), Qt::magenta);
                t2.restart();
              }
              currentFile->num_block++;
              mode = WRITE_FILE;
            }
            else // fin du transfert du fichier
            {
              int pc = ((currentFile->file_size-cpt_size)*100)/currentFile->file_size;
              message(tr("Install : %1 (%2 %)").arg(currentFile->fileName).arg(pc), Qt::magenta);
              currentFile->close();
              mode = WRITE_FILE_FIN;
            }
          }
          else if (t.elapsed()>5000) // Attente de la nouvelle trame
          {
            error(tr("No answer from the regulator !"));
            mode = R422F_FAULT;
          }
          break;

        case R422FI_DEL_FILE  : error(tr("Cannot remove file '%1\\%2' on the regulator !").arg(currentFile->dest).arg(currentFile->fileName)); mode = R422F_FAULT; break;
        case R422FI_MAKE_FILE : error(tr("Cannot make file '%1\\%2' on the regulator !").arg(currentFile->dest).arg(currentFile->fileName));   mode = R422F_FAULT; break;
        case R422FI_NUM_BLOCK : error(tr("Block number error '%1' !").arg(currentFile->num_block));                                           mode = R422F_FAULT; break;
        case R422FI_SIZE_FILE : error(tr("File size error '%1\\%2' !").arg(currentFile->dest).arg(currentFile->fileName));                     mode = R422F_FAULT; break;
        default               : error(tr("Regulator state unknown : '%1' !").arg(st_info->info));                                             mode = R422F_FAULT; break;
      }
      break;
    case WRITE_FILE_FIN:
      // TODO QT5 ui->textBrowser->removeParagraph(para_ok);
      mode = NEXT_FILE;
      break;
    //================================================
    case READ_FILE_INIT:
      //Demande lecture de 1 fichier
      cmd(R422FC_READ, currentFile->src, currentFile->fileName);
      cpt_size =0;
      size_block=0;
      currentFile->fileName ="";
      t.restart();
      t2.restart();
      mode = READ_FILE_ATT;
      break;
    case READ_FILE:
      // Lecture du fichier par trame
      if ((t.elapsed()>core->module->ioctlTime()) && read())
      {
        size_block = st_read->size_block;
        if (!size_block)
        {
          error(tr("Bad size block to read !"));
          mode = R422F_FAULT;
          break;
        }
        cpt_size = cpt_size + size_block;
        if (currentFile->num_block!=st_read->num_block)
        {
          error(tr("Bad num block to read !"));
          mode = R422F_FAULT;
          break;
        }
        currentFile->seek(currentFile->num_block*R422F_SIZE_BLOCK);
        currentFile->write(st_read->data, st_read->size_block);
        // Indique au régulateur que la trame a bien été lue
        cmd(R422FC_TRAME_OK, currentFile->src, currentFile->fileName);
        cmd(R422FC_WAIT);
        currentFile->num_block++; // bloc suivant
        t.restart();
        mode = READ_FILE_ATT;
      }
      else if (t.elapsed()>5000) // Attente de la nouvelle trame
      {
        error(tr("No answer from the regulator !"));
        mode = R422F_FAULT;
      }
      break;
    case READ_FILE_ATT: // Attente info pour lecture trame du régulateur

      //Mise a jour de la structure INFO
      core->module->readData(ST_R422F_INFO, 0, mLink->getCOM());
      currentFile->inum_block=st_info->num_block;

      switch (st_info->info)
      {
        case R422FI_WAIT     :
          if (currentFile->num_block && !st_info->file_size) // régulateur en attente sans bloc suivant à lire
          {
            // fin du transfert du fichier ou des fichiers dans si "File=*.*"
            message(tr("Save : %1 (%2 %)").arg(currentFile->fileName).arg(100), Qt::magenta);
            currentFile->close();
            mode = READ_FILE_FIN;
          }
          else if (t.elapsed()>5000)
          {
            error(tr("No answer from the regulator !"));
            mode = R422F_FAULT;
          }
          break;

        case R422FI_TRAME_OK :
          if (QString(st_info->file_name).isEmpty() && (t.elapsed()>5000)) // Attente du nom de fichier
          {
            error(tr("No file name from the regulator !"));
            mode = R422F_FAULT;
            break;
          }

          if (currentFile->fileName != QString(st_info->file_name))
          {
            // nouveau fichier
            if (!currentFile->fileName.isEmpty())
            {
              // TODO QT5 ui->textBrowser->removeParagraph(para_ok);
              message(tr("Save : %1 (%2 %)").arg(currentFile->fileName).arg(100), Qt::magenta);
              currentFile->close();
            }
            currentFile->fileName = st_info->file_name;
            currentFile->setFileName(currentFile->dest+currentFile->fileName);
            currentFile->file_size=st_info->file_size;
            currentFile->num_block=0;
            cpt_size=0;
            t2.restart();
            if (!currentFile->open(QIODevice::WriteOnly))
            {
              error(tr("Cannot open file '%1' !").arg(currentFile->objectName()));
              mode = R422F_FAULT;
              break;
            }
            para_ok = ui->textBrowser->document()->blockCount();
            message(tr("Save : %1 (%2 %)").arg(currentFile->fileName).arg(0), Qt::magenta);
          }

          if (!currentFile->fileName.isEmpty() && (currentFile->inum_block==currentFile->num_block)) // Controle si nouvelle trame
          {
            if (t2.elapsed()>250) // Empêche les scintillements
            {
              // TODO QT5 ui->textBrowser->removeParagraph(para_ok);
              float pc = 100.0* (1.0-(((float)currentFile->file_size-(float)cpt_size))/(float)currentFile->file_size);
              message(tr("Save : %1 (%2 %)").arg(currentFile->fileName).arg((int)pc), Qt::magenta);
              t2.restart();
            }
            t.restart();
            read();
            mode = READ_FILE;
          }
          else if (t.elapsed()>5000) // Attente de la nouvelle trame
          {
            error(tr("No answer from the regulator !"));
            mode = R422F_FAULT;
          }
          break;

        case R422FI_NUM_BLOCK : error(tr("Block number error '%1' !").arg(currentFile->num_block));                                           mode = R422F_FAULT; break;
        case R422FI_SIZE_FILE : error(tr("File size error '%1\\%2' !").arg(currentFile->dest).arg(currentFile->fileName));                     mode = R422F_FAULT; break;
        case R422FI_NODIR     : error(tr("Cannot find directory '%1' !").arg(currentFile->src));                                              mode = R422F_FAULT; break;
        case R422FI_NOFILE    : error(tr("Cannot find file '%1' !").arg(currentFile->fileName));                                              mode = R422F_FAULT; break;
        case R422FI_OPENFILE  : error(tr("Cannot open file '%1' !").arg(currentFile->fileName));                                              mode = R422F_FAULT; break;
        default               : error(tr("Regulator state unknown : '%1' !").arg(st_info->info));                                             mode = R422F_FAULT; break;
      }
      break;
    case READ_FILE_FIN:
      // TODO QT5 ui->textBrowser->removeParagraph(para_ok);
      mode = NEXT_FILE;
      break;
    //================================================
    case NEXT_FILE:
      currentFile = files.value(cpt_file++);
      if (currentFile)
      {
        if (currentFile->install)
        {
          mode = WRITE_FILE_INIT;
        }
        else
        {
          mode = READ_FILE_INIT;
        }
      }
      else
      {
        if (reboot)
        {
          disable_r422f=true;
          mode = CMD_REBOOT_R422F;
        }
        else
        {
          cmd(R422FC_END);
          mode = R422M_STOP_R422F;
        }
        message(tr("End of file transfert."), Qt::magenta);
        t.restart();
      }
      break;
    default:
      break;
  }
  if (mode!=mem_mode)
  {
//     message(tr("mode = %1 => %2").arg(mem_mode).arg(mode), gray, "tt");
  }
}

bool CYNetR422F::cmd(s8 cmd, QString path_name, QString file_name, int file_size)
{
  return core->module->cmdR422F(mLink->getCOM(), cmd, path_name, file_name, file_size);
}

bool CYNetR422F::write(s16 num_block, s16 size_block, s8 *data)
{
  return core->module->writeR422F(mLink->getCOM(), num_block, size_block, data);
}

bool CYNetR422F::read()
{
  return core->module->readR422F(mLink->getCOM());
}

void CYNetR422F::message(QString txt, QColor color, QString style)
{
  if ((color.name()=="") && style.isEmpty())
    ui->textBrowser->append(txt);
  else if ((color.name()=="") && !style.isEmpty())
    ui->textBrowser->append(QString("<%1>%2</%3>").arg(style).arg(txt).arg(style));
  else if (style.isEmpty())
    ui->textBrowser->append(QString("<font color=%1>%2</font>").arg(color.name()).arg(txt));
  else
    ui->textBrowser->append(QString("<%1><font color=%2>%3</font></%4>").arg(style).arg(color.name()).arg(txt).arg(style));
}

void CYNetR422F::error(QString txt)
{
  message(txt, "red", "i");
}

void CYNetR422F::closeEvent(QCloseEvent * e)
{
  if (mTimer->isActive())
  {
    CYMessageBox::sorry(this, tr("You can't close this box before the end of the transfert !"), tr("File transfert"));
  }
  else
    CYDialog::closeEvent(e);
}
