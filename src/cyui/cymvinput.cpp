/***************************************************************************
                          cymvinput.cpp  -  description
                             -------------------
    begin                : jeu déc 30 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cymvinput.h"

// QT
#include <QGroupBox>
// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "cymv.h"
#include "ui_cymvinput.h"

CYMVInput::CYMVInput(QWidget *parent, const QString &name)
  : CYWidget(parent,name), ui(new Ui::CYMVInput)
{
  ui->setupUi(this);
  mMV = 0;
}

CYMVInput::~CYMVInput()
{
  delete ui;
}

void CYMVInput::setGenericMark(const QString txt)
{
  CYWidget::setGenericMark(txt.toUtf8());
  linkDatas();
}

void CYMVInput::linkDatas()
{
  CYWidget::linkDatas();
  if(core)
  {
    mMV = core->mv(genericMark(), false);
    if (mMV)
    {
      ui->forwardBox->setTitle(mMV->forwardDesc()+" ("+tr("Forward motion")+")");
      ui->backwardBox->setTitle(mMV->backwardDesc()+" ("+tr("Backward motion")+")");
    }
  }
}
