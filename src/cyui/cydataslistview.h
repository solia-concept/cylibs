//
// C++ Interface: cydataslistview
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDATASLISTVIEW_H
#define CYDATASLISTVIEW_H

// QT
#include <QTreeWidgetItem>
#include <QPen>
#include <QDragEnterEvent>
#include <QDropEvent>
// CYLIBS
#include "cytreewidget.h"

class CYDisplayItem;

/**
@short Vue d'une liste de données.

@author Gérald LE CLEACH
*/
class CYDatasListView : public CYTreeWidget
{
Q_OBJECT
    Q_PROPERTY( int maxNbDatas READ maxNbDatas WRITE setMaxNbDatas )
    Q_PROPERTY( bool withColor READ withColor WRITE setWithColor )
    Q_PROPERTY( bool withPen READ withPen WRITE setWithPen )
    Q_PROPERTY( bool withCoef READ withCoef WRITE setWithCoef )
    Q_PROPERTY( bool withOffset READ withOffset WRITE setWithOffset )

public:
    CYDatasListView(QWidget *parent = 0, const QString &name = 0);

    ~CYDatasListView();
    virtual void addData(CYDisplayItem *item, int pos=-1);

    virtual int column(QString label);

    virtual int numColumn();

    virtual int colorColumn();
    virtual QColor color(int num);
    virtual QColor color(QTreeWidgetItem *lvi);

    virtual int penColumn();
    virtual QPen pen(int num);
    virtual QPen pen(QTreeWidgetItem *lvi);

    virtual int coefColumn();
    virtual double coef(int num);
    virtual double coef(QTreeWidgetItem *lvi);

    virtual int offsetColumn();
    virtual double offset(int num);
    virtual double offset(QTreeWidgetItem *lvi);

    virtual int dataNameColumn();
    virtual QString dataName(int num);
    virtual QString dataName(QTreeWidgetItem *lvi);

    virtual int labelColumn();
    virtual QString label(QTreeWidgetItem *lvi);

    virtual int maxNbDatas() const;
    virtual bool withColor() const;
    virtual bool withPen() const;
    virtual bool withCoef() const;
    virtual bool withOffset() const;
    virtual int addColumn ( const QString & label, int width = -1 );

    virtual QTreeWidgetItem * item( int num );
    virtual int num(QTreeWidgetItem * item);
    bool find(CYDisplayItem *item);
    QString posToString(int pos);

    virtual void setBackgroundColorPen(QColor color) { mBackgroundColorPen=color; }

protected:
    virtual void dragEnterEvent( QDragEnterEvent * ev );
    virtual void dropEvent( QDropEvent * ev );

public slots:
    virtual void setColor();
    virtual void setPen();
    virtual void setCoef();
    virtual void setOffset();
    virtual void deleteData();
    virtual void setMaxNbDatas(const int val);
    virtual void setWithColor(const bool val);
    virtual void setWithPen(const bool val);
    virtual void setWithCoef(const bool val);
    virtual void setWithOffset(const bool val);
    virtual void addData(QString dataName);
    virtual void addGroup(QString txt);
    virtual void setLabel();

protected:
    int mNumColumn;
    int mColorColumn;
    int mPenColumn;
    int mCoefColumn;
    int mOffsetColumn;
    int mLabelColumn;
    int mGroupColumn;
    int mLinkColumn;
    int mHostColumn;
    int mUnitColumn;
    int mStatusColumn;
    int mDataNameColumn;

    int mMaxNbDatas;
    bool mWithColor;
    bool mWithPen;
    bool mWithCoef;
    bool mWithOffset;

    QColor mBackgroundColorPen;
};

#endif
