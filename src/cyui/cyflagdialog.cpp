#include "cyflagdialog.h"
#include "ui_cyflagdialog.h"


CYFlagDialog::CYFlagDialog(QWidget *parent, const QString &name)
  : CYDialog(parent, name), ui(new Ui::CYFlagDialog)
{
  ui->setupUi(this);
}


CYFlagDialog::~CYFlagDialog()
{
  delete ui;
}

void CYFlagDialog::setDataName(const QByteArray &name)
{
  ui->flagInput->setDataName(name);
  ui->textLabel->setDataName(name);
}
