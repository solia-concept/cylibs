/***************************************************************************
                          cylabel.h  -  description
                             -------------------
    début                  : jeu aoû 28 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYLABEL_H
#define CYLABEL_H

// QT
#include <qnamespace.h>
#include <qlabel.h>
//Added by qt3to4:
#include <QString>
#include <QShowEvent>
#include <QWheelEvent>
#include <QMouseEvent>
// CYLIBS
#include "cydatawdg.h"

/** @short Base d'un visualisteur.
  * @author Gérald LE CLEACH
  */

class CYLabel : public QLabel, public CYDataWdg
{
  Q_OBJECT
  Q_PROPERTY(bool movable READ movable WRITE setMovable)
  Q_PROPERTY(bool alwaysOk READ alwaysOk WRITE setAlwaysOk)
  Q_PROPERTY(bool autoRefresh READ autoRefresh WRITE setAutoRefresh)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray forcingName READ forcingName WRITE setForcingName)
  Q_PROPERTY(QByteArray settingName READ settingName WRITE setSettingName)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(int genericMarkOffset READ genericMarkOffset WRITE setGenericMarkOffset)
  Q_PROPERTY(Qt::Orientation orientation READ orientation WRITE setOrientation)
  Q_PROPERTY(bool mirror READ mirror WRITE setMirror)
  Q_PROPERTY(bool clickable READ clickable WRITE setClickable)
  Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
  Q_PROPERTY(QColor foregroundColor READ foregroundColor WRITE setForegroundColor)
  Q_PROPERTY(QPixmap backgroundPixmap READ backgroundPixmap WRITE setBackgroundPixmap)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:
  CYLabel(QWidget *parent=0, const QString &name=0);

  ~CYLabel();

  /** @return \a true si l'objet peut être déplacé.
    * @see void setMovable(const bool val). */
  virtual bool movable() const;
  /** Saisie \a true pour pouvoir déplacer. */
  virtual void setMovable(const bool val);

  /** @return \a true si l'objet peut être cliqué.
    * @see void setClickable(const bool val). */
  virtual bool clickable() const;
  /** Saisie \a true pour pouvoir cliquer. */
  virtual void setClickable(const bool val);

public: // Public methods
  /** @return \a true la donnée traîtée est considérée comme toujours valide. */
  virtual bool alwaysOk() const;
  /** La donnée traîtée est considérée comme toujours valide si \a val vaut true. */
  virtual void setAlwaysOk(const bool val);

  /** @return \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  bool autoRefresh() const { return mAutoRefresh; }
  /** Saisir \a true pour que le widget soit rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  void setAutoRefresh(const bool val) { mAutoRefresh = val; }

  /** @return \a true si le widget est en mode saisie. */
  bool enableReadOnly() const { return mEnableReadOnly; }
  /** Saisir \a true pour que le widget soit en mode saisie. */
  void setEnableReadOnly(const bool val) { mEnableReadOnly = val; }

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le nom de la donnée de forçage associée. */
  virtual QByteArray forcingName() const;
  /** Saisie le nom de la donnée de forçageassociée. */
  virtual void setForcingName(const QByteArray &name);

  /** @return le nom de la donnée en écriture. */
  virtual QByteArray settingName() const;
  /** Saisie le nom de la donnée en écriture. */
  virtual void setSettingName(const QByteArray &name);
  /** @return la donnée en écriture. */
  virtual CYData *settingData();
  /** @return \a true si l'objet graphique doît être connecté à une donnée en écriture. */
  virtual bool hasSettingData();

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return l'offset sur le repère de généricité lorqu'il s'agit d'un numéro. */
  virtual int genericMarkOffset() const;
  /** Saisie l'offset sur le repère de généricité lorqu'il s'agit d'un numéro. */
  virtual void setGenericMarkOffset(const int val);

  /** Saisie l'orientation du contenu. */
  virtual void setOrientation(const Qt::Orientation orientation);
  /** @return l'orientation du contenu. */
  Qt::Orientation orientation() const;

  /** @return la taille de la police.
   * Cette taille est initialisée par une taille de police par défaut avec un offset global à toute l'application.
   * Cette taille peut cependant être forcée par CYLabel::setFontSize. */
  virtual int fontSize() const;
  /** Saisie la taille de la police utilisée. */
  virtual void setFontSize(const int val);

  /** Saisie le flag d'activation du mirroir. */
  virtual void setMirror(bool flag);
  /** @return le flag d'activation du mirroir. */
  bool mirror() const;

  /** Saisie la couleur de fond du widget. */
  virtual void setBackgroundColor ( const QColor & );
  /** @return la couleur de fond du widget */
  virtual const QColor & backgroundColor () const;

  /** Saisie la couleur du premier plan du widget. */
  virtual void setForegroundColor ( const QColor & );
  /** @return la couleur du premier plan du widget */
  virtual const QColor & foregroundColor () const;

  /** Saisie l'image de fond. */
  virtual void setBackgroundPixmap ( const QPixmap & );
  /** @return la couleur du premier plan du widget */
  virtual const QPixmap & backgroundPixmap () const;

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** Saisie l'angle de rotation du contenu. */
  virtual void setRotate(int value);
  /** @return l'angle de rotation du contenu. */
  int rotate() const;
  /** Ajuste l'angle de rotation du contenu. */
  virtual void updateRotate();

  /** @return l'état de la commande. */
  bool state() const { return mState; }
  /** @return la valeur courante. */
  bool value();

  virtual void moveInsideParent(int x, int y);
  virtual void setEditPositions( bool val );

signals: // Signals
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();
  /** Signale la base de données de la donnée de forçage comme une base de donnée de forçage. */
  void forcingDB(CYDB *db);
  /** Emis à chaque déplacement à la souris afin de définir un ordre dans la pile de widget du parent. */
  void movedInsideParent();
  /** Signal emis après un appui suivi d'un relachement d'un appui bouton gauche de la souris et si la propriété ckickable() est activée. */
  void clicked();

public slots: // Public slots
  /** Saisie l'état de la commande. */
  virtual void setState(bool);
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(bool val);

  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  virtual void refreshPosition();
  virtual void updatePosition();

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

  void initSettingValue();
  /** Met à jour la representation de l'objet en fonction de la valeur de paramétrage. */
  void refreshSetting() {}
  void setEnabledTmp(bool val);

protected: // Protected methods
  /** Fonction appelée à l'affichage du widget. */
  virtual void showEvent(QShowEvent *e);
  /** Initialise la palette de couleurs. */
  virtual void initPalette();
  /** Gestion de la molette. */
  virtual void wheelEvent(QWheelEvent * e);
  virtual void mousePressEvent ( QMouseEvent * e );
  virtual void mouseReleaseEvent ( QMouseEvent * e );
  virtual void mouseMoveEvent ( QMouseEvent * e );
  /** Initialise le widget en mode écriture ou pas suivant \a mSettingMode. */
  virtual void initSettingMode();

protected:
  Qt::Orientation mOrientation;
  /** Angle de rotation du contenu (voulu). */
  int mRotate;
  /** Angle de rotation du contenu courrant. */
  int mRotateCurrent;
  /** Flag d'activation du mirroir. */
  bool mMirror;
  /** Vaut \a true si c'est une nouvelle image. */
  bool newPixmap;
  /** Marges par défaut. */
  int mDefMargin;

  /** Etat de la commande. */
  bool mState;

  /** Valeur d'écriture en mode écriture. */
  bool mSettingValue;
  bool mInitSetting;

private:
  void init(bool value=false);
  /** Fait une rotation du contenu en fonction de l'orientation et
    * de l'activation ou non du mirroir. */
  virtual void rotateOrientationMirror();
};

#endif
