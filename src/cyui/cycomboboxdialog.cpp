#include "cycomboboxdialog.h"
#include "ui_cycomboboxdialog.h"

CYComboBoxDialog::CYComboBoxDialog(QWidget *parent, const QString &name)
  : CYDialog(parent, name), ui(new Ui::CYComboBoxDialog)
{
  ui->setupUi(this);
}


CYComboBoxDialog::~CYComboBoxDialog()
{
  delete ui;
}

void CYComboBoxDialog::setDataName(const QByteArray &name)
{
  ui->comboBox->setDataName(name);
  ui->textLabel->setDataName(name);
}

void CYComboBoxDialog::hideButtonApply(bool hide)
{
  if (hide)
    ui->applyButton->hide();
  else
    ui->applyButton->show();
}
