//
// C++ Implementation: cymovingpixmap
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

//QT
#include <QStyle>
#include <QPainter>
#include <QPicture>
#include <QMovie>
// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyword.h"
#include "cyflag.h"
#include "cymovingpixmap.h"

CYMovingPixmap::CYMovingPixmap(QWidget *parent, const QString &name)
  : CYLabel(parent, name)
{
  mValX = 0.0;
  mValY = 0.0;
  mPainter = new QPainter();

  // TOCHECK QT5
  //  setAlignment( Qt::AlignHCenter | Qt::AlignVCenter | Qt::TextExpandTabs );
  setAlignment( Qt::AlignHCenter | Qt::AlignVCenter );
}


CYMovingPixmap::~CYMovingPixmap()
{
}

void CYMovingPixmap::setDataXName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYMovingPixmap::dataXName() const
{
  return CYDataWdg::dataName();
}

void CYMovingPixmap::setDataYName(const QByteArray &name)
{
  CYDataWdg::setDataSupName(2,name);
}

QByteArray CYMovingPixmap::dataYName() const
{
  return CYDataWdg::dataSupName(2);
}


void CYMovingPixmap::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYMovingPixmap::setData2(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SIGNAL( formatUpdated() ) );
  }
  CYDataWdg::setDataSup(2, data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SIGNAL( formatUpdated() ) );
  }
}


void CYMovingPixmap::linkData()
{
  ctrlHideFlag();

  if (!hasData() || !mData)
    return;

  if (!mDataSup[2])
  {
    this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));
  }
  else
  {
    this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()
                             +"<hr>"+
                             mDataSup[2]->displayHelp()+"<br>"+mDataSup[2]->infoCY()));
  }

  switch (mData->type())
  {
  case Cy::Bool   :
  {
    CYBool *data = (CYBool *)mData;
    setValX(data->val());
    break;
  }
  case Cy::Word   :
  {
    CYWord *data = (CYWord *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VFL    :
  {
    CYFlag *data = (CYFlag *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VS8    :
  {
    CYS8 *data = (CYS8 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VS16   :
  {
    CYS16 *data = (CYS16 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VS32   :
  {
    CYS32 *data = (CYS32 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VS64   :
  {
    CYS64 *data = (CYS64 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VU8    :
  {
    CYU8  *data = (CYU8  *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VU16   :
  {
    CYU16 *data = (CYU16 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VU32   :
  {
    CYU32 *data = (CYU32 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VU64   :
  {
    CYU64 *data = (CYU64 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VF32   :
  {
    CYF32 *data = (CYF32 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VF64   :
  {
    CYF64 *data = (CYF64 *)mData;
    setValX(data->val());
    break;
  }
  default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYMovingPixmap::linkData2()
{
  if (!hasDataSup(2) || !mDataSup[2])
    return;

  if (!mData)
  {
    this->setToolTip(QString(mDataSup[2]->displayHelp()+"<br>"+mDataSup[2]->infoCY()));
  }
  else
  {
    this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()
                             +"<hr>"+
                             mDataSup[2]->displayHelp()+"<br>"+mDataSup[2]->infoCY()));
  }

  switch (mDataSup[2]->type())
  {
  case Cy::Bool   :
  {
    CYBool *data = (CYBool *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::Word   :
  {
    CYWord *data = (CYWord *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VFL    :
  {
    CYFlag *data = (CYFlag *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VS8    :
  {
    CYS8 *data = (CYS8 *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VS16   :
  {
    CYS16 *data = (CYS16 *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VS32   :
  {
    CYS32 *data = (CYS32 *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VS64   :
  {
    CYS64 *data = (CYS64 *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VU8    :
  {
    CYU8  *data = (CYU8  *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VU16   :
  {
    CYU16 *data = (CYU16 *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VU32   :
  {
    CYU32 *data = (CYU32 *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VU64   :
  {
    CYU64 *data = (CYU64 *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VF32   :
  {
    CYF32 *data = (CYF32 *)mDataSup[2];
    setValY(data->val());
    break;
  }
  case Cy::VF64   :
  {
    CYF64 *data = (CYF64 *)mDataSup[2];
    setValY(data->val());
    break;
  }
  default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mDataSup[2]->objectName());
  }
}


void CYMovingPixmap::refresh()
{
  ctrlHideFlag();

  refresh2();

  if (!hasData())
    return;

  if (mData==0)
    return;
  if (!isVisible())
    return;
  if (!ctrlFlag())
  {
    setEnabled(false);
    return;
  }
  if (mData==0)
    return;
  if (!core || !core->simulation())
  {
    if (!mData->isOk())
    {
      setEnabled(false);
      return;
    }

    if (mData->flag()!=0)
    {
      if (!mData->flag()->val())
      {
        setEnabled(false);
        return;
      }
    }
  }

  if ( !isEnabled() )
    setEnabled(true);

  switch (mData->type())
  {
  case Cy::Bool   :
  {
    CYBool *data = (CYBool *)mData;
    setValX(data->val());
    break;
  }
  case Cy::Word   :
  {
    CYWord *data = (CYWord *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VFL    :
  {
    CYFlag *data = (CYFlag *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VS8    :
  {
    CYS8 *data = (CYS8 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VS16   :
  {
    CYS16 *data = (CYS16 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VS32   :
  {
    CYS32 *data = (CYS32 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VS64   :
  {
    CYS64 *data = (CYS64 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VU8    :
  {
    CYU8  *data = (CYU8  *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VU16   :
  {
    CYU16 *data = (CYU16 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VU32   :
  {
    CYU32 *data = (CYU32 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VU64   :
  {
    CYU64 *data = (CYU64 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VF32   :
  {
    CYF32 *data = (CYF32 *)mData;
    setValX(data->val());
    break;
  }
  case Cy::VF64   :
  {
    CYF64 *data = (CYF64 *)mData;
    setValX(data->val());
    break;
  }
  default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
    break;
  }

}

void CYMovingPixmap::refresh2()
{
  if (!hasDataSup(2))
    return;

  if (mDataSup[2]==0)
    return;
  if (!isVisible())
    return;

  if (!ctrlFlag())
  {
    setEnabled(false);
    return;
  }
  if (mDataSup[2]==0)
    return;
  if (!core || !core->simulation())
  {
    if (!mDataSup[2]->isOk())
    {
      setEnabled(false);
      return;
    }
    if (mDataSup[2]->flag()!=0)
    {
      if (!mDataSup[2]->flag()->val())
      {
        setEnabled(false);
        return;
      }
    }
  }

  if ( !isEnabled() )
    setEnabled(true);

  switch (mDataSup[2]->type())
  {
  case Cy::Bool   :
  {
    CYBool *data = (CYBool *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::Word   :
  {
    CYWord *data = (CYWord *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VFL    :
  {
    CYFlag *data = (CYFlag *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VS8    :
  {
    CYS8 *data = (CYS8 *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VS16   :
  {
    CYS16 *data = (CYS16 *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VS32   :
  {
    CYS32 *data = (CYS32 *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VS64   :
  {
    CYS64 *data = (CYS64 *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VU8    :
  {
    CYU8  *data = (CYU8  *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VU16   :
  {
    CYU16 *data = (CYU16 *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VU32   :
  {
    CYU32 *data = (CYU32 *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VU64   :
  {
    CYU64 *data = (CYU64 *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VF32   :
  {
    CYF32 *data = (CYF32 *)mDataSup[2];
    setValY(data->val());
    return;
  }
  case Cy::VF64   :
  {
    CYF64 *data = (CYF64 *)mDataSup[2];
    setValY(data->val());
    return;
  }
  default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mDataSup[2]->objectName());
    break;
  }
}


/*! @return la valeur en % de la position horizontale de l'image de la gauche du rectangle.
    \fn CYMovingPixmap::valX() const
 */
double CYMovingPixmap::valX() const
{
  return mValX;
}


/*! Saisie la valeur en % de la position horizontale de l'image de la gauche du rectangle.
    \fn CYMovingPixmap::setValX(const double val)
 */
void CYMovingPixmap::setValX(const double val)
{
  if ( mValX != val )
  {
    mValX = val;
    drawContents();
  }
}


/*! @return la valeur en % de la position verticale de l'image du haut du rectangle.
    \fn CYMovingPixmap::valY() const
 */
double CYMovingPixmap::valY() const
{
  return mValY;
}


/*! Saisie la valeur en % de la position verticale de l'image du haut du rectangle.
    \fn CYMovingPixmap::setValY(const double val)
 */
void CYMovingPixmap::setValY(const double val)
{
  if ( mValY != val )
  {
    mValY = val;
    drawContents();
  }
}


/*!
    Draws the label contents.
*/

void CYMovingPixmap::drawContents()
{
  delete mPainter;
  mPainter = 0;
  repaint();
}

void CYMovingPixmap::paintEvent(QPaintEvent *event)
{
  if (mPainter)
  {
    CYLabel::paintEvent(event);
  }
  else
  {
    mPainter = new QPainter();
    mPainter->begin(this);
    QRect cr = contentsRect();

    const QPixmap pix = pixmap(Qt::ReturnByValue);
#ifndef QT_NO_PICTURE
    const QPicture pic = picture(Qt::ReturnByValue);
#else
    const QPicture pic;
#endif
#ifndef QT_NO_MOVIE
    QMovie *mov = movie();
#else
    const int mov = 0;
#endif

    if ( !mov && pix.isNull() && pic.isNull() )
    {
      int m = indent();

      if ( m < 0 && frameWidth() ) // no indent, but we do have a frame
        m = fontMetrics().horizontalAdvance('x') / 2 - margin();

      if ( m > 0 )
      {
        int hAlign = Qt::Alignment();// Qt3 QApplication::horizontalAlignment( alignment() );
        if ( hAlign & Qt::AlignLeft )
          cr.setLeft( cr.left() + m );
        if ( hAlign & Qt::AlignRight )
          cr.setRight( cr.right() - m );
        if ( alignment() & Qt::AlignTop )
          cr.setTop( cr.top() + m );
        if ( alignment() & Qt::AlignBottom )
          cr.setBottom( cr.bottom() - m );
      }
    }

#ifndef QT_NO_MOVIE
    if ( mov )
    {
      // ### should add movie to qDrawItem
      QFontMetrics fm(font());
      QRect r = style()->itemTextRect(fm, cr, alignment(), isEnabled(), QString() );

      // ### could resize movie frame at this point
      mPainter->drawPixmap(r.x(), r.y(), mov->currentPixmap() );
    }
    else
#endif
#ifndef QT_NO_PICTURE
      if ( !pic.isNull() )
      {
        QRect br = pic.boundingRect();
        int rw = br.width();
        int rh = br.height();
        if ( hasScaledContents() )
        {
          mPainter->save();
          mPainter->translate( cr.x(), cr.y() );
#ifndef QT_NO_TRANSFORMATIONS
          mPainter->scale( (double)cr.width()/rw, (double)cr.height()/rh );
#endif
          mPainter->drawPicture( -br.x(), -br.y(), pic );
          mPainter->restore();
        }
        else
        {
          int xo = 0;
          int yo = 0;
          if ( alignment() & Qt::AlignVCenter )
            yo = (cr.height()-rh)/2;
          else if ( alignment() & Qt::AlignBottom )
            yo = cr.height()-rh;
          if ( alignment() & Qt::AlignRight )
            xo = cr.width()-rw;
          else if ( alignment() & Qt::AlignHCenter )
            xo = (cr.width()-rw)/2;
          mPainter->drawPicture( cr.x()+xo-br.x(), cr.y()+yo-br.y(), pic );
        }
      }
      else if ( !pix.isNull() )
#endif
      {
        // ordinary text or pixmap label
        if ( alignment() & Qt::AlignHCenter )
          cr.moveLeft( (int) (cr.left() + ( mValX * ( cr.width () - pix.width () ) / 200) ) );
        else if ( alignment() & Qt::AlignLeft )
          cr.moveLeft( (int) (cr.left() + ( mValX * ( cr.width () - pix.width () ) / 100) ) );
        else if ( alignment() & Qt::AlignRight )
          cr.moveRight( (int) (cr.right() - ( mValX * ( cr.width () - pix.width () ) / 100) ) );

        if ( alignment() & Qt::AlignVCenter )
          cr.moveTop( (int) (cr.top() + ( mValY * ( cr.height () - pix.height () ) / 200) ) );
        else if ( alignment() & Qt::AlignTop )
          cr.moveTop( (int) (cr.top() + ( mValY * ( cr.height () - pix.height () ) / 100) ) );
        else if ( alignment() & Qt::AlignBottom )
          cr.moveBottom( (int) (cr.bottom() - ( mValY * ( cr.height () - pix.height () ) / 100) ) );

        if (!pix.isNull())
        {
          style()->drawItemPixmap( mPainter, cr, alignment(), pix );
        }
        else
          style()->drawItemText( mPainter, cr, alignment(), palette(), isEnabled(), text() );
      }
      mPainter->end();
  }
}
