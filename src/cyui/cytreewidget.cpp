#include "cytreewidget.h"

CYTreeWidget::CYTreeWidget(QWidget *parent, const QString &name)
  : QTreeWidget(parent)
{
  setObjectName(name);
}


CYTreeWidget::~CYTreeWidget()
{
}

void CYTreeWidget::setSelected( QTreeWidgetItem *item, bool selected )
{
  if (item)
    item->setSelected( selected );
}

QTreeWidgetItem *CYTreeWidget::findItem(const QString & text, int column) const
{
  return findItem(text, Qt::MatchRecursive, column);
}

QTreeWidgetItem *CYTreeWidget::findItem(const QString & text, Qt::MatchFlags flags, int column) const
{
  QList<QTreeWidgetItem *> list=findItems(text, flags, column);
  if (!list.isEmpty())
  {
    return list.first();
  }
  return 0;
}

QTreeWidgetItem *CYTreeWidget::nextSibling( QTreeWidgetItem *item )
{
  QTreeWidgetItem *current = item;
  QTreeWidgetItem *parent = current->parent();
  QTreeWidgetItem *nextSibling=0;
  if(parent)
    nextSibling =parent->child(parent->indexOfChild(current)+1);
  else
  {
    QTreeWidget *treeWidget = current->treeWidget();
    nextSibling = treeWidget->topLevelItem(treeWidget->indexOfTopLevelItem(current)+1);
  }
  return nextSibling;
}

bool CYTreeWidget::dropMimeData(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action)
{
  if (QTreeWidget::dropMimeData(parent, index, data, action))
    emit modifie();
  return false; //v5.18
}
