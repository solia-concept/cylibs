/***************************************************************************
                          cybuttongroup.cpp  -  description
                             -------------------
    begin                : jeu nov 13 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cybuttongroup.h"

// QT
#include <QLayout>
#include <QString>
#include <QShowEvent>
#include <QMouseEvent>
// CYLIBS
#include "cydb.h"
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cystring.h"
#include "cycolor.h"
#include "cybool.h"
#include "cyword.h"
#include "cyreadonlyfilter.h"

CYButtonGroup::CYButtonGroup(QWidget *parent, const QString &name )
  : QGroupBox("Title", parent),
    CYDataWdg(this)
{
  setObjectName(name);
  init();
}

CYButtonGroup::CYButtonGroup(const QString &title, QWidget *parent, const QString &name)
  : QGroupBox(title, parent),
    CYDataWdg(this)
{
  setObjectName(name);
  init();
}

CYButtonGroup::~CYButtonGroup()
{
}

void CYButtonGroup::init()
{
  mAutoRefresh = false;
  mShowLabel = true;
  mShowValue = true;
  mShowGroup = No;
  mInputHelp = true;
  mShowToolTip = false;
  mPrefix = "";
  mTreatChildDataWdg = true;
  mProtectedAccess = false;

  setTitle(" ");
  initPalette();
  connect( this, SIGNAL( toggled( bool ) ), this, SLOT( stateChanged( bool ) ) );
  connect(core, SIGNAL(accessChanged()), SLOT(linkData()));
}

bool CYButtonGroup::movable() const
{
  return mMovable;
}

void CYButtonGroup::setMovable(const bool val)
{
  mMovable = val;
  setEditPositions( mEditPositions );
}

bool CYButtonGroup::alwaysOk() const
{
  return mAlwaysOk;
}

void CYButtonGroup::setAlwaysOk(const bool val)
{
  mAlwaysOk = val;
}

void CYButtonGroup::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYButtonGroup::dataName() const
{
  return CYDataWdg::dataName();
}

void CYButtonGroup::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYButtonGroup::flagName() const
{
  return CYDataWdg::flagName();
}

void CYButtonGroup::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYButtonGroup::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYButtonGroup::setForcingName(const QByteArray &name)
{
  CYDataWdg::setForcingName(name);
}

QByteArray CYButtonGroup::forcingName() const
{
  return CYDataWdg::forcingName();
}

void CYButtonGroup::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYButtonGroup::benchType() const
{
  return CYDataWdg::benchType();
}

int CYButtonGroup::genericMarkOffset() const
{
  return mGenericMarkOffset;
}

void CYButtonGroup::setGenericMarkOffset(const int val)
{
  mGenericMarkOffset = val;
}

bool CYButtonGroup::inverseFlag() const
{
  return CYDataWdg::inverseFlag();
}

void CYButtonGroup::setInverseFlag(bool inverse)
{
  CYDataWdg::setInverseFlag(inverse);
}

bool CYButtonGroup::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

void CYButtonGroup::setInverseHideFlag(bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

void CYButtonGroup::setPosXName(const QByteArray &name)
{
  CYDataWdg::setPosXName( name );
}

QByteArray CYButtonGroup::posXName() const
{
  return CYDataWdg::posXName();
}

void CYButtonGroup::setPosYName(const QByteArray &name)
{
  CYDataWdg::setPosYName( name );
}

QByteArray CYButtonGroup::posYName() const
{
  return CYDataWdg::posYName();
}

bool CYButtonGroup::ctrlFlag()
{
  if (!((QWidget *)parent())->isEnabled() && !core)
    return true;

  bool force=false;
  bool enable1=true;
  if (protectedAccess())
  {
    force=true;
    enable1 = (core && core->isProtectedAccess()) ? false : true;
  }

  bool enable2=true;
  if (mFlag)
  {
    force=true;
    if (core && core->simulation())
      enable2=true;
    else
    {
      if (mFlag->val() && mInverseFlag)
        enable2 = false;
      else if (!mFlag->val() && !mInverseFlag)
        enable2 = false;
    }
  }

  if (!force)
    return isEnabled();

  bool res = (enable1 && enable2) ? true : false;
  setEnabled(res);
  return res;
}

void CYButtonGroup::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYButtonGroup::setReadOnly(bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }


  ctrlFlag();
}

void CYButtonGroup::changeEvent(QEvent* event)
{
  if (event && (event->type()==QEvent::EnabledChange))
  {
     // cet événement est envoyé, si l'état actif change
     mEnabled = isEnabled();
  }
  QGroupBox::changeEvent(event);
}

void CYButtonGroup::initPalette()
{
  mPalette = palette();
}

bool CYButtonGroup::value() const
{
  if (isCheckable())
  {
    if (isChecked())
      return true;
    else
      return false;
  }
  else
    return false;
}

void CYButtonGroup::setValue(bool value)
{
  QGroupBox::setChecked(value);
}

void CYButtonGroup::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYButtonGroup::linkData()
{
  ctrlHideFlag();

  if (!ctrlFlag())
    return;

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  switch (mData->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
//                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
//                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->val());
                      break;
                    }
    case Cy::Color :
                    {
                      CYColor *data = (CYColor *)mData;
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
  setTitle(mPrefix+title());

  if (mInputHelp)
  {
    if (mShowToolTip)
      this->setToolTip(mData->inputHelp()+"<br>"+mData->infoCY());
  }
  else
  {
    if (mShowToolTip)
      this->setToolTip(mData->displayHelp()+"<br>"+mData->infoCY());
  }
}

void CYButtonGroup::refresh()
{
  ctrlHideFlag();
  refreshPosition();

  if (!ctrlFlag())
    return;

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (!mData->isOk() && !mAlwaysOk)
  {
    setEnabled(false);
    return;
  }
  if (mData->flag()!=0)
  {
    if (!mData->flag()->val() && !mAlwaysOk)
    {
      setEnabled(false);
      return;
    }
  }

  switch (mData->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
//                      setValue((bool)data->val());
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->val());
                      break;
                    }
    case Cy::Color :
                    {
                      CYColor *data = (CYColor *)mData;
                      if (mShowLabel)
                        setTitle(data->label());
                      else if (mShowGroup)
                        setTitle(data->groupSection((Cy::GroupSection)mShowGroup));
                      else if (mShowValue)
                        setTitle(data->toString());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
  setTitle(mPrefix+title());
}


void CYButtonGroup::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::String :
                    {
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYButtonGroup::update()
{
  updatePosition();
  if (!isVisible() && mUpdateIfVisible)
    return;

  if (!isCheckable())
    return;

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (!isEnabled())
    return;

  if (isHidden())
    return;

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      data->setVal(QString("%1").arg(value()));
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}


/*! Rafraîchit la position de l'objet en fonction des données positions.
    \fn CYDataWdg::refreshPosition()
 */
void CYButtonGroup::refreshPosition()
{
  if ( !movable() )
    return;

  if ( ( mPosX && mPosY ) && ( ( mPosX->val()!=x() ) || ( mPosY->val()!=y() ) ) )
  {
    if ( (mPosX->val()>=0) && (mPosY->val()>=0) )
      move( mPosX->val(), mPosY->val() );
    else if ( mPosX->val()>=0 )
      move( mPosX->val(), y()          );
    else if ( mPosY->val()>=0 )
      move( x()         , mPosY->val() );
  }
  else if ( ( mPosX ) && ( mPosX->val()!=x() ) && ( mPosX->val()>=0 ) )
  {
    move( mPosX->val(), y()          );
  }
  else if ( ( mPosY ) && ( mPosY->val()!=y() ) && ( mPosY->val()>=0 ) )
    move( x()         , mPosY->val() );
}


/*! Met à jour des données positions en fonction de la position de l'objet.
    \fn CYDataWdg::updatePosition()
 */
void CYButtonGroup::updatePosition()
{
  if ( !mEditPositions )
    return;

  if ( mPosX )
    mPosX->setVal( x() );

  if ( mPosY )
    mPosY->setVal( y() );
}


/*! Saisie du titre
    \fn CYButtonGroup::setTitle ( const QString & )
 */
void CYButtonGroup::setTitle ( const QString & title)
{
  QGroupBox::setTitle(title+"   ");
}



/*!
    \fn CYButtonGroup::showEvent(QShowEvent *e)
 */
void CYButtonGroup::showEvent(QShowEvent *e)
{
  updateGeometry();
  QGroupBox::showEvent(e);
  refresh();
}


/*! Traîtement réalisé à chaque changement d'état de la case à cocher.
    \fn CYButtonGroup::stateChanged( bool checked )
 */
void CYButtonGroup::stateChanged( bool checked )
{
  Q_UNUSED(checked)
  if (mData)
  {
    int val=value();
    mData->setTmp(val);
  }
  emit modifie();
}

/*! Gestion d'appui de bouton de souris
    \fn CYButtonGroup::mousePressEvent ( QMouseEvent * e )
 */
void CYButtonGroup::mousePressEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions )
  {
    mXOldPos = x();
    mYOldPos = y();
    mXMousePos = e->x();
    mYMousePos = e->y();
    emit movedInsideParent();
  }
  QGroupBox::mousePressEvent ( e );
}


/*! Gestion de déplacement de bouton de souris
    \fn CYButtonGroup::mouseMoveEvent ( QMouseEvent * e )
 */
void CYButtonGroup::mouseMoveEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions && (e->buttons()==Qt::LeftButton) )
  {
    int ex = e->x();
    int ey = e->y();
    int x = mXOldPos + ( ex - mXMousePos );
    int y = mYOldPos + ( ey - mYMousePos );
    moveInsideParent( x, y );
    raise();
  }
  QGroupBox::mouseMoveEvent( e );
}


/*! Déplacement à l'intérieur du parent.
    \fn CYButtonGroup::moveInsideParent(int x, int y)
 */
void CYButtonGroup::moveInsideParent(int x, int y)
{
  QWidget * p = (QWidget *)parent();
  if ( !p->rect().contains(x, p->rect().y()) )
  {
    if ( x < p->rect().left() )
      x = p->rect().left();            // sort à gauche
    else
      x = p->rect().right()-width();   // sort à droite
  }
  else if ( !p->rect().contains(x+width(), p->rect().y()) )
  {
    x = p->rect().right()-width();     // sort en partie à droite
  }

  if ( !p->rect().contains(p->rect().x(), y) )
  {
    if ( y < p->rect().top() )
      y = p->rect().top();             // sort en haut
    else
      y = p->rect().bottom()-height(); // sort en bas
  }
  else if ( !p->rect().contains(p->rect().x(), y+height()) )
  {
    y = p->rect().bottom()-height();   // sort en partie en bas
  }

  move(x, y);
  mXOldPos = x;
  mYOldPos = y;

  emit modifie();
}


/*! Active/désactive le déplacement
    \fn CYButtonGroup::setEditPositions( bool val )
 */
void CYButtonGroup::setEditPositions( bool val )
{
  if ( !movable() )
    return;

  mEditPositions = val;
  refreshPosition();

  if ( mEditPositions )
    setCursor( Qt::PointingHandCursor );
  else
    unsetCursor();
}
