//
// C++ Interface: cymovingpixmap
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMOVINGPIXMAP_H
#define CYMOVINGPIXMAP_H

// CYLIBS
#include "cylabel.h"

/**
@short Image mouvante.
Cet objet graphique permet de positionner une image dans un rectangle en fonction 2 valeurs de données entre 0 et 100%. Il est par exemple possible de représenter un niveau ou une position analogique en pourcentage par la position verticale ou horisontal de l'image dans le rectangle.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMovingPixmap : public CYLabel
{
  Q_OBJECT
  Q_PROPERTY(QByteArray dataXName READ dataXName WRITE setDataXName)
  Q_PROPERTY(QByteArray dataYName READ dataYName WRITE setDataYName)
  Q_PROPERTY(double valX READ valX WRITE setValX)
  Q_PROPERTY(double valY READ valY WRITE setValY)

public:
    CYMovingPixmap(QWidget *parent=0, const QString &name="movingPixmap");

    ~CYMovingPixmap();

  /** @return le nom de la donnée traîtée pour la position horizontale en pourcentage. Celui-ci correspond au positionnement par rapport à l'espace disponible en X dans le rectangle. Pour 0% l'image est à gauche du rectangle et pour 100% l'image est décalée à droite du rectangle. */
  virtual QByteArray dataXName() const;
  /** Saisie le nom de la donnée traîtée pour la position horizontale en pourcentage. */
  virtual void setDataXName(const QByteArray &name);

  /** @return le nom de la donnée traîtée pour la position verticale en pourcentage. Celui-ci correspond au positionnement par rapport à l'espace disponible en Y dans le rectangle. Pour 0% l'image est en haut du rectangle et pour 100% l'image est décalée en bas du rectangle. */
  virtual QByteArray dataYName() const;
  /** Saisie le nom de la donnée traîtée pour la position verticale en pourcentage. */
  virtual void setDataYName(const QByteArray &name);

  /** @return le nom du fichier de l'image. */
  QString pixmapFile() const { return mPixmapFile; }

    virtual double valX() const;
    virtual double valY() const;

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

  /** Place @p data comme la donnée supplémentaire du widget. */
  virtual void setData2(CYData *data);


public slots:
    virtual void setValX(const double val);
    virtual void setValY(const double val);

  /** Charge la valeur de la donnée représentant la position X. */
  virtual void refresh();
  /** Charge la valeur de la donnée représentant la position Y. */
  virtual void refresh2();

  /** Configure le widget en fonction de la donnée représentant la position X. */
  virtual void linkData();
  /** Configure le widget en fonction de la donnée représentant la position Y. */
  virtual void linkData2();

protected: // Protected methods
   virtual void drawContents();
   virtual void paintEvent(QPaintEvent * event);

protected: // Protected attributes
    /** Nom du fichier de l'image. */
    QString mPixmapFile;
    double mValX;
    double mValY;
    QPainter *mPainter;
};

#endif
