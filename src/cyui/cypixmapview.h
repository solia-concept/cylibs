/***************************************************************************
                          cypixmapview.h  -  description
                             -------------------
    begin                : jeu oct 9 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYPIXMAPVIEW_H
#define CYPIXMAPVIEW_H

// CYLIBS
#include "cylabel.h"

/** @short Visualisateur d'image.
  * @author Gérald LE CLEACH
  */

class CYPixmapView : public CYLabel
{
  Q_OBJECT
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QByteArray pixmapFileTrue READ pixmapFileTrue WRITE setPixmapFileTrue)
  Q_PROPERTY(QByteArray pixmapFileFalse READ pixmapFileFalse WRITE setPixmapFileFalse)
  Q_PROPERTY(bool state READ state WRITE setState )

public:
  CYPixmapView(QWidget *parent=0, const QString &name=0);
  ~CYPixmapView();

public: // Public methods
  /** @return la valeur courante. */
  bool value();

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

  /** @return le nom du fichier de l'image de l'état vrai. */
  QByteArray pixmapFileTrue() const { return mPixmapFileTrue.toUtf8(); }
  /** @return le nom du fichier de l'image de l'état faux. */
  QByteArray pixmapFileFalse() const { return mPixmapFileFalse.toUtf8(); }

  /** Recherche, dans la base de données \a db, la donnée de forçage ayant
    * pour nom celui saisie par la fonction setForcingName() et la place
    * comme donnée de forçage du widget. */
  virtual bool findForcing(QHash<QString, CYData*> *db);

signals:
  /** Emis à chaque changement de valeur de forçage à partir du synoptic. */
  void synopticForcingChanged(int);
  /** Emis à détection d'évènement demandant le changement de valeur de forçage. */
  void toggleForcingEvent();
  /** Emis à détection d'évènement demandant le changement de valeur de paramétrage. */
  void toggleSettingEvent();

public slots: // Public slots
  /** Place la valeur du contrôleur. */
  void setValue(bool);
  /** Saisie l'état. */
  virtual void setState(bool);
  /** Saisie le nom du fichier de l'image de l'état vrai. */
  virtual void setPixmapFileTrue(const QByteArray &file);
  /** Saisie le nom du fichier de l'image de l'état faux. */
  virtual void setPixmapFileFalse(const QByteArray &file);

  /** Met le widget en mode forçage ou pas suivant \a val. */
  virtual void setForcingMode(const bool val);

  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Met à jour la valeur de la donnée de forçage traîtée avec la valeur de forçage. */
  virtual void update();
  /** Charge la valeur de la deuxième donnée. */
  virtual void refresh2() {}

  /** Bascule/rebascule la valeur de la donnée de forçage traîtée avec la valeur saisie. */
  virtual void toggleForcing();
  /** Met à jour la representation de l'objet en fonction de la valeur de forçage. */
  void refreshForcing();

  /** Bascule/rebascule la valeur de la donnée de paramétrage traîtée avec la valeur saisie. */
  virtual void toggleSetting();
  /** Met à jour la representation de l'objet en fonction de la valeur de paramétrage. */
  void refreshSetting();

  void tableForcingChanged(int);

  void initForceValue();

  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();

protected: // Protected methods
  /** Gestion du clavier. */
  virtual void keyPressEvent(QKeyEvent *e);
  /** Gestion du clic de souris. */
  virtual void mousePressEvent(QMouseEvent * e);
  /** Initialise le widget en mode forçage ou pas suivant \a mForcingMode. */
  virtual void initForcingMode();
  /** Gestion de la molette. */
  virtual void wheelEvent(QWheelEvent * e);

private:
  void init(bool value=false);

protected: // Protected attributes
  /** Valeur courante. */
  bool mValue;
  /** Nom du fichier de l'image de l'état vrai. */
  QString mPixmapFileTrue;
  /** Nom du fichier de l'image de l'état faux. */
  QString mPixmapFileFalse;
  /** Valeur de forçage en mode forçage. */
  bool mForcingValue;
  bool mInit;
  bool mInitForcing;
};

#endif
