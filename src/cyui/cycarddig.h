/***************************************************************************
                          cycarddig.h  -  description
                             -------------------
    début                  : jeu fév 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCARDDIG_H
#define CYCARDDIG_H

// CYLIBS
#include "cydig.h"
#include "cycardio.h"

class CYBoxDIG;

/** CYCardDIG est un widget représentant des cartes
  * électriques d'E/S TOR.
  * @see CYCardDI, CYCardDO
  * @short Widget d'une carte TOR.
  * @author Gérald LE CLEACH
  */

class CYCardDIG : public CYCardIO
{
  Q_OBJECT

public:
  /** Constructeur */
  CYCardDIG(QWidget *parent=0, const QString &name=0, int no=0, Mode m=View);
  /** Destructeur */
  ~CYCardDIG();

  virtual QSize sizeHint() const;
  virtual QSize minimumSizeHint() const;

public slots: // Public slots
  /** Simulation. */
  virtual void simulation() {} 

protected:
  /** Initialise les widgets selon les paramètres de la carte. */
  virtual void init() {}
  /** Met à jour les widgets. */
  virtual void updateCard() {}
};

#endif
