/***************************************************************************
                          cytextedit.h  -  description
                             -------------------
    begin                : ven déc 5 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYTEXTEDIT_H
#define CYTEXTEDIT_H

// QT
#include <QTextEdit>
#include <QStyle>
#include <QHash>
#include <QMenu>
#include <QSizeGrip>
// CYLIBS
#include "cydatawdg.h"
#include "cymessagebox.h"


class CYPopupMenu;

/** @short Boîte d'édition de texte amélioré.
  * Cette boîte permet d'éditer des données de type CYString avec la gestion
  * de paragraphes, couleurs...
  * @author Gérald LE CLEACH
  */

class CYTextEdit : public QTextEdit, public CYDataWdg
{
  Q_OBJECT
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool readOnly READ readOnly WRITE setReadOnly)
  Q_PROPERTY(bool alwaysOk READ alwaysOk WRITE setAlwaysOk)
  Q_PROPERTY(bool autoRefresh READ autoRefresh WRITE setAutoRefresh)
  Q_PROPERTY(bool enableReadOnly READ enableReadOnly WRITE setEnableReadOnly)
  Q_PROPERTY(bool showLastLine READ showLastLine WRITE setShowLastLine)
  Q_PROPERTY(int maxLines READ maxLines WRITE setMaxLines)
  Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
  Q_PROPERTY(QColor foregroundColor READ foregroundColor WRITE setForegroundColor)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:
  CYTextEdit(QWidget *parent=0, const QString &name=0);
  ~CYTextEdit();

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);
  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return \a true si le widget est en lecture seule. */
  bool readOnly() const;
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);

  /** @return \a true la donnée traîtée est considérée comme toujours valide. */
  virtual bool alwaysOk() const;
  /** La donnée traîtée est considérée comme toujours valide si \a val vaut true. */
  virtual void setAlwaysOk(const bool val);

  /** @return \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  bool autoRefresh() const { return mAutoRefresh; }
  /** Saisir \a true pour que le widget soit rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  void setAutoRefresh(const bool val) { mAutoRefresh = val; }

  /** @return \a true si le widget est en mode saisie. */
  bool enableReadOnly() const { return mEnableReadOnly; }
  /** Saisir \a true pour que le widget soit en mode saisie. */
  void setEnableReadOnly(const bool val) { mEnableReadOnly = val; }

  /** @return \a true si la vue doit afficher les nouvelles lignes de manière automatique. */
  bool showLastLine() const { return mShowLastLine; }
  /** Saisir \a true pour que la vue affiche les nouvelles lignes de manière automatique. */
  void setShowLastLine(const bool val) { mShowLastLine = val; }

  /** @return le nombre maxi de lignes. Par défaut vaut \a 0 pour un nombre de lignes illimité. */
  int maxLines() const { return mMaxLines; }
  /** Saisir le nombre maxi de lignes ou \a 0 pour ne pas mettre de limites. */
  void setMaxLines(const int val) { mMaxLines = val; }

  /** @return la valeur courrante. */
  QString value() const;
  /** Fixe la valeur courante à \a value. */
  void setValue(QString value);

  /** Saisie la couleur de fond. */
  virtual void setBackgroundColor(const QColor &color);
  /** @return la couleur de fond du widget */
  virtual const QColor & backgroundColor () const { return mBackgroundColor; }
  /** Saisie la couleur du premier plan. */
  virtual void setForegroundColor(const QColor &color);
  /** @return la couleur de devant du widget */
  virtual const QColor & foregroundColor () const { return mForegroundColor; }
  /** Saisie la palettes de couleurs. */

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  virtual void setPalette(const QPalette &palette);
  /** Ajoute un menu popupmenu supplémentaire.
    * @param text     Texte du menu.
    * @param id       Index de position après le ou les menus par défaut.
    * @param readOnly Affiche se menu uniquement en mode readOnly si se paramètre est vaut \a true. */
  virtual CYPopupMenu *addPopupMenu(QString text, int id, bool readOnly=true);

  /** @return le text contenu en une liste de toutes les lignes utilisé dans l'éditeur.*/
  virtual QStringList splitByLines();

signals: // Signals
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();
  /** Emis lorsque le widget reçoit le focus. */
  void focusIn();

public slots: // Public slots
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

  /** Place l'objet comme étant modifié par l'utilisateur. */
  virtual void setModified(bool m);

  /** Configure le widget en fonction de la donnée à laquelle il est lié. */
  virtual void linkData();

  /** Contrôle que le texte a un nombre de ligne inférieur au nombre de lignes maxi @see setMaxLines, sinon force au text précédemment valide.*/
  virtual void ctrlMaxLines();

  /** Inverse le mode en cours d'affichage ou non des nouvelles lignes de manière automatique. */
  void toogleShowLastLine() { setShowLastLine(!mShowLastLine); }

  /** Active le mode automatique de la barre de défilement verticale */
  void setVerticalScrollBarAuto();
  /** Active la barre de défilement verticale */
  void setVerticalScrollBarOn();
  /** Désactive la barre de défilement verticale */
  void setVerticalScrollBarOff();

  /** Active le mode automatique de la barre de défilement horizontale */
  void setHorizontalScrollBarAuto();
  /** Active la barre de défilement horizontale */
  void setHorizontalScrollBarOn();
  /** Désactive la barre de défilement horizontale */
  void setHorizontalScrollBarOff();

protected: // Protected methods
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
     Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);
  /** Gestion de la réception du focus. */
  void focusInEvent(QFocusEvent *e);
  virtual QMenu *createPopupMenu( const QPoint & pos );

  void setVPMenuItems();

private: // Private methods
  void init();

protected: // Protected attributes
  QSize   mSizeEdit;
  bool    mWasOk;

  QMenu *vp_options;
  QAction *nlar_action, *vauto_action, *vaoff_action, *vaon_action, *hauto_action, *haoff_action, *haon_action, *corn_action;

  QSizeGrip* corner;
  bool mShowLastLine;
  int mMaxLines;
  QString mValidText;

  QHash<int, CYPopupMenu*> mPopupMenus;
};

#endif
