//
// C++ Implementation: cyimage
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyimage.h"
#include "cystring.h"
#include "cyimagelist.h"
//Added by qt3to4:
#include <QString>

CYImage::CYImage(CYImageList *list, int index, QString label, QString fileName)
 : QImage(fileName),
   mIndex(index),
   mLabel(label),
   mFileName(fileName)
{
  Q_UNUSED(list)
  mList=0;
//   setList(list);

  if (!fileName.isEmpty())
    loadImage(fileName);
}


CYImage::CYImage(CYImageList *list)
 : QImage()
{
  // TODO
  Q_UNUSED(list)
  mList=0;
//   setList(list);
}

CYImage::~CYImage()
{
}


/*! Charge l'image à partir du fichier \a fileName.
    \fn CYImage::loadImage(const QString &fileName)
 */
void CYImage::loadImage(const QString &fileName)
{
  if (!load(fileName))
  {
    QString path = QString("%1/cylibs/pics/").arg(qApp->applicationDirPath()).toUtf8();
    load(path+"cyno_pic_available.png");
  }
}

bool CYImage::loadFromDOM(QDomElement &el)
{
  mIndex = el.attribute("index").toInt();
  mFileName = el.attribute("fileName");
  mLabel = el.attribute("label");
  if (!mFileName.isEmpty())
    loadImage(mFileName);
  return true;
}

bool CYImage::saveToDOM(QDomElement &el)
{
  el.setAttribute("index", QString::number(mIndex));
  el.setAttribute("fileName", mFileName);
  el.setAttribute("label", mLabel );
  return true;
}

void CYImage::setList(CYImageList *list)
{
  Q_UNUSED(list)
//   if ( mList )
//   {
//     mList->removeImage( this );
//   }
//   mList = list;
//   mList->addImage( this );
}

