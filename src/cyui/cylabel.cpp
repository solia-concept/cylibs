/***************************************************************************
                          cylabel.cpp  -  description
                             -------------------
    début                  : jeu aoû 28 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cylabel.h"

// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyword.h"
#include "cyflag.h"
#include "cydo.h"
#include "cyao.h"
#include "cyreadonlyfilter.h"

CYLabel::CYLabel(QWidget *parent, const QString &name)
  : QLabel(parent),
    CYDataWdg(this)
{
  setObjectName(name);
  mMirror        = false;
  mRotate        = 0;
  mRotateCurrent = 0;
  mSettingValue  = 0;
  mInitSetting   = false;
  mEnableReadOnly=false;
  // TODO QT5
  //  setBackgroundOrigin(QWidget::ParentOrigin);
  initPalette();
  mDefMargin = -1;
  newPixmap = false;
}

CYLabel::~CYLabel()
{
}

bool CYLabel::movable() const
{
  return mMovable;
}

void CYLabel::setMovable(const bool val)
{
  mMovable = val;
  setEditPositions( mEditPositions );
}

bool CYLabel::clickable() const
{
  return mClickable;
}

void CYLabel::setClickable(const bool val)
{
  mClickable = val;
}

bool CYLabel::alwaysOk() const
{
  return mAlwaysOk;
}

void CYLabel::setAlwaysOk(const bool val)
{
  mAlwaysOk = val;
}

void CYLabel::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYLabel::flagName() const
{
  return CYDataWdg::flagName();
}

void CYLabel::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYLabel::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYLabel::setForcingName(const QByteArray &name)
{
  CYDataWdg::setForcingName(name);
}

QByteArray CYLabel::forcingName() const
{
  return CYDataWdg::forcingName();
}

void CYLabel::setSettingName(const QByteArray &name)
{
  CYDataWdg::setDataSupName(1, name);
}

QByteArray CYLabel::settingName() const
{
  return CYDataWdg::dataSupName(1);
}

CYData *CYLabel::settingData()
{
  return CYDataWdg::dataSup(1);
}

bool CYLabel::hasSettingData()
{
  return CYDataWdg::hasDataSup(1);
}


void CYLabel::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYLabel::benchType() const
{
  return CYDataWdg::benchType();
}

int CYLabel::genericMarkOffset() const
{
  return mGenericMarkOffset;
}

void CYLabel::setGenericMarkOffset(const int val)
{
  mGenericMarkOffset = val;
}

bool CYLabel::inverseFlag() const
{
  return CYDataWdg::inverseFlag();
}

void CYLabel::setInverseFlag(bool inverse)
{
  CYDataWdg::setInverseFlag(inverse);
}

bool CYLabel::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

void CYLabel::setInverseHideFlag(bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

void CYLabel::setOrientation(const Qt::Orientation orientation)
{
  if (mOrientation != orientation)
    sizePolicy().transpose();

  mOrientation = orientation;
  rotateOrientationMirror();
}

Qt::Orientation CYLabel::orientation() const
{
  return mOrientation;
}

void CYLabel::setMirror(bool flag)
{
  mMirror = flag;
  rotateOrientationMirror();
}

bool CYLabel::mirror() const
{
  return mMirror;
}


void CYLabel::setBackgroundColor ( const QColor & color )
{
  mBackgroundColor=color;
  setAutoFillBackground(true);
  QPalette pal = palette();
  pal.setColor(backgroundRole(), mBackgroundColor);
  setPalette(pal);
  initPalette();
}

const QColor & CYLabel::backgroundColor () const
{
  return mBackgroundColor;
}

void CYLabel::setForegroundColor ( const QColor & color )
{
  mForegroundColor=color;
  QPalette pal = palette();
  pal.setColor(QPalette::WindowText, mForegroundColor);
  setPalette(pal);
  initPalette();
}

const QColor & CYLabel::foregroundColor () const
{
  return mForegroundColor;
}


void CYLabel::setBackgroundPixmap( const QPixmap & pixmap )
{
  if (pixmap.isNull())
    return;

  mBackgroundPixmap=pixmap;

//  if (pixmap())
//    clear();
//
//  setScaledContents(true);
//  setPixmap(QPixmap(QString(mPixmapFileTrue)));
//
//  if (pixmap())
//  {
//    newPixmap = true;
//    setRotate(rotate());
//  }
//  else
//    setScaledContents(false);

  QPalette palette;
  palette.setBrush(backgroundRole(), QBrush(pixmap));
  setPalette(palette);
}

const QPixmap & CYLabel::backgroundPixmap () const
{
  return mBackgroundPixmap;
}

void CYLabel::setRotate(int value)
{
//  updateRotate();
  const QPixmap pm = pixmap(Qt::ReturnByValue);
  if (!pm.isNull())
  {
//     hide();
    QTransform m;
    int angle;
    if (!newPixmap)
      angle = value-mRotate;
    else
      angle = value;
    if (!angle)
      return;
    m.rotate(-angle);
    QPixmap res = pm.transformed(m);
    setPixmap(res);
//     show();
    ctrlHideFlag();
  }
  mRotate = value;
}

int CYLabel::rotate() const
{
  return mRotate;
}

void CYLabel::rotateOrientationMirror()
{
  if (!mMirror && (mOrientation==Qt::Horizontal))
  {
    setRotate(-mRotate);
    setRotate(0);
  }
  if (!mMirror && (mOrientation==Qt::Vertical))
  {
    setRotate(-mRotate);
    setRotate(90);
  }
  if (mMirror && (mOrientation==Qt::Horizontal))
  {
    setRotate(-mRotate);
    setRotate(180);
  }
  if (mMirror && (mOrientation==Qt::Vertical))
  {
    setRotate(-mRotate);
    setRotate(270);
  }
}

void CYLabel::setState(bool value)
{
  mState = value;
  setRotate(mRotate);
}

bool CYLabel::ctrlFlag()
{
  if (!mCtrlFlag || (mFlag==0))
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;

  setEnabled(res);

  return res;
}

void CYLabel::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYLabel::setReadOnly(bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }

  ctrlFlag();
}

void CYLabel::showEvent(QShowEvent *e)
{
  QLabel::showEvent(e);
//  updateRotate();
//  initForcingMode();
}

void CYLabel::updateRotate()
{
//  if (mRotateCurrent != mRotate)
//  {
//    mRotateCurrent = mRotate;
//    QPixmap *pm = pixmap();
//    if (pm)
//    {
//      hide();
//      QWMatrix m;
//      if (newPixmap)
//        newPixmap = false;
//      else
//      {
//        int angle = 360-mRotate;
//        m.rotate(-angle);
//        setPixmap(pm->transformed(m));
//      }
//      int angle = mRotate;
//      m.rotate(-angle);
//      QPixmap res = pm->transformed(m);
//      setPixmap(res);
//      show();
//    }
//  }
}

void CYLabel::initPalette()
{
  mPalette = palette();
}

void CYLabel::initSettingMode()
{
  if (!mSettingMode)
    return;

  if (!settingData())
    return;

  if (mInitSetting)
    return;

  mSettingValue = false;
  mDefMargin = margin();
/*  setMargin(2);*/
  setAutoFillBackground(true);
  QPalette pal = palette();
  mBackgroundColor = pal.color(backgroundRole());
  pal.setColor(backgroundRole(), mBackgroundColor.darker(170));
  setPalette(pal);
  setFocusPolicy(Qt::StrongFocus);

  setFrameShape(QFrame::WinPanel);
  setFrameShadow(QFrame::Raised);

  initSettingValue();
  mInitSetting = true;
}

/*! Initialise la valeur de donnée de paramétrage.
    \fn CYLabel::initSettingValue()
 */
void CYLabel::initSettingValue()
{
  if (!settingData())
    return;

  switch (settingData()->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)settingData();
                      mSettingValue = data->val();
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)settingData();
                      mSettingValue = (bool)data->val();
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+settingData()->objectName());
                      break;
  }

  refreshSetting();
}

int CYLabel::fontSize() const
{
  return mFontSize;
}

void CYLabel::setFontSize(const int val)
{
  mFontSize = val;
  QFont font = QWidget::font();
  font.setPointSize(mFontSize);
  setFont(font);
}

void CYLabel::wheelEvent(QWheelEvent * e)
{
/*  if ((e->delta()!=0) && (e->state() == Qt::ControlButton))
  {
    if (e->delta()>0)
      mFontSize++;
    else
      mFontSize--;

    QFont font = QWidget::font();
    font.setPointSize(mFontSize);
    setFont(font);
  }*/
  QLabel::wheelEvent(e);
}

/*! Gestion d'appui de bouton de souris
    \fn CYLabel::mousePressEvent ( QMouseEvent * e )
 */
void CYLabel::mousePressEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions )
  {
    mXOldPos = x();
    mYOldPos = y();
    mXMousePos = e->x();
    mYMousePos = e->y();
    emit movedInsideParent();
  }
  else if ( clickable() && (e->button()==Qt::LeftButton))
  {
    setFrameShadow(QFrame::Sunken);
  }
  QLabel::mousePressEvent( e );
}


/*! Gestion relachement de bouton de souris
    \fn CYLabel::mouseReleaseEvent( QMouseEvent * e )
 */
void CYLabel::mouseReleaseEvent( QMouseEvent * e )
{
  QLabel::mouseReleaseEvent(e);
  if ( clickable() && (e->button()==Qt::LeftButton))
  {
    setFrameShadow(QFrame::Raised);
    emit clicked();
  }
}


/*! Gestion de déplacement de bouton de souris
    \fn CYLabel::mouseMoveEvent ( QMouseEvent * e )
 */
void CYLabel::mouseMoveEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions && (e->buttons()==Qt::LeftButton) )
  {
    int ex = e->x();
    int ey = e->y();
    int x = mXOldPos + ( ex - mXMousePos );
    int y = mYOldPos + ( ey - mYMousePos );
    moveInsideParent( x, y );
    raise();
  }
  QLabel::mouseMoveEvent( e );
}


/*! Déplacement à l'intérieur du parent.
    \fn CYLabel::moveInsideParent(int x, int y)
 */
void CYLabel::moveInsideParent(int x, int y)
{
  QWidget * p = (QWidget *)parent();
  if ( !p->rect().contains(x, p->rect().y()) )
  {
    if ( x < p->rect().left() )
      x = p->rect().left();            // sort à gauche
    else
      x = p->rect().right()-width();   // sort à droite
  }
  else if ( !p->rect().contains(x+width(), p->rect().y()) )
  {
    x = p->rect().right()-width();     // sort en partie à droite
  }

  if ( !p->rect().contains(p->rect().x(), y) )
  {
    if ( y < p->rect().top() )
      y = p->rect().top();             // sort en haut
    else
      y = p->rect().bottom()-height(); // sort en bas
  }
  else if ( !p->rect().contains(p->rect().x(), y+height()) )
  {
    y = p->rect().bottom()-height();   // sort en partie en bas
  }

  move(x, y);
  mXOldPos = x;
  mYOldPos = y;

  emit modifie();
}


/*! Active/désactive le déplacement
    \fn CYLabel::setEditPositions( bool val )
 */
void CYLabel::setEditPositions( bool val )
{
  if ( !movable() )
    return;

  mEditPositions = val;
  refreshPosition();

  if ( mEditPositions )
    setCursor( Qt::PointingHandCursor );
  else
    unsetCursor();
}

void CYLabel::refresh()
{
  refreshPosition();
}


void CYLabel::update()
{
  updatePosition();
}


/*! Rafraîchit la position de l'objet en fonction des données positions.
    \fn CYDataWdg::refreshPosition()
 */
void CYLabel::refreshPosition()
{
  if ( !movable() )
    return;

  if ( ( mPosX && mPosY ) && ( ( mPosX->val()!=x() ) || ( mPosY->val()!=y() ) ) )
  {
    if ( (mPosX->val()>=0) && (mPosY->val()>=0) )
      move( mPosX->val(), mPosY->val() );
    else if ( mPosX->val()>=0 )
      move( mPosX->val(), y()          );
    else if ( mPosY->val()>=0 )
      move( x()         , mPosY->val() );
  }
  else if ( ( mPosX ) && ( mPosX->val()!=x() ) && ( mPosX->val()>=0 ) )
  {
    move( mPosX->val(), y()          );
  }
  else if ( ( mPosY ) && ( mPosY->val()!=y() ) && ( mPosY->val()>=0 ) )
    move( x()         , mPosY->val() );
}


/*! Met à jour des données positions en fonction de la position de l'objet.
    \fn CYDataWdg::updatePosition()
 */
void CYLabel::updatePosition()
{
  if ( !mEditPositions )
    return;

  if ( mPosX )
    mPosX->setVal( x() );

  if ( mPosY )
    mPosY->setVal( y() );
}


/*! Active / désactive temporairement le widget.
    Inhibe ctrlFlag jusqu'au prochain linkData pour
    une fenêtre de paramétrage avec rafraichissement.
    \fn CYLabel::setEnabledTmp(bool val)
 */
void CYLabel::setEnabledTmp(bool val)
{
  mCtrlFlag=false;
  setEnabled(val);
}
