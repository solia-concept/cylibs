/***************************************************************************
                          cycarddo.cpp  -  description
                             -------------------
    début                  : Thu Feb 13 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cyboxdo.h"
#include "cycarddo.h"

CYCardDO::CYCardDO(QWidget *parent, const QString &name, int no, Mode m)
  : CYCardDIG(parent, name, no, m)
{
  mNbCols=3;
  mNbRows=8;

  init();
}

CYCardDO::~CYCardDO()
{
}

void CYCardDO::init()
{
  int cpt=0;
  CYBoxDO *box;

  if (mMode == Forcing)
    setFocusPolicy(Qt::StrongFocus);
  else
    setFocusPolicy(Qt::NoFocus);
    
  mGrid = new QGridLayout(this);
  this->setLayout(mGrid);

  for (int col=0; col<mNbCols; col++)
  {
    // TOCHECK QT5 QVBoxLayout *vbox = new QVBoxLayout(col);
    QVBoxLayout *vbox= new QVBoxLayout(this);
    vbox->setObjectName(QString("colonne%1").arg(col));
    mVBox.append(vbox);
    vbox->setSpacing(6);

    for (int row=0, c=cpt; c<mNbRows+cpt ; row++, c++)
    {
      char buf[10];
      sprintf(buf,"%dS%02d", mNoCard, c);
      box = new CYBoxDO(this, buf, *mColors[col], (CYBoxIO::Mode) mMode);
      mBoxIOList.append(box);                                         
      mGrid->addWidget(box, row, col);
      connect(box, SIGNAL(help(const QString &)), this, SLOT(setHelp(const QString &)));
    }
    cpt += mNbRows;
  }
  adjustSize();
}

void CYCardDO::setData(QHash<int, CYDO*> db)
{
  datas = db;
  linkData();
}

void CYCardDO::linkData()
{
  CYBoxDO *box = (CYBoxDO *)mBoxIOList.first();
  QList<CYBoxDO*>::iterator it;

  for ( int i=0; i<datas.count(); i++)
  {
    if (box!=0)
      box->setData(datas[i]);
    else
      CYFATALDATA(datas[i]->objectName());
    it++;
    box = *it;
  }
  refreshForcing();
}

void CYCardDO::refreshForcing()
{
  CYBoxDO *box = (CYBoxDO *)mBoxIOList.first();
  QList<CYBoxDO*>::iterator it;

  for ( int i=0; i<datas.count(); i++)
  {
    if (box!=0)
      box->refreshForcing();
    else
      CYFATAL;
    it++;
    box = *it;
  }
}

bool CYCardDO::writeForcing()
{
  bool ret = true;

  CYBoxDO *box = (CYBoxDO *)mBoxIOList.first();
  QList<CYBoxDO*>::iterator it;

  for ( int i=0; i<datas.count(); i++)
  {
    if (box!=0)
      box->setValue();
    it++;
    box = *it;

    if (!datas[i]->writeForcing())
    {
      ret = false;
    }      
  }

  return ret;
}

void CYCardDO::updateCard()
{
  hide();
  while (!mBoxIOList.isEmpty())
    delete mBoxIOList.takeFirst();
  while (!mVBox.isEmpty())
    delete mVBox.takeFirst();
  delete mGrid;
  init();
  linkData();
  show();
}

void CYCardDO::simulation()
{
  emit simul();
}

QSize CYCardDO::sizeHint() const
{
  return QSize(QGroupBox::sizeHint());
}

QSize CYCardDO::minimumSizeHint() const
{
  if (QGroupBox::minimumSize().width() > mGrid->minimumSize().width())
    return QSize(QGroupBox::minimumSize());
  else
    return QSize(mGrid->minimumSize());
}
