/***************************************************************************
                          cyboxdig.h  -  description
                             -------------------
    début                  : jeu fév 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYBOXDIG_H
#define CYBOXDIG_H

// CYLIBS
#include "cyboxio.h"

class CYCardDIG;

/**
 * CYBoxDIG est un ensemble de widgets représentant une E/S TOR.
 * @short Représentation d'une E/S TOR.
 * @author Gérald LE CLEACH
 */

class CYBoxDIG : public CYBoxIO
{
  Q_OBJECT

public:
  /** Construit un objet représentant une E/S TOR.
    * @param parent  Widget parent.
    * @param name    Nom du widget.
    * @param c       Couleur de la led.
    * @param m       Mode d'utilisation du widget. */
  CYBoxDIG(QWidget *parent=0, const QString &name=0, QColor c=QColor(Qt::red), Mode m=View);
  /** Destructeur */
  ~CYBoxDIG();

  /** Initialise le widget. */
  virtual void init() {}

public slots: // Public slots
  virtual void refresh() {}
  /** Simulation. */
  virtual void simulation() {}

protected: // Protected methods
  /** Met à jour l'objet. */
  virtual void update() {}

protected: // Protected attributes 
  /** Couleur de la led. */
  QColor mColor;
  /** Vaut \a true si en simulation. */
  bool mSimul;
};

#endif
