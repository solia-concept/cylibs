/***************************************************************************
                          cyboxio.cpp  -  description
                             -------------------
    début                  : jeu mar 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cydata.h"
#include "cycardio.h"
#include "cyboxio.h"

CYBoxIO::CYBoxIO(QWidget *parent, const QString &name, Mode m)
: QFrame(parent),
  mMode(m),
  mHasData(false)
{
  setObjectName(name);
  mEnableMode = true;
  if (parent->inherits("CYCardIO"))
  {
    mCard  = (CYCardIO *)parent;
    connect((QObject *)parent, SIGNAL(simul()), this, SLOT(simulation()));
  }
  else
    mCard = 0;
}

CYBoxIO::~CYBoxIO()
{
}

CYBoxIO::Mode CYBoxIO::mode() const
{
  return mMode;
}

void CYBoxIO::setMode(const Mode mode)
{
  if (mode != mMode)
  {
    mMode = mode;
    mEnableMode = false;
    update();
    mEnableMode = true;
  }
}

void CYBoxIO::insertButton(QAbstractButton * button)
{
  if (mCard)
  {
    mCard->addButton(button);
  }
}

void CYBoxIO::removeButton(QAbstractButton * button)
{
  if (mCard)
  {
    mCard->removeButton(button);
  }
}
