/***************************************************************************
                          cydatasbrowser.cpp  -  description
                             -------------------
    begin                : ven mai 14 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydatasbrowser.h"

// QT
#include <QLayout>
// CYLIBS
#include "cycore.h"
#include "cydb.h"
#include "cydata.h"
#include "cynetlink.h"
#include "cynethost.h"
#include "cytextinput.h"
#include "cytype.h"
#include "cydatasbrowserlist.h"

CYDatasBrowser::CYDatasBrowser(QWidget *parent, const QString &name)
  : QFrame(parent)
{
  setObjectName(name);
  QVBoxLayout * l = new QVBoxLayout( this );
  mFilterBox = new QComboBox( this );
  mFilterBox->insertItem(User, tr("User datas"));
  mFilterBox->insertItem(Sys , tr("System datas"));
  mFilterBox->insertItem(All , tr("All datas"));
  l->addWidget( mFilterBox );

  mSearchBox = new CYTextInput(this);
  l->addWidget( mSearchBox );
  mSearchBox->setToolTip( "<p>"+
                          tr("The Seach bar makes it possible to search by words or characters among the filtered results. <br/><br/>"
                             " Only matches (data and/or categories) are then displayed.")
                          +"</p>" );

  mList = new CYDatasBrowserList( this, "datasBrowserList");
  mList->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Expanding);
  l->addWidget( mList );

  connect ( mSearchBox, SIGNAL(textChanged(QString)), mList, SLOT(changeSearch(QString)));
  connect ( mFilterBox, SIGNAL(activated(int)), mList, SLOT(changeFilter(int)));
  connect( core, SIGNAL( DBChanged() ), mList, SLOT( rebuild() ) );

  enabelFilterBox( true );
}

CYDatasBrowser::~CYDatasBrowser()
{
}

void CYDatasBrowser::init(CYNetHost *host, CYNetLink *link, bool razHost)
{
  mList->init(host, link, razHost);
}

void CYDatasBrowser::clear()
{
  mList->clear();
}


/*! Active / désactive la boîte de sélection du filtrage des données si \a val vaut \a true.
    \fn CYDatasBrowser::enabelFilterBox(bool val)
 */
void CYDatasBrowser::enabelFilterBox(bool val)
{
  if (!val)
  {
    mFilterBox->setSizePolicy( QSizePolicy::Fixed, QSizePolicy::Fixed);
    mFilterBox->setMaximumHeight(0);
    mFilterBox->setEnabled( false );
  }
  else
  {
    mFilterBox->setSizePolicy( QSizePolicy::Preferred, QSizePolicy::Fixed);
    mFilterBox->show();
    mFilterBox->setEnabled( true );
  }
}
