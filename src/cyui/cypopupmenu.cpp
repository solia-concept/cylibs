//
// C++ Implementation: cypopupmenu
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cypopupmenu.h"
//Added by qt3to4:
#include <QMenu>

CYPopupMenu::CYPopupMenu(QString text, bool readOnly, QWidget *parent, const QString &name)
  : QMenu(parent),
    mText(text),
    mReadOnly(readOnly)
{
  setObjectName(name);
}


CYPopupMenu::~CYPopupMenu()
{
}

void CYPopupMenu::setText(QString txt)
{
  mText = txt;
}


QString CYPopupMenu::text()
{
  return mText;
}


void CYPopupMenu::insertInto(QWidget *from, QMenu *pm)
{
  this->setTitle(text());
  pm->addMenu(this);
  emit inserted(from);
}
