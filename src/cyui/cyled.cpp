/***************************************************************************
                          cyled.cpp  -  description
                             -------------------
    début                  : mar jun 17 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyled.h"

#define PAINT_BENCH
#undef PAINT_BENCH

#ifdef PAINT_BENCH
#include <QDateTime>
#include <QString>
#include <QPaintEvent>
#include <QPixmap>
#include <QMouseEvent>
#include <stdio.h>
#endif

// QT
#include <QPainter>
#include <QImage>
#include <QColor>
// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyword.h"
#include "cyflag.h"
#include "cyflagdialog.h"
#include "cytextlabel.h"
#include <cyapplication.h>

class CYLed::CYLedPrivate
{
  friend class CYLed;

  int dark_factor;
  QColor offcolor;
  QPixmap *off_map;
  QPixmap *on_map;
};


CYLed::CYLed(QWidget *parent, const QString &name)
  : QWidget( parent),
    CYDataWdg(this),
    led_state(On),
    led_look(Raised),
    led_shape(Circular)
{
  setObjectName(name);
  QColor col(Qt::green);
  d = new CYLed::CYLedPrivate;
  d->dark_factor = 300;
  d->offcolor = col.darker(300);
  d->off_map = 0;
  d->on_map = 0;

  setColor(col);
  init(false);
}

CYLed::CYLed(const QColor& col, QWidget *parent, const QString &name)
  : QWidget( parent),
    CYDataWdg(this),
    led_state(On),
    led_look(Raised),
    led_shape(Circular)
{
  setObjectName(name);
  d = new CYLed::CYLedPrivate;
  d->dark_factor = 300;
  d->offcolor = col.darker(300);
  d->off_map = 0;
  d->on_map = 0;

  setColor(col);
  //setShape(Circular);
  init(false);
}

CYLed::CYLed(const QColor& col, CYLed::State state,
             CYLed::Look look, CYLed::Shape shape, QWidget *parent, const QString &name )
  : QWidget(parent),
    CYDataWdg(this),
    led_state(state),
    led_look(look),
    led_shape(shape)
{
  setObjectName(name);
  d = new CYLed::CYLedPrivate;
  d->dark_factor = 300;
  d->offcolor = col.darker(300);
  d->off_map = 0;
  d->on_map = 0;

  //setShape(shape);
  setColor(col);

  init(state);
}

void CYLed::init(bool value)
{
  // TODO QT5
  //  setBackgroundOrigin(QWidget::ParentOrigin);
  mBicolor = false;
  mValue = value;
  setDarkFactor(450);
  mPalette = palette();
  mColorOn  = Qt::green;
  mColorOff = Qt::red;
  mEnableState = true;
  mFlashing = false;
  mFlashState =true;
}


CYLed::~CYLed()
{
  delete d->off_map;
  delete d->on_map;
  delete d;
}


bool CYLed::movable() const
{
  return mMovable;
}

void CYLed::setMovable(const bool val)
{
  mMovable = val;
  setEditPositions( mEditPositions );
}

void CYLed::paintEvent(QPaintEvent *)
{
#ifdef PAINT_BENCH
  const int rounds = 1000;
  QTime t;
  t.start();
  for (int i=0; i<rounds; i++) {
#endif
    switch(led_shape)
    {
      case Rectangular:
        switch (led_look)
        {
          case Sunken :
            paintRectFrame(false);
            break;
          case Raised :
            paintRectFrame(true);
            break;
          case Flat   :
            paintRect();
            break;
          default  :
            CYWARNINGTEXT(QString("%1: in class CYLed: no CYLed::Look set").arg(objectName()));
        }
        break;
      case Circular:
        switch (led_look)
        {
          case Flat   :
            paintFlat();
            break;
          case Raised :
            paintRound();
            break;
          case Sunken :
            paintSunken();
            break;
          default:
            CYWARNINGTEXT(QString("%1: in class CYLed: no CYLed::Look set").arg(objectName()));
        }
        break;
      default:
        CYWARNINGTEXT(QString("%1: in class CYLed: no CYLed::Shape set").arg(objectName()));
        break;
    }
#ifdef PAINT_BENCH
  }
  int ready = t.elapsed();
  CYWARNINGTEXT(QString("elapsed: %1 msec. for %2 rounds").arg(ready).arg(rounds));
#endif
}

int CYLed::ensureRoundLed()
{
  // Initialize coordinates, width, and height of the LED
  //
  int width = this->width();
  // Make sure the LED is round!
  if (width > this->height())
    width = this->height();
  width -= 2; // leave one pixel border
  if (width < 0)
    width = 0;

  return width;
}

bool CYLed::paintCachedPixmap()
{
  if (led_state) {
    if (d->on_map) {
      QPainter paint(this);
      paint.drawPixmap(0, 0, *d->on_map);
      return true;
    }
  } else {
    if (d->off_map) {
      QPainter paint(this);
      paint.drawPixmap(0, 0, *d->off_map);
      return true;
    }
  }

  return false;
}

void CYLed::paintRect()
{
  QPainter painter(this);
  QBrush lightBrush(led_color);
  QBrush darkBrush(d->offcolor);
  QPen pen(led_color.darker(300));
  int w=width();
  int h=height();
  // -----
  switch(led_state)
  {
    case On:
      painter.setBrush(lightBrush);
      painter.drawRect(0, 0, w, h);
      break;
    case Off:
      painter.setBrush(darkBrush);
      painter.drawRect(0, 0, w, h);
      painter.setPen(pen);
      painter.drawLine(0, 0, w, 0);
      painter.drawLine(0, h-1, w, h-1);
      // Draw verticals
      int i;
      for(i=0; i < w; i+= 4 /* dx */)
        painter.drawLine(i, 1, i, h-1);
      break;
    default: break;
  }
}

void CYLed::paintRectFrame(bool raised)
{
  QPainter painter(this);
  QBrush lightBrush(led_color);
  QBrush darkBrush(d->offcolor);
  int w=width();
  int h=height();
  QColor black=Qt::black;
  QColor white=Qt::white;
  // -----
  if(raised)
  {
    painter.setPen(white);
    painter.drawLine(0, 0, 0, h-1);
    painter.drawLine(1, 0, w-1, 0);
    painter.setPen(black);
    painter.drawLine(1, h-1, w-1, h-1);
    painter.drawLine(w-1, 1, w-1, h-1);
    painter.fillRect(1, 1, w-2, h-2,
                     (led_state==On)? lightBrush : darkBrush);
  } else {
    painter.setPen(black);
    painter.drawRect(0,0,w,h);
    painter.drawRect(0,0,w-1,h-1);
    painter.setPen(white);
    painter.drawRect(1,1,w-1,h-1);
    painter.fillRect(2, 2, w-4, h-4,
                     (led_state==On)? lightBrush : darkBrush);
  }
}

CYLed::State CYLed::state() const
{
  return led_state;
}

CYLed::Shape CYLed::shape() const
{
  return led_shape;
}

QColor CYLed::color() const
{
  return led_color;
}

CYLed::Look CYLed::look() const
{
  return led_look;
}

void CYLed::setState( State state )
{
  if (led_state != state)
  {
    led_state = state;
    QWidget::update();
  }
}

void CYLed::toggleState()
{
  toggle();
  emit stateChanged(state());
}

void CYLed::setShape(CYLed::Shape s)
{
  if(led_shape!=s)
  {
    led_shape = s;
    QWidget::update();
  }
}

void CYLed::setColor(const QColor& col)
{
  if(led_color!=col) {
    if(d->on_map)  { delete d->on_map; d->on_map = 0; }
    if(d->off_map) { delete d->off_map; d->off_map = 0; }
    led_color = col;
    d->offcolor = col.darker(d->dark_factor);
    QWidget::update();
  }
}

void
CYLed::setDarkFactor(int darkfactor)
{
  if (d->dark_factor != darkfactor) {
    d->dark_factor = darkfactor;
    d->offcolor = led_color.darker(darkfactor);
    QWidget::update();
  }
}

int
CYLed::darkFactor() const
{
  return d->dark_factor;
}

void
CYLed::setLook( Look look )
{
  if(led_look!=look)
  {
    led_look = look;
    QWidget::update();
  }
}

void
CYLed::toggle()
{
  led_state = (led_state == On) ? Off : On;
  // setColor(led_color);
  QWidget::update();
}

void
CYLed::on()
{
  setState(On);
}

void
CYLed::off()
{
  setState(Off);
}

QSize
CYLed::sizeHint() const
{
  return QSize(16, 16);
}

QSize
CYLed::minimumSizeHint() const
{
  return QSize(16, 16 );
}

void CYLed::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYLed::dataName() const
{
  return CYDataWdg::dataName();
}

void CYLed::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYLed::benchType() const
{
  return CYDataWdg::benchType();
}

int CYLed::genericMarkOffset() const
{
  return mGenericMarkOffset;
}

void CYLed::setGenericMarkOffset(const int val)
{
  mGenericMarkOffset = val;
}

void CYLed::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYLed::flagName() const
{
  return CYDataWdg::flagName();
}

void CYLed::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYLed::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYLed::setInverseFlag(bool inverse)
{
  CYDataWdg::setInverseFlag(inverse);
}

bool CYLed::inverseFlag() const
{
  return CYDataWdg::inverseFlag();
}

void CYLed::setInverseHideFlag(bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYLed::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

void CYLed::setPosXName(const QByteArray &name)
{
  CYDataWdg::setPosXName( name );
}

QByteArray CYLed::posXName() const
{
  return CYDataWdg::posXName();
}

void CYLed::setPosYName(const QByteArray &name)
{
  CYDataWdg::setPosYName( name );
}

QByteArray CYLed::posYName() const
{
  return CYDataWdg::posYName();
}

bool CYLed::ctrlFlag()
{
  if (mFlag==0)
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;

  setEnabled(res);
  return res;
}

void CYLed::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYLed::setData(CYData *data)
{
  mData = data;
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  linkData();
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}

bool CYLed::value()
{
  return mValue;
}

void CYLed::setValue(bool val)
{
  if ( mFlashing && val)
  {
    mFlashState=!mFlashState;
    val = mFlashState;
  }
  if ( !mBicolor )
  {
    //     setColor(mColorOn);
    //     mPalette.setColor(QPalette::Active, QPalette::Foreground, mColorOn);
    if (mEnableState)
      setState(val ? CYLed::On : CYLed::Off);
    else
      setState(val ? CYLed::Off : CYLed::On);
  }
  else
  {
    if (val)
    {
      setColor(mColorOn);
      mPalette.setColor(QPalette::Active, QPalette::Foreground, mColorOn);
    }
    else
    {
      setColor(mColorOff);
      mPalette.setColor(QPalette::Active, QPalette::Foreground, mColorOff);
    }
    setState(CYLed::On);
  }

  mValue = val;
}

void CYLed::setValue(int val)
{
  setValue((bool)val);
}


void CYLed::linkData()
{
  ctrlHideFlag();

  CYDataWdg::linkData();

  if (!hasData())
  {
    return;
  }

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (!ctrlFlag())
  {
    setState(CYLed::Off);
    return;
  }
  if (mData->flag()!=0)
  {
    if (!mData->flag()->val())
    {
      setState(CYLed::Off);
      return;
    }
  }

  this->setToolTip(mData->displayHelp()+"<br>"+mData->infoCY());

  switch (mData->type())
  {
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      setValue((bool)data->val());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      setValue((bool)data->val());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

/*! Gestion d'appui de bouton de souris
    \fn CYLed::mousePressEvent ( QMouseEvent * e )
 */
void CYLed::mousePressEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions )
  {
    mXOldPos = x();
    mYOldPos = y();
    mXMousePos = e->x();
    mYMousePos = e->y();
    emit movedInsideParent();
  }
  QWidget::mousePressEvent ( e );
}


/*! Gestion de déplacement de bouton de souris
    \fn CYLed::mouseMoveEvent ( QMouseEvent * e )
 */
void CYLed::mouseMoveEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions && (e->buttons()==Qt::LeftButton) )
  {
    int ex = e->x();
    int ey = e->y();
    int x = mXOldPos + ( ex - mXMousePos );
    int y = mYOldPos + ( ey - mYMousePos );
    moveInsideParent( x, y );
    raise();
  }
  QWidget::mouseMoveEvent( e );
}


/*! Déplacement à l'intérieur du parent.
    \fn CYLed::moveInsideParent(int x, int y)
 */
void CYLed::moveInsideParent(int x, int y)
{
  QWidget * p = (QWidget *)parent();
  if ( !p->rect().contains(x, p->rect().y()) )
  {
    if ( x < p->rect().left() )
      x = p->rect().left();            // sort à gauche
    else
      x = p->rect().right()-width();   // sort à droite
  }
  else if ( !p->rect().contains(x+width(), p->rect().y()) )
  {
    x = p->rect().right()-width();     // sort en partie à droite
  }

  if ( !p->rect().contains(p->rect().x(), y) )
  {
    if ( y < p->rect().top() )
      y = p->rect().top();             // sort en haut
    else
      y = p->rect().bottom()-height(); // sort en bas
  }
  else if ( !p->rect().contains(p->rect().x(), y+height()) )
  {
    y = p->rect().bottom()-height();   // sort en partie en bas
  }

  move(x, y);
  mXOldPos = x;
  mYOldPos = y;

  emit modifie();
}


/*! Active/désactive le déplacement
    \fn CYLed::setEditPositions( bool val )
 */
void CYLed::setEditPositions( bool val )
{
  if ( !movable() )
    return;

  mEditPositions = val;
  refreshPosition();

  if ( mEditPositions )
    setCursor( Qt::PointingHandCursor );
  else
    unsetCursor();
}


/*! Rafraîchit la position de l'objet en fonction des données positions.
    \fn CYDataWdg::refreshPosition()
 */
void CYLed::refreshPosition()
{
  if ( !movable() )
    return;

  if ( ( mPosX && mPosY ) && ( ( mPosX->val()!=x() ) || ( mPosY->val()!=y() ) ) )
  {
    if ( (mPosX->val()>=0) && (mPosY->val()>=0) )
      move( mPosX->val(), mPosY->val() );
    else if ( mPosX->val()>=0 )
      move( mPosX->val(), y()          );
    else if ( mPosY->val()>=0 )
      move( x()         , mPosY->val() );
  }
  else if ( ( mPosX ) && ( mPosX->val()!=x() ) && ( mPosX->val()>=0 ) )
  {
    move( mPosX->val(), y()          );
  }
  else if ( ( mPosY ) && ( mPosY->val()!=y() ) && ( mPosY->val()>=0 ) )
    move( x()         , mPosY->val() );
}


/*! Met à jour des données positions en fonction de la position de l'objet.
    \fn CYDataWdg::updatePosition()
 */
void CYLed::updatePosition()
{
  if ( !mEditPositions )
    return;

  if ( mPosX )
    mPosX->setVal( x() );

  if ( mPosY )
    mPosY->setVal( y() );
}


void CYLed::refresh()
{
  refreshPosition();
  ctrlHideFlag();

  if (!hasData())
  {
    return;
  }

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (!isVisible())
    return;

  if (!ctrlFlag())
  {
    setState(CYLed::Off);
    return;
  }
  if (mData==0)
    return;
  if (mData->flag()!=0)
  {
    if (!mData->flag()->val())
    {
      setState(CYLed::Off);
      return;
    }
  }

  switch (mData->type())
  {
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      if (data->isOk())
      {
        setEnabled(true);
        setValue((bool)data->val());
        return;
      }
      setEnabled(false);
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
      break;
  }
}

void CYLed::update()
{
  updatePosition();
}

/*!
    \fn CYLed::paintFlat()
 */
void CYLed::paintFlat()
{
  QPainter paint;
  QColor color;
  QBrush brush;
  QPen pen;

  // Initialize coordinates, width, and height of the LED
  //
  int width = this->width();
  int xPos = 1;
  int yPos = 1;

  // Make sure the LED is round!
  if (width > this->height())
  {
    xPos += (width-this->height())/2;
    width = this->height();
  }
  else
    yPos += (this->height()-width)/2;

  width -= 2; // leave one pixel border
  if (width < 0)
    width = 0;


  // start painting widget
  //
  paint.begin( this );

  // Set the color of the LED according to given parameters
  //   color = ( state() ) ? CYLed::color() : CYLed::color().darker();
  color = ( state() ) ? CYLed::color() : CYLed::color().darker(darkFactor());

  // Set the brush to SolidPattern, this fills the entire area
  // of the ellipse which is drawn with a thin grey "border" (pen)
  brush.setStyle( Qt::SolidPattern );
  brush.setColor( color );

  pen.setWidth( 1 );
  color.setRgb( 170, 170, 170 );      // This is a grey color value
  pen.setColor( color );          // Set the pen accordingly

  paint.setPen( pen );            // Select pen for drawing
  paint.setBrush( brush );        // Assign the brush to the painter

  // Draws a "flat" LED with the given color:
  paint.drawEllipse( xPos, yPos, width, width );

  paint.end();
  //
  // painting done
}


/*!
    \fn CYLed::paintRound()
 */
void CYLed::paintRound()
{
  QPainter paint;
  QColor color;
  QBrush brush;
  QPen pen;

  // Initialize coordinates, width, and height of the LED
  int width = this->width();
  int xPos = 1;
  int yPos = 1;

  // Make sure the LED is round!
  if (width > this->height())
  {
    xPos += (width-this->height())/2;
    width = this->height();
  }
  else
    yPos += (this->height()-width)/2;

  width -= 2; // leave one pixel border
  if (width < 0)
    width = 0;

  // start painting widget
  //
  paint.begin( this );

  // Set the color of the LED according to given parameters
  color = ( state() ) ? CYLed::color() : CYLed::color().darker(darkFactor());

  // Set the brush to SolidPattern, this fills the entire area
  // of the ellipse which is drawn first
  brush.setStyle( Qt::SolidPattern );
  brush.setColor( color );
  paint.setBrush( brush );            // Assign the brush to the painter

  // Draws a "flat" LED with the given color:
  paint.drawEllipse( xPos, yPos, width, width );

  // Draw the bright light spot of the LED now, using modified "old"
  // painter routine taken from KDEUI?s CYLed widget:

  // Setting the new width of the pen is essential to avoid "pixelized"
  // shadow like it can be observed with the old LED code
  pen.setWidth( 2 );

  // shrink the light on the LED to a size about 2/3 of the complete LED
  int xPos2 = xPos + width/5;
  int yPos2 = yPos + width/5;
  int light_width = width;
  light_width *= 2;
  light_width /= 3;

  // Calculate the LED?s "light factor":
  int light_quote = (130*2/(light_width?light_width:1))+100;

  // Now draw the bright spot on the LED:
  while (light_width) {
    color = color.lighter( light_quote );                     // make color lighter
    pen.setColor( color );                          // set color as pen color
    paint.setPen( pen );                            // select the pen for drawing
    paint.drawEllipse( xPos2, yPos2, light_width, light_width );        // draw the ellipse (circle)
    light_width--;
    if (!light_width)
      break;
    paint.drawEllipse( xPos2, yPos2, light_width, light_width );
    light_width--;
    if (!light_width)
      break;
    paint.drawEllipse( xPos2, yPos2, light_width, light_width );
    xPos2++; yPos2++; light_width--;
  }

  // Drawing of bright spot finished, now draw a thin grey border
  // around the LED; it looks nicer that way. We do this here to
  // avoid that the border can be erased by the bright spot of the LED

  pen.setWidth( 1 );
  color.setRgb( 170, 170, 170 );              // This is a grey color value
  pen.setColor( color );                      // Set the pen accordingly
  paint.setPen( pen );                        // Select pen for drawing
  brush.setStyle( Qt::NoBrush );          // Switch off the brush
  paint.setBrush( brush );                    // This avoids filling of the ellipse

  paint.drawEllipse( xPos, yPos, width, width );

  paint.end();
  //
  // painting done

  return;
}


/*!
    \fn CYLed::paintSunken()
 */
void CYLed::paintSunken()
{
  QPainter paint;
  QColor color;
  QBrush brush;
  QPen pen;

  // First of all we want to know what area should be updated
  // Initialize coordinates, width, and height of the LED
  int width = this->width();
  int xPos = 1;
  int yPos = 1;

  // Make sure the LED is round!
  if (width > this->height())
  {
    xPos += (width-this->height())/2;
    width = this->height();
  }
  else
    yPos += (this->height()-width)/2;

  width -= 2; // leave one pixel border
  if (width < 0)
    width = 0;

  // maybe we could stop HERE, if width <=0 ?

  // start painting widget
  //
  paint.begin( this );

  // Set the color of the LED according to given parameters
  color = ( state() ) ? CYLed::color() : CYLed::color().darker(darkFactor());

  // Set the brush to SolidPattern, this fills the entire area
  // of the ellipse which is drawn first
  brush.setStyle( Qt::SolidPattern );
  brush.setColor( color );
  paint.setBrush( brush );                // Assign the brush to the painter

  // Draws a "flat" LED with the given color:
  paint.drawEllipse( xPos, yPos, width, width );

  // Draw the bright light spot of the LED now, using modified "old"
  // painter routine taken from KDEUI?s CYLed widget:

  // Setting the new width of the pen is essential to avoid "pixelized"
  // shadow like it can be observed with the old LED code
  pen.setWidth( 2 );

  // shrink the light on the LED to a size about 2/3 of the complete LED
  int xPos2 = xPos + width/5;
  int yPos2 = yPos + width/5;
  int light_width = width;
  light_width *= 2;
  light_width /= 3;

  // Calculate the LED?s "light factor":
  int light_quote = (130*2/(light_width?light_width:1))+100;

  // Now draw the bright spot on the LED:
  while (light_width) {
    color = color.lighter( light_quote );                      // make color lighter
    pen.setColor( color );                                   // set color as pen color
    paint.setPen( pen );                                     // select the pen for drawing
    paint.drawEllipse( xPos2, yPos2, light_width, light_width ); // draw the ellipse (circle)
    light_width--;
    if (!light_width)
      break;
    paint.drawEllipse( xPos2, yPos2, light_width, light_width );
    light_width--;
    if (!light_width)
      break;
    paint.drawEllipse( xPos2, yPos2, light_width, light_width );
    xPos2++; yPos2++; light_width--;
  }

  // Drawing of bright spot finished, now draw a thin border
  // around the LED which resembles a shadow with light coming
  // from the upper left.

  pen.setWidth( 3 );
  brush.setStyle( Qt::NoBrush );              // Switch off the brush
  paint.setBrush( brush );                        // This avoids filling of the ellipse

  // Set the initial color value to 200 (bright) and start
  // drawing the shadow border at 45? (45*16 = 720).

  int shadow_color = 200, angle;

  for ( angle = 720; angle < 6480; angle += 240 ) {
    color.setRgb( shadow_color, shadow_color, shadow_color );
    pen.setColor( color );
    paint.setPen( pen );
    paint.drawArc( xPos, yPos, width, width, angle, 240 );
    if ( angle < 2320 ) {
      shadow_color -= 25;         // set color to a darker value
      if ( shadow_color < 100 ) shadow_color = 100;
    }
    else if ( ( angle > 2320 ) && ( angle < 5760 ) ) {
      shadow_color += 25;         // set color to a brighter value
      if ( shadow_color > 255 ) shadow_color = 255;
    }
    else {
      shadow_color -= 25;         // set color to a darker value again
      if ( shadow_color < 100 ) shadow_color = 100;
    } // end if ( angle < 2320 )
  }   // end for ( angle = 720; angle < 6480; angle += 160 )

  paint.end();
  //
  // painting done
}

void CYLed::mouseDoubleClickEvent ( QMouseEvent * e )
{
  if (!core->simulation())
    return QWidget::mouseDoubleClickEvent ( e );

  CYFlagDialog *dlg = new CYFlagDialog(this);
  if (mData)
  {
    dlg->setDataName(mData->objectName().toLocal8Bit());
    dlg->exec();
  }
}
