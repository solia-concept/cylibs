/***************************************************************************
                          cycardana.cpp  -  description
                             -------------------
    début                  : jeu mar 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// QT
#include <qapplication.h>
// CYLIBS
#include "cydata.h"
#include "cyboxana.h"
#include "cycardana.h"

CYCardANA::CYCardANA(QWidget *parent, const QString &name, int no, Mode m)
  : CYCardIO(parent, name, no, m)
{
  mMaxCols = 1;
  mNoCol   = 0;

  mColors.clear();
  for (int col=0; col<mMaxCols; col++)
    mColors.insert(col, new QColor(Qt::red));
}

CYCardANA::~CYCardANA()
{
}

QSize CYCardANA::sizeHint() const
{
  return QSize(48, 48);
}

QSize CYCardANA::minimumSizeHint() const
{
  return QSize(16, 16);
}
