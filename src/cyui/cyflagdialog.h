#ifndef CYFLAGDIALOG_H
#define CYFLAGDIALOG_H

// CYLIBS
#include <cydialog.h>

namespace Ui {
    class CYFlagDialog;
}


class CYFlagDialog : public CYDialog
{
public:
  CYFlagDialog(QWidget *parent=0, const QString &name="flagDialog");
  ~CYFlagDialog();

  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

private:
    Ui::CYFlagDialog *ui;
};


#endif // CYFLAGDIALOG_H
