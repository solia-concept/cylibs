#include "cyplotpicker.h"

CYPlotPicker::CYPlotPicker( QWidget *canvas ):
  QwtPlotPicker(canvas)
{
}

CYPlotPicker::CYPlotPicker(int xAxis, int yAxis,
                           RubberBand rubberBand,
                           DisplayMode trackerMode,
                           QWidget *canvas) :
  QwtPlotPicker(xAxis, yAxis, rubberBand, trackerMode, canvas)
{
}

void CYPlotPicker::widgetMousePressEvent(QMouseEvent *mouseEvent)
{
  emit mousePressed(*mouseEvent);

  QwtPlotPicker::widgetMousePressEvent(mouseEvent);
}

void CYPlotPicker::widgetMouseReleaseEvent(QMouseEvent *mouseEvent)
{
  emit mouseReleased(*mouseEvent);

  QwtPlotPicker::widgetMouseReleaseEvent(mouseEvent);
}
