/***************************************************************************
                          cycarddi.h  -  description
                             -------------------
    début                  : Thu Feb 13 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCARDDI_H
#define CYCARDDI_H

// CYLIBS
#include "cydi.h"
#include "cycarddig.h"

/** CYCardDI représente les cartes électriques d'entrées TOR.
  * @see CYCardDIG, CYCardDO
  * @short Carte d'entrées TOR.
  * @author Gérald LE CLEACH
  */

class CYCardDI : public CYCardDIG
{
  Q_OBJECT

public:
  /** Constructeur */
  CYCardDI(QWidget *parent=0, const QString &name=0, int no=0, Mode m=View);
  /** Destructeur */
  ~CYCardDI();

  virtual QSize sizeHint() const;
  virtual QSize minimumSizeHint() const;

public slots:
  /** Simulation: permute l'état des leds. */
  virtual void simulation();
  /** Place les données dans le widget.
   * @param db Base de données à traîter. */
  void setData(QHash<int, CYDI*> db);
  /** Lie les données avec les widgets des voies relatives. */
  void linkData();

private:
  /** Initialise le widget selon les paramètres de la carte. */
  virtual void init();
  /** Met à jour les widgets d'E/S. */
  virtual void updateCard();

  /** Données */
  QHash<int, CYDI*> mDatas;
};

#endif
