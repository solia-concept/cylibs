/***************************************************************************
                          cydial.h  -  description
                             -------------------
    begin                : dim nov 23 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDIAL_H
#define CYDIAL_H

// QWT
#include "qwt_dial.h"
//Added by qt3to4:
#include <QString>
#include <QMouseEvent>
// CYLIBS
#include "cydatawdg.h"

class CYF64;
class CYS32;


/** @short Afficheur à aiguilles.
  * @author Gérald LE CLEACH
  */

class CYDial : public QwtDial, public CYDataWdg
{
  Q_OBJECT
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public: 
  CYDial(QWidget *parent=0, const QString &name=0);
  CYDial(CYDB *db, QWidget *parent=0, const QString &name=0);
  ~CYDial();

signals: // Signals
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();
  /** Emis à chaque validation des valeurs de l'ensemble des données. */
  void validate();

public: // Public methods
  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** @return la légende. */
  QString label() const;
  /** Saisie la légende. */
  void setLabel(const QString &label);

  /** @return la taille de police de la légende. */
  int fontSizeLabel() { return mFontSizeLabel; }
  /** Saisie la taille de police de la légende. */
  void setFontSizeLabel(int size) { mFontSizeLabel = size; }

  /** @return la valeur courante. */
  double value();

  virtual void setColors( QColor needle, QColor background, QColor text);

public slots: // Public slots
  /** Place \a data comme donnée du widget. */
  void setData(CYData *data);

  /** Place la valeur du contrôleur. */
  void setValue(double);
  /** Place la valeur du contrôleur. */
  void setValue(QString);
  /** Met à jour l'échelle. */
  virtual void updateScale();

  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();

protected: // Protected methods
  /** Dessine le contenu de l'échelle. */
  virtual void drawScaleContents(QPainter *painter, const QPointF &center, double radius) const;
  /** Gestion du clic de souris. */
  virtual void mousePressEvent(QMouseEvent * e);
  //  /** Gestion de redimenssionnement. */
  //  virtual void resizeEvent(QResizeEvent *e) ;

private: // Private methods
  void init();

protected: // Protected attributes
  /** Légende. */
  QString mLabel;
  /** Taille de police de la légende. */
  int mFontSizeLabel;
  /** Unité. */
  QString mUnit;
  /** Nombre de décimales. */
  int mNbDec;
  /** Valeur min de l'échelle. */
  CYF64 *mScaleMin;
  /** Valeur max de l'échelle. */
  CYF64 *mScaleMax;
  /** Pas de l'échelle. */
  CYF64 *mScaleStep;
  /** Nombre maximum de graduations principales dans un pas. */
  CYS32 *mMaxMajIntv;
  /** Nombre maximum de graduations secondaires dans un pas. */
  CYS32 *mMaxMinIntv;
};

#endif
