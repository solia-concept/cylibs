/***************************************************************************
                          cybuttongroup.h  -  description
                             -------------------
    begin                : jeu nov 13 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYBUTTONGROUP_H
#define CYBUTTONGROUP_H

// QT
#include <QGroupBox>
// CYLIBS
#include "cydatawdg.h"

/** @short QButtonGroup CY.
  * @author Gérald LE CLEACH
  */

class CYButtonGroup : public QGroupBox, public CYDataWdg
{
  Q_OBJECT
  Q_ENUMS(GroupSection)
  Q_PROPERTY(bool movable READ movable WRITE setMovable)
  Q_PROPERTY(bool showLabel READ showLabel WRITE setShowLabel)
  Q_PROPERTY(GroupSection showGroup READ showGroup WRITE setShowGroup)
  Q_PROPERTY(bool showValue READ showValue WRITE setShowValue)
  Q_PROPERTY(bool alwaysOk READ alwaysOk WRITE setAlwaysOk)
  Q_PROPERTY(bool autoRefresh READ autoRefresh WRITE setAutoRefresh)
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray posXName READ posXName WRITE setPosXName)
  Q_PROPERTY(QByteArray posYName READ posYName WRITE setPosYName)
  Q_PROPERTY(QByteArray forcingName READ forcingName WRITE setForcingName)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(int genericMarkOffset READ genericMarkOffset WRITE setGenericMarkOffset)
  Q_PROPERTY(bool inputHelp READ inputHelp WRITE setInputHelp)
  Q_PROPERTY(bool showToolTip READ showToolTip WRITE setShowToolTip)
  Q_PROPERTY(bool protectedAccess READ protectedAccess WRITE setProtectedAccess)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)


public:
  CYButtonGroup(QWidget *parent=0, const QString &name=0);
  CYButtonGroup(const QString &title, QWidget *parent=0, const QString &name=0);
  ~CYButtonGroup();

  /** Mode d'affichage du groupe. */
  /* ATTENTION énumération deavant être identique à Cy::GroupSection*/
  enum GroupSection
  {
    /** Pas d'affichage du groupe. */
    No,
    /** Affichage du dernier sous-groupe. */
    lastGroup,
    /** Affichage des sous-groupes. */
    underGroup,
    /** Affichage du groupe entier. */
    wholeGroup
  };

  /** @return \a true si l'objet peut être déplacé.
    * @see void setMovable(const bool val). */
  virtual bool movable() const;
  /** Saisie \a true pour pouvoir déplacer. */
  virtual void setMovable(const bool val);

  /** @return \a true s'il faut y afficher l'étiquette de la donnée. */
  virtual bool showLabel() const { return mShowLabel; }
  /** Saisir \a true pour y afficher l'étiquette de la donnée. */
  virtual void setShowLabel(const bool val) { mShowLabel = val; setAlwaysOk(true); }

  /** @return le mode d'affichage du groupe. */
  virtual GroupSection showGroup() const { return mShowGroup; }
  /** Saisir le mode d'affichage du groupe. */
  virtual void setShowGroup(const GroupSection val) { mShowGroup = val; }

  /** @return \a true s'il faut y afficher la valeur de la donnée. */
  virtual bool showValue() const { return mShowValue; }
  /** Saisir \a true pour y afficher la valeur de la donnée. */
  virtual void setShowValue(const bool val) { mShowValue = val; setAlwaysOk(true); }

  /** @return \a true la donnée traîtée est considérée comme toujours valide. */
  virtual bool alwaysOk() const;
  /** La donnée traîtée est considérée comme toujours valide si \a val vaut true. */
  virtual void setAlwaysOk(const bool val);

  /** @return \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  bool autoRefresh() const { return mAutoRefresh; }
  /** Saisir \a true pour que le widget soit rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  void setAutoRefresh(const bool val) { mAutoRefresh = val; }

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le nom de la donnée de positionnement en X. */
  virtual QByteArray posXName() const;
  /** Saisie le nom de la donnée de positionnement en X. */
  virtual void setPosXName(const QByteArray &name);

  /** @return le nom de la donnée de positionnement en Y. */
  virtual QByteArray posYName() const;
  /** Saisie le nom de la donnée de positionnement en Y. */
  virtual void setPosYName(const QByteArray &name);

  /** @return le nom de la donnée de forçage associée. */
  virtual QByteArray forcingName() const;
  /** Saisie le nom de la donnée de forçageassociée. */
  virtual void setForcingName(const QByteArray &name);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return l'offset sur le repère de généricité lorqu'il s'agit d'un numéro. */
  virtual int genericMarkOffset() const;
  /** Saisie l'offset sur le repère de généricité lorqu'il s'agit d'un numéro. */
  virtual void setGenericMarkOffset(const int val);

  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(bool val);

  virtual void setTitle ( const QString & );

  /** @return la valeur courrante. */
  virtual bool value() const;
  /** Fixe la valeur courante à \a value. */
  virtual void setValue(bool value);

  /** Utilise l'aide ou pas de saisie (valeur constructeur). */
  void setInputHelp(bool input=false) { mInputHelp = input; }
  /** @return si \a true utilise l'aide de saisie (valeur constructeur) sinon l'aide d'affichage. */
  bool inputHelp() const { return mInputHelp; }

  /** Affiche ou non l'aide en bulle. */
  void setShowToolTip(bool show=false) { mShowToolTip = show; }
  /** @return si \a true l'affichage de l'aide en bulle est actif. */
  bool showToolTip() const { return mShowToolTip; }

  /** Saisir \a true pour l'inhiber en cas d'accès protégé*/
  void setProtectedAccess(const bool val) { mProtectedAccess = val; }
  /** @return \a true s'il est inhibé en accès protégé */
  bool protectedAccess() const { return mProtectedAccess; }

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

  virtual void moveInsideParent(int x, int y);
  virtual void setEditPositions( bool val );

public slots: // Public slots
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  virtual void refreshPosition();
  virtual void updatePosition();

  /** Configure le widget en fonction de la donnée à laquelle il est lié. */
  virtual void linkData();

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

signals:
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis à chaque déplacement à la souris afin de définir un ordre dans la pile de widget du parent. */
  void movedInsideParent();

protected: // Protected methods
  /** Initialise la palette de couleurs. */
  virtual void initPalette();
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
     Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);

  virtual void showEvent(QShowEvent *e);
  virtual void mousePressEvent ( QMouseEvent * e );
  virtual void mouseMoveEvent ( QMouseEvent * e );

protected slots: // Protected slots
  void stateChanged( bool checked );

protected: // Protected attributes
  /** Si \a true utilise l'aide de saisie (valeur constructeur) sinon l'aide d'affichage. */
  bool mInputHelp;

private: // Private methods
  void init();

  /** Vaut \a true s'il faut y afficher l'étiquette de la donnée. */
  bool mShowLabel;
  /** Vaut \a true s'il faut y afficher la valeur de la donnée. */
  bool mShowValue;

  /** Mode d'affichage du groupe. */
  GroupSection mShowGroup;

  /** Protection en accès protégé */
  bool mProtectedAccess;

  bool mShowToolTip;

  QString mPrefix;
};

#endif
