/***************************************************************************
                          cyboxdo.h  -  description
                             -------------------
    début                  : Sun Feb 23 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYBOXDO_H
#define CYBOXDO_H

// CYLIBS
#include "cydo.h"
#include "cyboxdig.h"
#include "cyled.h"

class CYCheckBox;

/** CYBoxDO est le widget représentant une sortie TOR.
  * @short Widget de sortie TOR.
  * @author Gérald LE CLEACH
  */

class CYBoxDO : public CYBoxDIG
{
  Q_OBJECT

public:
  /** Construit un widget représentant une sortie TOR.
    * @param parent  Widget parent.
    * @param name    Nom du widget.
    * @param c       Couleur de la voie.
    * @param m       Mode d'utilisation du widget. */
  CYBoxDO(QWidget *parent=0, const QString &name=0, QColor c=QColor(Qt::red), Mode m=View);
  /** Destructeur. */
  ~CYBoxDO();

  /** Initialise l'objet. */
  virtual void init();
  /** Place la donnée dans l'objet
    * @param data Donnée à traîter. */
  void setData(CYDO *data);

public slots: // Public slots
  /** Met à jour l'état de la CheckBox. */
  virtual void refresh();
  /** Met à jour l'état de la LED en mode forçage. */
  void refreshForcing();
  /** Applique la nouvelle valeur de la sortie si il y a inversion. */
  void setValue();
  /** Simulation: permute l'état de la led. */
  virtual void simulation();
  /** Gestion de la réception du focus. */
  virtual void focusIn();
  /** Gestion de la perte du focus. */
  virtual void focusOut();

protected: // Protected methods
  /** Met à jour les widgets. */
  virtual void update();
  /** Gestion de la réception du focus. */
  virtual void focusInEvent(QFocusEvent *);
  /** Gestion de la perte du focus. */
  virtual void focusOutEvent(QFocusEvent *);

protected: // Protected attributes
  /** Donnée connectée. */
  CYDO *mData;
  /** Etiquette. */
  QLabel *mLabel;
  /** Led de visu. */
  CYLed *mLed;
  /** Case à cocher de contrôle (mode forçage). */
  CYCheckBox *mCheck;
};

#endif
