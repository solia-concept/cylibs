//
// C++ Implementation: cypendialog
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

// CYLIBS
#include "cydialog.h"
#include "cypendialog.h"
#include "cypen.h"
#include "cypeninput.h"
#include "ui_cypendialog.h"

CYPenDialog::CYPenDialog(QWidget *parent, const QString &name)
 : CYDialog(parent, name), ui(new Ui::CYPenDialog)
{
  ui->setupUi(this);
}


CYPenDialog::~CYPenDialog()
{
  delete ui;
}


/*! 
    \fn CYPenDialog::setPen( CYPen *pen )
 */
void CYPenDialog::setPen( CYPen *pen )
{
  setGenericMark( pen->objectName().toUtf8() );
  setLocaleDB( pen->db() );
//  penInput->setPen( pen );
}
