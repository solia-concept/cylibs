/***************************************************************************
                          cycombobox.cpp  -  description
                             -------------------
    début                  : lun avr 28 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
  
                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX
                 
 ***************************************************************************/

#include "cycombobox.h"

// CYLIBS
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cyword.h"
#include "cybool.h"
#include "cystring.h"
#include "cydialog.h"
#include "cywidget.h"
#include "cyframe.h"
#include "cycore.h"
#include "cyreadonlyfilter.h"

CYComboBox::CYComboBox(QWidget *parent, const QString &name)
  : QComboBox(parent),
    CYDataWdg(this)
{
  setObjectName(name);
  init();
}

CYComboBox::CYComboBox(bool rw, QWidget *parent, const QString &name)
  : QComboBox(parent),
    CYDataWdg(this)
{
  setEditable(rw);
  setObjectName(name);
  init();
}

void CYComboBox::init()
{
  mWriteData    = true;
  mInputHelp    = true;
  mReadOnly     = false;
  mAutoRefresh  = false;
  mEmitModifie  = true;
  mOffset       = 0;
  
  mPalette = palette();
  setFocusPolicy(Qt::WheelFocus);

  connect(this, SIGNAL(activated(int)), SLOT(setActivated(int)));
  connect(this, SIGNAL(activated(const QString &)), SLOT(setActivated(const QString &)));
  connect(this, SIGNAL(editTextChanged(const QString &)), SLOT(setTextChanged(const QString &)));
}

void CYComboBox::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYComboBox::dataName() const
{
  return CYDataWdg::dataName();
}

void CYComboBox::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYComboBox::flagName() const
{
  return CYDataWdg::flagName();
}

void CYComboBox::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYComboBox::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYComboBox::setInverseFlag(bool inverse)
{
  CYDataWdg::setInverseFlag(inverse);
}

bool CYComboBox::inverseFlag() const
{
  return CYDataWdg::inverseFlag();
}

void CYComboBox::setInverseHideFlag(bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYComboBox::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

bool CYComboBox::ctrlFlag()
{
  if (mFlag==0)
    return true;
  
  if (!((QWidget *)parent())->isEnabled())
    return true;
  
  bool res;
  
  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;
  
  setEnabled(res);
  return res;
}

void CYComboBox::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;
  
  bool res;
  
  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;
  
  if (res)
  {
    hide();
  }
  else
  {
    show();
  }
}

void CYComboBox::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYComboBox::benchType() const
{
  return CYDataWdg::benchType();
}

int CYComboBox::value() const
{
  if (mCorrelationMap.isEmpty())
    return currentIndex()+mOffset;
  return(mCorrelationMap[currentIndex()]);
}

QString CYComboBox::valueTxt() const
{
  return currentText();
}

void CYComboBox::setValue(int value)
{
  if (mCorrelationMap.isEmpty())
  {
    if ((value-mOffset)<count())
      QComboBox::setCurrentIndex(value-mOffset);
  }
  else
  {
    QMapIterator<int, int> it(mCorrelationMap);
    while (it.hasNext())
    {
      it.next();                   // must come first
      if ((it.value())==value)
      {
        QComboBox::setCurrentIndex(it.key());
        return;
      }
    }
  }
}

void CYComboBox::setValue(QString value)
{
  QComboBox::setCurrentText(value);
}

void CYComboBox::setCurrentIndex( int index )
{
  QComboBox::setCurrentIndex( index );
  emit currentItemChanged( index );
}

void CYComboBox::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYComboBox::linkData()
{
  ctrlHideFlag();
  
  if (!hasData())
    return;
  
  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }
  
  mEmitModifie = false;
  
  if ( mData->choiceDict.count()>0 )
  {
    clear();
    mCorrelationMap.clear();
    int id=0;
    QMapIterator<int, CYDataChoice*> it1(mData->choiceDict);
    while (it1.hasNext())
    {
      it1.next();
      // insertion des valeurs négatives
      if (it1.key()>=0)
        continue;
      CYDataChoice * choice =it1.value();
      QString label = core->designer() ? QString("%1: %2").arg(it1.key()).arg(choice->label) : choice->label;
      mCorrelationMap.insert(id, choice->value);
      if ( !choice->pixmap.isNull() && !choice->label.isNull() )
      {
        insertItem(id, QIcon(choice->pixmap), label);
      }
      else if ( !choice->pixmap.isNull() )
      {
        insertItem(id, QIcon(choice->pixmap), "");
      }
      else if ( !choice->label.isNull() )
      {
        insertItem(id, label);
      }
      id++;
    }
    
    // insertion des valeurs positives
    QMapIterator<int, CYDataChoice*> it2(mData->choiceDict);
    it2.toFront();
    while (it2.hasNext())
    {
      it2.next();
      if (it2.key()<0)
        continue;
      CYDataChoice * choice =it2.value();
      if (!choice)
        continue;
      QString label = core->designer() ? QString("%1: %2").arg(it2.key()).arg(choice->label) : choice->label;

      mCorrelationMap.insert(id, choice->value);
      if ( !choice->pixmap.isNull() && !choice->label.isNull() )
      {
        insertItem(id, QIcon(choice->pixmap), label);
      }
      else if ( !choice->pixmap.isNull() )
      {
        insertItem(id, QIcon(choice->pixmap), "");
      }
      else if ( !choice->label.isNull())
      {
        insertItem(id, label);
      }
      id++;
    }
  }

  int max=0;
  switch (mData->type())
  {
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      max = (int)data->max() + mOffset;
      setValue((int)data->val());
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mData;
      max = (int)USHRT_MAX;
      setValue((int)data->val());
      break;
    }
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mData;
      max = (int)2;
      setValue((int)data->val());
      break;
    }
    case Cy::String :
    {
      CYString *data = (CYString *)mData;
      setValue(data->val());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  bool readOnly = mReadOnly;
  setReadOnly(false);
  mPalette.setColor(QPalette::Active, QPalette::Text, mData->inputColor());
  QComboBox::setPalette(mPalette);
  setReadOnly(readOnly);
  
  if (mInputHelp)
  {
    this->setToolTip(QString(mData->inputHelp(false)+"<br>"+mData->infoCY()));
  }
  else
  {
    this->setToolTip(QString(mData->displayHelp(false)+"<br>"+mData->infoCY()));
  }

  mEmitModifie = true;
}

void CYComboBox::refresh()
{
  ctrlHideFlag();
  
  if (!hasData())
    return;
  
  if (!readOnly())
    return;
  
  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }
  
  switch (mData->type())
  {
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::String :
    {
      CYString *data = (CYString *)mData;
      setValue(data->val());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYComboBox::designer()
{
  if (!hasData())
    return;
  
  if (!isVisible())
    return;
  
  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }
  
  switch (mData->type())
  {
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      setValue((int)data->def());
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mData;
      setValue((int)data->val());
      break;
    }
    case Cy::String :
    {
      CYString *data = (CYString *)mData;
      setValue(data->val());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYComboBox::update()
{
  if (!isVisible() && mUpdateIfVisible)
    return;
  
  if (!hasData())
    return;
  
  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }
  
  if (!isEnabled())
    return;

  if (isHidden())
    return;

  mEmitModifie = false;
  
  switch (mData->type())
  {
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      data->setVal(value());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      data->setVal(value());
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mData;
      data->setVal(value());
      break;
    }
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mData;
      data->setVal(value());
      break;
    }
    case Cy::String :
    {
      CYString *data = (CYString *)mData;
      data->setVal(valueTxt());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
  mEmitModifie = true;
}

void CYComboBox::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }


  ctrlFlag();
}

void CYComboBox::changeEvent(QEvent* event)
{
  if (event && (event->type()==QEvent::EnabledChange))
  {
    // cet événement est envoyé, si l'état actif change
    bool state = isEnabled();
    if (!readOnly() || !state)
      mEnabled = state;
  }
  QComboBox::changeEvent(event);
}

void CYComboBox::keyPressEvent(QKeyEvent *e)
{
  QComboBox::keyPressEvent(e);
  
  switch (e->key())
  {
    case Qt::Key_Return :
    case Qt::Key_Enter  : update();
      emit applying();
      break;
  }
}

void CYComboBox::setTextChanged(const QString &)
{
  if ((mEmitModifie) && isVisible())
    emit modifie();
}


void CYComboBox::setActivated(int index)
{
  if ((mEmitModifie) && isVisible())
  {
    emit modifie();
    setFocus();
  }
  emit activatedWithOffset(index+mOffset);
  if (mData)
  {
    int val=value();
    mData->setTmp(val);
    emit valueChanged(val);
  }
}

void CYComboBox::setActivated(const QString &)
{
  if ((mEmitModifie) && isVisible())
    emit modifie();
}

void CYComboBox::focusInEvent(QFocusEvent *e)
{
  QComboBox::focusInEvent(e);
  emit focusIn();
  if (mMovieStepFocusIn!=-1)
    emit focusIn(mMovieStepFocusIn);
}

void CYComboBox::focusOutEvent(QFocusEvent *e)
{
  QComboBox::focusOutEvent(e);
  emit focusOut();
  // TOCHECK QT4
  //  if ((mMovieStepFocusOut!=-1) && !view()->listBox()->hasFocus())
  if ((mMovieStepFocusOut!=-1) && !hasFocus())
    emit focusOut(mMovieStepFocusOut);
}

void CYComboBox::enterEvent(QEvent *e)
{
  QComboBox::enterEvent(e);
  emit enteringEvent();
  //   setFocus(); a_voir
}

void CYComboBox::leaveEvent ( QEvent *e )
{
  QComboBox::leaveEvent(e);
  emit leavingEvent();
}

void CYComboBox::setBackgroundColor(const QColor &color)
{
  QPalette palette;
  mBackgroundColor=color;
  palette.setColor(backgroundRole(), color);
  setPalette(palette);
}

void CYComboBox::setForegroundColor(const QColor &color)
{
  QPalette palette;
  mForegroundColor=color;
  palette.setColor(QPalette::WindowText, color);
  setPalette(palette);
}

void CYComboBox::setPalette(const QPalette &palette)
{
  QComboBox::setPalette(palette);
}
