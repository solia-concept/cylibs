//
// C++ Implementation: cyslider
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyslider.h"

// QT
#include <qapplication.h>
//Added by qt3to4:
#include <QString>
#include <QShowEvent>
// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cymessagebox.h"

CYSlider::CYSlider(QWidget *parent, const QString &name)
  : QwtSlider(parent),
    CYDataWdg(this)
{
  setObjectName(name);

  mAutoRefresh = false;
  mWriteData = true;
  mDirect    = true;
  connect(this, SIGNAL(valueChanged(double)), this, SLOT(setValue(double)));
}

CYSlider::~CYSlider()
{
}

void CYSlider::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYSlider::dataName() const
{
  return CYDataWdg::dataName();
}

void CYSlider::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYSlider::flagName() const
{
  return CYDataWdg::flagName();
}

void CYSlider::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYSlider::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYSlider::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYSlider::benchType() const
{
  return CYDataWdg::benchType();
}

void CYSlider::setInverseFlag(bool inverse)
{
  mInverseFlag = inverse;
  ctrlFlag();
}

bool CYSlider::inverseFlag() const
{
  return mInverseFlag;
}

void CYSlider::setInverseHideFlag(const bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYSlider::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

bool CYSlider::readOnly() const
{
  return mReadOnly;
}

void CYSlider::setReadOnly(const bool val)
{
  mReadOnly = val;
  QwtSlider::setReadOnly(val);
  ctrlFlag();
}

double CYSlider::value() const
{
  return QwtSlider::value();
}

void CYSlider::setValue(double value)
{
  QwtSlider::setValue(value);

  if (mDirect)
  {
    update();
    if (mData && isEnabled() && isVisible())
      mData->writeData();
  }
  else if (mEmitModifie && isVisible())
    emit modifie();

  if (mData)
  {
    double val=CYSlider::value();
    mData->setTmp(val);
  }
}

//double CYSlider::minValue() const
//{
//  return d->mapToDouble(QSpinBox::minValue());
//}
//
//void CYSlider::setMinValue(double value)
//{
//  bool ok = false;
//  int min = d->mapToInt(value, &ok);
//  if (!ok)
//    return;
//  QSpinBox::setMinValue(min);
//  updateValidator();
//}
//
//double CYSlider::maxValue() const
//{
//  return d->mapToDouble(QSpinBox::maxValue());
//}
//
//void CYSlider::setMaxValue(double value)
//{
//  bool ok = false;
//  int max = d->mapToInt(value, &ok);
//  if (!ok)
//    return;
//  QSpinBox::setMaxValue(max);
//  updateValidator();
//}
//
//double CYSlider::lineStep() const
//{
//  return d->mapToDouble(QSpinBox::lineStep());
//}
//
//void CYSlider::setLineStep(double step)
//{
//  bool ok = false;
//
//  if (!mStepDataName.isEmpty())
//  {
//    CYF64 *d = (CYF64 *)core->findData(mStepDataName);
//    step = d->val();
//  }
//
//  if (step > (maxValue() - minValue()))
//    QSpinBox::setLineStep(1);
//  else
//    QSpinBox::setLineStep(kMax(d->mapToInt(step, &ok), 1));
//}

void CYSlider::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYSlider::linkData()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
  {
    setEnabled(false);
    return;
  }

  mEmitModifie = false;

  if (!mStepDataName.isEmpty())
  {
    CYF64 *step = (CYF64 *)core->findData(mStepDataName);
    // TOCHECK QWT
    if (step)
      setSingleSteps(step->val());
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8 *data = (CYU8  *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      data->setTmp(data->CYU32::val());
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue(data->val());
                      rescale(data->min(), data->max(), singleSteps());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));

  mEmitModifie = true;
}

void CYSlider::refresh()
{
  ctrlHideFlag();

  if (!ctrlFlag())
    return;

  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    return;
  }
  if (!mData->isOk() && !mAlwaysOk)
  {
    setEnabled(false);
    return;
  }
  if (mData->flag()!=0)
  {
    if (!mData->flag()->val())
    {
      setEnabled(false);
      return;
    }
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8 *data = (CYU8  *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      data->setTmp(data->CYU32::val());
                      setValue(data->def());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue(data->def());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYSlider::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    QwtSlider::setEnabled(false);
    return;
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8 *data = (CYU8  *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      data->setTmp(data->CYU32::val());
                      setValue(data->def());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue(data->def());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYSlider::update()
{
  if (!hasData())
    return;

  if (!isVisible() && mUpdateIfVisible)
    return;

  if ((mData==0) || (!isEnabled()))
    return;

  if (isHidden())
    return;

  mEmitModifie = false;

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      data->setVal((flg)value());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      data->setVal((s8)value());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      data->setVal((s16)value());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      data->setVal((s32)value());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      data->setVal((s64)value());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      data->setVal((u8)value());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      data->setVal((u8)value());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      data->setVal((u32)value());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      data->setVal((u64)value());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      data->setValSection((int)value());
                      data->setVal((u32)data->tmp());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      data->setVal(value());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  mEmitModifie = true;
}

bool CYSlider::ctrlFlag()
{
  if (mFlag==0)
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;

  if (!res)
    designer();

  setEnabled(res);
  return res;
}

void CYSlider::showEvent(QShowEvent *e)
{
  QwtSlider::showEvent(e);
}

void CYSlider::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}
