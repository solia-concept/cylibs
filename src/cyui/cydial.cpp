/***************************************************************************
                          cydial.cpp  -  description
                             -------------------
    begin                : dim nov 23 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydial.h"

// QT
#include <QPainter>
// QWT
#include "qwt_dial.h"
#include "qwt_dial_needle.h"
#include "qwt_round_scale_draw.h"
// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyword.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cynuminput.h"
#include "cydialscaledialog.h"

CYDial::CYDial(QWidget *parent, const QString &name)
  : QwtDial(parent),
    CYDataWdg(this)
{
  setObjectName(name);

  mDB = new CYDB(this, "db");
  init();
}

CYDial::CYDial(CYDB *db, QWidget *parent, const QString &name)
  : QwtDial(parent),
    CYDataWdg(this)
{
  setObjectName(name);

  mDB = db;
  init();
}

void CYDial::init()
{
  mNbDec = 2;
  int offset = (core) ? core->offsetFontSize() : 0;
  mFontSizeLabel = 12+offset;
  mDB->setHelpDesignerValue(false);

  setWrapping(false);
  QwtDial::setReadOnly(true);

  setOrigin(135.0);
  setScaleArc(0.0, 270.0);

  QwtDialSimpleNeedle *needle = new QwtDialSimpleNeedle(QwtDialSimpleNeedle::Arrow, true, Qt::red, QColor(Qt::gray).lighter(130));
  setNeedle(needle);

  mScaleMin = new CYF64(mDB, "SCALE_MIN", new f64);
  mScaleMin->setLabel(tr("Minimum value"));
  // TOCHECK QT4
  // mScaleMin->setVal(minValue());
  mScaleMin->setVal(minimum());

  mScaleMax = new CYF64(mDB, "SCALE_MAX", new f64);
  mScaleMax->setLabel(tr("Maximum value"));
  // TOCHECK QT4
  // mScaleMax->setVal(maxValue());
  mScaleMax->setVal(maximum());

  mScaleStep = new CYF64(mDB, "SCALE_STEP", new f64);
  mScaleStep->setLabel(tr("Scale step"));
  // TOCHECK QT4
  // mScaleStep->setVal((maxValue()-minValue())/15);
  mScaleStep->setVal((maximum()-minimum())/15);
  mScaleStep->setMin(0.0);

  mMaxMajIntv = new CYS32(mDB, "MAX_MAJ_INTV", new s32);
  mMaxMajIntv->setVal(5);

  mMaxMinIntv = new CYS32(mDB, "MAX_MIN_INTV", new s32);
  mMaxMinIntv->setVal(15);

  // TOCHECK QT4
//  setScale(mMaxMajIntv->val(), mMaxMinIntv->val(), mScaleStep->val());
//  setScaleOptions(ScaleTicks | ScaleLabel);
//  setScaleTicks(2, 6, 8);
//  scaleDraw()->setPenWidth(3);
  rescale(mMaxMajIntv->val(), mMaxMinIntv->val(), mScaleStep->val());
  scaleDraw()->enableComponent(QwtAbstractScaleDraw::Ticks, true);
  scaleDraw()->enableComponent(QwtAbstractScaleDraw::Labels, true);
  scaleDraw()->setTickLength(QwtScaleDiv::MinorTick, 2);
  scaleDraw()->setTickLength(QwtScaleDiv::MediumTick, 6);
  scaleDraw()->setTickLength(QwtScaleDiv::MajorTick, 8);
  scaleDraw()->setPenWidth(3);
  setLineWidth(4);
  setFrameShadow(QwtDial::Sunken);
  setMinimumSize(200, 200);
}

CYDial::~CYDial()
{
}

void CYDial::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYDial::dataName() const
{
  return CYDataWdg::dataName();
}

void CYDial::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYDial::flagName() const
{
  return CYDataWdg::flagName();
}

void CYDial::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYDial::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYDial::setInverseFlag(bool inverse)
{
  CYDataWdg::setInverseFlag(inverse);
}

bool CYDial::inverseFlag() const
{
  return CYDataWdg::inverseFlag();
}

void CYDial::setInverseHideFlag(bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYDial::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

bool CYDial::ctrlFlag()
{
  if (mFlag==0)
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;

  setEnabled(res);
  return res;
}

void CYDial::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYDial::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYDial::benchType() const
{
  return CYDataWdg::benchType();
}

void CYDial::setLabel(const QString &label)
{
  mLabel = label;
  QwtDial::update();
}

QString CYDial::label() const
{
  return mLabel;
}

void CYDial::setData(CYData *data)
{
  mData = data;
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  linkData();
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}

double CYDial::value()
{
  return QwtDial::value();
}

void CYDial::setValue(double val)
{
  setLabel(QString("%1 %2").arg(val, 0, 'g', mNbDec).arg(mUnit));
  QwtDial::setValue(val);
}

void CYDial::setValue(QString string)
{
  double val = string.section(' ', 0, 0).toDouble();
  string.replace(' ','\n');
  setLabel(string);
  QwtDial::setValue(val);
}

void CYDial::linkData()
{
  ctrlHideFlag();

  CYDataWdg::linkData();

  if (!hasData() || !mData)
    return;

  mUnit  = mData->unit();
  mNbDec = mData->nbDec();

  switch (mData->type())
  {
    case Cy::Bool   :
                    {
                      CYBool  *data = (CYBool  *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
//                      setRange(0.0, 1.0);
                      setScale(0.0, 1.0);
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      setValue(data->CYU32::val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue(data->val());
                      // TOCHECK QT4
                      setScale((double)data->min(), (double)data->max());
                      this->setToolTip(data->displayHelp()+"<br>"+data->infoCY());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      return;
  }
  bool newFormat = ( mScaleMin->isFromFormat( mData->format() ) ) ? false : true;

  mScaleMin->setFormat(mData->format());
  mScaleMin->setAutoMinMaxFormat( false );
  mScaleMin->initMinMax();
  // TOCHECK QT4
  mScaleMin->setDef(minimum());

  mScaleMax->setFormat(mData->format());
  mScaleMax->setAutoMinMaxFormat( false );
  mScaleMax->initMinMax();
  // TOCHECK QT4
  mScaleMax->setDef(maximum());

  mScaleStep->setFormat(mData->format());
  mScaleStep->setAutoMinMaxFormat( false );
  mScaleStep->initMinMax();
  // TOCHECK QT4
  mScaleStep->setDef((maximum()-minimum())/10);

  if ( newFormat )
  {
    mScaleMin->designer();
    mScaleMax->designer();
    mScaleStep->designer();
  }

  mMaxMajIntv->setFormat(mData->format());
  mMaxMajIntv->setVal((int)mScaleStep->val()/50);

  mMaxMinIntv->setFormat(mData->format());
  mMaxMinIntv->setVal((int)mScaleStep->val()/10);

  updateScale();
}

void CYDial::refresh()
{
  if (parent() && parent()->inherits("CYMultimeter"))
    return;
  if (parent() && parent()->parent() && parent()->parent()->inherits("CYMultimeter"))
    return;

  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
    return;
  if (!isVisible())
    return;

  if (!ctrlFlag())
  {
    setEnabled(false);
    return;
  }
  if (mData==0)
    return;
  if (!mData->isOk())
  {
//    setEnabled(false);
    return;
  }
  if (mData->flag()!=0)
  {
    if (!mData->flag()->val())
    {
      setEnabled(false);
      return;
    }
  }

  switch (mData->type())
  {
    case Cy::Bool   :
                    {
                      CYBool  *data = (CYBool *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue(data->val());
                      return;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      setValue(data->CYU32::val());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue(data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      break;
  }
}

void CYDial::drawScaleContents(QPainter *painter, const QPointF &center, double radius) const
{
  QRectF rect(0, 0, 2 * radius, 2 * radius - 10);
  rect.moveCenter(center);

  painter->setBrush(palette().brush(QPalette::Text));
  QFont font = painter->font();
  font.setPointSize( mFontSizeLabel );
  painter->setFont(font);

  const int flags = Qt::AlignBottom | Qt::AlignHCenter;
  painter->drawText(rect, flags, mLabel);
}

void CYDial::mousePressEvent(QMouseEvent *e)
{
  if ( (e->button() == Qt::LeftButton) )
  {
    CYDialScaleDialog *dlg = new CYDialScaleDialog(this, "CYDialScaleDialog");
    dlg->setLocaleDB(mDB);
    dlg->setData(mScaleMin, mScaleMax, mScaleStep);
    connect(dlg, SIGNAL(validate()), SLOT(updateScale()));
    connect(dlg, SIGNAL(validate()), SIGNAL(validate()));
    dlg->exec();
  }
}

//void CYDial::resizeEvent(QResizeEvent *e)
//{
////  double f    = (double)((size().width() + size().height()))/(sizeHint().width() + sizeHint().height());
//////  double step    = (1/(size().width() + size().height()))*((minValue()-maxValue())/15);
////  qDebug(QString("%1").arg(f));
////  double step    = (minValue()-maxValue())/(15*f);
////  int maxMajIntv = (int)step/10;
////  int minMajIntv = (int)step/20;
////  setScale(minMajIntv, maxMajIntv, step);
//  QwtDial::resizeEvent(e);
//}

void CYDial::updateScale()
{
  setScaleMaxMinor(mMaxMajIntv->val());
  setScaleMaxMinor(mMaxMinIntv->val());
  setScaleStepSize(mScaleStep->val());
  setScale(mScaleMin->val(), mScaleMax->val());
  repaint();
}


/*! Change les couleurs
    \fn CYDial::setColors( QColor needle, QColor background, QColor text)
 */
void CYDial::setColors( QColor needle, QColor background, QColor text)
{
  QPalette pal = CYDial::needle()->palette();
  for ( int i = 0; i < QPalette::NColorGroups; i++ )
  {
    pal.setColor((QPalette::ColorGroup)i, QPalette::Mid , needle);

  }
  CYDial::needle()->setPalette( pal );

  setPalette( pal );
  for ( int i = 0; i < QPalette::NColorGroups; i++ )
  {
    pal.setColor((QPalette::ColorGroup)i, QPalette::Base, background);
    pal.setColor((QPalette::ColorGroup)i, QPalette::Foreground, background);
    pal.setColor((QPalette::ColorGroup)i, QPalette::Text, text);
  }
  setPalette( pal );
}
