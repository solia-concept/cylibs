/***************************************************************************
                          cypixmapview.cpp  -  description
                             -------------------
    begin                : jeu oct 9 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cypixmapview.h"

// QT
#include <QDir>
// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyword.h"
#include "cyflag.h"
#include "cydo.h"
#include "cyao.h"

CYPixmapView::CYPixmapView(QWidget *parent, const QString &name)
  : CYLabel(parent, name)
{
  init(false);
}

CYPixmapView::~CYPixmapView()
{
}

void CYPixmapView::init(bool value)
{
  mValue = value;
  setValue(!mValue);
  mOrientation = Qt::Horizontal;
  mMirror = false;
  mRotate = 0;
  mInit   = false;
  mInitForcing = false;
  setScaledContents(true);
}

void CYPixmapView::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYPixmapView::dataName() const
{
  return CYDataWdg::dataName();
}

void CYPixmapView::setPixmapFileTrue(const QByteArray &file)
{
  if (file.startsWith(":/"))
  {
    mPixmapFileTrue = QString(file);
  }
  else
  {
    mPixmapFileTrue = findResource(QString(file));

    QDir d(mPixmapFileTrue);
    if (d.exists())
      return;

    QFile f(mPixmapFileTrue);
    if (!f.exists())
      return;
  }

  if (!pixmap(Qt::ReturnByValue).isNull())
    clear();

  setScaledContents(true);

  setPixmap(QPixmap(mPixmapFileTrue));

  if (!pixmap(Qt::ReturnByValue).isNull())
  {
    newPixmap = true;
    setRotate(rotate());
  }
  else
    setScaledContents(false);
}


void CYPixmapView::setPixmapFileFalse(const QByteArray &file)
{
  if (file.startsWith(":/"))
  {
    mPixmapFileFalse = file;
  }
  else
  {
    mPixmapFileFalse = findResource(file);

    QDir d(mPixmapFileFalse);
    if (d.exists())
      return;

    QFile f(mPixmapFileFalse);
    if (!f.exists())
      return;
  }

  if (!pixmap(Qt::ReturnByValue).isNull())
    clear();

  setScaledContents(true);

  setPixmap(QPixmap(mPixmapFileFalse));

  if (!pixmap(Qt::ReturnByValue).isNull())
  {
    newPixmap = true;
    setRotate(rotate());
  }
  else
    setScaledContents(false);
}


void CYPixmapView::setForcingMode(bool val)
{
  mForcingMode = val;
}

void CYPixmapView::initForcingMode()
{
  if (!mForcingMode)
    return;

  if (!mForcingData)
    return;

  if (mInitForcing)
    return;

  mForcingValue = false;
  mDefMargin = margin();
  setMargin(2);
  QPalette pal = palette();
  mBackgroundColor = pal.color(QPalette::Button);
  pal.setColor(QPalette::Button, mBackgroundColor.darker(110));
  setPalette(pal);
  setFocusPolicy(Qt::StrongFocus);

  setFrameShape(QFrame::WinPanel);
  setFrameShadow(QFrame::Raised);


  initForceValue();
  mInitForcing = true;
  //  emit forcingDB(mForcingData->db());
  //  else
  //  {
  //    setMargin(mDefMargin);
  //    setBackgroundColor(mBackgroundColor);
  //    setFocusPolicy(QWidget::NoFocus);
  //    setFrameShape(QFrame::NoFrame);
  //    setFrameShadow(QFrame::Plain);
  //  }
}

bool CYPixmapView::value()
{
  return mValue;
}

void CYPixmapView::setValue(bool val)
{
  if ((mInit) && (mValue == val))
    return;

  mValue = val;

  if (mSettingMode)
    mSettingValue = mValue;

  if (mValue)
    setState(true);
  else
    setState(false);
}

void CYPixmapView::setState(bool value)
{
  if (mPixmapFileTrue.isEmpty() && mPixmapFileFalse.isEmpty())
    return;

  hide();
  setScaledContents(true);
  if (value)
    setPixmap(QPixmap(QString(mPixmapFileTrue)));
  else
    setPixmap(QPixmap(QString(mPixmapFileFalse)));
  if (!pixmap(Qt::ReturnByValue).isNull())
    setScaledContents(true);
  else
    setScaledContents(false);
  newPixmap = true;
  CYLabel::setState(value);
  show();
  ctrlHideFlag();
}

void CYPixmapView::linkData()
{
  ctrlHideFlag();

  /*  if (!hasData() && (!hasSettingData() && mSettingMode))*/
  if ((!hasSettingData() && mSettingMode) && !hasData())
    return;

  CYData *d=(hasSettingData() && mSettingMode) ? settingData() : mData;

  if (d==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  this->setToolTip(QString(d->displayHelp()+"<br>"+d->infoCY()));

  switch (d->type())
  {
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)d;
      setValue(data->val());
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)d;
      setValue(data->val());
      break;
    }
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)d;
      setValue(data->val());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)d;
      setValue(data->val());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)d;
      setValue(data->val());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)d;
      setValue(data->val());
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)d;
      setValue(data->val());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)d;
      setValue(data->val());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)d;
      setValue(data->val());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)d;
      setValue(data->val());
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)d;
      setValue(data->val());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)d;
      setValue(data->val());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)d;
      setValue(data->val());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+d->objectName());
  }
  initSettingMode();
}

void CYPixmapView::refresh()
{
  ctrlHideFlag();

  refresh2();

  if (!ctrlFlag())
    return;

  if (mSettingMode && hasSettingData())
    return;

  if (!hasData())
    return;

  CYData *d=mData;

  if (d==0)
  {
    setEnabled(false);
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  setEnabled(true);
  if (!d->isOk() && !mAlwaysOk)
  {
    setEnabled(false);
    return;
  }

  if (!core->simulation() && (d->flag()!=0))
  {
    if (!d->flag()->val())
    {
      setEnabled(false);
      return;
    }
  }

  if (!mInit)
    mInit = true;

  switch (d->type())
  {
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)d;
      setValue(data->val());
      return;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)d;
      setValue(data->val());
      return;
    }
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)d;
      setValue(data->val());
      return;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)d;
      setValue(data->val());
      return;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)d;
      setValue(data->val());
      return;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)d;
      setValue(data->val());
      return;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)d;
      setValue(data->val());
      return;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)d;
      setValue(data->val());
      return;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)d;
      setValue(data->val());
      return;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)d;
      setValue(data->val());
      return;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)d;
      setValue(data->val());
      return;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)d;
      setValue(data->val());
      return;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)d;
      setValue(data->val());
      return;
    }
    default         : CYWARNINGTEXT(QString("void CYPixmapView::refresh(): %1 de %2 "
                                            "=> type de la donnée %3 incorrecte")
                                    .arg(objectName())
                                    .arg(parent()->objectName())
                                    .arg(d->objectName()));
      break;
  }
}

void CYPixmapView::update()
{
  CYLabel::update();

  if (!mInitForcing && !mSettingMode)
    return;

  if (!hasForcingData() && (!hasSettingData() && mSettingMode))
    return;

  CYData *d=(hasSettingData() && mSettingMode) ? settingData() : mForcingData;

  if (!isVisible() && mUpdateIfVisible)
    return;

  if ((d==0) || (!isEnabled()))
    return;

  if (!core || (core->lockForcing() && (!hasSettingData() || !mSettingMode)))
    return;

  if (isHidden())
    return;

  bool value = (mSettingMode) ? mSettingValue : mForcingValue;
  switch (d->type())
  {
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)d;
      data->setVal(value);
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)d;
      data->setVal(value);
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)d;
      data->setVal(value);
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)d;
      data->setVal(value);
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)d;
      data->setVal(value);
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)d;
      data->setVal(value);
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)d;
      data->setVal(value);
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)d;
      data->setVal(value);
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)d;
      data->setVal(value);
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)d;
      data->setVal(value);
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)d;
      data->setVal(value);
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)d;
      data->setVal(value);
      break;
    }
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)d;
      data->setVal(value);
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+d->objectName());
  }
}

bool CYPixmapView::findForcing(QHash<QString, CYData*> *db)
{
  mForcingData=0;

  bool ret = CYLabel::findForcing(db);

  if (!mData)
    return ret;

  if (!ret && mData->dataForcing())
  {
    setForcingData(mData->dataForcing());
    ret = true;
  }

  if (mForcingData)
  {
    initForcingMode();
    connect(this, SIGNAL(synopticForcingChanged(int)), mForcingData, SIGNAL(synopticForcingChanged(int)));
    connect(mForcingData, SIGNAL(tableForcingChanged(int)), SLOT(tableForcingChanged(int)));
  }
  return ret;
}

void CYPixmapView::toggleForcing()
{
  if (!mForcingMode || !mForcingData)
    return;
  mForcingValue = !mForcingValue;
  emit synopticForcingChanged(mForcingValue);
  refreshForcing();
}

void CYPixmapView::refreshForcing()
{
  if (!mForcingMode)
    return;

  QPalette pal = palette();
  setAutoFillBackground(true);
  if (mForcingValue)
  {
    setFrameShadow(QFrame::Sunken);
    pal.setColor(QPalette::Window, mBackgroundColor.darker(170));
  }
  else
  {
    setFrameShadow(QFrame::Raised);
    pal.setColor(QPalette::Window, mBackgroundColor.darker(110));
  }
  setPalette(pal);
}

void CYPixmapView::toggleSetting()
{
  if (!mSettingMode || !settingData())
    return;
  mSettingValue = !mSettingValue;
  refreshSetting();
}


void CYPixmapView::refreshSetting()
{
  if (!mSettingMode)
    return;

  //   if (mSettingValue!=mValue)
  //   {
  //     setFrameShadow(QFrame::Sunken);
  //     setBackgroundColor(mBackgroundColor.darker(170));
  //   }
  //   else
  //   {
  //     setFrameShadow(QFrame::Raised);
  //     setBackgroundColor(mBackgroundColor.darker(110));
  //   }

  mValue = mSettingValue;
  if (mValue)
    setState(true);
  else
    setState(false);
  /*  setValue(mSettingValue);*/
}


void CYPixmapView::keyPressEvent(QKeyEvent *e)
{
  if (readOnly())
    return;

  switch (e->key())
  {
    case Qt::Key_Enter  :
    case Qt::Key_Return : emit applying();
      break;
    case Qt::Key_Space  : if (mForcingMode)
      {
        toggleForcing();
        emit toggleForcingEvent();
      }
      else if (mSettingMode)
      {
        toggleSetting();
        emit toggleSettingEvent();
      }
      break;
    default         : CYLabel::keyPressEvent(e);
  }
}

void CYPixmapView::mousePressEvent(QMouseEvent * e)
{
  if (e->button() == Qt::LeftButton)
  {
    if (mForcingMode)
    {
      toggleForcing();
      emit toggleForcingEvent();
    }
    else if (mSettingMode)
    {
      toggleSetting();
      emit toggleSettingEvent();
    }
  }
  if (isVisible())
    emit modifie();
  CYLabel::mousePressEvent(e);
}

void CYPixmapView::wheelEvent(QWheelEvent *)
{
  if (!core || core->simulation())
    setState(!state());
}


/*! Réception du signal émis à chaque changement de valeur de forçage à partir de la table de sorties.
    \fn CYFlagInput::tableForcingChanged(int)
 */
void CYPixmapView::tableForcingChanged(int value)
{
  mForcingValue = value;
  refreshForcing();
}


/*! Initialise la valeur de donnée de forçage.
    \fn CYPixmapView::initForceValue()
 */
void CYPixmapView::initForceValue()
{
  if (!mForcingData)
    return;

  if (mData)
    mData->initForcingValue();

  switch (mForcingData->type())
  {
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mForcingData;
      mForcingValue = data->val();
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mForcingData;
      mForcingValue = (bool)data->val();
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mForcingData->objectName());
      break;
  }

  refreshForcing();
}
