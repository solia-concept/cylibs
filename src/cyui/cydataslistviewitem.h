//
// C++ Interface: cydataslistviewitem
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDATASLISTVIEWITEM_H
#define CYDATASLISTVIEWITEM_H

// QT
#include <QTreeWidgetItem>
// CYLIBS
#include "cydataslistview.h"

class CYDB;
class CYPen;
// class CYF64;
class CYDisplayItem;

/**
@short QListViewItem représentant un CYDisplayItem.

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYDatasListViewItem : public QTreeWidgetItem
{
public:
    CYDatasListViewItem(CYDatasListView * parent, CYDisplayItem *di);

    ~CYDatasListViewItem();

    // TODO QT5
//    virtual void paintCell ( QPainter * p, const QColorGroup & cg, int column, int width, int align );
    CYPen *  cypen();
//     CYF64 *  cycoef();
    double coefficient;
    CYDisplayItem * displayItem();

protected:
    CYDB  * mDB;
    CYPen * mPen;
//     CYF64 * mCoef;
    CYDatasListView * mListView;
    CYDisplayItem * mDisplayItem;
};

#endif
