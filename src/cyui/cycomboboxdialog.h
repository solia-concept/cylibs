#ifndef CYCOMBOBOXDIALOG_H
#define CYCOMBOBOXDIALOG_H

// CYLIBS
#include <cydialog.h>

namespace Ui {
    class CYComboBoxDialog;
}


class CYComboBoxDialog : public CYDialog
{
public:
  CYComboBoxDialog(QWidget *parent=0, const QString &name="textDialog");
  ~CYComboBoxDialog();

  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  void hideButtonApply(bool hide=true);

private:
  Ui::CYComboBoxDialog *ui;
};

#endif // CYCOMBOBOXDIALOG_H
