//
// C++ Implementation: cydataslistview
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cydataslistview.h"

// QT
#include <QInputDialog>
#include <QColorDialog>
#include <QPixmap>
#include <QHeaderView>
#include <QMimeData>
#include <QPaintEngine>
// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "cydisplaystyle.h"
#include "cydisplayitem.h"
#include "cypendialog.h"
#include "cydataslistviewitem.h"
#include "cypen.h"
#include "cymessagebox.h"

CYDatasListView::CYDatasListView(QWidget *parent, const QString &name)
 : CYTreeWidget(parent)
{
  setObjectName(name);
  setColumnCount(0);
  mNumColumn      = addColumn( tr( "#" ) );
  mColorColumn    = addColumn( tr( "Color" ) );
  mPenColumn      = addColumn( tr( "Pen" ) );
  mLabelColumn    = addColumn( tr( "Label" ) );
  mGroupColumn    = addColumn( tr( "Group" ) );
  mLinkColumn     = addColumn( tr( "Connection" ) );
  mHostColumn     = addColumn( tr( "Host" ) );
  mUnitColumn     = addColumn( tr( "Unit" ) );
  mStatusColumn   = addColumn( tr( "Status" ) );
  mDataNameColumn = addColumn( tr( "Designer Name" ) );
  mCoefColumn     = addColumn( tr( "Coefficient" ) );
  mOffsetColumn   = addColumn( tr( "Offset" ) );

  mMaxNbDatas = 99;
  mWithColor = true;
  mWithPen   = true;
  mWithCoef  = true;
  mWithOffset = true;
//   setMinimumSize( QSize( 400, 0 ) );
  setAllColumnsShowFocus( true );
  // TODO QT5
//  setItemMargin( 3 );
  setSelectionMode(QAbstractItemView::ExtendedSelection);
  setDragEnabled(true);
  setDragDropMode(QAbstractItemView::InternalMove);
  //setAcceptDrops(true);
  viewport()->setAcceptDrops(true);
  setDropIndicatorShown(true);
  setDragDropOverwriteMode(true);
}


CYDatasListView::~CYDatasListView()
{
}

/*! Gestion du glisser (glisser-déposer)
    \fn CYDatasListView::dragEnterEvent( QDragEnterEvent * ev )
 */
void CYDatasListView::dragEnterEvent( QDragEnterEvent * ev )
{
  // TOCHECK QT5
//  ev->accept(Q3TextDrag::canDecode(ev));
  if (ev->mimeData()->hasFormat("text/plain"))
    ev->accept();
}


/*! Gestion du déposer (glisser-déposer)
    \fn CYDatasListView::dropEvent( QDropEvent * ev )
 */
void CYDatasListView::dropEvent( QDropEvent * ev )
{

  if (ev->mimeData())
  {
    QString txt = ev->mimeData()->text();
    if (txt.isEmpty())
    {
      CYWARNING
      return;
    }

    if (txt.contains(tr("Group")))
      addGroup(txt);
    else
      addData(txt);

    if (isVisible())
      emit modifie();
  }
}


/*! Ajoute une ligne représentant une donnée
    @param pos Position de la donnée. Par défaut à -1, permet un positionnement automatique.
    \fn CYDatasListView::addData(CYDisplayItem *item, int pos=-1)
 */
void CYDatasListView::addData(CYDisplayItem *item, int pos)
{
  if ( mMaxNbDatas==1 )
    clear();

  if (pos<0)
    pos = topLevelItemCount() + 1;

  CYDatasListViewItem *lvi = new CYDatasListViewItem(this, item);
  QString id = posToString(pos);
  if (id.isEmpty())
  {
    takeTopLevelItem(pos);
    return;
  }
  lvi->setText(mNumColumn      , id);
  lvi->setText(mLabelColumn    , item->label);
  lvi->setText(mGroupColumn    , item->group);
  lvi->setText(mLinkColumn     , item->link);
  lvi->setText(mHostColumn     , item->host);
  lvi->setText(mUnitColumn     , item->unit);
  lvi->setText(mStatusColumn   , item->status());
  lvi->setText(mDataNameColumn , item->objectName());
  lvi->setText(mCoefColumn     , QString("%1").arg(item->coefficient));
  lvi->setText(mOffsetColumn   , QString("%1").arg(item->offset));

  int h=12;
  int w=48;
  QImage image( w, h, QImage::Format_ARGB32 );
  QPainter painter( &image );
  painter.fillRect(0,0,w,h,mBackgroundColorPen);
  painter.setPen(item->pen);
  painter.drawLine(0,h/2,w,h/2);
  painter.end();
  QPixmap pm = QPixmap::fromImage(image);
  lvi->cypen()->setPen( item->pen );
  setIconSize(QSize(w,h));
  lvi->setIcon(mColorColumn, QIcon(pm) );
  lvi->setIcon(mPenColumn, QIcon(pm) );

  addTopLevelItem(lvi);

  lvi->coefficient =item->coefficient;
// //   cycoef()->setVal(item->coefficient);

  header()->setSectionResizeMode(QHeaderView::ResizeToContents);
  emit modifie();
}


/*! Saisie la couleur associée à la donnée sélectionnée.
    \fn CYDatasListView::setColor()
 */
void CYDatasListView::setColor()
{
  CYDatasListViewItem * lvi = (CYDatasListViewItem *)currentItem();

  if (!lvi)
    return;

  // TOCHECK QT5
  QColor c = QColorDialog::getColor( lvi->icon( mColorColumn ).pixmap(12,1).toImage().pixel(1, 0) );

  if(c.isValid())
  {
    QPixmap newPm(12, 1);
    newPm.fill(c);
    lvi->setIcon(mColorColumn, QIcon(newPm));
  }

  emit modifie();
}


/*! Saisie le crayon associé à la donnée sélectionnée.
    \fn CYDatasListView::setPen()
 */
void CYDatasListView::setPen()
{
  CYDatasListViewItem *lvi = (CYDatasListViewItem *)currentItem();

  if (!lvi)
    return;

  CYPenDialog *dlg = new CYPenDialog( this );
  dlg->setPen( lvi->cypen() );
  if ( dlg->exec() )
  {
  }

//   QPen pen = lvi->pixmap( mPenColumn )->toImage().pixel(1, 0);
//   int result = CYPenDialog::getColor( pen );
//   if (result == KColorDialog::Accepted)
//   {
//     QPixmap newPm(12, 1);
//     newPm.fill(c);
//     lvi->setPixmap(mColorColumn, newPm);
//   }

  emit modifie();
}

/*! Saisie le coefficient associé à la donnée sélectionnée.
    \fn CYDatasListView::setCoef()
 */
void CYDatasListView::setCoef()
{
  CYDatasListViewItem *lvi = (CYDatasListViewItem *)currentItem();

  if (!lvi)
    return;

  bool ok;
  double limit = 1000000;

  double res = QInputDialog::getDouble(this,
                                       tr("Display coefficient"),
                                       tr("Enter the new display coefficient: "),
                                       coef(lvi), -limit, limit, 2, &ok);
  if (ok)
  {
    if (res==0.0)
      CYMessageBox::sorry(this, tr("You must enter a coefficient display different of 0 !"));
    else
      lvi->setText(mCoefColumn, QString("%1").arg(res));
  }

  emit modifie();
}

/*! Saisie l'offset associé à la donnée sélectionnée.
    \fn CYDatasListView::setOffset()
 */
void CYDatasListView::setOffset()
{
  CYDatasListViewItem *lvi = (CYDatasListViewItem *)currentItem();

  if (!lvi)
    return;

  bool ok;
  double limit = 1000000;

  double res = QInputDialog::getDouble(this,
                                       tr("Display offset"),
                                       tr("Enter the new display offset: "),
                                       offset(lvi), -limit, limit, 2, &ok);
  if (ok)
  {
    lvi->setText(mOffsetColumn, QString("%1").arg(res));
  }

  emit modifie();
}

/*! Supprime la donnée sélectionnée.
    \fn CYDatasListView::deleteData()
 */
void CYDatasListView::deleteData()
{
  CYDatasListViewItem *lvi = (CYDatasListViewItem *)currentItem();
  if (lvi)
  {
    int id = lvi->text(mNumColumn).toInt();
    for (QTreeWidgetItemIterator it(this); *it; ++it)
    {
      CYDatasListViewItem *lvi2 = (CYDatasListViewItem *)(*it);
      int id2 = lvi2->text(mNumColumn).toInt();
      if (id2 > id)
      {
        id2--;
        lvi2->setText(mNumColumn, posToString(id2));
      }
    }

    /* Avant de supprimer l'élément sélectionnée nous déterminons un nouvel élément à sélectionner.
    De cette manière nous pouvons garantir que plusieurs éléments puissent être sélectionnés sans
    obliger l'utilisateur à sélectionner un nouvel élément entre les suppressions. Si tous les
    éléments sont supprimés, les boutons sont à nouveau désactivés. */
    CYDatasListViewItem *newSelected = 0;
    if (itemBelow(lvi))
    {
      itemBelow(lvi)->setSelected(true);
      newSelected = (CYDatasListViewItem *)itemBelow(lvi);
    }
    else if (itemAbove(lvi))
    {
      itemAbove(lvi)->setSelected(true);
      newSelected = (CYDatasListViewItem *)itemAbove(lvi);
    }
    // TOCHECK QT5
//    else
//      selectionChanged(0);

    delete lvi;

    if (newSelected)
      scrollToItem(newSelected);

    emit modifie();
  }
}


/*! @return le numéro de la colonne ayant pour libellé \a label
    \fn CYDatasListView::column(QString label)
 */
int CYDatasListView::column(QString label)
{
  QTreeWidgetItem *header=headerItem();
  for (int c = 0; c<header->columnCount(); c++)
    if (header->text(c)==label)
      return c;
  return -1;
}

/*! @return le numéro de la colonne contenant le numéro
    \fn CYDatasListView::numColumn()
 */
int CYDatasListView::numColumn()
{
  return mNumColumn;
}


/*! @return le numéro de la colonne contenant la couleur
    \fn CYDatasListView::colorColumn()
 */
int CYDatasListView::colorColumn()
{
  return mColorColumn;
}


/*! @return la couleur définie au numéro \a num.
    \fn CYDatasListView::color(int num)
 */
QColor CYDatasListView::color(int num)
{
  return color(findItems(QString("%1").arg(num), Qt::MatchExactly, mNumColumn).first());
}


/*! @return la couleur définie par l'élément \a lvi.
    \fn CYDatasListView::color(QTreeWidgetItem *lvi)
 */
QColor CYDatasListView::color(QTreeWidgetItem *lvi)
{
  CYDatasListViewItem *dlvi = (CYDatasListViewItem *)lvi;

  if (dlvi) // s'il existe un élément correspondant au numéro
    return dlvi->cypen()->pen().color();

  return QColor();
}


/*! @return le numéro de la colonne contenant le crayon
    \fn CYDatasListView::penColumn()
 */
int CYDatasListView::penColumn()
{
  return mPenColumn;
}


/*! @return le crayon défini au numéro \a num.
    \fn CYDatasListView::pen(int num)
 */
QPen CYDatasListView::pen(int num)
{
  return pen(findItems(QString("%1").arg(num), Qt::MatchExactly, mNumColumn).first());
}


/*! @return le crayon défini par l'élément \a lvi.
    \fn CYDatasListView::pen(QTreeWidgetItem *lvi)
 */
QPen CYDatasListView::pen(QTreeWidgetItem *lvi)
{
  CYDatasListViewItem *dlvi = (CYDatasListViewItem *)lvi;

  if (dlvi) // s'il existe un élément correspondant au numéro
    return dlvi->cypen()->pen();

  return QPen();
}


/*! @return le numéro de la colonne contenant le coefficient
    \fn CYDatasListView::coefColumn()
 */
int CYDatasListView::coefColumn()
{
  return mCoefColumn;
}

/*! @return le numéro de la colonne contenant l'offset
    \fn CYDatasListView::offsetColumn()
 */
int CYDatasListView::offsetColumn()
{
  return mOffsetColumn;
}

/*! @return le coefficient défini au numéro \a num.
    \fn CYDatasListView::coef(int num)
 */
double CYDatasListView::coef(int num)
{
  return coef(findItems(QString("%1").arg(num), Qt::MatchExactly, mCoefColumn).first());
}


/*! @return le coefficient défini par l'élément \a lvi.
    \fn CYDatasListView::coef(QTreeWidgetItem *lvi)
 */
double CYDatasListView::coef(QTreeWidgetItem *lvi)
{
  CYDatasListViewItem *dlvi = (CYDatasListViewItem *)lvi;

  if (dlvi) // s'il existe un élément correspondant au numéro
    return dlvi->text(coefColumn()).toDouble();

  return 1.0;
}

/*! @return l'offset défini au numéro \a num.
    \fn CYDatasListView::offset(int num)
 */
double CYDatasListView::offset(int num)
{
  return coef(findItems(QString("%1").arg(num), Qt::MatchExactly, mOffsetColumn).first());
}


/*! @return l'offset défini par l'élément \a lvi.
    \fn CYDatasListView::offset(QTreeWidgetItem *lvi)
 */
double CYDatasListView::offset(QTreeWidgetItem *lvi)
{
  CYDatasListViewItem *dlvi = (CYDatasListViewItem *)lvi;

  if (dlvi) // s'il existe un élément correspondant au numéro
    return dlvi->text(offsetColumn()).toDouble();

  return 0.0;
}

/*! @return le numéro de la colonne contenant le nom de donnée
    \fn CYDatasListView::dataNameColumn()
 */
int CYDatasListView::dataNameColumn()
{
  return mDataNameColumn;
}


/*! @return le nom de la donnée défini au numéro \a num.
    \fn CYDatasListView::dataName(int num)
 */
QString CYDatasListView::dataName(int num)
{
  return dataName(findItems(QString("%1").arg(num), Qt::MatchExactly, mNumColumn).first());
}


/*! @return le nom de la donnée défini par l'élément \a lvi.
    \fn CYDatasListView::dataName()
 */
QString CYDatasListView::dataName(QTreeWidgetItem *lvi)
{
  if (lvi) // s'il existe un élément correspondant au numéro
    return lvi->text(dataNameColumn());
//   else
//     CYFATAL;

  return 0;
}


/*! @return le numéro de la colonne contenant le libellé
    \fn CYDatasListView::labelColumn()
 */
int CYDatasListView::labelColumn()
{
  return mLabelColumn;
}


/*! @return le libellé défini par l'élément \a lvi.
    \fn CYDatasListView::label()
 */
QString CYDatasListView::label(QTreeWidgetItem *lvi)
{
  if (lvi) // s'il existe un élément correspondant au numéro
    return lvi->text(labelColumn());
//   else
//     CYFATAL;

  return 0;
}


/*! @return le nombre maximum de données possible.
    \fn CYDatasListView::maxNbDatas()
 */
int CYDatasListView::maxNbDatas() const
{
  return mMaxNbDatas;
}


/*! Saisir le nombre maximum de données possible.
    \fn CYDatasListView::setMaxNbDatas(const int val)
 */
void CYDatasListView::setMaxNbDatas(const int val)
{
  mMaxNbDatas = val;
  if ( mMaxNbDatas>1 )
    setColumnHidden( mNumColumn, false);
  else
    setColumnHidden( mNumColumn, true );
}


/*! @return true si le choix d'une couleur est possible.
    \fn CYDatasListView::withColor()
 */
bool CYDatasListView::withColor() const
{
  return mWithColor;
}


/*! Saisir \a true pour que le choix d'une couleur soit possible.
    \fn CYDatasListView::setWithColor(const bool val)
 */
void CYDatasListView::setWithColor(const bool val)
{
  mWithColor = val;

  if ( val )
    setColumnHidden( mColorColumn, false);
  else
    setColumnHidden( mColorColumn, true );
}


/*! @return true si le choix d'un crayon est possible.
    \fn CYDatasListView::withPen()
 */
bool CYDatasListView::withPen() const
{
  return mWithPen;
}


/*! Saisir \a true pour que le choix d'un crayon soit possible.
    \fn CYDatasListView::setWithPen(const bool val)
 */
void CYDatasListView::setWithPen(const bool val)
{
  mWithPen = val;
  if ( val )
    setColumnHidden( mPenColumn, false);
  else
    setColumnHidden( mPenColumn, true );
}


/*! @return true si le choix d'un coefficient est possible.
    \fn CYDatasListView::withCoef()
 */
bool CYDatasListView::withCoef() const
{
  return mWithCoef;
}


/*! Saisir \a true pour que le choix d'un coefficient soit possible.
    \fn CYDatasListView::setWithCoef(const bool val)
 */
void CYDatasListView::setWithCoef(const bool val)
{
  mWithCoef = val;
  if ( val )
    setColumnHidden( mCoefColumn, false);
  else
    setColumnHidden( mCoefColumn, true );
}

/*! @return true si le choix d'un offset est possible.
    \fn CYDatasListView::withOffset()
 */
bool CYDatasListView::withOffset() const
{
  return mWithOffset;
}


/*! Saisir \a true pour que le choix d'un offset soit possible.
    \fn CYDatasListView::setWithOffset(const bool val)
 */
void CYDatasListView::setWithOffset(const bool val)
{
  mWithOffset = val;
  if ( val )
    setColumnHidden( mOffsetColumn, false);
  else
    setColumnHidden( mOffsetColumn, true );
}

/*!
    \fn CYDatasListView::addColumn ( const QString & label, int width = -1 )
 */
int CYDatasListView::addColumn ( const QString & label, int width )
{
  QTreeWidgetItem *header = headerItem();
  for (int c = 0; c<header->columnCount(); c++)
    if (header->text(c)==label)
      return -1;
  int index = header->columnCount();
  header->setText(index, label);
  setColumnWidth(index, width);
  return index;
}


/*! Saisie le libellé associée à la donnée sélectionnée.
    \fn CYDatasListView::setLabel()
 */
void CYDatasListView::setLabel()
{
  CYDatasListViewItem *lvi = (CYDatasListViewItem *)currentItem();

  if (!lvi)
    return;

  bool ok;
  QString text = QInputDialog::getText(this,
                                       tr("Data label"),
                                       tr("Enter the new label: "),
                                       QLineEdit::Normal,
                                       lvi->text(mLabelColumn), &ok);
  if (ok && !text.isEmpty())
    lvi->setText(mLabelColumn, text);

  emit modifie();
}


/*! @return le QTreeWidgetItem définie au numéro \a num.
    \fn CYDatasListView::item( int num )
 */
QTreeWidgetItem * CYDatasListView::item( int num )
{
  return findItems(QString("%1").arg(num), Qt::MatchExactly, mNumColumn).first();
}


/*! @return le numéro du QTreeWidgetItem \a item.
    \fn CYDatasListView::num(QTreeWidgetItem * item)
 */
int CYDatasListView::num(QTreeWidgetItem * item)
{
  return item->text(mNumColumn).toInt();
}


/*! @return \a true si l'élément item existe.
    \fn CYDatasListView::find(CYDisplayItem *item)
 */
bool CYDatasListView::find(CYDisplayItem *item)
{
  QTreeWidgetItemIterator it( this );
  while ( *it )
  {
    CYDatasListViewItem *lvi = (CYDatasListViewItem *)(*it);
    if ( lvi->displayItem() == item )
      return true;
    ++it;
   }
  return false;
}


/*!
    \fn CYDatasListView::addData(QByteArray dataName)
 */
void CYDatasListView::addData(QString dataName)
{
  CYData *d = core->findData(dataName, false);
  if (!d)
    return;
  uint num = topLevelItemCount() + 1;
  CYDisplayItem *item = new CYDisplayItem(this, d, num);

  CYDisplayStyle *style;
  if (!core)
    style = new CYDisplayStyle;
  else
    style = core->style;

  QColor color = style->getDataColor(num-1);

  if (topLevelItemCount()>1)// test si la couleur est différente de celle de l'élément précédent
  {
    QString txt=QString("%1").arg(num-1);
    CYDatasListViewItem *lvi_prec = (CYDatasListViewItem *)findItem(txt,mNumColumn);

    if (lvi_prec) // s'il existe un élément précédent
    {
      // TOCHECK QT5
      QColor color_prec = lvi_prec->icon(mColorColumn).pixmap(12,1).toImage().pixel(1, 0);
      if (color.name() == color_prec.name()) // si même couleur
      {
        num++;
        if (num==style->getDataColorCount()) // si au bout de la liste des couleurs disponibles
          num = 0;
        color = style->getDataColor(num);
      }
    }
  }

  item->pen.setColor( color );
  addData(item);
}


/*!
    \fn CYDatasListView::addGroup(QString txt)
 */
void CYDatasListView::addGroup(QString txt)
{
  if ( mMaxNbDatas==1 )
    return;
  if (core)
    core->addGroup(txt, this);
}


/*! @return la position en chaîne de caractères avec le nombre de 0 devant en fonction de @see maxNbDatas()
    \fn CYDatasListView::posToString(int pos)
 */
QString CYDatasListView::posToString(int pos)
{
  if (pos > maxNbDatas())
  {
    CYMessageBox::sorry(this, tr("You can enter a maximun of %1 data!").arg(maxNbDatas()), tr("Acquisition list of datas full"));
    return 0;
  }

  if ( maxNbDatas()<10 )
  {
    return QString::number(pos);
  }
  else if ( maxNbDatas()<100 )
  {
    char num[3];
    sprintf(num,"%02d", pos);
    return QString(num);
  }
  else if ( maxNbDatas()<1000 )
  {
    char num[4];
    sprintf(num,"%03d", pos);
    return QString(num);
  }
  else if ( maxNbDatas()<10000 )
  {
    char num[5];
    sprintf(num,"%04d", pos);
    return QString(num);
  }
  return QString::number(pos);
}
