#include "cyactioncollection.h"

// CYLIBS
#include "cyaction.h"
#include "cywin.h"
#include "cy.h"

CYActionCollection::CYActionCollection(CYWin *parent, const char * 	name) :
  QObject(parent)
{
  setObjectName(name);
  mWin = parent;
}
void CYActionCollection::addAction( CYAction *action )
{
  mActionDict.insert( action->objectName(), action );
}

/** @return l'action ayant pour nom \a name. */
CYAction *CYActionCollection::action( const QString &name )
{
  return mActionDict.value( name );
}

CYAction *CYActionCollection::addActionTo( const QString &actionName, QMenu *pm)
{
  CYAction *a = action(actionName);
  if (!a)
  {
    CYMESSAGETEXT(tr("Cannot add action %1 in menu of window %2!")
                  .arg(actionName).arg(mWin->windowTitle()));
    return 0;
  }
  pm->addAction(a);
  return a;
}

CYAction *CYActionCollection::addActionTo( const QString &actionName, QToolBar *tb)
{
  CYAction *a = action(actionName);
  if (!a)
  {
    CYMESSAGETEXT(tr("Cannot add action %1 in tool bar %2 of window %3!")
                  .arg(actionName).arg(tb->windowTitle()).arg(mWin->windowTitle()));
    return 0;
  }
  tb->addAction(a);
  tb->setToolButtonStyle(mWin->toolButtonStyle());

  return a;
}

CYActionPtrList CYActionCollection::actions() const
{
  CYActionPtrList lst;

  QHashIterator<QString, CYAction *> it(mActionDict);
  while (it.hasNext())
  {
    it.next();
    lst.append( it.value() );
  }

  return lst;
}

//void CYActionCollection::addMenu( CYActionMenu *menu )
//{
//  mMenuDict.insert( menu->objectName(), menu );
//}

///** @return le menu ayant pour nom \a name. */
//CYActionMenu *CYActionCollection::menu( const QString &name )
//{
//  return mMenuDict.value( name );
//}

//CYActionMenu *CYActionCollection::addMenuTo( const QString &menuName, QMenu *pm)
//{
//  CYActionMenu *a = menu(menuName);
//  if (!a)
//  {
//    CYMESSAGETEXT(tr("Cannot add menu %1 in menu of window %2!")
//                  .arg(menuName).arg(mWin->windowTitle()));
//    return 0;
//  }
//  pm->addMenu(a);
//  return a;
//}

//CYActionMenu *CYActionCollection::addMenuTo( const QString &menuName, QToolBar *tb)
//{
//  CYMenuMenu *a = menu(menuName);
//  if (!a)
//  {
//    CYMESSAGETEXT(tr("Cannot add menu %1 in tool bar %2 of window %3!")
//                  .arg(menuName).arg(tb->windowTitle()).arg(mWin->windowTitle()));
//    return 0;
//  }
//  tb->addMenu(a);
//  tb->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

//  return a;
//}

//CYMenuPtrList CYActionCollection::menus() const
//{
//  CYMenuPtrList lst;

//  QHashIterator<QString, CYActionMenu *> it(mMenuDict);
//  while (it.hasNext())
//  {
//    it.next();
//    lst.append( it.value() );
//  }

//  return lst;
//}

void CYActionCollection::setXMLFile( const QString &fileName )
{
  mXMLFile = fileName;
}

QString CYActionCollection::xmlFile()
{
  return mXMLFile;
}
