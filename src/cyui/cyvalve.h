/***************************************************************************
                          cyvalve.h  -  description
                             -------------------
    début                  : mer aoû 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYVALVE_H
#define CYVALVE_H

// CYLIBS
#include "cypixmapview.h"
#include "cydatawdg.h"

/** Vanne.
  * @author Gérald LE CLEACH
  */

class CYValve : public CYPixmapView
{
  Q_OBJECT
  Q_ENUMS(Way)
  Q_PROPERTY(Way way READ way WRITE setWay)
  Q_PROPERTY(bool NF READ NF WRITE setNF)
  Q_PROPERTY(bool manual READ manual WRITE setManual)
  Q_PROPERTY(bool calibrated READ calibrated WRITE setCalibrated)
  Q_PROPERTY(QByteArray pixmapFileTrue READ pixmapFileTrue WRITE setPixmapFileTrue DESIGNABLE false STORED false)
  Q_PROPERTY(QByteArray pixmapFileFalse READ pixmapFileFalse WRITE setPixmapFileFalse DESIGNABLE false STORED false)

public:
  /** Sens en cas de clapet (pilote en haut)*/
  enum Way
  {
    /** Pas de sens (pas de clapet).  */
    None=0,
    /** Vers la gauche.  */
    toLeft,
    /** Vers la droite.  */
    toRight,
  };

  CYValve(QWidget *parent=0, const QString &name=0);

  ~CYValve();

public: // Public methods
  /** @return \a true si la vanne est une NF. */
  bool NF() const;
  /** Si \a val vaut \a true alors la vanne est NF sinon elle est NO. */
  void setNF(const bool val);
  /** @return \a true si la vanne est manuelle. */
  bool manual() const;
  /** Si \a val vaut \a true alors la vanne est manuelle sinon elle est pilotée. */
  void setManual(const bool val);
  /** @return \a true si la vanne est tarée. */
  bool calibrated() const;
  /** Si \a val vaut \a true alors la vanne est tarée. */
  void setCalibrated(const bool val);
  /** @return le sens en cas de clapet @see Way, sinon @return \a None. */
  Way way() const { return mWay; }
  /** Saisie le sens en cas de clapet dans la vanne @see Way, sinon saisir \a None. */
  void setWay(const Way val);

public slots: // Public slots
  /** Saisie l'état du pilote de la vanne. */
//   virtual void setState(bool);

private:
  void init(bool value=false);

  /** Vaut \a true si la vanne est une NF. */
  bool mNF;
  /** Vaut \a true si la vanne est manuelle. */
  bool mManual;
  /** Vaut \a true si la vanne est tarée. */
  bool mCalibrated;
  /** Sens en cas de clapet @see Way. */
  Way mWay;
};

#endif
