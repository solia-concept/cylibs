//
// C++ Interface: cyimage
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYIMAGE_H
#define CYIMAGE_H

// QT
#include <qimage.h>
// CYLIBS
#include "cy.h"
#include "cydatasgroup.h"

class CYString;
class CYImageList;

/**
  @short Ensemble de données définissant une image.
  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYImage : public QImage
{
public:
  /** Création d'une image.
    * @param index    Index dans la liste.
    * @param label    Etiquette.
    * @param fileName Chemin de l'image. */
    CYImage(CYImageList *list, int index, QString label, QString fileName);

  /** Création d'une image.
    * @param parent   Liste d'images à la quelle elle appartient.. */
    CYImage(CYImageList *list);

    ~CYImage();

    void loadImage(const QString &fileName);

    /** Charge les paramètres de l'image à partir d'un QDomElement. */
    virtual bool loadFromDOM(QDomElement &el);
    /** Sauvegarde les paramètres de l'image dans un QDomElement. */
    virtual bool saveToDOM(QDomElement &el);


    /** @return l'index utilisé dans la liste. */
    uint index() { return mIndex; }
    /** Saisie l'index utilisé dans la liste. */
    void setIndex(uint i) { mIndex = i; }

    /** Saisie la list d'images à la quelle elle appartient. */
    void setList(CYImageList *list);
    /** @return la list d'images à la quelle elle appartient. */
    CYImageList *list() { return mList; }

    QString fileName() { return mFileName; }
    QString label() { return mLabel; }

protected:
    /** Liste d'images à la quelle elle appartient. */
    CYImageList *mList;
    /** Index utilisé dans la liste. */
    uint mIndex;
    QString mLabel;
    QString mFileName;
};

#endif
