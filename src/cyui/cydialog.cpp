/***************************************************************************
                          cydialog.cpp  -  description
                             -------------------
    début                  : mer mai 21 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cydialog.h"

// QT
#include <QShowEvent>
#include <QKeyEvent>
#include <QString>
// CYLIBS
#include "cydb.h"
#include "cywidget.h"
#include "cycore.h"

CYDialog::CYDialog(QWidget *parent, const QString &name, Qt::WindowFlags f)
  : QDialog(parent, f),
    CYTemplate(this)
{
  setObjectName(name);
  mType  = Cy::Dialog;
  mWidget = 0;
  mTimer = new QTimer(this);

  mBackgroundColor=palette().color(backgroundRole());
  mForegroundColor=palette().color(foregroundRole());

  connect(mTimer, SIGNAL(timeout()), SLOT(refresh()));
}

CYDialog::~CYDialog()
{
}

QByteArray CYDialog::genericMark() const
{
  return CYTemplate::genericMark();
}

void CYDialog::setGenericMark(const QByteArray txt)
{
  CYTemplate::setGenericMark(txt);
}

void CYDialog::setGenericMark(int index)
{
  CYTemplate::setGenericMark(index);
}

void CYDialog::setBackgroundColor ( const QColor & color )
{
  mBackgroundColor=color;
  setAutoFillBackground(true);
  QPalette pal = palette();
  pal.setColor(backgroundRole(), mBackgroundColor);
  setPalette(pal);
}

const QColor & CYDialog::backgroundColor () const
{
  return mBackgroundColor;
}

void CYDialog::setForegroundColor ( const QColor & color )
{
  mForegroundColor=color;
  QPalette pal = palette();
  pal.setColor(QPalette::WindowText, mForegroundColor);
  setPalette(pal);
}

const QColor & CYDialog::foregroundColor () const
{
  return mForegroundColor;
}


void CYDialog::setBackgroundPixmap( const QPixmap & pixmap )
{
  if (pixmap.isNull())
    return;

  mBackgroundPixmap=pixmap;
  setAutoFillBackground(true);

  QPalette palette;
  palette.setBrush(backgroundRole(), QBrush(pixmap));
  setPalette(palette);
}

const QPixmap & CYDialog::backgroundPixmap () const
{
  return mBackgroundPixmap;
}


void CYDialog::linkDatas()
{
  mDatasLinked = false;
  mModified = false;
  init(this);
  setValidate();
  setReadOnly(mReadOnly);
  mDatasLinked = true;
}

void CYDialog::apply()
{
  if (!isVisible())
    return;

  if (mWidget==0)
    CYTemplate::update(this);
  else
    CYTemplate::update(mWidget);

  foreach (CYDB *db, mWriteDB)
    db->apply();

  if (mForcingMode)
  {
    foreach (CYDB *db, mForcingDB)
      if (!mWriteDB.value(db->objectName()))
        db->apply();
  }
  if (mWidget==0)
    CYTemplate::apply(this);
  else
    CYTemplate::apply(mWidget);
  setValidate();
}

void CYDialog::update()
{
  if (isVisible())
  {
    if (mWidget==0)
      CYTemplate::update(this);
    else
      CYTemplate::update(mWidget);
  }
}

void CYDialog::refresh()
{
  if (isVisible())
  {
    if (mWidget==0)
      CYTemplate::refresh(this);
    else
      CYTemplate::refresh(mWidget);
  }
}

void CYDialog::designer()
{
  if (isVisible())
  {
    if (mWidget==0)
      CYTemplate::designer(this);
    else
      CYTemplate::designer(mWidget);
    setModifie();
  }
}

void CYDialog::setModifie()
{
  if (isVisible() && mDatasLinked && mEmitModifie)
  {
    if (!isModified())
    {
      mModified = true;
      setModifieView();
    }
    emit modifie();
    emit modified(true);
  }
}

void CYDialog::setValidate()
{
  if (isVisible() && mDatasLinked)
  {
    if (isModified())
    {
      mModified = false;
      setValidateView();
    }
    emit validate();
    emit modified(false);
  }
}

void CYDialog::setReadOnly(bool val)
{
  mReadOnly = val;
  if (mWidget==0)
    CYTemplate::setReadOnly(val, this);
  else
    CYTemplate::setReadOnly(val, mWidget);
}

void CYDialog::setForcingMode(bool val)
{
  mForcingMode = val;
  if (mWidget==0)
    CYTemplate::setForcingMode(val, this);
  else
    CYTemplate::setForcingMode(val, mWidget);
}

void CYDialog::setSettingMode(bool val)
{
  mSettingMode = val;
  if (mWidget==0)
    CYTemplate::setSettingMode(val, this);
  else
    CYTemplate::setSettingMode(val, mWidget);
}

void CYDialog::changeEvent(QEvent* event)
{
  QDialog::changeEvent(event);
}

void CYDialog::setWidget(QWidget *widget)
{
  if (widget->inherits("CYWidget"))
    mWidget = (CYWidget*)widget;
}

void CYDialog::showEvent(QShowEvent *e)
{
  if (mLinkDatasShow)
    linkDatas();
  int width = fontMetrics().horizontalAdvance(windowTitle())+150;
  if (minimumWidth()<width)
    setMinimumWidth(width);
  QDialog::showEvent(e);
  //  raise();
  //  activateWindow();
}

void CYDialog::accept()
{
  apply();
  QDialog::accept();
}

void CYDialog::help()
{
  core->invokeHelp("cydoc", metaObject()->className());
}

void CYDialog::keyPressEvent(QKeyEvent *e)
{
  if (e->modifiers() == 0 || (e->modifiers() & Qt::KeypadModifier && e->key() == Qt::Key_Enter))
  {
    switch (e->key())
    {
      case Qt::Key_Escape:
        reject();
        break;
      case Qt::Key_Up:
      case Qt::Key_Left:
        if (focusWidget() && (focusWidget()->focusPolicy() == Qt::StrongFocus || focusWidget()->focusPolicy() == Qt::WheelFocus))
        {
          e->ignore();
          break;
        }
        // call ours, since c++ blocks us from calling the one
        // belonging to focusWidget().
        focusNextPrevChild(false);
        break;
      case Qt::Key_Down:
      case Qt::Key_Right:
        if (focusWidget() && (focusWidget()->focusPolicy() == Qt::StrongFocus || focusWidget()->focusPolicy() == Qt::WheelFocus))
        {
          e->ignore();
          break;
        }
        focusNextPrevChild(true);
        break;
      default:
        e->ignore();
        return;
    }
  }
  else
  {
    e->ignore();
  }
}


/*! @return \a true si la connexion automatique des données lors de l'affichage est autorisée.
    @see CYDialog::setLinkDatasShow(bool val)
    \fn CYDialog::linkDatasShow() const
 */
bool CYDialog::linkDatasShow() const
{
  return CYTemplate::linkDatasShow();
}


/*! Autorise ou non la connexion des données suivant \a val.
    Ceci est utile lorsqu'on veut inhiber la connexion qui est faite automatiquement au moment de l'affichage, par exemple pour le mode forçage.
    \fn CYDialog::setLinkDatasShow(const bool val)
 */
void CYDialog::setLinkDatasShow(const bool val)
{
  CYTemplate::setLinkDatasShow( val );
}
