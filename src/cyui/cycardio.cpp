/***************************************************************************
                          cycardio.cpp  -  description
                             -------------------
    début                  : jeu mar 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cycardio.h"
// QT
#include <qapplication.h>
// CYLIBS
#include "cytype.h"
#include "cyboxdig.h"
#include "cycarddig.h"

CYCardIO::CYCardIO(QWidget *parent, const QString &name, int no, Mode m)
: QGroupBox(parent),
  mNoCard(no),
  mMode(m)
{
  setObjectName(name);
  mMaxCols       = 1;
  mNoCol         = 0;
  mCurrentBoxIO  = 0;
  mHasFocus      = false;
  mButtonGroup   = new QButtonGroup(this) ;

  for (int col=0; col<mMaxCols; col++)
    mColors.insert(col, new QColor(Qt::red));

  setAlignment(int(Qt::AlignVCenter|Qt::AlignHCenter));
}

CYCardIO::~CYCardIO()
{
}

CYCardIO::Mode CYCardIO::mode() const
{
  return mMode;
}

void CYCardIO::setMode(const Mode mode)
{
  if (mode != mMode)
  {
    mMode = mode;
    updateCard();
  }
}

int CYCardIO::num() const
{
  return mNoCard;
}

void CYCardIO::setNum(const int num)
{
  if (num && (mNoCard != num))
  {
    mNoCard = num;
    updateCard();
  }
}

int CYCardIO::col() const
{
  return mNbCols;
}

void CYCardIO::setCol(const int col)
{
  if (col && (col <= mMaxCols) && (mNbCols != col))
  {
    mNbCols = col;
    updateCard();
  }
}

int CYCardIO::row() const
{
  return mNbRows;
}

void CYCardIO::setRow(const int row)
{
  if (row && (mNbRows != row))
  {
    mNbRows = row;
    updateCard();
  }
}

int CYCardIO::numCol() const
{
  return mNoCol;
}

void CYCardIO::setNumCol(const int col)
{
  if ((col > -1) && (col < mNbCols) && (col != mNoCol))
  {
    mNoCol = col;
  }
}

QColor CYCardIO::colorCol() const
{
  return QColor(*mColors[mNoCol]);
}

QColor CYCardIO::colorCol(int col) const
{
  return QColor(*mColors[col]);
}

void CYCardIO::setColorCol(const QColor &color)
{
  if (mColors[mNoCol] != &color)
  {
    mColors.insert(mNoCol, new QColor(color));
    updateCard();
  }
}

void CYCardIO::setLEDColor(const QColor &color)
{
  for (int col=0; col<mNbCols; col++)
    setLEDColor(col, color);
}

void CYCardIO::setLEDColor(int num, const QColor &color)
{
  mColors.insert(num, new QColor(color));
  updateCard();
}

void CYCardIO::refresh()
{
  foreach (CYBoxIO *box, mBoxIOList)
    box->refresh();
}

void CYCardIO::setHelp(const QString &msg)
{
  emit help(msg);
}

void CYCardIO::focusInfo(CYBoxIO *box)
{
  mCurrentBoxIO = box;
}

QSize CYCardIO::sizeHint() const
{
  return QSize(48, 48);
}

QSize CYCardIO::minimumSizeHint() const
{
  return QSize(16, 16);
}

void CYCardIO::addButton( QAbstractButton * button )
{
  mButtonGroup->addButton(button);
}


void CYCardIO::removeButton( QAbstractButton * button )
{
  mButtonGroup->removeButton(button);
}
