#ifndef CYTREEWIDGET_H
#define CYTREEWIDGET_H

// QT
#include <QTreeWidget>

class CYTreeWidget : public QTreeWidget
{
  Q_OBJECT
public:
  CYTreeWidget(QWidget *parent = 0, const QString &name = 0);

  ~CYTreeWidget();

  virtual void setSelected( QTreeWidgetItem *item, bool selected );

  QTreeWidgetItem *findItem(const QString & text, Qt::MatchFlags flags, int column = 0) const;
  QTreeWidgetItem *findItem(const QString & text, int column = 0) const;

  virtual QTreeWidgetItem *nextSibling( QTreeWidgetItem *item );

signals:
  /** Emis à chaque modification. */
  void modifie();

protected:
  virtual bool dropMimeData(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action);

};

#endif // CYTREEWIDGET_H
