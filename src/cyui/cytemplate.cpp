/***************************************************************************
                          cytemplate.cpp  -  description
                             -------------------
    début                  : lun sep 1 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cytemplate.h"

// CYLIBS
#include "cycore.h"
#include "cydb.h"
#include "cydata.h"
#include "cypushbutton.h"
#include "cyflaginput.h"
#include "cycheckbox.h"
#include "cynuminput.h"
#include "cytextinput.h"
#include "cytextedit.h"
#include "cydatewidget.h"
#include "cynumdisplay.h"
#include "cytextlabel.h"
#include "cylevelview.h"
#include "cymovingpixmap.h"
#include "cyvalve.h"
#include "cypump.h"
#include "cyventilator.h"
#include "cyled.h"
#include "cydial.h"
#include "cycardio.h"
#include "cymultimeter.h"
#include "cybargraph.h"
#include "cyscope.h"
#include "cyrobottopview.h"
#include "cyslider.h"
#include "cywheel.h"
#include "cycolorbutton.h"
#include "cydatastable.h"
#include "cybuttongroup.h"
#include "cyframe.h"
#include "cywidget.h"
#include "cydialog.h"
#include "cyanalysesheet.h"
#include "cytabwidget.h"

CYTemplate::CYTemplate(QWidget *widget)
{
  mThis       = widget;
  mDatasLinked= false;
  mLinkDatasShow = true;
  mReadOnly   = false;
  mForcingMode = false;
  mSettingMode = false;
  mEmitModifie = true;
  mModified    = false;
  mUpdateIfVisible = true;
  mTreatChildDataWdg = true;
}

CYTemplate::~CYTemplate()
{
}

void CYTemplate::init(QObject *parent)
{
  if (!core)
    return;

  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYPushButton"))
      {
        CYPushButton *child = (CYPushButton *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYFlagInput"))
      {
        CYFlagInput *child = (CYFlagInput *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYComboBox"))
      {
        CYComboBox *child = (CYComboBox *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYCheckBox"))
      {
        CYCheckBox *child = (CYCheckBox *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYTextInput"))
      {
        CYTextInput *child = (CYTextInput *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYTextEdit"))
      {
        CYTextEdit *child = (CYTextEdit *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYDateWidget"))
      {
        CYDateWidget *child = (CYDateWidget *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYNumDisplay"))
      {
        CYNumDisplay *child = (CYNumDisplay *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYTextLabel"))
      {
        CYTextLabel *child = (CYTextLabel *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYPixmapView"))
      {
        CYPixmapView *child = (CYPixmapView *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYMovingPixmap"))
      {
        CYMovingPixmap *child = (CYMovingPixmap *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYLabel"))
      {
        CYTextLabel *child = (CYTextLabel *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYLineEdit"))
      {
        CYLineEdit *child = (CYLineEdit *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYLevelView"))
      {
        CYLevelView *child = (CYLevelView *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYValve"))
      {
        CYValve *child = (CYValve *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYPump"))
      {
        CYPump *child = (CYPump *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYVentilator"))
      {
        CYVentilator *child = (CYVentilator *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYLed"))
      {
        CYLed *child = (CYLed *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYDial"))
      {
        CYDial *child = (CYDial *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYRobotTopView"))
      {
        CYRobotTopView *child = (CYRobotTopView *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYSlider"))
      {
        CYSlider *child = (CYSlider *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYWheel"))
      {
        CYWheel *child = (CYWheel *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYColorButton"))
      {
        CYColorButton *child = (CYColorButton *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));

        if (child->treatChildDataWdg()) init(child);
      }
      else if (w->inherits("CYButtonGroup"))
      {
        CYButtonGroup *child = (CYButtonGroup *)w;
        child->CYDataWdg::setParent(mThis, mType, mGenericMark);

        QObject::disconnect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::disconnect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        QObject::connect(child, SIGNAL(modifie())   , mThis, SLOT(setModifie()));
        QObject::connect(mThis, SIGNAL(refreshing()), child, SLOT(refresh()));

        if (child->treatChildDataWdg()) init(child);
      }
      else
      {
        if (w->inherits("CYFrame"))
        {
          CYFrame *child = (CYFrame *)w;

          QObject::disconnect(child, SIGNAL(modifie()), mThis, SLOT(setModifie()));
          QObject::disconnect(child, SIGNAL(validate()), mThis, SLOT(setValidate()));

          QObject::connect(child, SIGNAL(modifie()), mThis, SLOT(setModifie()));
          QObject::connect(child, SIGNAL(validate()), mThis, SLOT(setValidate()));

          if (child->treatChildDataWdg()) init(child);
          //           child->linkDatas();
        }
        else if (w->inherits("CYWidget"))
        {
          CYWidget *child = (CYWidget *)w;

          QObject::disconnect(child, SIGNAL(modifie()), mThis, SLOT(setModifie()));
          QObject::disconnect(child, SIGNAL(validate()), mThis, SLOT(setValidate()));

          QObject::connect(child, SIGNAL(modifie()), mThis, SLOT(setModifie()));
          QObject::connect(child, SIGNAL(validate()), mThis, SLOT(setValidate()));

          if (child->treatChildDataWdg()) init(child);
          /*          child->linkDatas();*/
        }
        else if (w->inherits("CYTabWidget"))
        {
          CYTabWidget *child = (CYTabWidget *)w;

          QObject::disconnect(mThis, SIGNAL(modified(bool)), child, SLOT(setModified(bool)));
          QObject::disconnect(child, SIGNAL(validating()), mThis, SLOT(setValidate()));
          QObject::disconnect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
          QObject::disconnect(child, SIGNAL(refreshing()), mThis, SIGNAL(refreshing()));

          QObject::connect(mThis, SIGNAL(modified(bool)), child, SLOT(setModified(bool)));
          QObject::connect(child, SIGNAL(validating()), mThis, SLOT(setValidate()));
          QObject::connect(child, SIGNAL(applying())  , mThis, SLOT(apply()));
          QObject::connect(child, SIGNAL(refreshing()), mThis, SIGNAL(refreshing()));

          if (child->treatChildDataWdg()) init(child);
        }
        else if (w->inherits("QTabWidget"))
        {
          setUpdateIfVisible(false);

          init(w);
        }
        else if (!w->inherits("QDialog"))
        {
          init(w);
        }
      }
    }
  }
}

void CYTemplate::addWriteDB(CYDB *db)
{
  mWriteDB.insert(db->objectName(), db);
}

void CYTemplate::addForcingDB(CYDB *db)
{
  mForcingDB.insert(db->objectName(), db);
}

void CYTemplate::apply(QObject *parent)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYWidget"))
      {
        CYWidget *child = (CYWidget *)w;
        child->apply();
      }
      else if (w->inherits("CYFrame"))
      {
        CYFrame *child = (CYFrame *)w;
        child->apply();
      }
      else
        apply(w);
    }
  }
}

void CYTemplate::update(QObject *parent)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYFlagInput"))
      {
        CYFlagInput *child = (CYFlagInput *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYComboBox"))
      {
        CYComboBox *child = (CYComboBox *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYCheckBox"))
      {
        CYCheckBox *child = (CYCheckBox *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYTextInput"))
      {
        CYTextInput *child = (CYTextInput *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYLineEdit"))
      {
        CYLineEdit *child = (CYLineEdit *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYTextEdit"))
      {
        CYTextEdit *child = (CYTextEdit *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYDateWidget"))
      {
        CYDateWidget *child = (CYDateWidget *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYSlider"))
      {
        CYSlider *child = (CYSlider *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYWheel"))
      {
        CYWheel *child = (CYWheel *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYColorButton"))
      {
        CYColorButton *child = (CYColorButton *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYPixmapView"))
      {
        CYPixmapView *child = (CYPixmapView *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYMovingPixmap"))
      {
        CYMovingPixmap *child = (CYMovingPixmap *)w;
        child->update();
        if (child->treatChildDataWdg()) update(child);
      }
      else if (w->inherits("CYPushButton"))
      {
        CYPushButton *child = (CYPushButton *)w;
        child->update();
        if (child->treatChildDataWdg()) if (child->treatChildDataWdg()) update(child);
      }
      else
      {
        if (w->inherits("CYButtonGroup"))
        {
          CYButtonGroup *child = (CYButtonGroup *)w;
          child->update();
        }
        update(w);
      }
    }
  }
}

void CYTemplate::refresh(QObject *parent, bool useAutoRefresh)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYFlagInput"))
      {
        CYFlagInput *child = (CYFlagInput *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYPushButton"))
      {
        CYPushButton *child = (CYPushButton *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYComboBox"))
      {
        CYComboBox *child = (CYComboBox *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYCheckBox"))
      {
        CYCheckBox *child = (CYCheckBox *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYNumDisplay"))
      {
        CYNumDisplay *child = (CYNumDisplay *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYLineEdit"))
      {
        CYLineEdit *child = (CYLineEdit *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYTextInput"))
      {
        CYTextInput *child = (CYTextInput *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYTextEdit"))
      {
        CYTextEdit *child = (CYTextEdit *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYDateWidget"))
      {
        CYDateWidget *child = (CYDateWidget *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYTextLabel"))
      {
        CYTextLabel *child = (CYTextLabel *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYRobotTopView"))
      {
        CYRobotTopView *child = (CYRobotTopView *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYLevelView"))
      {
        CYLevelView *child = (CYLevelView *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYMovingPixmap"))
      {
        CYMovingPixmap *child = (CYMovingPixmap *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYValve"))
      {
        CYValve *child = (CYValve *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYPump"))
      {
        CYPump *child = (CYPump *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYVentilator"))
      {
        CYVentilator *child = (CYVentilator *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYPixmapView"))
      {
        CYPixmapView *child = (CYPixmapView *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYLed"))
      {
        CYLed *child = (CYLed *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYDial"))
      {
        CYDial *child = (CYDial *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYCardIO"))
      {
        CYCardIO *child = (CYCardIO *)w;
        {
          child->refresh();
        }
      }
      else if (w->inherits("CYSlider"))
      {
        CYSlider *child = (CYSlider *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYWheel"))
      {
        CYWheel *child = (CYWheel *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else if (w->inherits("CYColorButton"))
      {
        CYColorButton *child = (CYColorButton *)w;
        if ( (child->autoRefresh()) || !useAutoRefresh )
        {
          child->refresh();
          if (child->treatChildDataWdg()) refresh(child);
        }
      }
      else
      {
        if (w->inherits("CYButtonGroup"))
        {
          CYButtonGroup *child = (CYButtonGroup *)w;
          if ( (child->autoRefresh()) || !useAutoRefresh )
            child->refresh();
        }
        if (!w->inherits("QDialog"))
          refresh(w);
      }
    }
  }
}

void CYTemplate::designer(QObject *parent)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYFlagInput"))
      {
        CYFlagInput *child = (CYFlagInput *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYComboBox"))
      {
        CYComboBox *child = (CYComboBox *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYCheckBox"))
      {
        CYCheckBox *child = (CYCheckBox *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYTextInput"))
      {
        CYTextInput *child = (CYTextInput *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYTextEdit"))
      {
        CYTextEdit *child = (CYTextEdit *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYDateWidget"))
      {
        CYDateWidget *child = (CYDateWidget *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYSlider"))
      {
        CYSlider *child = (CYSlider *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYWheel"))
      {
        CYWheel *child = (CYWheel *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYColorButton"))
      {
        CYColorButton *child = (CYColorButton *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else if (w->inherits("CYPushButton"))
      {
        CYPushButton *child = (CYPushButton *)w;
        child->designer();
        if (child->treatChildDataWdg()) designer(child);
        setModifie();
      }
      else
      {
        if (w->inherits("CYButtonGroup"))
        {
          CYButtonGroup *child = (CYButtonGroup *)w;
          child->designer();
          setModifie();
        }
        designer(w);
      }
    }
  }
}

void CYTemplate::setReadOnly(bool val, QObject *parent)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYPushButton"))
      {
        CYPushButton *child = (CYPushButton *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYComboBox"))
      {
        CYComboBox *child = (CYComboBox *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYCheckBox"))
      {
        CYCheckBox *child = (CYCheckBox *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYNumDisplay"))
      {
        CYNumDisplay *child = (CYNumDisplay *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYTextInput"))
      {
        CYTextInput *child = (CYTextInput *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYLineEdit"))
      {
        CYLineEdit *child = (CYLineEdit *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYTextEdit"))
      {
        CYTextEdit *child = (CYTextEdit *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYDateWidget"))
      {
        CYDateWidget *child = (CYDateWidget *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYSlider"))
      {
        CYSlider *child = (CYSlider *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYWheel"))
      {
        CYWheel *child = (CYWheel *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYColorButton"))
      {
        CYColorButton *child = (CYColorButton *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYLabel"))
      {
        CYLabel *child = (CYLabel *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else if (w->inherits("CYWidget"))
      {
        CYWidget *child = (CYWidget *)w;
        child->setReadOnly(val);
      }
      else if (w->inherits("CYFrame"))
      {
        CYFrame *child = (CYFrame *)w;
        child->setReadOnly(val);
      }
      else if (w->inherits("CYDateWidget"))
      {
        CYDateWidget *child = (CYDateWidget *)w;
        child->setReadOnly(val);
        if (child->treatChildDataWdg()) setReadOnly(val, child);
      }
      else
      {
        if (w->inherits("CYButtonGroup"))
        {
          CYButtonGroup *child = (CYButtonGroup *)w;
          child->setReadOnly(val);
        }
        setReadOnly(val, w);
      }
    }
  }
}

void CYTemplate::setForcingMode(bool val, QObject *parent)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYPushButton"))
      {
        CYPushButton *child = (CYPushButton *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYComboBox"))
      {
        CYComboBox *child = (CYComboBox *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYCheckBox"))
      {
        CYCheckBox *child = (CYCheckBox *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYLineEdit"))
      {
        CYLineEdit *child = (CYLineEdit *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYWidget"))
      {
        CYWidget *child = (CYWidget *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYFrame"))
      {
        CYFrame *child = (CYFrame *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYSlider"))
      {
        CYSlider *child = (CYSlider *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYWheel"))
      {
        CYWheel *child = (CYWheel *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYColorButton"))
      {
        CYColorButton *child = (CYColorButton *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYPixmapView"))
      {
        CYPixmapView *child = (CYPixmapView *)w;
        child->setForcingMode(val);
        if (child->treatChildDataWdg()) setForcingMode(val, child);
      }
      else if (w->inherits("CYAnalyseSheet"))
      {
        CYAnalyseSheet *child = (CYAnalyseSheet *)w;
        child->setForcing(val);
      }
      else
      {
        if (w->inherits("CYButtonGroup"))
        {
          CYButtonGroup *child = (CYButtonGroup *)w;
          child->setForcingMode(val);
        }
        setForcingMode(val, w);
      }
    }
  }
}

void CYTemplate::setSettingMode(bool val, QObject *parent)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYPushButton"))
      {
        CYPushButton *child = (CYPushButton *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYComboBox"))
      {
        CYComboBox *child = (CYComboBox *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYCheckBox"))
      {
        CYCheckBox *child = (CYCheckBox *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYLineEdit"))
      {
        CYLineEdit *child = (CYLineEdit *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYWidget"))
      {
        CYWidget *child = (CYWidget *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYFrame"))
      {
        CYFrame *child = (CYFrame *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYSlider"))
      {
        CYSlider *child = (CYSlider *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYWheel"))
      {
        CYWheel *child = (CYWheel *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYColorButton"))
      {
        CYColorButton *child = (CYColorButton *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else if (w->inherits("CYPixmapView"))
      {
        CYPixmapView *child = (CYPixmapView *)w;
        child->setSettingMode(val);
        if (child->treatChildDataWdg()) setSettingMode(val, child);
      }
      else
      {
        if (w->inherits("CYButtonGroup"))
        {
          CYButtonGroup *child = (CYButtonGroup *)w;
          child->setSettingMode(val);
        }
        setSettingMode(val, w);
      }
    }
  }
}
void CYTemplate::initForceValues()
{
  bool tmp = isReadOnly();
  initForceValues(mThis);
  setReadOnly(true);
  refresh(mThis, false);
  setReadOnly(tmp);
}

void CYTemplate::initSettingValues()
{
  bool tmp = isReadOnly();
  initSettingValues(mThis);
  setReadOnly(true);
  refresh(mThis, false);
  setReadOnly(tmp);
}

void CYTemplate::initForceValues(QObject *parent)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYDatasTable"))
      {
        CYDatasTable *child = (CYDatasTable *)w;
        child->initForceValues();
        if (child->treatChildDataWdg()) initForceValues(child);
      }
      else if (w->inherits("CYPixmapView"))
      {
        CYPixmapView *child = (CYPixmapView *)w;
        child->initForceValue();
        if (child->treatChildDataWdg()) initForceValues(child);
      }
      else
        initForceValues(w);
    }
  }
}

void CYTemplate::initSettingValues(QObject *parent)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYLabel"))
      {
        CYLabel *child = (CYLabel *)w;
        child->initSettingValue();
        if (child->treatChildDataWdg()) initSettingValues(child);
      }
      else
        initSettingValues(w);
    }
  }
}

void CYTemplate::setLocaleDB(CYDB *db)
{
  mWriteDB.clear();
  mWriteDB.insert(db->objectName(), db);
  setLocaleDB(mThis, db);
}

void CYTemplate::setLocaleDB(QObject *parent, CYDB *db)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYPushButton"))
      {
        CYPushButton *child = (CYPushButton *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYFlagInput"))
      {
        CYFlagInput *child = (CYFlagInput *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYComboBox"))
      {
        CYComboBox *child = (CYComboBox *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYCheckBox"))
      {
        CYCheckBox *child = (CYCheckBox *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYTextInput"))
      {
        CYTextInput *child = (CYTextInput *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYTextEdit"))
      {
        CYTextEdit *child = (CYTextEdit *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYDateWidget"))
      {
        CYDateWidget *child = (CYDateWidget *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYNumDisplay"))
      {
        CYNumDisplay *child = (CYNumDisplay *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYTextLabel"))
      {
        CYTextLabel *child = (CYTextLabel *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYLevelView"))
      {
        CYLevelView *child = (CYLevelView *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYMovingPixmap"))
      {
        CYMovingPixmap *child = (CYMovingPixmap *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYRobotTopView"))
      {
        CYRobotTopView *child = (CYRobotTopView *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYValve"))
      {
        CYValve *child = (CYValve *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYPump"))
      {
        CYPump *child = (CYPump *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYVentilator"))
      {
        CYVentilator *child = (CYVentilator *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYPixmapView"))
      {
        CYPixmapView *child = (CYPixmapView *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYLed"))
      {
        CYLed *child = (CYLed *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYDial"))
      {
        CYDial *child = (CYDial *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYSlider"))
      {
        CYSlider *child = (CYSlider *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYWheel"))
      {
        CYWheel *child = (CYWheel *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else if (w->inherits("CYColorButton"))
      {
        CYColorButton *child = (CYColorButton *)w;
        child->setLocaleDB(db);
        if (child->treatChildDataWdg()) setLocaleDB(child, db);
      }
      else
      {
        if (w->inherits("CYButtonGroup"))
        {
          CYButtonGroup *child = (CYButtonGroup *)w;
          child->setLocaleDB(db);
        }
        setLocaleDB(w, db);
      }
    }
  }
}

void CYTemplate::setEditPositions(bool state)
{
  setEditPositions(mThis, state);
}

void CYTemplate::setEditPositions(QObject *parent, bool state)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      //       if (w->inherits("CYPushButton"))
      //       {
      //         CYPushButton *child = (CYPushButton *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYFlagInput"))
      //       {
      //         CYFlagInput *child = (CYFlagInput *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYComboBox"))
      //       {
      //         CYComboBox *child = (CYComboBox *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYCheckBox"))
      //       {
      //         CYCheckBox *child = (CYCheckBox *)w;
      //         child->setEditPositions(state);
      //       }
      if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        child->setEditPositions(state);
        if (child->treatChildDataWdg()) setEditPositions(child, state);
      }
      //       else if (w->inherits("CYTextInput"))
      //       {
      //         CYTextInput *child = (CYTextInput *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYTextEdit"))
      //       {
      //         CYTextEdit *child = (CYTextEdit *)w;
      //         child->setEditPositions(state);
      //       }
      else if (w->inherits("CYLineEdit"))
      {
        CYLineEdit *child = (CYLineEdit *)w;
        child->setEditPositions(state);
        if (child->treatChildDataWdg()) setEditPositions(child, state);
      }
      else if (w->inherits("CYLabel"))
      {
        CYLabel *child = (CYLabel *)w;
        child->setEditPositions(state);
        if (child->treatChildDataWdg()) setEditPositions(child, state);
      }
      //       else if (w->inherits("CYTextLabel"))
      //       {
      //         CYTextLabel *child = (CYTextLabel *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYLevelView"))
      //       {
      //         CYLevelView *child = (CYLevelView *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYRobotTopView"))
      //       {
      //         CYRobotTopView *child = (CYRobotTopView *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYValve"))
      //       {
      //         CYValve *child = (CYValve *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYPump"))
      //       {
      //         CYPump *child = (CYPump *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYPixmapView"))
      //       {
      //         CYPixmapView *child = (CYPixmapView *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYLed"))
      //       {
      //         CYLed *child = (CYLed *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYDial"))
      //       {
      //         CYDial *child = (CYDial *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYSlider"))
      //       {
      //         CYSlider *child = (CYSlider *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYWheel"))
      //       {
      //         CYWheel *child = (CYWheel *)w;
      //         child->setEditPositions(state);
      //       }
      //       else if (w->inherits("CYColorButton"))
      //       {
      //         CYColorButton *child = (CYColorButton *)w;
      //         child->setEditPositions(state);
      //       }
      else
      {
        if (w->inherits("CYButtonGroup"))
        {
          CYButtonGroup *child = (CYButtonGroup *)w;
          child->setEditPositions(state);
        }
        if (w->inherits("CYFrame"))
        {
          CYFrame *child = (CYFrame *)w;
          child->setEditPositions(state);
        }
        setEditPositions(w, state);
      }
    }
  }
}

bool CYTemplate::updateIfVisible() const
{
  return mUpdateIfVisible;
}

void CYTemplate::setUpdateIfVisible(const bool state)
{
  mUpdateIfVisible = state;
  setUpdateIfVisible(mThis, state);
}

void CYTemplate::setUpdateIfVisible(QObject *parent, bool state)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYPushButton"))
      {
        CYPushButton *child = (CYPushButton *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYFlagInput"))
      {
        CYFlagInput *child = (CYFlagInput *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYComboBox"))
      {
        CYComboBox *child = (CYComboBox *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYCheckBox"))
      {
        CYCheckBox *child = (CYCheckBox *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYNumInput"))
      {
        CYNumInput *child = (CYNumInput *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYTextInput"))
      {
        CYTextInput *child = (CYTextInput *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYTextEdit"))
      {
        CYTextEdit *child = (CYTextEdit *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYDateWidget"))
      {
        CYDateWidget *child = (CYDateWidget *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYNumDisplay"))
      {
        CYNumDisplay *child = (CYNumDisplay *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYTextLabel"))
      {
        CYTextLabel *child = (CYTextLabel *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYLevelView"))
      {
        CYLevelView *child = (CYLevelView *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYMovingPixmap"))
      {
        CYMovingPixmap *child = (CYMovingPixmap *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYRobotTopView"))
      {
        CYRobotTopView *child = (CYRobotTopView *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYValve"))
      {
        CYValve *child = (CYValve *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYPump"))
      {
        CYPump *child = (CYPump *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYVentilator"))
      {
        CYVentilator *child = (CYVentilator *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYPixmapView"))
      {
        CYPixmapView *child = (CYPixmapView *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYLed"))
      {
        CYLed *child = (CYLed *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYDial"))
      {
        CYDial *child = (CYDial *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYSlider"))
      {
        CYSlider *child = (CYSlider *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYWheel"))
      {
        CYWheel *child = (CYWheel *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else if (w->inherits("CYColorButton"))
      {
        CYColorButton *child = (CYColorButton *)w;
        child->setUpdateIfVisible(state);
        if (child->treatChildDataWdg()) setUpdateIfVisible(child, state);
      }
      else
      {
        if (w->inherits("CYButtonGroup"))
        {
          CYButtonGroup *child = (CYButtonGroup *)w;
          child->setUpdateIfVisible(state);
        }
        setUpdateIfVisible(w, state);
      }
    }
  }
}

bool CYTemplate::quitView()
{
  if (isModified())
  {
    QString msg = QString(QObject::tr("There are unsaved changes in the active view.\n"
                                      "Do you want to apply or discard this changes?"));

    int res = CYMessageBox::warningYesNoCancel(mThis, msg, QObject::tr("Unsaved changes"),
                                               QObject::tr("&Apply"),
                                               QObject::tr("&Discard"));

    if (res == CYMessageBox::Yes)
      apply();
    if (res == CYMessageBox::No)
    {
      refresh();
      setValidate();
    }
    else if (res == CYMessageBox::Cancel)
      return false;
  }
  return true;
}

void CYTemplate::setGenericMark(int index)
{
  setGenericMark(mGenericMark.setNum(index));
}

void CYTemplate::startTimer(int ms)
{
  if ( mTimer )
    mTimer->start(ms);
}


/*! @return \a true si la connexion automatique des données lors de l'affichage est autorisée.
    @see CYTemplate::setLinkDatasShow(bool val)
    \fn CYTemplate::linkDatasShow() const
 */
bool CYTemplate::linkDatasShow() const
{
  return mLinkDatasShow;
}


/*! Autorise ou non la connexion des données suivant \a val.
    Ceci est utile lorsqu'on veut inhiber la connexion qui est faite automatiquement au moment de l'affichage, par exemple pour le mode forçage.
    \fn CYTemplate::setLinkDatasShow(const bool val)
 */
void CYTemplate::setLinkDatasShow(const bool val)
{
  mLinkDatasShow = val;
  setLinkDatasShow(mThis, val);
}


/*! Autorise ou non la connexion automatique des données lors de l'affichage suivant \a val.
    Ceci est utile par exemple en mode forçage.
    \fn CYTemplate::setLinkDatasShow(QObject *parent, bool val)
 */
void CYTemplate::setLinkDatasShow(QObject *parent, bool val)
{
  if (!parent)
    return;

  if (parent->findChild<QWidget *>())
  {
    QList<QWidget *> widgets = parent->findChildren<QWidget *>(QString(), Qt::FindDirectChildrenOnly);
    for (int i=0; i < widgets.count(); i++)
    {
      QWidget *w = (QWidget *)widgets.at(i);
      if (w->inherits("CYWidget"))
      {
        CYWidget *child = (CYWidget *)w;
        child->setLinkDatasShow(val);
      }
      else if (w->inherits("CYFrame"))
      {
        CYFrame *child = (CYFrame *)w;
        child->setLinkDatasShow(val);
      }
      else if (w->inherits("CYDialog"))
      {
        CYDialog *child = (CYDialog *)w;
        child->setLinkDatasShow(val);
      }
      else
       setLinkDatasShow(w, val);
    }
  }
}
