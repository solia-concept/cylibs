/***************************************************************************
                          cypump.cpp  -  description
                             -------------------
    begin                : jeu oct 9 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cypump.h"

// QT
#include <qimage.h>
//Added by qt3to4:
#include <QString>
// CYLIBS
#include "cycore.h"

CYPump::CYPump(QWidget *parent, const QString &name)
  : CYMovieView(parent,name)
{
  mState = true;
  mSens = LeftToRight;

  addPixmapFile(LeftToRight, ":/pixmaps/cypump_off_lr.png");
  addPixmapFile(RightToLeft, ":/pixmaps/cypump_off_rl.png");
  addPixmapFile(BottomToTop, ":/pixmaps/cypump_off_bt.png");
  addPixmapFile(TopToBottom, ":/pixmaps/cypump_off_tb.png");

  addMovieFile(LeftToRight, ":/pixmaps/cypump_on_lr.gif");
  addMovieFile(RightToLeft, ":/pixmaps/cypump_on_rl.gif");
  addMovieFile(BottomToTop, ":/pixmaps/cypump_on_bt.gif");
  addMovieFile(TopToBottom, ":/pixmaps/cypump_on_tb.gif");

  setSens(mSens);
  setAttribute(Qt::WA_OpaquePaintEvent);
  initPalette();
}

CYPump::~CYPump()
{
}

void CYPump::setSens(Sens s)
{
  mSens = s;
  mPixmapIndex  = -1;
  mMovieIndex   = -1;
  mOffsetIndexFile = 0;

  setPixmapFile((int)mSens);
  setMovieFile((int)mSens);
  setState(mState);
}

void CYPump::setBackgroundPixmap( const QPixmap & pixmap )
{
  if (pixmap.isNull())
    return;

  mBackgroundPixmap=pixmap;
  setAutoFillBackground(true);

  QPalette palette;
  palette.setBrush(backgroundRole(), QBrush(pixmap));
  setPalette(palette);
}

const QPixmap & CYPump::backgroundPixmap () const
{
  return mBackgroundPixmap;
}
