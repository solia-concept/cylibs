#ifndef CYDIALSCALEDIALOG_H
#define CYDIALSCALEDIALOG_H

// CYLIBS
#include <cydialog.h>

class CYData;

namespace Ui {
    class CYDialScaleDialog;
}


class CYDialScaleDialog : public CYDialog
{
public:
  CYDialScaleDialog(QWidget *parent=0, const QString &name="textDialog");
  ~CYDialScaleDialog();

  /** Saisie les données de l'échelle. */
  virtual void setData(CYData *min, CYData *max, CYData *step);

private:
    Ui::CYDialScaleDialog *ui;
};

#endif // CYDIALSCALEDIALOG_H
