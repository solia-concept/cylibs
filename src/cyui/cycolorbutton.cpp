//
// C++ Implementation: cycolorbutton
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cycolorbutton.h"

// QT
#include <QStyleOptionButton>
#include <QColorDialog>
#include <QStyle>
#include <QPainter>
#include <qdrawutil.h>
// CYLIBS
#include "cyflag.h"
#include "cycolor.h"
#include "cydialog.h"
#include "cywidget.h"
#include "cyframe.h"
#include "cycore.h"
#include "cyreadonlyfilter.h"

class CYColorButton::CYColorButtonPrivate
{
public:
  bool m_bdefaultColor;
  QColor m_defaultColor;
};

CYColorButton::CYColorButton( QWidget *parent, const QString &name )
  : QPushButton( parent ),
    CYDataWdg(this)
{
  setObjectName(name);
  d = new CYColorButtonPrivate;
  d->m_bdefaultColor = false;
  d->m_defaultColor = QColor();
  setAcceptDrops( true);

  // 2000-10-15 (putzer): fixes broken keyboard usage
  connect (this, SIGNAL(clicked()), this, SLOT(chooseColor()));
  init();
}

CYColorButton::CYColorButton( const QColor &c, QWidget *parent,
															const QString &name )
	: QPushButton( parent ),
		CYDataWdg(this)
{
  setObjectName(name);
  col=c;
  d = new CYColorButtonPrivate;
  d->m_bdefaultColor = false;
  d->m_defaultColor = QColor();
  setAcceptDrops( true);

  // 2000-10-15 (putzer): fixes broken keyboard usage
  connect (this, SIGNAL(clicked()), this, SLOT(chooseColor()));
  init();
}

CYColorButton::CYColorButton( const QColor &c, const QColor &defaultColor, QWidget *parent,
															const QString &name )
	: QPushButton( parent ),
		CYDataWdg(this)
{
  setObjectName( name );
  col=c;
  d = new CYColorButtonPrivate;
  d->m_bdefaultColor = true;
  d->m_defaultColor = defaultColor;
  setAcceptDrops( true);

  // 2000-10-15 (putzer): fixes broken keyboard usage
  connect (this, SIGNAL(clicked()), this, SLOT(chooseColor()));
  init();
}

CYColorButton::~CYColorButton()
{
  delete d;
}

void CYColorButton::init()
{
  mAutoRefresh = false;
  mWriteData = false;

  connect(this, SIGNAL(changed(const QColor &)), SLOT(colorChanged(const QColor &)));
}

void CYColorButton::setPalette(const QPalette &palette)
{
  QPushButton::setPalette(palette);
}

void CYColorButton::changeEvent(QEvent* event)
{
  if (event && (event->type()==QEvent::EnabledChange))
  {
    // cet événement est envoyé, si l'état actif change
    bool state = isEnabled();
    if (!readOnly() || !state)
      mEnabled = state;
  }
  QPushButton::changeEvent(event);
}

void CYColorButton::setColor( const QColor &c )
{
  if ( col != c )
  {
    col = c;
    repaint();
    emit changed( col );
  }
}

QColor CYColorButton::defaultColor() const
{
  return d->m_defaultColor;
}

void CYColorButton::setDefaultColor( const QColor &c )
{
  d->m_bdefaultColor = c.isValid();
  d->m_defaultColor = c;
}

void CYColorButton::paintEvent(QPaintEvent *event)
{
  QPushButton::paintEvent(event);

  QPainter painter(this);
  drawButtonLabel(&painter);
}

void CYColorButton::drawButtonLabel( QPainter *painter )
{
  int x, y, w, h;
  QStyleOptionButton *styleOption= new QStyleOptionButton;
  QRect r = rect();
  r.getRect(&x, &y, &w, &h);

  int margin = style()->pixelMetric( QStyle::PM_ButtonMargin,
                                     styleOption, this );
  x += margin;
  y += margin;
  w -= 2*margin;
  h -= 2*margin;

  if (isChecked() || isDown())
  {
    x += style()->pixelMetric( QStyle::PM_ButtonShiftHorizontal,
                               styleOption, this );
    y += style()->pixelMetric( QStyle::PM_ButtonShiftVertical,
                               styleOption, this );
  }

  QColor fillCol = isEnabled() ? col : painter->background().color();
  qDrawShadePanel( painter, x, y, w, h, palette(), true, 1, NULL);
  if ( fillCol.isValid() )
    painter->fillRect( x+1, y+1, w-2, h-2, fillCol );

  if ( hasFocus() )
  {
//    QRect focusRect = style()->subElementRect( QStyle::SE_PushButtonFocusRect,
//                                               styleOption, this );
    style()->drawPrimitive( QStyle::PE_FrameFocusRect,
                            styleOption, painter, this);//CYDEBUG,focusRect, QColorGroup(palette()) );
  }
}

QSize CYColorButton::sizeHint() const
{
  //   TODO QT4
  //	return style().sizeFromContents(QStyle::CT_PushButton, this, QSize(40, 15)).
  //			expandedTo(QApplication::globalStrut());
  return QPushButton::sizeHint();
}

void CYColorButton::chooseColor()
{
  QColor c = QColorDialog::getColor( color(), this );
  if(c.isValid())
  {
    setColor( c );
  }
}

void CYColorButton::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYColorButton::dataName() const
{
  return CYDataWdg::dataName();
}

void CYColorButton::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYColorButton::flagName() const
{
  return CYDataWdg::flagName();
}

void CYColorButton::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYColorButton::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYColorButton::setInverseFlag(bool inverse)
{
  mInverseFlag = inverse;
}

bool CYColorButton::inverseFlag() const
{
  return mInverseFlag;
}

void CYColorButton::setInverseHideFlag(bool inverse)
{
  mInverseHideFlag = inverse;
}

bool CYColorButton::inverseHideFlag() const
{
  return mInverseHideFlag;
}

void CYColorButton::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYColorButton::benchType() const
{
  return CYDataWdg::benchType();
}

QColor CYColorButton::value() const
{
  return mValue;
}

void CYColorButton::setValue(QColor value)
{
  mValue = value;
}

bool CYColorButton::ctrlFlag()
{
  if (mFlag==0)
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;

  setEnabled(res);
  return res;
}

void CYColorButton::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYColorButton::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYColorButton::linkData()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
    return;

  this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));

  switch (mData->type())
  {
    case Cy::Color  :
    {
      CYColor *data = (CYColor *)mData;
      setValue(data->val());
      setColor(data->val());
      setDefaultColor(data->def());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  refresh();
}

void CYColorButton::refresh()
{
  ctrlHideFlag();

  if (!ctrlFlag())
    return;

  if (!hasData())
    return;

  if (mData==0)
  {
    setEnabled(false);
    return;
  }
  if (mData->flag())
  {
    if (!mData->flag()->val())
    {
      setEnabled(false);
      return;
    }
  }

  switch (mData->type())
  {
    case Cy::Color  :
    {
      CYColor *data = (CYColor *)mData;
      setValue(data->val());
      setColor(data->val());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYColorButton::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    setEnabled(false);
    return;
  }

  switch (mData->type())
  {
    case Cy::Color  :
    {
      CYColor *data = (CYColor *)mData;
      setValue(data->def());
      setColor(data->def());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYColorButton::update()
{
  if (!hasData())
    return;

  if (!isVisible() && mUpdateIfVisible)
    return;

  if (mData==0)
    return;

  if (isHidden())
    return;

  switch (mData->type())
  {
    case Cy::Color   :
    {
      CYColor *data = (CYColor *)mData;
      data->setVal(value());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYColorButton::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }


  ctrlFlag();
}

void CYColorButton::focusInEvent(QFocusEvent *e)
{
  QPushButton::focusInEvent(e);
  emit focusIn();
}


/*! Traîtement lors d'un changement de couleur
    \fn CYColorButton::colorChanged(const QColor &newColor)
 */
void CYColorButton::colorChanged(const QColor &newColor)
{
  setValue(newColor);
  if ( isVisible() )
    emit modifie();
}
