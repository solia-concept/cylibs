/***************************************************************************
                          cyboxdo.cpp  -  description
                             -------------------
    début                  : Sun Feb 23 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyboxdo.h"

// CYLIBS
#include "cycore.h"
#include "cydb.h"
#include "cydo.h"
#include "cycheckbox.h"
#include "cycarddo.h"

CYBoxDO::CYBoxDO(QWidget *parent, const QString &name, QColor c, Mode m)
: CYBoxDIG(parent, name, c, m)
{
  mTitle = QString(name);

  hl = new QHBoxLayout(this);
  hl->setObjectName(mTitle.toUtf8());
  hl->setSpacing(3);
  hl->setMargin(2);
  hl->setAlignment(Qt::AlignVCenter|Qt::AlignHCenter);

  init();
}

CYBoxDO::~CYBoxDO()
{
}

void CYBoxDO::init()
{
  if (hasData())
  {
    mTitle = mData->elec();
    this->setToolTip(mData->displayHelp()+"<br>"+mData->infoCY());    
  }

  if (mMode == Forcing)
  {
    mCheck = new CYCheckBox(this);
    mCheck->setFocusPolicy(Qt::ClickFocus);
    mCheck->setTristate(false);
    hl->addWidget(mCheck);
    insertButton(mCheck);
    connect(mCheck, SIGNAL(focusIn()), this, SLOT(focusIn()));
    connect(mCheck, SIGNAL(focusOut()), this, SLOT(focusOut()));        
  }

  mLed = new CYLed(this);
  QSizePolicy sp((QSizePolicy::Policy)0, (QSizePolicy::Policy)0);
  sp.setHeightForWidth(mLed->sizePolicy().hasHeightForWidth());
  mLed->setSizePolicy(sp);
  mLed->setState(CYLed::On);
  mLed->setLook(CYLed::Raised);
  mLed->setColor(mColor);
  hl->addWidget(mLed);

  mLabel = new QLabel(this);
  mLabel->setText(mTitle);
  hl->addWidget(mLabel);

  refresh();  
}

void CYBoxDO::update()
{
  delete mLed;
  delete mLabel;
  if (mEnableMode)
  {
    if (mMode == Forcing)
    {
      removeButton(mCheck);
      delete mCheck;
    }      
  }
  else
  {
    if (mMode != Forcing)
    {
      removeButton(mCheck);
      delete mCheck;
    }
  }
     
  init();
}

void CYBoxDO::setData(CYDO *data)
{
  mData    = data;
  mHasData = true;
  update();
}

void CYBoxDO::refresh()
{
  if (!mHasData)
    return;

  if (mData->val())
    mLed->on();
  else
    mLed->off();
}

void CYBoxDO::refreshForcing()
{
  if ((mMode != Forcing) || (!mHasData))
    return;

  bool state = mData->val();
  mData->setForcing(state);
  mCheck->setChecked(state);
}

void CYBoxDO::setValue()
{
  if ((mMode != Forcing) || (!mHasData))
    return;

  mData->setForcing(mCheck->checkState());
}

void CYBoxDO::simulation()
{
  if (mSimul)
  {
    mLed->on();
    mSimul = false;
  }
  else
  {
    mLed->off();
    mSimul = true;
  }
}

void CYBoxDO::focusIn()
{
  hide();
  setFrameShape(Box);
  show();

  if (!mHasData)
    return;
    
  emit help(mData->label() + ": " + mData->help());

  if (mCard != 0)
    mCard->focusInfo(this); 
}

void CYBoxDO::focusOut()
{
  hide();
  setFrameShape(NoFrame);
  show();
}

void CYBoxDO::focusInEvent(QFocusEvent *e)
{
  mCheck->QObject::event(e);
}

void CYBoxDO::focusOutEvent(QFocusEvent *e)
{
  mCheck->QObject::event(e);
}
