#ifndef CYACTION_H
#define CYACTION_H

// QT
#include <qaction.h>
// CYLIBS
#include <cyactioncollection.h>

class CYFlag;

class CYAction : public QAction
{
  Q_OBJECT
public:
  CYAction(const QString &text = 0,
           const QString &pix = 0,
           const QKeySequence &accel = 0,
           const QObject *receiver = 0,
           const char *slot = 0,
           CYActionCollection *parent = 0,
           const QString &name =0,
           bool toggle = false );

  CYAction(const QString &text = 0,
           const QIcon icon = QIcon(),
           const QKeySequence &accel = 0,
           const QObject *receiver = 0,
           const char *slot = 0,
           CYActionCollection *parent = 0,
           const QString &name =0,
           bool toggle = false );

  CYAction(const QString &text = 0,
           const QString &pix = 0,
           const QKeySequence &accel = 0,
           CYActionCollection *parent = 0,
           const QString &name =0,
           bool toggle = false );

  CYAction(const QString &text = 0,
           const QIcon icon = QIcon(),
           const QKeySequence &accel = 0,
           CYActionCollection *parent = 0,
           const QString &name =0,
           bool toggle = false );

  virtual bool hasIcon();

  CYActionCollection *actionCollection() { return mActionCollection; }

  virtual CYFlag *usedFlag() { return mUsedFlag; }

		/**
	 * @brief Saisie le marque-page du manuel associé.
	 * Le texte à afficher est généré automatiquement à partir du chemin d'accès @see path()
	 * @param id    Identifiant dans la documentation.
	 */
	virtual void setIdDocBookMark(QString id);
	/**
	 * @brief Saisie le chemin d'accès par menu à l'action.
	 * Si un marque-page du manuel existe pour cette action, alors le chemin d'accès sert à comme texte de lien.
	 */
	virtual void setMenuPath(const QString& path);
	/**
	 * @return le chemin d'accès par menu à l'action
	 */
	QString menuPath() { return mMenuPath; }
	/**
	 * @return le chemin d'accès complet de l'action
	 */
	QString path();

public slots:
  virtual void setUsed(bool val, bool notify=false);

signals:
  /** Emis à chaque mis à jour de l'état d'utilisation.
   *  Le paramètre \a used vaut \a false en accès protégé ou
   *  si l'utilisateur en cours n'est pas autorisé pour accéder à cette action.*/
  void usedUpdated(bool used);

protected:
  virtual void init();

  CYActionCollection *mActionCollection;

  /** Flag d'utilisation suivant l'utilisateur en cours. */
  CYFlag *mUsedFlag;

	QString mIdBookMark;
	QString mMenuPath;
};

#endif // CYACTION_H
