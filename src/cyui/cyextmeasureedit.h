//
// C++ Interface: cyextmeasureedit
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYEXTMEASUREEDIT_H
#define CYEXTMEASUREEDIT_H

// QT
#include <QWidget>
#include <QString>
#include <QHash>
// CYLIBS
#include <cywidget.h>

class CYExtMeasure;

class CYExtMeasureEditPage : public QObject
{
public:
	CYExtMeasureEditPage() {}
	~CYExtMeasureEditPage() {}

	QWidget *widget;
	QString label;
	bool enable;
	int index;
};

namespace Ui {
		class CYExtMeasureEdit;
}


/**
@short Widget d'édition d'une mesure externe.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYExtMeasureEdit : public CYWidget
{
	Q_OBJECT
	Q_ENUMS(GroupSection)
	Q_PROPERTY(GroupSection showGroup READ showGroup WRITE setShowGroup)
	Q_PROPERTY(bool enableCT READ enableCT WRITE setEnableCT)
	Q_PROPERTY(bool hideLabel READ hideLabel WRITE setHideLabel)

public:
    CYExtMeasureEdit(QWidget *parent=0, const QString &name=0);

    ~CYExtMeasureEdit();

    /** Mode d'affichage du groupe. */
    /* ATTENTION énumération deavant être identique à Cy::GroupSection*/
    enum GroupSection
    {
      /** Pas d'affichage du groupe. */
      No,
      /** Affichage du dernier sous-groupe. Annule setShowLabel ou setShowChoiceLabel. */
      lastGroup,
      /** Affichage des sous-groupes. Annule setShowLabel ou setShowChoiceLabel. */
      underGroup,
      /** Affichage du groupe entier. Annule setShowLabel ou setShowChoiceLabel. */
      wholeGroup
    };

    virtual void linkDatas();

    /** @return le mode d'affichage du groupe. */
    virtual GroupSection showGroup() const;
    /** Saisir le mode d'affichage du groupe. */
    virtual void setShowGroup(const GroupSection val);

    /** @return \a true si le paramétrage contrôle-cotérance est actif. */
    virtual bool enableCT() const;
    /** Saisir \a true pour activer le paramétrage contrôle-cotérance. */
    virtual void setEnableCT(const bool val);

	/** @return \a true si le libellé en haut de la vue est caché. */
	virtual bool hideLabel() const;
	/** Saisir \a true pour cacher le libellé en haut de la vue. */
	virtual void setHideLabel(const bool val);

public slots:
    virtual void apply();

protected:
		void removePage(int index);

		CYExtMeasure * mMeasure;
		QHash<int, CYExtMeasureEditPage*> mPage;

private:
    Ui::CYExtMeasureEdit *ui;

		bool mEnableCT;
	bool mHideLabel;
};

#endif
