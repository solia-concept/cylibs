/***************************************************************************
                          cyrobottopview.cpp  -  description
                             -------------------
    begin                : lun nov 1 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyrobottopview.h"

// ANSI
#include <math.h>
// QT
#include <qpainter.h>
#include <qpixmap.h>
// QWT
#include "qwt_math.h"
//#include "qwt_paint_buffer.h"
#include "qwt_round_scale_draw.h"
#include "qwt_painter.h"
#include "qwt_dial_needle.h"
#include "qwt_dial.h"
//Added by qt3to4:
#include <QString>
// CYLIBS
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyword.h"
#include "cyflag.h"
#include "cyrobottopviewarm.h"

CYRobotTopView::CYRobotTopView(QWidget *parent, const QString &name)
: QwtDial(parent),
  CYDataWdg(this)
{
  setObjectName(name);

  mMinLength = 0.0;
  mMaxLength = 100.0;
  mEnableValueChange = true;
  // TODO QT5 setAttribute(
//  setBackgroundMode(Qt::PaletteBackground);
//  setBackgroundOrigin(QWidget::ParentOrigin);
  setWrapping(true);
  // TOCHECK QT4
  //setScaleOptions(0);
  scaleDraw()->enableComponent(QwtAbstractScaleDraw::Ticks, false);
  scaleDraw()->enableComponent(QwtAbstractScaleDraw::Labels, false);
  scaleDraw()->enableComponent(QwtAbstractScaleDraw::Backbone, false);
  mArm = new CYRobotTopViewArm();
  setNeedle(mArm);
}

CYRobotTopView::~CYRobotTopView()
{
}

void CYRobotTopView::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYRobotTopView::dataName() const
{
  return CYDataWdg::dataName();
}

void CYRobotTopView::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYRobotTopView::flagName() const
{
  return CYDataWdg::flagName();
}

void CYRobotTopView::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYRobotTopView::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYRobotTopView::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYRobotTopView::benchType() const
{
  return CYDataWdg::benchType();
}

void CYRobotTopView::setInverseFlag(const bool inverse)
{
  CYDataWdg::setInverseFlag(inverse);
}

bool CYRobotTopView::inverseFlag() const
{
  return CYDataWdg::inverseFlag();
}

void CYRobotTopView::setInverseHideFlag(const bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYRobotTopView::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

void CYRobotTopView::setLengthDataName(const QByteArray &name)
{
  CYDataWdg::setDataSupName(2, name);
}

QByteArray CYRobotTopView::lengthDataName() const
{
  return CYDataWdg::dataSupName(2);
}


void CYRobotTopView::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYRobotTopView::setData2(CYData *data)
{
  if ( mDataSup[2])
  {
    disconnect( mDataSup[2], SIGNAL( formatUpdated() ), this, SLOT( linkData2() ) );
  }
  CYDataWdg::setDataSup(2, data );
  if ( mDataSup[2])
  {
    connect( mDataSup[2], SIGNAL( formatUpdated() ), this, SLOT( linkData2() ) );
  }
}


void CYRobotTopView::linkData()
{
  CYDataWdg::linkData();
  ctrlHideFlag();

  if (!hasData() || !mData)
    return;

  this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));

  mEnableValueChange = false;
  switch (mData->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue(data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
  linkData2();
  mEnableValueChange = true;
}

void CYRobotTopView::linkData2()
{
  CYDataWdg::linkDataSup(2);

  if (!hasDataSup(2) || !mDataSup[2])
    return;

  if (!mData)
  {
    this->setToolTip(QString(mDataSup[2]->displayHelp()+"<br>"+mDataSup[2]->infoCY()));
  }
  else
  {
    this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()
                                  +"<hr>"+
                                  mDataSup[2]->displayHelp()+"<br>"+mDataSup[2]->infoCY()));
  }

  switch (mDataSup[2]->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mDataSup[2];
                      setLength(data->val());
                      mMinLength = data->min();
                      mMaxLength = data->max();
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mDataSup[2]->objectName());
  }
}

void CYRobotTopView::refresh()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
    return;
  if (!isVisible())
    return;
  if (!ctrlFlag())
  {
    setEnabled(false);
    return;
  }
  if (mData==0)
    return;
  if (!mData->isOk())
  {
    setEnabled(false);
    return;
  }
  if (mData->flag()!=0)
  {
    if (!mData->flag()->val())
    {
      setEnabled(false);
      return;
    }
  }

  mEnableValueChange = false;
  switch (mData->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue(data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      break;
  }
  refresh2();
  QwtDial::update();
  mEnableValueChange = true;
}

void CYRobotTopView::refresh2()
{
  if (!hasDataSup(2))
    return;

  if (mDataSup[2]==0)
    return;
  if (!isVisible())
    return;

  if (!ctrlFlag())
  {
    setEnabled(false);
    return;
  }
  if (mDataSup[2]==0)
    return;
  if (!mDataSup[2]->isOk())
  {
    setEnabled(false);
    return;
  }
  if (mDataSup[2]->flag()!=0)
  {
    if (!mDataSup[2]->flag()->val())
    {
      setEnabled(false);
      return;
    }
  }

  switch (mDataSup[2]->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mDataSup[2];
                      setLength(data->val());
                      return;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mDataSup[2]->objectName());
                      break;
  }
}

void CYRobotTopView::drawContents(QPainter *painter) const
{
  // TOCHECK QWT
//  const QRect insideScaleRect = scaleContentsRect();
  const QRect insideScaleRect = boundingRect();
  const QPoint center = insideScaleRect.center();
  const int radius = insideScaleRect.width() / 2;

  painter->save();
  drawScaleContents(painter, center, radius);
  painter->restore();

  double direction = origin();

  if (isValid())
  {
    direction = origin() + minScaleArc();
    if ( maximum() > minimum() && maxScaleArc() > minScaleArc() )
    {
      const double ratio = (value() - minimum()) / (maximum() - minimum());
      direction += ratio * ( maxScaleArc() - minScaleArc());
    }

    if ( direction >= 360.0 )
      direction -= 360.0;
  }

  double orig = origin();
  if ( mode() == RotateScale )
  {
    orig -= direction - origin();
    direction = origin();
  }

  painter->save();
  // TOCHECK QWT
//  drawScale(painter, center, radius, orig, minScaleArc(), maxScaleArc());
  // TODO QWT
//  setOrigin(orig);
  drawScale(painter, center, radius);
  painter->restore();

  if ( isValid() )
  {
    QPalette::ColorGroup cg;
    if ( isEnabled() )
      cg = hasFocus() ? QPalette::Active : QPalette::Inactive;
    else
      cg = QPalette::Disabled;

    painter->save();
    drawNeedle(painter, center, radius, direction, cg);
    painter->restore();
  }
}

void CYRobotTopView::setLength(double val)
{
  if ((val<mMinLength) && (val>mMaxLength))
    CYMESSAGE;

  if (mMaxLength!=mMinLength)
  {
    double pc = (val/(mMaxLength-mMinLength));
    mArm->setPercentStroke(pc);
  }
  else
    CYMESSAGE

  valueChange();
}

void CYRobotTopView::valueChange()
{
  if (mEnableValueChange)
  {
    // TOCHEK QWT
    // QwtDial::valueChange();
    QwtDial::update();
  }
}

void CYRobotTopView::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}
