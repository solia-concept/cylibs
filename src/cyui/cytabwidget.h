/***************************************************************************
                          cytabwidget.h  -  description
                             -------------------
    begin                : dim fév 29 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYTABWIDGET_H
#define CYTABWIDGET_H

// QT
#include <qtabwidget.h>

/** @short TabWidget
  * @author LE CLÉACH Gérald
  */

class CYTabWidget : public QTabWidget
{
  Q_OBJECT
  Q_PROPERTY(QString pageTitle READ pageTitle WRITE setPageTitle)
  
public: 
  CYTabWidget(QWidget *parent=0, const QString &name=0);
  ~CYTabWidget();

  /** @return \a true si le widget contient des valeurs de données modifiées. */
  bool isModified() { return mModified; }

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

public: // Public methods
  /** @return le titre de la page courrante. */
  QString pageTitle() const;
  /** Saisie le titre de la page courrante. */
  void setPageTitle(const QString &title);

signals: // Signals
  /** Emis à chaque changement de page. */
  void currentPageIndexChanged(int index);
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();
  /** Emis pour demander la validation du widget parent. */
  void validating();
  /** Ordonne le rafraîchissement du widget parent. */
  void refreshing();
  /** Emis à chaque modification de valeur d'une des données. */
  void modifie();
  /** Emis à chaque validation des valeurs des données des widgets enfants. */
  void validate();

public slots: // Public slots
  /** Traîtement réalisé à chaque changement de page. */
  void setCurrentChanged(QWidget * widget);
  /** Place l'objet comme ayant des valeurs de données modifiées si \a state vaut \a true. */
  virtual void setModified(bool state);

//
//protected: // Protected methods
//  /** Fonction d'affichage. */
//  virtual void showEvent(QShowEvent *e);

protected: // Protected attributes
  /** Widget ayant des valeurs de données modifiées. */
  bool mModified;
  /** S'il vaut \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool mTreatChildDataWdg;
};

#endif
