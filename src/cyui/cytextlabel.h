/***************************************************************************
                          cytextlabel.h  -  description
                             -------------------
    début                  : lun sep 1 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYTEXTLABEL_H
#define CYTEXTLABEL_H

// QT
#include <QWidget>
#include <QColor>
// CYLIBS
#include "cylabel.h"

/** @short Visualisateur de texte.
  * @author Gérald LE CLEACH
  */

class CYTextLabel : public CYLabel
{
  Q_OBJECT
  Q_ENUMS(GroupSection)
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QString suffix READ suffix WRITE setSuffix)
  Q_PROPERTY(QString prefix READ prefix WRITE setPrefix)
  Q_PROPERTY(bool dataShow READ dataShow WRITE setDataShow)
  Q_PROPERTY(bool showLabel READ showLabel WRITE setShowLabel)
  Q_PROPERTY(bool showChoiceLabel READ showChoiceLabel WRITE setShowChoiceLabel)
  Q_PROPERTY(GroupSection showGroup READ showGroup WRITE setShowGroup)
  Q_PROPERTY(bool enableProcessColor READ enableProcessColor WRITE setEnableProcessColor)

public:
  CYTextLabel(QWidget *parent=0, const QString &name="textLabel");
  ~CYTextLabel();

  /** Mode d'affichage du groupe. */
  /* ATTENTION énumération deavant être identique à Cy::GroupSection*/
  enum GroupSection
  {
    /** Pas d'affichage du groupe. */
    No,
    /** Affichage du dernier sous-groupe. Annule setShowLabel ou setShowChoiceLabel. */
    lastGroup,
    /** Affichage des sous-groupes. Annule setShowLabel ou setShowChoiceLabel. */
    underGroup,
    /** Affichage du groupe entier. Annule setShowLabel ou setShowChoiceLabel. */
    wholeGroup
  };

public: // Public methods
  /** @return la valeur courante. */
  QString value();

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

  /** @return \a true pour un affichage de donnée comme par exemple sa valeur, son étiquette ou son groupe (@see CYTextLabel::setDataShow). */
  virtual bool dataShow() const { return mDataShow; }
  /** Saisir \a true pour un affichage de la valeur de la donnée ou en d'autres textes de la donnée comme son étiquette (@see CYTextLabel::setShowLabel), son choix relatif à sa valeur (@see CYTextLabel::setShowChoiceLabel) ou son groupe (@see CYTextLabel::setShowGroup).
   *  Saisir \a false pour afficher le texte de l'objet graphique. */
  virtual void setDataShow(const bool val) { mDataShow = val; setAlwaysOk(true); }

  /** @return \a true s'il faut y afficher l'étiquette de la donnée. */
  virtual bool showLabel() const { return mShowLabel; }
  /** Saisir \a true pour y afficher l'étiquette de la donnée. */
  virtual void setShowLabel(const bool val) { mShowLabel = val; setAlwaysOk(true); }

  /** @return \a true s'il faut y afficher l'étiquette relative à la valeur active de la donnée.
    * @see setChoiceLabel(int value, QString label)*/
  virtual bool showChoiceLabel() const { return mShowChoiceLabel; }
  /** Saisir \a true pour y afficher l'étiquette relative à la valeur active de la donnée. */
  virtual void setShowChoiceLabel(const bool val) { mShowChoiceLabel = val; }

  /** @return le mode d'affichage du groupe. */
  virtual GroupSection showGroup() const;
  /** Saisir le mode d'affichage du groupe. */
  virtual void setShowGroup(const GroupSection val);

  /** @return le suffixe.
    * @see setSuffix(). */
  QString suffix() const { return mSuffix; }
  /** @return le préfixe.
    * @see setPrefix(). */
  QString prefix() const { return mPrefix; }

  /** @return \a true si la gestion des couleurs process est active. */
  bool enableProcessColor() const { return mEnableProcessColor; }

public slots: // Public slots
  /** Place la valeur du contrôleur. */
  void setValue(QString);

  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();

  /** Saisie le suffixe à afficher. Pour désactiver cette propriété il
    * suffit d'utiliser QString().
    * @see suffix() */
  void setSuffix(const QString &suffix) { mSuffix = suffix; }
  /** Saisie le préfixe à afficher. Pour désactiver cette propriété il
    * suffit d'utiliser QString().
    * @see preffix() */
  void setPrefix(const QString &prefix) { mPrefix = prefix; }

  /** Configure le widget en fonction de la donnée à laquelle il est lié. */
  virtual void linkData();

  /** Saisir \a true pour activer la gestion des couleurs process. */
  void setEnableProcessColor(const bool val);
  /** Saisie la couleur d'affichage utilisée lorsque la valeur est valide et que la gestion des couleurs process @see setEnableProcessColor(const bool val). */
  void setEnableColor(const QColor &c);
  /** Saisie la couleur d'affichage utilisée lorsque la valeur n'est pas valide et que la gestion des couleurs process @see setEnableProcessColor(const bool val). */
  void setDisableColor(const QColor &c);
  /** Saisie la couleur de fond et que la gestion des couleurs process @see setEnableProcessColor(const bool val). */
  void setBackgroundColor(const QColor &c);
  /** Saisie la couleur du premier plan du widget. */
  virtual void setForegroundColor ( const QColor & );

protected: // Protected methods
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
     Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);
  /** Un double-clic permet en mode simulation de changer la valeur de la donnée visualisée */
  virtual void mouseDoubleClickEvent ( QMouseEvent * e );

private:
  void init(QString value="");

protected: // Protected attributs
  QString mValue;

  /** Suffixe.. */
  QString mSuffix;
  /** Préfixe.. */
  QString mPrefix;

  /** Vaut \a true pour un affichage de donnée comme par exemple sa valeur, son étiquette ou son groupe. */
  bool mDataShow;
  /** Vaut \a true s'il faut y afficher l'étiquette de la donnée. */
  bool mShowLabel;
  /** Vaut \a true s'il faut y afficher l'étiquette relative à la valeur active de la donnée. */
  bool mShowChoiceLabel;
  /** Mode d'affichage du groupe. */
  GroupSection mShowGroup;

  /** Vaut \a true si la gestion des couleurs process est active. */
  bool mEnableProcessColor;
  /** Couleur d'affichage utilisée lorsque la valeur est valide si mEnableProcessColor vaut \a true.  */
  QColor mEnableColor;
  /** Couleur d'affichage utilisée lorsque la valeur n'est pas valide si mEnableProcessColor vaut \a true.  */
  QColor mDisableColor;
};

#endif
