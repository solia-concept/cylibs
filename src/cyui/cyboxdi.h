/***************************************************************************
                          cyboxdi.h  -  description
                             -------------------
    début                  : Sun Feb 23 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYBOXDI_H
#define CYBOXDI_H

// CYLIBS
#include "cydi.h"
#include "cyboxdig.h"
#include "cyled.h"
//Added by qt3to4:
#include <QLabel>

/** CYBoxDI est un ensemble de widgets représentant une entrée TOR.
  * @short Représentation d'entrée TOR.
  * @author Gérald LE CLEACH
  */

class CYBoxDI : public CYBoxDIG
{
  Q_OBJECT
  
public: 
  /** Construit un objet représentant une entrée TOR.
    * @param parent  Widget parent.
    * @param name    Nom du widget.
    * @param c       Couleur de la voie.
    * @param m       Mode d'utilisation du widget. */
  CYBoxDI(QWidget *parent=0, const QString &name=0, QColor c=QColor(Qt::red), Mode m=View);
  /** Destructeur */
  ~CYBoxDI();

  /** Initialise le widget. */
  virtual void init();
  /** Associe une donnée à l'objet.
    * @param data Donnée à traîter. */
  void setData(CYDI *data);
  
public slots: // Public slots
  /** Met à jour l'état de la LED. */
  virtual void refresh();
  /** Simulation: permute l'état de la led. */
  virtual void simulation();

protected: // Protected methods
  /** Met à jour les widgets. */
  virtual void update();

protected: // Protected attributes
  /** Donnée connectée. */
  CYDI *mData;
  /** Etiquette. */
  QLabel *mLabel;
  /** Led de visu. */
  CYLed *mLed;
};

#endif
