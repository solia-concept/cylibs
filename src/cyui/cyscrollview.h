/***************************************************************************
                          cyscrollview.h  -  description
                             -------------------
    début                  : dim aoû 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYSCROLLVIEW_H
#define CYSCROLLVIEW_H

#include <QWidget>
#include <QScrollArea>

/**
  *@author Gérald LE CLEACH
  */

class CYScrollView : public QScrollArea
{
   Q_OBJECT
public:
  CYScrollView(QWidget *parent=0, const QString &name=0);
  ~CYScrollView();
};

#endif
