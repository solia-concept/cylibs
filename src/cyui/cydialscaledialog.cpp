#include "cydialscaledialog.h"
#include "ui_cydialscaledialog.h"

CYDialScaleDialog::CYDialScaleDialog(QWidget *parent, const QString &name)
  : CYDialog(parent, name), ui(new Ui::CYDialScaleDialog)
{
  ui->setupUi(this);
}


CYDialScaleDialog::~CYDialScaleDialog()
{
  delete ui;
}

void CYDialScaleDialog::setData(CYData *min, CYData *max, CYData *step)
{
  ui->min->setData(min);
  ui->max->setData(max);
  ui->step->setData(step);
}
