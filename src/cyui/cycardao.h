/***************************************************************************
                          cycardao.h  -  description
                             -------------------
    début                  : jeu fév 13 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCARDAO_H
#define CYCARDAO_H


/**
  *@author Gérald LE CLEACH
  */

class CYCardAO {
public: 
	CYCardAO();
	~CYCardAO();
};

#endif
