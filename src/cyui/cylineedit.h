/***************************************************************************
                          cylineedit.h  -  description
                             -------------------
    début                  : lun mar 3 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYLINEEDIT_H
#define CYLINEEDIT_H

// QT
#include <QLineEdit>
#include <QStyle>
// CYLIBS
#include "cydatawdg.h"

/** CYLineEdit est un QLineEdit pouvant être associée à une donnée CYData.
  * @short Boîte d'édition.
  * @author Gérald LE CLEACH
  */

class CYLineEdit : public QLineEdit, public CYDataWdg
{
  Q_OBJECT
  Q_ENUMS(Type)
  Q_PROPERTY(bool movable READ movable WRITE setMovable)
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray posXName READ posXName WRITE setPosXName)
  Q_PROPERTY(QByteArray posYName READ posYName WRITE setPosYName)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(int genericMarkOffset READ genericMarkOffset WRITE setGenericMarkOffset)
  Q_PROPERTY(bool alwaysOk READ alwaysOk WRITE setAlwaysOk)
  Q_PROPERTY(Type type READ type WRITE setType)
  Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize)
  Q_PROPERTY(QString suffix READ suffix WRITE setSuffix)
  Q_PROPERTY(QString prefix READ prefix WRITE setPrefix)
  Q_PROPERTY(QString specialValueText READ specialValueText WRITE setSpecialValueText)
  Q_PROPERTY(bool bold READ bold WRITE setBold)
  Q_PROPERTY(QColor enableColor READ enableColor WRITE setEnableColor)
  Q_PROPERTY(QColor disableColor READ disableColor WRITE setDisableColor)
  Q_PROPERTY(bool autoRefresh READ autoRefresh WRITE setAutoRefresh)
  Q_PROPERTY(bool enableReadOnly READ enableReadOnly WRITE setEnableReadOnly)
  Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
  Q_PROPERTY(QColor foregroundColor READ foregroundColor WRITE setForegroundColor)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:
  /** Type de boîte. */
  enum Type
  {
    /** Affichage configurable à souhait. */
    Free,
    /** Saisie d'une valeur. */
    Input,
    /** Affichage d'une valeur process. */
    Process,
    /** Affichage d'une valeur locale. */
    Local,
  };

  CYLineEdit(QWidget *parent=0, const QString &name=0);

  virtual ~CYLineEdit();

  /** @return \a true si l'objet peut être déplacé.
    * @see void setMovable(const bool val). */
  virtual bool movable() const;
  /** Saisie \a true pour pouvoir déplacer. */
  virtual void setMovable(const bool val);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

//  /** @return le nom de la donnée traîtée. */
//  virtual QByteArray dataName() const;
//  /** Saisie le nom de la donnée traîtée. */
//  virtual void setDataName(const QByteArray &name);

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le nom de la donnée de positionnement en X. */
  virtual QByteArray posXName() const;
  /** Saisie le nom de la donnée de positionnement en X. */
  virtual void setPosXName(const QByteArray &name);

  /** @return le nom de la donnée de positionnement en Y. */
  virtual QByteArray posYName() const;
  /** Saisie le nom de la donnée de positionnement en Y. */
  virtual void setPosYName(const QByteArray &name);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return l'offset sur le repère de généricité lorqu'il s'agit d'un numéro. */
  virtual int genericMarkOffset() const;
  /** Saisie l'offset sur le repère de généricité lorqu'il s'agit d'un numéro. */
  virtual void setGenericMarkOffset(const int val);

  /** @return \a true si le widget est en lecture seule. */
  bool readOnly() const;
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);

  /** @return \a true la donnée traîtée est considérée comme toujours valide. */
  virtual bool alwaysOk() const;
  /** La donnée traîtée est considérée comme toujours valide si \a val vaut true. */
  virtual void setAlwaysOk(const bool val);

  /** @return \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  bool autoRefresh() const { return mAutoRefresh; }
  /** Saisir \a true pour que le widget soit rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  void setAutoRefresh(const bool val) { mAutoRefresh = val; }

  /** @return \a true si le widget est en mode saisie. */
  bool enableReadOnly() const { return mEnableReadOnly; }
  /** Saisir \a true pour que le widget soit en mode saisie. */
  void setEnableReadOnly(const bool val) { mEnableReadOnly = val; }

  /** @return la taille de la police utilisée ici uniquement pour le type CYLineEdit::Process.
   * Cette taille est initialisée par une taille de police par défaut avec un offset global à toute l'application.
   * Cette taille peut cependant être forcée par CYLineEdit::setFontSize.
   * Pour la taille des autres types il faut passer par QWidget::setFont. */
  virtual int fontSize() const;
  /** Saisie la taille de la police utilisée ici uniquement pour le type Process. */
  virtual void setFontSize(const int val);

  /** Saisie la couleur de fond. */
  virtual void setBackgroundColor(const QColor &color);
  /** Saisie la couleur du premier plan. */
  virtual void setForegroundColor(const QColor &color);
  /** Saisie la palettes de couleurs.  */
  virtual void setPalette(const QPalette &palette);
  /** Saisie la palettes de couleurs.
    * @param type S'il vaut \a true alors la palette saisie est enregistrée comme nouvelle palette du type en cours. */
  virtual void setPalette(const QPalette &palette, bool type);

  /** @return le type de boîte. */
  Type type() const;
  /** @return le suffixe.
    * @see setSuffix(). */
  QString suffix() const;
  /** @return le préfixe.
    * @see setPrefix(). */
  QString prefix() const;
  /** @return le texte affiché pour une valeur paticulière.
    * @see setSpecialValueText() */
  QString specialValueText() const { return mSpecialValue; }
  /** @return \a true si le texte est en gras. */
  bool bold() const { return mBold; }

  /** @return la couleur d'affichage utilisée lorsque la valeur est valide.  */
  QColor enableColor () const { return mEnableColor; }
  /** @return la couleur d'affichage utilisée lorsque la valeur n'est pas valide.  */
  QColor disableColor () const { return mDisableColor; }

  /** @return la taille recommandée de l'afficheur en fonction de @see sizePolicy().
      @return @see minimumSizeHint() si un au moins des composant horizontal ou vertical de @a sizePolicy() est au minimum. */
  virtual QSize sizeHint() const;
  /** @return la taille maximum de l'afficheur. */
  virtual QSize maximumSizeHint () const;
  /** @return la taille minimum de l'afficheur. */
  virtual QSize minimumSizeHint () const;

  virtual void moveInsideParent(int x, int y);
  virtual void setEditPositions( bool val );

  /** @return la palette courante. */
  virtual const QPalette & palette() const { return QLineEdit::palette(); }
  /** @return la palette du \a type */
  virtual QPalette palette(Type type);
  /** @return la palette du \a type */
  virtual void setPalette(QPalette palette, Type type);

signals: // Signals
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();
  /** Emis lorsque le widget reçoit le focus. */
  void focusIn();
  /** Emis à chaque déplacement à la souris afin de définir un ordre dans la pile de widget du parent. */
  void movedInsideParent();

public slots: // Public slots
  /** Configure la boîte en fonction du type d'affichage. */
  virtual void setType();
  /** Saisie le type de boîte.
    * @param type Type de boîte. */
  void setType(const Type type);
  /** Saisie le suffixe à afficher. Pour désactiver cette propriété il
    * suffit d'utiliser QString().
    * @see suffix() */
  void setSuffix(const QString &suffix);
  /** Saisie le préfixe à afficher. Pour désactiver cette propriété il
    * suffit d'utiliser QString().
    * @see preffix() */
  void setPrefix(const QString &prefix);
  /** Saisie le texte de la valeur spéciale. */
  void setSpecialValueText(const QString& text);
  /** Place le texte de la valeur spéciale. */
  void putSpecialValueText();
  /** Affiche le texte en gras si \a state vaut \a true. */
  void setBold(bool state);
  /** Désactive le widget. */
  virtual void setEnabled(bool);

  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer() {}
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  virtual void refreshPosition();
  virtual void updatePosition();

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

  /** Valide le contenu de la boîte. */
  virtual void setValidate() {}
  /** Traîtement à chaque changement de text. */
  virtual void setTextChanged(const QString &text) { Q_UNUSED(text); }

  /** Initialise les différentes palettes de couleurs. */
  void initPalettes();

  /** Saisie la couleur d'affichage utilisée lorsque la valeur est valide.  */
  void setEnableColor (const QColor &c);
  /** Saisie la couleur d'affichage utilisée lorsque la valeur n'est pas valide.  */
  void setDisableColor (const QColor &c);

  /** @return la couleur de fond du widget */
  virtual const QColor & backgroundColor () const;

  /** @return la couleur de devant du widget */
  virtual const QColor & foregroundColor () const;

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  virtual void setEnabledTmp(bool val);

protected: // Protected methods
  /** Gestion des évènements. */
  virtual bool event(QEvent *e);
  /** Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
     Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);
  virtual void resetEditBox() {}
  /** Gestion de la réception du focus. */
  void focusInEvent(QFocusEvent *e);
  virtual void showEvent(QShowEvent *ev);
  virtual void contextMenuEvent( QContextMenuEvent * event );
  virtual void mousePressEvent ( QMouseEvent * e );
  virtual void mouseMoveEvent ( QMouseEvent * e );

private: // Private methods
  void init();

protected: // Protected attributes
  QSize   mSizeEdit;
  Type    mType;
  bool    mWasOk;
  QString mSpecialValue, mPrefix, mSuffix;
  bool    mBold;
  /** Couleur d'affichage utilisée lorsque la valeur est valide.  */
  QColor mEnableColor;
  /** Couleur d'affichage utilisée lorsque la valeur n'est pas valide.  */
  QColor mDisableColor;

  /** Palettes des différents type d'affichage (Type). */
//  QIntDict<QPalette> mPalettes;
  typedef QMap<Type, QPalette> mPalettesMap;
  mPalettesMap mPalettes;
};

#endif
