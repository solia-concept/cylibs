//
// C++ Implementation: cyventilator
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
// CYLIBS
#include "cyventilator.h"

CYVentilator::CYVentilator(QWidget *parent, const QString &name)
  : CYMovieView(parent,name)
{
  mState = true;
  setAttribute(Qt::WA_OpaquePaintEvent);
  initPalette();
  setPixmapFile(":/pixmaps/cyventilator_off.png");
  setMovieFile(":/pixmaps/cyventilator_on.gif");
  setState(mState);
}

CYVentilator::~CYVentilator()
{
}
