#ifndef CYPLOTPICKER_H
#define CYPLOTPICKER_H

// QT
#include <QMouseEvent>
// QWT
#include <qwt_plot_picker.h>

class CYPlotPicker : public QwtPlotPicker
{
  Q_OBJECT
public:
  explicit CYPlotPicker( QWidget *canvas );

  explicit CYPlotPicker(int xAxis, int yAxis,
                        RubberBand rubberBand,
                        DisplayMode trackerMode,
                        QWidget *);

signals:
  void mousePressed(const QMouseEvent &e);
  void mouseReleased(const QMouseEvent &e);

public slots:


protected:
  virtual void widgetMousePressEvent(QMouseEvent * mouseEvent);
  virtual void widgetMouseReleaseEvent(QMouseEvent * mouseEvent);

};

#endif // CYPLOTPICKER_H
