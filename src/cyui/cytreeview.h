//
// C++ Interface: cylistview
//
// Description:
//
//
// Author: Gérald LE CLEACH <g.lecleach@clemessy.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYLISTVIEW_H
#define CYLISTVIEW_H

// QT
#include <QTreeWidget>


/**
@short QListView enrichi.

@author Gérald LE CLEACH
*/
class CYListView : public QTreeWidget
{
Q_OBJECT
public:
    CYListView(QWidget *parent = 0, const QString &name = 0);

    ~CYListView();

    virtual void setSelected( QTreeWidgetItem *item, bool selected );
};

#endif
