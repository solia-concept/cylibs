//
// C++ Interface: cytable
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYTABLE_H
#define CYTABLE_H

#include <QTableWidget>

/**
@short Table

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYTable : public QTableWidget
{
Q_OBJECT
public:
    CYTable(QWidget *parent = 0, const QString &name = 0);

    ~CYTable();

public slots:
    /** Impression tableau */
    virtual void print(QPainter *p, QRectF *r);
    /** Saisie le texte d'une cellule à la ligne \a row et à la colonne \a col. */
    virtual void setText(int row, int col, QString text);
};

#endif
