/***************************************************************************
                          cycarddig.cpp  -  description
                             -------------------
    début                  : jeu fév 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// QT
#include <qapplication.h>
// CYLIBS
#include "cyboxdig.h"
#include "cycarddig.h"

CYCardDIG::CYCardDIG(QWidget *parent, const QString &name, int no, Mode m)
: CYCardIO(parent, name, no, m)
{
  mMaxCols = 20;
  mNoCol   = 0;

  mColors.clear();
  for (int col=0; col<mMaxCols; col++)
    mColors.insert(col, new QColor(Qt::red));
}

CYCardDIG::~CYCardDIG()
{
}

QSize CYCardDIG::sizeHint() const
{
  return QSize(48, 48);
}

QSize CYCardDIG::minimumSizeHint() const
{
  return QSize(fontMetrics().horizontalAdvance(title()), fontMetrics().height());
}
