/***************************************************************************
                          cymvinput.h  -  description
                             -------------------
    begin                : jeu déc 30 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYMVINPUT_H
#define CYMVINPUT_H

// CYLIBS
#include <cywidget.h>

class CYMV;

namespace Ui {
    class CYMVInput;
}


/** @short Boîte de saisie de mouvement.
  * @author LE CLÉACH Gérald
  */

class CYMVInput : public CYWidget
{
  Q_OBJECT

public:
  CYMVInput(QWidget *parent=0, const QString &name=0);
  ~CYMVInput();

  /** Saisie le repère de généricité utile aux widgets génériques.
    * Par example un widget dans certain cas peut servir pour 2 postes.
    * Les noms des données pour les 2 postes doivent se différencier par le
    * numéro du poste. Il suffit alors de remplacer cette partie du nom par
    * un # dans dataName, flagName, forcingName et data2Name des widgets enfants.
    * Ex: DATA_POSTE1 & DATA_petit_poste1 => DATA_POSTE#
    * La rechercher des données remplacera automatiquement ce # par
    * le numéro du bon poste passé ici comme index. */
  virtual void setGenericMark(const QString txt);

  /** Connection des objets graphiques avec les données de l'application. */
  virtual void linkDatas();

protected:
    CYMV *mMV;

private:
    Ui::CYMVInput *ui;
};

#endif
