/***************************************************************************
                          cytextinput.h  -  description
                             -------------------
    début                  : mer sep 3 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYTEXTINPUT_H
#define CYTEXTINPUT_H

// CYLIBS
#include "cylineedit.h"

/** CYTextInput est une boîte de saisie d'une donnée texte.
  * @short Boîte de saisie d'une donnée text.
  * @author Gérald LE CLEACH
  */

class CYTextInput : public CYLineEdit
{
  Q_OBJECT
  
public: 
  CYTextInput(QWidget *parent=0, const QString &name="textInput");
  ~CYTextInput();

  /** @return la valeur courrante. */
  QString value() const;
  /** Fixe la valeur courante à \a value. */
  void setValue(QString value);
  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

public slots:
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  /** Traîtement à chaque changement de text. */
  virtual void setTextChanged(const QString &text);

  /** Configure le widget en fonction de la donnée à laquelle il est lié. */
  virtual void linkData();
};

#endif
