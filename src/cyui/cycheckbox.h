/***************************************************************************
                          cycheckbox.h  -  description
                             -------------------
    début                  : Sun Feb 23 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCHECKBOX_H
#define CYCHECKBOX_H

// QT
#include <qcheckbox.h>
//Added by qt3to4:
#include <QResizeEvent>
#include <QString>
#include <QKeyEvent>
#include <QFocusEvent>
// CYLIBS
#include "cydatawdg.h"

/** CYCheckBox est une classe de base pour réaliser des case à cocher relatives
  * à des données.
  * @short Case à cocher d'une donnée.
  * @author Gérald LE CLEACH
  */

class CYCheckBox : public QCheckBox, public CYDataWdg
{
  Q_OBJECT
  Q_ENUMS(GroupSection)
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool inputHelp READ inputHelp WRITE setInputHelp)
  Q_PROPERTY(int lenText READ lenText WRITE setLenText)
  Q_PROPERTY(bool showLabel READ showLabel WRITE setShowLabel)
  Q_PROPERTY(GroupSection showGroup READ showGroup WRITE setShowGroup)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:

  /** Section de groupe de données. */
  enum GroupSection
  {
    /** Pas de demande relative au groupe. */
    No,
    /** Demande relative au dernier sous-groupe.. */
    lastGroup,
    /** Demande relative aux sous-groupes. */
    underGroup,
    /** Demande relative au groupe entier. */
    wholeGroup
  };

  CYCheckBox(QWidget *parent=0, const QString &name=0);
  CYCheckBox(const QString &text, QWidget *parent, const QString &name=0);
  ~CYCheckBox();

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** Utilise l'aide ou pas de saisie (valeur constructeur). */
  void setInputHelp(bool input=false) { mInputHelp = input; }
  /** @return si \a true utilise l'aide de saisie (valeur constructeur) sinon l'aide d'affichage. */
  bool inputHelp() const { return mInputHelp; }

  /** Saisie le nombre de caractères à dessiner, si \a len = -1 (par defaut) alors tout le texte est dessiné. */
  void setLenText(const int len) { mLenText = len; }
  /** @return le nombre de caractères à dessiner. */
  int lenText() const { return mLenText; }

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** @return \a true s'il faut y afficher l'étiquette de la donnée. */
  virtual bool showLabel() const { return mShowLabel; }
  /** Saisir \a true pour y afficher l'étiquette de la donnée. */
  virtual void setShowLabel(const bool val) { mShowLabel = val; setAlwaysOk(true); }

  /** @return le mode d'affichage du groupe. */
  virtual GroupSection showGroup() const;
  /** Saisir le mode d'affichage du groupe. */
  virtual void setShowGroup(const GroupSection val);

  /** @return la valeur courrante. */
  virtual bool value() const;
  /** Fixe la valeur courante à \a value. */
  virtual void setValue(bool value);

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

signals:
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();
  /** Emis lorsque le widget reçoit le focus. */
  void focusIn();
  /** Emis lorsque le widget perd le focus. */
  void focusOut();
  void clearFocus(QKeyEvent *e);
  void stateOn(bool);

public slots: // Public slots
  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

protected slots: // Protected slots
  virtual void setStateChanged(int index);

protected: // Protected methods
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
     Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);
  /** Gestion de la réception du focus. */
  virtual void focusInEvent(QFocusEvent *e);
  /** Gestion de la perte du focus. */
  virtual void focusOutEvent(QFocusEvent *);
  /** Gestion du clavier. */
  virtual void keyPressEvent(QKeyEvent *);
  /** Gestion du redimensionnement. */
  virtual void resizeEvent(QResizeEvent *e);

private: // Private methods
  void init();

protected: // Protected attributes
  /** Si \a true utilise l'aide de saisie (valeur constructeur) sinon l'aide d'affichage. */
  bool mInputHelp;
  /** Nombre de caractères à dessiner. */
  int mLenText;
  /** Vaut \a true s'il faut y afficher l'étiquette de la donnée. */
  bool mShowLabel;
  /** Mode d'affichage du groupe. */
  GroupSection mShowGroup;
};

#endif
