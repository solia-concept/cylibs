//
// C++ Implementation: cyextmeasureedit
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <QTabWidget>
// CYLIBS
#include "cycore.h"
#include "cyextmeasureedit.h"
#include "cyextmeasure.h"
#include "cytextlabel.h"
#include "ui_cyextmeasureedit.h"

CYExtMeasureEdit::CYExtMeasureEdit(QWidget* parent, const QString &name)
  : CYWidget(parent,name), ui(new Ui::CYExtMeasureEdit)
{
  ui->setupUi(this);
	int nb = ui->tabWidget->count();
	for (int i=0; i<nb; i++)
	{
		CYExtMeasureEditPage *page = new CYExtMeasureEditPage;
		page->widget = ui->tabWidget->widget( i );
		page->label = ui->tabWidget->tabText( i );
		page->enable = true;
		mPage.insert(i, page);
	}

  mMeasure = 0;
	setHideLabel(false);
  setEnableCT(false);
}


CYExtMeasureEdit::~CYExtMeasureEdit()
{
  delete ui;
}


/*! Applique les données.
    \fn CYExtMeasureEdit::apply()
 */
void CYExtMeasureEdit::apply()
{
  CYWidget::apply();
	if ( mMeasure )
	{
    mMeasure->updateFormat();
    refresh();
  }
}


void CYExtMeasureEdit::removePage(int index)
{
	CYExtMeasureEditPage *page = mPage[index];
	if (page->enable)
	{
		ui->tabWidget->removeTab( index );
		page->enable=false;
	}
}

/*! Connection des objets graphiques avec les données de l'application
    \fn CYExtMeasureEdit::linkDatas()
 */
void CYExtMeasureEdit::linkDatas()
{
  CYWidget::linkDatas();

  if ( ui->label->CYDataWdg::data() && (ui->label->CYDataWdg::data()->inherits("CYMeasure") ) )
  {
		setHidden(true);
		int nb = ui->tabWidget->count();
		for (int i=nb-1; i>-1; i--)
			ui->tabWidget->removeTab( i );

	mMeasure = (CYExtMeasure *)ui->label->CYDataWdg::data();
		for (int i = 0; i < mPage.count(); ++i)
		{
			CYExtMeasureEditPage *page = mPage[i];

			page->enable=true;
			if ( (i==0) && !mMeasure->editScale() && !mMeasure->editFormat())
				page->enable=false;
			if ( (i==1) && !mMeasure->editSupervision() )
				page->enable=false;

			if (page->enable)
			{
				int nb = ui->tabWidget->count();
				page->index = nb;
				ui->tabWidget->insertTab(page->index, page->widget, page->label);
			}
		}

		if (mMeasure && enableCT())
		{
			QString mark;
			mark = QString("DCT_%1").arg(mMeasure->objectName());
			ui->CTInput->setGenericMark(mark.toLocal8Bit());
		}
		else if (core) // pas dans Qt Designer
			ui->CTInput->hide();

    mLinkDatasShow=false;
    setHidden(false);
    mLinkDatasShow=true;
    updateGeometry();
  }
}

CYExtMeasureEdit::GroupSection CYExtMeasureEdit::showGroup() const
{
  CYTextLabel *label = ui->label;
  return (CYExtMeasureEdit::GroupSection)label->showGroup();
}

void CYExtMeasureEdit::setShowGroup(const GroupSection val)
{
  ui->label->setShowGroup((CYTextLabel::GroupSection) val);
}

bool CYExtMeasureEdit::enableCT() const
{
  return mEnableCT;
}

void CYExtMeasureEdit::setEnableCT(const bool val)
{
  mEnableCT = val;
  if (val)
    ui->CTInput->show();
  else
		ui->CTInput->hide();
}

bool CYExtMeasureEdit::hideLabel() const
{
	return mHideLabel;
}

void CYExtMeasureEdit::setHideLabel(const bool val)
{
	mHideLabel = val;
	if (val)
		ui->label->hide();
	else
		ui->label->show();
}
