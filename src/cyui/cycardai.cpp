/***************************************************************************
                          cycardai.cpp  -  description
                             -------------------
    début                  : jeu fév 13 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cycardai.h"

CYCardAI::CYCardAI(QWidget *parent, const QString &name)
  : QFrame(parent)
{
  setObjectName(name);

  init();
}

CYCardAI::~CYCardAI()
{
}

void CYCardAI::init()
{
}

void CYCardAI::updateCard()
{
}
