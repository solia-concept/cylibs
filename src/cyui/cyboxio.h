/***************************************************************************
                          cyboxio.h  -  description
                             -------------------
    début                  : jeu mar 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYBOXIO_H
#define CYBOXIO_H

// QT
#include <QFrame>
#include <QLabel>
#include <QAbstractButton>
#include <QHBoxLayout>

class CYData;
class CYCardIO;

/** CYBoxIO est un ensemble de widgets représentant une E/S.
  * @short Représentation d'une E/S.
  * @author Gérald LE CLEACH
  */

class CYBoxIO : public QFrame
{
  Q_OBJECT
//   Q_ENUMS(Mode)
  Q_PROPERTY(Mode mode READ mode WRITE setMode)

public:
  /** @short Mode d'utilisation de l'objet. */
  enum Mode
  {
    /** Visualise l'état de l'E/S. */
    View,
    /** Visualise l'état de l'E/S et permet de forcer de la sortie. */
    Forcing
  };

  /** Construit un objet représentant une E/S.
    * @param parent  Widget parent.
    * @param name    Nom du widget.
    * @param m       Mode d'utilisation du widget. */
  CYBoxIO(QWidget *parent=0, const QString &name=0, Mode m=View);

  /** Destructeur */
  ~CYBoxIO();

  /** Initialise le widget. */
  virtual void init() {}

  /** @return le mode d'utilisation du widget. */
  Mode mode() const;
  /** Saisie le mode d'utilisation du widget. */
  void setMode(const Mode mode);

  /** @return \a true si une donnée est connectée. */
  bool hasData() { return mHasData; }

  /** Insert un bouton contenu dans la trame dans un QButtonGroup parent ou grand parent. */
  void insertButton(QAbstractButton * button);
  /** Enlève un bouton contenu dans la trame d'un QButtonGroup parent ou grand parent. */
  void removeButton(QAbstractButton * button);

  QHBoxLayout *hl;

signals:
  /** Emet un signal d'aide sur cette donnée. */
  void help(const QString &msg);

public slots: // Public slots
  virtual void refresh() {}
  /** Simulation. */
  virtual void simulation() {}
  /** Gestion de la réception du focus. */
  virtual void focusIn() {}
  /** Gestion de la perte du focus. */
  virtual void focusOut() {}

protected: // Protected methods
  /** Met à jour l'objet. */
  virtual void update() {}

protected: // Protected attributes
  /** Carte d'E/S. */
  CYCardIO *mCard;
  /** Titre de la boîte. */
  QString mTitle;
  /** Mode d'utilisation du widget. */
  Mode mMode;
  /** Vaut \a true si la gestion du mode d'utilisation est géré. */
  bool mEnableMode;
  /** Vaut \a true si une donnée est connectée. */
  bool mHasData;
};

#endif
