//
// C++ Interface: cypopupmenu
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYPOPUPMENU_H
#define CYPOPUPMENU_H

// QT
#include <QMenu>

/**
@short Menu contextuel.
Un tel menu contextuel peut être créé au niveau de l'affaire et ajouté à des objets graphiques de la libraririe tel que CYTextEdit pour être appelé ensuite avec le menu contextuel de cet objet.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYPopupMenu : public QMenu
{
Q_OBJECT
public:
    CYPopupMenu(QString text, bool readOnly=true, QWidget *parent = 0, const QString &name = 0);

    ~CYPopupMenu();

    /** @return \a true si ce menu ne doit être affiché uniquement si l'objet dont il est appelé est en lecture seule. */
    bool readOnly() { return mReadOnly; }
    /** @return le texte de ce menu. */
    virtual QString text();

public slots:
    /** Saisie le texte de ce menu. */
    virtual void setText(QString txt);
    /** Insert ce menu dans le menu contextuel \a pm de l'objet graphique \a from qui le génère. */
    virtual void insertInto(QWidget *from, QMenu *pm);

protected:
    QString mText;
    bool mReadOnly;
signals:
    /** Emis lors de l'insertion de ce menu dans le menu contextuel de l'objet graphique \a from qui le génère. */
    void inserted(QWidget *from);
};

#endif
