/***************************************************************************
                          cydatawdg.h  -  description
                             -------------------
    début                  : mer avr 23 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDATAWDG_H
#define CYDATAWDG_H

// QT
#include <QWidget>
#include <QHash>
#include <QToolTip>
// CYLIBS
#include "cy.h"

class CYDB;
class CYData;
class CYFlag;
class CYS32;
class CYDialog;
class CYWidget;
class CYFrame;
class CYDisplaySimple;

/** CYDataWdg est une classe de base pour réaliser des widgets représentatifs d'une donnée
  * provenant d'un dictionnaire QDict utilisé comme base de données. Cette classe offre
  * ses classes filles plusieurs mécanismes ayant pour but de simplifier  l'impémentation des
  * différentes intéractions possibles entre la donnée et le widget qui la représante.
  * @short Classe de base des widgets représentatifs d'une donnée.
  * @author Gérald LE CLEACH
  */

class CYDataWdg : public Cy
{
public:
  CYDataWdg(QWidget *widget);
  virtual ~CYDataWdg();

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le nom de la donnée flag associée de changement de couleur de cet objet graphique. */
  virtual QByteArray colorFlagName() const;
  /** Saisie le nom de la donnée flag associée de changement de couleur de cet objet graphique. */
  virtual void setColorFlagName(const QByteArray &name);

  /** @return si le flag associé de changement de couleur doit être pris en compte dans le sens inverse. */
  virtual bool inverseColorFlag() const;
  /** Inverse le sens du flag associé de changement de couleur si \a inverse est à \p true. */
  virtual void setInverseColorFlag(const bool inverse);

  /** @return le nom de la donnée de forçage associée. */
  virtual QByteArray forcingName() const;
  /** Saisie le nom de la donnée de forçage associée. */
  virtual void setForcingName(const QByteArray &name);

  /** @return le nom de la donnée supplémentaire \a num associée. */
  virtual QByteArray dataSupName(int num) const;
  /** Saisie le nom de la donnée supplémentaire \a num associée. */
  virtual void setDataSupName(int num, const QByteArray &name);

  /** @return le nom de la donnée de positionnement en X. */
  virtual QByteArray posXName() const;
  /** Saisie le nom de la donnée de positionnement en X. */
  virtual void setPosXName(const QByteArray &name);

  /** @return le nom de la donnée de positionnement en Y. */
  virtual QByteArray posYName() const;
  /** Saisie le nom de la donnée de positionnement en Y. */
  virtual void setPosYName(const QByteArray &name);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return \a true si le widget est en lecture seule. */
  virtual bool readOnly() const;
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);

  /** @return \a true si le widget est en mode forçage. */
  virtual bool forcingMode() const;
  /** Met le widget en mode forçage ou pas suivant \a val. */
  virtual void setForcingMode(const bool val);

  /** @return \a true si le widget est en mode paramétrage. */
  virtual bool settingMode() const;
  /** Met le widget en mode paramétrage ou pas suivant \a val. */
  virtual void setSettingMode(const bool val);

  /** @return \a true la donnée traîtée est considérée comme toujours valide. */
  virtual bool alwaysOk() const;
  /** La donnée traîtée est considérée comme toujours valide si \a val vaut true. */
  virtual void setAlwaysOk(const bool val);

  /** @return \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  bool autoRefresh() const { return mAutoRefresh; }
  /** Saisir \a true pour que le widget soit rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  void setAutoRefresh(const bool val) { mAutoRefresh = val; }

  /** @return \a true alors la mise à jour de la donnée par la valeur
    * saisie par le widget n'est possible que si celui-ci est visible. */
  bool updateIfVisible() const { return mUpdateIfVisible; }
  /** Saisir \a true pour que la mise à jour de la donnée par la valeur
    * saisie par le widget ne soit possible que si celui-ci est visible. */
  void setUpdateIfVisible(const bool val) { mUpdateIfVisible = val; }

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** @return la taille de la police.
   * Cette taille est initialisée par une taille de police par défaut avec un offset global à toute l'application.
   * Cette taille peut cependant être forcée par CYLabel::setFontSize. */
  virtual int fontSize() const { return mFontSize; }
  /** Saisie la taille de la police utilisée. */
  virtual void setFontSize(const int val) { mFontSize = val; }

  /** @return \a true si la taille minimum est calculée automatiquement par rapport à la donnée.
    * Valable pour CYNumInput seulement. */
  virtual bool autoMinimumSize() const { return mAutoMinimumSize; }
  /** Saisir \a true pour que la taille minimum soit calculée automatiquement par rapport à la donnée.
    * Valable pour CYNumInput seulement. */
  virtual void setAutoMinimumSize(const bool val) { mAutoMinimumSize = val; }

  /** @return l'offset sur le repère de généricité lorsqu'il s'agit d'un numéro. */
  virtual int genericMarkOffset() const { return mGenericMarkOffset; }
  /** Saisie l'offset sur le repère de généricité lorsqu'il s'agit d'un numéro. */
  virtual void setGenericMarkOffset(const int val) { mGenericMarkOffset = val; }

  /** @return \a true si l'objet graphique doît être connecté à une donnée. */
  bool hasData();
  /** @return la donnée traîtée. */
  CYData *data();
  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);
  /** @return la base de données locale à ce widget. */
  CYDB *localeDB() { return mDB; }
  /** Saisie la base de données locale à ce widget. */
  void setLocaleDB(CYDB *db);
 /** Recherche, dans la base de données globale, la donnée ayant
    * pour nom celui saisie par la fonction setdataName() et la place
    * comme donnée du widget. */
  bool findData();
  /** Recherche, dans la base de données \a db, la donnée ayant
    * pour nom celui saisie par la fonction setdataName() et la place
    * comme donnée du widget. */
  bool findData(QHash<QString, CYData*> *db);

  /** @return \a true si l'objet graphique doît être contrôlé par une donnée. */
  bool hasFlag();
  /** @return le flag de contrôle. */
  CYData *flag();
  /** Place @p data comme flag du widget. */
  virtual void setFlag(CYData *data);
  /** Recherche, dans la base de données globale, le flag ayant
    * pour nom celui saisie par la fonction setFlagName() et la place
    * comme donnée de contrôle du widget. */
  bool findFlag();
  /** Recherche, dans la base de données \a db, le flag ayant
    * pour nom celui saisie par la fonction setFlagName() et la place
    * comme donnée de contrôle du widget. */
  bool findFlag(QHash<QString, CYData*> *db);
  /** Contrôle le flag. */
  virtual bool ctrlFlag() { return true; }

  /** @return \a true si l'objet graphique doît être affiché par une donnée. */
  bool hasHideFlag();
  /** @return le flag d'affichage. */
  CYFlag *hideFlag();
  /** Place @p data comme flag d'affichage du widget. */
  virtual void setHideFlag(CYData *data);
  /** Recherche, dans la base de données globale, le flag ayant
    * pour nom celui saisie par la fonction setHideFlagName() et la place
    * comme donnée d'affichage du widget. */
  bool findHideFlag();
  /** Recherche, dans la base de données \a db, le flag ayant
    * pour nom celui saisie par la fonction setHideFlagName() et la place
    * comme donnée d'affichage du widget. */
  bool findHideFlag(QHash<QString, CYData*> *db);
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag() {}

  /** @return \a true si l'objet graphique doît changer de couleur par une donnée. */
  bool hasColorFlag();
  /** @return le flag de changement de couleur. */
  CYFlag *colorFlag();
  /** Place @p data comme flag de changement de couleur du widget. */
  virtual void setColorFlag(CYData *data);
  /** Recherche, dans la base de données globale, le flag ayant
    * pour nom celui saisie par la fonction setColorFlagName() et la place
    * comme donnée de changement de couleur du widget. */
  bool findColorFlag();
  /** Recherche, dans la base de données \a db, le flag ayant
    * pour nom celui saisie par la fonction setColorFlagName() et la place
    * comme donnée de changement de couleur du widget. */
  bool findColorFlag(QHash<QString, CYData*> *db);
  /** Contrôle le flag de changement de couleur et affiche l'objet graphique suivant la couleur. */
  virtual void ctrlColorFlag() {}

  /** @return \a true si l'objet graphique peut être forcé par une donnée. */
  bool hasForcingData();
  /** @return la donnée de forçage. */
  CYData *forcingData();
  /** Place @p data comme la donnée de forçage du widget. */
  virtual void setForcingData(CYData *data);
  /** Recherche, dans la base de données globale, la donnée de forçage ayant
    * pour nom celui saisie par la fonction setForcingName() et la place
    * comme donnée de forçage du widget. */
  bool findForcing();
  /** Recherche, dans la base de données \a db, la donnée ayant
    * pour nom celui saisie par la fonction setForcingName() et la place
    * comme donnée de forçage du widget. */
  virtual bool findForcing(QHash<QString, CYData*> *db);

  /** @return \a true si l'objet graphique a au moins une donnée supplémentaire. */
  bool hasDataSup();
  /** @return \a true si l'objet graphique a une donnée supplémentaire \a num. */
  bool hasDataSup(int num);
  /** @return la donnée supplémentaire \a num. */
  CYData *dataSup(int num);
  /** Place @p data comme la donnée supplémentaire \a num du widget. */
  virtual void setDataSup(int num, CYData *data);
  /** Recherche, dans la base de données globale, les données supplémentaires ayant
    * pour nom celui saisie par la fonction setDataSupName() et la place
    * comme donnée supplémentaire du widget. */
  bool findDataSup();
  /** Recherche, dans la base de données \a db, la donnée supplémentaire ayant
    * pour nom celui saisie par la fonction setDataSupName() et la place
    * comme donnée supplémentaire du widget. */
  virtual bool findDataSup(QHash<QString, CYData*> *db);
  /** Recherche, dans la base de données \a db, la donnée supplémentaire \a num ayant
    * pour nom celui saisie par la fonction setDataSupName() et la place
    * comme donnée supplémentaire du widget. */
  virtual bool findDataSup(int num, QHash<QString, CYData*> *db);

  /** @return \a true si l'objet graphique a une donnée de positionnement en X. */
  bool hasPosX();
  /** @return la donnée de positionnement en X. */
  CYS32 *posX();
  /** Place @p data comme la donnée de positionnement en X du widget. */
  virtual void setPosX(CYS32 *data);
  /** Recherche, dans la base de données globale, la donnée de positionnement en X ayant
    * pour nom celui saisie par la fonction setPosXName() et la place
    * comme donnée de positionnement en X du widget. */
  bool findPosX();
  /** Recherche, dans la base de données \a db, la donnée ayant
    * pour nom celui saisie par la fonction setPosXName() et la place
    * comme donnée de positionnement en X du widget. */
  virtual bool findPosX(QHash<QString, CYData*> *db);

  /** @return \a true si l'objet graphique a une donnée de positionnement en Y. */
  bool hasPosY();
  /** @return la donnée de positionnement en Y. */
  CYS32 *posY();
  /** Place @p data comme la donnée de positionnement en Y du widget. */
  virtual void setPosY(CYS32 *data);
  /** Recherche, dans la base de données globale, la donnée de positionnement en Y ayant
    * pour nom celui saisie par la fonction setPosYName() et la place
    * comme donnée de positionnement en Y du widget. */
  bool findPosY();
  /** Recherche, dans la base de données \a db, la donnée ayant
    * pour nom celui saisie par la fonction setPosYName() et la place
    * comme donnée de positionnement en Y du widget. */
  virtual bool findPosY(QHash<QString, CYData*> *db);

  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh() {}
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer() {}
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update() {}

  /** Configure le widget en fonction de la donnée à laquelle il est lié. */
  virtual void linkData() {}
  /** Configure le widget en fonction de la donnée supplémentaire \a bum à laquelle il est liée. */
  virtual void linkDataSup(int num) { Q_UNUSED(num); }

  /** Initialise toutes les fonctions relatives au parent.
    * @param mark  Repère de généricité utile aux widgets parents génériques.
    *              @see void CYTemplate::setGenericMark(QByteArray txt) */
  virtual void setParent(QWidget *parent, Cy::TemplateType type, QByteArray mark);

  /** Ajoute un nom de donnée à la liste des noms de données. Celle-ci est insérée à la position \a index
    * en écrasant celle qui s'y trouve déjà. Si \a index = -1 le nom de la donnée sera ajoutée à la
    * fin de la liste.
    * @see void setCurrentDataName(int index). */
  virtual void addDataName(const QString &name, int index=-1);
  /** Change le nom de la donnée courrante. \a Index est la position dans la liste des noms de données disponibles. */
  virtual void setCurrentDataName(int index);
  /** Ajoute une donnée à la liste de donnée. Celle-ci est insérée à la position \a index
    * en écrasant celle qui s'y trouve déjà. Si \a index = -1 la donnée sera ajoutée à la
    * fin de la liste. */
  virtual void addData(const QString &name, int index=-1);
  /** Change la donnée courrante. \a Index est la position dans la liste de données disponibles. */
  virtual void setCurrentData(int index);
  /** Vide la liste des données. */
  virtual void clearDatas();

  /** @return l'état du widget. */
  bool enabled() { return mEnabled; }

  /** Saisie l'afficheur simple utilisant cet objet graphique. */
  virtual void setDisplaySimple(CYDisplaySimple *display) { mDisplaySimple = display; }

  /** Protège la palette de couleurs du widget si \a val vaut true. */
  virtual void setPaletteProtected(bool val) { mPaletteProtected = val; }

protected: // Protected methods
  /** Initialise le widget en mode forçage ou pas suivant \a mForcingMode. */
  virtual void initForcingMode() {}
  /** Ajoute la base de données de la donnée au conteneur graphique
    * parent qui gère l'écriture sur le disque dur et sur le réseau. */
  virtual void addWriteDBToParent();
  /** Ajoute la base de données de la donnée de forçage au conteneur graphique
    * parent qui gère l'écriture sur le réseau en mode forçage. */
  virtual void addForcingDBToParent();
  /** Ajoute la base de données de la donnée supplémentaire \a num au conteneur graphique
    * parent qui gère l'écriture sur le disque dur et sur le réseau. */
  virtual void addWriteDBToParentX(int num);
  /** Initialise le widget en mode écriture ou pas suivant \a mSettingMode. */
  virtual void initSettingMode() {}

protected: // Protected attributes
  /** Widget de cet objet graphique. */
  QWidget *mThis;
  /** Nom de cet objet graphique. */
  QString mName;

  bool mHideIfNoData;
  /** Donnée courrante gérée par cet objet graphique. */
  CYData *mData;
  /** Donnée de contrôle qui gère l'activation de cet objet graphique. */
  CYFlag *mFlag;
  /** Donnée de contrôle qui gère l'affichage ou non de cet objet graphique. */
  CYFlag *mHideFlag;
  /** Donnée de contrôle de changement de couleur de cet objet graphique. */
  CYFlag *mColorFlag;
  /** Donnée en mode forçage utile à certains objets de visualisation. */
  CYData *mForcingData;
  /** Donnée de positionnement en X de cet objet graphique. */
  CYS32 *mPosX;
  /** Donnée de positionnement en Y de cet objet graphique. */
  CYS32 *mPosY;
  /** Données supplémentaires gérées par cet objet graphique. */
  QHash<int, CYData*> mDataSup;
  int mXOldPos;
  int mYOldPos;
  int mXMousePos;
  int mYMousePos;

  /** Nom de la donnée courrante gérée par cet objet graphique. */
  QString mDataName;
  /** Nom de la donnée de contrôle qui gère l'activation de cet objet graphique. */
  QString mFlagName;
  /** Nom de la donnée de contrôle qui gère l'affichage ou non de cet objet graphique. */
  QString mHideFlagName;
  /** Nom de la donnée de contrôle de  changement de couleur de cet objet graphique. */
  QString mColorFlagName;
  /** Nom de la donnée en mode forçage utile à certains objets de visualisation. */
  QString mForcingName;
  /** Nom de la donnée de positionnement en X de cet objet graphique. */
  QString mPosXName;
  /** Nom de la donnée de positionnement en Y de cet objet graphique. */
  QString mPosYName;
  /** Type du banc d'essai. */
  QString mBenchType;
  /** Noms de données supplémentaires gérées par cet objet graphique @see CYRobotTopView. */
  QHash<int, QString*> mDataSupName;

  bool    mInverseFlag;
  bool    mInverseHideFlag;
  bool    mInverseColorFlag;

  /** Base de données locale à cet afficheur. */
  CYDB *mDB;
  /** Force la recherche de la donnée dans la base données globale. */
  bool mCoreDB;
  /** Si \a true le widget est en lecture seule. */
  bool mReadOnly;
  /** Vaut \a true si le widget est la gestion du mode lecture-seule est active. */
  bool mEnableReadOnly;
  /** Si \a true le widget peut être bougé par l'utilisateur. */
  bool mMovable;
  /** Si \a true le widget peut être cliqué par l'utilisateur. */
  bool mClickable;
  /** Si \a true le widget a été cliqué par l'utilisateur. */
  bool mClicked;
  /** Active/désactive le déplacement. */
  bool mEditPositions;
  /** Si \a true le widget est en mode forçage. */
  bool mForcingMode;
  /** Si \a true le widget est en mode paramétrage. */
  bool mSettingMode;
  /** Si \a true la donnée traîtée est considérée comme toujours valide. */
  bool mAlwaysOk;
  /** Indique si l'objet graphique peut gérer des données en écriture sur le disque dur et sur le réseau. */
  bool mWriteData;
  /** Si \a true alors le signal modifie() peut être émis. */
  bool mEmitModifie;
  /** Vaut \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  bool mAutoRefresh;

  /** Palette de couleurs du widget. */
  QPalette mPalette;
  /** Protège la palette de couleurs du widget. */
  bool mPaletteProtected;

  /** Type de conteneur. */
  Cy::TemplateType mTypeParent;
  /** Objet graphique parent. */
  QWidget  *mParent;
  /** Boîte de dialogue parent. */
  CYDialog *mDlgParent;
  /** Widget parent. */
  CYWidget *mWdgParent;
  /** Frame parent. */
  CYFrame  *mFrmParent;

  /** Liste des noms de données permettant d'associer plusieurs données à un objet graphique.
    * Ceci peut être utile lors d'un changement de donnée à la volée. */
  QHash<int, QString*> mDatasName;
  /** Liste de données permettant d'associer plusieurs données à un objet graphique.
    * Ceci peut être utile lors d'un changement de donnée à la volée. */
  QHash<int, CYData*> mDatas;
  /** Etat du widget. */
  bool mEnabled;
  /** Numéro du pas envoyé à une animation lors de la réception du focus. */
  int mMovieStepFocusIn;
  /** Numéro du pas envoyé à une animation lors de la perte du focus. */
  int mMovieStepFocusOut;
  /** Repère de généricité utile aux widgets parents génériques.
    * @see void CYTemplate::setGenericMark(QByteArray txt) */
  QString mGenericMark;
  /** Offset sur le repère de généricité lorsqu'il s'agit d'un numéro.
    * @see void CYTemplate::setGenericMark(QByteArray txt) */
  int mGenericMarkOffset;
  /** S'il vaut \a true alors la mise à jour de la donnée par la valeur
    * saisie par le widget n'est possible que si celui-ci est visible. */
  bool mUpdateIfVisible;
  /** S'il vaut \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool mTreatChildDataWdg;

  /** Taille de la police. */
  int mFontSize;
  /** Vaut \a true si la dernière mise à jour de la donnée s'est bien passée. */
  bool mUpdateOk;

  /** Vaut \a true si la taille minimum est calculée automatiquement par rapport à la donnée.
    * Valable pour CYNumInput seulement. */
  bool mAutoMinimumSize;

  /** Afficheur simple utilisant cet objet graphique. */
  CYDisplaySimple *mDisplaySimple;

  bool mCtrlFlag;

  /** Couleur de fond par défaut. */
  QColor mBackgroundColor;
  /** Couleur de fond de changement suivant @see colorFlag. */
  QColor mBackgroundColor2;
  /** Couleur de devant par défaut. */
  QColor mForegroundColor;
  /** Image de fond par défaut. */
  QPixmap mBackgroundPixmap;
};

#endif
