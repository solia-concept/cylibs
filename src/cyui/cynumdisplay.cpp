/***************************************************************************
                          cynumdisplay.cpp  -  description
                             -------------------
    début                  : jeu mai 15 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cynumdisplay.h"

// QT
#include <QDateTime>
#include <QLayout>
#include <QValidator>
#include <QStyle>
// CYLIBS
#include "cy.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyflag.h"
#include "cytime.h"
#include "cydatetime.h"
#include "cytsec.h"
#include "cyword.h"
#include "cycore.h"
#include "cynumdialog.h"
#include "cymessagebox.h"
#include "cyreadonlyfilter.h"

CYNumDisplay::CYNumDisplay(QWidget *parent, const QString &name)
  : CYLineEdit(parent, name)
{
  init(0.0, 10);
}

CYNumDisplay::CYNumDisplay(double value, QWidget *parent, const QString &name, long base)
  : CYLineEdit(parent, name)
{
  init(value, base);
}

CYNumDisplay::~CYNumDisplay()
{
}

void CYNumDisplay::init(double value, long base)
{
  mNumBase  = Cy::BaseData;
  mBase     = base;
  mShowUnit = true;
  mShowLabel = false;
  mValue = value;
  mLower = -100.0;
  mUpper = 100.0;
  mStep  = 1.0;
  mDec   = 0;
  mRange = false;
  mSuffix = mPrefix = "";
  mType   = Process;
  mEnableReadOnly = false;
  setContextMenuPolicy(Qt::NoContextMenu);
  setAlignment(Qt::AlignRight);
  QDoubleValidator* validator = new QDoubleValidator(this);
  setValidator(validator);

  resetEditBox();
}

double CYNumDisplay::value()
{
  if (isModified())
    interpretText();

  return mValue;
}

void CYNumDisplay::setValue(const double val)
{
  double prevVal = mValue;
  mValue = val;

  resetEditBox();

  if (mRange && fabs(prevVal - mValue) > mStep)
    emit valueChanged(mValue);
}

void CYNumDisplay::setRange(double lower, double upper, double step, int dec)
{
  double prevVal = mValue;

  mLower = lower;
  mUpper = upper;
  mStep  = step;
  mDec   = dec;
  mRange = (mLower < mUpper);

  // limites de contrôle des valeurs
  if (mValue < mLower) mValue = mLower;
  if (mUpper < mLower) mUpper = mValue;
  if (mUpper < mValue) mValue = mUpper;

  // créé une valeur multiple du pas d'incrémentation.
  mValue = floor((mValue+.5*mStep) / mStep) * mStep;

  resetEditBox();

  if (prevVal!=mValue)
    emit valueChanged(mValue);
}

void CYNumDisplay::setNumBase(const NumBase b)
{
  mNumBase = b;
  switch ( mNumBase )
  {
    case Cy::Binary     : mBase =  2;  break;
    case Cy::Hexadecimal: mBase = 16;  break;
    case Cy::Octal      : mBase = 8;  break;
    case Cy::BaseData   :
    case Cy::Decimal    :
    default             : mBase = 10;  break;
  }
}

double CYNumDisplay::minValue() const
{
  return mLower;
}

void CYNumDisplay::setMinValue(double min)
{
  setRange(min, mUpper, mStep);
}

double CYNumDisplay::maxValue() const
{
  return mUpper;
}

void CYNumDisplay::setMaxValue(double max)
{
  setRange(mLower, max, mStep);
}

double CYNumDisplay::lineStep() const
{
  return mStep;
}

void CYNumDisplay::setLineStep(double step)
{
  setRange(mLower, mUpper, step);
}

void CYNumDisplay::interpretText()
{
  QString s = QString(text()).trimmed();
  if (!mPrefix.isEmpty())
  {
    QString px = QString(mPrefix).trimmed();
    int len = px.length();
    if (len && s.left(len) == px)
      s.remove(0, len);
  }
  if (!mSuffix.isEmpty())
  {
    QString sx = QString(mSuffix).trimmed();
    int len = sx.length();
    if (len && s.right(len) == sx)
      s.truncate(s.length() - len);
  }

  s = s.trimmed();

  if (isModified())
  {
    bool ok;


//    TODO KDE3->QT4
//    QLocale locale;
//    s.replace(QRegExp(locale.decimalPoint()), ".");
//    s.replace(QRegExp(locale.groupSeparator()), "");

    double value = s.toDouble(&ok);

    if (ok)
    {
      setValue(value);
      setModified(false);
    }
    else
    {
      CYMessageBox::information(0, tr("The input text must be a numeric value !"));
      resetEditBox();
      selectAll();
    }
  }
}

void CYNumDisplay::resetEditBox()
{
  int base = ( mData && (mNumBase==Cy::BaseData) ) ? mData->numBase() : mNumBase;
  if ((base!=Cy::Hexadecimal) && (base!=Cy::Binary) && (base!=Cy::Decimal))
    base = Cy::Decimal;

  // Si pas de flag de validite => contrôle les bornes
  if ( !mAlwaysOk && ( mData && !mData->flag() && !hasFlag() && ((mData->type() != Cy::Time) && ((mValue<mLower) || (mValue>mUpper))) ) )
  {
    setEnabled(false);
    putSpecialValueText();
    home(false);
  }
  else if (mData && mShowLabel)
  {
    setText(mData->label());
  }
  else
  {
    QString s;
    if (mData)
    {
      if (mData->type() == Cy::Time)
      {
        CYTime *time = (CYTime *)mData;
        QString txt = time->toString(mValue);
        setText(txt);
      }
      else if (mData->inherits("CYDateTime"))
      {
        CYDateTime *dt = (CYDateTime *)mData;
        QString txt = dt->toString(mValue);
        setText(txt);
      }
      else if (mData->isExponential() && (mData->type()==Cy::VF32))
      {
        QString txt = mData->toString(mValue, mShowUnit, (Cy::NumBase)base );
        setText(txt);
      }
      else if (mData->isExponential() && (mData->type()==Cy::VF64))
      {
        QString txt = mData->toString(mValue, mShowUnit, (Cy::NumBase)base );
        setText(txt);
      }
      else if ( (mData->type()==Cy::VU16) || (mData->type()==Cy::VU16) || (mData->type()==Cy::VU32) || (mData->type()==Cy::VU64) )
      {
        s = mData->toString( false, false, (Cy::NumBase)base );
        setText(mPrefix + s + mSuffix);
      }
      else
      {
        s = mData->toString( false, false, (Cy::NumBase)base );
        if (mValue<0)
          setText(mPrefix + s + mSuffix);
        else
          setText(mPrefix + " " + s + mSuffix);
      }
    }
    else
    {
      s =  QString("%1").arg(mValue, 0, 'f', mDec);
      if (mValue<0)
        setText(mPrefix + s + mSuffix);
      else
        setText(mPrefix + " " + s + mSuffix);
    }

    home(false);
  }
}

void CYNumDisplay::doLayout()
{
  ensurePolished();
  QFontMetrics fm(font());
  QString s;
  int h = fm.height();
  s = QString("%1").arg(mValue, 0, 'f', mDec);
  int w0 = fm.horizontalAdvance(mPrefix) + fm.horizontalAdvance(mSuffix);
  int w = w0 + fm.horizontalAdvance(s);
  w = qMax(w, fm.horizontalAdvance(mSpecialValue));
  if (mRange)
  {
    s = QString("%1").arg(mLower, 0, 'f', mDec);
    w = qMax(w, fm.horizontalAdvance(s)+w0);
    s = QString("%1").arg(mUpper, 0, 'f', mDec);
    w = qMax(w, fm.horizontalAdvance(s)+w0);
    // something inbetween
    s = QString("%1").arg(mLower + mStep, 0, 'f', mDec);
    w = qMax(w, fm.horizontalAdvance(s)+w0);
  }

  if (hasFrame())
  {
    h += 8;
    // TODO QT5
//    if (style()->styleHint(QStyle::SH_GUIStyle) == Qt::WindowsStyle && h < 26)
//        h = 22;
//    mSizeEdit.setWidth(w + 8);
    mSizeEdit.setWidth(w);
    mSizeEdit.setHeight(h);
  } else
  {
//    mSizeEdit.setWidth(w + 4);
    mSizeEdit.setWidth(w);
    mSizeEdit.setHeight(h + 4);
  }
}

void CYNumDisplay::focusInEvent(QFocusEvent *)
{
  selectAll();
}

void CYNumDisplay::focusOutEvent(QFocusEvent *)
{
  setValue(value());
}


void CYNumDisplay::setData(CYData *data)
{

  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
    disconnect(mData, SIGNAL(enableColorChanged(const QColor&)), this, SLOT(setEnableColor(const QColor&)));
    disconnect(mData, SIGNAL(disableColorChanged(const QColor&)), this, SLOT(setDisableColor(const QColor&)));
  }
  CYLineEdit::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
    connect(mData, SIGNAL(enableColorChanged(const QColor&)), this, SLOT(setEnableColor(const QColor&)));
    connect(mData, SIGNAL(disableColorChanged(const QColor&)), this, SLOT(setDisableColor(const QColor&)));
  }
}


void CYNumDisplay::linkData()
{
  mCtrlFlag=true;

  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    else
    {
      setText(mDataName);
      this->setToolTip(QString(tr("Cannot find the data %1").arg(mDataName)));
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  mDec = mData->nbDec();
  mEnableColor  = mData->enableColor();
  mDisableColor = mData->disableColor();
  initPalettes();

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      if (data->isExponential())
                        setRange(data->min(), data->max(), lineStep(), 0);
                      else
                        setRange(data->min(), data->max(), lineStep(), data->nbDec());
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      if (data->isExponential())
                        setRange(data->min(), data->max(), lineStep(), 0);
                      else
                        setRange(data->min(), data->max(), lineStep(), data->nbDec());
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setRange(data->min(), data->max(), lineStep(), data->nbDec());
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setRange(data->min(), data->max(), lineStep(), 0);
                      setValue((double)data->val());
                      if (mShowUnit && !mShowLabel)
                        setSuffix(" "+data->unit());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->displayHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      return;
  }

  if (mShowLabel)
  {
    mSpecialValue = prefix() + mData->label() + suffix();
  }

  setType();
  refresh();
}

void CYNumDisplay::changeEvent(QEvent* event)
{
  if (event && (event->type()==QEvent::EnabledChange))
  {
    // cet événement est envoyé, si l'état actif change
    bool state = isEnabled();
    mEnabled = state;
    if (!state && !mAlwaysOk)
      putSpecialValueText();
  }
  QLineEdit::changeEvent(event);
}


void CYNumDisplay::refresh()
{
  ctrlHideFlag();

  refreshPosition();

  bool ctrl = ctrlFlag();

  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    else
    {
      setText(mDataName);
      this->setToolTip(QString(tr("Cannot find the data %1").arg(mDataName)));
    }
    return;
  }
  if ( movable() && mEditPositions )
  {
    setText( mData->label() );
    return;
  }

  if (!core->simulation() && ctrl)
    setEnabled(mData->isOk() || mAlwaysOk);

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue(data->val());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue(data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      break;
  }

  if (!isEnabled())
    putSpecialValueText();
}

void CYNumDisplay::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    setEnabled(false);
    return;
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      setValue((double)data->def());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue((double)data->def());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

CYNumDisplay::Format CYNumDisplay::format() const
{
  return mFmt;
}

void CYNumDisplay::setFormat(const Format fmt)
{
  mFmt = fmt;
  resetEditBox();
}

void CYNumDisplay::setCurrentDataName(int index)
{
  CYDataWdg::setCurrentDataName(index);
  emit currentDataChanged();
  if ((mEmitModifie) && isVisible())
    emit modifie();
}

void CYNumDisplay::setCurrentData(int index)
{
  CYDataWdg::setCurrentData(index);
  emit currentDataChanged();
  if ((mEmitModifie) && isVisible())
    emit modifie();
}

void CYNumDisplay::setReadOnly(const bool val)
{
  CYLineEdit::setReadOnly(val);
}

void CYNumDisplay::mouseDoubleClickEvent ( QMouseEvent * e )
{
  if (!core->simulation())
    return CYLineEdit::mouseDoubleClickEvent ( e );

  CYNumDialog *dlg = new CYNumDialog(this);
  if (mData)
  {
    dlg->setDataName(mData->objectName().toLocal8Bit());
    dlg->exec();
  }
}
