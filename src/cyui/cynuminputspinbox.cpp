/***************************************************************************
                          cynuminput.cpp  -  description
                             -------------------
    début                  : jeu mai 15 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : g.lecleach@clemessy.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cynuminput.h"

#include <unistd.h>

// QT
#include <qlineedit.h>
#include <qapplication.h>
#include <qvalidator.h>
#include <QKeyEvent>
#include <QString>
#include <QMouseEvent>
#include <QEvent>
#include <QStyle>
#include <QStyleOption>
// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cymessagebox.h"
#include "cynumvalidator.h"
#include "cy.h"

class CYHexaSpinBoxValidator : public CYIntValidator
{
public:
  CYHexaSpinBoxValidator( int bottom, int top, int base, CYNumInput* sb, const QString &name )
    : CYIntValidator( bottom, top, sb, base, name.toUtf8() ), spinBox( sb ) { }

  virtual State validate( QString& str, int& pos ) const;

private:
  CYNumInput *spinBox;
};

QValidator::State CYHexaSpinBoxValidator::validate( QString& str, int& pos ) const
{
  QString pref = spinBox->QSpinBox::prefix();
  QString suff = spinBox->QSpinBox::suffix();
  QString suffStriped = suff.trimmed();
  uint overhead = pref.length() + suff.length();
  State state = Invalid;

  if ( overhead == 0 ) {
    state = CYIntValidator::validate( str, pos );
  } else {
    bool stripedVersion = false;
    if ( str.length() >= overhead && str.startsWith(pref)
         && (str.endsWith(suff)
             || (stripedVersion = str.endsWith(suffStriped))) ) {
      if ( stripedVersion )
        overhead = pref.length() + suffStriped.length();
      QString core = str.mid( pref.length(), str.length() - overhead );
      int corePos = pos - pref.length();
      state = CYIntValidator::validate( core, corePos );
      pos = corePos + pref.length();
      str.replace( pref.length(), str.length() - overhead, core );
    } else {
      state = CYIntValidator::validate( str, pos );
      if ( state == Invalid ) {
        // trimmed(), cf. QSpinBox::interpretText()
        QString special = spinBox->specialValueText().trimmed();
        QString candidate = str.trimmed();

        if ( special.startsWith(candidate) ) {
          if ( candidate.length() == special.length() ) {
            state = Acceptable;
          } else {
            state = Intermediate;
          }
        }
      }
    }
  }
  return state;
}

class CYDoubleSpinBoxValidator : public CYDoubleValidator
{
public:
  CYDoubleSpinBoxValidator( double bottom, double top, int decimals, CYNumInput* sb, const QString &name )
    : CYDoubleValidator( bottom, top, decimals, sb, name.toUtf8() ), spinBox( sb ) { }

  virtual State validate( QString& str, int& pos ) const;

private:
  CYNumInput *spinBox;
};

QValidator::State CYDoubleSpinBoxValidator::validate( QString& str, int& pos ) const
{
  QString pref = spinBox->QSpinBox::prefix();
  QString suff = spinBox->QSpinBox::suffix();
  QString suffStriped = suff.trimmed();
  uint overhead = pref.length() + suff.length();
  State state = Invalid;

  if ( overhead == 0 )
  {
    state = CYDoubleValidator::validate( str, pos );
  }
  else
  {
    bool stripedVersion = false;
    if ( str.length() >= overhead && str.startsWith(pref)
         && (str.endsWith(suff)
             || (stripedVersion = str.endsWith(suffStriped))) )
    {
      if ( stripedVersion )
        overhead = pref.length() + suffStriped.length();
      QString core = str.mid( pref.length(), str.length() - overhead );
      int corePos = pos - pref.length();
      state = CYDoubleValidator::validate( core, corePos );
      pos = corePos + pref.length();
      str.replace( pref.length(), str.length() - overhead, core );
    }
    else
    {
      state = CYDoubleValidator::validate( str, pos );
      if ( state == Invalid )
      {
        // trimmed(), cf. QSpinBox::interpretText()
        QString special = spinBox->specialValueText().trimmed();
        QString candidate = str.trimmed();

        if ( special.startsWith(candidate) )
        {
          if ( candidate.length() == special.length() )
          {
            state = Acceptable;
          }
          else
          {
            state = Intermediate;
          }
        }
      }
    }
  }
  return state;
}

// We use a kind of fixed-point arithmetic to represent the range of
// doubles [mLower,mUpper] in steps of 10^(-mPrecision). Thus, the
// following relations hold:
//
// 1. factor = 10^mPrecision
// 2. basicStep = 1/factor = 10^(-mPrecision);
// 3. lowerInt = lower * factor;
// 4. upperInt = upper * factor;
// 5. lower = lowerInt * basicStep;
// 6. upper = upperInt * basicStep;
class CYNumInput::Private
{
public:
  Private(int precision=0)
    : mPrecision(precision),
      mBase(10),
      mIsInt(true),
      mIntValidator(0),
      mDoubleValidator(0)
  {
  }

  int factor() const
  {
    int f = 1;
    for (int i = 0 ; i < mPrecision ; ++i)
      f *= 10;
    return f;
  }

  double basicStep() const
  {
    return 1.0/double(factor());
  }

  int mapToInt(double value, bool * ok) const
  {
    if (!ok)
      CYFATAL

          const double f = factor();
    if (value > double(INT_MAX) / f)
    {
      CYWARNINGTEXT(tr("Can't represent value %1 in terms of fixed-point numbers with precision %2")
                    .arg(value).arg(mPrecision))
          *ok = false;
      return INT_MAX;
    }
    else if (value < double(INT_MIN) / f)
    {
      double res = (double(INT_MIN) / f);
      CYWARNINGTEXT(tr("Can't represent value %1 in terms of fixed-point numbers with precision %2 %3/%4 = %5")
                    .arg(value).arg(mPrecision).arg(double(INT_MIN)).arg(f).arg(res))
          *ok = false;
      return INT_MIN;
    }
    else
    {
      *ok = true;
      int ret = int(value * f + (value < 0 ? -0.5 : 0.5));
      return ret;
    }
  }

  double mapToDouble(int value) const
  {
    return (double)(value * basicStep());
  }

  int  mPrecision;
  int  mBase;
  bool mIsInt;

  CYIntValidator *mIntValidator;
  CYDoubleValidator *mDoubleValidator;
};

CYNumInput::CYNumInput(QWidget *parent, const QString &name)
  : QSpinBox(parent),
    CYDataWdg(this)
{
  setObjectName(name);
  mNumBase      = BaseData;
  mWriteData    = true;
  mInvalid      = false;
  mNoControl    = false;
  mShowUnit     = true;
  mInputHelp    = true;
  mEnableEdit   = true;
  mAutoRefresh  = false;
  mTestingNewDataValue = false;
  mChangingSection = false;
  mNewNumDec    =-1;


  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  // TOCHECK QT4
  //  editor()->setAlignment(Qt::AlignRight);
  setAlignment(Qt::AlignRight);
  d = new Private();
  updateValidator();

  setFocusPolicy(Qt::StrongFocus);
  updateROPalette();

  connect(this, SIGNAL(valueChanged(int)), SLOT(slotValueChanged(int)));
  connect(this, SIGNAL(valueChanged(const QString &)), SLOT(slotValueChanged(const QString &)));
  connect(this, SIGNAL(editingFinished()), SLOT(slotEditingFinished()));
}

CYNumInput::~CYNumInput()
{
  if (d)
    delete d;
  d = 0;
}

CYNumInput::NumBase CYNumInput::numBase() const
{
  return mNumBase;
}

void CYNumInput::setNumBase(const NumBase b)
{
  mNumBase = b;

  switch ( mNumBase )
  {
    case Binary     : d->mBase =  2;  setDataNbDec(0); break;
    case Hexadecimal: d->mBase = 16;  setDataNbDec(0);
      /*  if (mData && (QString(mData->objectName())=="LINCAN_FRAME0_DATA1_VAL"))
    CYDEBUGTEXT(QString("%1/%2 %3 %4 %5)").arg(value()).arg(QSpinBox::value()).arg(d->mPrecision).arg(d->factor()).arg(d->basicStep()))*/
      break;
    case BaseData   :
    case Decimal    :
    default         : d->mBase = 10;  resetDataNbDec(); break;
  }
  //   int val = value();
  //   if (mData && (QString(mData->objectName())=="LINCAN_FRAME0_DATA1_VAL"))
  //     CYDEBUGTEXT(QString("%1/%2 %3 %4 %5)").arg(val).arg(QSpinBox::value()).arg(d->mPrecision).arg(d->factor()).arg(d->basicStep()))
  textFromValue(value());
}

void CYNumInput::setNumBase(int base)
{
  //   const Cy::NumBase b = (CYNumInput::NumBase)base;
  setNumBase((CYNumInput::NumBase)base);
  refresh();
}

bool CYNumInput::movable() const
{
  return mMovable;
}

void CYNumInput::setMovable(const bool val)
{
  mMovable = val;
  setEditPositions( mEditPositions );
}

void CYNumInput::setDataName(const QString &name)
{
  CYDataWdg::setDataName(name);
}

QString CYNumInput::dataName() const
{
  return CYDataWdg::dataName();
}

void CYNumInput::setFlagName(const QString &name)
{
  CYDataWdg::setFlagName(name);
}

QString CYNumInput::flagName() const
{
  return CYDataWdg::flagName();
}

void CYNumInput::setHideFlagName(const QString &name)
{
  CYDataWdg::setHideFlagName(name);
}

QString CYNumInput::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYNumInput::setPosXName(const QString &name)
{
  CYDataWdg::setPosXName( name );
}

QString CYNumInput::posXName() const
{
  return CYDataWdg::posXName();
}

void CYNumInput::setPosYName(const QString &name)
{
  CYDataWdg::setPosYName( name );
}

QString CYNumInput::posYName() const
{
  return CYDataWdg::posYName();
}

void CYNumInput::setBenchType(const QString &name)
{
  CYDataWdg::setBenchType(name);
}

QString CYNumInput::benchType() const
{
  return CYDataWdg::benchType();
}

void CYNumInput::setInverseFlag(bool inverse)
{
  CYDataWdg::setInverseFlag(inverse);
}

bool CYNumInput::inverseFlag() const
{
  return CYDataWdg::inverseFlag();
}

void CYNumInput::setInverseHideFlag(bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYNumInput::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

bool CYNumInput::acceptLocalizedNumbers() const
{
  if ((!d->mDoubleValidator) || (d->mIsInt))
    return true;

  return d->mDoubleValidator->acceptLocalizedNumbers();
}

void CYNumInput::setAcceptLocalizedNumbers(bool accept)
{
  if ((!d->mDoubleValidator) || (d->mIsInt))
    updateValidator();

  d->mDoubleValidator->setAcceptLocalizedNumbers(accept);
}


bool CYNumInput::readOnly() const
{
  return mReadOnly;
}

void CYNumInput::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  removeEventFilter(core->readOnlyFilter());
  if (mReadOnly)
    installEventFilter(core->readOnlyFilter());

  ctrlFlag();
}

void CYNumInput::setRange(double lower, double upper, double step, int precision)
{
  lower = qMin(upper, lower);
  upper = qMax(upper, lower);
  if (mData && (mData->type()!=Cy::Time))
    setPrecision(precision, true); // disable bounds checking, since
  setMinValue(lower);            // it's done in set{Min,Max}Value
  setMaxValue(upper);            // anyway and we want lower, upper
  setSingleStep(step);             // and step to have the right precision
}

int CYNumInput::precision() const
{
  return d->mPrecision;
}

void CYNumInput::setPrecision(int precision)
{
  setPrecision(precision, false);
}

void CYNumInput::setPrecision(int precision, bool force)
{
  if (precision < 0)
    return;
  if (!force)
  {
    int maxPrec = maxPrecision();
    if (precision > maxPrec)
      precision = maxPrec;
  }
  d->mPrecision = precision;
  updateValidator();
}

int CYNumInput::maxPrecision() const
{
  // INT_MAX must be > maxAbsValue * 10^precision
  // ==> 10^precision < INT_MAX / maxAbsValue
  // ==> precision < log10 (INT_MAX / maxAbsValue)
  // ==> maxPrecision = floor(log10 (INT_MAX / maxAbsValue));
  double maxAbsValue = qMax(fabs(minValue()), fabs(maxValue()));
  if (maxAbsValue == 0)
    return 6; // return arbitrary value to avoid dbz...

  return int(floor(log10(double(INT_MAX) / maxAbsValue)));
}

double CYNumInput::value() const
{
  return d->mapToDouble(QSpinBox::value());
}

void CYNumInput::setValue(double value)
{
  if (value == this->value())
    return;
  bool ok = false;
  int val = d->mapToInt(value, &ok);
  QSpinBox::setValue(val);
  if (!ok)
    CYFATALOBJECT(objectName());
}

double CYNumInput::minValue() const
{
  return d->mapToDouble(QSpinBox::minimum());
}

void CYNumInput::setMinValue(double value)
{
  bool ok = false;
  int min = d->mapToInt(value, &ok);
  if (!ok)
    return;
  QSpinBox::setMinimum(min);
  updateValidator();
}

double CYNumInput::maxValue() const
{
  return d->mapToDouble(QSpinBox::maximum());
}

void CYNumInput::setMaxValue(double value)
{
  bool ok = false;
  int max = d->mapToInt(value, &ok);
  if (!ok)
    return;
  QSpinBox::setMaximum(max);
  updateValidator();
}

double CYNumInput::singleStep() const
{
  if (mData)
    return mData->precision();
  return d->mapToDouble(QSpinBox::singleStep());
}

void CYNumInput::setSingleStep(double step)
{
  bool ok = false;

  if (!mstepDataName.isEmpty())
  {
    CYF64 *d = (CYF64 *)core->findData(mstepDataName);
    step = d->val();
  }

  if (step > (maxValue() - minValue()))
    QSpinBox::setSingleStep(1);
  else
    QSpinBox::setSingleStep(qMax(d->mapToInt(step, &ok), 1));
}

QString CYNumInput::textFromValue(int value) const
{
  return QSpinBox::textFromValue(value);
  QString txt;

  int base = ( mData && (mNumBase==BaseData) ) ? mData->numBase() : mNumBase;
  if ((base!=Hexadecimal) && (base!=Binary) && (base!=Decimal))
    base = Decimal;

  if (d->mIsInt || (base!=Decimal))
  {
    if (mData && (mData->type()==Cy::Time))
    {
      CYTime *time = (CYTime *)mData;
      if (!time->isSectionMaxi())
        txt = forward0();
    }

    txt=txt.append(QString::number(value, base));
    if (base==Hexadecimal)
    {
      txt=txt.toUpper();
      txt=txt.prepend("0x");
      /*      if (mData && (QString(mData->objectName())=="LINCAN_FRAME0_DATA1_VAL"))
        CYDEBUGTEXT(QString("%1: %2 (%3/%4 %5 %6 %7)").arg(mData->objectName()).arg(txt).arg(value).arg(QSpinBox::value()).arg(d->mPrecision).arg(d->factor()).arg(d->basicStep()))*/
    }
  }
  else
  {
    txt = QString().setNum(d->mapToDouble(value), 'f', d->mPrecision);
  }
  return txt;
}

int CYNumInput::valueFromText(const QString & text) const
{
//  return QSpinBox::valueFromText(text);
  if (text.isEmpty())
    return 0;


  CYNumInput *widget = (CYNumInput *)mThis;

  int ret;
  bool *ok;
  ok = new bool;
  int base = ( mData && (mNumBase==BaseData) ) ? mData->numBase() : mNumBase;
  if ((base!=Hexadecimal) && (base!=Binary) && (base!=Decimal))
    base = Decimal;

  // VALEUR ENTIERE
  if (d->mIsInt || (base!=Decimal))
  {
    int value = cleanText().toInt(ok, base);
    if (!ok)
      return 0;
    // TEMPO
    if (mData && (mData->type()==Cy::Time))
    {
      CYTime *time = (CYTime *)mData;
      value = cyRound(value, time->precisionSection());
      time->setValSection(value);
      emit widget->tmpValue(time->tmp());
      ret = value;
    }
    else if (mData && (mData->isExponential()) && (mData->type()==Cy::VF32))
    {
      CYF32 *data = (CYF32 *)mData;
      value = cyRound(value, data->precisionSection());
      data->setValSection(value);
      emit widget->tmpValue(data->tmp());
      ret = value;
    }
    else if (mData && (mData->isExponential()) && (mData->type()==Cy::VF64))
    {
      CYF64 *data = (CYF64 *)mData;
      value = cyRound(value, data->precisionSection());
      data->setValSection(value);
      emit widget->tmpValue(data->tmp());
      ret = value;
    }
    else
    {
      if (mData)
        value = (long)cyRound(value, mData->precision());

      // AUTRE VALEUR ENTIERE
      if (mData && ( (value > cyRound(maxValue(), mData->precision())) || (value < cyRound(minValue(), mData->precision())) ) )
      {
        widget->setInvalid(true);
        int val = QSpinBox::value();
        emit widget->tmpValue(val);
        ret = val;
      }
      else
      {
        emit widget->tmpValue(value);
        ret = value;
      }
      if (mData)
        mData->setTmp(ret);
    }
  }
  // VALEUR FLOTTANTE
  else
  {
    double value = cleanText().toDouble(ok);
    if (!ok)
      return 0;

    if (mData && (mData->isExponential()) && (mData->type()==Cy::VF32))
    {
      CYF32 *data = (CYF32 *)mData;
      value = cyRound(value, data->precisionSection());
      data->setValSection(value);
      emit widget->tmpValue(data->tmp());
      ret = d->mapToInt(value, ok);
    }
    else if (mData && (mData->isExponential()) && (mData->type()==Cy::VF64))
    {
      CYF64 *data = (CYF64 *)mData;
      value = cyRound(value, data->precisionSection());
      data->setValSection(value);
      emit widget->tmpValue(data->tmp());
      ret = d->mapToInt(value, ok);
    }
    else if (mData)
    {
      value = (double)cyRound(value, mData->precision());
      QString maxValString(QString("%1").arg(cyRound(maxValue(), mData->precision())));
      QString minValString(QString("%1").arg(cyRound(minValue(), mData->precision())));
      if ((value > maxValString.toDouble()) || (value < minValString.toDouble()))
      {
        widget->setInvalid(true);
        int val = QSpinBox::value();
        emit widget->tmpValue(CYNumInput::value());
        ret = val;
      }
      else
      {
        if (mData->type()==Cy::Sec)
        {
          value = cleanText().toDouble(ok);
          value = cyRound(value, mData->precision());
          if (!ok)
            return 0;
        }
        mData->setTmp(value);
        emit widget->tmpValue(value);
        ret = d->mapToInt(value, ok);
      }
    }
  }
  if (!mChangingSection && mEmitModifie && isVisible())
    emit widget->modifie();

  if (!mInvalid)
    widget->setLastValid(ret);
  return ret;
}

void CYNumInput::slotEditingFinished()
{
  return;
  int ret;
  bool *ok;
  ok = new bool;
  int base = ( mData && (mNumBase==BaseData) ) ? mData->numBase() : mNumBase;
  if ((base!=Hexadecimal) && (base!=Binary) && (base!=Decimal))
    base = Decimal;

  // VALEUR ENTIERE
  if (d->mIsInt || (base!=Decimal))
  {
    int value = cleanText().toInt(ok, base);
    if (!ok)
      return;
    // TEMPO
    if (mData && (mData->type()==Cy::Time))
    {
      CYTime *time = (CYTime *)mData;
      value = cyRound(value, time->precisionSection());
      time->setValSection(value);
      emit tmpValue(time->tmp());
      ret = value;
    }
    else if (mData && (mData->isExponential()) && (mData->type()==Cy::VF32))
    {
      CYF32 *data = (CYF32 *)mData;
      value = cyRound(value, data->precisionSection());
      data->setValSection(value);
      emit tmpValue(data->tmp());
      ret = value;
    }
    else if (mData && (mData->isExponential()) && (mData->type()==Cy::VF64))
    {
      CYF64 *data = (CYF64 *)mData;
      value = cyRound(value, data->precisionSection());
      data->setValSection(value);
      emit tmpValue(data->tmp());
      ret = value;
    }
    else
    {
      if (mData)
        value = (long)cyRound(value, mData->precision());

      // AUTRE VALEUR ENTIERE
      if (mData && ( (value > cyRound(maxValue(), mData->precision())) || (value < cyRound(minValue(), mData->precision())) ) )
      {
        setInvalid(true);
        int val = QSpinBox::value();
        emit tmpValue(val);
        ret = val;
      }
      else
      {
        emit tmpValue(value);
        ret = value;
      }
      if (mData)
        mData->setTmp(ret);
    }
  }
  // VALEUR FLOTTANTE
  else
  {
    double value = cleanText().toDouble(ok);
    if (!ok)
      return;

    if (mData && (mData->isExponential()) && (mData->type()==Cy::VF32))
    {
      CYF32 *data = (CYF32 *)mData;
      value = cyRound(value, data->precisionSection());
      data->setValSection(value);
      emit tmpValue(data->tmp());
      ret = d->mapToInt(value, ok);
    }
    else if (mData && (mData->isExponential()) && (mData->type()==Cy::VF64))
    {
      CYF64 *data = (CYF64 *)mData;
      value = cyRound(value, data->precisionSection());
      data->setValSection(value);
      emit tmpValue(data->tmp());
      ret = d->mapToInt(value, ok);
    }
    else if (mData)
    {
      value = (double)cyRound(value, mData->precision());
      QString maxValString(QString("%1").arg(cyRound(maxValue(), mData->precision())));
      QString minValString(QString("%1").arg(cyRound(minValue(), mData->precision())));
      if ((value > maxValString.toDouble()) || (value < minValString.toDouble()))
      {
        setInvalid(true);
        int val = QSpinBox::value();
        emit tmpValue(CYNumInput::value());
        ret = val;
      }
      else
      {
        if (mData->type()==Cy::Sec)
        {
          value = cleanText().toDouble(ok);
          value = cyRound(value, mData->precision());
          if (!ok)
            return;
        }
        mData->setTmp(value);
        emit tmpValue(value);
        ret = d->mapToInt(value, ok);
      }
    }
  }
  if (!mChangingSection && mEmitModifie && isVisible())
    emit modifie();

  if (!mInvalid)
    setLastValid(ret);
}

void CYNumInput::setValidator(const QValidator *)
{
  // silently discard the new validator. We don't want another one ;-)
}

void CYNumInput::slotValueChanged(int value)
{
  if (!d->mIsInt)
  {
    emit valueChanged(d->mapToDouble(value));
  }
}

void CYNumInput::slotValueChanged(const QString & text)
{
  CYDEBUGTEXT(text)
}

void CYNumInput::updateValidator()
{
  if (numBase()==Cy::Hexadecimal)
  {
    if (!d->mIntValidator)
    {
      d->mIntValidator =  new CYHexaSpinBoxValidator(minValue(), maxValue(), numBase(), this, "d->mIntValidator");
      lineEdit()->setValidator(d->mIntValidator);
    }
    else
    {
      d->mIntValidator->setRange(minValue(), maxValue());
    }
  }
  else
  {
    if (!d->mDoubleValidator)
    {
      d->mDoubleValidator =  new CYDoubleSpinBoxValidator(minValue(), maxValue(), precision(), this, "d->mDoubleValidator");
      lineEdit()->setValidator(d->mDoubleValidator);
    }
    else
    {
      d->mDoubleValidator->setRange(minValue(), maxValue(), precision());
    }
  }
}

void CYNumInput::setEditFocus(bool mark)
{
  lineEdit()->setFocus();
  if(mark)
    selectAll();
}

void CYNumInput::setData(CYData *data)
{
  mNewNumDec=-1;
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYNumInput::linkData()
{
  ctrlHideFlag();
  if (!hasData())
    return;
  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    else
    {
      lineEdit()->setText(mDataName);
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  mEmitModifie  = false;

  setPrecision(mData->nbDec());
  switch (mData->type())
  {
    case Cy::VS8    :
    {
      d->mIsInt  = true;
      CYS8 *data = (CYS8 *)mData;
      setRange(data->min(), data->max(), singleStep(), 0);
      setValue((double)data->val());
      if (mShowUnit)
        setDataSuffix(" "+data->unit());
      if (!data->stringDef().isEmpty())
        mSpecialValue = prefix() + data->stringDef() + suffix();
      break;
    }
    case Cy::VS16   :
    {
      d->mIsInt   = true;
      CYS16 *data = (CYS16 *)mData;
      setRange(data->min(), data->max(), singleStep(), 0);
      setValue((double)data->val());
      if (mShowUnit)
        setDataSuffix(" "+data->unit());
      if (!data->stringDef().isEmpty())
        mSpecialValue = prefix() + data->stringDef() + suffix();
      break;
    }
    case Cy::VS32   :
    {
      d->mIsInt   = true;
      CYS32 *data = (CYS32 *)mData;
      setRange(data->min(), data->max(), singleStep(), 0);
      setValue((double)data->val());
      if (mShowUnit)
        setDataSuffix(" "+data->unit());
      if (!data->stringDef().isEmpty())
        mSpecialValue = prefix() + data->stringDef() + suffix();
      break;
    }
    case Cy::VU8    :
    {
      d->mIsInt  = true;
      CYU8 *data = (CYU8  *)mData;
      setRange(data->min(), data->max(), singleStep(), 0);
      setValue((double)data->val());
      if (mShowUnit)
        setDataSuffix(" "+data->unit());
      if (!data->stringDef().isEmpty())
        mSpecialValue = prefix() + data->stringDef() + suffix();
      break;
    }
    case Cy::VU16   :
    {
      d->mIsInt   = true;
      CYU16 *data = (CYU16 *)mData;
      setRange(data->min(), data->max(), singleStep(), 0);
      setValue((double)data->val());
      if (mShowUnit)
        setDataSuffix(" "+data->unit());
      if (!data->stringDef().isEmpty())
        mSpecialValue = prefix() + data->stringDef() + suffix();
      break;
    }
    case Cy::VU32   :
    {
      d->mIsInt   = true;
      CYU32 *data = (CYU32 *)mData;
      setRange(data->min(), data->max(), singleStep(), 0);
      setValue((double)data->val());
      if (mShowUnit)
        setDataSuffix(" "+data->unit());
      if (!data->stringDef().isEmpty())
        mSpecialValue = prefix() + data->stringDef() + suffix();
      break;
    }
    case Cy::VF32   :
    {
      d->mIsInt   = false;
      CYF32 *data = (CYF32 *)mData;
      if (data->isExponential())
      {
        data->setTmp(data->val());
        data->initToSectionMaxi();
        changeSection();
        setRange(data->minSection(), data->maxSection(), data->precisionSection(), mData->nbDec());
        setValue(data->valSection());
        setDataPrefix(data->prefix());
        setDataSuffix(data->suffix());
        if (!data->stringDef().isEmpty())
        {
          if (!data->unit().isEmpty())
            mSpecialValue = data->stringDef() + " " + data->unit();
          else
            mSpecialValue = data->stringDef();
        }
      }
      else
      {
        setRange(data->min(), data->max(), singleStep(), mData->nbDec());
        setValue((double)data->val());
        if (mShowUnit)
          setDataSuffix(" "+data->unit());
        if (!data->stringDef().isEmpty())
          mSpecialValue = prefix() + data->stringDef() + suffix();
      }
      break;
    }
    case Cy::VF64   :
    {
      d->mIsInt   = false;
      CYF64 *data = (CYF64 *)mData;
      if (data->isExponential())
      {
        data->setTmp(data->val());
        data->initToSectionMaxi();
        changeSection();
        setRange(data->minSection(), data->maxSection(), data->precisionSection(), mData->nbDec());
        setValue(data->valSection());
        setDataPrefix(data->prefix());
        setDataSuffix(data->suffix());
        if (!data->stringDef().isEmpty())
        {
          if (!data->unit().isEmpty())
            mSpecialValue = data->stringDef() + " " + data->unit();
          else
            mSpecialValue = data->stringDef();
        }
      }
      else
      {
        setRange(data->min(), data->max(), singleStep(), mData->nbDec());
        setValue((double)data->val());
        if (mShowUnit)
          setDataSuffix(" "+data->unit());
        if (!data->stringDef().isEmpty())
          mSpecialValue = prefix() + data->stringDef() + suffix();
      }
      break;
    }
    case Cy::Time   :
    {
      d->mIsInt   = true;
      CYTime *data = (CYTime *)mData;
      data->setTmp(data->val());
      setRange(data->minSection(), data->maxSection(), data->precisionSection());
      setValue(data->valSection());
      setDataPrefix(data->prefix());
      setDataSuffix(data->suffix());
      if (!data->stringDef().isEmpty())
      {
        mSpecialValue = prefix() + data->stringDef() + suffix();
      }
      break;
    }
    case Cy::Sec    :
    {
      d->mIsInt   = false;
      CYTSec *data = (CYTSec *)mData;
      setRange(data->min(), data->max(), singleStep(), mData->nbDec());
      setValue((double)data->val());
      if (mShowUnit)
        setDataSuffix(" "+data->unit());
      if (!data->stringDef().isEmpty())
        mSpecialValue = prefix() + data->stringDef() + suffix();
      break;
    }
    default         : CYWARNINGTEXT(QString("void CYNumInput::linkData(): %1 de %2 "
                                            "=> type de la donnÃ©e %3 incorrecte")
                                    .arg(objectName())
                                    .arg(parent()->objectName())
                                    .arg(mData->objectName()));
  }
  bool readOnly = mReadOnly;
  setReadOnly(false);
  mPalette.setColor(QPalette::Active, QPalette::Text, mData->inputColor());
  QSpinBox::setPalette(mPalette);
  updateROPalette();
  setReadOnly(readOnly);

  if (mInputHelp)
  {
    this->setToolTip(QString(mData->inputHelp()+"<br><br>"+mData->infoCY()));
  }
  else
  {
    this->setToolTip(QString(mData->displayHelp()+"<br><br>"+mData->infoCY()));
  }

  if (mAutoMinimumSize)
  {
    QStyle *style = qApp->style();
    //    QStyleOptionButton option;
    //    option.initFrom(this);
    //    QRect upRect = style->sizeFromContents(QStyle::CT_SpinBox, &option, &contentsRect().size(),this);
    //    setMinimumWidth(lineEdit()->fontMetrics().width(mSpecialValue)+upRect.width());
    setMinimumWidth(lineEdit()->fontMetrics().width(mSpecialValue)+50);
  }
  updateValidator();
  ctrlFlag();

  //   if (mData && (QString(mData->objectName())=="LINCAN_FRAME0_DATA1_VAL"))
  //     CYDEBUGTEXT(QString("%1 %2 %3 %4)").arg(QSpinBox::value()).arg(d->mPrecision).arg(d->factor()).arg(d->basicStep()))

  mEmitModifie = true;
}

void CYNumInput::refresh()
{
  if (movable())
    refreshPosition();

  ctrlHideFlag();

  if (!hasData())
    return;

  if (!readOnly())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    else
    {
      lineEdit()->setText(mDataName);
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  ctrlFlag();

  switch (mData->type())
  {
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      setValue((double)data->val());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      setValue((double)data->val());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      setValue((double)data->val());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      setValue((double)data->val());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      setValue((double)data->val());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      setValue((double)data->val());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      if (data->isExponential())
      {
        data->setTmp(data->val());
        setDataPrefix(data->prefix());
        setDataSuffix(data->suffix());
        setValue(data->valSection());
      }
      else
        setValue((double)data->val());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      if (data->isExponential())
      {
        data->setTmp(data->val());
        setDataPrefix(data->prefix());
        setDataSuffix(data->suffix());
        setValue(data->valSection());
      }
      else
        setValue((double)data->val());
      break;
    }
    case Cy::Time   :
    {
      CYTime *data = (CYTime *)mData;
      data->setTmp(data->val());
      setDataPrefix(data->prefix());
      setDataSuffix(data->suffix());
      setValue(data->valSection());
      break;
    }
    case Cy::Sec    :
    {
      CYTSec *data = (CYTSec *)mData;
      setValue((double)data->val());
      break;
    }
    default         : CYWARNINGTEXT(QString("void CYNumInput::loadVal(): %1 de %2 "
                                            "=> type de la donnÃ©e %3 incorrecte")
                                    .arg(objectName())
                                    .arg(parent()->objectName())
                                    .arg(mData->objectName()));
  }
}

void CYNumInput::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  switch (mData->type())
  {
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      setValue((double)data->def());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      setValue((double)data->def());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      setValue((double)data->def());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      setValue((double)data->def());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      setValue((double)data->def());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      setValue((double)data->def());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      if (data->isExponential())
      {
        data->designer();
        setDataPrefix(data->prefix());
        setDataSuffix(data->suffix());
        setValue(data->defSection());
      }
      else
        setValue((double)data->def());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      if (data->isExponential())
      {
        data->designer();
        setDataPrefix(data->prefix());
        setDataSuffix(data->suffix());
        setValue(data->defSection());
      }
      else
        setValue((double)data->def());
      break;
    }
    case Cy::Time   :
    {
      CYTime *data = (CYTime *)mData;
      data->designer();
      setDataPrefix(data->prefix());
      setDataSuffix(data->suffix());
      setValue(data->defSection());
      break;
    }
    case Cy::Sec   :
    {
      CYTSec *data = (CYTSec *)mData;
      data->designer();
      setValue((double)data->def());
      break;
    }
    default         : CYWARNINGTEXT(QString("void CYNumInput::designer(): %1 de %2 "
                                            "=> type de la donnÃ©e %3 incorrecte")
                                    .arg(objectName())
                                    .arg(parent()->objectName())
                                    .arg(mData->objectName()));
  }
}

void CYNumInput::update()
{
  if (movable())
    updatePosition();

  if (!hasData())
    return;

  if (!isVisible() && mUpdateIfVisible)
    return;

  if (!isEnabled())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (mNewNumDec!=-1)
  {
    mData->setNbDec(mNewNumDec);
    mNewNumDec=-1;
  }

  mEmitModifie = false;
  mUpdateOk    = true;

  switch (mData->type())
  {
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      data->setVal((s8)value());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      data->setVal((s16)value());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      data->setVal((s32)value());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      data->setVal((u8)value());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      data->setVal((u8)value());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      data->setVal((u32)value());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      if (data->isExponential())
      {
        data->setValSection(value());
        if (data->isValid())
        {
          data->setVal(data->tmp());
          mInvalid = false;
        }
        else
        {
          linkData();
          mInvalid = true;
        }
      }
      else
        data->setVal(value());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      if (data->isExponential())
      {
        data->setValSection(value());
        data->setVal(data->tmp());
        if (data->isValid())
        {
          data->setVal(data->tmp());
          mInvalid = false;
        }
        else
        {
          linkData();
          mInvalid = true;
        }
      }
      else
        data->setVal(value());
      break;
    }
    case Cy::Time   :
    {
      CYTime *data = (CYTime *)mData;
      data->setValSection((int)value());
      if (data->isValid())
      {
        data->setVal((u32)data->tmp());
        mInvalid = false;
      }
      else
      {
        linkData();
        mInvalid = true;
      }
      break;
    }
    case Cy::Sec    :
    {
      CYTSec *data = (CYTSec *)mData;
      data->setVal((f32)value());
      break;
    }
    default         : CYWARNINGTEXT(QString("void CYNumInput::update(): %1 de %2 "
                                            "=> type de la donnÃ©e %3 incorrecte")
                                    .arg(objectName())
                                    .arg(parent()->objectName())
                                    .arg(mData->objectName()));
  }

  mEmitModifie = true;
}

bool CYNumInput::ctrlFlag()
{
  if (mFlag==0)
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;
  if (mFlag->val() && !mInverseFlag)
  {
    res = true;
  }
  else if (!mFlag->val() && mInverseFlag)
  {
    res = true;
  }
  else
  {
    res = false;
  }

  setEnabled(res);
  return res;
}

void CYNumInput::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

bool CYNumInput::event(QEvent *e)
{
  mEmitModifie = true;

  if (e->type() == QEvent::FocusIn)
  {
    emit focusIn();
    if (mMovieStepFocusIn!=-1)
      emit focusIn(mMovieStepFocusIn);
  }
  if (e->type() == QEvent::FocusOut)
  {
    if (mData && (mData->isExponential() || (mData->type()==Cy::Time)) )
      update();
    if (!mTestingNewDataValue && testNewDataValue())
    {
      emit focusOut();
      if (mMovieStepFocusOut!=-1)
        emit focusOut(mMovieStepFocusOut);
    }
    else
    {
      setFocus();
      return false;
    }
  }

  if (e->type() == QEvent::KeyPress)
  {
    QKeyEvent *k = (QKeyEvent*)e;

    switch (k->key())
    {
      case Qt::Key_Return   :
        if (mEnableEdit)
        {
          update();
          if (testNewDataValue())
          {
            emit applying();
            return true;
          }
        }
        break;
      case Qt::Key_Enter    :
        if (mEnableEdit)
        {
          update();
          if (testNewDataValue())
          {
            emit applying();
            return true;
          }
        }
        break;

      case Qt::Key_Delete   :
        if (mEnableEdit)
          emit modifie();
        break;

      case Qt::Key_Backspace:
        if (mEnableEdit)
          emit modifie();
        break;

      case Qt::Key_Home     :
        if ((mData) && (k->QInputEvent::modifiers() != Qt::ShiftModifier))
        {
          if (mData->type() == Cy::Time)
          {
            CYTime *time = (CYTime *)mData;
            if (time->sectionMaxi(value()))
              changeSection();
            return true;
          }
          else if (mData->isExponential() && (mData->type()==Cy::VF32))
          {
            CYF32 *data = (CYF32 *)mData;
            if (data->sectionMaxi(value()))
              changeSection();
            return true;
          }
          else if (mData->isExponential() && (mData->type()==Cy::VF64))
          {
            CYF64 *data = (CYF64 *)mData;
            if (data->sectionMaxi(value()))
              changeSection();
            return true;
          }
        }

      case Qt::Key_End      :
        if ((mData) && (k->QInputEvent::modifiers() != Qt::ShiftModifier))
        {
          if (mData->type() == Cy::Time)
          {
            CYTime *time = (CYTime *)mData;
            if (time->sectionMini(value()))
              changeSection();
            return true;
          }
          else if (mData->isExponential() && (mData->type()==Cy::VF32))
          {
            CYF32 *data = (CYF32 *)mData;
            if (data->sectionMini(value()))
              changeSection();
            return true;
          }
          else if (mData->isExponential() && (mData->type()==Cy::VF64))
          {
            CYF64 *data = (CYF64 *)mData;
            if (data->sectionMini(value()))
              changeSection();
            return true;
          }
        }

      case Qt::Key_Left     :
        if ((mData) && (k->QInputEvent::modifiers() != Qt::KeypadModifier) && (k->QInputEvent::modifiers() != Qt::ShiftModifier))
        {
          if (mData->type() == Cy::Time)
          {
            CYTime *time = (CYTime *)mData;
            if (time->sectionUp(value()))
              changeSection();
            return true;
          }
          else if (mData->isExponential() && (mData->type()==Cy::VF32))
          {
            CYF32 *data = (CYF32 *)mData;
            if (data->sectionUp(value()))
              changeSection();
            return true;
          }
          else if (mData->isExponential() && (mData->type()==Cy::VF64))
          {
            CYF64 *data = (CYF64 *)mData;
            if (data->sectionUp(value()))
              changeSection();
            return true;
          }
        }

      case Qt::Key_Right    :
        if ((mData) && (k->QInputEvent::modifiers() != Qt::KeypadModifier) && (k->QInputEvent::modifiers() != Qt::ShiftModifier))
        {
          if (mData->type() == Cy::Time)
          {
            CYTime *time = (CYTime *)mData;
            if (time->sectionDown(value()))
              changeSection();
            return true;
          }
          else if (mData->isExponential() && (mData->type()==Cy::VF32))
          {
            CYF32 *data = (CYF32 *)mData;
            if (data->sectionDown(value()))
              changeSection();
            return true;
          }
          else if (mData->isExponential() && (mData->type()==Cy::VF64))
          {
            CYF64 *data = (CYF64 *)mData;
            if (data->sectionDown(value()))
              changeSection();
            return true;
          }
        }
      case Qt::Key_E        :
        if ((mData) && (k->QInputEvent::modifiers() != Qt::KeypadModifier) && (k->QInputEvent::modifiers() != Qt::ShiftModifier))
        {
          if (mData->isExponential() && (mData->type()==Cy::VF32))
          {
            CYF32 *data = (CYF32 *)mData;
            if (data->sectionDown(value()))
              changeSection();
            return true;
          }
          else if (mData->isExponential() && (mData->type()==Cy::VF64))
          {
            CYF64 *data = (CYF64 *)mData;
            if (data->sectionDown(value()))
              changeSection();
            return true;
          }
        }
      case Qt::Key_A          :
      case Qt::Key_B          :
      case Qt::Key_C          :
      case Qt::Key_D          :
      case Qt::Key_F          :
        if (numBase()==Cy::Hexadecimal)
        {
          if (!mEnableEdit)
            return true;
          slotHexaKeyPress();
          break;
        }
      case Qt::Key_1          :
      case Qt::Key_2          :
      case Qt::Key_3          :
      case Qt::Key_4          :
      case Qt::Key_5          :
      case Qt::Key_6          :
      case Qt::Key_7          :
      case Qt::Key_8          :
      case Qt::Key_9          :
      case Qt::Key_Minus      :
      case Qt::Key_Comma      :
        if (!mEnableEdit)
          return true;
        slotNumKeyPress();
        break;
    }
  }

  if (hasFocus() && (e->type() == QEvent::MouseButtonPress))
  {
    QTimer::singleShot( 250, this, SLOT(slotMouseButtonPress()) );
  }

  return QSpinBox::event(e);
}

void CYNumInput::enterEvent ( QEvent *e )
{
  QSpinBox::enterEvent(e);
  emit enteringEvent();
}

void CYNumInput::leaveEvent ( QEvent *e )
{
  QSpinBox::leaveEvent(e);
  emit leavingEvent();
}

void CYNumInput::stepBy(int steps)
{
  if (!hasFocus())
    return;
  QSpinBox::stepBy(steps);
//  valueFromText(text());
  if ((mEmitModifie) && isVisible())
    emit modifie();
}

void CYNumInput::textChanged()
{
  //  lineEdit()->textChanged();
  if ((mEmitModifie) && isVisible())
    emit modifie();
}

void CYNumInput::changeSection()
{
  mChangingSection = true;
  if ((mData) && (mData->type() == Cy::Time))
  {
    CYTime *time = (CYTime *)mData;
    mNoControl   = true;
    mEmitModifie = false;
    int old = time->valSection();
    setRange(time->minSection(), time->maxSection(), time->precisionSection());
    uint tmp = time->tmp();
    setValue(old);
    time->setTmp(tmp);
    setDataPrefix(time->prefix());
    setDataSuffix(time->suffix());
    selectAll();
    getTmpValue();
    mEmitModifie = true;
  }
  else if (mData && mData->isExponential() && (mData->type()==Cy::VF32))
  {
    CYF32 *data = (CYF32 *)mData;
    mNoControl   = true;
    mEmitModifie = false;
    d->mIsInt = data->isSectionMini() ? true : false;
    double old = data->valSection();
    setRange(data->minSection(), data->maxSection(), data->precisionSection(), data->nbDec());
    setValue(old);
    setDataPrefix(data->prefix());
    setDataSuffix(data->suffix());
    updateValidator();
    selectAll();
    getTmpValue();
    mEmitModifie = true;
  }
  else if (mData && mData->isExponential() && (mData->type()==Cy::VF64))
  {
    CYF64 *data = (CYF64 *)mData;
    mNoControl   = true;
    mEmitModifie = false;
    d->mIsInt = data->isSectionMini() ? true : false;
    double old = data->valSection();
    setRange(data->minSection(), data->maxSection(), data->precisionSection(), data->nbDec());
    setValue(old);
    setDataPrefix(data->prefix());
    setDataSuffix(data->suffix());
    updateValidator();
    selectAll();
    getTmpValue();
    mEmitModifie = true;
  }
  mChangingSection = false;
}

void CYNumInput::setMilliseconds(unsigned int val)
{
}

void CYNumInput::setSeconds(unsigned int val)
{
}

double CYNumInput::getTmpValue()
{
  double ret;
  if (mData)
  {
    if (mData->type()==Cy::Time)
    {
      CYTime *time = (CYTime *)mData;
      ret = time->tmp();
      emit tmpValue(ret);
    }
    else if (mData->type()==Cy::Sec)
    {
      CYTSec *tsec = (CYTSec *)mData;
      ret = tsec->tmp();
      emit tmpValue(ret);
    }
    else if (mData->isExponential() && (mData->type()==Cy::VF32))
    {
      CYF32 *data = (CYF32 *)mData;
      ret = data->tmp();
      emit tmpValue(ret);
    }
    else if (mData->isExponential() && (mData->type()==Cy::VF64))
    {
      CYF64 *data = (CYF64 *)mData;
      ret = data->tmp();
      emit tmpValue(ret);
    }
    else
    {
      ret = value();
      emit tmpValue(ret);
    }
  }
  else
  {
    ret = value();
    emit tmpValue(ret);
  }
  return ret;
}

void CYNumInput::setCurrentDataName(int index)
{
  CYDataWdg::setCurrentDataName(index);
  emit currentDataChanged();
  if ((mEmitModifie) && isVisible())
    emit modifie();
}

void CYNumInput::setCurrentData(int index)
{
  CYDataWdg::setCurrentData(index);
  emit currentDataChanged();
  if ((mEmitModifie) && isVisible())
    emit modifie();
}

void CYNumInput::setForcingData(CYData *data)
{
  CYDataWdg::setForcingData(data);
  if (data)
  {
    connect(data, SIGNAL(synopticForcingChanged(double)), SLOT(synopticForcingChanged(double)));
    connect(this, SIGNAL(valueChanged(double)), data, SIGNAL(synopticForcingChanged(double)));
  }
}

void CYNumInput::synopticForcingChanged(double value)
{
  setValue(value);
}

QString CYNumInput::forward0() const
{
  QString val = QString("%1").arg((int)value());
  QString max = QString("%1").arg((int)maxValue());
  int nb = max.length()-val.length();
  QString ret;
  return ret.fill('0', nb);
}

void CYNumInput::setBackgroundColor(const QColor &color)
{
  mBackgroundColor=color;
  setAutoFillBackground(true);
  QPalette pal = palette();
  pal.setColor(QPalette::Base, mBackgroundColor);
  setPalette(pal);
  updateROPalette();

  if (!core)
  {
    // mise Ã  jour dans Qt Designer
    QString tmp = styleSheet();
    setStyleSheet(" ");
    setStyleSheet(tmp);
  }
}

void CYNumInput::setForegroundColor(const QColor &color)
{
  mForegroundColor=color;
  QPalette pal = palette();
  pal.setColor(foregroundRole(), mForegroundColor);
  setPalette(pal);
  updateROPalette();
  if (!core)
  {
    // mise à jour dans Qt Designer
    QString tmp = styleSheet();
    setStyleSheet(" ");
    setStyleSheet(tmp);
  }
}

void CYNumInput::setPalette(const QPalette &palette)
{
  QSpinBox::setPalette(palette);
  updateROPalette();
}

void CYNumInput::setEnabled(bool state)
{
  if (readOnly() && state)
    return;
  QSpinBox::setEnabled(state);
  mEnabled = state;
}

bool CYNumInput::testNewDataValue()
{
  mTestingNewDataValue = true;
  bool ret = mInvalid;
  mInvalid = false;
  if (mData && ret)
  {
    CYMessageBox::sorry(this, mData->inputHelp());
  }
  mTestingNewDataValue = false;
  return ~ret;
}

/*! Action différée suite à un appui sur la touche A,B,C,D,E ou F en saisie Hexa.
    \fn CYNumInput::slotHexaKeyPress()
 */
void CYNumInput::slotHexaKeyPress()
{
  emit modifie();
}

/*! Action différée suite à un appui d'un boutton numérique pour laisser le temps de saisie.
    \fn CYNumInput::slotNumKeyPress()
 */
void CYNumInput::slotNumKeyPress()
{
  emit modifie();
}


/*! Action suite à un clic gauche de souris. Permet de cliquer sur la section à éditer.
    \fn CYNumInput::slotMouseButtonPress()
 */
void CYNumInput::slotMouseButtonPress()
{
  QString left = lineEdit()->text().left(lineEdit()->cursorPosition());
  if (mData && (mData->type() == Cy::Time))
  {
    CYTime *time = (CYTime *)mData;
    time->setValSection((int)value());
    if (time->section(left))
      changeSection();
  }
  else if (mData && mData->isExponential() && (mData->type()==Cy::VF32))
  {
    CYF32 *data = (CYF32 *)mData;
    if (testNewDataValue())
    {
      data->setValSection(value());
      if (data->section(left))
        changeSection();
    }
  }
  else if (mData && mData->isExponential() && (mData->type()==Cy::VF64))
  {
    CYF64 *data = (CYF64 *)mData;
    data->setValSection(value());
    if (data->section(left))
      changeSection();
  }
}

void CYNumInput::setDataPrefix( const QString & text )
{
  QString txt = prefix();
  txt = txt.remove(mDataPrefix);
  mDataPrefix = text;
  setPrefix(txt+mDataPrefix);
}

void CYNumInput::setDataSuffix( const QString & text )
{
  QString txt = suffix();
  txt = txt.remove(mDataSuffix);
  mDataSuffix = text;
  setSuffix(mDataSuffix+txt);
}


/*! Saisie le nombre de décimales de la donnée si elle est de type flottant
    \fn CYNumInput::setDataNbDec(int nb)
 */
void CYNumInput::setDataNbDec(int nb)
{
  if (mData && ((mData->type()==Cy::VF32) || (mData->type()==Cy::VF64)))
  {
    mNewNumDec=-1;
    int old=mData->nbDec();
    mData->setNbDec(nb);
    /*    if (nb<=0)
      setPrecision(0,true);*/
    linkData();
    mData->setNbDec(old);
    mNewNumDec=nb;
  }
}

/*! Remet le nombre de décimales par défaut de la donnée si elle est de type flottant
    \fn CYNumInput::resetDataNbDec()
 */
void CYNumInput::resetDataNbDec()
{
  if (mData && ((mData->type()==Cy::VF32) || (mData->type()==Cy::VF64)))
  {
    mNewNumDec=-1;
    //     setPrecision(mData->precision());
    mData->setNbDec(mData->nbDec());
    linkData();
    mNewNumDec=mData->nbDec();
  }
}


/*! Gestion d'appui de bouton de souris
    \fn CYNumInput::mousePressEvent ( QMouseEvent * e )
 */
void CYNumInput::mousePressEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions )
  {
    mXOldPos = x();
    mYOldPos = y();
    mXMousePos = e->x();
    mYMousePos = e->y();
    emit movedInsideParent();
  }
  QSpinBox::mousePressEvent ( e );
}


/*! Gestion de déplacement de bouton de souris
    \fn CYNumInput::mouseMoveEvent ( QMouseEvent * e )
 */
void CYNumInput::mouseMoveEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions && (e->buttons()==Qt::LeftButton) )
  {
    int ex = e->x();
    int ey = e->y();
    int x = mXOldPos + ( ex - mXMousePos );
    int y = mYOldPos + ( ey - mYMousePos );
    moveInsideParent( x, y );
    raise();
  }
  QSpinBox::mouseMoveEvent( e );
}


/*! Déplacement à l'intérieur du parent.
    \fn CYNumInput::moveInsideParent(int x, int y)
 */
void CYNumInput::moveInsideParent(int x, int y)
{
  QWidget * p = (QWidget *)parent();
  if ( !p->rect().contains(x, p->rect().y()) )
  {
    if ( x < p->rect().left() )
      x = p->rect().left();            // sort à gauche
    else
      x = p->rect().right()-width();   // sort à droite
  }
  else if ( !p->rect().contains(x+width(), p->rect().y()) )
  {
    x = p->rect().right()-width();     // sort en partie à droite
  }

  if ( !p->rect().contains(p->rect().x(), y) )
  {
    if ( y < p->rect().top() )
      y = p->rect().top();             // sort en haut
    else
      y = p->rect().bottom()-height(); // sort en bas
  }
  else if ( !p->rect().contains(p->rect().x(), y+height()) )
  {
    y = p->rect().bottom()-height();   // sort en partie en bas
  }

  move(x, y);
  mXOldPos = x;
  mYOldPos = y;

  emit modifie();
}


/*! Active/désactive le déplacement
    \fn CYNumInput::setEditPositions( bool val )
 */
void CYNumInput::setEditPositions( bool val )
{
  if ( !movable() )
    return;

  mEditPositions = val;
  refreshPosition();

  if ( mEditPositions )
    setCursor( Qt::PointingHandCursor );
  else
    unsetCursor();
}

/*! Rafraîchit la position de l'objet en fonction des données positions.
    \fn CYDataWdg::refreshPosition()
 */
void CYNumInput::refreshPosition()
{
  if ( !movable() )
    return;

  if ( ( mPosX && mPosY ) && ( ( mPosX->val()!=x() ) || ( mPosY->val()!=y() ) ) )
  {
    if ( (mPosX->val()>=0) && (mPosY->val()>=0) )
      move( mPosX->val(), mPosY->val() );
    else if ( mPosX->val()>=0 )
      move( mPosX->val(), y()          );
    else if ( mPosY->val()>=0 )
      move( x()         , mPosY->val() );
  }
  else if ( ( mPosX ) && ( mPosX->val()!=x() ) && ( mPosX->val()>=0 ) )
  {
    move( mPosX->val(), y()          );
  }
  else if ( ( mPosY ) && ( mPosY->val()!=y() ) && ( mPosY->val()>=0 ) )
    move( x()         , mPosY->val() );
}


/*! Met àjour des données positions en fonction de la position de l'objet.
    \fn CYDataWdg::updatePosition()
 */
void CYNumInput::updatePosition()
{
  if ( !mEditPositions )
    return;

  if ( mPosX )
    mPosX->setVal( x() );

  if ( mPosY )
    mPosY->setVal( y() );
}

void CYNumInput::showEvent(QShowEvent *e)
{
  QSpinBox::showEvent(e);
  if (!core)
  {
    // mise Ã  jour dans Qt Designer
    QString tmp = styleSheet();
    setStyleSheet(" ");
    setStyleSheet(tmp);
  }
}
