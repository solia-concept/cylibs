/***************************************************************************
                          cyboxdig.cpp  -  description
                             -------------------
    début                  : jeu fév 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cydig.h"
#include "cycarddig.h"
#include "cyboxdig.h"

CYBoxDIG::CYBoxDIG(QWidget *parent, const QString &name, QColor c, Mode m)
: CYBoxIO(parent, name, m),
  mColor(c)
{
  mSimul = false;
}

CYBoxDIG::~CYBoxDIG()
{
}
