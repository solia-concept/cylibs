/***************************************************************************
                          cypidinput.h  -  description
                             -------------------
    begin                : ven avr 23 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYPIDINPUT_H
#define CYPIDINPUT_H

// CYLIBS
#include <cywidget.h>
#include <cybuttongroup.h>

namespace Ui {
    class CYPIDInput;
}

/** @short Boîte de saisie d'un PID.
  * @author LE CLÉACH Gérald
  */

class CYPIDInput : public CYWidget
{
  Q_OBJECT
  Q_ENUMS(GroupSection)
  Q_PROPERTY(QString prefix READ prefix WRITE setPrefix)
  Q_PROPERTY(QString description READ description WRITE setDescription)
  Q_PROPERTY(QPixmap illustration READ prefix WRITE setIllustration)
  Q_PROPERTY(GroupSection showGroup READ showGroup WRITE setShowGroup)

public:
  CYPIDInput(QWidget *parent=0, const QString &name=0);
  ~CYPIDInput();

  /** Mode d'affichage du groupe. */
  /* ATTENTION énumération deavant être identique à Cy::GroupSection*/
  enum GroupSection
  {
    /** Pas d'affichage du groupe. */
    No,
    /** Affichage du dernier sous-groupe. */
    lastGroup,
    /** Affichage des sous-groupes. */
    underGroup,
    /** Affichage du groupe entier. */
    wholeGroup
  };


  /** Connection des objets graphiques avec les données de l'application. */
  virtual void linkDatas();

  /** @return le préfixe du nom des données. */
  QString prefix() const { return mPrefix; }
  /** Saisie le préfixe du nom des données. */
  void setPrefix(const QString &txt) { mPrefix = txt; linkDatas(); }

  /** @return la description affichée au dessus de l'illustration. */
  QString description() const { return mDescription; }
  /** Saisie le description affichée au dessus de l'illustrations. */
  void setDescription(const QString &txt);

  /** @return l'image d'illustration. */
  QPixmap illustration() const { return mIllustration; }
  /** Saisie l'image d'illustration. */
  void setIllustration(const QPixmap &p);
  /** @return le CYButtonGroup.*/
  virtual CYButtonGroup * buttonGroup();

  /** @return le mode d'affichage du groupe. */
  virtual GroupSection showGroup() const { return mShowGroup; }
  /** Saisir le mode d'affichage du groupe. */
  virtual void setShowGroup(const GroupSection val) { mShowGroup = val; }


protected: // Protected attributes
  /** Préfixe du nom des données.  */
  QString mPrefix;

  /** Description affichée au dessus de l'illustration.  */
  QString mDescription;

  /** Image d'illustration.  */
  QPixmap mIllustration;

  /** Mode d'affichage du groupe. */
  GroupSection mShowGroup;

protected slots:
    virtual void changeType(int type);

private:
    Ui::CYPIDInput *ui;
};

#endif
