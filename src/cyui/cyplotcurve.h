#ifndef CYPLOTCURVE_H
#define CYPLOTCURVE_H

#include "qwt_plot_curve.h"

class CYPlotCurve : public QwtPlotCurve
{
public:
  //! Identifier how to interprete a QVariant
  enum LegendRole
  {
      // L'idendifiant de la courbe
      KeyRole = QwtLegendData::UserRole+1,
  };

  explicit CYPlotCurve( const QString &title = QString() );
  explicit CYPlotCurve( const QwtText &title );

  virtual void setKey(const int val);
  int key() { return mKey; }

  QList<QwtLegendData> legendData() const;

protected:
  int mKey;
};

#endif // CYPLOTCURVE_H
