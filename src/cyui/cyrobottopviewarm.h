/***************************************************************************
                          cyrobottopviewarm.h  -  description
                             -------------------
    begin                : lun nov 1 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYROBOTTOPVIEWARM_H
#define CYROBOTTOPVIEWARM_H

// QWT
#include "qwt_dial_needle.h"

/** @short Bras d'un robot @see CYRobotTopView.
  * @author LE CLÉACH Gérald
  */

class CYRobotTopViewArm : public QwtDialSimpleNeedle // TOCHECK QT4 QwtDialNeedle
{
public: 
  CYRobotTopViewArm();
  ~CYRobotTopViewArm();

  /** Dessine le bras du robot avec comme course \a length et comme position angulaire \a direction. */
  virtual void draw(QPainter *painter, const QPoint &center, int length, double direction,
                    QPalette::ColorGroup cg = QPalette::Active) const;

  /** Saisie le pourcentage de la course du bras déployé. */
  void setPercentStroke(double val) { mPercentStroke = val; }
  /** @return le pourcentage de la course du bras déployé. */
  double percentStroke() { return mPercentStroke; }

protected:
//  const QColorGroup &colorGroup(QPalette::ColorGroup) const;

protected: // Protected attributes
  /** Pourcentage de la course du bras déployé. */
  double mPercentStroke;
  /** Pourcentage de la course du bras déployable par la première partie. */
  double mPercentStroke1;
  /** Pourcentage de la course du bras déployable par la deuxième partie. */
  double mPercentStroke2;
};

#endif
