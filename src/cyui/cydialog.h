/***************************************************************************
                          cydialog.h  -  description
                             -------------------
    début                  : mer mai 21 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDIALOG_H
#define CYDIALOG_H

// QT
#include <qdialog.h>
//Added by qt3to4:
#include <QString>
#include <QShowEvent>
#include <QKeyEvent>
// CYLIBS
#include "cytemplate.h"

class CYWidget;

/** CYDialog est une boîte de dialogue utile à la gestion d'un ensemble de données.
  * Cette boîte est prévue pour contenir différents objets de saisie de donnée tels
  * que des CYNumInput ou des CYFlagInput. Dès que l'un d'eux subit une intéraction
  * de la part de l'utilisateur, il signal à la boîte qu'une donnée a été modifiée.
  * Ainsi,si au moins une des données a changé, CYDialog offre la possibilité de
  * prendre en compte ces modifications en sauvegardant et en transférant aux hôtes
  * concernés les structures de données qu'elle gère.
  * De plus, elle est capable de gérer le rafaraîchissement automatique des affichages
  * de données tels que des CYNumDisplay.
  * @short Boîte de dialogue CY.
  * @author Gérald LE CLEACH
  */

class CYDialog : public QDialog, public CYTemplate
{
  Q_OBJECT
  Q_PROPERTY(QByteArray genericMark READ genericMark WRITE setGenericMark)
  Q_PROPERTY(bool linkDatasShow READ linkDatasShow WRITE setLinkDatasShow)
  Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
  Q_PROPERTY(QColor foregroundColor READ foregroundColor WRITE setForegroundColor)
  Q_PROPERTY(QPixmap backgroundPixmap READ backgroundPixmap WRITE setBackgroundPixmap)

public:
  CYDialog(QWidget *parent=0, const QString &name="cyDialog", Qt::WindowFlags f=Qt::WindowFlags());
  ~CYDialog();

  /** @return le repère de généricité utile aux widgets génériques.
    * @see void setGenericMark(const QString txt). */
  virtual QByteArray genericMark() const;
  /** Saisie le repère de généricité utile aux widgets génériques.
    * Par example un widget dans certain cas peut servir pour 2 postes.
    * Les noms des données pour les 2 postes doivent se différencier par le
    * numéro du poste. Il suffit alors de remplacer cette partie du nom par
    * un # dans dataName, flagName, forcingName et data2Name des widgets enfants.
    * Ex: DATA_POSTE1 & DATA_petit_poste1 => DATA_POSTE#
    * La rechercher des données remplacera automatiquement ce # par
    * le numéro du bon poste passé ici comme index. */
  virtual void setGenericMark(const QByteArray txt);
  /** Saisie un index qui une fois transformé en QString deviendra le repère
    * de généricité utile aux widgets génériques @see setGenericMark(QString txt). */
  virtual void setGenericMark(int index);

  /** Saisie la couleur de fond du widget. */
  virtual void setBackgroundColor ( const QColor & );
  /** @return la couleur de fond du widget */
  virtual const QColor & backgroundColor () const;

  /** Saisie la couleur du premier plan du widget. */
  virtual void setForegroundColor ( const QColor & );
  /** @return la couleur du premier plan du widget */
  virtual const QColor & foregroundColor () const;

  /** Saisie l'image de fond. */
  virtual void setBackgroundPixmap ( const QPixmap & );
  /** @return la couleur du premier plan du widget */
  virtual const QPixmap & backgroundPixmap () const;

  /** Connection des objets graphiques avec les données de l'application. */
  virtual void linkDatas();

  virtual bool linkDatasShow() const;
  virtual void setLinkDatasShow(const bool val);

signals: // Signals
  /** Emis à chaque modification de valeur d'une des données avec state à \a true
    *  et à chaque validation des valeurs de l'ensemble des données avec state à \a false. */
  void modified(bool state);
  /** Emis à chaque modification de valeur d'une des données. */
  void modifie();
  /** Emis à chaque validation des valeurs des données des widgets enfants. */
  void validate();
  /** Ordonne le rafraîchissement des widgets enfants. */
  void refreshing();
  /** Donne l'état de la vue. */
  void stateChanged(const QString &state);

public slots: // Public slots
  /** Place la boîte de dialogue comme ayant des valeurs de données modifiées. */
  virtual void setModifie();
  /** Place la boîte de dialogue comme ayant des valeurs de données validées. */
  virtual void setValidate();
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(bool val);
  /** Met le widget en mode forçage ou pas suivant \a val. */
  virtual void setForcingMode(bool val);
  /** Met le widget en mode paramétrage ou pas suivant \a val. */
  virtual void setSettingMode(bool val);

  /** Affiche le widget comme ayant au moins une de ses données modifiées. */
  virtual void setModifieView() {}
  /** Affiche le widget comme ayant ses données validées. */
  virtual void setValidateView() {}

  /** Applique les données. */
  virtual void apply();
  /** Met à jour les valeur des données. */
  virtual void update();
  /** Rafraîchit la vue. */
  virtual void refresh();
  /** Charge les valeurs par défaut des données. */
  virtual void designer();
  /** Donne le widget des données à traîter. */
  virtual void setWidget(QWidget *widget);

  /** Applique les modifications et ferme la boîte de dialogue. */
  virtual void accept();

  /**
   * @brief Appel de l'aide contextuelle.
   * Ouvre automatiquement le manuel cydoc sur le signet ayant le même nom que la classe fille.
   */
  virtual void help();

protected: // Protected methods
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
     Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);
  /** Fonction appelée à l'affichage du widget. */
  virtual void showEvent(QShowEvent *e);
  /** Gestion du clavier. */
  virtual void keyPressEvent(QKeyEvent *e);

protected: // Protected attributes
  /** Si ce widget est non-nul, alors la boîte de dialogue ne traîte
    * que les données de ce widget, sinon  elle traîte toutes les
    * données des widgets qu'elle contient. */
  CYWidget *mWidget;

  /** Couleur de fond par défaut. */
  QColor mBackgroundColor;
  /** Couleur du premier plan par défaut. */
  QColor mForegroundColor;
  /** Image de fond par défaut. */
  QPixmap mBackgroundPixmap;
};

#endif
