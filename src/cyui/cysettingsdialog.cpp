//
// C++ Implementation: cysettingsdialog
//
// Description:
//
//
// Author: Géald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cysettingsdialog.h"
#include "cymessagebox.h"

CYSettingsDialog::CYSettingsDialog(QWidget *parent, const QString &name, bool modal, Qt::WindowFlags f)
  : CYDialog(parent, name, f)
{
  setModal(modal);

  mButtonDefaults = 0;
  mButtonApply    = 0;
  mButtonCancel   = 0;
  mListView       = 0;
  mWidgetStack    = 0;
  mTimer->start(250); // ms
}


CYSettingsDialog::~CYSettingsDialog()
{
}

void CYSettingsDialog::init()
{
  mInitTab = false;
  mListView->sortItems( 1, Qt::AscendingOrder );
  mListView->setHeaderHidden(true);
  mListView->hideColumn( 1 );
}

void CYSettingsDialog::changePage( QTreeWidgetItem *item, QTreeWidgetItem *previous )
{
  Q_UNUSED(previous)
  if (isModified())
  {
    QString msg = QString(tr("There are unsaved changes in the active view.\n"
                               "Do you want to apply or discard this changes?"));

    int res = CYMessageBox::warningYesNoCancel(this, msg, tr("Unsaved changes"), tr("&Apply"), tr("&Discard"));

    if (res == CYMessageBox::Yes)
      apply();
    if (res == CYMessageBox::No)
    {
      refresh();
      setValidate();
    }
    else if (res == CYMessageBox::Cancel)
      return;
  }

  mEmitModifie = false;
  bool ok;
  int page = item->text(1).toInt(&ok);
  if (ok)
    changeView(page);
  mEmitModifie = true;
}

void CYSettingsDialog::setModifieView()
{
  if (mEmitModifie && mButtonApply)
    mButtonApply->setEnabled(true);
}

void CYSettingsDialog::setValidateView()
{
  if (mButtonApply)
    mButtonApply->setEnabled(false);
}

void CYSettingsDialog::changeView(int page)
{
  if (mWidgetStack)
  {
    mWidgetStack->setCurrentIndex(page);
  }
  setValidate();
}

void CYSettingsDialog::selectSettings(int page)
{
  if ( mListView )
  {
    changeView(page);
    QTreeWidgetItem *item = mListView->findItems(QString("%1").arg(page), Qt::MatchExactly, 1).first();
    mListView->setCurrentItem(item);
  }
}
