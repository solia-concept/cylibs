/***************************************************************************
                          cycarddo.h  -  description
                             -------------------
    début                  : Thu Feb 13 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCARDDO_H
#define CYCARDDO_H

// CYLIBS
#include "cydo.h"
#include "cycarddig.h"

/** CYCardDO représente les cartes électriques de sorties TOR.
  * @see CYCardDIG, CYCardDI
  * @short Carte d'entrées TOR.
  * @author Gérald LE CLEACH
  */

class CYCardDO : public CYCardDIG
{
  Q_OBJECT
  
public:
  /** Constructeur. */
  CYCardDO(QWidget *parent=0, const QString &name=0, int no=0, Mode m=View);
  /** Destructeur. */
  ~CYCardDO();

  virtual QSize sizeHint() const;
  virtual QSize minimumSizeHint() const;

public slots:
  /** Simulation: permute l'état des leds. */
  virtual void simulation();
  /** Place les données dans le widget.
    * @param db Base de données à traîter. */
  void setData(QHash<int, CYDO*> db);
  /** Lie les données avec les widgets des voies relatives. */
  void linkData();
  /** Met à jour l'état des CheckBox en mode forçage. */
  void refreshForcing();
  /** Ecriture des données de forçage. */
  bool writeForcing();

private:
  /** Initialise le widget selon les paramètres de la carte. */
  virtual void init();
  /** Met à jour les widgets d'E/S. */
  virtual void updateCard();

  QHash<int, CYDO*> datas;
};

#endif
