/***************************************************************************
                          cylineedit.cpp  -  description
                             -------------------
    début                  : lun mar 3 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cylineedit.h"

// QT
#include <QShowEvent>
#include <QFocusEvent>
#include <QMouseEvent>
#include <QMenu>
#include <QFrame>
// CYLIBS
#include "cycore.h"
#include "cyflag.h"
#include "cys32.h"
#include "cystring.h"
#include "cydisplaysimple.h"
#include "cyreadonlyfilter.h"

CYLineEdit::CYLineEdit(QWidget *parent, const QString &name)
  : QLineEdit(parent),
    CYDataWdg(this)
{
  setObjectName(name);
  init();
}

CYLineEdit::~CYLineEdit()
{
}

void CYLineEdit::init()
{
  mWasOk        = false;
  mEnableColor  = Qt::white;
  mDisableColor = Qt::gray;
  mBold         = false;
  int offset = (core) ? core->offsetFontSize() : 0;
#if QT_VERSION == 0x030303 // version 3.03.03
  mFontSize     = 16+offset;
#else
  mFontSize     = 13+offset;
#endif
  initPalettes();
  setType(Free);
  // TOCHECK QT4
//  setFrameShape(QFrame::StyledPanel);

  connect(this, SIGNAL(textChanged(const QString &)), SLOT(setTextChanged(const QString &)));
  connect(this, SIGNAL(returnPressed()), SLOT(setValidate()));
}

void CYLineEdit::initPalettes()
{
  // FREE
  mPalettes.insert(Free, palette());

  // INPUT
  QPalette pal1(palette());

  pal1.setColor(QPalette::Active, QPalette::Text, palette().text().color());
  pal1.setColor(QPalette::Active, QPalette::Base, palette().base().color());
  pal1.setColor(QPalette::Active, QPalette::Window, palette().window().color());

  pal1.setColor(QPalette::Inactive, QPalette::Text, palette().text().color());
  pal1.setColor(QPalette::Inactive, QPalette::Base, palette().base().color());
  pal1.setColor(QPalette::Inactive, QPalette::Window, palette().window().color());

  pal1.setColor(QPalette::Disabled, QPalette::Text, palette().color(QPalette::Disabled, QPalette::Text));
  pal1.setColor(QPalette::Disabled, QPalette::Base, palette().color(QPalette::Disabled, QPalette::Base));
  pal1.setColor(QPalette::Disabled, QPalette::Window, palette().color(QPalette::Disabled, QPalette::Window));

  mPalettes.insert(Input, pal1);

  // PROCESS
  QPalette pal2(palette());

  pal2.setColor(QPalette::Active, QPalette::Text, mEnableColor);
  pal2.setColor(QPalette::Active, QPalette::Base, Qt::black);
  pal2.setColor(QPalette::Active, QPalette::Window, Qt::black);

  pal2.setColor(QPalette::Inactive, QPalette::Text, mEnableColor);
  pal2.setColor(QPalette::Inactive, QPalette::Base, Qt::black);
  pal2.setColor(QPalette::Inactive, QPalette::Window, Qt::black);

  pal2.setColor(QPalette::Disabled, QPalette::Text, mDisableColor);
  pal2.setColor(QPalette::Disabled, QPalette::Base, Qt::black);
  pal2.setColor(QPalette::Disabled, QPalette::Window, Qt::black);

  pal2.setColor( QPalette::Button, QColor(Qt::red));

  mPalettes.insert(Process, pal2);

  // LOCAL
  QPalette pal3(palette());

  pal3.setColor(QPalette::Active, QPalette::Text, palette().windowText().color());
  pal3.setColor(QPalette::Active, QPalette::Base, palette().window().color());
  pal3.setColor(QPalette::Active, QPalette::Window, palette().window().color());

  pal3.setColor(QPalette::Inactive, QPalette::Text, palette().windowText().color());
  pal3.setColor(QPalette::Inactive, QPalette::Base, palette().window().color());
  pal3.setColor(QPalette::Inactive, QPalette::Window, palette().window().color());

  pal3.setColor(QPalette::Disabled, QPalette::Text, palette().color(QPalette::Disabled, QPalette::Foreground));
  pal3.setColor(QPalette::Disabled, QPalette::Base, palette().color(QPalette::Disabled, QPalette::Window));
  pal3.setColor(QPalette::Disabled, QPalette::Window, palette().color(QPalette::Disabled, QPalette::Window));

  mPalettes.insert(Local, pal3);
}

void CYLineEdit::setEnableColor (const QColor &c)
{
  mEnableColor = c;
  initPalettes();
  setType();
}

void CYLineEdit::setDisableColor (const QColor &c)
{
  mDisableColor = c;
  initPalettes();
  setType();
}


bool CYLineEdit::movable() const
{
  return mMovable;
}

void CYLineEdit::setMovable(const bool val)
{
  mMovable = val;
  setEditPositions( mEditPositions );
}

//void CYLineEdit::setDataName(const QByteArray &name)
//{
//  CYDataWdg::setDataName(name);
//}

//QByteArray CYLineEdit::dataName() const
//{
//  return CYDataWdg::dataName();
//}

void CYLineEdit::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYLineEdit::flagName() const
{
  return CYDataWdg::flagName();
}

void CYLineEdit::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYLineEdit::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYLineEdit::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYLineEdit::benchType() const
{
  return CYDataWdg::benchType();
}

int CYLineEdit::genericMarkOffset() const
{
  return mGenericMarkOffset;
}

void CYLineEdit::setGenericMarkOffset(const int val)
{
  mGenericMarkOffset = val;
}

void CYLineEdit::setInverseFlag(bool inverse)
{
  mInverseFlag = inverse;
}

bool CYLineEdit::inverseFlag() const
{
  return mInverseFlag;
}

void CYLineEdit::setInverseHideFlag(bool inverse)
{
  mInverseHideFlag = inverse;
}

bool CYLineEdit::inverseHideFlag() const
{
  return mInverseHideFlag;
}

void CYLineEdit::setPosXName(const QByteArray &name)
{
  CYDataWdg::setPosXName( name );
}

QByteArray CYLineEdit::posXName() const
{
  return CYDataWdg::posXName();
}

void CYLineEdit::setPosYName(const QByteArray &name)
{
  CYDataWdg::setPosYName( name );
}

QByteArray CYLineEdit::posYName() const
{
  return CYDataWdg::posYName();
}

CYLineEdit::Type CYLineEdit::type() const
{
  return mType;
}

void CYLineEdit::setType(const Type type)
{
  mType = type;
  setType();
}

void CYLineEdit::setType()
{
  switch(mType)
  {
    case Free:
    {
      setPalette(mPalettes[Free]);
      break;
    }
    case Input:
    {
      setPalette(mPalettes[Input]);
      setFocusPolicy(Qt::StrongFocus);
      break;
    }
    case Process:
    {
      setPalette(mPalettes[Process]);
      setFocusPolicy(Qt::NoFocus);
      QFont font = QWidget::font();
      font.setPointSize(mFontSize);
      setFont(font);
      mBold = true;
      mReadOnly = true;
      break;
    }
    case Local:
    {
      setPalette(mPalettes[Local]);
      setFocusPolicy(Qt::NoFocus);
      mReadOnly = true;
      break;
    }
  }
  setBold(mBold);
}

QString CYLineEdit::suffix() const
{
  return mSuffix;
}

void CYLineEdit::setSuffix(const QString &suffix)
{
  mSuffix = suffix;

  resetEditBox();
}

QString CYLineEdit::prefix() const
{
  return mPrefix;
}

void CYLineEdit::setPrefix(const QString &prefix)
{
  mPrefix = prefix;

  resetEditBox();
}

void CYLineEdit::setSpecialValueText(const QString& text)
{
  mSpecialValue = mPrefix + text + mSuffix;
  resetEditBox();
};

void CYLineEdit::putSpecialValueText()
{
  if (mData && mData->notSpecialValueText())
    return;
  if (!specialValueText().isEmpty())
    setText(specialValueText());
};

bool CYLineEdit::ctrlFlag()
{
  if (!mCtrlFlag || (mFlag==0))
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;

  setEnabled(res);
  return res;
}

void CYLineEdit::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYLineEdit::setBold(bool state)
{
  mBold = state;
  QFont font = QWidget::font();
  font.setBold(state);
  setFont(font);
}

QSize CYLineEdit::sizeHint() const
{
/*  if ((sizePolicy().horizontalPolicy()==QSizePolicy::Maximum) ||
      (sizePolicy().verticalPolicy()==QSizePolicy::Maximum))
    return maximumSizeHint();
  else */if ((sizePolicy().horizontalPolicy()==QSizePolicy::Minimum) ||
      (sizePolicy().verticalPolicy()==QSizePolicy::Minimum))
    return minimumSizeHint();
  else
    return QLineEdit::sizeHint();
}

QSize CYLineEdit::maximumSizeHint () const
{
  QSize size = QLineEdit::minimumSizeHint();
  QSize maxSize = fontMetrics().size(Qt::TextSingleLine, mSpecialValue);
  if (sizePolicy().horizontalPolicy()==QSizePolicy::Minimum)
  {
    size.setWidth(maxSize.width()+5);
  }
  if (sizePolicy().verticalPolicy()==QSizePolicy::Minimum)
  {
    size.setHeight(maxSize.height()+5);
  }
  return size;
}

QSize CYLineEdit::minimumSizeHint () const
{
  QSize size = QLineEdit::minimumSizeHint();
  QSize minSize = fontMetrics().size(Qt::TextSingleLine, mSpecialValue);
  if (sizePolicy().horizontalPolicy()==QSizePolicy::Minimum)
  {
    size.setWidth(minSize.width()+1);
  }
  if (sizePolicy().verticalPolicy()==QSizePolicy::Minimum)
  {
    size.setHeight(minSize.height()+1);
  }
  return size;
}

bool CYLineEdit::event(QEvent *event)
{
  if (event)
  {
    if (event->type() == QEvent::KeyPress)
    {
      QKeyEvent *k = (QKeyEvent*)event;

      switch (k->key())
      {
        case Qt::Key_Return   :
        case Qt::Key_Enter    :
        {
          if (!readOnly())
            emit applying();
          break;
        }
      }
    }
  }

  return QLineEdit::event(event);
}

void CYLineEdit::changeEvent(QEvent* event)
{
  if (event)
  {
    if (event->type()==QEvent::EnabledChange)
    {
      // cet événement est envoyé, si l'état actif change
      bool state = isEnabled();
      if (!readOnly() || !state)
        mEnabled = state;
      if (!state)
      {
        //if ((mData && mData->isOk()) || mAlwaysOk) a_voir
        //  return;
        putSpecialValueText();
      }
    }
    if (event->type() == QEvent::KeyPress)
    {
      QKeyEvent *k = (QKeyEvent*)event;

      switch (k->key())
      {
        case Qt::Key_Return   :
        case Qt::Key_Enter    :
        {
          emit applying();
          break;
        }
      }
    }
  }

  QLineEdit::changeEvent(event);
}

void CYLineEdit::setEnabled(bool state)
{
  QLineEdit::setEnabled(state);
  mEnabled = state;
  if (state)
    return;
  putSpecialValueText();
}

bool CYLineEdit::readOnly() const
{
  return mReadOnly;
}

void CYLineEdit::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }


  ctrlFlag();
}

bool CYLineEdit::alwaysOk() const
{
  return mAlwaysOk;
}

void CYLineEdit::setAlwaysOk(const bool val)
{
  mAlwaysOk = val;
}

void CYLineEdit::focusInEvent(QFocusEvent *e)
{
  QLineEdit::focusInEvent(e);
  emit focusIn();
}

void CYLineEdit::showEvent(QShowEvent *)
{
  setType();
}

void CYLineEdit::setBackgroundColor(const QColor &color)
{
  QPalette pal = palette();
  mBackgroundColor=color;
  pal.setColor(backgroundRole(), color);
  setPalette(pal);
  setType();
}

void CYLineEdit::setForegroundColor(const QColor &color)
{
  QPalette pal = palette();
  mForegroundColor=color;
  pal.setColor(QPalette::WindowText, color);
  setPalette(pal);
  setType();
}

void CYLineEdit::setPalette(const QPalette &palette)
{
  CYLineEdit::setPalette(palette, true);
}

void CYLineEdit::setPalette(const QPalette &palette, bool type)
{
  QLineEdit::setPalette(palette);
  if (type)
  {
    mPalettes[mType] = palette;
  }
}

int CYLineEdit::fontSize() const
{
  return mFontSize;
}

void CYLineEdit::setFontSize(const int val)
{
  mFontSize = val;
  QFont font = QWidget::font();
  font.setPointSize(mFontSize);
  setFont(font);
  hide();
  show();
}

// TOCHECK QT4
//QMenu * CYLineEdit::createPopupMenu()
//{
//  if ( mDisplaySimple )
//  {
//    QMenu *pm= QLineEdit::createPopupMenu();
//    pm->clear();
//    mDisplaySimple->showPopupMenu(pm, -1);
//    return pm;
//  }
//  return QLineEdit::createPopupMenu();
//}
void CYLineEdit::contextMenuEvent( QContextMenuEvent * event )
{
//  if ( mDisplaySimple )
//  {
//    QMenu *menu = createStandardContextMenu();
//    menu->clear();
//    mDisplaySimple->showPopupMenu(menu, -1);
//    menu->exec(QCursor::pos());
////    menu->exec(event->globalPos());
//    delete menu;
//  }
//  else
    QLineEdit::contextMenuEvent( event );
}

/*! Gestion d'appui de bouton de souris
    \fn CYLineEdit::mousePressEvent ( QMouseEvent * e )
 */
void CYLineEdit::mousePressEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions )
  {
    mXOldPos = x();
    mYOldPos = y();
    mXMousePos = e->x();
    mYMousePos = e->y();
    emit movedInsideParent();
  }
  QLineEdit::mousePressEvent ( e );
}


/*! Gestion de déplacement de bouton de souris
    \fn CYLineEdit::mouseMoveEvent ( QMouseEvent * e )
 */
void CYLineEdit::mouseMoveEvent ( QMouseEvent * e )
{
  if ( movable() && mEditPositions && (e->buttons()==Qt::LeftButton) )
  {
    int ex = e->x();
    int ey = e->y();
    int x = mXOldPos + ( ex - mXMousePos );
    int y = mYOldPos + ( ey - mYMousePos );
    moveInsideParent( x, y );
    raise();
  }
  QLineEdit::mouseMoveEvent( e );
}


/*! Déplacement à l'intérieur du parent.
    \fn CYLineEdit::moveInsideParent(int x, int y)
 */
void CYLineEdit::moveInsideParent(int x, int y)
{
  QWidget * p = (QWidget *)parent();
  if ( !p->rect().contains(x, p->rect().y()) )
  {
    if ( x < p->rect().left() )
      x = p->rect().left();            // sort à gauche
    else
      x = p->rect().right()-width();   // sort à droite
  }
  else if ( !p->rect().contains(x+width(), p->rect().y()) )
  {
    x = p->rect().right()-width();     // sort en partie à droite
  }

  if ( !p->rect().contains(p->rect().x(), y) )
  {
    if ( y < p->rect().top() )
      y = p->rect().top();             // sort en haut
    else
      y = p->rect().bottom()-height(); // sort en bas
  }
  else if ( !p->rect().contains(p->rect().x(), y+height()) )
  {
    y = p->rect().bottom()-height();   // sort en partie en bas
  }

  move(x, y);
  mXOldPos = x;
  mYOldPos = y;

  emit modifie();
}


/*! Active/désactive le déplacement
    \fn CYLineEdit::setEditPositions( bool val )
 */
void CYLineEdit::setEditPositions( bool val )
{
  if ( !movable() )
    return;

  mEditPositions = val;
  refreshPosition();

  if ( mEditPositions )
    setCursor( Qt::PointingHandCursor );
  else
    unsetCursor();
}


void CYLineEdit::refresh()
{
  refreshPosition();
}


void CYLineEdit::update()
{
  updatePosition();
}


/*! Rafraîchit la position de l'objet en fonction des données positions.
    \fn CYDataWdg::refreshPosition()
 */
void CYLineEdit::refreshPosition()
{
  if ( !movable() )
    return;

  if ( ( mPosX && mPosY ) && ( ( mPosX->val()!=x() ) || ( mPosY->val()!=y() ) ) )
  {
    if ( (mPosX->val()>=0) && (mPosY->val()>=0) )
      move( mPosX->val(), mPosY->val() );
    else if ( mPosX->val()>=0 )
      move( mPosX->val(), y()          );
    else if ( mPosY->val()>=0 )
      move( x()         , mPosY->val() );
  }
  else if ( ( mPosX ) && ( mPosX->val()!=x() ) && ( mPosX->val()>=0 ) )
  {
    move( mPosX->val(), y()          );
  }
  else if ( ( mPosY ) && ( mPosY->val()!=y() ) && ( mPosY->val()>=0 ) )
    move( x()         , mPosY->val() );
}


/*! Met à jour des données positions en fonction de la position de l'objet.
    \fn CYDataWdg::updatePosition()
 */
void CYLineEdit::updatePosition()
{
  if ( !mEditPositions )
    return;

  if ( mPosX )
    mPosX->setVal( x() );

  if ( mPosY )
    mPosY->setVal( y() );
}


QPalette CYLineEdit::palette(Type type)
{
  return mPalettes[type];
}

void CYLineEdit::setPalette(QPalette palette, Type type)
{
  mPalettes.insert(type, palette);
}

/*! Active / désactive temporairement le widget.
    Inhibe ctrlFlag jusqu'au prochain linkData pour
    une fenêtre de paramétrage avec rafraichissement.
    \fn CYLineEdit::setEnabledTmp(bool val)
 */
void CYLineEdit::setEnabledTmp(bool val)
{
  mCtrlFlag=false;
  setEnabled(val);
}


const QColor & CYLineEdit::backgroundColor () const
{
  return mBackgroundColor;
}

const QColor & CYLineEdit::foregroundColor () const
{
  return mForegroundColor;
}

