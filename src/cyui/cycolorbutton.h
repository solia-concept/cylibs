//
// C++ Interface: cycolorbutton
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYCOLORBUTTON_H
#define CYCOLORBUTTON_H

// QT
#include <QPushButton>
// CYLIBS
#include "cydatawdg.h"

class CYColorButtonPrivate;

/**
@short Un bouton poussoir affichant ou autorisant la sélection d'une donnée couleur CYColor.

@author Gérald LE CLEACH
*/

class CYColorButton : public QPushButton, public CYDataWdg
{
  Q_OBJECT
  Q_PROPERTY( QColor color READ color WRITE setColor )
  Q_PROPERTY( QColor defaultColor READ defaultColor WRITE setDefaultColor )
  Q_PROPERTY(bool autoRefresh READ autoRefresh WRITE setAutoRefresh)
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public: 
  /**
   * Creates a color button.
   */
  CYColorButton( QWidget *parent, const QString &name = 0L );

  /**
   * Creates a color button with an initial color @p c.
   */
  CYColorButton( const QColor &c, QWidget *parent, const QString &name = 0L );
  /// @since 3.1
  CYColorButton( const QColor &c, const QColor &defaultColor, QWidget *parent,
                 const QString &name=0L );

  virtual ~CYColorButton();

  /**
   * Returns the currently chosen color.
   */
  QColor color() const
  { return col; }

  /**
   * Sets the current color to @p c.
   */
  void setColor( const QColor &c );

  /**
   * Returns the default color or an invalid color
   * if no default color is set.
   * @since 3.4
   */
  QColor defaultColor() const;

  /**
   * Sets the default color to @p c.
   * @since 3.4
   */
  void setDefaultColor( const QColor &c );

  QSize sizeHint() const;

  /** @return \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  bool autoRefresh() const { return mAutoRefresh; }
  /** Saisir \a true pour que le widget soit rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  void setAutoRefresh(const bool val) { mAutoRefresh = val; }

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return la valeur courrante. */
  virtual QColor value() const;
  /** Fixe la valeur courante à \a value. */
  virtual void setValue(QColor value);

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

  /** Saisie la palettes de couleurs. */
  virtual void setPalette(const QPalette &palette);

signals:
  /**
   * Emitted when the color of the widget
   * is changed, either with setColor() or via user selection.
   */
  void changed( const QColor &newColor );
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis lorsque le widget reçoit le focus. */
  void focusIn();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();

public slots: // Public slots
  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();

  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

protected slots:
  void chooseColor();
  void colorChanged(const QColor &newColor);

protected: // Protected methods
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
       Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);

  /** Gestion de la réception du focus. */
  void focusInEvent(QFocusEvent *e);
  virtual void paintEvent(QPaintEvent *event);
  virtual void drawButtonLabel( QPainter *p );

private:
  void init();

protected: // Protected attributes
  QColor col;
  /** Valeur courante. */
  QColor mValue;

private:
  class CYColorButtonPrivate;
  CYColorButtonPrivate *d;
};

#endif
