//
// C++ Implementation: cyimagelistdialog
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyimagelistdialog.h"

CYImageListDialog::CYImageListDialog(QWidget *parent, const QString &name, bool modal, Qt::WindowFlags f)
  : CYDialog(parent, name, f)
{
  setModal(modal);

  mButtonApply    = 0;
  mButtonCancel   = 0;
  mListView       = 0;
  mTimer->start(250); // ms
}


CYImageListDialog::~CYImageListDialog()
{
}

void CYImageListDialog::init()
{
  mListView->sortItems( 1, Qt::AscendingOrder );
  mListView->setHeaderHidden(true);
}


void CYImageListDialog::setModifieView()
{
  if (mEmitModifie && mButtonApply)
    mButtonApply->setEnabled(true);
}

void CYImageListDialog::setValidateView()
{
  if (mButtonApply)
    mButtonApply->setEnabled(false);
}


/*! Ajoute une image par sélection d'un fichier
    \fn CYImageListDialog::add()
 */
void CYImageListDialog::add()
{
//   QString startWith = getenv( "HOME" );
//   QString filter = "*.csv *.txt";
//
//   QString fileName = KFileDialog::getSaveFileName( startWith, filter, 0, tr("Choose a filename to save under for this export file."));
//
//   if (fileName.isEmpty())
//     return;
}
