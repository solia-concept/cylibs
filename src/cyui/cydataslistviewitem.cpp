//
// C++ Implementation: cydataslistviewitem
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <qpainter.h>
// CYLIBS
#include "cydataslistviewitem.h"
#include "cydisplayitem.h"
#include "cypen.h"
// #include "cyf64.h"

CYDatasListViewItem::CYDatasListViewItem(CYDatasListView * parent, CYDisplayItem *di)
 : QTreeWidgetItem( parent ),
   mDisplayItem (di)
{
  mListView = parent;

  mDB  = new CYDB( mListView, "db");
  mPen = new CYPen( mDB, QString("PEN_%1").arg(di->data->objectName()), di->data->label(), di->pen );
/*  mCoef= new CYF64( mDB, QString("COEF_%1").arg(di->data->dataName()), new f64, di->coefficient );
  mCoef->setNbDec(10);
  mCoef->setLabel(tr("Coefficient of display"));*/
}


CYDatasListViewItem::~CYDatasListViewItem()
{
}


// TODO QT5
///*!
//    \fn CYDatasListViewItem::paintCell ( QPainter * p, const QColorGroup & cg, int column, int width, int align )
// */
//void CYDatasListViewItem::paintCell ( QPainter * p, const QColorGroup & cg, int column, int width, int align )
//{
//  QTreeWidgetItem::paintCell ( p, cg, column, width, align );
//  if ( mListView->withPen() && ( mListView->penColumn() == column ) )
//  {
//    p->setPen( mPen->pen() );
//    QRect rect = QRect(0, 0, width, height());
//    p->drawLine(rect.left(), rect.center().y(), rect.right(), rect.center().y());
//  }
//}


/*! @return l'ensemble de données définissant le crayon.
    \fn CYDatasListViewItem::cypen()
 */
CYPen *  CYDatasListViewItem::cypen()
{
  return mPen;
}

/*! @return l'ensemble la donnée d'échelle.
    \fn CYDatasListViewItem::cycoef()
 */
// CYF64 *  CYDatasListViewItem::cycoef()
// {
//   return mCoef;
// }

/*! @return la donnée de l'afficheur.
    \fn CYDatasListViewItem::displayItem()
 */
CYDisplayItem * CYDatasListViewItem::displayItem()
{
  return mDisplayItem;
}
