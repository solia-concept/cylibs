//
// C++ Implementation: cylcdnumber
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cylcdnumber.h"

CYLCDNumber::CYLCDNumber(QWidget *parent, const QString &name)
  : QLCDNumber(parent)
{
  setObjectName(name);
  setAutoFillBackground(true);
}

CYLCDNumber::CYLCDNumber(uint numDigits, QWidget *parent, const QString &name)
  : QLCDNumber(numDigits, parent)
{
  setObjectName(name);
  setAutoFillBackground(true);
}


CYLCDNumber::~CYLCDNumber()
{
}




/*! Change la couleur des digits
    \fn CYLCDNumber::setDigitColor(const QColor &col)
 */
void CYLCDNumber::setDigitColor(const QColor &col)
{
  QPalette p = palette();
  p.setColor(QPalette::Foreground, col);
  setPalette(p);
}


/*! Change la couleur de fond
    \fn CYLCDNumber::setBackgroundColor(const QColor &col)
 */
void CYLCDNumber::setBackgroundColor(const QColor &color)
{
  QPalette p = palette();
  p.setColor(backgroundRole(), color);
  p.setColor(QPalette::Light, color);
  p.setColor(QPalette::Dark, color);
  setPalette(p);
}
