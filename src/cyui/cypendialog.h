//
// C++ Interface: cypendialog
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYPENDIALOG_H
#define CYPENDIALOG_H

// CYLIBS
#include "cydialog.h"

class CYPen;

namespace Ui {
		class CYPenDialog;
}


/**
@short Boîte de configuration d'un crayon.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYPenDialog : public CYDialog
{
public:
    CYPenDialog(QWidget *parent=0, const QString &name="penDialog");

    ~CYPenDialog();
    void setPen( CYPen *pen );

private:
    Ui::CYPenDialog *ui;
};

#endif
