/***************************************************************************
                          cycombobox.h  -  description
                             -------------------
    début                  : lun avr 28 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCOMBOBOX_H
#define CYCOMBOBOX_H

// QT
#include <qcombobox.h>
//Added by qt3to4:
#include <QString>
#include <QEvent>
#include <QKeyEvent>
#include <QFocusEvent>
// CYLIBS
#include "cydatawdg.h"

/** CYComboBox est une classe de base pour réaliser des combos relatives
  * à des données.
  * @short ComboBox d'une donnée.
  * @author Gérald LE CLEACH
  */

class CYComboBox : public QComboBox, public CYDataWdg
{
  Q_OBJECT
  Q_PROPERTY(int offset READ offset WRITE setOffset)
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool inputHelp READ inputHelp WRITE setInputHelp)
  Q_PROPERTY(int movieStepFocusIn READ movieStepFocusIn WRITE setMovieStepFocusIn)
  Q_PROPERTY(int movieStepFocusOut READ movieStepFocusOut WRITE setMovieStepFocusOut)
  Q_PROPERTY(bool autoRefresh READ autoRefresh WRITE setAutoRefresh)
  Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
  Q_PROPERTY(QColor foregroundColor READ foregroundColor WRITE setForegroundColor)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:
  /** Construit une combo. */
  CYComboBox(QWidget *parent=0, const QString &name="button");
  /** Construit une combo éditable.
    * @param rw     Si \a true, l'entrée peut directement être éditée.
    * @param parent Widget parent.
    * @param name   Nom du widget. */
  CYComboBox(bool rw, QWidget *parent=0, const QString &name=0);

  virtual ~CYComboBox() {}

  /** @return l'offset qui est ajouté à l'index de l'élément courant pour calculé la valeur. Il est ainsi possible d'avoir une liste ne commençant pas par 0. */
  virtual int offset() const { return mOffset; }
  /** Saisie l'offset qui est ajouté à l'index de l'élément courant pour calculé la valeur. Il est ainsi possible d'avoir une liste ne commençant pas par 0. */
  virtual void setOffset(const int val) { mOffset = val; }

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  virtual bool autoRefresh() const { return mAutoRefresh; }
  /** Saisir \a true pour que le widget soit rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  virtual void setAutoRefresh(const bool val) { mAutoRefresh = val; }

  /** Utilise l'aide ou pas de saisie (valeur constructeur). */
  void setInputHelp(bool input=false) { mInputHelp = input; }
  /** @return si \a true utilise l'aide de saisie (valeur constructeur) sinon l'aide d'affichage. */
  bool inputHelp() const { return mInputHelp; }

  /** @return le numéro du pas envoyé à une animation lors de la réception du focus. */
  int movieStepFocusIn() const { return mMovieStepFocusIn; }
  /** Saisie le numéro du pas envoyé à une animation lors de la réception du focus. */
  void setMovieStepFocusIn(const int step) { mMovieStepFocusIn = step; }

  /** @return le numéro du pas envoyé à une animation lors de la perte du focus. */
  int movieStepFocusOut() const { return mMovieStepFocusOut; }
  /** Saisie le numéro du pas envoyé à une animation lors de la perte du focus. */
  void setMovieStepFocusOut(const int step) { mMovieStepFocusOut = step; }

  /** Saisie la couleur de fond. */
  virtual void setBackgroundColor(const QColor &color);
  /** @return la couleur de fond du widget */
  virtual const QColor & backgroundColor () const { return mBackgroundColor; }
  /** Saisie la couleur du premier plan. */
  virtual void setForegroundColor(const QColor &color);
  /** @return la couleur de devant du widget */
  virtual const QColor & foregroundColor () const { return mForegroundColor; }

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** Saisie la palettes de couleurs. */
  virtual void setPalette(const QPalette &palette);

  /** @return la valeur courrante. */
  virtual int value() const;
  /** @return la valeur courrante. */
  virtual QString valueTxt() const;
  /** Fixe la valeur courante à \a value. */
  virtual void setValue(QString value);
  /** Changement de l'élément courant.
    * @param index Index de nouvel element courant. */
  virtual void setCurrentIndex( int index );

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

signals:
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();
  /** Emis lorsque la souris entre dans le widget. */
  void enteringEvent();
  /** Emis lorsque la souris sort du widget. */
  void leavingEvent();
  /** Emis lorsque le widget reçoit le focus. */
  void focusIn();
  /** Emis lorsque le widget perd le focus. */
  void focusOut();
  /** Emet le numéro du pas envoyé à une animation lors de la réception du focus. */
  void focusIn(int movieStep);
  /** Emet le numéro du pas envoyé à une animation lors de la perte du focus. */
  void focusOut(int movieStep);
  /** Emis lors d'un changement de l'élément courant.
    * @param index Index de nouvel element courant. */
  void currentItemChanged( int index );
  /** Emis à chaque fois que la valeur change. */
  void valueChanged(int value);
  /** Emis suite au signale activated(int index) en prenant en compte l'offset. */
  void activatedWithOffset(int index);

public slots: // Public slots
  /** Fixe la valeur courante à \a value. */
  virtual void setValue(int value); // v5.12.09 passage en slot
  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

protected slots: // Protected slots
  virtual void setTextChanged(const QString & text);
  virtual void setActivated(int index);
  virtual void setActivated(const QString &string);

protected: // Protected methods
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
     Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);

  /** Gestion de la réception du focus. */
  virtual void focusInEvent(QFocusEvent *e);
  /** Gestion de la perte du focus. */
  virtual void focusOutEvent(QFocusEvent *e);
  /** Gestion d'entrée dans le widget de la souris. */
  virtual void enterEvent ( QEvent * );
  /** Gestion de sortie du widget de la souris. */
  virtual void leaveEvent ( QEvent * );
  /** Gestion du clavier. */
  virtual void keyPressEvent(QKeyEvent * e);

private: // Private methods
  void init();

protected: // Protected attributes
  /** Si \a true utilise l'aide de saisie (valeur constructeur) sinon l'aide d'affichage. */
  bool mInputHelp;
  int mOffset;
  QMap<int, int> mCorrelationMap;
};

#endif
