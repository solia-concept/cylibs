/***************************************************************************
                          cypushbutton.cpp  -  description
                             -------------------
    begin                : mar oct 7 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cypushbutton.h"

// CYLIBS
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cyword.h"
#include "cybool.h"
#include "cydialog.h"
#include "cywidget.h"
#include "cyframe.h"
#include "cycore.h"
#include "cyreadonlyfilter.h"

CYPushButton::CYPushButton(QWidget *parent, const QString &name)
  : QPushButton(parent),
    CYDataWdg(this)
{
  setObjectName(name);
  init();
}

void CYPushButton::init()
{
  mProtectedAccess = false;
  mDisableSend = false;
  mAutoRefresh = false;

  mWriteData = true;
  mBackgroundColor=palette().color(backgroundRole());
  mForegroundColor=palette().color(foregroundRole());

  initPalette();

  connect(this, SIGNAL(toggled(bool)), SLOT(changeDataState(bool)));
  connect(this, SIGNAL(clicked()), SLOT(send()));
  connect(core, SIGNAL(accessChanged()), SLOT(ctrlFlag()));
}

void CYPushButton::initPalette()
{
  mPalette = palette();
}

void CYPushButton::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYPushButton::dataName() const
{
  return CYDataWdg::dataName();
}

void CYPushButton::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYPushButton::flagName() const
{
  return CYDataWdg::flagName();
}

void CYPushButton::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYPushButton::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYPushButton::setInverseFlag(bool inverse)
{
  mInverseFlag = inverse;
}

bool CYPushButton::inverseFlag() const
{
  return mInverseFlag;
}

void CYPushButton::setInverseHideFlag(bool inverse)
{
  mInverseHideFlag = inverse;
}

bool CYPushButton::inverseHideFlag() const
{
  return mInverseHideFlag;
}

void CYPushButton::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYPushButton::benchType() const
{
  return CYDataWdg::benchType();
}

bool CYPushButton::value() const
{
  return mValue;
}

void CYPushButton::setValue(bool value)
{
  mValue = value;
  setText(text());
}

QString CYPushButton::textOn() const
{
  return mTextOn;
}

void CYPushButton::setTextOn(const QString &text)
{
  mTextOn = text;
}

QString CYPushButton::textOff() const
{
  return mTextOff;
}

void CYPushButton::setTextOff(const QString &text)
{
  mTextOff = text;
}

bool CYPushButton::ctrlFlag()
{
  if (!((QWidget *)parent())->isEnabled() && !core)
    return true;

  bool force=false;
  bool enable1=true;
  if (protectedAccess())
  {
    force=true;
    enable1 = (core && core->isProtectedAccess()) ? false : true;
  }

  bool enable2=true;
  if (mFlag)
  {
    force=true;
    if (core && core->simulation())
      enable2=true;
    else
    {
      if (mFlag->val() && mInverseFlag)
        enable2 = false;
      else if (!mFlag->val() && !mInverseFlag)
        enable2 = false;
    }
  }

  if (!force)
    return isEnabled();

  bool res = (enable1 && enable2) ? true : false;
  setEnabled(res);
  return res;
}

void CYPushButton::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;
  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
		hide();
  else
		show();
}

void CYPushButton::linkData()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
  refresh();
}

void CYPushButton::refresh()
{
  ctrlHideFlag();
  ctrlFlag();

  if (!hasData())
    return;

//   if (!readOnly())
//     return;

  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (isCheckable())
   return;

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      if ((bool)data->val())
                        setState( CYPushButton::On );
                      else
                        setState( CYPushButton::Off );
                      setValue((bool)data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYPushButton::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYPushButton::update()
{
  if (!hasData())
    return;

  if (!isVisible() && mUpdateIfVisible)
    return;

  if (mData==0)
    return;

  if (isHidden())
    return;

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      data->setVal(value());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYPushButton::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }


  ctrlFlag();
}

void CYPushButton::changeEvent(QEvent* event)
{
  if (event && (event->type()==QEvent::EnabledChange))
  {
    // cet événement est envoyé, si l'état actif change
    bool state = isEnabled();
    if (!readOnly() || !state)
      mEnabled = state;
  }
  QPushButton::changeEvent(event);
}

void CYPushButton::setText( const QString & txt)
{
  if (!mTextOn.isEmpty() && !mTextOff.isEmpty())
  {
    if (isChecked())
      QPushButton::setText(mTextOn);
    else
      QPushButton::setText(mTextOff);
  }
  else
    QPushButton::setText(txt);
}

void CYPushButton::changeDataState(bool state)
{
  setValue(state);
  update();

  if (isVisible())
    emit modifie();
}

void CYPushButton::focusInEvent(QFocusEvent *e)
{
  QPushButton::focusInEvent(e);
  emit focusIn();
}

void CYPushButton::send()
{
  if (mDisableSend || !mData)
    return;

  if ( (mData->type()) && ( (mData->mode() == Cy::Monostable) || (mData->mode() == Cy::Bistable) ) )
  {
    if (mData->mode() == Cy::Monostable)
    {
      setValue(true);
      update();
    }
    emit applying();
    if (mData->mode() == Cy::Monostable)
    {
      setValue(false);
      update();
    }
  }
}


/*! Saisie l'état du bouton
    \fn CYPushButton::setState ( ToggleState s )
 */
void CYPushButton::setState ( ToggleState s )
{
  if ( isCheckable() )
  {
    setChecked( s );
  }
}

void CYPushButton::setBackgroundColor ( const QColor & color )
{
  mBackgroundColor=color;
  setAutoFillBackground(true);
  QPalette pal = palette();
  pal.setColor(backgroundRole(), mBackgroundColor);
  setPalette(pal);
  initPalette();
}

const QColor & CYPushButton::backgroundColor () const
{
  return mBackgroundColor;
}

void CYPushButton::setForegroundColor ( const QColor & color )
{
  mForegroundColor=color;
  QPalette pal = palette();
  pal.setColor(QPalette::WindowText, mForegroundColor);
  setPalette(pal);
  initPalette();
}

const QColor & CYPushButton::foregroundColor () const
{
  return mForegroundColor;
}

void CYPushButton::setBackgroundPixmap( const QPixmap & pixmap )
{
  if (pixmap.isNull())
    return;

  mBackgroundPixmap=pixmap;
  QIcon icon(pixmap);
  setIcon(icon);
  setIconSize(pixmap.size());
}

const QPixmap & CYPushButton::backgroundPixmap () const
{
  return mBackgroundPixmap;
}

void CYPushButton::setProtectedAccess(const bool val)
{
  mProtectedAccess = val;
}

bool CYPushButton::protectedAccess() const
{
  return mProtectedAccess;
}
