/***************************************************************************
                          cydatasbrowser.h  -  description
                             -------------------
    begin                : ven mai 14 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDATASBROWSER_H
#define CYDATASBROWSER_H

// QT
#include <QFrame>
#include <QComboBox>
#include "cytextinput.h"

class CYCore;
class CYNetHost;
class CYNetLink;
class CYDB;
class CYDatasBrowserList;

/** @short Explorateur de données.
  * @author Gérald LE CLEACH
  */
class CYDatasBrowser : public QFrame
{
  Q_OBJECT

public:
  /** Filtre sur le type de données affichées. */
  enum Filter
  {
    /** Affiche les données utilisateur. */
    User,
    /** Affiche les données système. */
    Sys,
    /** Affiche toutes les données. */
    All,
  };

  /** Constructeur. */
  CYDatasBrowser(QWidget *parent, const QString &name=0);
  /** Destructeur. */
  ~CYDatasBrowser();

  /** Initialise l'arborescence par rapport à la connexion réseau \a link. */
  void init(CYNetHost *host, CYNetLink *link, bool razHost=false);

public slots: // Public slots
    /** Efface la liste de données. */
    void clear();
    void enabelFilterBox(bool val);

protected: // Protected attributes
  /** Liste graphiques des données. */
  CYDatasBrowserList *mList;
  /** Boîte de sélection du type de données. */
  QComboBox *mFilterBox;
  /** Boîte de recherche de données. */
  CYTextInput *mSearchBox;
};

#endif
