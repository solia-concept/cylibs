//
// C++ Interface: cysettingsdialog
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSETTINGSDIALOG_H
#define CYSETTINGSDIALOG_H

// QT
#include <QTreeWidget>
#include <QStackedWidget>
#include <QTabWidget>
#include <QPushButton>
// CYLIBS
#include "cydialog.h"

/**
@short Boîte de dialogue de paramétrage.

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYSettingsDialog : public CYDialog
{
  Q_OBJECT

public:
    CYSettingsDialog(QWidget *parent=0, const QString &name="cySettingsDialog", bool modal=false, Qt::WindowFlags f=Qt::WindowFlags());

    ~CYSettingsDialog();

  /** Sélectionne le paramètrage à afficher. */
  virtual void selectSettings(int page);

public slots:
  virtual void changePage( QTreeWidgetItem *item, QTreeWidgetItem *previous );
  virtual void changeView(int page);
  virtual void setModifieView();
  virtual void setValidateView();
protected:
  QPushButton    * mButtonDefaults;
  QPushButton    * mButtonApply;
  QPushButton    * mButtonCancel;
  QTreeWidget    * mListView;
  QStackedWidget * mWidgetStack;

protected: // Protected methods
  virtual void init();

private:
  bool mInitTab;

};
#endif
