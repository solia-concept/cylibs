/***************************************************************************
                          cypushbutton.h  -  description
                             -------------------
    begin                : mar oct 7 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYPUSHBUTTON_H
#define CYPUSHBUTTON_H

// QT
#include <qpushbutton.h>
//Added by qt3to4:
#include <QString>
#include <QFocusEvent>
// CYLIBS
#include "cydatawdg.h"

/**
  * @short Bouton pour donnée agissant comme une commande.
  * @author Gérald LE CLEACH
  */

class CYPushButton : public QPushButton, public CYDataWdg
{
  Q_OBJECT
  Q_PROPERTY(bool autoRefresh READ autoRefresh WRITE setAutoRefresh)
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(QString textOn READ textOn WRITE setTextOn)
  Q_PROPERTY(QString textOff READ textOff WRITE setTextOff)
  Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
  Q_PROPERTY(QColor foregroundColor READ foregroundColor WRITE setForegroundColor)
  Q_PROPERTY(QPixmap backgroundPixmap READ backgroundPixmap WRITE setBackgroundPixmap)
  Q_PROPERTY(bool protectedAccess READ protectedAccess WRITE setProtectedAccess)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)
  Q_PROPERTY(bool disableSend READ disableSend WRITE setDisableSend)

public:
  /** Etats du bouton */
  enum ToggleState
  {
    Off,
    NoChange,
    On,
  };

  enum GroupSection
  {
    /** Pas d'affichage du groupe. */
    No,
    /** Affichage du dernier sous-groupe. */
    lastGroup,
    /** Affichage des sous-groupes. */
    underGroup,
    /** Affichage du groupe entier. */
    wholeGroup
  };

  CYPushButton(QWidget *parent=0, const QString &name=0);
  virtual ~CYPushButton() {}

  /** @return \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  bool autoRefresh() const { return mAutoRefresh; }
  /** Saisir \a true pour que le widget soit rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  void setAutoRefresh(const bool val) { mAutoRefresh = val; }

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return la valeur courrante. */
  virtual bool value() const;
  /** Fixe la valeur courante à \a value. */
  virtual void setValue(bool value);

  /** @return le texte de l'état est ON. */
  virtual QString textOn() const;
  /** Saisie le texte de l'état ON. */
  virtual void setTextOn(const QString &text);

  /** @return le texte de l'état est OFF. */
  virtual QString textOff() const;
  /** Saisie le texte de l'état OFF. */
  virtual void setTextOff(const QString &text);

  /** Saisie la couleur de fond du widget. */
  virtual void setBackgroundColor ( const QColor & );
  /** @return la couleur de fond du widget */
  virtual const QColor & backgroundColor () const;

  /** Saisie la couleur du premier plan du widget. */
  virtual void setForegroundColor ( const QColor & );
  /** @return la couleur du premier plan du widget */
  virtual const QColor & foregroundColor () const;

  /** Saisie l'image de fond. */
  virtual void setBackgroundPixmap ( const QPixmap & );
  /** @return la couleur du premier plan du widget */
  virtual const QPixmap & backgroundPixmap () const;

  /** Saisir \a true pour l'inhiber en cas d'accès protégé*/
  void setProtectedAccess(const bool val);
  /** @return \a true s'il est inhibé en accès protégé */
  bool protectedAccess() const;

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /**
   * Saisir \a true pour interdir l'envoi de la commande.
   * Permet par exemple dans un slot de réaliser d'autres tâches avant l'envoi de la commande tout en affichant son aide en bulle relative.
  */
  inline void setDisableSend(const bool val) { mDisableSend = val; }
  /** @return \a true l'envoi de la commande est interdite. */
  inline bool disableSend() const  { return mDisableSend; }

signals:
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis lorsque le widget reçoit le focus. */
  void focusIn();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();

public slots: // Public slots
  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);
  /** Sets the text shown on the button. See the "text", "textOn" and "textOff" properties for details. */
  virtual void setText( const QString & txt);

  /** Change l'état de la donnée. */
  void changeDataState(bool);
  /** Envoie la commande. */
  void send();

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

protected: // Protected methods
  /** Gestion de la réception du focus. */
  void focusInEvent(QFocusEvent *e);
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
     Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);

  virtual void setState ( ToggleState s );
  /** Initialise la palette de couleurs. */
  virtual void initPalette();

private:
  void init();

protected: // Protected attributes
  /** Valeur courante. */
  bool mValue;

  /** Texte de l'état On. */
  QString mTextOn;
  /** Texte de l'état OFF. */
  QString mTextOff;

  /** Protection en accès protégé */
  bool mProtectedAccess;
  /** Désactivation de l'envoi de la commande. */
  bool mDisableSend;
};

#endif
