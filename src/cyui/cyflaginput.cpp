/***************************************************************************
                          cyflaginput.cpp  -  description
                             -------------------
    début                  : jeu mai 15 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyflaginput.h"

// QT
#include <QStyle>
// CYLIBS
#include "cyflag.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyword.h"
#include "cybool.h"
#include "cydialog.h"
#include "cywidget.h"
#include "cyframe.h"
#include "cydo.h"
#include "cycore.h"

CYFlagInput::CYFlagInput(QWidget *parent, const QString &name)
  : CYComboBox(parent, name)
{
  mEnableString = true;
  setStringFalse(tr("No"));
  setStringTrue(tr("Yes"));

  mEnablePixmap = true;

  QString path = QString("%1/cylibs/pics/").arg(qApp->applicationDirPath()).toUtf8();
  mPixmapFileFalse="";
  mPixmapFileTrue="";

  setCurrentIndex(0);
}

CYFlagInput::~CYFlagInput()
{
}

int CYFlagInput::value() const
{
  if (currentIndex()==0)
    return 0;
  else
    return 1;
}

void CYFlagInput::setValue(int value)
{
  if (value==0)
    setCurrentIndex(0);
  else
    setCurrentIndex(1);
}

void CYFlagInput::setStringTrue(const QString string)
{
  mStringtrue = string;
  updatePixmapAndString();
}

void CYFlagInput::setStringFalse(const QString string)
{
  mStringfalse = string;
  updatePixmapAndString();
}

void CYFlagInput::setEnableString(const bool state)
{
  mEnableString = state;
  updatePixmapAndString();
}

void CYFlagInput::setPixmapFileTrue(const QByteArray &file)
{
  mPixmapFileTrue = findResource(QString(file));
  updatePixmapAndString();
}

void CYFlagInput::setPixmapFileFalse(const QByteArray &file)
{
  mPixmapFileFalse = findResource(QString(file));
  updatePixmapAndString();
}

void CYFlagInput::setEnablePixmap(const bool state)
{
  mEnablePixmap = state;
  updatePixmapAndString();
}

void CYFlagInput::updatePixmapAndString()
{
  clear();
  QIcon icon_true = (!mPixmapFileTrue.isEmpty()) ?
        QIcon(QString(mPixmapFileTrue)) :
        QIcon(":/icons/cydialog-ok-apply");

  QIcon icon_false = (!mPixmapFileFalse.isEmpty()) ?
        QIcon(QString(mPixmapFileFalse)) :
        QIcon(":/icons/cyedit-delete");

  if (mEnablePixmap && mEnableString)
  {
    insertItem(0, icon_false, mStringfalse);
    insertItem(1, icon_true , mStringtrue);
  }
  else if (mEnableString)
  {
    insertItem(0, mStringfalse);
    insertItem(1, mStringtrue);
  }
  else if (mEnablePixmap)
  {
    insertItem(0, icon_false, "");
    insertItem(1, icon_true , "");
  }
}

void CYFlagInput::linkData()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }


  mEmitModifie = false;

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setStringTrue(data->choicetrue());
                      setStringFalse(data->choicefalse());
                      setValue((int)data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  if (mInputHelp)
  {
    this->setToolTip(QString(mData->inputHelp()+"<br>"+mData->infoCY()));
  }
  else
  {
    this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));
  }

  mEmitModifie = true;
}

void CYFlagInput::refresh()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (!readOnly())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue((int)data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYFlagInput::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }


  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((int)data->def());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue((int)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue((int)data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYFlagInput::update()
{
  if (!hasData())
    return;

  if (!isVisible() && mUpdateIfVisible)
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (!isEnabled())
    return;

  if (isHidden())
    return;

  mEmitModifie = false;

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      data->setVal(value());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
  mEmitModifie = true;
}

void CYFlagInput::setActivated(int index)
{
  setState(index);
  CYComboBox::setActivated(index);
}

void CYFlagInput::setActivated(const QString &txt)
{
  setState(currentIndex());
  CYComboBox::setActivated(txt);
}

void CYFlagInput::setState(int index)
{
  bool val=true;
  if (index==0)
    val=false;
  if (mData)
    mData->setTmp(val);
  emit state(val);
}

void CYFlagInput::keyPressEvent(QKeyEvent * e)
{
  switch (e->key())
  {
    case Qt::Key_0  : setValue(0); break;
    case Qt::Key_1  : setValue(1); break;
  }
  if ((mEmitModifie) && isVisible())
    emit modifie();
  CYComboBox::keyPressEvent(e);
}

void CYFlagInput::setCurrentDataName(int index)
{
  CYDataWdg::setCurrentDataName(index);
  emit currentDataChanged();
  if ((mEmitModifie) && isVisible())
    emit modifie();
}

void CYFlagInput::setCurrentData(int index)
{
  CYDataWdg::setCurrentData(index);
  emit currentDataChanged();
  if ((mEmitModifie) && isVisible())
    emit modifie();
}

/*void CYFlagInput::setForcingMode(const bool val)
{
  if (val && (mForcingName==""))
    mForcingName=mDataName;
  CYDataWdg::setForcingMode(val);
}
*/
void CYFlagInput::setForcingData(CYData *data)
{
  CYDataWdg::setForcingData(data);
  if (data)
  {
    connect(data, SIGNAL(synopticForcingChanged(int)), SLOT(synopticForcingChanged(int)));
    connect(this, SIGNAL(activated(int)), data, SIGNAL(synopticForcingChanged(int)));
    connect(this, SIGNAL(activated(int)), data, SIGNAL(tableForcingChanged(int)));
  }
}



/*! Réception du signal émis à chaque changement de valeur de forçage à partir du synoptic.
    \fn CYFlagInput::synopticForcingChanged(int)
 */
void CYFlagInput::synopticForcingChanged(int value)
{
  setValue(value);
}
