#ifndef CYTEXTDIALOG_H
#define CYTEXTDIALOG_H

// CYLIBS
#include <cydialog.h>

namespace Ui {
    class CYTextDialog;
}


class CYTextDialog : public CYDialog
{
public:
  CYTextDialog(QWidget *parent=0, const QString &name="textDialog");
  ~CYTextDialog();

  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  void hideApplyButton(bool hide);

private:
    Ui::CYTextDialog *ui;
};

#endif // CYTEXTDIALOG_H
