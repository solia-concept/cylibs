//
// C++ Implementation: cyvalve3
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cyvalve3.h"
#include "cycore.h"

CYValve3::CYValve3(QWidget *parent, const QString &name)
  : CYPixmapView(parent, name)
{
  init(false);
}


CYValve3::~CYValve3()
{
}


void CYValve3::init(bool value)
{
  mValue = value;
  mManual = false;
  setType(mType);
  initPalette();
}

void CYValve3::setType(const Type val)
{
  mType = val;

  QString txt_true, txt_false;
  switch (mType)
  {
    case ESW:
      txt_true = "cyvalve3_esw_true";
      txt_false = "cyvalve3_esw_false";
      break;
    case EWS:
      txt_true = "cyvalve3_ews_true";
      txt_false = "cyvalve3_ews_false";
      break;
    case SEW:
      txt_true = "cyvalve3_sew_true";
      txt_false = "cyvalve3_sew_false";
      break;
    case SWE:
      txt_true = "cyvalve3_swe_true";
      txt_false = "cyvalve3_swe_false";
      break;
    case WES:
      txt_true = "cyvalve3_wes_true";
      txt_false = "cyvalve3_wes_false";
      break;
    case WSE:
      txt_true = "cyvalve3_wse_true";
      txt_false = "cyvalve3_wse_false";
      break;
  }

  if (mManual)
  {
    txt_true.append("_manual");
    txt_false.append("_manual");
  }

  setPixmapFileTrue(QString(":/pixmaps/%1.png").arg(txt_true).toUtf8());
  setPixmapFileFalse(QString(":/pixmaps/%1.png").arg(txt_false).toUtf8());
}

void CYValve3::setManual(const bool val)
{
  mManual = val;
  setType(mType);
}

bool CYValve3::manual() const
{
  return mManual;
}
