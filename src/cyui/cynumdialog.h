#ifndef CYNUMDIALOG_H
#define CYNUMDIALOG_H

// CYLIBS
#include <cydialog.h>

namespace Ui {
    class CYNumDialog;
}


class CYNumDialog : public CYDialog
{
public:
  CYNumDialog(QWidget *parent=0, const QString &name="textDialog");
  ~CYNumDialog();

  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

private:
    Ui::CYNumDialog *ui;
};

#endif // CYNUMDIALOG_H
