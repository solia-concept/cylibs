#ifndef CYACTIONMENU_H
#define CYACTIONMENU_H

// CYLIBS
#include "cyaction.h"
//Added by qt3to4:
#include <QMenu>

/**
 * A CYActionMenu is an action that holds a sub-menu of other actions.
 * insert() and remove() allow to insert and remove actions into this action-menu.
 * Plugged in a popupmenu, it will create a submenu.
 * Plugged in a toolbar, it will create a button with a popup menu.
 *
 * This is the action used by the XMLGUI since it holds other actions.
 * If you want a submenu for selecting one tool among many (without icons), see KSelectAction.
 * See also setDelayed about the main action.
 */
class CYActionMenu : public CYAction
{
  Q_OBJECT
  Q_PROPERTY( bool delayed READ delayed WRITE setDelayed )
  Q_PROPERTY( bool stickyMenu READ stickyMenu WRITE setStickyMenu )

public:
    CYActionMenu( const QString& text, CYActionCollection *parent = 0,
                 const char* name = 0 );
    CYActionMenu( const QString& text, const QIcon& icon,
                 CYActionCollection *parent = 0, const char* name = 0 );
    CYActionMenu( const QString& text, const QString& icon,
                 CYActionCollection *parent = 0, const char* name = 0 );
    CYActionMenu( CYActionCollection *parent = 0, const char* name = 0 );
    virtual ~CYActionMenu();

    virtual void insert( CYAction*, int index = -1 );
    virtual void remove( CYAction* );

    QMenu* popupMenu() const;
    void popup( const QPoint& global );

    /**
     * Returns true if this action creates a delayed popup menu
     * when plugged in a KToolbar.
     */
    bool delayed() const;
    /**
     * If set to true, this action will create a delayed popup menu
     * when plugged in a KToolbar. Otherwise it creates a normal popup.
     * Default: delayed
     *
     * Remember that if the "main" action (the toolbar button itself)
     * cannot be clicked, then you should call setDelayed(false).
     *
     * On the opposite, if the main action can be clicked, it can only happen
     * in a toolbar: in a menu, the parent of a submenu can't be activated.
     * To get a "normal" menu item when plugged a menu (and no submenu)
     * use KToolBarPopupAction.
     */
    void setDelayed(bool _delayed);

    /**
     * Returns true if this action creates a sticky popup menu.
     * See setStickyMenu().
     */
    bool stickyMenu() const;
    /**
     * If set to true, this action will create a sticky popup menu
     * when plugged in a KToolbar.
     * "Sticky", means it's visible until a selection is made or the mouse is
     * clicked elsewhere. This feature allows you to make a selection without
     * having to press and hold down the mouse while making a selection.
     * Default: sticky.
     */
    void setStickyMenu(bool sticky);

    virtual int plug( QWidget* widget, int index = -1 );

private:
    class CYActionMenuPrivate;
    CYActionMenuPrivate *d;
};

#endif // CYACTIONMENU_H
