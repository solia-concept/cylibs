/***************************************************************************
                          cycardana.h  -  description
                             -------------------
    début                  : jeu mar 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCARDANA_H
#define CYCARDANA_H

// CYLIBS
#include "cycardio.h"

class CYBoxANA;

/**
 * CYCardANA est un widget représentant des cartes
 * électriques d'E/S ANA.
 * @see CYCardAI, CYCardAO
 * @short Widget d'une carte ANA.
 * @author Gérald LE CLEACH
 */

class CYCardANA : public CYCardIO
{
  Q_OBJECT

public:
  /**
   * Constructeur
   */
  CYCardANA(QWidget *parent=0, const QString &name=0, int no=0, Mode m=View);
  /**
   * Destructeur
   */
  ~CYCardANA();

  /**
   * Place les données dans le widget.
   * @param db Base de données à traîter.
   */
  virtual void setData(QHash<int, CYTypeData<class T>*> db) { Q_UNUSED(db) }

  virtual QSize sizeHint() const;
  virtual QSize minimumSizeHint() const;

public slots: // Public slots
  /**
   * Raffraîchit les valeurs.
   */
  virtual void refresh() {}
  /**
   * Simulation.
   */
  virtual void simulation() {}

protected:
  /**
   * Initialise les widgets selon les paramètres de la carte.
   */
  virtual void init() {}
  /**
   * Met à jour les widgets.
   */
  virtual void updateCard() {}
};

#endif
