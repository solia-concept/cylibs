/***************************************************************************
                          cycheckbox.cpp  -  description
                             -------------------
    début                  : Sun Feb 23 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cycheckbox.h"

// QT
#include <QPushButton>
// CYLIBS
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cyword.h"
#include "cybool.h"
#include "cystring.h"
#include "cydialog.h"
#include "cywidget.h"
#include "cyframe.h"
#include "cycore.h"
#include "cyreadonlyfilter.h"

CYCheckBox::CYCheckBox(QWidget *parent, const QString &name)
  : QCheckBox(parent),
    CYDataWdg(this)
{
  setObjectName(name);
  init();
}

CYCheckBox::CYCheckBox(const QString &text, QWidget *parent, const QString &name)
  : QCheckBox(text,parent),
    CYDataWdg(this)
{
  setObjectName(name);
  init();
}

CYCheckBox::~CYCheckBox()
{
}


void CYCheckBox::init()
{
  mWriteData = true;
  mInputHelp = true;
  mEmitModifie  = true;
  mLenText   = -1;
  mShowLabel = true;
  mShowGroup =No;
  mAutoRefresh = false;

  mPalette = QPalette(palette());

  connect( this, SIGNAL( clicked() ), this, SIGNAL( modifie() ) );
  connect(this, SIGNAL(stateChanged(int)), SLOT(setStateChanged(int)));
  setFocusPolicy(Qt::WheelFocus);
}

void CYCheckBox::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYCheckBox::dataName() const
{
  return CYDataWdg::dataName();
}

void CYCheckBox::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYCheckBox::flagName() const
{
  return CYDataWdg::flagName();
}

void CYCheckBox::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYCheckBox::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYCheckBox::setInverseFlag(bool inverse)
{
  CYDataWdg::setInverseFlag(inverse);
}

bool CYCheckBox::inverseFlag() const
{
  return CYDataWdg::inverseFlag();
}

void CYCheckBox::setInverseHideFlag(bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYCheckBox::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

CYCheckBox::GroupSection CYCheckBox::showGroup() const
{
  return mShowGroup;
}

void CYCheckBox::setShowGroup(const GroupSection val)
{
  mShowGroup = val;
}

bool CYCheckBox::ctrlFlag()
{
  if (mFlag==0)
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;

  setEnabled(res);
  return res;
}

void CYCheckBox::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYCheckBox::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYCheckBox::benchType() const
{
  return CYDataWdg::benchType();
}

bool CYCheckBox::value() const
{
  return isChecked();
}

void CYCheckBox::setValue(bool value)
{
  QCheckBox::setChecked(value);
}

void CYCheckBox::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYCheckBox::linkData()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  mEmitModifie = false;

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  QString group = mData->groupSection((Cy::GroupSection)mShowGroup);
  if (!group.isEmpty())
    group.append(": ");
  if (mShowLabel)
    setText(group+mData->label());
  if (group.isEmpty())
    setText(mData->label());
  else
    setText(mData->groupSection((Cy::GroupSection)mShowGroup));

  if (mInputHelp)
  {
    this->setToolTip(mData->inputHelp()+"<br>"+mData->infoCY());
  }
  else
  {
    this->setToolTip(mData->displayHelp()+"<br>"+mData->infoCY());
  }
  mEmitModifie = true;
}

void CYCheckBox::refresh()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (!readOnly())
    return;


  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYCheckBox::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue((bool)data->def());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      setValue((bool)data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYCheckBox::update()
{
  if (!isVisible() && mUpdateIfVisible)
    return;

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (!isEnabled())
    return;

  if (isHidden())
    return;

  mEmitModifie = false;

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      data->setVal(value());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
  mEmitModifie = true;
}

void CYCheckBox::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }


  ctrlFlag();
}

void CYCheckBox::changeEvent(QEvent* event)
{
  if (event && (event->type()==QEvent::EnabledChange))
  {
    // cet événement est envoyé, si l'état actif change
    bool state = isEnabled();
    if (!readOnly() || !state)
      mEnabled = state;
  }
  QCheckBox::changeEvent(event);
}


void CYCheckBox::focusInEvent(QFocusEvent *e)
{
  QWidget::focusInEvent(e);
  emit focusIn();
}

void CYCheckBox::focusOutEvent(QFocusEvent *e)
{
  setDown(false);
  QWidget::focusOutEvent(e);
  emit focusOut();
}

void CYCheckBox::keyPressEvent(QKeyEvent *e)
{
  QCheckBox::keyPressEvent(e);

  switch (e->key())
  {
    case Qt::Key_Return :
    case Qt::Key_Enter  : update();
                      emit applying();
                      break;
  }

  switch (e->modifiers())
  {
    case Qt::AltModifier: // Déplacement rapide
    {
      switch (e->key())
      {
        case Qt::Key_Left   :
        case Qt::Key_Right  :
        case Qt::Key_Down   :
        case Qt::Key_Up     : emit clearFocus(e);
      }
    }
    break;
    default: // Déplacement normal
    {
      switch (e->key())
      {
        case Qt::Key_Enter:
        case Qt::Key_Return:
        {
          if (inherits("QPushButton"))
          {
            QPushButton *pb = (QPushButton *)this;
            if (pb->autoDefault() || pb->isDefault())
              emit clicked();
            else
              e->ignore();
          }
          else
            e->ignore();
          update();
          emit applying();
        }
          break;
        case Qt::Key_Space:
          if (!e->isAutoRepeat())
          {
            setDown(true);
            if (inherits("QPushButton"))
              emit pressed();
            else
              e->ignore();
          }
          break;
        case Qt::Key_Up:
        case Qt::Key_Left:
//        #ifndef QT_NO_BUTTONGROUP TODO QT4
//          if (group())
//            group()->setFocus(e->key());
//          else
//        #endif
            focusNextPrevChild(false);
          break;
        case Qt::Key_Right:
        case Qt::Key_Down:
//        #ifndef QT_NO_BUTTONGROUP TODO QT4
//          if (group())
//            group()->moveFocus(e->key());
//          else
//        #endif
            focusNextPrevChild(true);
          break;
        case Qt::Key_Escape:
          if (isDown())
          {
            setDown(false);
            update();
            break;
          }
        // fall through
        default:
          e->ignore();
      }
    }
    break;
  }
}

void CYCheckBox::resizeEvent(QResizeEvent *e)
{
  QAbstractButton::resizeEvent(e);

// TODO QT4
//  QPainter p(this);
//  QSize isz = style().itemRect(&p, QRect(0, 0, 1, 1), ShowPrefix, false, pixmap(), text(), mLenText).size();
//  QSize wsz = (style().sizeFromContents(QStyle::CT_CheckBox, this, isz).expandedTo(QApplication::globalStrut()));

//  QCheckBox::update(wsz.width(), isz.width(), 0, wsz.height());

//  if (autoMask())
//    updateMask();
}

void CYCheckBox::setStateChanged(int index)
{
  if (index==Qt::PartiallyChecked)
    return;
  if ((mEmitModifie) && isVisible())
  {
    emit modifie();
    setFocus();
  }
  bool val= (index==Qt::Checked) ? true : false;
  emit stateOn(val);
  if (mData)
    mData->setTmp(val);
}

