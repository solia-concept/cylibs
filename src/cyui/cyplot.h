#ifndef CYPLOT_H
#define CYPLOT_H

#include "qwt_plot.h"
#include "qwt_math.h"
#include "qwt_symbol.h"
#include "qwt_plot_curve.h"
#include "qwt_series_data.h"
#include "qwt_plot_marker.h"
#include "qwt_picker.h"
#include "qwt_plot_picker.h"
#include "qwt_plot_marker.h"

class CYPlotPicker;
class CYPlotCurve;

/** Classe facilitant la compatibilité de QWtPlot
 *  avec les anciennes librairies Qwt4. */
class CYPlot : public QwtPlot
{
  Q_OBJECT
public:
  CYPlot(QWidget *p = 0, const QString &name = 0);
  CYPlot(const QString &title, QWidget *p = 0, const QString &name = 0);

  long insertCurve(QwtPlotCurve *);
  virtual long insertCurve(const QString &title,
                           int xAxis = xBottom, int yAxis = yLeft);

  QwtPlotCurve *curve(long key);
  const QwtPlotCurve *curve(long key) const;

  bool removeCurve(long key);
  void removeCurves();

  // Curves

  bool setCurveRawData(long key, const double *x, const double *y, int size, double yCoef=1.0, double yOffset=0.0);
  bool setCurveData(long key, const double *x, const double *y, int size, double yCoef=1.0, double yOffset=0.0);

  virtual QColor printColor(const QColor &) const;

#ifndef QWT_NO_COMPAT
  // Outline

  void enableOutline(bool tf);
  bool outlineEnabled() const;
  void setOutlineStyle(QwtPicker::RubberBand os);
  QwtPicker::RubberBand outlineStyle() const;
  void setOutlinePen(const QPen &pn);
  QPen outlinePen() const;
#endif


  //  Markers

  long insertMarker(QwtPlotMarker *);
  long insertMarker(const QString &label = QString(),
                    int xAxis = xBottom, int yAxis = yLeft);
  long insertLineMarker(const QString &label, int axis);

  QwtPlotMarker *marker(long key);
  const QwtPlotMarker *marker(long key) const;

  bool removeMarker(long key);
  void removeMarkers();

  //  long closestMarker(int xpos, int ypos, int &dist) const;
  //  QwtArraySeriesData<long> markerKeys() const;

  bool setMarkerXAxis(long key, int axis);
  int markerXAxis(long key) const;
  bool setMarkerYAxis(long key, int axis);
  int markerYAxis(long key) const;

  bool setMarkerPos(long key, double xval, double yVal);
  bool setMarkerXPos(long key, double val);
  bool setMarkerYPos(long key, double val);
  void markerPos(long key, double &mx, double &my) const;

  bool setMarkerFont(long key, const QFont &f);
  QFont markerFont(long key) const;
  bool setMarkerPen(long key, const QPen &p);

  bool setMarkerLabel(long key, const QString &text,
                      const QFont &font = QFont(), const QColor &color = QColor(),
                      const QPen &pen = QPen(Qt::NoPen),
                      const QBrush &brush = QBrush(Qt::NoBrush));

  bool setMarkerLabelText(long key, const QString &text);
  const QString markerLabel(long key) const;
  bool setMarkerLabelAlign(long key, int align);
  int markerLabelAlign(long key) const;
  bool setMarkerLabelPen(long key, const QPen &p);
  QPen markerLabelPen(long key) const;

  bool setMarkerLinePen(long key, const QPen &p);
  QPen markerLinePen(long key) const;
  bool setMarkerLineStyle(long key, QwtPlotMarker::LineStyle st);
  QwtPlotMarker::LineStyle markerLineStyle(long key) const;

  bool setMarkerSymbol(long key, const QwtSymbol *s);
  const QwtSymbol *markerSymbol(long key) const;

  virtual bool showMarker(long key, bool show);

  // Legend
  virtual void enableLegend(bool tf, long curveKey = -1);

protected:
  long newCurveKey();
  long newMarkerKey();

  CYPlotPicker *mPicker;
};

#endif // CYPLOT_H
