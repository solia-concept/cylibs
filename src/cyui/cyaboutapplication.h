#ifndef CYABOUTAPPLICATION_H
#define CYABOUTAPPLICATION_H

// CYLIBS
#include <cydialog.h>

namespace Ui {
    class CYAboutApplication;
}

class CYAboutApplication : public CYDialog
{
  Q_OBJECT
public:
  CYAboutApplication(QWidget *parent=0, const QString &name="cyAboutApplication");
  ~CYAboutApplication();

signals:

public slots:
  virtual void addTab( QWidget * child, const QString & label );


private:
    Ui::CYAboutApplication *ui;
};

#endif // CYABOUTAPPLICATION_H
