/***************************************************************************
                          cyboxdi.cpp  -  description
                             -------------------
    début                  : Sun Feb 23 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cyboxdi.h"

CYBoxDI::CYBoxDI(QWidget *parent, const QString &name, QColor c, Mode m)
: CYBoxDIG(parent, name, c, m)
{
  mTitle = QString(name);

  hl = new QHBoxLayout(this);
  hl->setObjectName(mTitle.toUtf8());
  hl->setSpacing(3);
  hl->setMargin(2);
  hl->setAlignment(Qt::AlignVCenter|Qt::AlignHCenter);

  init();
}

CYBoxDI::~CYBoxDI()
{
}

void CYBoxDI::init()
{
  if (mHasData)
  {
    mTitle = mData->elec();
    this->setToolTip(mData->displayHelp()+"<br>"+mData->infoCY());
  }

  mLed = new CYLed(this);
  QSizePolicy sp((QSizePolicy::Policy)0, (QSizePolicy::Policy)0);
  sp.setHeightForWidth(mLed->sizePolicy().hasHeightForWidth());
  mLed->setSizePolicy(sp);
  mLed->setState(CYLed::On);
  mLed->setLook(CYLed::Raised);
  mLed->setColor(mColor);
  hl->addWidget(mLed);
   
  mLabel = new QLabel(this);
  mLabel->setText(mTitle);
  hl->addWidget(mLabel);

  refresh();
}

void CYBoxDI::update()
{
  delete mLed;
  delete mLabel;
  
  init();
}

void CYBoxDI::setData(CYDI *data)
{
  mData    = data;
  mHasData = true;
  update();
}

void CYBoxDI::refresh()
{
  if (!mHasData)
    return;

  if (mData->val())
    mLed->on();    
  else
    mLed->off();
}

void CYBoxDI::simulation()
{
  if (mSimul)
  {
    mLed->on();
    mSimul = false;
  }
  else
  {
    mLed->off();
    mSimul = true;
  }
}

