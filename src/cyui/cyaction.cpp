// QT
#include <QResource>
// CYLIBS
#include "cyaction.h"
#include "cydblibs.h"
#include "cyflag.h"
#include "cywin.h"
#include "cycore.h"

CYAction::CYAction(const QString &text,
                   const QString &pix,
                   const QKeySequence &accel,
                   const QObject *receiver,
                   const char *slot,
                   CYActionCollection *parent,
                   const QString &name,
                   bool toggle) :
  QAction(text, parent)
{
  mActionCollection=parent;
  if (pix.startsWith(":") || pix.startsWith("/"))
    setIcon(QIcon(pix));
  else if (!pix.isEmpty())
  {
    QString resource = QString(":/icons/%1.png").arg(pix);
    if (QResource(resource).isValid())
      setIcon(QIcon(resource));
    else
    {
      resource = QString(":/pics/%1.png").arg(pix);
      if (QResource(resource).isValid())
        setIcon(QIcon(resource));
      else
        setIcon(QIcon::fromTheme(pix));
    }
  }
  setObjectName(name);
  setShortcut(accel);
  parent->addAction( this );
  setCheckable(toggle);
  init();
  if ( toggle )
    connect( this, SIGNAL( toggled(bool) ), receiver, slot );
  else if ( receiver && slot )
    connect( this, SIGNAL( triggered(bool) ), receiver, slot );
}

CYAction::CYAction(const QString &text,
                   const QIcon icon,
                   const QKeySequence &accel,
                   const QObject *receiver,
                   const char *slot,
                   CYActionCollection *parent,
                   const QString &name,
                   bool toggle) :
  QAction(text, parent)
{
  mActionCollection=parent;
  setIcon(icon);
  setObjectName(name);
  setShortcut(accel);
  parent->addAction( this );
  setCheckable(toggle);
  init();
  if ( toggle )
    connect( this, SIGNAL( toggled(bool) ), receiver, slot );
  else if ( receiver && slot )
    connect( this, SIGNAL( triggered(bool) ), receiver, slot );
}


CYAction::CYAction(const QString &text,
                   const QString &pix,
                   const QKeySequence &accel,
                   CYActionCollection *parent,
                   const QString &name,
                   bool toggle) :
  QAction(text, parent)
{
  mActionCollection=parent;
  if (pix.startsWith(":") || pix.startsWith("/"))
    setIcon(QIcon(pix));
  else if (!pix.isEmpty())
  {
    QString resource = QString(":/icons/%1.png").arg(pix);
    if (QResource(resource).isValid())
      setIcon(QIcon(resource));
    else
    {
      resource = QString(":/pics/%1.png").arg(pix);
      if (QResource(resource).isValid())
        setIcon(QIcon(resource));
      else
        setIcon(QIcon::fromTheme(pix));
    }
  }
  setObjectName(name);
  setShortcut(accel);
  parent->addAction( this );
  setCheckable(toggle);
  init();
}

CYAction::CYAction(const QString &text,
                   const QIcon icon,
                   const QKeySequence &accel,
                   CYActionCollection *parent,
                   const QString &name,
                   bool toggle) :
  QAction(text, parent)
{
  mActionCollection=parent;
  setIcon(icon);
  setObjectName(name);
  setShortcut(accel);
  parent->addAction( this );
  setCheckable(toggle);
  init();
}

void CYAction::init()
{
  QString dataName = QString("CYACTION_USED_%1_%2").arg(mActionCollection->window()->objectName()).arg(objectName());

  mUsedFlag=(CYFlag *)core->findData( dataName, false, false);

  if (!mUsedFlag)
  {
    // Ajout d'un flag d'autorisation de l'action
    mUsedFlag = new CYFlag( core->cydblibs(), dataName, new flg );
    mUsedFlag->setVolatile(true);
    mUsedFlag->setLabel(text());
    mUsedFlag->setHelp(tr("Function usage flag"),
                       tr("The current user is authorized to access this function."),
                       tr("The current user is not authorized to access this function or Cylix is in protected access."));
    mUsedFlag->setGroup(tr("Users administration")+":"+mActionCollection->window()->windowTitle());
  }
}

bool CYAction::hasIcon()
{
  return !icon().isNull();
}

void CYAction::setIdDocBookMark(QString id)
{
	mIdBookMark = id;
}

void CYAction::setMenuPath(const QString &path)
{
	mMenuPath = path;
	if (!mIdBookMark.isEmpty())
		core->addDocBookMark(mIdBookMark, this->path());
}

QString CYAction::path()
{
	QString txt = mMenuPath+text();
	return QString(txt.replace("&",""));
}

void CYAction::setUsed(bool val, bool notify)
{
  if (mUsedFlag)
  {
    mUsedFlag->setVal(val);
    if (notify)
      emit usedUpdated( val );
  }
}
