/***************************************************************************
                          cyctinput.h  -  description
                             -------------------
    begin                : mar avr 13 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYCTINPUT_H
#define CYCTINPUT_H

// CYLIBS
#include <cywidget.h>

namespace Ui {
    class CYCTInput;
}

/** @short Boîte de saisie de contrôle tolérances.
  * @author LE CLÉACH Gérald
  */

class CYCTInput : public CYWidget
{
  Q_OBJECT
  Q_ENUMS(GroupSection)
  Q_PROPERTY(QString prefix READ prefix WRITE setPrefix)
  Q_PROPERTY(QByteArray levelName READ levelName WRITE setLevelName)
  Q_PROPERTY(GroupSection showGroup READ showGroup WRITE setShowGroup)
  Q_PROPERTY(bool projectData READ projectData WRITE setProjectData)

public:
  CYCTInput(QWidget *parent=0, const QString &name=0);
  ~CYCTInput();
  
  /** Mode d'affichage du groupe. */
  /* ATTENTION énumération deavant être identique à Cy::GroupSection*/
  enum GroupSection
  {
    /** Pas d'affichage du groupe. */
    No,
    /** Affichage du dernier sous-groupe. */
    lastGroup,
    /** Affichage des sous-groupes. */
    underGroup,
    /** Affichage du groupe entier. */
    wholeGroup
  };

  /** Connection des objets graphiques avec les données de l'application. */
  virtual void linkDatas();

  /** @return le préfixe du nom des données. */
  QString prefix() const { return mPrefix; }
  /** Saisie le préfixe du nom des données. */
  void setPrefix(const QString &txt) { mPrefix = txt; linkDatas(); }

  /** @return le nom de la donnée du seuil à surveiller. */
  QByteArray levelName() const { return mLevelName; }
  /** Saisie le nom de la donnée du seuil à surveiller. 
      Si celui-est null, alors la ligne de saisie relative est cachée. */
  void setLevelName(const QByteArray &txt) { mLevelName = txt; linkDatas(); }

  /** @return le mode d'affichage du groupe. */
  virtual GroupSection showGroup() const { return mShowGroup; }
  /** Saisir le mode d'affichage du groupe. */
  virtual void setShowGroup(const GroupSection val) { mShowGroup = val; }

  /** @return \a true s'il sagit de données projet. */
  virtual bool projectData() const { return mProjectData; }
  /** Saisir \a true pour signaler qu'il sagit de données projet. */
  virtual void setProjectData(const bool val);

protected: // Protected attributes
  /** Préfixe du nom des données.  */
  QString mPrefix;
  /** Nom de la donnée du seuil à surveiller.  */
  QByteArray mLevelName;
  /** Données projet ou pas */
  bool mProjectData;

  /** Mode d'affichage du groupe. */
  GroupSection mShowGroup;

private:
    Ui::CYCTInput *ui;
};

#endif
