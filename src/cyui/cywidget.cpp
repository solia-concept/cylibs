/***************************************************************************
                          cywidget.cpp  -  description
                             -------------------
    début                  : mer mai 21 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cywidget.h"

// QT
#include <QString>
#include <QShowEvent>
#include <QMouseEvent>
// CYLIBS
#include "cydb.h"
#include "cycore.h"

CYWidget::CYWidget(QWidget *parent, const QString &name, Qt::WindowFlags f)
  : QWidget(parent, f),
    CYTemplate(this)
{
  setObjectName(name);
  mMovable = false;
  mType = Cy::Widget;

  mBackgroundColor=palette().color(backgroundRole());
  mForegroundColor=palette().color(foregroundRole());

  mTimer = new QTimer(this);
  connect(mTimer, SIGNAL(timeout()), SLOT(refresh()));
}

CYWidget::~CYWidget()
{
}

QByteArray CYWidget::genericMark() const
{
  return CYTemplate::genericMark();
}

void CYWidget::setGenericMark(const QByteArray txt)
{
  CYTemplate::setGenericMark(txt);
}

void CYWidget::setGenericMark(int index)
{
  CYTemplate::setGenericMark(index);
}

bool CYWidget::movable() const
{
  return mMovable;
}

void CYWidget::setMovable(const bool val)
{
  mMovable = val;
}

void CYWidget::setBackgroundColor ( const QColor & color )
{
  mBackgroundColor=color;
  setAutoFillBackground(true);
  QPalette pal = palette();
  pal.setColor(backgroundRole(), mBackgroundColor);
  setPalette(pal);
}

const QColor & CYWidget::backgroundColor () const
{
  return mBackgroundColor;
}

void CYWidget::setForegroundColor ( const QColor & color )
{
  mForegroundColor=color;
  QPalette pal = palette();
  pal.setColor(QPalette::WindowText, mForegroundColor);
  setPalette(pal);
}

const QColor & CYWidget::foregroundColor () const
{
  return mForegroundColor;
}


void CYWidget::setBackgroundPixmap( const QPixmap & pixmap )
{
  if (pixmap.isNull())
    return;

  mBackgroundPixmap=pixmap;
  setAutoFillBackground(true);
  QPalette palette;
  palette.setBrush(backgroundRole(), QBrush(pixmap));
  setPalette(palette);
}

const QPixmap & CYWidget::backgroundPixmap () const
{
  return mBackgroundPixmap;
}

void CYWidget::linkDatas()
{
  mDatasLinked = false;
  mModified = false;
  init(this);
  setValidate();
  setReadOnly(mReadOnly);
  mDatasLinked = true;
}

void CYWidget::apply()
{
  if (!isVisible())
    return;

  CYTemplate::update(this);

  foreach (CYDB *db, mWriteDB)
    db->apply();

  if (mForcingMode)
  {
    foreach (CYDB *db, mForcingDB)
      if (!mWriteDB.value(db->objectName()))
        db->apply();
  }
  CYTemplate::apply(this);
  setValidate();
}

void CYWidget::update()
{
  if (isVisible())
    CYTemplate::update(this);
}

void CYWidget::refresh()
{
  if (isVisible())
    CYTemplate::refresh(this);
}

void CYWidget::designer()
{
  if (isVisible())
  {
    CYTemplate::designer(this);
    setModifie();
  }
}

void CYWidget::help()
{
  core->invokeHelp("cydoc", metaObject()->className());
}

void CYWidget::setModifie()
{
  if (isVisible() && mDatasLinked && mEmitModifie)
  {
    if (!isModified())
    {
      mModified = true;
      setModifieView();
    }
    emit modifie();
    emit modified(true);
  }
}

void CYWidget::setValidate()
{
  if (isVisible() && mDatasLinked)
  {
    if (isModified())
    {
      mModified = false;
      setValidateView();
    }
    emit validate();
    emit modified(false);
  }
}

void CYWidget::setReadOnly(bool val)
{
  mReadOnly = val;
  CYTemplate::setReadOnly(val, this);
}

void CYWidget::setForcingMode(bool val)
{
  mForcingMode = val;
  CYTemplate::setForcingMode(val, this);
}

void CYWidget::setSettingMode(bool val)
{
  mSettingMode = val;
  CYTemplate::setSettingMode(val, this);
}

void CYWidget::changeEvent(QEvent* event)
{
  QWidget::changeEvent(event);
}

void CYWidget::showEvent(QShowEvent *e)
{
  if (mLinkDatasShow)
    linkDatas();
  QWidget::showEvent(e);
}

/*! Gestion d'appui de bouton de souris
    \fn CYWidget::mousePressEvent ( QMouseEvent * e )
 */
void CYWidget::mousePressEvent ( QMouseEvent * e )
{
  if ( movable() && !isReadOnly() )
  {
    mXOldPos = x();
    mYOldPos = y();
    mXMousePos = e->x();
    mYMousePos = e->y();
    emit movedInsideParent();
  }
  QWidget::mousePressEvent ( e );
}


/*! Gestion de déplacement de bouton de souris
    \fn CYWidget::mouseMoveEvent ( QMouseEvent * e )
 */
void CYWidget::mouseMoveEvent ( QMouseEvent * e )
{
  if ( movable() && !isReadOnly() )
  {
    int ex = e->x();
    int ey = e->y();
    int x = mXOldPos + ( ex - mXMousePos );
    int y = mYOldPos + ( ey - mYMousePos );
    moveInsideParent( x, y );
    raise();
  }
  QWidget::mouseMoveEvent( e );
}


/*!
    \fn CYWidget::moveInsideParent(int x, int y)
 */
void CYWidget::moveInsideParent(int x, int y)
{
  QWidget * p = (QWidget *)parent();
  if ( !p->rect().contains(x, p->rect().y()) )
  {
    if ( x < p->rect().left() )
      x = p->rect().left();            // sort à gauche
    else
      x = p->rect().right()-width();   // sort à droite
  }
  else if ( !p->rect().contains(x+width(), p->rect().y()) )
  {
    x = p->rect().right()-width();     // sort en partie à droite
  }

  if ( !p->rect().contains(p->rect().x(), y) )
  {
    if ( y < p->rect().top() )
      y = p->rect().top();             // sort en haut
    else
      y = p->rect().bottom()-height(); // sort en bas
  }
  else if ( !p->rect().contains(p->rect().x(), y+height()) )
  {
    y = p->rect().bottom()-height();   // sort en partie en bas
  }

  move(x, y);
  mXOldPos = x;
  mYOldPos = y;
}


/*! @return \a true si la connexion automatique des données lors de l'affichage est autorisée.
    @see CYWidget::setLinkDatasShow(bool val)
    \fn CYWidget::linkDatasShow() const
 */
bool CYWidget::linkDatasShow() const
{
  return CYTemplate::linkDatasShow();
}


/*! Autorise ou non la connexion des données suivant \a val.
    Ceci est utile lorsqu'on veut inhiber la connexion qui est faite automatiquement au moment de l'affichage, par exemple pour le mode forçage.
    \fn CYWidget::setLinkDatasShow(const bool val)
 */
void CYWidget::setLinkDatasShow(const bool val)
{
  CYTemplate::setLinkDatasShow( val );
}
