/***************************************************************************
                          cytextlabel.cpp  -  description
                             -------------------
    début                  : lun sep 1 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cytextlabel.h"

// CYLIBS
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cyflag.h"
#include "cystring.h"
#include "cycolor.h"
#include "cybool.h"
#include "cyword.h"
#include "cycore.h"
#include "cytextdialog.h"
//Added by qt3to4:
#include <QString>
#include <QMouseEvent>

CYTextLabel::CYTextLabel(QWidget *parent, const QString &name)
  : CYLabel(parent, name)
{
  init("textLabel");
}

CYTextLabel::~CYTextLabel()
{
}

void CYTextLabel::init(QString value)
{
  setValue(value);
  mOrientation = Qt::Horizontal;
  mMirror = false;
  mRotate = 0;
  mAlwaysOk  = true;
  mDataShow  = true;
  mShowLabel = false;
  mShowChoiceLabel = false;
  mEnableProcessColor = false;
  mShowGroup = No;

//  mEnableColor = QColor(palette().color(QPalette::WindowText));
//  mBackgroundColor = QColor(palette().color(QPalette::Window));
  mEnableColor = QColor(foregroundRole());
  mDisableColor = QColor(palette().color(QPalette::Disabled, QPalette::WindowText));
  mBackgroundColor = QColor(backgroundRole());
  setScaledContents(true);
}

QString CYTextLabel::value()
{
  return mValue;
}

void CYTextLabel::setValue(QString val)
{
  mValue = val;
  setText(val);
}

void CYTextLabel::setData(CYData *data)
{
  if (mData)
  {
    disconnect(mData, SIGNAL(enableColorChanged(const QColor&)), this, SLOT(setEnableColor(const QColor&)));
    disconnect(mData, SIGNAL(disableColorChanged(const QColor&)), this, SLOT(setDisableColor(const QColor&)));
  }
  CYLabel::setData(data);
  if (mData)
  {
    connect(mData, SIGNAL(enableColorChanged(const QColor&)), this, SLOT(setEnableColor(const QColor&)));
    connect(mData, SIGNAL(disableColorChanged(const QColor&)), this, SLOT(setDisableColor(const QColor&)));
  }
}

void CYTextLabel::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYTextLabel::dataName() const
{
  return CYDataWdg::dataName();
}

CYTextLabel::GroupSection CYTextLabel::showGroup() const
{
  return mShowGroup;
}

void CYTextLabel::setShowGroup(const GroupSection val)
{
  mShowGroup = val;
}

void CYTextLabel::linkData()
{
  mCtrlFlag=true;

  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));

  if (!mDataShow)
    return;

  switch (mData->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                          group.append(": ");
                      if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                      {
                        QString txt = data->toString();
                        setValue(mPrefix+txt+mSuffix);
                      }
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::Color :
                    {
                      CYColor *data = (CYColor *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYTextLabel::refresh()
{
  ctrlHideFlag();

  if (!ctrlFlag())
    return;

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
      hide();
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (!mData->isOk() && !mAlwaysOk)
  {
    setEnabled(false);
    return;
  }
  if (mData->flag()!=0)
  {
    if (!mData->flag()->val() && !mAlwaysOk)
    {
      setEnabled(false);
      return;
    }
  }

  if (!mDataShow)
    return;

	this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));

  switch (mData->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowChoiceLabel)
                        setValue(mPrefix+group+data->choiceLabel(data->val())+mSuffix);
                      else if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                      {
                        QString txt = data->toString();
                        setValue(mPrefix+txt+mSuffix);
                      }
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    case Cy::Color  :
                    {
                      CYColor *data = (CYColor *)mData;
                      QString group = data->groupSection((Cy::GroupSection)mShowGroup);
                      if (!group.isEmpty())
                        group.append(": ");
                      if (mShowLabel)
                        setValue(mPrefix+group+data->label()+mSuffix);
                      else if (group.isEmpty())
                        setValue(mPrefix+data->toString()+mSuffix);
                      else
                        setValue(mPrefix+data->groupSection((Cy::GroupSection)mShowGroup)+mSuffix);
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      break;
  }
}

void CYTextLabel::changeEvent(QEvent* event)
{
  if (event && (event->type()==QEvent::EnabledChange))
  {
    // cet événement est envoyé, si l'état actif change
    bool state = isEnabled();
    if (!readOnly() || !state)
    {
      if (mEnabled!=state) // Mise à jour des couleurs
        setEnableProcessColor( enableProcessColor() );

      mEnabled = state;
    }
  }
  CYLabel::changeEvent(event);
}

void CYTextLabel::setEnableProcessColor(const bool val)
{
  mEnableProcessColor = val;

  QPalette pal = palette();

  if (!mEnableProcessColor)
  {
    pal.setColor(QPalette::Active  , foregroundRole(), mEnableColor );
    pal.setColor(QPalette::Active  , backgroundRole(), mBackgroundColor );
    pal.setColor(QPalette::Inactive, foregroundRole(), mEnableColor );
    pal.setColor(QPalette::Inactive, backgroundRole(), mBackgroundColor );
    pal.setColor(QPalette::Disabled, foregroundRole(), mDisableColor );
    pal.setColor(QPalette::Disabled, backgroundRole(), mBackgroundColor );
  }
  else if (mData)
  {
    pal.setColor(QPalette::Active  , foregroundRole(), mData->enableColor() );
    pal.setColor(QPalette::Active  , backgroundRole(), mBackgroundColor );
    pal.setColor(QPalette::Inactive, foregroundRole(), mData->enableColor() );
    pal.setColor(QPalette::Inactive, backgroundRole(), mBackgroundColor );
    pal.setColor(QPalette::Disabled, foregroundRole(), mData->disableColor() );
    pal.setColor(QPalette::Disabled, backgroundRole(), mBackgroundColor );
  }
  setPalette( pal );
}

void CYTextLabel::setEnableColor(const QColor &c)
{
  mEnableColor = c;
  if (!showLabel() && !showGroup())
    setEnableProcessColor(mEnableProcessColor);
}

void CYTextLabel::setDisableColor(const QColor &c)
{
  mDisableColor = c;
  if (!showLabel() && !showGroup())
    setEnableProcessColor(mEnableProcessColor);
}

void CYTextLabel::setBackgroundColor(const QColor &c)
{
  CYLabel::setBackgroundColor(c);
//
//  if (!showLabel() && !showGroup())
//    setEnableProcessColor(mEnableProcessColor);
}

void CYTextLabel::setForegroundColor(const QColor &c)
{
  CYLabel::setForegroundColor(c);
  setEnableColor(c);
}

void CYTextLabel::mouseDoubleClickEvent ( QMouseEvent * e )
{
  if (!core->simulation())
    return CYLabel::mouseDoubleClickEvent ( e );

  CYTextDialog *dlg = new CYTextDialog(this);
  if (mData)
  {
    dlg->setDataName(mData->objectName().toLocal8Bit());
    dlg->exec();
  }
}
