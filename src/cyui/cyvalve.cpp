/***************************************************************************
                          cyvalve.cpp  -  description
                             -------------------
    début                  : mer aoû 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyvalve.h"

// PIXMAPS
// #include "valve_no_true.xpm"
// #include "valve_no_false.xpm"
// #include "valve_nf_true.xpm"
// #include "valve_nf_false.xpm"

// QT
#include <QImage>
#include <QPixmap>
#include <QMatrix>
// CYLIBS
#include "cycore.h"
#include "cy.h"

CYValve::CYValve(QWidget *parent, const QString &name)
  : CYPixmapView(parent, name)
{
  init(false);
}

CYValve::~CYValve()
{
}

void CYValve::init(bool value)
{
  mValue = value;
  mNF = true ;
  mWay = None;
  mCalibrated = false;
  setManual( false );
  initPalette();
}

void CYValve::setNF(const bool val)
{
  mNF = val;

  setWay(mWay);
}

bool CYValve::NF() const
{
  return mNF;
}

void CYValve::setManual(const bool val)
{
  mManual = val;


  if ( mManual )
  {
    setPixmapFileTrue(":/pixmaps/cyvalve_manual_true.png");
    setPixmapFileFalse(":/pixmaps/cyvalve_manual_false.png");
  }
  else
    setNF( mNF );
}

bool CYValve::manual() const
{
  return mManual;
}

void CYValve::setWay(const Way val)
{
  mWay = val;

  switch (mWay)
  {
    case None :
      if (mNF)
      {
        setPixmapFileTrue(":/pixmaps/cyvalve_nf_true.png" );
        setPixmapFileFalse(":/pixmaps/cyvalve_nf_false.png" );

      }
      else
      {
        setPixmapFileTrue( ":/pixmaps/cyvalve_no_true.png" );
        setPixmapFileFalse( ":/pixmaps/cyvalve_no_false.png" );

      }
      break;
    case toLeft:
      if (mNF)
        setPixmapFileTrue(":/pixmaps/cyvalve_rl_true.png");
      else
        setPixmapFileFalse(":/pixmaps/cyvalve_rl_false.png");
      break;
    case toRight:
      if (mNF)
        setPixmapFileTrue(":/pixmaps/cyvalve_lr_true.png");
      else
        setPixmapFileFalse(":/pixmaps/cyvalve_lr_false.png");
      break;
    default :
      CYWARNINGTEXT(objectName());
  }
  setCalibrated(mCalibrated);
}

bool CYValve::calibrated() const
{
  return mCalibrated;
}

void CYValve::setCalibrated(const bool val)
{
  mCalibrated = val;

  if (!mCalibrated)
    return;

  switch (mWay)
  {
    case None :
      break;
    case toLeft:
      if (mNF)
        setPixmapFileFalse(":/pixmaps/cyvalve_rlt_false.png");
      else
        setPixmapFileTrue(":/pixmaps/cyvalve_rlt_true.png");
      break;
    case toRight:
      if (mNF)
        setPixmapFileFalse(":/pixmaps/cyvalve_lrt_false.png");
      else
        setPixmapFileTrue(":/pixmaps/cyvalve_lrt_true.png");
      break;
    default :
      CYWARNINGTEXT(objectName());
  }
}
