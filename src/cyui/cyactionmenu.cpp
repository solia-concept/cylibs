#include "cyactionmenu.h"

#include "cy.h"

class CYActionMenu::CYActionMenuPrivate
{
public:
  CYActionMenuPrivate()
  {
    m_popup = new QMenu(0L);
    m_delayed = true;
    m_stickyMenu = true;
  }
  ~CYActionMenuPrivate()
  {
    delete m_popup; m_popup = 0;
  }
  QMenu *m_popup;
  bool m_delayed;
  bool m_stickyMenu;
};

CYActionMenu::CYActionMenu( CYActionCollection *parent, const char* name )
: CYAction( 0, 0, 0, 0, 0, parent, name )
{
  d = new CYActionMenuPrivate;
//  setShortcutConfigurable( false );
}

CYActionMenu::CYActionMenu( const QString& text, CYActionCollection *parent,
                          const char* name )
  : CYAction( text, 0, 0, 0, 0, parent, name )
{
  d = new CYActionMenuPrivate;
//  setShortcutConfigurable( false );
}

CYActionMenu::CYActionMenu( const QString& text, const QIcon& icon,
                          CYActionCollection *parent, const char* name )
: CYAction( text, 0, 0, 0, 0, parent, name )
//  : CYAction( text, icon, 0, 0, parent, name )
{
  Q_UNUSED(icon)
//  TODO QT5
  d = new CYActionMenuPrivate;
//  setShortcutConfigurable( false );
}

CYActionMenu::CYActionMenu( const QString& text, const QString& icon,
                          CYActionCollection *parent, const char* name )
  : CYAction( text, icon, 0, 0, 0, parent, name )
{
  d = new CYActionMenuPrivate;
//  setShortcutConfigurable( false );
}

CYActionMenu::~CYActionMenu()
{
//    unplugAll();
    delete d; d = 0;
}

void CYActionMenu::popup( const QPoint& global )
{
  popupMenu()->popup( global );
}

QMenu* CYActionMenu::popupMenu() const
{
  return d->m_popup;
}

void CYActionMenu::insert( CYAction* cmd, int index )
{
  Q_UNUSED(index)
//  TODO QT5
  if ( cmd )
  {
    actionCollection()->addActionTo( cmd->objectName(), d->m_popup);
  }
//    cmd->setMenu(d->m_popup);

//  if ( cmd )
//    cmd->plug( d->m_popup, index );
}

void CYActionMenu::remove( CYAction* cmd )
{
  Q_UNUSED(cmd)
//  TODO QT5
//  if ( cmd )
//    cmd->unplug( d->m_popup );
}

bool CYActionMenu::delayed() const {
    return d->m_delayed;
}

void CYActionMenu::setDelayed(bool _delayed) {
    d->m_delayed = _delayed;
}

bool CYActionMenu::stickyMenu() const {
    return d->m_stickyMenu;
}

void CYActionMenu::setStickyMenu(bool sticky) {
    d->m_stickyMenu = sticky;
}

int CYActionMenu::plug( QWidget* widget, int index )
{
  Q_UNUSED(widget)
  Q_UNUSED(index)
//  TODO QT5
//  if (kapp && !kapp->authorizeCYAction(objectName()))
//    return -1;
//  kdDebug(129) << "CYActionMenu::plug( " << widget << ", " << index << " )" << endl; // remove -- ellis
//  if ( ::qt_cast<QPopupMenu *>( widget ) )
//  {
//    QPopupMenu* menu = static_cast<QPopupMenu*>( widget );
//    int id;
//    if ( hasIcon() )
//      id = menu->insertItem( iconSet(), text(), d->m_popup, -1, index );
//    else
//      id = menu->insertItem( text(), d->m_popup, -1, index );
//
//    if ( !isEnabled() )
//      menu->setItemEnabled( id, false );
//
//    addContainer( menu, id );
//    connect( menu, SIGNAL( destroyed() ), this, SLOT( slotDestroyed() ) );
//
//    if ( m_parentCollection )
//      m_parentCollection->connectHighlight( menu, this );
//
//    return containerCount() - 1;
//  }
////  else if ( ::qt_cast<KToolBar *>( widget ) )
////  {
////    KToolBar *bar = static_cast<KToolBar *>( widget );
//
////    int id_ = CYAction::getToolButtonID();
//
////    if ( icon().isEmpty() && !iconSet().isNull() )
////      bar->insertButton( iconSet().pixmap(), id_, SIGNAL( clicked() ), this,
////                         SLOT( slotActivated() ), isEnabled(), plainText(),
////                         index );
////    else
////    {
////      KInstance *instance;
//
////      if ( m_parentCollection )
////        instance = m_parentCollection->instance();
////      else
////        instance = KGlobal::instance();
//
////      bar->insertButton( icon(), id_, SIGNAL( clicked() ), this,
////                         SLOT( slotActivated() ), isEnabled(), plainText(),
////                         index, instance );
////    }
//
////    addContainer( bar, id_ );
//
////    if (!whatsThis().isEmpty())
////      QWhatsThis::add( bar->getButton(id_), whatsThis() );
//
////    connect( bar, SIGNAL( destroyed() ), this, SLOT( slotDestroyed() ) );
//
////    if (delayed()) {
////        bar->setDelayedPopup( id_, popupMenu(), stickyMenu() );
////    } else {
////        bar->getButton(id_)->setPopup(popupMenu(), stickyMenu() );
////    }
//
////    if ( m_parentCollection )
////      m_parentCollection->connectHighlight( bar, this );
//
////    return containerCount() - 1;
////  }
//  else if ( ::qt_cast<QMenuBar *>( widget ) )
//  {
//    QMenuBar *bar = static_cast<QMenuBar *>( widget );
//
//    int id;
//
//    id = bar->insertItem( text(), popupMenu(), -1, index );
//
//    if ( !isEnabled() )
//        bar->setItemEnabled( id, false );
//
//    addContainer( bar, id );
//    connect( bar, SIGNAL( destroyed() ), this, SLOT( slotDestroyed() ) );
//
//    return containerCount() - 1;
//  }
//
  return -1;
}
