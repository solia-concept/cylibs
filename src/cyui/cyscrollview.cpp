/***************************************************************************
                          cyscrollview.cpp  -  description
                             -------------------
    début                  : dim aoû 10 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS

#include "cyscrollview.h"

CYScrollView::CYScrollView(QWidget *parent, const QString &name )
  : QScrollArea(parent)
{
  setObjectName(name);

//   if (parent->children())
//   {
//     QObjectList *l = (QObjectList*)parent->children();
//     for (uint i=0; i < l->count(); i++)
//     {
//       if (l->at(i)->inherits("QWidget"))
//       {
//         QWidget *wdg = (QWidget *)l->at(i);
// //         addChild(wdg);
//           setMaximumWidth(wdg->maximumWidth()+5);
//           setMaximumHeight(wdg->maximumHeight()+5);
//           break;
//       }
//     }
//   }
}

CYScrollView::~CYScrollView()
{
}
