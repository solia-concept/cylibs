//
// C++ Interface: cyaddimagedialog
//
// Description:
//
//
// Author: Gérald LE CLEACH <glc@solia-concept.fr>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYIMAGELISTDIALOG_H
#define CYIMAGELISTDIALOG_H

// QT
#include <QTreeWidget>
#include <QPushButton>
// CYLIBS
#include "cydialog.h"

/**
@short Boîte de dialogue d'édition de liste d'image.

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYImageListDialog : public CYDialog
{
  Q_OBJECT

public:
    CYImageListDialog(QWidget *parent=0, const QString &name="cySettingsDialog", bool modal=false, Qt::WindowFlags f=Qt::WindowFlags());

    ~CYImageListDialog();

    /** Charge une feuille d'analyses à partir d'un fichier. */
    bool load(const QString &fN);
    /** Enregistre une feuille d'analyses dans un fichier. */
    bool save(const QString &fN);

public slots:
  virtual void setModifieView();
  virtual void setValidateView();
    virtual void add();

protected:
  QPushButton  * mButtonApply;
  QPushButton  * mButtonCancel;
  QTreeWidget  * mListView;

protected: // Protected methods
  virtual void init();


};
#endif
