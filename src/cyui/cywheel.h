/***************************************************************************
                          cywheel.h  -  description
                             -------------------
    begin                : jeu fév 19 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYWHEEL_H
#define CYWHEEL_H

// QT
#include <qnamespace.h> 
// QWT
#include "qwt_wheel.h"
//Added by qt3to4:
#include <QString>
#include <QShowEvent>
// CYLIBS
#include "cydatawdg.h"

/** CYWheel est une molette de saisie d'une donnée numérique.
  * @short Molette de saisie d'une donnée numérique.
  * @author Gérald LE CLEACH
  */

class CYWheel : public QwtWheel, public CYDataWdg
{
  Q_OBJECT
  Q_ENUMS(Qt::Orientation)
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool readOnly READ readOnly WRITE setReadOnly)
  Q_PROPERTY(QByteArray stepDataName READ stepDataName WRITE setStepDataName)
  Q_PROPERTY(Qt::Orientation orientation READ orientation WRITE setOrientation)
  Q_PROPERTY(double mass READ mass WRITE setMass)
  Q_PROPERTY(bool direct READ direct WRITE setDirect)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:
  /** Construit une molette de saisie d'une donnée numérique. */
  CYWheel(QWidget *parent=0, const QString &name="wheel");

  ~CYWheel();

public: // Public methods
  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return \a true si le widget est en lecture seule. */
  virtual bool readOnly() const;
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);

  /** @return le nom de la donnée donnant le pas d'incrémentation. */
  virtual QByteArray stepDataName() const { return mStepDataName; }
  /** Saisie le nom de la donnée donnant le pas d'incrémentation. */
  virtual void setStepDataName(const QByteArray name) { mStepDataName = name; }

  /** @return l'orientation. */
  virtual Qt::Orientation orientation() const { return QwtWheel::orientation(); }
  /** Saisie l'orientation. */
  virtual void setOrientation(const Qt::Orientation val) { QwtWheel::setOrientation(val); }

  /** @return la masse du glissement. */
  virtual double mass() const { return QwtWheel::mass(); }
  /** Saisie la masse du glissement. */
  virtual void setMass(double val) { QwtWheel::setMass(val); }

  /** @return \a true si la donnée doit être directement mise à jour à chaque changement de valeur. */
  virtual bool direct() const { return mDirect; }
  /** Saisir \a true si la donnée doit être directement mise à jour à chaque changement de valeur. */
  virtual void setDirect(bool val) { mDirect = val; }

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** @return la valeur courrante. */
  double value() const;
  /** Saisie la valeur courante. Si \a direct() vaut \a true la donnée est directement
    * mise à jour par cette valeur. */
  void setValue(double value);

  /** Contrôle le flag. */
  virtual bool ctrlFlag();

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

signals: // Signals
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();

public slots: // Public slots
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

  /** Configure le widget en fonction de la donnée à laquelle il est lié. */
  virtual void linkData();

protected: // Protected methods
  /** Fonction appelée à l'affichage du widget. */
  virtual void showEvent(QShowEvent *e);
  /** Gestion du clic de souris. */
  virtual void mousePressEvent(QMouseEvent *e);

protected: // Protected attributes
  /** Nom de la donnée donnant le pas d'incrémentation. */
  QByteArray mStepDataName;
  /** Vaut \a true si la donnée doit être directement mise à jour à chaque changement de valeur. */
  bool mDirect;
};

#endif
