/***************************************************************************
                          cywheel.cpp  -  description
                             -------------------
    begin                : jeu fév 19 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cywheel.h"

// QT
#include <qapplication.h>
// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cymessagebox.h"
#include "cyreadonlyfilter.h"

CYWheel::CYWheel(QWidget *parent, const QString &name)
  : QwtWheel(parent),
    CYDataWdg(this)
{
  setObjectName(name);

  mWriteData = true;
  mDirect    = true;
  connect(this, SIGNAL(valueChanged(double)), SLOT(setValue(double)));
}

CYWheel::~CYWheel()
{
}

void CYWheel::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYWheel::dataName() const
{
  return CYDataWdg::dataName();
}

void CYWheel::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYWheel::flagName() const
{
  return CYDataWdg::flagName();
}

void CYWheel::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYWheel::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYWheel::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYWheel::benchType() const
{
  return CYDataWdg::benchType();
}

void CYWheel::setInverseFlag(bool inverse)
{
  mInverseFlag = inverse;
  ctrlFlag();
}

bool CYWheel::inverseFlag() const
{
  return mInverseFlag;
}

void CYWheel::setInverseHideFlag(const bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYWheel::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

bool CYWheel::readOnly() const
{
  return mReadOnly;
}

void CYWheel::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }

  ctrlFlag();
}

double CYWheel::value() const
{
  return QwtWheel::value();
}

void CYWheel::setValue(double value)
{
  QwtWheel::setValue(value);

  if (mDirect)
  {
    update();
    if (mData && isEnabled())
      mData->writeData();
  }
  else if (mEmitModifie && isVisible())
    emit modifie();
}

//double CYWheel::minValue() const
//{
//  return d->mapToDouble(QSpinBox::minValue());
//}
//
//void CYWheel::setMinValue(double value)
//{
//  bool ok = false;
//  int min = d->mapToInt(value, &ok);
//  if (!ok)
//    return;
//  QSpinBox::setMinValue(min);
//  updateValidator();
//}
//
//double CYWheel::maxValue() const
//{
//  return d->mapToDouble(QSpinBox::maxValue());
//}
//
//void CYWheel::setMaxValue(double value)
//{
//  bool ok = false;
//  int max = d->mapToInt(value, &ok);
//  if (!ok)
//    return;
//  QSpinBox::setMaxValue(max);
//  updateValidator();
//}
//
//double CYWheel::linesingleStep() const
//{
//  return d->mapToDouble(QSpinBox::linesingleStep());
//}
//
//void CYWheel::setLineStep(double step)
//{
//  bool ok = false;
//
//  if (!mStepDataName.isEmpty())
//  {
//    CYF64 *d = (CYF64 *)core->findData(mStepDataName);
//    step = d->val();
//  }
//
//  if (step > (maxValue() - minValue()))
//    QSpinBox::setLineStep(1);
//  else
//    QSpinBox::setLineStep(kMax(d->mapToInt(step, &ok), 1));
//}

void CYWheel::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYWheel::linkData()
{
  if (!hasData())
    return;

  if (mData==0)
  {
    setEnabled(false);
    return;
  }

  mEmitModifie = false;

  if (!mStepDataName.isEmpty())
  {
    CYF64 *step = (CYF64 *)core->findData(mStepDataName);
    // TOCHECK QWT
    if (step)
      setSingleStep(step->val());
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      // TOCHECK QWT
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      // TOCHECK QWT
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8 *data = (CYU8  *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      data->setTmp(data->val());
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setRange(data->min(), data->max());
                      setValue(data->val());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));

  mEmitModifie = true;
}

void CYWheel::refresh()
{
  ctrlHideFlag();

  if (!ctrlFlag())
    return;

  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    return;
  }
  if (!mData->isOk() && !mAlwaysOk)
  {
    setEnabled(false);
    return;
  }
  if (mData->flag()!=0)
  {
    if (!mData->flag()->val())
    {
      setEnabled(false);
      return;
    }
  }
//
//  switch (mData->type())
//  {
//    case Cy::VFL    :
//                    {
//                      CYFlag *data = (CYFlag *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VS8    :
//                    {
//                      CYS8 *data = (CYS8 *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VS16   :
//                    {
//                      CYS16 *data = (CYS16 *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VS32   :
//                    {
//                      CYS32 *data = (CYS32 *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VS64   :
//                    {
//                      CYS64 *data = (CYS64 *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VU8    :
//                    {
//                      CYU8 *data = (CYU8  *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VU16   :
//                    {
//                      CYU16 *data = (CYU16 *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VU32   :
//                    {
//                      CYU32 *data = (CYU32 *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VU64   :
//                    {
//                      CYU64 *data = (CYU64 *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VF32   :
//                    {
//                      CYF32 *data = (CYF32 *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::VF64   :
//                    {
//                      CYF64 *data = (CYF64 *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::Time   :
//                    {
//                      CYTime *data = (CYTime *)mData;
//                      data->setTmp(data->CYU32::val());
//                      setValue(data->def());
//                      break;
//                    }
//    case Cy::Sec    :
//                    {
//                      CYTSec *data = (CYTSec *)mData;
//                      setValue(data->def());
//                      break;
//                    }
//    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName())
//  }
}

void CYWheel::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    QwtWheel::setEnabled(false);
    return;
  }

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8 *data = (CYU8  *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      data->setTmp(data->val());
                      setValue(data->def());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      setValue(data->def());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYWheel::update()
{
  if (!hasData())
    return;

  if (!isVisible() && mUpdateIfVisible)
    return;

  if ((mData==0) || (!isEnabled()))
    return;

  if (isHidden())
    return;

  mEmitModifie = false;

  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      data->setVal((flg)value());
                      break;
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      data->setVal((s8)value());
                      break;
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      data->setVal((s16)value());
                      break;
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      data->setVal((s32)value());
                      break;
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      data->setVal((s64)value());
                      break;
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      data->setVal((u8)value());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      data->setVal((u8)value());
                      break;
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      data->setVal((u32)value());
                      break;
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      data->setVal((u64)value());
                      break;
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      data->setValSection((int)value());
                      data->setVal((u32)data->tmp());
                      break;
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      data->setVal(value());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  mEmitModifie = true;
}

bool CYWheel::ctrlFlag()
{
  if (mFlag==0)
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;

  if (!res)
    designer();

  setEnabled(res);
  return res;
}

void CYWheel::showEvent(QShowEvent *e)
{
  QwtWheel::showEvent(e);
}

void CYWheel::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYWheel::mousePressEvent(QMouseEvent *e)
{
  if ( readOnly() )
  {
    e->ignore();
    return;
  }
  QwtWheel::mousePressEvent(e);
}
