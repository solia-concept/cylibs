//
// C++ Interface: cyimagelist
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYIMAGELIST_H
#define CYIMAGELIST_H

#include <QObject>
#include <QList>
#include <QPixmap>
// CYLIBS
#include "cyimage.h"

/**
	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYImageList : public QObject
{
Q_OBJECT
public:
    CYImageList(QObject *parent = 0, const QString &name = 0);

    ~CYImageList();

    virtual bool load(const QString &fileName);
    virtual bool save(const QString &fileName);

    /** Ajoute une nouvelle image. */
    void addImage(CYImage *image);
    /** Supprime une image. */
    void removeImage(CYImage *image);

    CYImage *addImage(int index, QString label, QString fileName);

    /** Liste des images qu'elle regroupe. */
    QList<CYImage*> images;

    CYImage * image(uint index);
    QPixmap pixmap(uint index);

protected: // Protected attributes

    QString mFileName;
    /** Version de CYLIBS lors de l'enregistrement du fichier de sauvegarde */
    QString mVersionCYDOM;
public slots:
    void clear();
};

#endif
