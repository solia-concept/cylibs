#include "cynumdialog.h"
#include "ui_cynumdialog.h"

CYNumDialog::CYNumDialog(QWidget *parent, const QString &name)
  : CYDialog(parent, name), ui(new Ui::CYNumDialog)
{
  ui->setupUi(this);
}


CYNumDialog::~CYNumDialog()
{
  delete ui;
}

void CYNumDialog::setDataName(const QByteArray &name)
{
  ui->numInput->setDataName(name);
  ui->textLabel->setDataName(name);
}
