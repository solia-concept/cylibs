/***************************************************************************
                          cyrobottopviewarm.cpp  -  description
                             -------------------
    begin                : lun nov 1 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyrobottopviewarm.h"

// ANSI
#include <math.h>
// QT
#include <QPainter>
#include <QPolygon>
// QWT
#include "qwt_math.h"
#include "qwt_painter.h"
#include "qwt_dial_needle.h"
#include "qwt_point_polar.h"

CYRobotTopViewArm::CYRobotTopViewArm()
  : QwtDialSimpleNeedle(Ray) // TOCHECK QT4 QwtDialNeedle()
{
  mPercentStroke1 = 0.3;
  mPercentStroke2 = 0.6;
  mPercentStroke  = 0.7;
}

CYRobotTopViewArm::~CYRobotTopViewArm()
{
}

void CYRobotTopViewArm::draw(QPainter *painter, const QPoint &center, int length, double direction, QPalette::ColorGroup cg) const
{
  int width = 16;
  if ( width <= 0 )
    width = 5;

  direction *= M_PI / 180.0;

  painter->save();
  painter->setPen(Qt::NoPen);
  //    painter->setBrush(colorGroup(cg).brush(QPalette::Mid));
  painter->setBrush(QColor(240, 130, 10));

  QPoint p1, p2, p3, p4;
  if ( width == 1 )
  {
    //    painter->setPen(QPen(colorGroup(cg).mid(), 1));
    //    painter->drawLine(p1, p2);
  }
  else
  {
    QPolygon po1(4);
    p1 = QPoint(center.x() + 1, center.y() + 2);
    p2 = qwtPolar2Pos(p1, length*0.3, direction);
    po1.setPoint( 0, qwtPolar2Pos(p1, width / 1, direction + M_PI_2));
    po1.setPoint( 1, qwtPolar2Pos(p2, width / 1, direction + M_PI_2));
    po1.setPoint( 2, qwtPolar2Pos(p2, width / 1, direction - M_PI_2));
    po1.setPoint( 3, qwtPolar2Pos(p1, width / 1, direction - M_PI_2));
    painter->drawPolygon(po1);

    if ( mPercentStroke > mPercentStroke1 )
    {
      QPolygon po2(4);
      if ( mPercentStroke > mPercentStroke2 )
        p3 = qwtPolar2Pos(p2, length*(mPercentStroke2-mPercentStroke1), direction);
      else
        p3 = qwtPolar2Pos(p2, length*(mPercentStroke-mPercentStroke1), direction);
      po2.setPoint( 0, qwtPolar2Pos(p2, width / 2, direction + M_PI_2));
      po2.setPoint( 1, qwtPolar2Pos(p3, width / 2, direction + M_PI_2));
      po2.setPoint( 2, qwtPolar2Pos(p3, width / 2, direction - M_PI_2));
      po2.setPoint( 3, qwtPolar2Pos(p2, width / 2, direction - M_PI_2));
      painter->drawPolygon(po2);
    }

    if ( mPercentStroke > mPercentStroke2 )
    {
      QPolygon po3(4);
      p4 = qwtPolar2Pos(p3, length*(mPercentStroke-mPercentStroke2), direction);
      po3.setPoint( 0, qwtPolar2Pos(p3, width / 4, direction + M_PI_2));
      po3.setPoint( 1, qwtPolar2Pos(p4, width / 4, direction + M_PI_2));
      po3.setPoint( 2, qwtPolar2Pos(p4, width / 4, direction - M_PI_2));
      po3.setPoint( 3, qwtPolar2Pos(p3, width / 4, direction - M_PI_2));
      painter->drawPolygon(po3);
    }
  }
  //  if ( hasKnob )
  //  {
  int knobWidth = qMax(qRound(width * 0.7), 5);
  if ( knobWidth % 2 == 0 )
    knobWidth++;

  // TOCHECK QWT
  //      drawKnob(painter, center, knobWidth, colorGroup(cg).brush(QPalette::Base), false);
  drawKnob(painter, knobWidth, palette().brush(cg,QPalette::Base), false);
  //  }

  painter->restore();
}

/*!
    \param cg Identifier of the color group
    \return The color group of the palette.
*/

//const QColorGroup &CYRobotTopViewArm::colorGroup(QPalette::ColorGroup cg) const
//{
//    switch(cg)
//    {
//        case QPalette::Disabled:
//            return palette().disabled();
//        case QPalette::Inactive:
//            return palette().inactive();
//        default:
//            return palette().active();
//    }
//}
