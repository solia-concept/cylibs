/***************************************************************************
                          cyled.h  -  description
                             -------------------
    début                  : mar jun 17 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYLED_H
#define CYLED_H

// CYLIBS
#include "cydatawdg.h"

/**Led pouvant afficher l'état d'une donnée.
  *@author Gérald LE CLEACH
  */

class CYLed : public QWidget, public CYDataWdg
{
  Q_OBJECT
  Q_ENUMS( State Shape Look )
  Q_PROPERTY(bool movable READ movable WRITE setMovable)
  Q_PROPERTY( State state READ state WRITE setState )
  Q_PROPERTY( Shape shape READ shape WRITE setShape )
  Q_PROPERTY( Look look READ look WRITE setLook )
  Q_PROPERTY( QColor color READ color WRITE setColor )
  Q_PROPERTY( int darkFactor READ darkFactor WRITE setDarkFactor )
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray posXName READ posXName WRITE setPosXName)
  Q_PROPERTY(QByteArray posYName READ posYName WRITE setPosYName)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(int genericMarkOffset READ genericMarkOffset WRITE setGenericMarkOffset)
  Q_PROPERTY(bool bicolor READ bicolor WRITE setBicolor)
  Q_PROPERTY(QColor colorOn READ colorOn WRITE setColorOn)
  Q_PROPERTY(QColor colorOff READ colorOff WRITE setColorOff)
  Q_PROPERTY(bool enableState READ enableState WRITE setEnableState)
  Q_PROPERTY(bool flashing READ flashing WRITE setFlashing)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:

  /**
   * Status of the light is on/off.
   * @short LED on/off.
   */
  enum State { Off, On };

  /**
   * Shades of the lamp.
   * @short LED shape
   */
  enum Shape { Rectangular, Circular };

  /**
   * Displays a flat, round or sunken LED.
   *
   * Displaying the LED flat is less time and color consuming,
   * but not so nice to see.
   *
   * The sunken LED itself is (certainly) smaller than the round LED
   * because of the 3 shading circles and is
   * most time consuming. Makes sense for LED > 15x15 pixels.
   *
   * \b Timings: \n
   * ( AMD K5/133, Diamond Stealth 64 PCI Graphics, widgetsize 29x29 )
   *  @li flat Approximately 0.7 msec per paint
   *  @li round Approximately 2.9 msec per paint
   *  @li sunken Approximately 3.3 msec per paint
   *
   * The widget will be updated on the next repaining event.
   *
   * @short LED look.
   */
  enum Look  { Flat, Raised, Sunken };

  /**
   * Constructs a green, round LED widget which will initially
   * be turned on.
   */
  CYLed(QWidget *parent=0, const QString &name=0);
  /**
   * Constructor with the ledcolor, the parent widget, and the name.
   *
   * The State will be defaulted On and the Look round.
   *
   * @param ledcolor Initial color of the LED.
   * @param parent   Will be handed over to QWidget.
   * @param name     Will be handed over to QWidget.
   * @short Constructor
   */
  CYLed(const QColor &col=Qt::green, QWidget *parent=0, const QString &name=0);

  /**
   * Constructor with the ledcolor, ledstate, ledlook,
   * the parent widget, and the name.
   *
   * Differs from above only in the parameters, which configure all settings.
   *
   * @param ledcolor Initial color of the LED.
   * @param state    Sets the State.
   * @param look     Sets the Look.
   * @param parent   Will be handed over to QWidget.
   * @param name     Will be handed over to QWidget.
   * @short Constructor
   */
  CYLed(const QColor& col, CYLed::State st, CYLed::Look look, CYLed::Shape shape, QWidget *parent=0, const QString &name=0);

  ~CYLed();


  /** @return \a true si l'objet peut être déplacé.
    * @see void setMovable(const bool val). */
  virtual bool movable() const;
  /** Saisie \a true pour pouvoir déplacer. */
  virtual void setMovable(const bool val);

  /**
   * Returns the current state of the widget (on/off).
   *
   * @see State
   * @short Returns LED state.
   */
  State state() const;

  Shape shape() const;

  /**
   * Returns the color of the widget
   *
   * @see Color
   * @short Returns LED color.
   */
  QColor color() const;

  /**
   * Returns the look of the widget.
   *
   * @see Look
   * @short Returns LED look.
   */
  Look look() const;

  /**
   * Returns the factor to darken the LED.
   *
   * @see setDarkFactor()
   * @short Returns dark factor
   */
  int darkFactor() const;

  /**
   * Sets the state of the widget to On or Off.
   *
   * The widget will be painted immediately.
   * @see on() off() toggle() toggleState()
   *
   * @param state The LED state: on or off.
   * @short Set LED state.
   */
  void setState( State state );

  /**
   * Set the shape of the LED to @p s.
   */
  void setShape(Shape s);


  /**
   * Set the color of the widget.
   * The Color is shown with the CYLed::On state.
   * The CYLed::Off state is shown with QColor.darker() method
   *
   * The widget calls the update() method, so it will
   * be updated when entering the main event loop.
   *
   * @see Color
   *
   * @param color New color of the LED.
   * @short Sets the LED color.
   */
  void setColor(const QColor& color);

  /**
   * Sets the factor to darken the LED in OFF state.
   * Same as QColor::darker().
   * "darkfactor should be greater than 100, else the LED gets lighter
   * in OFF state.
   * Defaults to 300.
   *
   * @see QColor
   *
   * @param darkfactor sets the factor to darken the LED.
   * @short sets the factor to darken the LED.
   */
  void setDarkFactor(int darkfactor);

  /**
   * Sets the color of the widget.
   * The Color is shown with the CYLed::On state.
   * darkcolor is explicidly used for the off state of the LED.
   * Normally you don't have to use this method, the setColor(const QColor& color) is sufficient for the task.
   *
   * The widget calls the update() method, so it will
   * be updated when entering the main event loop.
   *
   * @see Color setColor()
   *
   * @param color New color of the LED used for on state.
   * @param darkcolor Dark color of the LED used for off state.
   * @short Sets the light and dark LED color.
   *
  void setColor(const QColor& color, const QColor& darkcolor);
  */

  /**
   * Sets the look of the widget.
   *
   * The look may be flat, round or sunken.
   * The widget calls the update() method, so it will
   * be updated when entering the main event loop.
   *
   * @see Look
   *
   * @param look New look of the LED.
   * @short Sets LED look.
   */
  void setLook( Look look );

  virtual QSize sizeHint() const;
  virtual QSize minimumSizeHint() const;

public: // Public methods
  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le nom de la donnée de positionnement en X. */
  virtual QByteArray posXName() const;
  /** Saisie le nom de la donnée de positionnement en X. */
  virtual void setPosXName(const QByteArray &name);

  /** @return le nom de la donnée de positionnement en Y. */
  virtual QByteArray posYName() const;
  /** Saisie le nom de la donnée de positionnement en Y. */
  virtual void setPosYName(const QByteArray &name);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return l'offset sur le repère de généricité lorqu'il s'agit d'un numéro. */
  virtual int genericMarkOffset() const;
  /** Saisie l'offset sur le repère de généricité lorqu'il s'agit d'un numéro. */
  virtual void setGenericMarkOffset(const int val);

  /** @return \a true si le mode bi-couleur est actif, sinon c'est le paramètre de contraste qui différencie l'état. */
  virtual bool bicolor() const { return mBicolor; }
  /** Saisir \a true pour rendre le mode bi-couleur actif, sinon c'est le paramètre de contraste qui différencie l'état. */
  virtual void setBicolor(const bool mode)  { mBicolor = mode; }

  /** @return la couleur de l'état \a true en mode bi-couleur, ou couleur de la led en mode normal. */
  virtual QColor colorOn() const { return mColorOn; }
  /** Saisir la couleur de l'état \a true en mode bi-couleur, ou couleur de la led en mode normal. */
  virtual void setColorOn(const QColor color)  { mColorOn = color; }

  /** @return la couleur de l'état \a false en mode bi-couleur. */
  virtual QColor colorOff() const { return mColorOff; }  /** @return \a true si la led est en mode clignotement. */
  virtual bool flashing() const { return mFlashing; }
  /** Saisir la couleur de l'état \a false en mode bi-couleur. */
  virtual void setColorOff(const QColor color)  { mColorOff = color; }

  /** @return \a true pour un état actif à 1. */
  virtual bool enableState() const { return mEnableState; }
  /** Saisir \a true pour un état actif à 1. */
  virtual void setEnableState(const bool mode)  { mEnableState = mode; }


  /** Saisir \a true pour rendre le mode clignotement actif. */
  virtual void setFlashing(const bool mode)  { mFlashing = mode; }

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** @return la valeur courante. */
  bool value();

  virtual void moveInsideParent(int x, int y);
  virtual void setEditPositions( bool val );

signals: // Signals
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis à chaque changement d'état de la led. */
  void stateChanged(bool);
  /** Emis à chaque déplacement à la souris afin de définir un ordre dans la pile de widget du parent. */
  void movedInsideParent();

public slots: // Public slots
  /**
   * Toggles the state of the led from Off to On or vice versa.
   *
   * The widget repaints itself immediately.
   */
  void toggle();

  /**
   * Toggle the state of the LED from Off to On and vice versa.
   *
   * The widget will be repainted when returning to the main
   * event loop.
   * @short Toggles LED on->off / off->on.
   * @deprecated, use #toggle() instead.
   */
  void toggleState();

  /**
   * Sets the state of the widget to On.
   *
   * The widget will be painted immediately.
   * @see off() toggle() toggleState() setState()
   */
  void on();

  /**
   * Sets the state of the widget to Off.
   *
   * The widget will be painted immediately.
   * @see on() toggle() toggleState() setState()
   */
  void off();

  /** Place \a data comme donnée du widget. */
  void setData(CYData *data);

  /** Place la valeur du contrôleur. */
  void setValue(bool);
  /** Place la valeur du contrôleur. */
  void setValue(int);

  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  virtual void refreshPosition();
  virtual void updatePosition();

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();

protected:
  /**
   * Paints a circular, flat LED.
   */
  virtual void paintFlat();
  /**
   * Paints a circular, raised LED.
   */
  virtual void paintRound();
  /**
   * Paints a circular, sunken LED.
   */
  virtual void paintSunken();
  /**
   * Paints a rectangular, flat LED.
   */
  virtual void paintRect();
  /**
   * Paints a rectangular LED, either raised or
   * sunken, depending on its argument.
   */
  virtual void paintRectFrame(bool raised);

  void paintEvent( QPaintEvent * );

  /**
   * Compute LED width
   */
  int ensureRoundLed();

  /**
   * Paint the cached antialiased pixmap corresponding to the state if any
   * @return true if the pixmap was painted, false if it hasn't been created yet
   */
  bool paintCachedPixmap();

  /** Un double-clic permet en mode simulation de changer la valeur de la donnée visualisée */
  virtual void mouseDoubleClickEvent ( QMouseEvent * e );

  virtual void mousePressEvent ( QMouseEvent * e );
  virtual void mouseMoveEvent ( QMouseEvent * e );

  /** Vaut \a true si le mode bi-couleur est actif, sinon c'est le paramètre de contraste qui différencie l'état. */
  bool mBicolor;
  /** Couleur de l'état \a true en mode bi-couleur, ou couleur de la led en mode normal. */
  QColor mColorOn;
  /** Couleur de l'état \a false en mode bi-couleur. */
  QColor mColorOff;
  /** Vaut \a true pour un état actif à 1. */
  bool mEnableState;

  /** Vaut \a true si la led est en mode clignotement. */
  bool mFlashing;
  bool mFlashState;

private:
  void init(bool value=false);

  State led_state;
  QColor led_color;
  Look  led_look;
  Shape led_shape;

  bool mValue;

private:
  class CYLedPrivate;
  CYLedPrivate *d;
};

#endif
