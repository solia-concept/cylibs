//
// C++ Implementation: cyheater
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <qapplication.h>
//Added by qt3to4:
#include <QString>
// CYLIBS
#include "cyheater.h"

CYHeater::CYHeater(QWidget *parent, const QString &name)
 : CYPixmapView(parent, name)
{
  setPixmapFileTrue(":/pixmaps/cyheater_on.png");
  setPixmapFileFalse(":/pixmaps/cyheater_off.png");
}


CYHeater::~CYHeater()
{
}
