#include "cytextdialog.h"
#include "ui_cytextdialog.h"

CYTextDialog::CYTextDialog(QWidget *parent, const QString &name)
  : CYDialog(parent, name), ui(new Ui::CYTextDialog)
{
  ui->setupUi(this);
}


CYTextDialog::~CYTextDialog()
{
  delete ui;
}

void CYTextDialog::setDataName(const QByteArray &name)
{
  ui->textInput->setDataName(name);
  ui->textLabel->setDataName(name);
}

void CYTextDialog::hideApplyButton(bool hide)
{
  if (hide)
    ui->applyButton->hide();
  else
    ui->applyButton->show();
}
