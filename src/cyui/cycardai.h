/***************************************************************************
                          cycardai.h  -  description
                             -------------------
    début                  : jeu fév 13 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCARDAI_H
#define CYCARDAI_H

// QT
#include <QFrame>

/**
 * CYCardAI représente les cartes électriques d'entrées ANA.
 * @see CYCardIO, CYCardO, CYCardAI, CYCardAI
 * @short Carte d'entrées ANA.
 * @author Gérald LE CLEACH
 */

class CYCardAI : public QFrame
{
  Q_OBJECT

public:
  /**
   * Constructeur
   */
  CYCardAI(QWidget *parent=0, const QString &name=0);

  /**
   * Destructeur
   */
  ~CYCardAI();

private:
  /** Initialise le widget selon les paramètres de la carte. */
  virtual void init();
  /** Met à jour les widgets d'E/S. */
  virtual void updateCard();
};

#endif
