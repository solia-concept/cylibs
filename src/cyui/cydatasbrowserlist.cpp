/***************************************************************************
                          cydatasbrowserlist.cpp  -  description
                             -------------------
    begin                : lun nov 22 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cydatasbrowserlist.h"

// ANSI
#include <assert.h>
// QT
#include <QDrag>
#include <QToolTip>
#include <QMimeData>
#include <QHeaderView>
// CYLIBS
#include "cycore.h"
#include "cydb.h"
#include "cydata.h"
#include "cynetlink.h"
#include "cynethost.h"
#include "cytype.h"
#include "cymessagebox.h"

CYDatasBrowserListItem::CYDatasBrowserListItem(QObject *parent, QTreeWidgetItem *item0, CYData *data)
  : QObject(parent)
{
  setObjectName(data->objectName());
  mData = data;
  QString label=data->label();
  label.remove("\n");
  QStringList strings;
  strings<<label<<data->physicalType()<<data->objectName();
  mItem = new QTreeWidgetItem(item0, strings);
  // TODO QT5 trop lent mItem->setIcon(0, QIcon(":/icons/cyhwinfo.png"));
}


CYDatasBrowserListItem::~CYDatasBrowserListItem()
{
}

void CYDatasBrowserListItem::refresh()
{
  if (mItem)
  {
    mItem->setText(0, mData->label());
    mItem->setText(1, mData->physicalType());
    mItem->setText(2, mData->objectName());
  }
}

CYDatasBrowserList::CYDatasBrowserList(QWidget *parent, const QString &name)
  : QTreeWidget(parent)
{
  setObjectName(name);
  if (!core)
    return;

  mOnlyHost = 0;
  mOnlyLink = 0;

  mFilter = CYDatasBrowser::User;
  mSearchData ="";

  setDragEnabled(true);

  connect(this, SIGNAL(currentItemChanged(QTreeWidgetItem *,QTreeWidgetItem*)), this, SLOT(newItemSelected(QTreeWidgetItem *,QTreeWidgetItem*)));

  QStringList labels;
  labels<<tr("Data Browser");
  labels<<tr("Physical Type");
  if (core->designer())
  {
    labels<<tr("Filter");
    labels<<tr("Host");
    labels<<tr("Link");
    labels<<tr("Group");
  }
  setHeaderLabels(labels);

  this->setToolTip(tr("Drag datas to empty cells of a analyse sheet."));

  setRootIsDecorated(true);

  // L'explorateur de données peut être complètement caché.
  setMinimumWidth(1);

  this->setToolTip( "<p>"+
                    tr("The data browser lists the connected hosts and the datas that they provide. "
                       "Click and drag datas into drop zones of a analyse sheet. "
                       "A display will appear that visualizes the values provided by the data. "
                       "Some data displays can display values of multiple datas. "
                       "Simply drag other datas on to the display to add more datas.")
                    +"</p>" );

  CYNetHost *host;
  QHashIterator<QString, CYNetHost *> it(core->hostList);
  it.toFront();
  while (it.hasNext())
  {
    it.next();                   // must come first
    host=it.value();
    init(host);
  }

  header()->setSectionResizeMode(QHeaderView::ResizeToContents);
  header()->setSectionHidden(1, true);

  sortByColumn(0, Qt::AscendingOrder);
  //  refresh();
  //  QTimer *timer = new QTimer(this);
  //  timer->start(1000);
  //  connect(timer, SIGNAL(timeout()), SLOT(refresh()));
}

CYDatasBrowserList::~CYDatasBrowserList()
{
}

void CYDatasBrowserList::mousePressEvent(QMouseEvent *ev)
{
  QTreeWidget::mousePressEvent(ev);

  if (ev->button() == Qt::LeftButton)
  {
    QTreeWidgetItem *item = itemAt(ev->pos());

    if (!item)
      return;

    if (item->childCount())
    {
      dragStartPosition = ev->pos();
      return;
    }

    if (item->text(2).isNull())
      return;   // aucun élément sous le curseur

    if ((!item->child(0)) && (core))
    {
      dragStartPosition = ev->pos();
      return;
    }
  }
}

void CYDatasBrowserList::mouseMoveEvent(QMouseEvent *ev)
{
  /* setMouseTracking(false) semble être cassé. Avec Qt actuel, trainer la
      souris ne peut être mis à l'arrêt. Nous devons donc vérifier pour chaque
      évènement que le LBM puisse être pressé. */
  if (!(ev->buttons() & Qt::LeftButton))
    return;

  if ((ev->pos() - dragStartPosition).manhattanLength()
      < QApplication::startDragDistance())
    return;

  QTreeWidgetItem *item = itemAt(dragStartPosition);

  if (!item)
    return;

  if (item->childCount())
  {
    QString filter = tr("Filter")+": "+item->text(2)+" ";
    QString host   = tr("Host")  +": "+item->text(3)+" ";
    QString link   = tr("Link")  +": "+item->text(4)+" ";
    QString group  = tr("Group") +": "+item->text(5);
    int nb = item->childCount();
    int cpt = 0;
    QString subGroup = "";
    for (cpt=0; cpt<nb; cpt++)
    {
      QTreeWidgetItem *child=item->child(cpt);
      if (child->childCount())
        subGroup = ":"; // ":" indique qu'il y a au moins un sous-groupe
    }

    QDrag *drag = new QDrag(this);
    QMimeData *mimeData = new QMimeData;
    mimeData->setText(filter+host+link+group+subGroup);
    drag->setMimeData(mimeData);
    drag->exec(Qt::CopyAction | Qt::MoveAction);
    return;
  }

  if (item->text(2).isNull())
    return;   // aucun élément sous le curseur

  if ((!item->child(0)) && (core))
  {
    CYData *data = core->findData(item->text(2));
    // Construit l'objet texte trainé ainsi: "<dataname>".
    // TOCHECK QT5
    //    Q3DragObject *dObj = new Q3TextDrag(data->objectName(), this);
    //    Q_CHECK_PTR(dObj);
    //    dObj->dragCopy();
    QDrag *drag = new QDrag(this);
    QMimeData *mimeData = new QMimeData;
    mimeData->setText(data->objectName());
    drag->setMimeData(mimeData);
    drag->exec(Qt::CopyAction | Qt::MoveAction);
  }
  else
    return;
}

void CYDatasBrowserList::init(CYNetHost *host)
{
  mHLVI = 0;
  CYNetLink *link;
  QHashIterator<QString, CYNetLink *> it(host->linkList);
  it.toFront();
  while (it.hasNext())
  {
    it.next();                   // must come first
    link=it.value();
    init(host, link);
  }
}

void CYDatasBrowserList::init(CYNetHost *host, CYNetLink *link, bool only)
{
  if (only)
  {
    mOnlyHost = host;
    mOnlyLink = link;
    mHLVI = 0;
  }

  mLLVI = 0;
  CYDB *db;
  QHashIterator<QString, CYDB *> it(link->dbList);
  it.toFront();
  while (it.hasNext())
  {
    it.next();                   // must come first
    db=it.value();
    if (db->host() == host)
      init(host, link, db);
  }
}

void CYDatasBrowserList::init(CYNetHost *host, CYNetLink *link, CYDB *db)
{
  CYData *data;
  QHashIterator<QString, CYData *> it(db->datas);
  QString currentDataSearch;
  it.toFront();
  while (it.hasNext())
  {
    it.next();                   // must come first
    data=it.value();
    currentDataSearch = data->dataName().toUpper();
    currentDataSearch.append(data->label().toUpper() + " ");
    currentDataSearch.append(data->group().toUpper() + " ");
    if ( (mSearchData=="" || currentDataSearch.contains(mSearchData)) &&
         ( ( (data->mode() == Cy::User) && (mFilter==CYDatasBrowser::User) ) ||
         ( (data->mode() != Cy::User) && (mFilter==CYDatasBrowser::Sys ) ) ||
         ( mFilter == CYDatasBrowser::All ) ))
    {
      Group groups(data->group(), '\n');
      if (!mHLVI)
      {
        mHLVI = new QTreeWidgetItem(this, QStringList(host->label()));
        Q_CHECK_PTR(mHLVI);
        if (core)
        {
          QPixmap hpix = core->loadIcon("network_local.png", 22);
          mHLVI->setIcon(0, QIcon(hpix));
        }
      }
      if (!mLLVI)
      {
        mLLVI = new QTreeWidgetItem(mHLVI, QStringList(link->label()));
        Q_CHECK_PTR(mLLVI);
        if (core)
        {
          QPixmap hpix = core->loadIcon("cyconnect_no.png", 22);
          mLLVI->setIcon(0, QIcon(hpix));
        }
      }
      if (!(mSearchData=="")){
        mHLVI->setExpanded(true);
        mLLVI->setExpanded(true);
      }
      QTreeWidgetItem *parent = mLLVI;
      for (uint i = 0; i < groups.number(); ++i)
      {
        if (groups[i].isEmpty())
          break;
        Group parts(groups[i], ':');
        for (uint j = 0; j < parts.number(); ++j)
        {
          if (parts[i].isEmpty())
            break;

          bool found = false;
          QString group = parts[j];
          // parcours les branches existantes
          if (parent->childCount())
          {
            QTreeWidgetItem *sibling = parent->child(0);

            // Vérifie que le groupe n'existe pas déjà.
            while (sibling && !found)
            {
              if (sibling->text(0) == group && !sibling->text(5).isEmpty())
              {
                found = true; // Le groupe existe déjà.
              }
              else
                sibling = parent->child(parent->indexOfChild(sibling)+1);
            }

            if (!found)
            {
              QString txt;
              if (parent->text(5).isEmpty())
                txt = group;
              else
                txt = parent->text(5)+":"+group;
              QStringList labels;
              labels<<group;
              labels<<0;
              labels<<QString("%1").arg((int)mFilter);
              labels<<mHLVI->text(0);
              labels<<mLLVI->text(0);
              labels<<txt;
              QTreeWidgetItem *mGLVI = new QTreeWidgetItem(parent, labels);
              Q_CHECK_PTR(mGLVI);
              parent = mGLVI;
              // TOCHECK QT5
              //              repaintItem(parent);
            }
            else
              parent = sibling;
          }
          // première branche
          else
          {
            QString txt;
            if (parent->text(5).isEmpty())
              txt = group;
            else
              txt = parent->text(5)+":"+group;
            QStringList labels;
            labels<<group;
            labels<<0;
            labels<<QString("%1").arg((int)mFilter);
            labels<<mHLVI->text(0);
            labels<<mLLVI->text(0);
            labels<<txt;
            QTreeWidgetItem *mGLVI = new QTreeWidgetItem(parent, labels);
            Q_CHECK_PTR(mGLVI);
            parent = mGLVI;
            // TOCHECK QT5
            //              repaintItem(parent);
          }
          if (!(mSearchData==""))
            parent->setExpanded(true);
        }
      }
      new CYDatasBrowserListItem(this, parent, data);
//      items.append(new CYDatasBrowserListItem(this, parent, data));
    }
  }

  // tri nouvelle arborescence
  sortByColumn(1, Qt::AscendingOrder);
  sortByColumn(0, Qt::AscendingOrder);
}

void CYDatasBrowserList::disconnectHost()
{
  //  QPtrListIterator<HostInfo> it(hostInfos);
  //
  //  for (; it.current(); ++it)
  //    if ((*it)->getLVI()->isSelected())
  //    {
  //      kdDebug() << "Disconnecting " <<  (*it)->getHostobjectName().toUtf8() << endl;
  //      dataManager->disengage((*it)->getHostAgent());
  //    }
}

void CYDatasBrowserList::refresh()
{
  CYNetHost *host;
  CYNetLink *link;

  QHashIterator<QString, CYNetHost *> it(core->hostList);
  it.toFront();
  while (it.hasNext())
  {
    it.next();                   // must come first
    host=it.value();
    mHLVI = findItems(host->objectName(), Qt::MatchExactly, 0).first();
    if (mHLVI)
    {
      QHashIterator<QString, CYNetLink *> it(host->linkList);
      it.toFront();
      while (it.hasNext())
      {
        it.next();                   // must come first
        link=it.value();
        mLLVI = mHLVI->child(0);
        while(mLLVI)
        {
          if (core && (mLLVI->text(0) == link->objectName()))
          {
            QPixmap pix;
            if (link->isOn())
              pix = core->loadIcon("cyconnect_established.png", 22);
            else
              pix = core->loadIcon("cyconnect_no.png", 22);
            mLLVI->setIcon(0, QIcon(pix));
          }
          mLLVI = mHLVI->child(mHLVI->indexOfChild(mLLVI)+1);
        }
      }
    }
  }

  setMouseTracking(false);
}

void CYDatasBrowserList::newItemSelected(QTreeWidgetItem *, QTreeWidgetItem *)
{
  if (parent() && parent()->parent() && parent()->parent()->inherits("CYAnalyseWin"))
    CYMessageBox::information(this, tr("Drag datas to empty fields in a work sheet"));
}

void CYDatasBrowserList::changeFilter(int filter)
{
  mFilter = (CYDatasBrowser::Filter)filter;
  rebuild();
}

void CYDatasBrowserList::changeSearch(QString InputData)
{
  mSearchData = InputData.toUpper();
  rebuild();
}

void CYDatasBrowserList::rebuild()
{
  clear();
//  while (!items.isEmpty())
//    delete items.takeFirst();
  if ( mOnlyHost && mOnlyLink )
    init(mOnlyHost, mOnlyLink, true);
  else
  {
    CYNetHost *host;
    QHashIterator<QString, CYNetHost *> it(core->hostList);
    it.toFront();
    while (it.hasNext())
    {
      it.next();                   // must come first
      host=it.value();
      init(host);
    }
  }
  setSortingEnabled(true);
}
