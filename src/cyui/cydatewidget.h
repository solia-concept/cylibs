//
// C++ Interface: cydatewidget
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDATEWIDGET_H
#define CYDATEWIDGET_H

// QT
#include <QDateEdit>
// CYLIBS
#include "cydatawdg.h"

/**
@short Boîte de saisie d'une donnée date  CYDateTime.

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYDateWidget : public QDateEdit, public CYDataWdg
{
  Q_OBJECT
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:
  CYDateWidget(QWidget *parent = 0, const QString &name = 0);

  ~CYDateWidget();

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** Saisie la couleur de fond. */
  virtual void setBackgroundColor(const QColor &color);
  /** Saisie la couleur du premier plan. */
  virtual void setForegroundColor(const QColor &color);
  /** Saisie la palettes de couleurs. */
  virtual void setPalette(const QPalette &palette);

  /** @return la valeur courrante, dans la BT de la donnée. */
  s64 value() const;
  /** Fixe la valeur courante à \a value, exprimée dans la BT de la donnée. */
  void setValue(s64 value);

signals: // Signals
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();
  /** Emis lorsque le widget reçoit le focus. */
  void focusIn();

public slots:
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();

  /** Configure le widget en fonction de la donnée à laquelle il est lié. */
  virtual void linkData();

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);

protected slots:
  virtual void setDateChanged(QDate date);

protected: // Protected methods
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
       Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);
  /** Gestion de la réception du focus. */
  void focusInEvent(QFocusEvent *e);

  /** Base de temps. */
  double mBase;
};


#endif
