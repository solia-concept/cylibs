/***************************************************************************
                          cynuminput.h  -  description
                             -------------------
    début                  : jeu mai 15 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : g.lecleach@clemessy.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYNUMINPUT_H
#define CYNUMINPUT_H

// QT
#include <QDoubleSpinBox>
#include <QString>
#include <QMouseEvent>
#include <QEvent>
// CYLIBS
#include "cydatawdg.h"

/** CYNumInput est une .
  * @short Boîte de saisie d'une donnée numérique.
  * @author Gérald LE CLEACH
  */

class CYNumInput : public QDoubleSpinBox, public CYDataWdg
{
  Q_OBJECT
  Q_ENUMS(NumBase)
  Q_PROPERTY(QString dataName READ dataName WRITE setDataName)
  Q_PROPERTY(bool hideIfNoData READ hideIfNoData WRITE setHideIfNoData)
  Q_PROPERTY(QString flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QString hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QString posXName READ posXName WRITE setPosXName)
  Q_PROPERTY(QString posYName READ posYName WRITE setPosYName)
  Q_PROPERTY(QString benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool acceptLocalizedNumbers READ acceptLocalizedNumbers WRITE setAcceptLocalizedNumbers)
  Q_PROPERTY(NumBase numBase READ numBase WRITE setNumBase)
  Q_PROPERTY(bool showUnit READ showUnit WRITE setShowUnit)
  Q_PROPERTY(bool inputHelp READ inputHelp WRITE setInputHelp)
  Q_PROPERTY(bool readOnly READ readOnly WRITE setReadOnly)
  Q_PROPERTY(bool enableEdit READ enableEdit WRITE setEnableEdit)
  Q_PROPERTY(QString stepDataName READ stepDataName WRITE setstepDataName)
  Q_PROPERTY(int movieStepFocusIn READ movieStepFocusIn WRITE setMovieStepFocusIn)
  Q_PROPERTY(int movieStepFocusOut READ movieStepFocusOut WRITE setMovieStepFocusOut)
  Q_PROPERTY(bool autoMinimumSize READ autoMinimumSize WRITE setAutoMinimumSize)
  Q_PROPERTY(bool autoRefresh READ autoRefresh WRITE setAutoRefresh)
  Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
  Q_PROPERTY(QColor foregroundColor READ foregroundColor WRITE setForegroundColor)

public:
  /** Base numérique en cas de donnée entière positive */
  enum NumBase
  {
    /** Base numérique définie par la donnée.  */
    BaseData = 0,
    /** A base 2.  */
    Binary = 2,
    /** A base 10.  */
    Decimal = 10,
    /** A base 16.  */
    Hexadecimal = 16,
  };

  /** Construit une boîte de saisie d'une donnée numérique. */
  CYNumInput(QWidget *parent=0, const QString &name="numInput");

  ~CYNumInput();

  /** @return \a true si l'objet peut être déplacé.
    * @see void setMovable(const bool val). */
  virtual bool movable() const;
  /** Saisie \a true pour pouvoir déplacer. */
  virtual void setMovable(const bool val);

public: // Public methods
  /** @return la base numérique. */
  virtual NumBase numBase() const;

  /** @return le nom de la donnée traîtée. */
  virtual QString dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QString &name);

  /** @return \a true si l'objet doit être caché dans le cas où la donnée ne peut être trouvée. */
  virtual bool hideIfNoData() const { return mHideIfNoData; }
  /** Saisie \a true pour que l'objet soit caché dans le cas où la donnée ne peut être trouvée. */
  virtual void setHideIfNoData(const bool val) { mHideIfNoData = val; }

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QString flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QString &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QString hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QString &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le nom de la donnée de positionnement en X. */
  virtual QString posXName() const;
  /** Saisie le nom de la donnée de positionnement en X. */
  virtual void setPosXName(const QString &name);

  /** @return le nom de la donnée de positionnement en Y. */
  virtual QString posYName() const;
  /** Saisie le nom de la donnée de positionnement en Y. */
  virtual void setPosYName(const QString &name);

  /** @return le type du banc d'essai. */
  virtual QString benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QString &name);

  /** @return \a true si la spinbox utilise le système de localisation des nombres. */
  bool acceptLocalizedNumbers() const;
  /** Saisie si la spinbox utilise et accepte le système de localisation des nombres comme
    * ce que retourne KLocale::formatNumber(). */
  void setAcceptLocalizedNumbers(bool accept);

  /** Authorise/inhibe l'affichage de l'unité. */
  void setShowUnit(bool show=false) { mShowUnit = show; }
  /** @return \a true si l'affichage de l'unité est authorisé. */
  bool showUnit() const { return mShowUnit; }

  /** Utilise l'aide ou pas de saisie (valeur constructeur). */
  void setInputHelp(bool input=false) { mInputHelp = input; }
  /** @return si \a true utilise l'aide de saisie (valeur constructeur) sinon l'aide d'affichage. */
  bool inputHelp() const { return mInputHelp; }

  /** @return \a true si le widget est en lecture seule. */
  virtual bool readOnly() const;
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(const bool val);

  /** @return \a true si le widget est éditable. */
  virtual bool enableEdit() const { return mEnableEdit; }
  /** Rend le widget éditable ou pas suivant \a val. */
  virtual void setEnableEdit(const bool val) { mEnableEdit = val; }

  /** @return le nom de la donnée donnant le pas d'incrémentation. */
  virtual QString stepDataName() const { return mstepDataName; }
  /** Saisie le nom de la donnée donnant le pas d'incrémentation. */
  virtual void setstepDataName(const QString name) { mstepDataName = name; }

  /** @return le numéro du pas envoyé à une animation lors de la réception du focus. */
  int movieStepFocusIn() const { return mMovieStepFocusIn; }
  /** Saisie le numéro du pas envoyé à une animation lors de la réception du focus. */
  void setMovieStepFocusIn(const int step) { mMovieStepFocusIn = step; }

  /** @return le numéro du pas envoyé à une animation lors de la perte du focus. */
  int movieStepFocusOut() const { return mMovieStepFocusOut; }
  /** Saisie le numéro du pas envoyé à une animation lors de la perte du focus. */
  void setMovieStepFocusOut(const int step) { mMovieStepFocusOut = step; }

  /** @return \a true si la taille minimum est calculée automatiquement par rapport à la donnée. */
  virtual bool autoMinimumSize() const { return mAutoMinimumSize; }
  /** Saisir \a true pour que la taille minimum soit calculée automatiquement par rapport à la donnée. */
  virtual void setAutoMinimumSize(const bool val) { mAutoMinimumSize = val; }

  /** @return \a true si le widget est rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  virtual bool autoRefresh() const { return mAutoRefresh; }
  /** Saisir \a true pour que le widget soit rafraîchi à chaque émission du signal \a refreshing de son parent @see mParent. */
  virtual void setAutoRefresh(const bool val) { mAutoRefresh = val; }

  /** Saisie la couleur de fond. */
  virtual void setBackgroundColor(const QColor &color);
  /** @return la couleur de fond du widget */
  virtual const QColor & backgroundColor () const { return mBackgroundColor; }
  /** Saisie la couleur du premier plan. */
  virtual void setForegroundColor(const QColor &color);
  /** @return la couleur de devant du widget */
  virtual const QColor & foregroundColor () const { return mForegroundColor; }
  /** Saisie la palettes de couleurs. */
  virtual void setPalette(const QPalette &palette);

  /** Saisie une nouvelle plage de valeurs de la spin box. A noter que \a lower,
    * \a upper et \a step sont arrondies par rapport à la précision \a precision
    * du point des décimales. */
  void setRange(double lower, double upper, double step=0.01, int precision=2);

  /** @return le nombre courant de décimales affichées. */
  int precision() const;
  /** Equivalent à setPrecsion(\a precison, \a false). */
  void setPrecision(int precision);
  /** Saisie le nombre de décimales utilisées. A noter qu'il y a un échange
    * entre la précision utilisée et la plage des valeurs authorisées.
    * @param precision  Le nouveau nombre de décimales utilisées.
    * @param force      Désactive la vérification des dépassement de limites.
                        Pose problème si l'augmentation de la précision est telle que
                        les valeurs mimimum et maximum ne peuvent plus être représentées.
                        Désactivé ceci est utile pour ne pas garder les valeurs
                        mimimum et maximum courantes.  */
  void setPrecision(int precision, bool force);

  /** @return la valeur courrante. */
  double value() const;
  /** Fixe la valeur courante à \a value, d'abord arrondie suivant la précision
    * puis rognée en fonction de l'intervale [\a minvalue(),\a maxValue()]. */
  void setValue(double value);

  /** @return la borne inférieure. */
  double minValue() const;
  /** Fixe la borne inférieure de la plage à \a value, d'abord arrondie suivant
    * la précison puis rognée par rapport à l'intervale maximum de représentation.
    * @see maxValue, minValue, setMaxValue, setRange. */
  void setMinValue(double value);

  /** @return la borne supérieure. */
  double maxValue() const;
  /** Fixe la borne supérieure de la plage à \a value, d'abord arrondie suivant
    * la précison puis rognée par rapport à l'intervale maximum de représentation.
    * @see minValue, maxValue, setMinValue, setRange. */
  void setMaxValue(double value);

  /** @return la taille du pas courrant. */
  double singleStep() const;
   /** Fixe la taille du pas d'incrémentation/de décrémentation à \a step,
     * d'abord arrondi suivant la précision puis rogné pour un intervale
     * significatif [1, \a maxValue - \a minValue]. */
  void setSingleStep(double step);

  /** Réimplémenter pour ignorer tous les appels à setValidator(). */
  void setValidator(const QValidator *);

  /** Place le focus et optionnellement sélectionne tout le texte. */
  void setEditFocus(bool mark);

  /** Saisie la valeur de la donnée temporelle traîtée en millisecondes. */
  void setMilliseconds(unsigned int val);
  /** Saisie la valeur de la donnée temporelle traîtée en secondes. */
  void setSeconds(unsigned int val);

  /** @return la chaîne de caracteres de 0 devant la valeur. */
  QString forward0() const;

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

  virtual void moveInsideParent(int x, int y);
  virtual void setEditPositions( bool val );

  void stepBy(int steps);

signals: // Signals
  /** Emis à chaque fois que QSpinBox::valueChanged(int) est émis. */
  void valueChanged(int value);
  /** Emis à chaque modification de valeur. */
  void modifie();
  /** Emis pour demander l'application de la valeur et de toutes celles des autres widgets de saisie visibles. */
  void applying();
  /** Emis lorsque la souris entre dans le widget. */
  void enteringEvent();
  /** Emis lorsque la souris sort du widget. */
  void leavingEvent();
  /** Emis lorsque le widget reçoit le focus. */
  void focusIn();
  /** Emis lorsque le widget perd le focus. */
  void focusOut();
  /** Emet le numéro du pas envoyé à une animation lors de la réception du focus. */
  void focusIn(int movieStep);
  /** Emet le numéro du pas envoyé à une animation lors de la perte du focus. */
  void focusOut(int movieStep);
  /** Emet la valeur temporaire d'une donnée. */
  void tmpValue(double val);
  /** Signale que la donnée courrante vient de changer. */
  void currentDataChanged();
  /** Emis à chaque déplacement à la souris afin de définir un ordre dans la pile de widget du parent. */
  void movedInsideParent();

public slots: // Public slots
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();
  /** Change de section. */
  void changeSection();
  /** @return et donne l'ordre d'émettre la valeur temporaire. */
  double getTmpValue();
  /** Change le nom de la donnée courrante. \a Index est la position dans la liste des noms de données disponibles. */
  virtual void setCurrentDataName(int index);
  /** Change la donnée courrante. \a Index est la position dans la liste de données disponibles. */
  virtual void setCurrentData(int index);

  /** Place @p data comme la donnée de forçage du widget. */
  virtual void setForcingData(CYData *data);
  /** Réception du signal émis à chaque changement de valeur de forçage à partir du synoptic. */
  virtual void synopticForcingChanged(double);

  /** Mise à jour la palette de couleurs en ReadOnly. */
  virtual void updateROPalette();
  /** Test la validité de la nouvelle valeur de la donnée traîtée. */
  virtual bool testNewDataValue();
  /** Désactive le widget. */
  virtual void setEnabled(bool);
//  virtual void adjustSize();

  /** Contrôle le flag d'activation de l'objet graphique. */
  virtual bool ctrlFlag();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();

  /** Configure le widget en fonction de la donnée à laquelle il est lié. */
  virtual void linkData();

  /** Saisie le préfixe de la donnée.*/
  virtual void setDataPrefix( const QString & text );
  /** Saisie le suffix de la donnée .*/
  virtual void setDataSuffix( const QString & text );

  virtual void setDataNbDec(int nb);
  virtual void resetDataNbDec();

  virtual void refreshPosition();
  virtual void updatePosition();

  /** Saisie la base numérique. */
  virtual void setNumBase(const NumBase b);
  /** Saisie un entier correspondant à une base numérique prédéfinie. */
  virtual void setNumBase(int base);

  /** Saisir \a true si la valeur qui vient d'être saisie est invalide. */
  void setInvalid(bool val) { mInvalid=val; }
  /** Saisie la la dernière valeur valide. */
  void setLastValid(double val) { mLastValid =val; }

protected slots: // Protected slots
  /** Même fonctionnement que QSpinBox::textChanged() avec en plus l'émission du signal \a modifie() */
  virtual void textChanged();
  virtual void slotValueChanged(double value);
  virtual void slotHexaKeyPress();
  virtual void slotNumKeyPress();
  virtual void slotMouseButtonPress();

protected: // Protected methods
  /** Gestion des évènements. */
  virtual bool event(QEvent *e);
  /** Gestion d'entrée dans le widget de la souris. */
  virtual void enterEvent ( QEvent * );
  /** Gestion de sortie du widget de la souris. */
  virtual void leaveEvent ( QEvent * );
  virtual void updateValidator();

  virtual QString mapValueToText(int);
//  virtual int mapTextToValue(bool *);
  virtual QString textFromValue(double value) const;
  virtual double valueFromText(const QString & text) const;

  virtual void mousePressEvent ( QMouseEvent * e );
  virtual void mouseMoveEvent ( QMouseEvent * e );

  virtual void showEvent(QShowEvent *e);

protected: // Protected attributes
  NumBase mNumBase;
  /** Vaut \a true la valeur qui vient d'être saisie est invalide. */
  bool mInvalid;
  /** Donne la dernière valeur valide. */
  double mLastValid;
  bool mShowUnit;
  /** Si \a true utilise l'aide de saisie (valeur constructeur) sinon l'aide d'affichage. */
  bool mInputHelp;
  /** Indique qu'il ne faut pas contrôler la validité de la donnée. */
  bool mNoControl;
  QString mSpecialValue;
  /** Vaut \a true si le widget est éditable. */
  bool mEnableEdit;
  /** Nom de la donnée donnant le pas d'incrémentation. */
  QString mstepDataName;
  /** Vaut \a true si un test est déjà en cours. */
  bool mTestingNewDataValue;
  /** Vaut \a true si un changement de section est en cours. */
  bool mChangingSection;

  QString mDataPrefix;
  QString mDataSuffix;

  int mNewNumDec;

private: // Private methods
  int maxPrecision() const;

private: // Private attributes
  class Private;
  Private *d;
};

#endif
