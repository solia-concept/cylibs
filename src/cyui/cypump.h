/***************************************************************************
                          cypump.h  -  description
                             -------------------
    begin                : jeu oct 9 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYPUMP_H
#define CYPUMP_H

// CYLIBS
#include "cymovieview.h"

/**Visualisateur de pompe.
  *@author Gérald LE CLEACH
  */

class CYPump : public CYMovieView
{
  Q_OBJECT
  Q_ENUMS(Sens)
  Q_PROPERTY(Sens sens READ sens WRITE setSens)
  Q_PROPERTY(QPixmap backgroundPixmap READ backgroundPixmap WRITE setBackgroundPixmap)

public:
  CYPump(QWidget *parent=0, const QString &name=0);
  ~CYPump();

  enum Sens { LeftToRight, RightToLeft, BottomToTop, TopToBottom };

public: // Public methods
  /** Saisie le sens de circulation de la pompe.
      Attention cette fonction fonctionne uniquement si il n'y a pas de donnée d'index de sélection des fichiers dans les dictionnaires d'images animées et figées. @see setDataIndexFile.
      En outre cette fonction remet à zéro l'offset de cet index. */
  void setSens(Sens s);
  /** @return le sens de circulation de la pompe. */
  Sens sens() const { return mSens; }

  /** Saisie l'image de fond. */
  virtual void setBackgroundPixmap ( const QPixmap & );
  /** @return la couleur du premier plan du widget */
  virtual const QPixmap & backgroundPixmap () const;

private: // Private attributes
  /** Sens de circulation de la pompe. */
  Sens mSens;
};

#endif
