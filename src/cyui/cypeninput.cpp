//
// C++ Implementation: cypeninput
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

// QT
#include <qlabel.h>
// CYLIBS
#include "cypeninput.h"
#include "cypen.h"
#include "ui_cypeninput.h"

CYPenInput::CYPenInput(QWidget *parent, const QString &name)
  : CYWidget(parent,name), ui(new Ui::CYPenInput)
{
  ui->setupUi(this);
  mPen = 0;
}


CYPenInput::~CYPenInput()
{
  delete ui;
}


/*!
    \fn CYPenInput::setPen( CYPen *pen )
 */
void CYPenInput::setPen( CYPen *pen )
{
  mPen = pen;
}
