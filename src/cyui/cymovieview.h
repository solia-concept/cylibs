/***************************************************************************
                          cymovieview.h  -  description
                             -------------------
    begin                : mar oct 7 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYMOVIEVIEW_H
#define CYMOVIEVIEW_H

// QT
#include <QMovie>
// CYLIBS
#include "cypixmapview.h"

/** @short Visualisateur d'image annimée.
  * @author Gérald LE CLEACH
  */

class CYMovieView : public CYPixmapView
{
  Q_OBJECT
  Q_PROPERTY(QByteArray pixmapFile READ pixmapFile WRITE setPixmapFile)
  Q_PROPERTY(QByteArray movieFile READ movieFile WRITE setMovieFile)
  Q_PROPERTY(QByteArray dataIndexFile READ dataIndexFile WRITE setDataIndexFile)
  Q_PROPERTY(int offsetIndexFile READ offsetIndexFile WRITE setOffsetIndexFile)
  Q_PROPERTY(QMovie::CacheMode cacheMode READ cacheMode WRITE setCacheMode)
  Q_PROPERTY(int speed READ speed WRITE setSpeed)
  Q_PROPERTY(bool stepByStep READ stepByStep WRITE setStepByStep)
  Q_PROPERTY(QByteArray pixmapFileTrue READ pixmapFileTrue WRITE setPixmapFileTrue DESIGNABLE false STORED false)
  Q_PROPERTY(QByteArray pixmapFileFalse READ pixmapFileFalse WRITE setPixmapFileFalse DESIGNABLE false STORED false)

public:
  CYMovieView(QWidget *parent=0, const QString &name=0);
  ~CYMovieView();

  QSize sizeHint() const
  {
    return sh;
  }

  /** Saisie le nom de la donnée d'index de sélection des fichiers dans les dictionnaires d'images animées et figées.  */
  virtual void setDataIndexFile(const QByteArray &name);
  /** @return la donnée d'index de sélection des fichiers dans les dictionnaires d'images animées et figées. */
  virtual QByteArray dataIndexFile() const;

  /** Saisie l'offset sur la donnée d'index de sélection des fichiers dans les dictionnaires d'images animées et figées.  */
  virtual void setOffsetIndexFile(const int offset) { mOffsetIndexFile = offset; }
  /** @return l'offset sur la donnée d'index de sélection des fichiers dans les dictionnaires d'images animées et figées. */
  virtual int offsetIndexFile() const { return mOffsetIndexFile; }

  /** @return le nom du fichier de l'image figée. */
  QByteArray pixmapFile() const;
  /** @return le nom du fichier de l'image animée. */
  QByteArray movieFile() const;
  /** @return la propriété QMovie::CacheMode du film.*/
  QMovie::CacheMode	cacheMode() const;

  /** @return \a true si l'image animée est en pause. */
  bool paused() const;
  /** @return \a true si l'image animée n'a plus rien à jouer: Ceci arrive
    * lorsque toutes les boucles de la trame ont été réalisées. */
  bool finished() const;
  /** @return \a true si l'image animée est ni en pas à pas, ni en pause pause et ni terminé. */
  bool running() const;
  /** Désactive la pause en animation. */
  void unpause();
  /** Active la pause en animation. */
  void pause();
  //  /** Avance d'1 pas trame de l'image animée puis la met en pause. */
  //  void step();
  //  /** Avance de \a steps pas trame de l'image animée puis la met en pause. */
  //  void step(int steps);
  /** Rembobine le film jusqu'au début. Si le film n'a pas été mis en pause, sa lecture est redémarrée. */
  void restart();
  /** @return la vitesse de lecture en pourcentage. Par défaut elle est à 100%. */
  int speed() const;
  /** Saisie la vitesse de lecture en pourcentage. Par défaut elle est à 100%.
    * C'est en fait est un pourcentage de la vitesse de l'image animmée source. */
  void setSpeed(const int percent);
  /** @return  \a true si le pas à pas est actif. */
  bool stepByStep() const { return mStepByStep; }
  /** Saisie \a true si le pas à pas doit être actif. */
  void setStepByStep(const bool val);

  /** Saisie l'angle de rotation du contenu. */
  virtual void setRotate(int value) { mRotate = value; }

public slots: // Public slots
  /** Saisie le nom du fichier de l'image figée. */
  virtual void setPixmapFile(const QByteArray &file);
  /** Saisie l'index de l'image figée dans le dictionnaire d'images figées. */
  virtual void setPixmapFile(int index);
  /** Saisie le nom du fichier de l'image animée. */
  virtual void setMovieFile(const QByteArray &file);
  /** Saisie l'index de l'image animée dans le dictionnaire d'images animées. */
  virtual void setMovieFile(int index);
  /** Ajoute un nom de fichier à l'index \a index dans le dictionnaire d'images figées. */
  virtual void addPixmapFile(int index, QString fileName);
  /** Ajoute un nom de fichier à l'index \a index dans le dictionnaire d'images animées. */
  virtual void addMovieFile(int index, QString fileName);
  /** Saisie la propriété QMovie::CacheMode du film.*/
  virtual void setCacheMode(QMovie::CacheMode mode);
  /** Saisie l'état. */
  virtual void setState(bool);
  /** Avance d'1 pas trame de l'image animée puis la met en pause. */
  void step();
  /** Avance de \a steps pas trame de l'image animée puis la met en pause. */
  void step(int steps);
  /** Avance l'image animée au pas \a step. */
  void goToStep(int step);
  /** Configure le widget en fonction de la donnée représentant la longueur du bras déployé en pourcentage de sa course. */
  virtual void linkData2();
  /** Charge la valeur de la donnée de sélection de l'image figée et de l'animée disponibles dans leurs dictionnaires respectifs. */
  virtual void refresh2();

protected: // Protected methods
  virtual void paintEvent(QPaintEvent* event);
  /** Dessine le contenu de la trame. */
  virtual void drawContents(QPainter* p);

private slots: // Private slots
  void movieResized(const QSize& size);

protected: // Protected attributes
  /** Nom du fichier de l'image figée. */
  QString mPixmapFile;
  /** Nom du fichier de l'image animée. */
  QString mMovieFile;
  QSize sh;
  /** Vitesse de lecture en pourcentage. Par défaut elle est à 100%. */
  int mSpeed;
  /** Vaut \a true si le pas à pas est actif. */
  bool mStepByStep;
  /** Propriété QMovie::CacheMode du film.*/
  QMovie::CacheMode mCacheMode;

  /** Dictionnaire d'images figées. */
  QHash<int, QString*> mPixmapFiles;
  /** Dictionnaire d'images animées. */
  QHash<int, QString*> mMovieFiles;
  int mPixmapIndex;
  int mMovieIndex;
  int mOffsetIndexFile;
};

#endif
