//
// C++ Implementation: cydatewidget
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <QFocusEvent>
// CYLIBS
#include "cydatewidget.h"
#include "cydatetime.h"
#include "cyflag.h"
#include "cycore.h"
#include "cyreadonlyfilter.h"

CYDateWidget::CYDateWidget(QWidget *parent, const QString &name)
 : QDateEdit(parent),
   CYDataWdg(this)
{
  setObjectName(name);

  mWriteData    = true;
  mReadOnly     = false;
  mAutoRefresh  = false;
  mEmitModifie  = true;

  mPalette = palette();

  mBase = BASE_SEC;

//  connect(this, SIGNAL(changed(QDate)), this, SLOT(setDateChanged(QDate)));
  setFocusPolicy(Qt::StrongFocus);
}


CYDateWidget::~CYDateWidget()
{
}

void CYDateWidget::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYDateWidget::dataName() const
{
  return CYDataWdg::dataName();
}

void CYDateWidget::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYDateWidget::flagName() const
{
  return CYDataWdg::flagName();
}

void CYDateWidget::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYDateWidget::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYDateWidget::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYDateWidget::benchType() const
{
  return CYDataWdg::benchType();
}

void CYDateWidget::setInverseFlag(bool inverse)
{
  CYDataWdg::setInverseFlag(inverse);
}

bool CYDateWidget::inverseFlag() const
{
  return CYDataWdg::inverseFlag();
}

void CYDateWidget::setInverseHideFlag(bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYDateWidget::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

s64 CYDateWidget::value() const
{
  s64 val;
  QDate d = date();

  if (mBase==BASE_SEC)
    val = d.endOfDay().toSecsSinceEpoch();
  else if (mBase==BASE_MSEC)
    val = d.endOfDay().toMSecsSinceEpoch();
  else
    CYWARNINGTEXT(QString("%1: Base inconnue %2").arg(objectName()).arg(mBase));

  return val;
}

void CYDateWidget::setValue(s64 val)
{
  QDateTime dt = date().endOfDay();
  if (mBase==BASE_SEC)
    dt.setSecsSinceEpoch(val);
  else if (mBase==BASE_MSEC)
    dt.setMSecsSinceEpoch(val);
  else

    CYWARNINGTEXT(QString("%1: Base inconnue %2").arg(objectName()).arg(mBase));
  setDate(dt.date());
  if (mEmitModifie && isVisible())
    emit modifie();
}

void CYDateWidget::setDateChanged(QDate date)
{
  Q_UNUSED(date)
  // TODO QT5
  if (mEmitModifie && isVisible())
    emit modifie();
}


void CYDateWidget::linkData()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
    return;

  mEmitModifie = false;

  if (mData->inherits("CYDateTime"))
  {
    CYDateTime *data = (CYDateTime *)mData;
    setValue(data->val());
    mBase = data->base();
    this->setToolTip(QString(data->inputHelp()+"<br>"+data->infoCY()));
  }

  if (mEnableReadOnly)
  {
    bool readOnly = mReadOnly;
    setReadOnly(false);
    mPalette.setColor(QPalette::Active, QPalette::Text, mData->inputColor());
    QDateEdit::setPalette(mPalette);
    setReadOnly(readOnly);
  }

  ctrlFlag();

  mEmitModifie = true;
}

void CYDateWidget::refresh()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (!readOnly())
    return;

  if (mData==0)
  {
    setEnabled(false);
    return;
  }

  if (!mData->isOk() && !alwaysOk())
  {
    setEnabled(false);
    return;
  }

  if (mData->inherits("CYDateTime"))
  {
    CYDateTime *data = (CYDateTime *)mData;
    setDate(data->date());
  }
}

void CYDateWidget::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    setEnabled(false);
    return;
  }

  if (mData->inherits("CYDateTime"))
  {
    CYDateTime *data = (CYDateTime *)mData;
    setValue(data->val());
  }
}

void CYDateWidget::update()
{
  if (!isVisible() && mUpdateIfVisible)
    return;

  if ((mData==0) || (!isEnabled()))
    return;

  if (isHidden())
    return;

  mEmitModifie = false;

  if (mData->inherits("CYDateTime"))
  {
    CYDateTime *data = (CYDateTime *)mData;
    data->setVal(value());
  }

  mEmitModifie = true;
}

bool CYDateWidget::ctrlFlag()
{
  if (mFlag==0)
    return true;

  if (parent() && !((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
  {
    res = true;
  }
  else if (!mFlag->val() && mInverseFlag)
  {
    res = true;
  }
  else
  {
    res = false;
  }
  setEnabled(res);
  return res;
}

void CYDateWidget::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYDateWidget::focusInEvent(QFocusEvent *e)
{
  QDateEdit::focusInEvent(e);
  emit focusIn();
}

void CYDateWidget::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }


  ctrlFlag();
}

void CYDateWidget::setBackgroundColor(const QColor &color)
{
  QPalette palette;
  palette.setColor(backgroundRole(), color);
  setPalette(palette);
}

void CYDateWidget::setForegroundColor(const QColor &color)
{
  QPalette palette;
  palette.setColor(QPalette::WindowText, color);
  setPalette(palette);
}

void CYDateWidget::setPalette(const QPalette &palette)
{
  QDateEdit::setPalette(palette);
}

void CYDateWidget::changeEvent(QEvent* event)
{
  if (event && (event->type()==QEvent::EnabledChange))
  {
    // cet événement est envoyé, si l'état actif change
    bool state = isEnabled();
    if (!readOnly() || !state)
      mEnabled = state;
  }
  QDateEdit::changeEvent(event);
}


