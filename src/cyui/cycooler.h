//
// C++ Interface: cycooler
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYCOOLER_H
#define CYCOOLER_H

#include <cypixmapview.h>

/**
@short Système de refroidissement.

@author Gérald LE CLEACH
*/
class CYCooler : public CYPixmapView
{
Q_OBJECT
public:
    CYCooler(QWidget *parent = 0, const QString &name = 0);

    ~CYCooler();

};

#endif
