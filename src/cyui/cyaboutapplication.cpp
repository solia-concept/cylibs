#include "cyaboutapplication.h"

// QT
#include <qtabwidget.h>
// CYLIBS
#include "ui_cyaboutapplication.h"
#include "cyapplication.h"
#include "cyaboutdata.h"

CYAboutApplication::CYAboutApplication(QWidget *parent, const QString &name) :
  CYDialog(parent, name), ui(new Ui::CYAboutApplication)
{
  ui->setupUi(this);
  CYAboutData *data = cyapp->aboutData();
  ui->aboutLabel->setText(QString("%1\n\n%2")
                          .arg(data->shortDescription())
                          .arg(data->copyrightStatement()));

  QString authorText = QString("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN"
                               "http://www.w3.org/TR/REC-html40/strict.dtd\">"
                               "<html><head><meta name=\"qrichtext\" content=\"1\" />"
                               "<style type=\"text/css\">p, li { white-space: pre-wrap; }"
                               "</style></head>"
                               "<body style=\" font-family:'Droid Sans'; "
                               "font-size:10pt; font-weight:400; font-style:normal;\">");

  QString bugReport = tr("Please report bugs to: ");
  authorText.append(QString("<p style=\" "
                            "margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px;"
                            " -qt-block-indent:0; text-indent:0px;\">"
                            "%1"
                            "<a href=\"%2\">"
                            "<span style=\" text-decoration: underline; color:#0000ff;\">%2</span></a>"
                            "<br></p>")
                    .arg(bugReport).arg(data->bugAddress()));

  QList<CYAboutPerson> authors = data->authors();
  for (int i = 0; i < authors.size(); ++i)
  {
    CYAboutPerson author = authors[i];
    QString name = QString("<p style=\" \n"
                           "margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px;"
                           "-qt-block-indent:0; text-indent:0px;\">"
                           "%1</p>").arg(author.name());

    QString emailAddress =QString("<p style=\" "
                                  "margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;"
                                  " -qt-block-indent:0; text-indent:0px;\">"
                                  "<span style=\" color:#008000;\"></span>"
                                  "<a href=\"%1\">"
                                  "<span style=\" text-decoration: underline; color:#0000ff;\">%1</span></a>"
                                  "</p>").arg(author.emailAddress());

    QString task = QString("<p style=\" "
                           "margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;"
                           "-qt-block-indent:0; text-indent:0px;\">"
                           "%1</p>").arg(author.task());

    authorText.append(QString("%1\n\t%2\n\t%3\n")
                      .arg(name)
                      .arg(emailAddress)
                      .arg(task));
  }
  authorText.append("</body></html>");
  ui->authorBrowser->setText(authorText);
}

CYAboutApplication::~CYAboutApplication()
{
  delete ui;
}

void CYAboutApplication::addTab( QWidget * child, const QString & label )
{
  ui->tabWidget->addTab(child, label);
}
