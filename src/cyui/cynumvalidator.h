/**********************************************************************
**
** $Id: knumvalidator.h 469215 2005-10-10 13:47:50Z lukas $
**
** Copyright (C) 1999 Glen Parker <glenebob@nwlink.com>
** Copyright (C) 2002 Marc Mutz <mutz@kde.org>
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
*****************************************************************************/

#ifndef __KNUMVALIDATOR_H
#define __KNUMVALIDATOR_H

#include <qvalidator.h>

class QWidget;
class QString;

/**
 * QValidator for integers.

  This can be used by QLineEdit or subclass to provide validated
  text entry.  Can be provided with a base value (default is 10), to allow
  the proper entry of hexadecimal, octal, or any other base numeric data.

  @author Glen Parker <glenebob@nwlink.com>
  @version 0.0.1
*/
class CYIntValidator : public QValidator {

  public:
    /**
      Constuctor.  Also sets the base value.
    */
    CYIntValidator ( QWidget * parent, int base = 10, const char * name = 0 );
    /**
     * Constructor.  Also sets the minimum, maximum, and numeric base values.
     */
    CYIntValidator ( int bottom, int top, QWidget * parent, int base = 10, const char * name = 0 );
    /**
     * Destructs the validator.
     */
    virtual ~CYIntValidator ();
    /**
     * Validates the text, and return the result.  Does not modify the parameters.
     */
    virtual State validate ( QString &, int & ) const;
    /**
     * Fixes the text if possible, providing a valid string.  The parameter may be modified.
     */
    virtual void fixup ( QString & ) const;
    /**
     * Sets the minimum and maximum values allowed.
     */
    virtual void setRange ( int bottom, int top );
    /**
     * Sets the numeric base value.
     */
    virtual void setBase ( int base );
    /**
     * Returns the current minimum value allowed.
     */
    virtual int bottom () const;
    /**
     * Returns the current maximum value allowed.
     */
    virtual int top () const;
    /**
     * Returns the current numeric base.
     */
    virtual int base () const;

  private:
    int _base;
    int _min;
    int _max;

};

#endif
