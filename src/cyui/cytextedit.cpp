/***************************************************************************
                          cytextedit.cpp  -  description
                             -------------------
    begin                : ven déc 5 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cytextedit.h"

// QT
#include <QTextBlock>
// CYLIBS
#include "cyflag.h"
#include "cystring.h"
#include "cypopupmenu.h"
#include "cycore.h"
#include "cyreadonlyfilter.h"

CYTextEdit::CYTextEdit(QWidget *parent, const QString &name)
  : QTextEdit(parent),
    CYDataWdg(this)
{
  setObjectName(name);
  init();
}

CYTextEdit::~CYTextEdit()
{
}

void CYTextEdit::init()
{
  mWriteData = true;
  mWasOk     = false;
  mAlwaysOk  = true;
  mShowLastLine = true;
  setModified(false);
  connect(document(), SIGNAL(modificationChanged(bool)), this, SLOT(setModified(bool)));
  connect(this, SIGNAL(textChanged()), SLOT(ctrlMaxLines()));
}

void CYTextEdit::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYTextEdit::dataName() const
{
  return CYDataWdg::dataName();
}

void CYTextEdit::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYTextEdit::flagName() const
{
  return CYDataWdg::flagName();
}

void CYTextEdit::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYTextEdit::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYTextEdit::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYTextEdit::benchType() const
{
  return CYDataWdg::benchType();
}

void CYTextEdit::setInverseFlag(bool inverse)
{
  mInverseFlag = inverse;
}

bool CYTextEdit::inverseFlag() const
{
  return mInverseFlag;
}

void CYTextEdit::setInverseHideFlag(bool inverse)
{
  mInverseHideFlag = inverse;
}

bool CYTextEdit::inverseHideFlag() const
{
  return mInverseHideFlag;
}

bool CYTextEdit::ctrlFlag()
{
  if (mFlag==0)
    return true;

  if (!((QWidget *)parent())->isEnabled())
    return true;

  bool res;

  if (mFlag->val() && !mInverseFlag)
    res = true;
  else if (!mFlag->val() && mInverseFlag)
    res = true;
  else
    res = false;

  setEnabled(res);
  return res;
}

void CYTextEdit::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

bool CYTextEdit::readOnly() const
{
  return mReadOnly;
}

void CYTextEdit::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;

  mReadOnly = val;
  if (core)
  {
    removeEventFilter(core->readOnlyFilter());
    if (mReadOnly)
      installEventFilter(core->readOnlyFilter());
  }

  ctrlFlag();
}

void CYTextEdit::changeEvent(QEvent* event)
{
  if (event && (event->type()==QEvent::EnabledChange))
  {
    // cet événement est envoyé, si l'état actif change
    bool state = isEnabled();
    if (!readOnly() || !state)
      mEnabled = state;
  }
  QTextEdit::changeEvent(event);
}

bool CYTextEdit::alwaysOk() const
{
  return mAlwaysOk;
}

void CYTextEdit::setAlwaysOk(const bool val)
{
  mAlwaysOk = val;
}

QString CYTextEdit::value() const
{
  return document()->toPlainText();
}

void CYTextEdit::setValue(QString val)
{
  if (value()!=val)
  {
    if (mShowLastLine)
    {
      setText(0);
      append(val);
    }
    else
    {
      setText(val);
    }
  }
}

void CYTextEdit::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYTextEdit::linkData()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
    return;

  disconnect(document(), SIGNAL(modificationChanged(bool)), this, SLOT(setModified(bool)));
  switch (mData->type())
  {
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      setValue(data->val());
                      this->setToolTip(QString(data->inputHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
  bool readOnly = mReadOnly;
  setReadOnly(false);
  mPalette.setColor(QPalette::Active, QPalette::Text, mData->inputColor());
  QTextEdit::setPalette(mPalette);
  setReadOnly(readOnly);
  setModified(false);
  connect(document(), SIGNAL(modificationChanged(bool)), this, SLOT(setModified(bool)));
}

void CYTextEdit::refresh()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (!readOnly())
    return;

  if (mData==0)
  {
    setEnabled(false);
    return;
  }
  if (!mData->isOk() && !mAlwaysOk)
  {
    setEnabled(false);
    return;
  }

  switch (mData->type())
  {
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      if (data->isOk() || mAlwaysOk)
                      {
                        if (!isEnabled())
                          setEnabled(true);
                        setValue(data->val());
                        return;
                      }
                      if (isEnabled())
                        setEnabled(false);
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      break;
  }
}

void CYTextEdit::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    setEnabled(false);
    return;
  }

  switch (mData->type())
  {
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      setValue(data->def());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYTextEdit::update()
{
  if (!isVisible() && mUpdateIfVisible)
    return;

  if ((mData==0) || (!isEnabled()))
    return;

  if (isHidden())
    return;

  switch (mData->type())
  {
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      data->setVal(value());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYTextEdit::focusInEvent(QFocusEvent *e)
{
  QTextEdit::focusInEvent(e);
  emit focusIn();
}

void CYTextEdit::setBackgroundColor(const QColor &color)
{
  QPalette palette;
  mBackgroundColor=color;
  palette.setColor(backgroundRole(), color);
  setPalette(palette);
}

void CYTextEdit::setForegroundColor(const QColor &color)
{
  QPalette palette;
  mForegroundColor=color;
  palette.setColor(QPalette::WindowText, color);
  setPalette(palette);
}

void CYTextEdit::setPalette(const QPalette &palette)
{
  QTextEdit::setPalette(palette);
}

void CYTextEdit::setModified(bool m)
{
  if (m)
    emit modifie();
  document()->setModified(false);
}


QMenu * CYTextEdit::createPopupMenu( const QPoint & pos )
{
  QMenu *pm;
  if (!readOnly())
  {
    pm = QTextEdit::createStandardContextMenu( pos );
  }
  else
  {
    pm = new QMenu( this );
    vp_options = new QMenu( pm );
    Q_CHECK_PTR( vp_options );
    vp_options->setTitle(tr("&Scroll view"));
    // TOCHECK QT5
    // vp_options->setCheckable( true );
    pm->addMenu(vp_options );
    connect( vp_options, SIGNAL(activated(int)), this, SLOT(doVPMenuItem(int)) );

    nlar_action = vp_options->addAction( tr("Showing last line"), this,  SLOT(toogleShowLastLine()));
    vp_options->addSeparator();
    vauto_action = vp_options->addAction( tr("Vertical Auto"), this, SLOT(setVerticalScrollBarAuto()) );
    vaoff_action = vp_options->addAction( tr("Vertical Off"), this, SLOT(setVerticalScrollBarOff()) );
    vaon_action = vp_options->addAction( tr("Vertical On"), this, SLOT(setVerticalScrollBarOn()) );
    vp_options->addSeparator();
    hauto_action = vp_options->addAction( tr("Horizontal Auto"), this, SLOT(setHorizontalScrollBarAuto()) );
    haoff_action = vp_options->addAction( tr("Horizontal Off"), this, SLOT(setHorizontalScrollBarOff()) );
    haon_action = vp_options->addAction( tr("Horizontal On"), this, SLOT(setHorizontalScrollBarOn()) );

//     vp_options->addSeparator();
//     corn_action = vp_options->addAction( tr("cornerWidget") );

    setVPMenuItems();
  }

  foreach (CYPopupMenu *pm2, mPopupMenus)
  {
    if (readOnly() && pm2->readOnly())
    {
      pm2->insertInto(this, pm);
    }
  }
  return pm;
}

/*!
    \fn CYTextEdit::setVPMenuItems()
 */
void CYTextEdit::setVPMenuItems()
{
  nlar_action->setChecked(mShowLastLine);

  Qt::ScrollBarPolicy vm = verticalScrollBarPolicy();
  vauto_action->setChecked( vm==Qt::ScrollBarAsNeeded);
  vaoff_action->setChecked( vm==Qt::ScrollBarAlwaysOff);
  vaon_action->setChecked( vm==Qt::ScrollBarAlwaysOn);

  Qt::ScrollBarPolicy hm = horizontalScrollBarPolicy();
  hauto_action->setChecked( hm==Qt::ScrollBarAsNeeded);
  haoff_action->setChecked( hm==Qt::ScrollBarAlwaysOff);
  haon_action->setChecked( hm==Qt::ScrollBarAlwaysOn);

////   vp_options->setItemChecked( corn_id, !!cornerWidget() );
}


CYPopupMenu *CYTextEdit::addPopupMenu(QString text, int id, bool readOnly)
{
  CYPopupMenu *pm = new CYPopupMenu(text, readOnly);
  mPopupMenus.insert(id, pm);
  return pm;
}

QStringList CYTextEdit::splitByLines()
{
  QTextDocument *doc = document();
  if(!doc)
    return QStringList();
  QStringList ret;
  QTextBlock tb = doc->begin();
  while(tb.isValid())
  {
    QString blockText = tb.text();
    Q_ASSERT(tb.layout());
    if(!tb.layout())
      continue;
    for(int i = 0; i != tb.layout()->lineCount(); ++i)
    {
      QTextLine line = tb.layout()->lineAt(i);
      ret.append(blockText.mid(line.textStart(), line.textLength()));
    }
    tb = tb.next();
  }
  return ret;
}

void CYTextEdit::ctrlMaxLines()
{
  int nbLines = splitByLines().count();
  if ((mMaxLines<=0) || (nbLines<=mMaxLines))
  {
    mValidText = document()->toPlainText();
  }
  else
  {
    if ((document()->toPlainText().length()+1)>mValidText.length())
    {
      CYMessageBox::sorry(this, tr("Maximum number of lines : %1 !").arg(mMaxLines));
    }
    setText(mValidText);
    moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
  }
}

void CYTextEdit::setVerticalScrollBarAuto()
{
  setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void CYTextEdit::setVerticalScrollBarOn()
{
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
}

void CYTextEdit::setVerticalScrollBarOff()
{
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void CYTextEdit::setHorizontalScrollBarAuto()
{
  setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void CYTextEdit::setHorizontalScrollBarOn()
{
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
}

void CYTextEdit::setHorizontalScrollBarOff()
{
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}
