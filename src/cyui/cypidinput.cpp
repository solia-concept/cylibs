/***************************************************************************
                          cypidinput.cpp  -  description
                             -------------------
    begin                : ven avr 23 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cypidinput.h"

// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "ui_cypidinput.h"

CYPIDInput::CYPIDInput(QWidget *parent, const QString &name)
  : CYWidget(parent,name), ui(new Ui::CYPIDInput)
{
  ui->setupUi(this);
  mShowGroup = No;
}

CYPIDInput::~CYPIDInput()
{
  delete ui;
}

void CYPIDInput::linkDatas()
{
  if (!core)
    return;

  CYWidget::linkDatas();

  if (ui->type_input)
  {
    changeType(ui->type_input->value());
  }

  if ((mShowGroup!=No) && ui->type_input->CYDataWdg::data())
    ui->mButtonGroup->setTitle(ui->type_input->CYDataWdg::data()->groupSection((Cy::GroupSection)mShowGroup));
}

/*!
    \fn CYPIDInput::changeType(int type)
 */
void CYPIDInput::changeType(int type)
{
  Q_UNUSED(type)
  switch (type)
  {
    case PID_TYPE_FAULT:
    {
      ui->p_label->setEnabled(true );
      ui->p_input->setEnabled(true );
      ui->i_label->setEnabled(true );
      ui->i_input->setEnabled(true );
      ui->k_label->setEnabled(true );
      ui->k_input->setEnabled(true );
      ui->type_d_label->setEnabled(true );
      ui->type_d_input->setEnabled(true );
      ui->no_i_ramp_input->setEnabled(true);
      ui->no_i_ramp_label->setEnabled(true);
      ui->calcul_d_input->setEnabled(true);
      ui->calcul_d_label->setEnabled(true);
      break;
    }
    case PID_TYPE_P:
    {
      ui->p_label->setEnabled(true );
      ui->p_input->setEnabled(true );
      ui->i_label->setEnabled(false);
      ui->i_input->setEnabled(false);
      ui->k_label->setEnabled(false);
      ui->k_input->setEnabled(false);
      ui->type_d_label->setEnabled(false);
      ui->type_d_input->setEnabled(false);
      ui->no_i_ramp_input->setEnabled(false);
      ui->no_i_ramp_label->setEnabled(false);
      ui->calcul_d_input->setEnabled(false);
      ui->calcul_d_label->setEnabled(false);
      break;

    }
    case PID_TYPE_PI:
    {
      ui->p_label->setEnabled(true );
      ui->p_input->setEnabled(true );
      ui->i_label->setEnabled(true );
      ui->i_input->setEnabled(true );
      ui->k_label->setEnabled(false);
      ui->k_input->setEnabled(false);
      ui->type_d_label->setEnabled(false);
      ui->type_d_input->setEnabled(false);
      ui->no_i_ramp_input->setEnabled(true);
      ui->no_i_ramp_label->setEnabled(true);
      ui->calcul_d_input->setEnabled(false);
      ui->calcul_d_label->setEnabled(false);
      break;
    }
    case PID_TYPE_PD:
    {
      ui->p_label->setEnabled(true );
      ui->p_input->setEnabled(true );
      ui->i_label->setEnabled(true );
      ui->i_input->setEnabled(true );
      ui->k_label->setEnabled(true );
      ui->k_input->setEnabled(true );
      ui->type_d_label->setEnabled(true );
      ui->type_d_input->setEnabled(true );
      ui->no_i_ramp_input->setEnabled(false);
      ui->no_i_ramp_label->setEnabled(false);
      ui->calcul_d_input->setEnabled(true);
      ui->calcul_d_label->setEnabled(true);
      break;
    }
    case PID_TYPE_PID:
    {
      ui->p_label->setEnabled(true );
      ui->p_input->setEnabled(true );
      ui->i_label->setEnabled(true );
      ui->i_input->setEnabled(true );
      ui->k_label->setEnabled(true );
      ui->k_input->setEnabled(true );
      ui->type_d_label->setEnabled(true );
      ui->type_d_input->setEnabled(true );
      ui->no_i_ramp_input->setEnabled(true);
      ui->no_i_ramp_label->setEnabled(true);
      ui->calcul_d_input->setEnabled(true);
      ui->calcul_d_label->setEnabled(true);
      break;
    }
    case PID_TYPE_I:
    {
      ui->p_label->setEnabled(false);
      ui->p_input->setEnabled(false);
      ui->i_label->setEnabled(true );
      ui->i_input->setEnabled(true );
      ui->k_label->setEnabled(false);
      ui->k_input->setEnabled(false);
      ui->type_d_label->setEnabled(false);
      ui->type_d_input->setEnabled(false);
      ui->no_i_ramp_input->setEnabled(true);
      ui->no_i_ramp_label->setEnabled(true);
      ui->calcul_d_input->setEnabled(false);
      ui->calcul_d_label->setEnabled(false);
      break;
    }
  }
}


CYButtonGroup * CYPIDInput::buttonGroup()
{
  return ui->mButtonGroup;
}

void CYPIDInput::setDescription(const QString &txt)
{
  mDescription = txt;
  ui->desc->setText(mDescription);
}

void CYPIDInput::setIllustration(const QPixmap &p)
{
  mIllustration = p;
  ui->pixmap->setPixmap(mIllustration);
}
