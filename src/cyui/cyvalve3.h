//
// C++ Interface: CYValve33
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYValve33_H
#define CYValve33_H

// CYLIBS
#include "cypixmapview.h"
#include "cydatawdg.h"
//Added by qt3to4:
#include <QString>

/** @short Vanne 3 voies.
  * @author Gérald LE CLEACH
  */

class CYValve3 : public CYPixmapView
{
  Q_OBJECT
  Q_ENUMS(Type)
  Q_PROPERTY(Type type READ type WRITE setType)
  Q_PROPERTY(bool manual READ manual WRITE setManual)
  Q_PROPERTY(QByteArray pixmapFileTrue READ pixmapFileTrue WRITE setPixmapFileTrue DESIGNABLE false STORED false)
  Q_PROPERTY(QByteArray pixmapFileFalse READ pixmapFileFalse WRITE setPixmapFileFalse DESIGNABLE false STORED false)

public:
  /** Type de vanne 3 voies */
  enum Type
  {
    /** Est Sud au repos, Est Ouest piloté.  */
    ESW=0,
    /** Est Ouest au repos, Est Sud piloté.  */
    EWS,
    /** Sud Est au repos, Sud Ouest piloté.  */
    SEW,
    /** Sud Ouest au repos, Sud Est piloté.  */
    SWE,
    /** Ouest Est au repos, Ouest Sud piloté.  */
    WES,
    /** Ouest Sud au repos, Ouest Est piloté.  */
    WSE,
  };

  CYValve3(QWidget *parent=0, const QString &name=0);

  ~CYValve3();

public: // Public methods
  /** @return le type de la vanne @see Type. */
  Type type() const { return mType; }
  /** Saisie du type de la vanne @see Type. */
  void setType(const Type val);
  /** @return \a true si la vanne est manuelle. */
  bool manual() const;
  /** Si \a val vaut \a true alors la vanne est manuelle sinon elle est pilotée. */
  void setManual(const bool val);

private:
  void init(bool value=false);

  /** Type de la vanne @see Type. */
  Type mType;
  /** Vaut \a true si la vanne est manuelle. */
  bool mManual;
};

#endif
