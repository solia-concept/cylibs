/***************************************************************************
                          cytextinput.cpp  -  description
                             -------------------
    début                  : mer sep 3 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cytextinput.h"

// CYLIBS
#include "cystring.h"
#include "cycore.h"
#include "cydatetime.h"

CYTextInput::CYTextInput(QWidget *parent, const QString &name)
  : CYLineEdit(parent,name)
{
  mWriteData = true;
  mAlwaysOk  = true;
}

CYTextInput::~CYTextInput()
{
}

QString CYTextInput::value() const
{
  return text();
}

void CYTextInput::setValue(QString val)
{
  setText(val);
}

void CYTextInput::setData(CYData *data)
{
  if (mData)
  {
    disconnect(mData, SIGNAL(enableColorChanged(const QColor&)), this, SLOT(setEnableColor(const QColor&)));
    disconnect(mData, SIGNAL(disableColorChanged(const QColor&)), this, SLOT(setDisableColor(const QColor&)));
  }
  CYLineEdit::setData(data);
  if (mData)
  {
    connect(mData, SIGNAL(enableColorChanged(const QColor&)), this, SLOT(setEnableColor(const QColor&)));
    connect(mData, SIGNAL(disableColorChanged(const QColor&)), this, SLOT(setDisableColor(const QColor&)));
  }
}


void CYTextInput::setTextChanged(const QString &)
{
  if (mEmitModifie && isVisible())
    emit modifie();
}

void CYTextInput::linkData()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    else
    {
      setText(mDataName);
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  mEmitModifie = false;

  setEnableColor(mData->enableColor());
  setDisableColor(mData->disableColor());
  setType();

  switch (mData->type())
  {
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;                        
                      setValue(data->val());
                      if (echoMode()==QLineEdit::Password)
                        setValue(data->simpleDecrypt());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      if (data->nbChar()>0)
                        setMaxLength(data->nbChar());
                      this->setToolTip(QString(data->inputHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    case Cy::VS64   :
      if (mData->inherits("CYDateTime"))
                    {
                      CYDateTime *data = (CYDateTime *)mData;
                      setValue(data->toString());
                      if (!data->stringDef().isEmpty())
                        mSpecialValue = prefix() + data->stringDef() + suffix();
                      this->setToolTip(QString(data->inputHelp()+"<br>"+data->infoCY()));
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  if (mEnableReadOnly)
  {
    bool readOnly = mReadOnly;
    setReadOnly(false);
    mPalette.setColor(QPalette::Active, QPalette::Text, mData->inputColor());
    QLineEdit::setPalette(mPalette);
    setReadOnly(readOnly);
  }

  ctrlFlag();

  mEmitModifie = true;
}

void CYTextInput::refresh()
{
  ctrlHideFlag();

  if (!hasData())
    return;

  if (!readOnly())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    else
    {
      setText(mDataName);
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }


  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    else
    {
      setText(mDataName);
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }


  if (!mData->isOk() && !alwaysOk())
  {
    setEnabled(false);
    return;
  }

  switch (mData->type())
  {
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      setText(data->val());
                      break;
                    }
    case Cy::VS64 :
      if (mData->inherits("CYDateTime"))
                    {
                      CYDateTime *data = (CYDateTime *)mData;
                      setText(data->toString());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      break;
  }
}

void CYTextInput::designer()
{
  if (!hasData())
    return;

  if (!isVisible())
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    else
    {
      setText(mDataName);
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  switch (mData->type())
  {
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      setValue(data->def());
                      break;
                    }
    case Cy::VS64 :
      if (mData->inherits("CYDateTime"))
                    {
                      CYDateTime *data = (CYDateTime *)mData;
                      setValue(data->toString(data->def(), false, true));
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYTextInput::update()
{
  if (!isVisible() && mUpdateIfVisible)
    return;

  if (mData==0)
  {
    if ( hideIfNoData() )
    {
      hide();
    }
    else
    {
      setText(mDataName);
    }
    return;
  }
  else if (isHidden() && hideIfNoData())
  {
    show();
    ctrlHideFlag();
  }

  if (!isEnabled())
    return;

  if (isHidden())
    return;

  mEmitModifie = false;

  switch (mData->type())
  {
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      data->setVal(value());
                      break;
                    }
    case Cy::VS64 :
      if (mData->inherits("CYDateTime"))
                    {
                      CYDateTime *data = (CYDateTime *)mData;
                      data->setVal(value());
                      break;
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }

  mEmitModifie = true;
}
