/***************************************************************************
                          cyctinput.cpp  -  description
                             -------------------
    begin                : mar avr 13 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyctinput.h"

// QT
#include <QGroupBox>
// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "cytextlabel.h"
#include "ui_cyctinput.h"

CYCTInput::CYCTInput(QWidget *parent, const QString &name)
  : CYWidget(parent,name), ui(new Ui::CYCTInput)
{
  ui->setupUi(this);
  mLevelName = "";
  mShowGroup = wholeGroup;
  setProjectData(false);
}

CYCTInput::~CYCTInput()
{
  delete ui;
}

void CYCTInput::linkDatas()
{
  if (!core)
    return;
  if (!mLevelName.isEmpty())
  {
    ui->level_label->setDataName(mLevelName);
    ui->level->setDataName(mLevelName);
    ui->level_label->show();
    ui->level->show();
  }
  else
  {
    ui->level_label->hide();
    ui->level->hide();
  }
  CYWidget::linkDatas();
  if ((mShowGroup!=No) && ui->inh->CYDataWdg::data())
    ui->buttonGroup->setTitle(ui->inh->CYDataWdg::data()->groupSection((Cy::GroupSection)mShowGroup)+"   ");
}

void CYCTInput::setProjectData(const bool val)
{
  mProjectData = val;
  if (mProjectData)
    ui->projectLabel->show();
  else
    ui->projectLabel->hide();

  updateGeometry();
}
