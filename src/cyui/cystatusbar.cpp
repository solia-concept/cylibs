/* This file is part of the KDE libraries
    Copyright (C) 1997 Mark Donohoe (donohoe@kde.org)
              (C) 1997,1998, 2000 Sven Radej (radej@kde.org)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "cystatusbar.h"

// QT
#include <QSettings>
// CYLIBS
#include "cycore.h"

CYStatusBarLabel::CYStatusBarLabel( const QString& text, int _id,
                                 CYStatusBar *parent, const QString &name) :
  QLabel( parent)
{
  setObjectName(name);
  id = _id;

  setText( text );

  // umm... Mosfet? Can you help here?

  // Warning: QStatusBar draws shaded rectangle around every item - which
  // IMHO is stupid.
  // So NoFrame|Plain is the best you get. the problem is that only in case of
  // StyledPanel|Something you get QFrame to call QStyle::drawPanel().

  setLineWidth  (0);
  setFrameStyle (QFrame::NoFrame);

  // TODO QT5
//  setAlignment( Qt::AlignHCenter | Qt::AlignVCenter | Qt::TextSingleLine );
  setAlignment( Qt::AlignHCenter | Qt::AlignVCenter );


  connect (this, SIGNAL(itemPressed(int)), parent, SIGNAL(pressed(int)));
  connect (this, SIGNAL(itemReleased(int)), parent, SIGNAL(released(int)));
}

void CYStatusBarLabel::mousePressEvent (QMouseEvent *)
{
  emit itemPressed (id);
}

void CYStatusBarLabel::mouseReleaseEvent (QMouseEvent *)
{
  emit itemReleased (id);
}

CYStatusBar::CYStatusBar( QWidget *parent, const QString &name )
  : QStatusBar( parent )
{
  setObjectName(name);
  // make the size grip stuff configurable
  // ...but off by default (sven)
  QSettings *config = core->OSUserConfig();
  QString group(config->group());
  config->beginGroup(QString::fromLatin1("StatusBar style"));
  bool grip_enabled = config->value(QString::fromLatin1("SizeGripEnabled"), false).toBool();
  setSizeGripEnabled(grip_enabled);
  config->beginGroup(group);
}

CYStatusBar::~CYStatusBar ()
{
}

void CYStatusBar::insertItem( const QString& text, int id, int stretch, bool permanent)
{
  if (items[id])
    CYWARNINGTEXT(QString("CYStatusBar::insertItem: item id %1 already exists.").arg(id))

  CYStatusBarLabel *l = new CYStatusBarLabel (text, id, this);
  l->setFixedHeight(fontMetrics().height()+2);
  items.insert(id, l);
  if (permanent)
    addPermanentWidget(l, stretch);
  else
    addWidget(l, stretch);
  l->show();
}

void CYStatusBar::removeItem (int id)
{
  CYStatusBarLabel *l = items[id];
  if (l)
  {
    removeWidget (l);
    items.remove(id);
    delete l;
  }
  else
    CYWARNINGTEXT(QString("CYStatusBar::removeItem: bad item id: %1").arg(id))
}

bool CYStatusBar::hasItem( int id ) const
{
  CYStatusBarLabel *l = items[id];
  if (l)
    return true;
  else
    return false;
}

void CYStatusBar::changeItem( const QString& text, int id )
{
  CYStatusBarLabel *l = items[id];
  if (l)
  {
    l->setText(text);
    if(l->minimumWidth () != l->maximumWidth ())
    {
      reformat();
    }
  }
  else
    CYWARNINGTEXT(QString("CYStatusBar::changeItem: bad item id: %1").arg(id))
}

void CYStatusBar::setItemAlignment (int id, int align)
{
  CYStatusBarLabel *l = items[id];
  if (l)
  {
    l->setAlignment((Qt::Alignment)align);
  }
  else
    CYWARNINGTEXT(QString("CYStatusBar::setItemAlignment: bad item id: %1").arg(id))
}

void CYStatusBar::setItemFixed(int id, int w)
{
  CYStatusBarLabel *l = items[id];
  if (l)
  {
    if (w==-1)
      w=fontMetrics().boundingRect(l->text()).width()+3;

    l->setFixedWidth(w);
  }
  else
    CYWARNINGTEXT(QString("CYStatusBar::setItemFixed: bad item id: %1").arg(id))
}


