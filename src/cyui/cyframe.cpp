/***************************************************************************
                          cyframe.cpp  -  description
                             -------------------
    début                  : sam mai 24 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyframe.h"

// QT
#include <QMouseEvent>
// CYLIBS
#include "cydb.h"
#include "cycore.h"

CYFrame::CYFrame(QWidget *parent, const QString &name, Qt::WindowFlags f)
  : QFrame(parent, f),
    CYTemplate(this)
{
  setObjectName(name);
  mMovable = false;
  mEditPositions = false;
  mType = Cy::Frame;

  mBackgroundColor=palette().color(backgroundRole());
  mForegroundColor=palette().color(foregroundRole());

  mTimer = new QTimer(this);
  connect(mTimer, SIGNAL(timeout()), SLOT(refresh()));
}

CYFrame::~CYFrame()
{
}

QByteArray CYFrame::genericMark() const
{
  return CYTemplate::genericMark();
}

void CYFrame::setGenericMark(const QByteArray txt)
{
  CYTemplate::setGenericMark(txt);
}

void CYFrame::setGenericMark(int index)
{
  CYTemplate::setGenericMark(index);
}

void CYFrame::linkDatas()
{
  mDatasLinked = false;
  mModified = false;
  init(this);
  setValidate();
  setReadOnly(mReadOnly);
  mDatasLinked = true;
}

bool CYFrame::movable() const
{
  return mMovable;
}

void CYFrame::setMovable(const bool val)
{
  mMovable = val;
}


void CYFrame::setBackgroundColor ( const QColor & color )
{
  mBackgroundColor=color;
  setAutoFillBackground(true);
  QPalette pal = palette();
  pal.setColor(backgroundRole(), mBackgroundColor);
  setPalette(pal);
}

const QColor & CYFrame::backgroundColor () const
{
  return mBackgroundColor;
}

void CYFrame::setForegroundColor ( const QColor & color )
{
  mForegroundColor=color;
  QPalette pal = palette();
  pal.setColor(QPalette::WindowText, mForegroundColor);
  setPalette(pal);
}

const QColor & CYFrame::foregroundColor () const
{
  return mForegroundColor;
}


void CYFrame::setBackgroundPixmap( const QPixmap & pixmap )
{
  if (pixmap.isNull())
    return;

  mBackgroundPixmap=pixmap;
  setAutoFillBackground(true);
  QPalette palette;
  palette.setBrush(backgroundRole(), QBrush(pixmap));
  setPalette(palette);
}

const QPixmap & CYFrame::backgroundPixmap () const
{
  return mBackgroundPixmap;
}

void CYFrame::apply()
{
  if (!isVisible())
    return;

  CYTemplate::update(this);

  foreach (CYDB *db, mWriteDB)
    db->apply();

  if (mForcingMode)
  {
    foreach (CYDB *db, mForcingDB)
      if (!mWriteDB.value(db->objectName()))
        db->apply();
  }
  CYTemplate::apply(this);
  setValidate();
}

void CYFrame::update()
{
  if (isVisible())
    CYTemplate::update(this);
}

void CYFrame::refresh()
{
  if (isVisible())
    CYTemplate::refresh(this);
}

void CYFrame::designer()
{
  if (isVisible())
  {
    CYTemplate::designer(this);
    setModifie();
  }
}

void CYFrame::setModifie()
{
  if (isVisible() && mDatasLinked && mEmitModifie)
  {
    if (!isModified())
    {
      mModified = true;
      setModifieView();
    }
    emit modifie();
    emit modified(true);
  }
}

void CYFrame::setValidate()
{
  if (isVisible() && mDatasLinked)
  {
    if (isModified())
    {
      mModified = false;
      setValidateView();
    }
    emit validate();
    emit modified(false);
  }
}

void CYFrame::setReadOnly(bool val)
{
  mReadOnly = val;
  CYTemplate::setReadOnly(val, this);
}

void CYFrame::setForcingMode(bool val)
{
  mForcingMode = val;
  CYTemplate::setForcingMode(val, this);
}

void CYFrame::setSettingMode(bool val)
{
  mSettingMode = val;
  CYTemplate::setSettingMode(val, this);
}

void CYFrame::changeEvent(QEvent* event)
{
  QFrame::changeEvent(event);
}

void CYFrame::showEvent(QShowEvent *e)
{
  if (mLinkDatasShow)
    linkDatas();
  QFrame::showEvent(e);
}


/*! Gestion d'appui de bouton de souris
    \fn CYFrame::mousePressEvent ( QMouseEvent * e )
 */
void CYFrame::mousePressEvent ( QMouseEvent * e )
{
  if ( movable() && !isReadOnly() )
  {
    mXOldPos = x();
    mYOldPos = y();
    mXMousePos = e->x();
    mYMousePos = e->y();
    emit movedInsideParent();
  }
  QFrame::mousePressEvent ( e );
}


/*! Gestion de déplacement de bouton de souris
    \fn CYFrame::mouseMoveEvent ( QMouseEvent * e )
 */
void CYFrame::mouseMoveEvent ( QMouseEvent * e )
{
  if ( movable() && !isReadOnly() )
  {
    int ex = e->x();
    int ey = e->y();
    int x = mXOldPos + ( ex - mXMousePos );
    int y = mYOldPos + ( ey - mYMousePos );
    moveInsideParent( x, y );
    raise();
  }
  QFrame::mouseMoveEvent( e );
}


/*! Déplacement à l'intérieur du parent.
    \fn CYFrame::moveInsideParent(int x, int y)
 */
void CYFrame::moveInsideParent(int x, int y)
{
  QWidget * p = (QWidget *)parent();
  if ( !p->rect().contains(x, p->rect().y()) )
  {
    if ( x < p->rect().left() )
      x = p->rect().left();            // sort à gauche
    else
      x = p->rect().right()-width();   // sort à droite
  }
  else if ( !p->rect().contains(x+width(), p->rect().y()) )
  {
    x = p->rect().right()-width();     // sort en partie à droite
  }

  if ( !p->rect().contains(p->rect().x(), y) )
  {
    if ( y < p->rect().top() )
      y = p->rect().top();             // sort en haut
    else
      y = p->rect().bottom()-height(); // sort en bas
  }
  else if ( !p->rect().contains(p->rect().x(), y+height()) )
  {
    y = p->rect().bottom()-height();   // sort en partie en bas
  }

  move(x, y);
  mXOldPos = x;
  mYOldPos = y;
}


/*! Active/désactive le déplacement
    \fn CYFrame::setEditPositions( bool val )
 */
void CYFrame::setEditPositions( bool val )
{
  mEditPositions = val;
//   if ( mEditPositions )
//     setCursor( Qt::PointingHandCursor );
//   else
  //     unsetCursor();
}

void CYFrame::help()
{
  core->invokeHelp("cydoc", metaObject()->className());
}


/*! @return \a true si la connexion automatique des données lors de l'affichage est autorisée.
    @see CYFrame::setLinkDatasShow(bool val)
    \fn CYFrame::linkDatasShow() const
 */
bool CYFrame::linkDatasShow() const
{
  return CYTemplate::linkDatasShow();
}


/*! Autorise ou non la connexion des données suivant \a val.
    Ceci est utile lorsqu'on veut inhiber la connexion qui est faite automatiquement au moment de l'affichage, par exemple pour le mode forçage.
    \fn CYFrame::setLinkDatasShow(const bool val)
 */
void CYFrame::setLinkDatasShow(const bool val)
{
  CYTemplate::setLinkDatasShow( val );
}
