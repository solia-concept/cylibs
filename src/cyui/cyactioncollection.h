#ifndef CYACTIONCOLLECTION_H
#define CYACTIONCOLLECTION_H

// QT
#include <QHash>
#include <QMenu>
#include <QToolBar>

class CYAction;
class CYActionMenu;
class CYWin;

typedef QList<CYAction *> CYActionPtrList;
//typedef QList<CYActionMenu *> CYMenuPtrList;

class CYActionCollection : public QObject
{
  Q_OBJECT

public:
  CYActionCollection(CYWin *parent = 0, const char * 	name = 0);

  virtual void addAction( CYAction *action );

  virtual CYAction *action( const QString &name );
  virtual CYAction *addActionTo( const QString &actionName, QMenu *pm);
  virtual CYAction *addActionTo( const QString &actionName, QToolBar *tb);

//  virtual void addMenu( CYAction *action );
//  virtual CYActionMenu *menu( const QString &name );
//  virtual CYActionMenu *addMenuTo( const QString &menuName, QMenu *pm);
//  virtual CYActionMenu *addMenuTo( const QString &menuName, QToolBar *tb);

  virtual void setXMLFile( const QString &fileName );
  virtual QString xmlFile();

  /** @return la liste des actions gérées par cette collection d'actions. */
  virtual CYActionPtrList actions() const;
//  /** @return la liste des menus gérés par cette collection d'actions. */
//  virtual CYMenuPtrList menus() const;

  QHash<QString, CYAction*> mActionDict;
//  QHash<QString, CYActionMenu*> mMenuDict;

  const CYWin *window() { return mWin; }

protected:
    CYWin *mWin;
    QString mXMLFile;
};

#endif // CYACTIONCOLLECTION_H
