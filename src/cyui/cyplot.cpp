// QWT
#include "qwt_plot_marker.h"
#include "qwt_plot_curve.h"
#include "qwt_abstract_legend.h"
#include "qwt_plot_grid.h"
#include "qwt_legend.h"
#include "qwt_plot_layout.h"
// CYLIBS
#include "cyplot.h"
#include "cyplotpicker.h"
#include "cyplotcurve.h"
#include "cy.h"

CYPlot::CYPlot(QWidget *parent, const QString &name)
  : QwtPlot(parent)
{
  setObjectName(name);

  QwtPlotGrid *grid = new QwtPlotGrid();
  grid->enableXMin( true );
  grid->setMajorPen( Qt::white, 0, Qt::DotLine );
  grid->setMinorPen( Qt::gray, 0 , Qt::DotLine );
  grid->attach( this );

  mPicker = new CYPlotPicker(canvas());
}

/*!
  \brief Constructor
  \param title Title text
  \param parent Parent widget
  \param name Widget name
 */
CYPlot::CYPlot(const QString &title, QWidget *parent, const QString &name)
  : QwtPlot(title, parent)
{
  setObjectName(name);

  mPicker = new CYPlotPicker(canvas());
}
#ifndef QWT_NO_COMPAT

/*!
  \brief Enables or disables outline drawing.
  When the outline feature is enabled, a shape will be drawn
  in the plotting region  when the user presses
  or drags the mouse. It can be used to implement crosshairs,
  mark a selected region, etc.
  \param tf \c true (enabled) or \c false (disabled)
  \warning An outline style has to be specified.
  \sa setOutlineStyle()
*/
void CYPlot::enableOutline(bool tf)
{
  mPicker->setEnabled(tf);
}

/*!
  \brief Specify the style of the outline

  The outline style determines which kind of shape is drawn
  in the plotting region when the user presses a mouse button
  or drags the mouse.
  \sa QwtPicker::RubberBand
*/
void CYPlot::setOutlineStyle(QwtPicker::RubberBand  os)
{
  mPicker->setRubberBand(os);
}

/*!
  \brief Specify a pen for the outline
  \param pn new pen
*/
void CYPlot::setOutlinePen(const QPen &pn)
{
  mPicker->setRubberBandPen(pn);
}

/*!
  \return \c true if the outline feature is enabled
*/
bool CYPlot::outlineEnabled() const
{
  return mPicker->isEnabled();
}

/*!
  \return the pen used to draw outlines
*/
QPen CYPlot::outlinePen() const
{
  return mPicker->rubberBandPen();
}

/*!
  \return the outline style
  \sa setOutlineStyle()
*/
QwtPicker::RubberBand CYPlot::outlineStyle() const
{
  return mPicker->rubberBand();
}

#endif // ! QWT_NO_COMPAT


/*!
  \brief Generate a unique key for a new curve
  \return new unique key or 0 if no key could be found.
*/
long CYPlot::newCurveKey()
{
  long newkey = itemList(QwtPlotItem::Rtti_PlotCurve).count() + 1;

  if ( newkey > 1 )                   // size > 0: check if key exists
  {
    if ( curve( newkey ) )     // key size+1 exists => there must be a
      // free key <= size
    {
      // find the first available key <= size
      newkey = 1;
      while ( newkey <= long ( itemList(QwtPlotItem::Rtti_PlotCurve).count() ) )
      {
        if ( curve( newkey ) )
          newkey++;
        else
          break;
      }

      // This can't happen. Just paranoia.
      if ( newkey > long ( itemList(QwtPlotItem::Rtti_PlotCurve).count() ) )
      {
        while ( !curve( newkey ) )
        {
          newkey++;
          if ( newkey > 10000 ) // prevent infinite loop
          {
            newkey = 0;
            break;
          }
        }
      }
    }
  }
  return newkey;
}

/*!
  \brief Insert a curve
  \param curve Curve
  \return The key of the new curve or 0 if no new key could be found
          or if no new curve could be allocated.
*/

long CYPlot::insertCurve ( QwtPlotCurve *curve )
{
  if ( curve == 0 )
    return 0;

  long key = newCurveKey();
  curve->attach( this );
  return key;
}

/*!
  \brief Insert a new curve and return a unique key
  \param title title of the new curve
  \param xAxis x axis to be attached. Defaults to xBottom.
  \param yAxis y axis to be attached. Defaults to yLeft.
  \return The key of the new curve or 0 if no new key could be found
          or if no new curve could be allocated.
*/
long CYPlot::insertCurve ( const QString &title, int xAxis, int yAxis )
{
  CYPlotCurve *curve = new CYPlotCurve();
  if ( !curve )
    return 0;

  curve->setAxes ( xAxis, yAxis );
  curve->setTitle ( title );

  long key = insertCurve ( curve );
  /*TODO QT5
  if ( key == 0 )
    delete curve;*/

  if ( key > 0 )
    curve->setKey(key);
  return key;
}

/*!
  \brief Find and return an existing curve.
  \param key Key of the curve
  \return The curve for the given key or 0 if key is not valid.
*/
QwtPlotCurve *CYPlot::curve ( long key )
{
  QwtPlotItemList curveList = itemList(QwtPlotItem::Rtti_PlotCurve);

  if ((key>0) && (key<=curveList.size()))
    return (QwtPlotCurve *)curveList.at(key-1);
  return 0;
}

/*!
  \brief Find and return an existing curve.
  \param key Key of the curve
  \return The curve for the given key or 0 if key is not valid.
*/
const QwtPlotCurve *CYPlot::curve ( long key ) const
{
  QwtPlotItemList curveList = itemList(QwtPlotItem::Rtti_PlotCurve);
  return (QwtPlotCurve *)curveList.at(key);
}

/*!
  \brief remove the curve indexed by key
  \param key Key of the curve
*/
bool CYPlot::removeCurve ( long key )
{
  curve(key)->detach();

  // TOCHECK QWT
  //  bool ok = d_curves->remove ( key );
  //  if ( !ok )
  //    return false;

  //  QWidget *item = d_legend->findItem ( key );
  //  if ( item )
  //  {
  //    delete item;
  //    updateLayout();
  //  }

  //  autoRefresh();
  return true;
}


/*!
  \brief  Initialize the curve data by pointing to memory blocks which are not
  managed by CYScopePlotter.

  \param key Key of the curve
  \param xData pointer to x data
  \param yData pointer to y data
  \param size size of x and y
  \param yCoef coef applied to y values
  \param yOffset offset applied to y values
  \return \c true if the curve exists

  \warning setRawData is provided for efficiency.  The programmer should not
  delete the data during the lifetime of the underlying QwtCPointerData class.

  \sa CYPlot::setCurveData(), QwtPlotCurve::setRawData
*/
bool CYPlot::setCurveRawData ( long key,
                               const double *xData, const double *yData, int size, double yCoef, double yOffset)
{
  QwtPlotCurve *c = curve( key );
  if ( !c )
    return false;

  c->setRawSamples ( xData, yData, size, yCoef, yOffset );
  return true;
}

/*!
  \brief Set curve data by copying x- and y-values from specified blocks.
  Contrary to \b CYPlot::setCurveRawData, this function makes a 'deep copy' of
  the data.

  \param key curve key
  \param xData pointer to x values
  \param yData pointer to y values
  \param size size of xData and yData
  \param yCoef coef applied to y values
  \param yOffset offset applied to y values
  \return \c true if the curve exists

  \sa setCurveRawData(), QwtPlotCurve::setData
*/
bool CYPlot::setCurveData ( long key,
                            const double *xData, const double *yData, int size, double yCoef, double yOffset )
{
  QwtPlotCurve *c = curve( key );
  if ( !c )
    return false;

  if (!xData || !yData)
    return false;

  c->setSamples( xData, yData, size, yCoef, yOffset );
  return true;
}

//! Generate a key for a new marker
long CYPlot::newMarkerKey()
{
  long newkey = itemList(QwtPlotItem::Rtti_PlotMarker).count() + 1;

  if (newkey > 1)                     // count > 0
  {
    if (marker(newkey))    // key count+1 exists => there must be a
      // free key <= count
    {
      // find the first available key <= count
      newkey = 1;
      while ( newkey <= long ( itemList(QwtPlotItem::Rtti_PlotMarker).count() ) )
      {
        if (marker(newkey))
          newkey++;
        else
          break;
      }

      // This can't happen. Just paranoia.
      if (newkey > long(itemList(QwtPlotItem::Rtti_PlotMarker).count()))
      {
        while (!marker(newkey))
        {
          newkey++;
          if (newkey > 10000) // prevent infinite loop
          {
            newkey = 0;
            break;
          }
        }
      }
    }
  }
  return newkey;

}

/*!
  This function is a shortcut to insert a horizontal or vertical
  line marker, dependent on the specified axis.
  \param label Label
  \param axis Axis to be attached
  \return New key if the marker could be inserted, 0 if not.
*/
long CYPlot::insertLineMarker(const QString &label, int axis)
{
  QwtPlotMarker::LineStyle lineStyle = QwtPlotMarker::NoLine;
  int xAxis = CYPlot::xBottom;
  int yAxis = CYPlot::yLeft;

  switch(axis)
  {
    case yLeft:
    case yRight:
      yAxis = axis;
      lineStyle = QwtPlotMarker::HLine;
      break;
    case xTop:
    case xBottom:
      xAxis = axis;
      lineStyle = QwtPlotMarker::VLine;
      break;
  }

  QwtPlotMarker *marker = new QwtPlotMarker();
  if ( marker == 0 )
    return 0;

  marker->setAxes(xAxis, yAxis);
  marker->setLabel(label);
  marker->setLineStyle(lineStyle);
  marker->setLabelAlignment(Qt::AlignRight|Qt::AlignTop);

  long key = insertMarker(marker);
  return key;
}

/*!
  \brief Insert a new marker
  \param label Label
  \param xAxis X axis to be attached
  \param yAxis Y axis to be attached
  \return New key if the marker could be inserted, 0 if not.
*/
long CYPlot::insertMarker(const QString &label, int xAxis, int yAxis)
{
  QwtPlotMarker *marker = new QwtPlotMarker();
  if ( marker == 0 )
    return 0;

  marker->setAxes(xAxis, yAxis);
  marker->setLabel(label);

  long key = insertMarker(marker);
  if ( key == 0 )
    delete marker;

  return key;
}

/*!
  \brief Insert a new marker
  \param marker Marker
  \return New key if the marker could be inserted, 0 if not.
*/
long CYPlot::insertMarker(QwtPlotMarker *marker)
{
  if ( marker == 0 )
    return 0;

  long key = newMarkerKey();
  //    if ( key == 0 )
  //        return 0;

  // TOCHECK QWT
  marker->attach( this );
  //    marker->reparent(this);
  //    d_markers->insert(key, marker);

  autoRefresh();

  return key;
}

/*!
  \brief Find and return an existing marker.
  \param key Key of the marker
  \return The marker for the given key or 0 if key is not valid.
*/

QwtPlotMarker *CYPlot::marker(long key)
{
  QwtPlotItemList list = itemList(QwtPlotItem::Rtti_PlotMarker);

  if ((key>0) && (key<=list.size()))
    return (QwtPlotMarker *)list.at(key-1);
  return 0;
}

/*!
  \brief Find and return an existing marker.
  \param key Key of the marker
  \return The marker for the given key or 0 if key is not valid.
*/

const QwtPlotMarker *CYPlot::marker(long key) const
{
  QwtPlotItemList list = itemList(QwtPlotItem::Rtti_PlotMarker);

  if (key<list.size())
    return (QwtPlotMarker *)list.at(key);
  else
    return 0;
}

///*!
//  \return an array containing the keys of all markers
//*/
//QwtArraySeriesData<long> CYPlot::markerKeys() const
//{
//  itemList(QwtPlotItem::Rtti_PlotMarker).count()
//    QwtArraySeriesData<long> keys(d_markers->count());

//    int i = 0;

//    QwtPlotMarkerIterator itm = markerIterator();
//    for (const QwtPlotMarker *m = itm.toFirst(); m != 0; m = ++itm, i++ )
//        keys[i] = itm.currentKey();

//    return keys;
//}

/*!
  \return the font of a marker
*/
QFont CYPlot::markerFont(long key) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
    return m->label().font();
  else
    return QFont();
}

/*!
  \return a marker's label
  \param key Marker key
*/
const QString CYPlot::markerLabel(long key) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
    return m->label().text();
  else
    return QString();
}

/*!
  \return a marker's label alignment
  \param key Marker key
*/
int CYPlot::markerLabelAlign(long key) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
    return m->labelAlignment();
  else
    return 0;
}

/*!
  \return the pen of a marker's label
  \param key Marker key
*/
QPen CYPlot::markerLabelPen(long key) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
    return m->label().borderPen();
  else
    return QPen();

}

/*!
  \return a marker's line pen
  \param key Marker key
*/
QPen CYPlot::markerLinePen(long key) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
    return m->linePen();
  else
    return QPen();

}

/*!
  \return a marker's line style
  \param key Marker key
*/
QwtPlotMarker::LineStyle CYPlot::markerLineStyle(long key) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
    return m->lineStyle();
  else
    return QwtPlotMarker::NoLine;
}

/*!
  \brief Get the position of a marker
  \param key Marker key
  \retval mx
  \retval my Marker position
*/

void CYPlot::markerPos(long key, double &mx, double &my ) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
  {
    mx = m->xValue();
    my = m->yValue();
  }
  else
  {
    mx = 0;
    my = 0;
  }
}

/*!
  \return a marker's symbol
  \param key Marker key
*/
const QwtSymbol *CYPlot::markerSymbol(long key) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
    return m->symbol();
  else
    return new QwtSymbol();
}

bool CYPlot::showMarker(long key, bool show)
{
  QwtPlotMarker *m = marker(key);
  if (m)
  {
    if (show)
      m->show();
    else
      m->hide();
    return true;
  }
  return false;
}

/*!
  \return the x axis to which a marker is attached
  \param key Marker key
*/
int CYPlot::markerXAxis(long key) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
    return m->xAxis();
  else
    return -1;

}


/*!
  \return the y axis to which a marker is attached
  \param key Marker key
*/
int CYPlot::markerYAxis(long key) const
{
  const QwtPlotMarker *m = marker(key);
  if (m)
    return m->yAxis();
  else
    return -1;

}

/*!
  \brief Remove the marker indexed by key
  \param key unique key
*/
bool CYPlot::removeMarker(long key)
{
  QwtPlotMarker *m=marker(key);
  if (m)
  {
    m->detach();
    autoRefresh();
    return true;
  }
  else
    return false;
}

//! Remove all markers
void CYPlot::removeMarkers()
{
  long key=long(itemList(QwtPlotItem::Rtti_PlotMarker).count());
  // This can't happen. Just paranoia.
  while (key>0)
  {
    removeMarker(key);
    key = long(itemList(QwtPlotItem::Rtti_PlotMarker).count());
  }

  autoRefresh();
}


/*!
  \brief Attach the marker to an x axis
  \return \c true if the specified marker exists.
*/
bool CYPlot::setMarkerXAxis(long key, int axis)
{
  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setXAxis(axis);
    return true;
  }
  else
    return false;
}

/*!
  \brief Attach the marker to a Y axis
  \param key Marker key
  \param axis Axis to be attached
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerYAxis(long key, int axis)
{
  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setYAxis(axis);
    return true;
  }
  else
    return false;
}

/*!
  \brief Specify a font for a marker's label
  \param key Marker key
  \param f New font
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerFont(long key, const QFont &f)
{
  int rv = false;

  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->label().setFont(f);
    rv = true;
  }
  return rv;
}

/*!
  \brief Specify a pen for a marker's line
  \param key Marker key
  \param p New pen
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerLinePen(long key, const QPen &p)
{
  int rv = false;

  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setLinePen(p);
    rv = true;
  }
  return rv;

}


/*!
  \brief Specify the line style for a marker
  \param key Marker key
  \param st Line style: <code>QwtPlotMarker::HLine, QwtPlotMarker::VLine,
                        QwtPlotMarker::NoLine</code> or a combination of them.
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerLineStyle(long key, QwtPlotMarker::LineStyle st)
{
  int rv = false;
  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setLineStyle(st);
    rv = true;
  }
  return rv;
}

/*!
  \brief Specify a pen for a marker's label.
  \param key Marker key
  \param p New pen
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerPen(long key, const QPen &p)
{
  int rv = false;

  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setLinePen(p);
    m->label().setBorderPen(p);
    rv = true;
  }
  return rv;
}


/*!
  \brief Change the position of a marker
  \param key Marker key
  \param xval
  \param yval Position of the marker in axis coordinates.
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerPos(long key, double xval, double yval)
{
  int rv = false;

  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setXValue(xval);
    m->setYValue(yval);
    rv = true;
  }
  return rv;
}

/*!
  \brief Specify the X position of a marker
  \param key Marker key
  \param val X position of the marker
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerXPos(long key, double val)
{
  int rv = false;

  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setXValue(val);
    rv = true;
  }
  return rv;
}

/*!
  \brief Specify the Y position of the marker
  \param key Marker key
  \param val Y position of the marker
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerYPos(long key, double val)
{
  int rv = false;

  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setYValue(val);
    rv = true;
  }
  return rv;
}

/*!
  \brief Assign a symbol to a specified marker
  \param key Marker key
  \param s new symbol
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerSymbol(long key, const QwtSymbol *s)
{
  int rv = false;
  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setSymbol(s);
    rv = true;
  }
  return rv;
}

/*!
  \brief Assign a text to the label of a marker
  \param key Marker key
  \param text Label text
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerLabelText(long key, const QString &text)
{
  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->label().setText(text);
    return true;
  }
  return false;
}

/*!
  \brief Set the marker label
  \param key Marker key
  \param text the label text
  \param font the font of the label text
  \param color the color of the label text
  \param pen the pen of the bounding box of the label text
  \param brush the brush of the bounding box of the label text
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerLabel(long key, const QString &text, const QFont &font,
                            const QColor &color, const QPen &pen, const QBrush &brush)
{
  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    // TOCHECK QWT
    m->label().setText(text);
    m->label().setFont(font);
    m->label().setColor(color);
    m->label().setBorderPen(pen);
    m->label().setBackgroundBrush(brush);
    return true;
  }
  return false;
}

/*!
  \brief Specify the alignment of a marker's label

  The alignment determines the position of the label relative to the
  marker's position. The default setting is AlignCenter.
  \param key Marker key
  \param align Alignment: AlignLeft, AlignRight, AlignTop, AlignBottom,
                          AlignHCenter, AlignVCenter, AlignCenter
                          or a combination of them.
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerLabelAlign(long key, int align)
{
  int rv = false;
  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->setLabelAlignment((Qt::Alignment)align);
    rv = true;
  }
  return rv;
}

/*!
  \brief Specify a pen for a marker's label
  \param key Marker key
  \param p Label pen
  \return \c true if the specified marker exists
*/
bool CYPlot::setMarkerLabelPen(long key, const QPen &p)
{
  int rv = false;
  QwtPlotMarker *m;
  if ((m = marker(key)))
  {
    m->label().setBorderPen(p);
    rv = true;
  }
  return rv;
}

/*!
  \brief Enable or disable the legend
  \param enable \c true (enabled) or \c false (disabled)
  \param curveKey Key of a existing curve.
                  If curveKey < 0 the legends for all
                  curves will be updated.
  \sa QwtPlot::setAutoLegend()
  \sa QwtPlot::setLegendPos()
*/
void CYPlot::enableLegend(bool enable, long curveKey)
{
  Q_UNUSED(curveKey)
  insertLegend( NULL );

  if ( enable )
  {
    //      if ( settings.legend.position > QwtPlot::TopLegend )
    //      {
    //          if ( legend() )
    //          {
    //              // remove legend controlled by the plot
    //              insertLegend( NULL );
    //          }
    //      }
    //      else
    {
      //          if ( legend() == NULL ||
      //              plotLayout()->legendPosition() != settings.legend.position )
      //      if ( legend() == NULL ||
      //           plotLayout()->legendPosition() != BottomLegend )
      if ( legend() == NULL )
      {
        insertLegend( new QwtLegend(),
                      QwtPlot::RightLegend );
      }
    }
  }
  else
  {
    insertLegend( NULL );
  }
  //  if (legend())
  //  {
  //    bool isUpdateEnabled = legend()->updatesEnabled();
  //    legend()->setUpdatesEnabled(false);
  //  }
  //    if ( curveKey < 0 ) // legends for all curves
  //    {
  //        if ( enable )
  //        {
  //            if ( d_legend->itemCount() < d_curves->count() )
  //            {
  //                // not all curves have a legend

  //                d_legend->clear();

  //                QwtPlotCurveIterator itc = curveIterator();
  //                for ( const QwtPlotCurve *curve = itc.toFirst();
  //                    curve != 0; curve = ++itc )
  //                {
  //                    insertLegendItem(itc.currentKey());
  //                }
  //            }
  //        }
  //        else
  //        {
  //            d_legend->clear();
  //        }
  //    }
  //    else
  //    {
  //        QWidget *legendItem = d_legend->findItem(curveKey);
  //        if ( enable )
  //        {
  //            if ( d_curves->find(curveKey) && !legendItem )
  //                insertLegendItem(curveKey);
  //        }
  //        else
  //            delete legendItem;
  //    }

  //    d_legend->setUpdatesEnabled(isUpdateEnabled);
  //    updateLayout();
}

/*!
  \brief Modifies a color for printing
  \param c Color to be modified
  \return Modified color.
*/
QColor CYPlot::printColor(const QColor &c) const
{
  if ((c.red() >= 230) && (c.green() >= 230) && (c.blue() >= 230))
    return c.darker();
  return c;
}
