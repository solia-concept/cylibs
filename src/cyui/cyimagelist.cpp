//
// C++ Implementation: cyimagelist
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <QFileInfo>
#include <QDir>
//Added by qt3to4:
#include <QPixmap>
#include <QTextStream>
// CYLIBS
#include "cyimagelist.h"
#include "cycore.h"
#include "cydb.h"
#include "cymessagebox.h"
#include "cyglobal.h"

CYImageList::CYImageList(QObject *parent, const QString &name)
 : QObject(parent)
{
  setObjectName(name);
}


CYImageList::~CYImageList()
{
}

/*! Chargement de la liste des images.
    \fn CYImageList::load(const QString &fileName)
 */
bool CYImageList::load(const QString &fileName)
{
  QFile file(mFileName = fileName);
  if ( !file.exists() )
    return true;

  if (!file.open(QIODevice::ReadOnly))
  {
    CYMessageBox::sorry(0, tr("Can't open the file %1").arg(mFileName));
    return (false);
  }

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  QString errorMsg;
  int errorLine;
  int errorColumn;
  if (!doc.setContent(&file, &errorMsg, &errorLine, &errorColumn))
  {
    CYMessageBox::sorry(0, tr("The file %1 does not contain valid XML\n"
                                  "%2: line:%3 colomn:%4").arg(mFileName).arg(errorMsg).arg(errorLine).arg(errorColumn));
    return (false);
  }
  // Vérifie le type propre du document.
  if (doc.doctype().name() != "CYLIXImageList")
  {
    CYMessageBox::sorry(0, QString(tr("The file %1 does not contain a valid"
                                          "definition, which must have a document type ").arg(mFileName)
                                          +"'CYLIXImageList'"));
    return (false);
  }
  // Vérifie la taille propre.
  QDomElement el = doc.documentElement();

  // Version de la librairie lors de la sauvegarde du document.
  mVersionCYDOM = el.attribute("CYDOM");

  /* Charge les images. */
  QDomNodeList el_images = el.elementsByTagName("image");
  clear();
  for (int i = 0; i < el_images.count(); ++i)
  {
    QDomElement item = el_images.item(i).toElement();
    CYImage *image = new CYImage( this );
    image->loadFromDOM(item);
    addImage(image);
  }
  return (true);
}


bool CYImageList::save(const QString &fileName)
{
  QFileInfo fileInfo(fileName);
  core->mkdir(fileInfo.absolutePath());

  QDomDocument doc("CYLIXImageList");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  // enregistre les informations la feuille de données.
  QDomElement el_list = doc.createElement("CYImageList");
  doc.appendChild(el_list);

  el_list.setAttribute("CYDOM", core->version());
  mVersionCYDOM = core->version();

  QDomElement el_images = doc.createElement("images");
  el_list.appendChild(el_images);
  // enregistre les images. */
  QListIterator<CYImage *> it(images);
  while (it.hasNext())
  {
    CYImage *image = it.next();
    QDomElement el_image = doc.createElement("image");
    el_images.appendChild(el_image);
    image->saveToDOM( el_image );
  }

  QFile file(mFileName = fileName);
  if (!file.open(QIODevice::WriteOnly))
  {
    CYWARNINGTEXT(tr("Can't save file %1!").arg(mFileName));
    return (false);
  }
  QTextStream s(&file);
  s.setCodec("UTF-8");
  s << doc;
  file.close();
  core->backupFile(mFileName);

//   setModified(false);
  return (true);
}

void CYImageList::addImage(CYImage *image)
{
  images.append(image);
}

void CYImageList::removeImage(CYImage *image)
{
  int i = images.indexOf(image);
  if (i != -1)
    delete images.takeAt(i);
}


/*! Ajoute une image à partir de son \a fileName.
    \fn CYImageList::addImage(int index, QString label, QString fileName)
 */
CYImage *CYImageList::addImage(int index, QString label, QString fileName)
{
  CYImage *image = new CYImage(this, index, label, fileName);
  addImage(image);
  return image;
}


/*! Supprime la liste
    \fn CYImageList::clear()
 */
void CYImageList::clear()
{
  while (!images.isEmpty())
    delete images.takeFirst();
}


/*! @return l'image suivant l' \a index
    \fn CYImageList::image(uint index)
 */
CYImage * CYImageList::image(uint index)
{
  return images.at(index);
}

/*! @return l'image en pixmap suivant l' \a index
    \fn CYImageList::pixmap(uint index)
 */
QPixmap CYImageList::pixmap(uint index)
{
  // TOCHECK QT4
//  return QPixmap(image(index)->fileName().toUtf8()).toImage();
  return QPixmap(image(index)->fileName().toUtf8());
}
