/***************************************************************************
                          cyboxana.h  -  description
                             -------------------
    début                  : jeu mar 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYBOXANA_H
#define CYBOXANA_H

// CYLIBS
#include <cyboxio.h>

class CYData;
class CYCardANA;

/** CYBoxANA est un ensemble de widgets représentant une E/S ANA.
  * @short Représentation d'une E/S ANA.
  * @author Gérald LE CLEACH
  */

class CYBoxANA : public CYBoxIO
{
  Q_OBJECT

public:
  /** Construit un objet représentant une E/S ANA.
    * @param parent  Widget parent.
    * @param name    Nom du widget.
    * @param m       Mode d'utilisation du widget. */
  CYBoxANA(QWidget *parent=0, const QString &name=0, Mode m=Forcing);
  /** Destructeur */
  ~CYBoxANA();

  /** Initialise le widget. */
  virtual void init() {}
  /** Associe une donnée à l'objet.
    * @param data Donnée à traîter. */
  virtual void setData(CYData *data) { Q_UNUSED(data) }

public slots: // Public slots
  virtual void refresh() {}
  /** Simulation. */
  virtual void simulation() {}

protected:
  /** Met à jour l'objet. */
  virtual void update() {}
};

#endif
