/***************************************************************************
                          cylevelview.cpp  -  description
                             -------------------
    begin                : dim jan 25 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cylevelview.h"

// QT
#include <qpainter.h>
//Added by qt3to4:
#include <QString>
#include <QWheelEvent>
// QWT
#include "qwt_scale_draw.h"
#include "qwt_scale_map.h"
#include "qwt_scale_div.h"
#include "qwt_color_map.h"
// CYLIBS
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyword.h"
#include "cyflag.h"
#include "cycore.h"

static inline void cywtDrawLine( QPainter *painter, int pos,
                                 const QColor &color, const QRect &pipeRect, const QRect &liquidRect,
                                 Qt::Orientation orientation )
{
  painter->setPen( color );
  if ( orientation == Qt::Horizontal )
  {
    if ( pos >= liquidRect.left() && pos < liquidRect.right() )
      painter->drawLine( pos, pipeRect.top(), pos, pipeRect.bottom() );
  }
  else
  {
    if ( pos >= liquidRect.top() && pos < liquidRect.bottom() )
      painter->drawLine( pipeRect.left(), pos, pipeRect.right(), pos );
  }
}


QVector<double> cywtTickList( const QwtScaleDiv &scaleDiv )
{
  QVector<double> values;

  double lowerLimit = scaleDiv.interval().minValue();
  double upperLimit = scaleDiv.interval().maxValue();

  if ( upperLimit < lowerLimit )
    qSwap( lowerLimit, upperLimit );

  values += lowerLimit;

  for ( int tickType = QwtScaleDiv::MinorTick;
        tickType < QwtScaleDiv::NTickTypes; tickType++ )
  {
    const QList<double> ticks = scaleDiv.ticks( tickType );

    for ( int i = 0; i < ticks.count(); i++ )
    {
      const double v = ticks[i];
      if ( v > lowerLimit && v < upperLimit )
        values += v;
    }
  }

  values += upperLimit;

  return values;
}


CYLevelView::CYLevelView(QWidget *parent, const QString &name)
  : QwtThermo(parent),
    CYDataWdg(this)
{
  setObjectName(name);
  mAlarmInversed = true;
  mAlarmState = false;
  setSensInversed(false);
  setAutoFillBackground(true);
  setScalePosition(NoScale);
  setPipeWidth(0);
  setUpperBound(100.0);
  setLowerBound(0.0);
  setValue(0.0);
  setAlarmLevel(80.0);
  setFillColor(Qt::green);
  setAlarmColor(Qt::red);

}

CYLevelView::~CYLevelView()
{
}

void CYLevelView::setDataName(const QByteArray &name)
{
  CYDataWdg::setDataName(name);
}

QByteArray CYLevelView::dataName() const
{
  return CYDataWdg::dataName();
}

void CYLevelView::setFlagName(const QByteArray &name)
{
  CYDataWdg::setFlagName(name);
}

QByteArray CYLevelView::flagName() const
{
  return CYDataWdg::flagName();
}

void CYLevelView::setHideFlagName(const QByteArray &name)
{
  CYDataWdg::setHideFlagName(name);
}

QByteArray CYLevelView::hideFlagName() const
{
  return CYDataWdg::hideFlagName();
}

void CYLevelView::setBenchType(const QByteArray &name)
{
  CYDataWdg::setBenchType(name);
}

QByteArray CYLevelView::benchType() const
{
  return CYDataWdg::benchType();
}

void CYLevelView::setSensInversed(const bool flag)
{
  mSensInversed = flag;
}

void CYLevelView::setInverseFlag(const bool inverse)
{
  mInverseFlag = inverse;
}

bool CYLevelView::inverseFlag() const
{
  return mInverseFlag;
}

void CYLevelView::setInverseHideFlag(const bool inverse)
{
  CYDataWdg::setInverseHideFlag(inverse);
}

bool CYLevelView::inverseHideFlag() const
{
  return CYDataWdg::inverseHideFlag();
}

void CYLevelView::setData(CYData *data)
{
  if ( mData)
  {
    disconnect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
  CYDataWdg::setData( data );
  if ( mData)
  {
    connect( mData, SIGNAL( formatUpdated() ), this, SLOT( linkData() ) );
  }
}


void CYLevelView::linkData()
{
  CYDataWdg::linkData();

  if (!hasData() || !mData)
    return;

  this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()));

  switch (mData->type())
  {
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mData;
      setValue(data->val());
      setScale(0.0, 1.0);
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      setValue(data->val());
      setScale((double)data->min(), (double)data->max());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
  }
}

void CYLevelView::refresh()
{
//  if (core && core->simulation())
//    return;

  ctrlHideFlag();

  if (!hasData())
    return;

  if (mData==0)
    return;
  if (!isVisible())
    return;

  if (!ctrlFlag())
  {
    setEnabled(false);
    return;
  }
  if (mData==0)
    return;
  if (!core || !core->simulation())
  {
    if (!mData->isOk())
    {
      setEnabled(false);
      return;
    }
    if (mData->flag()!=0)
    {
      if (!mData->flag()->val())
      {
        setEnabled(false);
        return;
      }
    }
  }


  switch (mData->type())
  {
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mData;
      setValue(data->val());
      return;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mData;
      setValue(data->val());
      return;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mData;
      setValue(data->val());
      return;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
      break;
  }
}

bool CYLevelView::alarmState(double val)
{
  if (mSensInversed)
  {
    val = (upperBound() - lowerBound()) - val;
  }
  const bool inverted = ( upperBound() < lowerBound() );
  bool state=false;
  //
  //  Determine if value exceeds alarm threshold.
  //  Note: The alarm value is allowed to lie
  //        outside the interval (minValue, maxValue).
  //
  if (alarmEnabled())
  {
    if (!mAlarmInversed)
    {
      if (inverted)
      {
        state = ((alarmLevel() >= upperBound())
                 && (alarmLevel() <= lowerBound())
                 && (val >= alarmLevel()));

      }
      else
      {
        state = (( alarmLevel() >= lowerBound())
                 && (alarmLevel() <= upperBound())
                 && (val >= alarmLevel()));
      }
    }
    else
    {
      if (inverted)
      {
        state = ((alarmLevel() >= upperBound())
                 && (alarmLevel() <= lowerBound())
                 && (val <= alarmLevel()));

      }
      else
      {
        state = ( (val <= lowerBound())
                  || (val >= upperBound())
                  || (val <= alarmLevel()));
      }
    }
  }
  return state;
}

/*!
  \brief Calculate the alarm rectangle of the pipe

  \param fillRect Filled rectangle in the pipe
  \return Rectangle to be filled with the alarm brush

  \sa pipeRect(), fillRect(), alarmLevel(), alarmBrush()
 */
QRect CYLevelView::alarmRect( const QRect &fillRect ) const
{
  int taval = 0;
  QRect alarmRect( 0, 0, -1, -1); // something invalid
  double val = value();

  if ( !alarmEnabled() )
    return alarmRect;

  if (mSensInversed)
  {
    val = (upperBound() - lowerBound()) - val;
  }
  const bool inverted = ( upperBound() < lowerBound() );

  //
  //  transform values
  //
  const QwtScaleMap map = scaleDraw()->scaleMap();
  int tval = qRound( map.transform( value() ) );
  if (mAlarmState)
  {
    taval = qRound( map.transform( value() ) );
  }
  //
  //  calculate recangles
  //
  if ( orientation() == Qt::Horizontal )
  {
      if (inverted)
      {
          if (mAlarmState)
          {
              alarmRect.setRect(tval, fillRect.y(), taval - tval + 1, fillRect.height());

          }
      }
      else
      {
          if (mAlarmState)
          {
              alarmRect.setRect(taval, fillRect.y(), tval - taval + 1, fillRect.height());
          }

      }
  }
  else // Qt::Vertical
  {
      if (tval < fillRect.y())
          tval = fillRect.y();
      else
      {
          if (tval > fillRect.y() + fillRect.height())
              tval = fillRect.y() + fillRect.height();
      }

      if (inverted)
      {
          if (mAlarmState)
          {
              alarmRect.setRect(fillRect.x(), taval, fillRect.width(), tval - taval + 1);
          }
      }
      else
      {
          if (mAlarmState)
          {
              alarmRect.setRect(fillRect.x(),tval, fillRect.width(), taval - tval + 1);
          }
      }
  }

  return alarmRect;
}

/*!
   Redraw the liquid in thermometer pipe.
   \param painter Painter
   \param pipeRect Bounding rectangle of the pipe without borders
*/
void CYLevelView::drawLiquid(
    QPainter *painter, const QRect &pipeRect ) const
{
  painter->save();
  painter->setClipRect( pipeRect, Qt::IntersectClip );
  painter->setPen( Qt::NoPen );

  const QwtScaleMap scaleMap = scaleDraw()->scaleMap();

  QRect liquidRect = fillRect( pipeRect );

  if ( colorMap() != NULL )
  {
    const QwtInterval interval = scaleDiv().interval().normalized();

    // Because the positions of the ticks are rounded
    // we calculate the colors for the rounded tick values

    QVector<double> values = cywtTickList( scaleDraw()->scaleDiv() );

    if ( scaleMap.isInverting() )
      std::sort( values.begin(), values.end(), std::greater<double>() );
    else
      std::sort( values.begin(), values.end(), std::less<double>() );

    int from;
    if ( !values.isEmpty() )
    {
      from = qRound( scaleMap.transform( values[0] ) );
      cywtDrawLine( painter, from,
                    colorMap()->color( interval, values[0] ),
          pipeRect, liquidRect, orientation() );
    }

    for ( int i = 1; i < values.size(); i++ )
    {
      const int to = qRound( scaleMap.transform( values[i] ) );

      for ( int pos = from + 1; pos < to; pos++ )
      {
        const double v = scaleMap.invTransform( pos );

        cywtDrawLine( painter, pos,
                      colorMap()->color( interval, v ),
                      pipeRect, liquidRect, orientation() );
      }

      cywtDrawLine( painter, to,
                    colorMap()->color( interval, values[i] ),
                    pipeRect, liquidRect, orientation() );

      from = to;
    }
  }
  else
  {
    if ( !liquidRect.isEmpty() && alarmEnabled() )
    {
      const QRect r = alarmRect( liquidRect );
      if ( !r.isEmpty() )
      {
        painter->fillRect( r, alarmBrush() );
        liquidRect = QRegion( liquidRect ).subtracted( r ).boundingRect();
      }

      if (mAlarmState || (mSensInversed && !mAlarmState))
        painter->fillRect( liquidRect, alarmBrush() );
      else
        painter->fillRect( liquidRect, fillBrush() );
    }
    else
    {
      painter->fillRect( liquidRect, fillBrush() );
    }
  }

  painter->restore();
}


void CYLevelView::ctrlHideFlag()
{
  if (mHideFlag==0)
    return;

  bool res;

  if (mHideFlag->val() && !mInverseHideFlag)
    res = true;
  else if (!mHideFlag->val() && mInverseHideFlag)
    res = true;
  else
    res = false;

  if (res)
    hide();
  else
    show();
}

void CYLevelView::wheelEvent(QWheelEvent * e)
{
  if (!core || core->simulation())
  {
    if (e->angleDelta().y()>0)
      setValue(value()+1);
    else
      setValue(value()-1);
  }
}

void CYLevelView::setValue( double val )
{
  mAlarmState=alarmState(val);
  QwtThermo::setValue(val);
}


/*! @return la taille minimum suggéré
  \warning La valeur retournée dépend de la police et de l'échelle.
  \sa QwtThermo::sizeHint
*/
QSize CYLevelView::minimumSizeHint() const
{
  return QwtThermo::minimumSizeHint();
}

/*! @return le comportement de redimensionnement
    \fn CYLevelView::sizePolicy() const
 */
QSizePolicy CYLevelView::sizePolicy() const
{
  return QWidget::sizePolicy();
}

void CYLevelView::setBackgroundColor ( const QColor & color )
{
  mBackgroundColor=color;
  setAutoFillBackground(true);
  QPalette pal = palette();
  pal.setColor(QPalette::Base, mBackgroundColor);
  setPalette(pal);
}

const QColor & CYLevelView::backgroundColor () const
{
  return mBackgroundColor;
}

void CYLevelView::setForegroundColor ( const QColor & color )
{
  mForegroundColor=color;
  QPalette pal = palette();
  pal.setColor(QPalette::WindowText, mForegroundColor);
  setPalette(pal);
}

const QColor & CYLevelView::foregroundColor () const
{
  return mForegroundColor;
}


void CYLevelView::setBackgroundPixmap( const QPixmap & pixmap )
{
  if (pixmap.isNull())
    return;

  mBackgroundPixmap=pixmap;
  setAutoFillBackground(true);

  QPalette palette;
  palette.setBrush(backgroundRole(), QBrush(pixmap));
  setPalette(palette);
}

const QPixmap & CYLevelView::backgroundPixmap () const
{
  return mBackgroundPixmap;
}
