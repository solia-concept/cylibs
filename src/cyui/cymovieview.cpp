/***************************************************************************
                          cymovieview.cpp  -  description
                             -------------------
    begin                : mar oct 7 2003
    copyright            : (C) 2003 by Gérald LE CLEACH
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cymovieview.h"

// QT
#include <QPainter>
#include <QDir>
// CYLIBS
#include "cycore.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyword.h"
#include "cyflag.h"

CYMovieView::CYMovieView(QWidget *parent, const QString &name)
  : CYPixmapView(parent,name)
{
  mSpeed = 100;

  mStepByStep   = false;
  mPixmapIndex  = -1;
  mMovieIndex   = -1;
  mOffsetIndexFile = 0;

  setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
}

CYMovieView::~CYMovieView()
{
  if (movie())
  {
    movie()->setPaused(true);
    clear();
  }
}

void CYMovieView::setDataIndexFile(const QByteArray &name)
{
  CYDataWdg::setDataSupName(2,name);
}

QByteArray CYMovieView::dataIndexFile() const
{
  return CYDataWdg::dataSupName(2);
}


QByteArray CYMovieView::pixmapFile() const
{
  return mPixmapFile.toUtf8();
}

void CYMovieView::setPixmapFile(const QByteArray &file)
{
  if (file.startsWith(":/"))
  {
    mPixmapFile = QString(file);
  }
  else
  {
    mPixmapFile = findResource(QString(file));

    QDir d(mPixmapFile);
    if (d.exists())
      return;

    QFile f(mPixmapFile);
    if (!f.exists())
      return;
  }

  if (!pixmap(Qt::ReturnByValue).isNull())
    clear();

  setScaledContents(true);
  setPixmap(QPixmap(QString(mPixmapFile)));

  if (!pixmap(Qt::ReturnByValue).isNull())
  {
    newPixmap = true;
    setRotate(rotate());
  }
  else
    setScaledContents(false);
}


void CYMovieView::setPixmapFile(int index)
{
  index = index + mOffsetIndexFile;

  if (index==mPixmapIndex)
    return;

  QString *fileName = mPixmapFiles.value(index);
  if (fileName)
  {
    mPixmapIndex = index;
    setPixmapFile((*fileName).toUtf8());
    setState(mState);
  }
}

QByteArray CYMovieView::movieFile() const
{
  return mMovieFile.toUtf8();
}

void CYMovieView::setMovieFile(const QByteArray &file)
{
  if (file.startsWith(":/"))
  {
    mMovieFile = file;
  }
  else
  {
    mMovieFile = findResource(file);

    QDir d(mMovieFile);
    if (d.exists())
      return;

    QFile f(mMovieFile);
    if (!f.exists())
      return;
  }

  if (movie())
  {
    movie()->setPaused(true);
    clear();
  }

  setScaledContents(true);
  QMovie *m = new QMovie(mMovieFile);
  setMovie(m);
  if (movie())
  {
    newPixmap = true;
    setRotate(rotate());
    setCacheMode(cacheMode());
    m->start();
  }
  else
    setScaledContents(false);
}

void CYMovieView::setMovieFile(int index)
{
  index = index + mOffsetIndexFile;
  if (index==mMovieIndex)
    return;

  QString *fileName = mMovieFiles.value(index);
  if (fileName)
  {
    mMovieIndex = index;
    setMovieFile((*fileName).toUtf8());
    setState(mState);
  }
}

void CYMovieView::addPixmapFile(int index, QString fileName)
{
  mPixmapFiles.insert(index, new QString(fileName));
}

void CYMovieView::addMovieFile(int index, QString fileName)
{
  mMovieFiles.insert(index, new QString(fileName));
}

QMovie::CacheMode CYMovieView::cacheMode() const
{
  return mCacheMode;
}

void CYMovieView::setCacheMode(QMovie::CacheMode mode)
{
  mCacheMode = mode;
  if (movie())
    movie()->setCacheMode(mode);
}

bool CYMovieView::paused() const
{
  if (movie())
    return (movie()->state()==QMovie::Paused) ? true : false;
  return false;
}

bool CYMovieView::finished() const
{
  if (movie() && (movie()->state()!=QMovie::NotRunning))
    return true;
  return false;
}

bool CYMovieView::running() const
{
  if (movie())
    return (movie()->state()==QMovie::Running) ? true : false;
  return false;
}

void CYMovieView::unpause()
{
  if (!isVisible())
    return;
  if (movie())
    movie()->setPaused(false);
}

void CYMovieView::pause()
{
  if (!isVisible())
    return;
  if (movie())
    movie()->setPaused(true);
}

void CYMovieView::step()
{
  if (!isVisible())
    return;
  if (movie())
    movie()->jumpToNextFrame();
}

void CYMovieView::step(int steps)
{
  if (!isVisible())
    return;

  if (movie())
    movie()->jumpToFrame(movie()->currentFrameNumber()+steps);
}

void CYMovieView::goToStep(int step)
{
  if (!movie() || !isVisible())
    return;

  movie()->jumpToFrame(step);
}

void CYMovieView::restart()
{
  if (!isVisible())
    return;
  if (!mStepByStep && movie())
  {
    movie()->stop();
    movie()->start();
  }
}

int CYMovieView::speed() const
{
  return mSpeed;
}

void CYMovieView::setSpeed(const int percent)
{
  mSpeed = percent;
  if (movie() && !mStepByStep)
    movie()->setSpeed(percent);
}

void CYMovieView::setStepByStep(const bool val)
{
  mStepByStep = val;

  if (movie())
  {
    if (val)
    {
      movie()->setCacheMode(QMovie::CacheAll);
      movie()->stop();
    }
    else
    {
      movie()->setCacheMode(mCacheMode);
      movie()->start();
    }
  }
}

void CYMovieView::paintEvent( QPaintEvent* event )
{
  CYPixmapView::paintEvent( event );

  QPainter painter( this );

  drawContents( &painter );
}

void CYMovieView::drawContents(QPainter* p)
{
  if (!isVisible())
    return;

  if (!movie())
    return;

  // Get the current movie frame.
  QPixmap pm = movie()->currentPixmap();

  // Get the area we have to draw in.
  QRect r = contentsRect();

  if (!pm.isNull())
  {
    // Only rescale is we need to - it can take CPU!
    if (r.size() != pm.size())
    {
      QTransform m;      m.scale((double)r.width()/pm.width(), (double)r.height()/pm.height());
      pm = pm.transformed(m);
    }

    // Draw the [possibly scaled] frame.  movieUpdated() below calls
    // repaint with only the changed area, so clipping will ensure we
    // only do the minimum amount of rendering.
    //
    p->drawPixmap(r.x(), r.y(), pm);
  }
}


void CYMovieView::movieResized(const QSize& size)
{
  if (!isVisible())
    return;

  // The movie changed size, probably from its initial zero size.
  int fw = frameWidth();
  sh = QSize(size.width() + fw*2, size.height() + fw*2);

  updateGeometry();
  if (parentWidget() && parentWidget()->isHidden())
    parentWidget()->show();
}

void CYMovieView::setState(bool value)
{
  if (value)
  {
    setMovieFile(mMovieFile.toUtf8());
  }
  else
  {
    setPixmapFile(mPixmapFile.toUtf8());
  }
  CYLabel::setState(value);
  ctrlHideFlag();
}


void CYMovieView::linkData2()
{
  CYDataWdg::linkDataSup(2);

  if (!hasDataSup(2) || !mDataSup[2])
    return;

  if (!mData)
  {
    this->setToolTip(QString(mDataSup[2]->displayHelp()+"<br>"+mDataSup[2]->infoCY()));
  }
  else
  {
    this->setToolTip(QString(mData->displayHelp()+"<br>"+mData->infoCY()
                             +"<hr>"+
                             mDataSup[2]->displayHelp()+"<br>"+mDataSup[2]->infoCY()));
  }

  switch (mDataSup[2]->type())
  {
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      break;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mDataSup[2]->objectName());
  }
}

void CYMovieView::refresh2()
{
  if (!hasDataSup(2))
    return;

  if (mDataSup[2]==0)
    return;
  if (!isVisible())
    return;

  if (!ctrlFlag())
  {
    setEnabled(false);
    return;
  }
  if (mDataSup[2]==0)
    return;
  if (!mDataSup[2]->isOk())
  {
    setEnabled(false);
    return;
  }
  if (mDataSup[2]->flag()!=0)
  {
    if (!mDataSup[2]->flag()->val())
    {
      setEnabled(false);
      return;
    }
  }

  if ( !isEnabled() )
    setEnabled(true);

  switch (mDataSup[2]->type())
  {
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VU8    :
    {
      CYU8  *data = (CYU8  *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)mDataSup[2];
      setPixmapFile((int)data->val());
      setMovieFile((int)data->val());
      return;
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mDataSup[2]->objectName());
      break;
  }
}
