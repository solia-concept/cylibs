/*
 *                              cymessagebox.cpp
 *
 *  Created on: 18 janv. 2015
 *  Author    : Gérald LE CLEACH
 *  email     : gerald.lecleach@solia-concept.fr
 */

/***************************************************************************
 *
 *                Ce fichier fait partie des bibliothèques
 *             utiles au développement d'un superviseur CYLIX
 *
 ***************************************************************************/

#include "cymessagebox.h"
#include "cy.h"

CYMessageBox::CYMessageBox(QWidget * parent, const char * name)
 :QMessageBox(parent)
{
  setObjectName(name);
  // TODO Auto-generated constructor stub

}

CYMessageBox::~CYMessageBox()
{
    // TODO Auto-generated destructor stub
}

void
CYMessageBox::information(QWidget *parent,const QString &text,
                          const QString &caption)
{

  QMessageBox::information(parent, caption, text);
}

int
CYMessageBox::warningYesNo(QWidget *parent,
                           const QString &text,
                           const QString &caption,
                           const QString &buttonYes,
                           const QString &buttonNo)
{
  int button = QMessageBox::warning( parent, caption, text, buttonYes, buttonNo);
  switch (button)
  {
    case 0 : return Yes;
    case 1 : return No;
    default: return Cancel;
  }
}

int
CYMessageBox::warningYesNoCancel(QWidget *parent, const QString &text,
                                const QString &caption,
                                const QString &buttonYes,
                                const QString &buttonNo,
                                const QString &buttonCancel)
{
  int button = QMessageBox::warning( parent, caption, text, buttonYes, buttonNo, buttonCancel);
  switch (button)
  {
    case 0 : return Yes;
    case 1 : return No;
    case 2 : return Cancel;
    default: return Cancel;
  }
}

int
CYMessageBox::warningContinueCancel(QWidget *parent,
                                    const QString &text,
                                    const QString &caption,
                                    const QString &buttonContinue,
                                    const QString &buttonCancel)
{
  int button = QMessageBox::warning( parent, caption, text, buttonContinue, buttonCancel);
  switch (button)
  {
    case 0 : return Continue;
    case 1 : return Cancel;
    default: return Cancel;
  }
}

void
CYMessageBox::sorry(QWidget *parent, const QString &text,
                    const QString &caption)
{
  QMessageBox::warning( parent, caption, text );
}

void
CYMessageBox::error(QWidget *parent,  const QString &text,
                    const QString &caption)
{
  QMessageBox::critical(parent, caption, text);
}

int
CYMessageBox::questionYesNo(QWidget *parent, const QString &text,
                                  const QString &caption,
                                  const QString &buttonYes,
                                  const QString &buttonNo)
{
  int button = QMessageBox::question( parent, caption, text, buttonYes, buttonNo);
  switch (button)
  {
    case 0 : return Yes;
    case 1 : return No;
    default: return Cancel;
  }
}

int
CYMessageBox::questionYesNoCancel(QWidget *parent, const QString &text,
                                  const QString &caption,
                                  const QString &buttonYes,
                                  const QString &buttonNo,
                                  const QString &buttonCancel)
{
  int button = QMessageBox::question( parent, caption, text, buttonYes, buttonNo, buttonCancel);
  switch (button)
  {
    case 0 : return Yes;
    case 1 : return No;
    case 2 : return Cancel;
    default: return Cancel;
  }
}
