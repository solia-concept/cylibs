/***************************************************************************
                          cydatawdg.cpp  -  description
                             -------------------
    début                  : mer avr 23 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cydatawdg.h"

// CYLIBS
#include "cycore.h"
#include "cyflag.h"
#include "cydialog.h"
#include "cywidget.h"
#include "cyframe.h"
//Added by qt3to4:
#include <QString>

CYDataWdg::CYDataWdg(QWidget *widget)
{
  mThis = widget;
  mName = mThis->objectName();

  mHideIfNoData = false;
  mDB       = 0;
  mData     = 0;
  mFlag     = 0;
  mHideFlag = 0;
  mColorFlag= 0;
  mForcingData = 0;
  while (!mDataSupName.isEmpty())
  {
    QString *value = *mDataSupName.begin();
    mDataSupName.erase(mDataSupName.begin());
    delete value;
  }
//  mDataSupName.setAutoDelete(true);
  mPosX     = 0;
  mPosY     = 0;
  mCoreDB       = false;
  mGenericMark  = "";
  mGenericMarkOffset = 0;
  mDataName     = "";
  mFlagName     = "";
  mHideFlagName = "";
  mColorFlagName= "";
  mForcingName  = "";
  mPosXName     = "";
  mPosYName     = "";
  mBenchType    = "";
  mReadOnly        = false;
  mEnableReadOnly  = true;
  mMovable         = false;
  mEditPositions   = false;
  mForcingMode     = false;
  mSettingMode     = false;
  mAlwaysOk        = false;
  mInverseFlag     = false;
  mInverseHideFlag = false;
  mInverseColorFlag= false;
  mWriteData       = false;
  mEmitModifie     = false;
  mAutoRefresh     = true;
  mEnabled         = true;
  int offset = (core) ? core->offsetFontSize() : 0;
  mFontSize        = 12+offset;
  mUpdateOk        = false;
  mAutoMinimumSize = true;
  mPaletteProtected= false;
  mClickable       = false;
  mCtrlFlag        = true;

  mParent    = 0;
  mDlgParent = 0;
  mWdgParent = 0;
  mFrmParent = 0;

  mDisplaySimple = 0;

  mMovieStepFocusIn  = -1;
  mMovieStepFocusOut = -1;

  mUpdateIfVisible = true;
  mTreatChildDataWdg = true;

  mBackgroundColor=mThis->palette().color(mThis->backgroundRole());
  mForegroundColor=mThis->palette().color(mThis->foregroundRole());
}

CYDataWdg::~CYDataWdg()
{
}

void CYDataWdg::setParent(QWidget *parent, Cy::TemplateType type, QByteArray mark)
{
  mParent      = parent;
  mTypeParent  = type;

  if (!mark.isEmpty())
  {
    if ( mGenericMarkOffset != 0 )
    {
      bool ok;
      int num = mark.toInt(&ok);
      if (ok)
      {
        num += mGenericMarkOffset;
        mGenericMark = QString("%1").arg(num).toUtf8();
      }
      else
        CYFATALTEXT(mThis->objectName());
    }
    else
      mGenericMark = mark;
  }

  switch(mTypeParent)
  {
    case Cy::Dialog     : mDlgParent = (CYDialog *)mParent;
                          break;
    case Cy::Widget     : mWdgParent = (CYWidget *)mParent;
                          break;
    case Cy::Frame      : mFrmParent = (CYFrame *)mParent;
                          break;
    default        : CYFATALTEXT(mParent->objectName());
  }

  // Si le champ benchType est non vide et que le nom de banc ne contient pas ce champ alors le cacher
  if (!core->isBenchType(benchType()))
  {
    mThis->hide();
  }
  else if (hasData() || hasDataSup() || hasFlag() || hasHideFlag() || hasColorFlag() || hasForcingData() || hasPosX() || hasPosY())
  {
    // Recherche la donnée, la donnée de contrôle et la donnée de forçage
    if (findData())
    {
    }
    else if (hasFlag() || hasHideFlag() || hasColorFlag() || hasForcingData())
    {
      if (hasFlag())
      {
        // Recherche la donnée de contrôle
        findFlag();
      }

      if (hasHideFlag())
      {
        // Recherche la donnée d'affichage
        findHideFlag();
      }

      if (hasColorFlag())
      {
        // Recherche la donnée de changement de couleur
        findColorFlag();
      }

      if (hasForcingData())
      {
        // Recherche la donnée de forçage
        findForcing();
      }

      if (hasPosX())
      {
        // Recherche la donnée de position en X
        findPosX();
      }

      if (hasPosY())
      {
        // Recherche la donnée de position en Y
        findPosY();
      }
    }
  }
}

bool CYDataWdg::hasData()
{
  if ((!mData) && (mDataName==""))
    return false;
  else
    return true;
}

CYData *CYDataWdg::data()
{
  return mData;
}

void CYDataWdg::setData(CYData *data)
{
  mData = data;
  addWriteDBToParent();
  linkData();

  findForcing();
}

void CYDataWdg::setDataName(const QByteArray &name)
{
  mDataName = QString(name);

  if ((mDataName.indexOf('_')) == 0)
    mDataName.remove(0, 1);
}

QByteArray CYDataWdg::dataName() const
{
  return mDataName.toUtf8();
}

bool CYDataWdg::findData()
{
  if (mDB && !mCoreDB)
    return findData(&mDB->datas);
  else if (core)
    return findData(&core->db);
  return false;
}

bool CYDataWdg::findData(QHash<QString, CYData*> *db)
{
  findFlag();
  findHideFlag();
  findColorFlag();
  findDataSup();
  findPosX();
  findPosY();

  if ( mDataName == "" )
    return false;

  QString name = mDataName;
  if ( ( mGenericMark != "") && name.contains("#") )
    name.replace("#", mGenericMark);

  CYData *data = 0;

  data = db->value(name);
  setData(data);

  if (!data)
    return false;

  return true;
}

bool CYDataWdg::hasFlag()
{
  if ((!mFlag) && (mFlagName==""))
    return false;
  else
    return true;
}

CYData *CYDataWdg::flag()
{
  return mFlag;
}

void CYDataWdg::setFlag(CYData *data)
{
  mFlag = (CYFlag *)data;
  ctrlFlag();
}

void CYDataWdg::setFlagName(const QByteArray &name)
{
  mFlagName = QString(name);

  if ((mFlagName.indexOf('_')) == 0)
    mFlagName.remove(0, 1);
}

QByteArray CYDataWdg::flagName() const
{
  return mFlagName.toUtf8();
}

bool CYDataWdg::findFlag()
{
 if (mDB && !mCoreDB)
    return findFlag(&mDB->datas);
  else if (core)
    return findFlag(&core->db);
  return false;
}

bool CYDataWdg::findFlag(QHash<QString, CYData*> *db)
{
  if ( mFlagName == "" )
    return false;

  QString name = mFlagName;
  if ( ( mGenericMark != "") && name.contains("#") )
    name.replace("#", mGenericMark);

  CYData *data = 0;

  if ((data = db->value(name)))
    setFlag(data);
  else
    return false;

  return true;
}

bool CYDataWdg::hasHideFlag()
{
  if ((!mHideFlag) && (mHideFlagName==""))
    return false;
  else
    return true;
}

CYFlag *CYDataWdg::hideFlag()
{
  return mHideFlag;
}

void CYDataWdg::setHideFlag(CYData *data)
{
  mHideFlag = (CYFlag *)data;
  ctrlHideFlag();
}

void CYDataWdg::setHideFlagName(const QByteArray &name)
{
  mHideFlagName = QString(name);

  if ((mHideFlagName.indexOf('_')) == 0)
    mHideFlagName.remove(0, 1);
}

QByteArray CYDataWdg::hideFlagName() const
{
  return mHideFlagName.toUtf8();
}

bool CYDataWdg::findHideFlag()
{
  if (mDB && !mCoreDB)
    return findHideFlag(&mDB->datas);
  else if (core)
    return findHideFlag(&core->db);
  return false;
}

bool CYDataWdg::findHideFlag(QHash<QString, CYData*> *db)
{
  if ( mHideFlagName == "" )
    return false;
  QString name = mHideFlagName;
  if ( ( mGenericMark != "") && name.contains("#") )
    name.replace("#", mGenericMark);

  CYData *data = 0;

  if ((data = db->value(name)))
  {
    setHideFlag(data);
  }
  else
    return false;

  return true;
}

bool CYDataWdg::hasColorFlag()
{
  if ((!mColorFlag) && (mColorFlagName==""))
    return false;
  else
    return true;
}

CYFlag *CYDataWdg::colorFlag()
{
  return mColorFlag;
}

void CYDataWdg::setColorFlag(CYData *data)
{
  mColorFlag = (CYFlag *)data;
  ctrlColorFlag();
}

QByteArray CYDataWdg::colorFlagName() const
{
  return mColorFlagName.toUtf8();
}

void CYDataWdg::setColorFlagName(const QByteArray &name)
{
  mColorFlagName = QString(name);

  if ((mColorFlagName.indexOf('_')) == 0)
    mColorFlagName.remove(0, 1);
}

bool CYDataWdg::findColorFlag()
{
  if (mDB && !mCoreDB)
    return findColorFlag(&mDB->datas);
  else if (core)
    return findColorFlag(&core->db);
  return false;
}

bool CYDataWdg::findColorFlag(QHash<QString, CYData*> *db)
{
  if ( mColorFlagName == "" )
    return false;
  QString name = mColorFlagName;
  if ( ( mGenericMark != "") && name.contains("#") )
    name.replace("#", mGenericMark);

  CYData *data = 0;

  if ((data = db->value(name)))
  {
    setColorFlag(data);
  }
  else
    return false;

  return true;
}

bool CYDataWdg::hasForcingData()
{
  if ((!mForcingData) && (mForcingName==""))
    return false;
  else
    return true;
}

CYData *CYDataWdg::forcingData()
{
  return mForcingData;
}

void CYDataWdg::setForcingData(CYData *data)
{
  mForcingData = (CYData *)data;
  addForcingDBToParent();
}

void CYDataWdg::setForcingName(const QByteArray &name)
{
  mForcingName = QString(name);

  if ((mForcingName.indexOf('_')) == 0)
    mForcingName.remove(0, 1);
}

QByteArray CYDataWdg::forcingName() const
{
  return mForcingName.toUtf8();
}

bool CYDataWdg::findForcing()
{
  if (mDB && !mCoreDB)
    return findForcing(&mDB->datas);
  else if (core)
    return findForcing(&core->db);
  return false;
}

bool CYDataWdg::findForcing(QHash<QString, CYData*> *db)
{
  if ( mForcingName == "" )
    return false;

  QString name = mForcingName;

  if ( ( mGenericMark != "") && name.contains("#") )
    name.replace("#", mGenericMark);

  CYData *data = 0;


  if ((data = db->value(name)))
    setForcingData(data);
  else
    return false;

  return true;
}

bool CYDataWdg::hasDataSup()
{
  if (mDataSup.count())
    return true;

  if (mDataSupName.count())
    return true;

  QHashIterator<int, QString *> it(mDataSupName);
  while (it.hasNext())
  {
    it.next();                   // must come first
    QString dataName = (*it.value());
    if (!dataName.isEmpty())
      return true;
  }
  return false;
}

bool CYDataWdg::hasDataSup(int num)
{
  CYData *data = mDataSup[num];
  QByteArray dataName = dataSupName(num);

  if ((!data) && (dataName==""))
    return false;
  else
    return true;
}

CYData *CYDataWdg::dataSup(int num)
{
  return mDataSup[num];
}

void CYDataWdg::setDataSup(int num, CYData *data)
{
  mDataSup.insert(num, data);
  addWriteDBToParentX(num);
  linkDataSup(num);

  findForcing();
}

void CYDataWdg::setDataSupName(int num, const QByteArray &name)
{
  QString *dataName = new QString(name);

  if (name.isEmpty())
    mDataSupName.remove(num);

  if ((dataName->indexOf('_')) == 0)
    dataName->remove(0, 1);

  delete mDataSupName.take(num);
   mDataSupName.insert(num, dataName);
}

QByteArray CYDataWdg::dataSupName(int num) const
{
  QString *dataName = mDataSupName[num];
  if (!dataName)
    return "";
  return (*dataName).toUtf8();
}

bool CYDataWdg::findDataSup()
{
  if (mDB && !mCoreDB)
    return findDataSup(&mDB->datas);
  else if (core)
    return findDataSup(&core->db);
  return false;
}

bool CYDataWdg::findDataSup(QHash<QString, CYData*> *db)
{
  bool res=false;
  QHashIterator<int, QString *> it(mDataSupName);
  while (it.hasNext())
  {
    it.next();                   // must come first
    QString dataName = (*it.value());
      if (!findDataSup(it.key(), db))
        res=false;
  }
  return res;
}

bool CYDataWdg::findDataSup(int num, QHash<QString, CYData*> *db)
{
  QString name = QString(dataSupName(num));
  if ( name == "" )
    return false;

  if ( ( mGenericMark != "") && name.contains("#") )
    name.replace("#", mGenericMark);

  CYData *data = 0;

  if ((data = db->value(name)))
    setDataSup(num, data);
  else
    return false;

  return true;
}

bool CYDataWdg::hasPosX()
{
  if ((!mPosX) && (mPosXName==""))
    return false;
  else
    return true;
}

CYS32 *CYDataWdg::posX()
{
  return mPosX;
}

void CYDataWdg::setPosX(CYS32 *data)
{
  mPosX = data;
}

void CYDataWdg::setPosXName(const QByteArray &name)
{
  mPosXName = name;

  if ((mPosXName.indexOf('_')) == 0)
    mPosXName.remove(0, 1);
}

QByteArray CYDataWdg::posXName() const
{
  return mPosXName.toUtf8();
}

bool CYDataWdg::findPosX()
{
  if (mDB && !mCoreDB)
    return findPosX(&mDB->datas);
  else if (core)
    return findPosX(&core->db);
  return false;
}

bool CYDataWdg::findPosX(QHash<QString, CYData*> *db)
{
  if ( mPosXName == "" )
    return false;

  QString name = mPosXName;
  if ( ( mGenericMark != "") && name.contains("#") )
    name.replace("#", mGenericMark);

  CYS32 *data = 0;

  if ((data = (CYS32 *)db->value(name)))
    setPosX(data);
  else
    return false;

  return true;
}

bool CYDataWdg::hasPosY()
{
  if ((!mPosY) && (mPosYName==""))
    return false;
  else
    return true;
}

CYS32 *CYDataWdg::posY()
{
  return mPosY;
}

void CYDataWdg::setPosY(CYS32 *data)
{
  mPosY = data;
}

void CYDataWdg::setPosYName(const QByteArray &name)
{
  mPosYName = name;

  if ((mPosYName.indexOf('_')) == 0)
    mPosYName.remove(0, 1);
}

QByteArray CYDataWdg::posYName() const
{
  return mPosYName.toUtf8();
}

bool CYDataWdg::findPosY()
{
  if (mDB && !mCoreDB)
    return findPosY(&mDB->datas);
  else if (core)
    return findPosY(&core->db);
  return false;
}

bool CYDataWdg::findPosY(QHash<QString, CYData*> *db)
{
  if ( mPosYName == "" )
    return false;

  QString name = mPosYName;
  if ( ( mGenericMark != "") && name.contains("#") )
    name.replace("#", mGenericMark);

  CYS32 *data = 0;

  if ((data = (CYS32 *)db->value(name)))
    setPosY(data);
  else
    return false;

  return true;
}

void CYDataWdg::setBenchType(const QByteArray &name)
{
  mBenchType = name;
}

QByteArray CYDataWdg::benchType() const
{
  return mBenchType.toUtf8();
}

bool CYDataWdg::inverseFlag() const
{
  return mInverseFlag;
}

void CYDataWdg::setInverseFlag(const bool inverse)
{
  mInverseFlag = inverse;
}

bool CYDataWdg::inverseHideFlag() const
{
  return mInverseHideFlag;
}

void CYDataWdg::setInverseHideFlag(const bool inverse)
{
  mInverseHideFlag = inverse;
}

bool CYDataWdg::inverseColorFlag() const
{
  return mInverseColorFlag;
}

void CYDataWdg::setInverseColorFlag(const bool inverse)
{
  mInverseColorFlag = inverse;
}

void CYDataWdg::setReadOnly(const bool val)
{
  if (!mEnableReadOnly)
    return;
  mReadOnly = val;
}

bool CYDataWdg::readOnly() const
{
  return mReadOnly;
}

void CYDataWdg::setForcingMode(const bool val)
{
  if (val && (mForcingName==""))
    mForcingName=mDataName;
  mForcingMode = val;
}

bool CYDataWdg::forcingMode() const
{
  return mForcingMode;
}

void CYDataWdg::setSettingMode(const bool val)
{
  mSettingMode = val;
}

bool CYDataWdg::settingMode() const
{
  return mSettingMode;
}

bool CYDataWdg::alwaysOk() const
{
  return mAlwaysOk;
}

void CYDataWdg::setAlwaysOk(const bool val)
{
  mAlwaysOk = val;
}

void CYDataWdg::setLocaleDB(CYDB *db)
{
  mDB = db;
}

void CYDataWdg::addWriteDBToParent()
{
  if (!mWriteData)
    return;

  if (mData)
  {
    if (mDlgParent)
      mDlgParent->addWriteDB(mData->db());
    else if (mWdgParent)
      mWdgParent->addWriteDB(mData->db());
    else if (mFrmParent)
      mFrmParent->addWriteDB(mData->db());
  }
}

void CYDataWdg::addForcingDBToParent()
{
  if (!mForcingData)
    CYFATAL;

  mAutoRefresh = true;
  if (mDlgParent)
    mDlgParent->addForcingDB(mForcingData->db());
  else if (mWdgParent)
    mWdgParent->addForcingDB(mForcingData->db());
  else if (mFrmParent)
    mFrmParent->addForcingDB(mForcingData->db());
}

void CYDataWdg::addWriteDBToParentX(int num)
{
  if (!mDataSup[num])
  {
    CYFATALTEXT(QString(dataSupName(num)));
  }

  if (!mWriteData)
    return;

  if (mDlgParent)
    mDlgParent->addWriteDB(mDataSup[num]->db());
  else if (mWdgParent)
    mWdgParent->addWriteDB(mDataSup[num]->db());
  else if (mFrmParent)
    mFrmParent->addWriteDB(mDataSup[num]->db());
}

void CYDataWdg::addDataName(const QString &name, int index)
{
  QString *dataName = new QString(name);
  if (index<0)
    mDatasName.insert(mDatasName.count(), dataName);
  else
    mDatasName.insert(index, dataName);
}

void CYDataWdg::setCurrentDataName(int index)
{
  if (mDatasName.isEmpty())
  {
    CYWARNINGTEXT(QString("void CYDataWdg::setCurrentDataName(%1) %2 mDatas.isEmpty()")
                  .arg(index).arg(mThis->objectName()));
    return;
  }
  setDataName((*mDatasName.value(index)).toUtf8());
  findData();
}

void CYDataWdg::addData(const QString &name, int index)
{
  CYData *d = core->findData(name);

  if (!d)
    CYFATALTEXT(QString("void CYDataWdg::addData(%1, %2)").arg(name).arg(index));

  if (index<0)
    mDatas.insert(mDatas.count(), d);
  else
    mDatas.insert(index, d);
}

void CYDataWdg::setCurrentData(int index)
{
  if (mDatas.isEmpty())
  {
    CYWARNINGTEXT(QString("void CYDataWdg::setCurrentData(%1) %2 mDatas.isEmpty()")
                  .arg(index).arg(mThis->objectName()));
    return;
  }

  CYData *d = mDatas.value(index);

  if (!d)
    CYWARNINGTEXT(QString("void CYDataWdg::setCurrentData(%1) mauvais index").arg(index));

  setData(d);
}

void CYDataWdg::clearDatas()
{
  mDatas.clear();
}
