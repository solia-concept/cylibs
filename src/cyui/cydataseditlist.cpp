//
// C++ Implementation: cydataseditlist
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cydataseditlist.h"

// CYLIBS
#include "cydataslistview.h"
#include "ui_cydataseditlist.h"

#define COL_LABEL 1

CYDatasEditList::CYDatasEditList(QWidget *parent, const QString &name)
  : CYWidget(parent,name), ui(new Ui::CYDatasEditList)
{
  ui->setupUi(this);
  mEnableEditLabel = true;
  mEnableDelete = true;
  connect(ui->editButton   , SIGNAL(clicked())                       , ui->list, SLOT(setLabel())                       );
  connect(ui->colorButton  , SIGNAL(clicked())                       , ui->list, SLOT(setColor())                       );
  connect(ui->penButton    , SIGNAL(clicked())                       , ui->list, SLOT(setPen())                         );
  connect(ui->coefButton   , SIGNAL(clicked())                       , ui->list, SLOT(setCoef())                        );
  connect(ui->offsetButton , SIGNAL(clicked())                       , ui->list, SLOT(setOffset())                      );
  connect(ui->deleteButton , SIGNAL(clicked())                       , ui->list, SLOT(deleteData())                     );
  connect(ui->list         , SIGNAL(entered(const QModelIndex &))  , this, SLOT(selectionChanged(const QModelIndex &)) );
  connect(ui->list         , SIGNAL(modifie())                       , this, SIGNAL(modifie())                      );
}


CYDatasEditList::~CYDatasEditList()
{
  delete ui;
}




/*! Ajoute une ligne de donnée.
    @param pos Position de la donnée. Par défaut à -1, permet un positionnement automatique.
    \fn CYDatasEditList::addData(CYDisplayItem *item, int pos)
 */
void CYDatasEditList::addData(CYDisplayItem *item, int pos)
{
  ui->list->addData(item, pos);
}


/*! Désactive les boutons si l'\a index est invalide.
    \fn CYDatasEditList::selectionChanged(const QModelIndex &index)
 */
void CYDatasEditList::selectionChanged(const QModelIndex &index)
{
  bool enable = index.isValid();

  ui->editButton->setEnabled(enable);
  ui->colorButton->setEnabled(enable);
  ui->penButton->setEnabled(enable);
  ui->coefButton->setEnabled(enable);
  ui->offsetButton->setEnabled(enable);
  ui->deleteButton->setEnabled(enable);
}


/*! @return le nombre maximum de données possible.
    \fn CYDatasEditList::maxNbDatas()
 */
int CYDatasEditList::maxNbDatas() const
{
  return ui->list->maxNbDatas();
}


/*! Saisir le nombre maximum de données possible.
    \fn CYDatasEditList::setMaxNbDatas(const int val)
 */
void CYDatasEditList::setMaxNbDatas(const int val)
{
  ui->list->setMaxNbDatas(val);
  if (val>1)
    ui->deleteButton->show();
  else
    ui->deleteButton->hide();
}


/*! @return true si le choix d'une couleur est possible.
    \fn CYDatasEditList::withColor()
 */
bool CYDatasEditList::withColor() const
{
  return ui->list->withColor();
}


/*! Saisir \a true pour que le choix d'une couleur soit possible.
    \fn CYDatasEditList::setWithColor(const bool val)
 */
void CYDatasEditList::setWithColor(const bool val)
{
  ui->list->setWithColor(val);
  if (val)
    ui->colorButton->show();
  else
    ui->colorButton->hide();
}


/*! @return true si le choix d'un crayon est possible.
    \fn CYDatasEditList::withPen()
 */
bool CYDatasEditList::withPen() const
{
  return ui->list->withPen();
}


/*! Saisir \a true pour que le choix d'un crayon soit possible.
    \fn CYDatasEditList::setWithPen(const bool val)
 */
void CYDatasEditList::setWithPen(const bool val)
{
  ui->list->setWithPen(val);
  if (val)
    ui->penButton->show();
  else
    ui->penButton->hide();
}


/*! @return true si le choix d'un coefficient est possible.
    \fn CYDatasEditList::withCoef()
 */
bool CYDatasEditList::withCoef() const
{
  return ui->list->withCoef();
}


/*! Saisir \a true pour que le choix d'un coefficient soit possible.
    \fn CYDatasEditList::setWithCoef(const bool val)
 */
void CYDatasEditList::setWithCoef(const bool val)
{
  ui->list->setWithCoef(val);
  if (val)
    ui->coefButton->show();
  else
    ui->coefButton->hide();
}

/*! @return true si le choix d'un offset est possible.
    \fn CYDatasEditList::withOffset()
 */
bool CYDatasEditList::withOffset() const
{
  return ui->list->withOffset();
}


/*! Saisir \a true pour que le choix d'un offset soit possible.
    \fn CYDatasEditList::setWithOffset(const bool val)
 */
void CYDatasEditList::setWithOffset(const bool val)
{
  ui->list->setWithOffset(val);
  if (val)
    ui->offsetButton->show();
  else
    ui->offsetButton->hide();
}


/*! @return true si l'édition du libellé est possible.
    \fn CYDatasEditList::enableEditLabel()
 */
bool CYDatasEditList::enableEditLabel() const
{
  return mEnableEditLabel;
}


/*! Saisir \a true pour que l'édition du libellé soit possible.
    \fn CYDatasEditList::setEnableEditLabel(const bool val)
 */
void CYDatasEditList::setEnableEditLabel(const bool val)
{
  mEnableEditLabel = val;
  if (val)
    ui->editButton->show();
  else
    ui->editButton->hide();
}


/*! @return true si la suppresion de la donnnée est possible.
    \fn CYDatasEditList::enableDelete()
 */
bool CYDatasEditList::enableDelete() const
{
  return mEnableDelete;
}


/*! Saisir \a true pour que la suppresion de la donnnée soit possible.
    \fn CYDatasEditList::setEnableDelete(const bool val)
 */
void CYDatasEditList::setEnableDelete(const bool val)
{
  mEnableDelete = val;
  if (val)
    ui->deleteButton->show();
  else
    ui->deleteButton->hide();
}


/*! Active / désactive la boîte de sélection du filtrage des données si \a val vaut \a true.
    \fn CYDatasEditList::enabelFilterBox(bool val)
 */
void CYDatasEditList::enabelFilterBox(bool val)
{
  ui->browser->enabelFilterBox(val);
}

CYDatasListView *CYDatasEditList::list()
{
  return ui->list;
}

CYDatasBrowser *CYDatasEditList::browser()
{
  return ui->browser;
}

void CYDatasEditList::hideBrowser()
{
  ui->browser->hide();
}
