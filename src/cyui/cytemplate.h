/***************************************************************************
                          cytemplate.h  -  description
                             -------------------
    début                  : lun sep 1 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYTEMPLATE_H
#define CYTEMPLATE_H

// QT
#include <QWidget>
#include <QTimer>
#include <QDomElement>
// CYLIBS
#include "cy.h"
#include "cydb.h"

/** @short Base pour un CYWidget, une CYDialog ou une CYFrame.
  * @author Gérald LE CLEACH
  */

class CYTemplate
{
public:
  CYTemplate(QWidget *widget);
  virtual ~CYTemplate();

  /** @return \a true si le widget contient des valeurs de données modifiées. */
  bool isModified() { return mModified; }
  /** @return \a true si le widget est en lecture seule. */
  bool isReadOnly() { return mReadOnly; }

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

public: // Public methods
  /** @return le repère de généricité utile aux widgets génériques.
    * @see void setGenericMark(const QString txt). */
  virtual QByteArray genericMark() const { return mGenericMark; }
  /** Saisie le repère de généricité utile aux widgets génériques.
    * Par example un widget dans certain cas peut servir pour 2 postes.
    * Les noms des données pour les 2 postes doivent se différencier par le
    * numéro du poste. Il suffit alors de remplacer cette partie du nom par
    * un # dans dataName, flagName, forcingName et data2Name des widgets enfants.
    * Ex: DATA_POSTE1 & DATA_petit_poste1 => DATA_POSTE#
    * La rechercher des données remplacera automatiquement ce # par
    * le numéro du bon poste passé ici comme index. */
  virtual void setGenericMark(const QByteArray txt) { mGenericMark = txt; }
  /** Saisie un index qui une fois transformé en QString deviendra le repère
    * de généricité utile aux widgets génériques @see setGenericMark(QString txt). */
  virtual void setGenericMark(int index);

  /** Place le widget comme ayant des valeurs de données modifiées. */
  virtual void setModifie() {}
  /** Place le widget comme ayant des valeurs de données validées. */
  virtual void setValidate() {}
  /** Applique les données. */
  virtual void apply() {}
  /** Mise à jour des données. */
  virtual void update() {}
  /** Rafraîchit la vue. */
  virtual void refresh() {}
  /** Charge les valeurs par défaut des données. */
  virtual void designer() {}
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(bool val) { Q_UNUSED(val); }
  /** Met le widget en mode forçage ou pas suivant \a val. */
  virtual void setForcingMode(bool val) { Q_UNUSED(val); }
  /** Initialise les valeurs des données de forçage. */
  virtual void initForceValues();
  /** Met le widget en mode paramétrage ou pas suivant \a val. */
  virtual void setSettingMode(bool val) {Q_UNUSED(val);}
  /** Initialise les valeurs des données de paramétrage. */
  virtual void initSettingValues();
  /** Saisie la base de données locale à ce widget. */
  virtual void setLocaleDB(CYDB *db);
  /** Active/désactive le déplacement de tous les widgets enfants. */
  virtual void setEditPositions(bool state);
  /**  @return \a true si la mise à jour des données par les valeurs
    * saisie par les widgets enfants n'est possible que si ceux-ci sont visibles. */
  virtual bool updateIfVisible() const;
  /** Saisir \a true pour que la mise à jour des données par les valeurs
    * saisie par les widgets enfants ne soit possible que si ceux-ci sont visibles. */
  virtual void setUpdateIfVisible(const bool val);

  virtual bool linkDatasShow() const;
  virtual void setLinkDatasShow(const bool val);

  /** Ajoute \a db à la liste des bases de données d'écriture sur le disque dur et sur le réseau. */
  void addWriteDB(CYDB *db);
  /** Ajoute \a db à la liste des bases de données d'écriture en mode forçage sur le réseau. */
  void addForcingDB(CYDB *db);

  /** Si les valeurs saisies ont changées par rapport aux données
    * demander s'il faut appliquer ces changements ou non avant de quitter
    * la vue.
    * @return \a false si l'on décide d'annuler le changement de vue.*/
  virtual bool quitView();
  /** Démarre le timer de raffraîchissement. */
  virtual void startTimer(int ms);

protected: // Protected methods
  /** Initialise les widgets enfants. */
  void init(QObject *parent);
  /** Applique les données des widgets enfants. */
  void apply(QObject *parent);
  /** Mise à jour des widgets enfants. */
  void update(QObject *parent);
  /** Rafraîchit la vue des widgets enfants. */
  void refresh(QObject *parent, bool useAutoRefresh=true);
  /** Charge les valeurs constructeur des widgets enfants. */
  void designer(QObject *parent);
  /** Met les widgets de saisie enfants en lecture seule ou pas suivant \a val. */
  void setReadOnly(bool val, QObject *parent);
  /** Met les widgets de saisie enfants en mode forçage ou pas suivant \a val. */
  void setForcingMode(bool val, QObject *parent);
  /** Met les widgets de saisie enfants en mode paramétrage ou pas suivant \a val. */
  void setSettingMode(bool val, QObject *parent);
  /** Initialise les valeurs des données de forçage des widgets enfants. */
  void initForceValues(QObject *parent);
  /** Initialise les valeurs des données de paramétrage des widgets enfants. */
  void initSettingValues(QObject *parent);
  /** Saisie la base de données locale aux widgets enfants. */
  void setLocaleDB(QObject *parent, CYDB *db);
  /** Active/désactive l'édition des positions des widgets enfants déplaçables. */
  void setEditPositions(QObject *parent, bool state);
  /** Saisir \a true pour que la mise à jour des données par les valeurs
    * saisie par les widgets enfants soit possible que si ceux-ci sont visibles. */
  void setUpdateIfVisible(QObject *parent, bool val);

  void setLinkDatasShow(QObject *parent, bool val);

protected: // Protected attributes
  /** Widget du template. */
  QWidget *mThis;
  /** Type de conteneur. */
  Cy::TemplateType mType;
  /** Liste des bases de données d'écriture sur le disque dur et sur le réseau. */
  QHash<QString, CYDB*> mWriteDB;
  /** Liste des bases de données d'écriture en mode forçage sur le réseau. */
  QHash<QString, CYDB*> mForcingDB;
  /** Widget ayant des valeurs de données modifiées. */
  bool mModified;
  /** Timer pouvant servir par exemple au rafraichissement du widget. */
  QTimer *mTimer;
  /** Si \a true le widget est en lecture seule. */
  bool mReadOnly;
  /** Si \a true le widget est en mode forçage. */
  bool mForcingMode;
  /** Si \a true le widget est en mode paramétrage. */
  bool mSettingMode;
  /** Indique si le widget a bien ses données connectées.  */
  bool mDatasLinked;
  /** Autorise ou non la connexion des données.  */
  bool mLinkDatasShow;
  /**  Si \a true la mise à jour des données par les valeurs
    * saisie par les widgets enfants n'est possible que si ceux-ci sont visibles. */
  bool mUpdateIfVisible;
  /** S'il vaut \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool mTreatChildDataWdg;

  /** Repère de généricité utile aux widgets génériques @see setGenericMark(QString txt). */
  QByteArray mGenericMark;
  /** Si \a true alors le signal modifie() peut être émis. */
  bool mEmitModifie;
};

#endif
