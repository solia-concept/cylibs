//
// C++ Implementation: cytable
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <QPainter>
#include <QHeaderView>
// CYLIBS
#include "cytable.h"
#include "cy.h"

CYTable::CYTable(QWidget *parent, const QString &name)
 : QTableWidget(parent)
{
  setObjectName(name);
}


CYTable::~CYTable()
{
}

void CYTable::print(QPainter *p,QRectF *r)
{
  QRectF rect;
  QRectF rTable = QRectF(r->left(), r->top(), r->width(), r->height());

  int nb_row = (rowCount()>180) ? 180 : rowCount();
  int nb_col = (columnCount()>10 ) ?  10 : columnCount();

  QFont old_font = p->font();
  QFont font = p->font();
//   QFont font("Sans", 8);
//   p->setFont(font);
  int hcell = p->fontMetrics().height()+1;

  font.setBold(true);
  font.setItalic(true);
  p->setFont(font);
  
  int wcell[nb_col];
  int offset = 0;
  int width=0;
  for ( int col=0; col<nb_col; col++ )
  {
    width+=columnWidth(col);
  }

  for ( int col=0; col<nb_col; col++ )
  {
    wcell[col] = columnWidth(col)*rTable.width()/width;
    offset = (col>0) ? (offset+wcell[col-1]) : 0;
    QRectF cell(rTable.left()+offset, rTable.top(), wcell[col], hcell*3);
    p->drawText(QRectF(cell.left(), cell.top(), cell.width(), cell.height()), Qt::AlignCenter, horizontalHeaderItem(col)->text(), &rect);
    p->drawRect(cell.left(), cell.top(), cell.width(), cell.height());
  }
  rTable.setBottom(rTable.top()+hcell*3);

  font.setBold(false);
  font.setItalic(false);
  p->setFont(font);

  for ( int row=0; row<nb_row; row++ )
  {
    for ( int col=0; col<nb_col; col++ )
    {
      offset = (col>0) ? (offset+wcell[col-1]) : 0;
      QRectF cell(rTable.left()+offset, rTable.bottom(), wcell[col], hcell);
      QTableWidgetItem *el=item(row, col);
      if (el)
        p->drawText(QRectF(cell.left(), cell.top(), cell.width(), cell.height()), Qt::AlignCenter, el->text(), &rect);
      else
        CYWARNINGTEXT(QString("row=%1, col%2").arg(row).arg(col));

      p->drawRect(cell.left(), cell.top(), cell.width(), cell.height());
    }
    rTable.setBottom(rTable.bottom()+hcell);
  }
  rTable.setBottom(rTable.bottom()+hcell);
  p->setFont(old_font);

  r->setTop(rTable.bottom());
}

void CYTable::setText(int row, int col, QString text)
{
  QTableWidgetItem *cell = item(row, col);
  if (cell)
      cell->setText(text);
  else
  {
    QTableWidgetItem *item = new QTableWidgetItem(text);
    item->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable);
    setItem(row, col, item);
  }
}
