/**********************************************************************
**
**
** CYIntValidator:
**   Copyright (C) 1999 Glen Parker <glenebob@nwlink.com>:
**   Copyright (c) 2002 Marc Mutz <mutz@kde.org>
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
**
*****************************************************************************/

#include <qwidget.h>
#include <qstring.h>
#include <qlocale.h>

#include "cynumvalidator.h"
#include "cy.h"

///////////////////////////////////////////////////////////////
//  Implementation of CYIntValidator
//

CYIntValidator::CYIntValidator ( QWidget * parent, int base, const char * name )
  : QValidator(parent)
{
  setObjectName(name);
  _base = base;
  if (_base < 2) _base = 2;
  if (_base > 36) _base = 36;

  _min = _max = 0;
}

CYIntValidator::CYIntValidator ( int bottom, int top, QWidget * parent, int base, const char * name )
  : QValidator(parent)
{
  setObjectName(name);
  _base = base;
  if (_base > 36) _base = 36;

  _min = bottom;
  _max = top;
}

CYIntValidator::~CYIntValidator ()
{}

QValidator::State CYIntValidator::validate ( QString &str, int & ) const
{
  bool ok;
  int  val = 0;
  QString newStr;

  newStr = str.trimmed();
  if (_base > 10)
    newStr = newStr.toUpper();

  if (newStr == QString::fromLatin1("-")) // a special case
    if ((_min || _max) && _min >= 0)
      ok = false;
    else
      return QValidator::Acceptable;
  else if (newStr.length())
    val = newStr.toInt(&ok, _base);
  else {
    val = 0;
    ok = true;
  }

  if (! ok)
    return QValidator::Invalid;

  if ((! _min && ! _max) || (val >= _min && val <= _max))
    return QValidator::Acceptable;

  if (_max && _min >= 0 && val < 0)
    return QValidator::Invalid;

  return QValidator::Intermediate;
}

void CYIntValidator::fixup ( QString &str ) const
{
  int                dummy;
  int                val;
  QValidator::State  state;

  state = validate(str, dummy);

  if (state == QValidator::Invalid || state == QValidator::Acceptable)
    return;

  if (! _min && ! _max)
    return;

  val = str.toInt(0, _base);

  if (val < _min) val = _min;
  if (val > _max) val = _max;

  str.setNum(val, _base);
}

void CYIntValidator::setRange ( int bottom, int top )
{
  _min = bottom;
  _max = top;

  if (_max < _min)
    _max = _min;
}

void CYIntValidator::setBase ( int base )
{
  _base = base;
  if (_base < 2) _base = 2;
}

int CYIntValidator::bottom () const
{
  return _min;
}

int CYIntValidator::top () const
{
  return _max;
}

int CYIntValidator::base () const
{
  return _base;
}
