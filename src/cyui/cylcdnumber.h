//
// C++ Interface: cylcdnumber
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYLCDNUMBER_H
#define CYLCDNUMBER_H

#include <qlcdnumber.h>

/**
@short Afficheur LCD

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYLCDNumber : public QLCDNumber
{
public:
    CYLCDNumber(QWidget *parent=0, const QString &name=0);
    CYLCDNumber(uint numDigits, QWidget *parent=0, const QString &name=0);

    ~CYLCDNumber();

public slots:
    virtual void setDigitColor(const QColor &col);
    virtual void setBackgroundColor(const QColor &col);
};

#endif
