//
// C++ Interface: cyventilator
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYVENTILATOR_H
#define CYVENTILATOR_H

// CYLIBS
#include <cymovieview.h>

/** Visualisateur d'un ventilateur
  *@author Gérald LE CLEACH
  */

class CYVentilator : public CYMovieView
{
  Q_OBJECT

public:
  CYVentilator(QWidget *parent=0, const QString &name=0);
  ~CYVentilator();
};

#endif
