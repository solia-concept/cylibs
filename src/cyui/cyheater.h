//
// C++ Interface: cyheater
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYHEATER_H
#define CYHEATER_H

#include <QString>
// CYLIBS
#include <cypixmapview.h>

/**
@short Système de chauffe.

@author Gérald LE CLEACH
*/
class CYHeater : public CYPixmapView
{
Q_OBJECT
  Q_PROPERTY(QByteArray pixmapFileTrue READ pixmapFileTrue WRITE setPixmapFileTrue DESIGNABLE false STORED false)
  Q_PROPERTY(QByteArray pixmapFileFalse READ pixmapFileFalse WRITE setPixmapFileFalse DESIGNABLE false STORED false)
public:
    CYHeater(QWidget *parent = 0, const QString &name = 0);

    ~CYHeater();
};

#endif
