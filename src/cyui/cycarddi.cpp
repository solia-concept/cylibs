/***************************************************************************
                          cycarddi.cpp  -  description
                             -------------------
    début                  : Thu Feb 13 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cyboxdi.h"
#include "cycarddi.h"

CYCardDI::CYCardDI(QWidget *parent, const QString &name, int no, Mode m)
: CYCardDIG(parent, name, no, m)
{
  mNbCols=3;
  mNbRows=8;

  init();
}

CYCardDI::~CYCardDI()
{
}

void CYCardDI::init()
{
  int cpt=0;
  CYBoxDI *box;

  mGrid = new QGridLayout(this);
  this->setLayout(mGrid);

  for (int col=0; col<mNbCols; col++)
  {
// TOCHECK QT5    QVBoxLayout *vbox = new QVBoxLayout(col);
    QVBoxLayout *vbox = new QVBoxLayout(this);
    vbox->setObjectName(QString("colonne%1").arg(col));
    mVBox.append(vbox);
    vbox->setSpacing(6);

    for (int row=0, c=cpt; c<mNbRows+cpt ; row++, c++)
    {
      char buf[10];
      sprintf(buf,"%dE%02d", mNoCard, c);
      box = new CYBoxDI(this, buf, *mColors[col], (CYBoxIO::Mode) mMode);
      mBoxIOList.append(box);                                   
      mGrid->addWidget(box, row, col);
    }
    cpt += mNbRows;
  }
  adjustSize();
}

void CYCardDI::setData(QHash<int, CYDI*> db)
{
  mDatas = db;
  linkData();
}

void CYCardDI::linkData()
{
  CYBoxDI *box = (CYBoxDI *)mBoxIOList.first();
  QList<CYBoxDI*>::iterator it;

  for ( int i=0; i<mDatas.count(); i++)
  {
    if (box!=0)
      box->setData(mDatas[i]);
    else
      CYFATALOBJECTDATA(objectName(), mDatas[i]->objectName());
    it++;
    box = *it;
  }
}

void CYCardDI::updateCard()
{
  hide();
  while (!mBoxIOList.isEmpty())
    delete mBoxIOList.takeFirst();
  while (!mVBox.isEmpty())
    delete mVBox.takeFirst();
  delete mGrid;
  init();
  linkData();
  show();
}

void CYCardDI::simulation()
{
  emit simul();
}

QSize CYCardDI::sizeHint() const
{
  return QSize(QGroupBox::sizeHint());
}

QSize CYCardDI::minimumSizeHint() const
{
  if (QGroupBox::minimumSize().width() > mGrid->minimumSize().width())
    return QSize(QGroupBox::minimumSize());
  else
    return QSize(mGrid->minimumSize());
}
