#include "cyplotcurve.h"

/*!
  Constructor
  \param title Title of the curve
*/
CYPlotCurve::CYPlotCurve( const QwtText &title ):
    QwtPlotCurve( title )
{
  mKey=-1;
}

/*!
  Constructor
  \param title Title of the curve
*/
CYPlotCurve::CYPlotCurve( const QString &title ):
    QwtPlotCurve( QwtText( title ) )
{
  mKey=-1;
}

void CYPlotCurve::setKey(const int val)
{
  mKey = val;
  legendData();
}

/*!
   \brief Return all information, that is needed to represent
          the item on the legend

   Most items are represented by one entry on the legend
   showing an icon and a text, but f.e. QwtPlotMultiBarChart
   displays one entry for each bar.

   QwtLegendData is basically a list of QVariants that makes it
   possible to overload and reimplement legendData() to
   return almost any type of information, that is understood
   by the receiver that acts as the legend.

   The default implementation returns one entry with
   the title() of the item and the legendIcon().

   \return Data, that is needed to represent the item on the legend
   \sa title(), legendIcon(), QwtLegend, QwtPlotLegendItem
 */
QList<QwtLegendData> CYPlotCurve::legendData() const
{
  QwtLegendData data;

  QwtText label = title();
  label.setRenderFlags( label.renderFlags() & Qt::AlignLeft );

  QVariant titleValue;
  titleValue.setValue( label );
  data.setValue( QwtLegendData::TitleRole, titleValue );

  const QwtGraphic graphic = legendIcon( 0, legendIconSize() );
  if ( !graphic.isNull() )
  {
    QVariant iconValue;
    iconValue.setValue( graphic );
    data.setValue( QwtLegendData::IconRole, iconValue );
  }

  data.setValue(KeyRole, QVariant(mKey));

  QList<QwtLegendData> list;
  list += data;

  return list;
}
