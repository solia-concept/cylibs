/***************************************************************************
                          cyflaginput.h  -  description
                             -------------------
    début                  : jeu mai 15 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYFLAGINPUT_H
#define CYFLAGINPUT_H

// QT
#include <qobject.h>
#include <qpixmap.h>
//Added by qt3to4:
#include <QKeyEvent>
#include <QString>
// CYLIBS
#include "cycombobox.h"

/** Une CYComboBox qui permet de saisir la valeur d'un flag.
  * @short Boîde de saisie d'un flag.
  * @author Gérald LE CLEACH
  */

class CYFlagInput : public CYComboBox
{
  Q_OBJECT
  Q_PROPERTY(bool enableString  READ enableString  WRITE setEnableString)
  Q_PROPERTY(QString stringTrue  READ stringTrue  WRITE setStringTrue)
  Q_PROPERTY(QString stringFalse READ stringFalse WRITE setStringFalse)
  Q_PROPERTY(QByteArray pixmapFileFalse READ pixmapFileFalse WRITE setPixmapFileFalse)
  Q_PROPERTY(QByteArray pixmapFileTrue READ pixmapFileTrue WRITE setPixmapFileTrue)
  Q_PROPERTY(bool enablePixmap READ enablePixmap WRITE setEnablePixmap)

public:
  /** Construit une boîde saisie d'un flag avec
    * une valeur initiale du flag à \a false. */
  CYFlagInput(QWidget *parent=0, const QString &name="flagInput");
  ~CYFlagInput();

  /** @return la valeur courrante. */
  virtual int value() const;
  /** Fixe la valeur courante à \a value. */
  virtual void setValue(int value);

  /** @return \a true si la gestion de chaîne de caractères est active */
  bool enableString() const { return mEnableString; }
  /** Active ou désactive la gestion de chaîne de caractères. */
  void setEnableString(const bool state);

  /** @return la chaîne de caractères correspondant à un état vrai du flag. */
  QString stringTrue() const { return mStringtrue; }
  /** Saisie la chaîne de caractères correspondant à un état vrai du flag. */
  void setStringTrue(const QString string);

  /** @return la chaîne de caractères correspondant à un état faux du flag. */
  QString stringFalse() const { return mStringfalse; }
  /** Saisie la chaîne de caractères correspondant à un état faux du flag. */
  void setStringFalse(const QString string);

  /** @return \a true si la gestion d'image est active */
  bool enablePixmap() const { return mEnablePixmap; }
  /** Active ou désactive la gestion d'image. */
  void setEnablePixmap(const bool state);

  /** @return le nom du fichier de l'image de l'état vrai. */
  QByteArray pixmapFileTrue() const { return mPixmapFileTrue.toUtf8(); }
  /** Saisie le nom du fichier de l'image de l'état vrai. */
  virtual void setPixmapFileTrue(const QByteArray &file);

  /** @return le nom du fichier de l'image de l'état faux. */
  QByteArray pixmapFileFalse() const { return mPixmapFileFalse.toUtf8(); }
  /** Saisie le nom du fichier de l'image de l'état faux. */
  virtual void setPixmapFileFalse(const QByteArray &file);

  /** Place @p data comme la donnée de forçage du widget. */
  virtual void setForcingData(CYData *data);

public slots: // Public slots
  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();
  /** Met à jour la valeur de la donnée traîtée avec la valeur saisie. */
  virtual void update();

  virtual void setActivated(int index);
  virtual void setActivated(const QString &string);
  /** Change l'état du flag dans la boîte. */
  void setState(int index);

  /** Change le nom de la donnée courrante. \a Index est la position dans la liste des noms de données disponibles. */
  virtual void setCurrentDataName(int index);
  /** Change la donnée courrante. \a Index est la position dans la liste de données disponibles. */
  virtual void setCurrentData(int index);
    virtual void synopticForcingChanged(int);

signals: // Signals
  /** Emis à chaque changement d'état du flag dans la la boîte. */
  void state(bool);
  /** Signale que la donnée courrante vient de changer. */
  void currentDataChanged();

protected: // Protected attributes
  /** Vaut \a true si la gestion de chaîne de caractères est active */
  bool mEnableString;
  /** Chaîne de caractères correspondant à un état vrai du flag. */
  QString mStringtrue;
  /** Chaîne de caractères correspondant à un état faux du flag. */
  QString mStringfalse;
  /** Nom du fichier de l'image correspondant à un état vrai du flag. */
  QString mPixmapFileTrue;
  /** Nom du fichier de l'image correspondant à un état faux du flag. */
  QString mPixmapFileFalse;
  /** Vaut \a true si la gestion d'image est active */
  bool mEnablePixmap;

protected: // Protected methods
  /** Met à jour le widget en fonction de l'image et la chaîne de caractères. */
  void updatePixmapAndString();
  /** Gestion du clavier. */
  virtual void keyPressEvent(QKeyEvent * e);
};

#endif
