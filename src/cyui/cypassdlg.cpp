// vi: ts=8 sts=4 sw=4
/* This file is part of the KDE libraries
   Copyright (C) 1998 Pietro Iglio <iglio@fub.it>
   Copyright (C) 1999,2000 Geert Jansen <jansen@kde.org>
   Copyright (C) 2004,2005 Andrew Coles <andrew_coles@yahoo.co.uk>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/
#include <unistd.h>

#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QLayout>
#include <QSize>
#include <QEvent>
#include <qnamespace.h>
#include <QCheckBox>
#include <QRegExp>
#include <QWhatsThis>
#include <QHash>
#include <QProgressBar>
#include <QSettings>
#include <QStyle>
#include <QPushButton>
// ANSI
#include <sys/time.h>
// TODO QTWIN
//#include <sys/resource.h>
// CYLIBS
#include "cypassdlg.h"
#include "cyapplication.h"
#include "cycore.h"
#include "cymessagebox.h"

/*
 * Password line editor.
 */

// BCI: Add a real d-pointer and put the int into that

static QHash<void *, int*>* d_ptr = 0;

static void cleanup_d_ptr() {
	delete d_ptr;
}

static int * ourMaxLength( const CYPasswordEdit* const e ) {
	if ( !d_ptr ) {
		d_ptr = new QHash<void*, int*>;
		qAddPostRoutine( cleanup_d_ptr );
	}
	// TOCHECK QT5 int* ret = d_ptr[ (void*) e ];
	int *ret = d_ptr->value((void*) e);
	if ( ! ret ) {
		ret = new int;
		d_ptr->insert((void*) e, ret );
	}
	return ret;
}

static void delete_d( const CYPasswordEdit* const e ) {
	if ( d_ptr )
		// TOCHECK QT5 d_ptr->remove( (void*) e );
		/*delete*/ d_ptr->take( (void*) e );
}

const int CYPasswordEdit::PassLen = 200;

class CYPasswordDialog::CYPasswordDialogPrivate
{
public:
	CYPasswordDialogPrivate()
    : m_MatchLabel( 0 ), allowEmptyPasswords( true ),
			minimumPasswordLength(0), maximumPasswordLength(CYPasswordEdit::PassLen - 1),
			passwordStrengthWarningLevel(1), m_strengthBar(0),
			reasonablePasswordLength(8)
	{}
	QLabel *m_MatchLabel;
	QString iconName;
	bool allowEmptyPasswords;
	int minimumPasswordLength;
	int maximumPasswordLength;
	int passwordStrengthWarningLevel;
	QProgressBar* m_strengthBar;
	int reasonablePasswordLength;
};


CYPasswordEdit::CYPasswordEdit(QWidget *parent, const QString &name)
  : QLineEdit(parent)
{
  setObjectName(name);
  init();

  QSettings *cfg = cyapp->OSUserConfig();
  while (!cfg->group().isEmpty()) cfg->endGroup();
  cfg->beginGroup("Passwords");

  const QString val = cfg->value("EchoMode", "OneStar").toString();
  if (val == "ThreeStars")
    m_EchoMode = ThreeStars;
  else if (val == "NoEcho")
    m_EchoMode = NoEcho;
  else
    m_EchoMode = OneStar;

  setAttribute(Qt::WA_InputMethodEnabled, true );
}

CYPasswordEdit::CYPasswordEdit(QWidget *parent, const QString &name, int echoMode)
  : QLineEdit(parent), m_EchoMode(echoMode)
{
  setObjectName(name);
  init();
}

CYPasswordEdit::CYPasswordEdit(EchoModes echoMode, QWidget *parent, const QString &name)
  : QLineEdit(parent), m_EchoMode(echoMode)
{
  setObjectName(name);
  init();
}

CYPasswordEdit::CYPasswordEdit(EchoMode echoMode, QWidget *parent, const QString &name)
  : QLineEdit(parent)
  , m_EchoMode( echoMode == QLineEdit::NoEcho ? NoEcho : OneStar )
{
  setObjectName(name);
  init();
}

void CYPasswordEdit::init()
{
  setEchoMode(QLineEdit::Password); // Just in case
  setAcceptDrops(false);
  int* t = ourMaxLength(this);
  *t = (PassLen - 1); // the internal max length
  m_Password = new char[PassLen];
  m_Password[0] = '\000';
  m_Length = 0;
}

CYPasswordEdit::~CYPasswordEdit()
{
  memset(m_Password, 0, PassLen * sizeof(char));
  delete[] m_Password;
  delete_d(this);
}

void CYPasswordEdit::insert(const QString &txt)
{
  // TOCHECK QT4
//  const QString localTxt = txt.local8Bit();
  QByteArray localTxt = txt.toUtf8();
  const unsigned int lim = localTxt.length();
  const int m_MaxLength = maxPasswordLength();
  for(unsigned int i=0; i < lim; ++i)
  {
    const unsigned char ke = localTxt[i];
    if (m_Length < m_MaxLength)
    {
      m_Password[m_Length] = ke;
      m_Password[++m_Length] = '\000';
    }
  }
  showPass();
}

void CYPasswordEdit::erase()
{
  m_Length = 0;
  memset(m_Password, 0, PassLen * sizeof(char));
  setText("");
}

void CYPasswordEdit::focusInEvent(QFocusEvent *e)
{
  const QString txt = text();
  setUpdatesEnabled(false);
  QLineEdit::focusInEvent(e);
  setUpdatesEnabled(true);
  setText(txt);
}


void CYPasswordEdit::keyPressEvent(QKeyEvent *e)
{
  switch (e->key()) {
    case Qt::Key_Return:
    case Qt::Key_Enter:
    case Qt::Key_Escape:
      e->ignore();
      break;
    case Qt::Key_Backspace:
    case Qt::Key_Delete:
    case 0x7f: // Delete
      if (e->QInputEvent::modifiers() & (Qt::ControlModifier | Qt::AltModifier))
        e->ignore();
      else if (m_Length) {
        m_Password[--m_Length] = '\000';
        showPass();
      }
      break;
    default:
      const unsigned char ke = e->text().toUtf8()[0];
      if (ke >= 32) {
        insert(e->text());
      } else
        e->ignore();
      break;
  }
}

bool CYPasswordEdit::event(QEvent *e) {
  switch(e->type()) {

    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseButtonDblClick:
    case QEvent::MouseMove:
//      TODO QT4
//    case QEvent::IMStart:
//    case QEvent::IMCompose:
      return true; //Ignore

//      TODO QT4
//    case QEvent::IMEnd:
//    {
//      QIMEvent* const ie = (QIMEvent*) e;
//      if (!ie->text().isEmpty())
//        insert( ie->text() );
//      return true;
//    }

    case QEvent::ShortcutOverride:
    {
      QKeyEvent* const k = (QKeyEvent*) e;
      switch (k->key()) {
        case Qt::Key_U:
          if (k->QInputEvent::modifiers() & Qt::ControlModifier) {
            m_Length = 0;
            m_Password[m_Length] = '\000';
            showPass();
          }
      }
      return true; // stop bubbling
    }

    default:
      // Do nothing
      break;
  }
  return QLineEdit::event(e);
}

void CYPasswordEdit::showPass()
{
  QString tmp;

  switch (m_EchoMode) {
    case OneStar:
      tmp.fill('*', m_Length);
      setText(tmp);
      break;
    case ThreeStars:
      tmp.fill('*', m_Length*3);
      setText(tmp);
      break;
    case NoEcho: default:
      emit textChanged(QString()); //To update the password comparison if need be.
      break;
  }
}

void CYPasswordEdit::setMaxPasswordLength(int newLength)
{
  if (newLength >= PassLen) newLength = PassLen - 1; // belt and braces
  if (newLength < 0) newLength = 0;
  int* t = ourMaxLength(this);
  *t = newLength;
  while (m_Length > newLength) {
    m_Password[m_Length] = '\000';
    --m_Length;
  }
  showPass();
}

int CYPasswordEdit::maxPasswordLength() const
{
  return *(ourMaxLength(this));
}
/*
 * Password dialog.
 */

CYPasswordDialog::CYPasswordDialog(Types type, bool enableKeep, int extraBttn,
                                   QWidget *parent, const QString &name)
  : QDialog(parent), m_Keep(enableKeep? 1 : 0), m_Type(type), d(new CYPasswordDialogPrivate)
// TODO QT5
//  : QDialog(parent, name, true, "", Ok|Cancel|extraBttn,
//            Ok, true), m_Keep(enableKeep? 1 : 0), m_Type(type), d(new CYPasswordDialogPrivate)
{
  Q_UNUSED(extraBttn)
  setObjectName(name);
  d->iconName = "password";
  init();
}

CYPasswordDialog::CYPasswordDialog(Types type, bool enableKeep, int extraBttn, const QString& icon,
																	 QWidget *parent, const QString &name )
	: QDialog(parent), m_Keep(enableKeep? 1 : 0), m_Type(type), d(new CYPasswordDialogPrivate)
// TODO QT5
//	: QDialog(parent, name, true, "", Ok|Cancel|extraBttn,
//						Ok, true), m_Keep(enableKeep? 1 : 0), m_Type(type), d(new CYPasswordDialogPrivate)
{
	Q_UNUSED(extraBttn)
	setObjectName(name);
	if ( icon.trimmed().isEmpty() )
		d->iconName = "password";
	else
		d->iconName = icon;
	init();
}

CYPasswordDialog::CYPasswordDialog(int type, QString prompt, bool enableKeep,
                                   int extraBttn)
  : QDialog(0L), m_Keep(enableKeep? 1 : 0), m_Type(type), d(new CYPasswordDialogPrivate)
  // TODO QT5
//  : QDialog(0L, "Password Dialog", true, "", Ok|Cancel|extraBttn,
//            Ok, true), m_Keep(enableKeep? 1 : 0), m_Type(type), d(new CYPasswordDialogPrivate)
{
  Q_UNUSED(extraBttn)
  setObjectName("Password Dialog");
  d->iconName = "password";
  init();
  setPrompt(prompt);
}

void CYPasswordDialog::init()
{
  m_Row = 0;

  QSettings *cfg = cyapp->OSUserConfig();
  while (!cfg->group().isEmpty()) cfg->endGroup();
  cfg->beginGroup("Passwords");

  if (m_Keep && cfg->value("Keep", false).toBool())
    ++m_Keep;

  m_pGrid = new QGridLayout(this);
  m_pGrid->addItem(new QSpacerItem(10, 0), 0, 1);

  // Row 1: pixmap + prompt
  QLabel *lbl;
  const QPixmap pix( core->loadIcon(d->iconName, QStyle::PixelMetric(QStyle::PM_LargeIconSize) ));
  if (!pix.isNull())
  {
    lbl = new QLabel(this);
    lbl->setPixmap(pix);
    lbl->setAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    lbl->setFixedSize(lbl->sizeHint());
    m_pGrid->addWidget(lbl, 0, 0, Qt::AlignCenter);
  }

  m_pHelpLbl = new QLabel(this);
  m_pHelpLbl->setAlignment(Qt::AlignLeft|Qt::AlignVCenter);
  m_pHelpLbl->setWordWrap(true);
  m_pGrid->addWidget(m_pHelpLbl, 0, 1, Qt::AlignLeft);
  m_pGrid->addItem(new QSpacerItem(0, 10), 1, 0);
  m_pGrid->setRowStretch(1, 12);

  // Row 2+: space for 4 extra info lines
  m_pGrid->addItem(new QSpacerItem(0, 5), 6, 0);
  m_pGrid->setRowStretch(6, 12);

  // Row 3: Password editor #1
  lbl = new QLabel(this);
  lbl->setAlignment(Qt::AlignLeft|Qt::AlignVCenter);
  lbl->setText(tr("&Password:"));
  lbl->setFixedSize(lbl->sizeHint());
  m_pGrid->addWidget(lbl, 7, 0, Qt::AlignLeft);

  QHBoxLayout *h_lay = new QHBoxLayout();
  m_pGrid->addLayout(h_lay, 7, 1);
  m_pEdit = new CYPasswordEdit(this);
  m_pEdit2 = 0;
  lbl->setBuddy(m_pEdit);
  QSize size = m_pEdit->sizeHint();
  m_pEdit->setFixedHeight(size.height());
  m_pEdit->setMinimumWidth(size.width());
  h_lay->addWidget(m_pEdit);

  mButtonOk = new QPushButton(tr("&Ok"));
  mButtonCancel = new QPushButton(tr("&Cancel"));

  // Row 4: Password editor #2 or keep password checkbox

  if ((m_Type == Password) && m_Keep)
  {
    m_pGrid->addItem(new QSpacerItem(0, 10), 8, 0);
    m_pGrid->setRowStretch(8, 12);
    QCheckBox* const cb = new QCheckBox(tr("&Keep password"), this);
    cb->setFixedSize(cb->sizeHint());
    if (m_Keep > 1)
      cb->setChecked(true);
    else
      m_Keep = 0;
    connect(cb, SIGNAL(toggled(bool)), SLOT(slotKeep(bool)));
    m_pGrid->addWidget(cb, 9, 2, Qt::AlignLeft|Qt::AlignVCenter);
  }
  else if (m_Type == NewPassword)
  {
    m_pGrid->addItem(new QSpacerItem(0, 10), 8, 0);
    lbl = new QLabel(this);
    lbl->setAlignment(Qt::AlignLeft|Qt::AlignVCenter);
    lbl->setText(tr("&Verify:"));
    lbl->setFixedSize(lbl->sizeHint());
    m_pGrid->addWidget(lbl, 9, 0, Qt::AlignLeft);

    h_lay = new QHBoxLayout();
    m_pGrid->addLayout(h_lay, 9, 1);
    m_pEdit2 = new CYPasswordEdit(this);
    lbl->setBuddy(m_pEdit2);
    size = m_pEdit2->sizeHint();
    m_pEdit2->setFixedHeight(size.height());
    m_pEdit2->setMinimumWidth(size.width());
    h_lay->addWidget(m_pEdit2);

    // Row 6: Password strength meter
    m_pGrid->addItem(new QSpacerItem(0, 10), 10, 0);
    m_pGrid->setRowStretch(10, 12);

    QWidget *strengthBox = new QWidget(this);
    QHBoxLayout *layout = new QHBoxLayout;
    layout->setSpacing(10);

    m_pGrid->addWidget(strengthBox, 11, 0, 1, -2, Qt::AlignVCenter);
    QLabel* const passStrengthLabel = new QLabel(strengthBox);
    passStrengthLabel->setAlignment(Qt::AlignLeft|Qt::AlignVCenter);
    passStrengthLabel->setText(tr("Password strength meter:"));

    d->m_strengthBar = new QProgressBar(strengthBox);
    d->m_strengthBar->setMaximum(100);
    d->m_strengthBar->setTextVisible(false);
    const QString strengthBarWhatsThis(tr("The password strength meter gives an indication of the security "
                                          "of the password you have entered. To improve the strength of "
                                          "the password, try:\n"
                                          " - using a longer password;\n"
                                          " - using a mixture of upper- and lower-case letters;\n"
                                          " - using numbers or symbols, such as #, as well as letters."));
    passStrengthLabel->setToolTip(strengthBarWhatsThis);
    d->m_strengthBar->setToolTip(strengthBarWhatsThis);

    layout->addWidget(passStrengthLabel);
    layout->addWidget(d->m_strengthBar);
    strengthBox->setLayout(layout);

    // Row 6: Label saying whether the passwords match
    m_pGrid->addItem(new QSpacerItem(0, 10), 12, 0);
    m_pGrid->setRowStretch(12, 12);

    d->m_MatchLabel = new QLabel(this);
    d->m_MatchLabel->setAlignment(Qt::AlignCenter);
    d->m_MatchLabel->setWordWrap(true);
    m_pGrid->addWidget(d->m_MatchLabel, 13, 0, 1, -1, Qt::AlignVCenter);
    d->m_MatchLabel->setText(tr("Passwords do not match"));

    connect( m_pEdit, SIGNAL(textChanged(const QString&)), SLOT(enableOkBtn()) );
    connect( m_pEdit2, SIGNAL(textChanged(const QString&)), SLOT(enableOkBtn()) );
    enableOkBtn();
  }

  h_lay = new QHBoxLayout();
  m_pGrid->addItem(new QSpacerItem(0, 10), 14, 0);

  h_lay->addSpacerItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));
  h_lay->addWidget(mButtonOk);
  h_lay->addWidget(mButtonCancel);
  m_pGrid->addLayout(h_lay, 14, 1, Qt::AlignLeft);

  connect(mButtonOk, SIGNAL(clicked(bool)), this, SLOT(accept()));
  connect(mButtonCancel, SIGNAL(clicked(bool)), this, SLOT(reject()));

  erase();
}


CYPasswordDialog::~CYPasswordDialog()
{
  delete d;
}


void CYPasswordDialog::clearPassword()
{
  m_pEdit->erase();
}

/* KDE 4: Make it const QString & */
void CYPasswordDialog::setPrompt(QString prompt)
{
  m_pHelpLbl->setText(prompt);
//CYDEBUG  m_pHelpLbl->setFixedSize(275, m_pHelpLbl->heightForWidth(275));
}


QString CYPasswordDialog::prompt() const

{
  return m_pHelpLbl->text();
}


/* KDE 4: Make them const QString & */
void CYPasswordDialog::addLine(QString key, QString value)
{
	if (m_Row > 3)
		return;

	QLabel *lbl = new QLabel(key, this);
	lbl->setAlignment(Qt::AlignLeft|Qt::AlignTop);
	lbl->setFixedSize(lbl->sizeHint());
	m_pGrid->addWidget(lbl, m_Row+2, 0, Qt::AlignLeft);

	lbl = new QLabel(value, this);
	lbl->setAlignment(Qt::AlignTop);
	lbl->setWordWrap(true);
	lbl->setFixedSize(275, lbl->heightForWidth(275));
	m_pGrid->addWidget(lbl, m_Row+2, 2, Qt::AlignLeft);
	++m_Row;
}


void CYPasswordDialog::erase()
{
  m_pEdit->erase();
  m_pEdit->setFocus();
  if (m_Type == NewPassword)
    m_pEdit2->erase();
}


void CYPasswordDialog::slotOk()
{
	if (m_Type == NewPassword) {
		if (strcmp(m_pEdit->password(), m_pEdit2->password())) {
			CYMessageBox::sorry(this, tr("You entered two different "
																	 "passwords. Please try again."));
			erase();
			return;
		}
//	TOCHECK QT5	if (d->m_strengthBar && d->m_strengthBar->progress() < d->passwordStrengthWarningLevel) {
		if (d->m_strengthBar && d->m_strengthBar->value() < d->passwordStrengthWarningLevel) {
			int retVal = CYMessageBox::warningContinueCancel(this,
																											 tr(   "The password you have entered has a low strength. "
																														 "To improve the strength of "
																														 "the password, try:\n"
																														 " - using a longer password;\n"
																														 " - using a mixture of upper- and lower-case letters;\n"
																														 " - using numbers or symbols as well as letters.\n"
																														 "\n"
																														 "Would you like to use this password anyway?"),
																											 tr("Low Password Strength"));
			if (retVal == CYMessageBox::Cancel) return;
		}
	}
	if (!checkPassword(m_pEdit->password())) {
		erase();
		return;
	}
	accept();
}


void CYPasswordDialog::slotCancel()
{
	reject();
}


void CYPasswordDialog::slotKeep(bool keep)
{
	m_Keep = keep;
}


// static . antlarr: KDE 4: Make it const QString & prompt
int CYPasswordDialog::getPassword(QString &password, QString prompt,
                                  int *keep)
{
  const bool enableKeep = (keep && *keep);
  CYPasswordDialog* const dlg = new CYPasswordDialog(int(Password), prompt, enableKeep);
  const int ret = dlg->exec();
  if (ret == Accepted) {
    password = dlg->password();
    if (enableKeep)
      *keep = dlg->keep();
  }
  delete dlg;
  return ret;
}


// static . antlarr: KDE 4: Make it const QString & prompt
int CYPasswordDialog::getNewPassword(QString &password, QString prompt)
{
  CYPasswordDialog* const dlg = new CYPasswordDialog(NewPassword, prompt);
  dlg->setWindowTitle(tr("Password change"));
  const int ret = dlg->exec();
  if (ret == Accepted)
    password = dlg->password();
  delete dlg;
  return ret;
}


// static
void CYPasswordDialog::disableCoreDumps()
{
// TODO QTWIN
//  struct rlimit rlim;
//  rlim.rlim_cur = rlim.rlim_max = 0;
//  setrlimit(RLIMIT_CORE, &rlim);
// TODO QTWIN
}

void CYPasswordDialog::enableOkBtn()
{
  if (m_Type == NewPassword) {
    const bool match = strcmp(m_pEdit->password(), m_pEdit2->password()) == 0
        && (d->allowEmptyPasswords || m_pEdit->password()[0]);

    const QString pass(m_pEdit->password());

    const int minPasswordLength = minimumPasswordLength();

    if ((int) pass.length() < minPasswordLength)
    {
      mButtonOk->setEnabled( false );
    }
    else
    {
      mButtonOk->setEnabled( match );
    }

    if ( match && d->allowEmptyPasswords && m_pEdit->password()[0] == 0 ) {
      d->m_MatchLabel->setText( tr("Password is empty") );
    } else {
      if ((int) pass.length() < minPasswordLength) {
        if (minPasswordLength == 1)
          d->m_MatchLabel->setText(tr("Password must be at least 1 character long"));
        else
          d->m_MatchLabel->setText(tr("Password must be at least %1 characters long").arg(minPasswordLength));
      } else {
        d->m_MatchLabel->setText( match? tr("Passwords match")
                                       :tr("Passwords do not match") );
      }
    }

    // Password strength calculator
    // Based on code in the Master Password dialog in Firefox
    // (pref-masterpass.js)
    // Original code triple-licensed under the MPL, GPL, and LGPL
    // so is license-compatible with this file

    const double lengthFactor = d->reasonablePasswordLength / 8.0;


    int pwlength = (int) (pass.length() / lengthFactor);
    if (pwlength > 5) pwlength = 5;

    const QRegExp numRxp("[0-9]", Qt::CaseSensitive, QRegExp::RegExp2);
    int numeric = (int) (pass.count(numRxp) / lengthFactor);
    if (numeric > 3) numeric = 3;

    const QRegExp symbRxp("\\W", Qt::CaseInsensitive, QRegExp::RegExp2);
    int numsymbols = (int) (pass.count(symbRxp) / lengthFactor);
    if (numsymbols > 3) numsymbols = 3;

    const QRegExp upperRxp("[A-Z]", Qt::CaseSensitive, QRegExp::RegExp2);
    int upper = (int) (pass.count(upperRxp) / lengthFactor);
    if (upper > 3) upper = 3;

    int pwstrength=((pwlength*10)-20) + (numeric*10) + (numsymbols*15) + (upper*10);

    if ( pwstrength < 0 ) {
      pwstrength = 0;
    }

    if ( pwstrength > 100 ) {
      pwstrength = 100;
    }
    // TOCHECK QT5 d->m_strengthBar->setProgress(pwstrength);
    d->m_strengthBar->setValue(pwstrength);
  }
}


void CYPasswordDialog::setAllowEmptyPasswords(bool allowed) {
  d->allowEmptyPasswords = allowed;
  enableOkBtn();
}


bool CYPasswordDialog::allowEmptyPasswords() const {
  return d->allowEmptyPasswords;
}

void CYPasswordDialog::setMinimumPasswordLength(int minLength) {
  d->minimumPasswordLength = minLength;
  enableOkBtn();
}

int CYPasswordDialog::minimumPasswordLength() const {
  return d->minimumPasswordLength;
}

void CYPasswordDialog::setMaximumPasswordLength(int maxLength) {

  if (maxLength < 0) maxLength = 0;
  if (maxLength >= CYPasswordEdit::PassLen) maxLength = CYPasswordEdit::PassLen - 1;

  d->maximumPasswordLength = maxLength;

  m_pEdit->setMaxPasswordLength(maxLength);
  if (m_pEdit2) m_pEdit2->setMaxPasswordLength(maxLength);

}

int CYPasswordDialog::maximumPasswordLength() const {
  return d->maximumPasswordLength;
}

// reasonable password length code contributed by Steffen Mthing

void CYPasswordDialog::setReasonablePasswordLength(int reasonableLength) {

  if (reasonableLength < 1) reasonableLength = 1;
  if (reasonableLength >= maximumPasswordLength()) reasonableLength = maximumPasswordLength();

  d->reasonablePasswordLength = reasonableLength;

}

int CYPasswordDialog::reasonablePasswordLength() const {
  return d->reasonablePasswordLength;
}


void CYPasswordDialog::setPasswordStrengthWarningLevel(int warningLevel) {
  if (warningLevel < 0) warningLevel = 0;
  if (warningLevel > 99) warningLevel = 99;
  d->passwordStrengthWarningLevel = warningLevel;
}

int CYPasswordDialog::passwordStrengthWarningLevel() const {
  return d->passwordStrengthWarningLevel;
}


