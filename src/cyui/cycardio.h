/***************************************************************************
                          cycardio.h  -  description
                             -------------------
    début                  : jeu mar 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCARDIO_H
#define CYCARDIO_H

// QT
#include <QGroupBox>
#include <QHash>
#include <QList>
#include <QLayout>
#include <QColor>
#include <QButtonGroup>
// CYLIBS
#include "cydb.h"

template<class T> class CYTypeData;
class CYBoxIO;

/** CYCardIO est un widget représentant des cartes
  * électriques d'E/S.
  * @see CYCardDIG, CYCardANA
  * @short Widget d'une carte d'E/S.
  * @author Gérald LE CLEACH
  */

class CYCardIO : public QGroupBox
{
  Q_OBJECT
  Q_ENUMS(Mode)
  Q_PROPERTY(int num READ num WRITE setNum)
  Q_PROPERTY(Mode mode READ mode WRITE setMode)
  Q_PROPERTY(int col READ col WRITE setCol)
  Q_PROPERTY(int row READ row WRITE setRow)
  Q_PROPERTY(int numCol READ numCol WRITE setNumCol)
  Q_PROPERTY(QColor colorCol READ colorCol WRITE setColorCol)

public:
  /** @short Mode d'utilisation du widget */
  enum Mode
  {
    /** Visualise l'état des l'E/S. */
    View,
    /** Visualise l'état des l'E/S et permet de forcer les sorties. */
    Forcing
  };

  /** Constructeur */
  CYCardIO(QWidget *parent=0, const QString &name=0, int no=0, Mode m=View);
  /** Destructeur */
  ~CYCardIO();

  /** Retourne le mode d'utilisation du widget. */
  Mode mode() const;
  /** Retourne le numéro de la carte.
    * @see setNum() */
  int num() const;
  /** Retourne le nombre de colonnes à représenter.
    * @see row(), setCol(), setRow() */
  int col() const;
  /** Retourne le nombre de lignes à représenter.
   * @see col(), setRow(), setCol() */
  int row() const;
  /** Retourne le numéro de colonne à configurer.
   * @see setNumCol(), setColorCol() et colorCol() */
  int numCol() const;
  /** Retourne la couleur de colonne à configurer.
   * @see setColorCol(), setNumCol() et numCol() */
  QColor colorCol() const;
  /** Retourne la couleur de la colonne \a col. */
  QColor colorCol(int col) const;

  /** Saisie le mode d'utilisation du widget.
    * @param mode Mode d'utilisation du widget. */
  void setMode(const Mode mode);
  /** Saisie le numéro de la carte.
   * Le widget sera redessiné immédiatement.
   * @param num Numéro de la carte. */
  void setNum(const int num);
  /** Saisie le nombre de colonnes à représenter.
    * Le widget sera redessiné immédiatement.
    * @param col Nombre de colonnes à représenter. */
  void setCol(const int col);
  /** Saisie le nombre de lignes à représenter.
    * Le widget sera redessiné immédiatement.
    * @param row Nombre de lignes à représenter. */
  void setRow(const int row);
  /** Saisie le numéro de la colonne à configurer.
    * @param num Numéro de la colonne.
    * @see setColorCol(), numCol() et colorCol() */
  void setNumCol(const int col);
  /** Saisie la couleur de la colonne à configurer.
    * Le widget sera redessiné immédiatement.
    * @param num Le numéro de la colonne.
    * @see setNumCol(), numCol() et colorCol() */
  void setColorCol(const QColor &color);

  /** Saisie la couleur \a color de toutes LED. */
  void setLEDColor(const QColor &color);
  /** Saisie la couleur \a color des LED de la colonne \a num. */
  void setLEDColor(int num, const QColor &color);

  virtual QSize sizeHint() const;
  virtual QSize minimumSizeHint() const;

  virtual void addButton ( QAbstractButton * button );
  virtual void removeButton ( QAbstractButton * button );

signals:
  /** Emet un signal d'aide sur la donnée en cours. */
  void help(const QString &msg);
  /** Donne l'ordre de simuler les E/S. */
  void simul();

public slots: // Public slots
  /** Raffraîchit les valeurs. */
  virtual void refresh();
  /** Simulation. */
  virtual void simulation() {}
  /** Affiche un message d'aide. */
  void setHelp(const QString &msg);
  /** Le widget ayant le focus informe le widget de la carte que c'est lui qui a le focus. */
  void focusInfo(CYBoxIO *box);

protected:
  /** Initialise les widgets selon les paramètres de la carte. */
  virtual void init() {}
  /** Met à jour les widgets. */
  virtual void updateCard() {}

protected: // Protected attributes
  /** Liste des bases de données d'écriture en mode forçage sur le réseau. */
  QHash<QString, CYDB*> mForcingDB;
  /** Numéro de carte. */
  int mNoCard;
  /** Mode d'utilisation. */
  Mode mMode;
  /** Numéro de colonnes. */
  int mNoCol;
  /** Nombre maximum de colonnes. */
  int mMaxCols;
  /** Nombre de colonnes. */
  int mNbCols;
  /** Nombre de lignes. */
  int mNbRows;
  /** Boîte d'E/S courrante. */
  CYBoxIO *mCurrentBoxIO;
  /** Listes des boîte d'E/S. */
  QList<CYBoxIO*> mBoxIOList;
  /** Couleur des leds de chaque colonne de boîte d'E/S. */
  QHash<int, QColor*>  mColors;
  /** Vaut \a true si le widget a le focus. */
  bool mHasFocus;
  /** Grille de positionnement. */
  QGridLayout *mGrid;
  /** Positionnement vertical. */
  QList<QVBoxLayout*> mVBox;
  /** Groupe de boutons. */
  QButtonGroup *mButtonGroup;
};

#endif
