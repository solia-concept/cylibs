//
// C++ Implementation: cycooler
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <qapplication.h>
//Added by qt3to4:
#include <QString>
// CYLIBS
#include "cycooler.h"

CYCooler::CYCooler(QWidget *parent, const QString &name)
 : CYPixmapView(parent, name)
{
  setPixmapFileTrue(":/pixmaps/cycooler_on.png");
  setPixmapFileFalse(":/pixmaps/cycooler_off.png");
}


CYCooler::~CYCooler()
{
}
