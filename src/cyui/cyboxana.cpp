/***************************************************************************
                          cyboxana.cpp  -  description
                             -------------------
    début                  : jeu mar 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cydata.h"
#include "cycardana.h"
#include "cyboxana.h"

CYBoxANA::CYBoxANA(QWidget *parent, const QString &name, Mode m)
: CYBoxIO(parent, name, m)
{
}

CYBoxANA::~CYBoxANA()
{
}
