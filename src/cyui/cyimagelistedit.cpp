//
// C++ Implementation: cyimagelistedit
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <QPushButton>
#include <QFileDialog>
// CYLIBS
#include "cyimagelistedit.h"
#include "cyimagelist.h"
#include "cyimage.h"
#include "ui_cyimagelistedit.h"

CYImageListEdit::CYImageListEdit(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYImageListEdit)
{
  ui->setupUi(this);
  mNbMax=99;
  mList = new CYImageList(this, QString("%1_LIST").arg(name));
}


CYImageListEdit::~CYImageListEdit()
{
  delete ui;
}

void CYImageListEdit::showEvent(QShowEvent *e)
{
  CYDialog::showEvent(e);
  for ( int i = 0; i < mList->images.count(); ++i )
  {
    CYImage *image = mList->images.at(i);
    if ( image )
      addImage(image->index(), image->label(), image->fileName());
  }
}

void CYImageListEdit::setModifieView()
{
  ui->buttonOk->setEnabled(true);
}

void CYImageListEdit::setValidateView()
{
  ui->buttonOk->setEnabled(false);
}

/*! Ajoute une image par sélection d'un fichier
    \fn CYImageListEdit::addImage()
 */
void CYImageListEdit::addImage()
{
  QString startWith = getenv( "HOME" );
  QString filter = "*.png *.xpm *.jpg";

  QString fileName = QFileDialog::getOpenFileName( 0, tr("Choose an image to add into this list."), startWith, filter);

  if (fileName.isEmpty())
    return;

  QFileInfo fileInfo(fileName);
  QString label=fileInfo.baseName();

  // TOCHECK QT5 addImage(listView->childCount()+1, label, fileName);
  addImage(ui->listView->children().count()+1, label, fileName);
  setModifieView();
}

/*! Ajoute une image par sélection d'un fichier
    \fn CYImageListEdit::addImage(int index, QString label, QString fileName)
 */
void CYImageListEdit::addImage(int index, QString label, QString fileName)
{
  QImage image(fileName);
  image=image.scaledToHeight(128);

  QPixmap pixmap;
  pixmap.convertFromImage(image);

  QTreeWidgetItem *item = new QTreeWidgetItem( ui->listView );
  char buf[5];
  sprintf(buf,"%02d", index);
  item->setText   (0, QString("%1").arg(buf));
  item->setText   (1, label    );
  item->setIcon   (2, QIcon(pixmap)   );
  item->setText   (3, fileName );

  ui->listView->addTopLevelItem(item);
}

void CYImageListEdit::deleteImage()
{
  char buf[5];
  QTreeWidgetItem *item = ui->listView->currentItem();
  if (!item)
    return;
  int index = item->text(0).toInt();
  // TOCHECK QT5 listView->takeItem(item);
  delete item;

  // TOCHECK QT5 item = listView->firstChild();
  item = ui->listView->topLevelItem(0);
  while(item)
  {
    if (item->text(0).toInt()>index)
    {
      sprintf(buf,"%02d", item->text(0).toInt()-1);
      item->setText(0, QString("%1").arg(buf));
    }
    // TOCHECK QT5 item = item->nextSibling();
    item = ui->listView->topLevelItem(ui->listView->indexOfTopLevelItem(item)+1);
  }
  setModifieView();
}

/*! Appliquer la liste
    \fn CYImageListEdit::apply()
 */
void CYImageListEdit::apply()
{
  mList->clear();
  QTreeWidgetItem * item;
  // TOCHECK QT5 item = listView->firstChild();
  item = ui->listView->topLevelItem(0);
  while( item)
  {
    mList->addImage(item->text(0).toInt(), item->text(1), item->text(3));
    // TOCHECK QT5 item = item->nextSibling();
    item = ui->listView->topLevelItem(ui->listView->indexOfTopLevelItem(item)+1);
  }
  setValidateView();
}

/*! Chargement de la liste des images.
    \fn CYImageListEdit::load(const QString &fileName)
 */
bool CYImageListEdit::load(const QString &fileName)
{
  return mList->load(fileName);
}

/*! Sauvegarde de la liste des images.
    \fn CYImageListEdit::save(const QString &fileName)
 */
bool CYImageListEdit::save(const QString &fileName)
{
  return mList->save(fileName);
}
