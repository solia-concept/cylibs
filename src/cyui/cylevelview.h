/***************************************************************************
                          cylevelview.h  -  description
                             -------------------
    begin                : dim jan 25 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYLEVELVIEW_H
#define CYLEVELVIEW_H

// QWT
#include "qwt_thermo.h"
//Added by qt3to4:
#include <QString>
#include <QWheelEvent>
// CYLIBS
#include "cydatawdg.h"

/** @short Affichage graphique d'un niveau.
  * @author LE CLÉACH Gérald
  */

class CYLevelView : public QwtThermo, public CYDataWdg
{
  Q_OBJECT
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(bool sensInversed READ sensInversed WRITE setSensInversed)
  Q_PROPERTY(QColor fillColor READ fillColor WRITE setFillColor)
  Q_PROPERTY(QColor alarmColor READ alarmColor WRITE setAlarmColor)
  Q_PROPERTY(double alarmLevel READ alarmLevel WRITE setAlarmLevel)
  Q_PROPERTY(bool alarmEnabled READ alarmEnabled WRITE setAlarmEnabled)
  Q_PROPERTY(bool alarmInversed READ alarmInversed WRITE setAlarmInversed)
  Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
  Q_PROPERTY(QColor foregroundColor READ foregroundColor WRITE setForegroundColor)
  Q_PROPERTY(QPixmap backgroundPixmap READ backgroundPixmap WRITE setBackgroundPixmap)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:
  CYLevelView(QWidget *parent=0, const QString &name=0);
  ~CYLevelView();

  //    enum ScalePos {None, Left, Right, Top, Bottom};

public: // Public methods
  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return \a true si le sens de remplissage est inversé. */
  bool sensInversed() const { return mSensInversed; }
  /** Inverse ou non le sens de remplissage. */
  void setSensInversed(const bool flag);

  /** @return la couleur de remplissage. */
  QColor fillColor() const { return fillBrush().color(); }
  /** Saisie la couleur de remplissage. */
  void setFillColor(const QColor color)
  {
    QBrush brush = fillBrush();
    brush.setColor(QColor(color));
    setFillBrush(brush);
  }

  /** @return la couleur d'alarme. */
  QColor alarmColor() const { return alarmBrush().color(); }
  /** Saisie la couleur d'alarme. */
  void setAlarmColor(const QColor color)
  {
    QBrush brush = alarmBrush();
    brush.setColor(color);
    setAlarmBrush(brush);
  }

  /** @return la couleur d'alarme. */
  double alarmLevel() const { return QwtThermo::alarmLevel(); }
  /** Saisie la couleur d'alarme. */
  void setAlarmLevel(const double level) { QwtThermo::setAlarmLevel(level); }

  /** @return \a true si la gestion d'alarme est active. */
  bool alarmEnabled() const { return QwtThermo::alarmEnabled(); }
  /** Activation/désactive la gestion d'alarme. */
  void setAlarmEnabled(const bool flag) { QwtThermo::setAlarmEnabled(flag); }

  /** @return \a true si la gestion d'alarme est inversée. */
  bool alarmInversed() const { return mAlarmInversed; }
  /** Inverse ou non la gestion d'alarme. */
  void setAlarmInversed(const bool flag) { mAlarmInversed = flag; }

  /** @return l'état de l'alarme suivant \a val. */
  bool alarmState(double val);

  virtual QSize minimumSizeHint() const;
  virtual QSizePolicy sizePolicy() const;

  /** Saisie la couleur de fond du widget. */
  virtual void setBackgroundColor ( const QColor & );
  /** @return la couleur de fond du widget */
  virtual const QColor & backgroundColor () const;

  /** Saisie la couleur du premier plan du widget. */
  virtual void setForegroundColor ( const QColor & );
  /** @return la couleur du premier plan du widget */
  virtual const QColor & foregroundColor () const;

  /** Saisie l'image de fond. */
  virtual void setBackgroundPixmap ( const QPixmap & );
  /** @return la couleur du premier plan du widget */
  virtual const QPixmap & backgroundPixmap () const;

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

public Q_SLOTS:
  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();
  virtual void setValue( double val );

  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();

protected: // Protected methods
  virtual QRect alarmRect( const QRect & ) const;
  virtual void drawLiquid( QPainter *, const QRect & ) const;
  /** Gestion de la molette. */
  virtual void wheelEvent(QWheelEvent * e);

protected: // Protected attributes
  /** Vaut \a true si la gestion d'alarme est inversée. */
  bool mAlarmInversed;
  /** Etat de l'alarme */
  bool mAlarmState;
};

#endif
