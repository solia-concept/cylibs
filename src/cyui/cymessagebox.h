/*
 *                              cymessagebox.h
 *
 *  Created on: 18 janv. 2015
 *  Author    : Gérald LE CLEACH
 *  email     : gerald.lecleach@solia-concept.fr
 */

/***************************************************************************
 *
 *                Ce fichier fait partie des bibliothèques
 *             utiles au développement d'un superviseur CYLIX
 *
 ***************************************************************************/
#ifndef CYMESSSAGEBOX_H
#define CYMESSSAGEBOX_H


// QT
#include <QObject>
#include <qmessagebox.h>

/**
@short Remplace CYMessageBox.

    @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMessageBox: public QMessageBox
{
  Q_OBJECT

public:
  /**
   * Button types.
   **/
  enum ButtonCode
  {
    Ok = 1,
    Cancel = 2,
    Yes = 3,
    No = 4,
    Continue = 5
  };

  CYMessageBox( QWidget * parent = 0, const char * name = 0 );
  virtual ~CYMessageBox();

  /**
  * Affiche une boîte "Information".
  *
  * @param parent  Si @p parent est 0, alors la boîte de message then the message box becomes an
  *                application-global modal dialog box. If @p parent is a
  *                widget, the message box becomes modal relative to parent.
  * @param caption Message box title. The application name is added to
  *                the title. The default title is i18n("Information").
  * @param text    Message string.
  *
  * Your program wants to tell the user something.
  * To be used for things like:
  * "Your bookmarks have been rearranged."
  *
  * The default button is "&OK". Pressing "Esc" selects the OK-button.
  *
  *  NOTE: The OK button will always have the i18n'ed text '&OK'.
  */

  static void information(QWidget *parent,
              const QString &text,
              const QString &caption = QString());

  static int warningYesNo(QWidget *parent,
                          const QString &text,
                          const QString &caption = QString(),
                          const QString &buttonYes = tr("&Yes"),
                          const QString &buttonNo = tr("&No"));

  static int warningYesNoCancel(QWidget *parent,
                                const QString &text,
                                const QString &caption = QString(),
                                const QString &buttonYes = tr("&Yes"),
                                const QString &buttonNo = tr("&No"),
                                const QString &buttonCancel = tr("&Cancel"));

  static int warningContinueCancel(QWidget *parent,
                                   const QString &text,
                                   const QString &caption = QString(),
                                   const QString &buttonContinue = tr("C&ontinue"),
                                   const QString &buttonCancel = tr("&Cancel"));

  static void sorry(QWidget *parent,
                    const QString &text,
                    const QString &caption = QString());

  static void error(QWidget *parent,
                    const QString &text,
                    const QString &caption = QString());


  static int questionYesNo(QWidget *parent,
                                const QString &text,
                                const QString &caption = QString(),
                                const QString &buttonYes = tr("&Yes"),
                                const QString &buttonNo = tr("&No"));

  static int questionYesNoCancel(QWidget *parent,
                                const QString &text,
                                const QString &caption = QString(),
                                const QString &buttonYes = tr("&Yes"),
                                const QString &buttonNo = tr("&No"),
                                const QString &buttonCancel = tr("&Cancel"));
};

#endif // CYMESSSAGEBOX_H
