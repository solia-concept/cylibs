//
// C++ Interface: cyimagelistedit
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYIMAGELISTEDIT_H
#define CYIMAGELISTEDIT_H

// CYLIBS
#include <cydialog.h>

class CYImageList;

namespace Ui {
		class CYImageListEdit;
}

/**
	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYImageListEdit : public CYDialog
{
Q_OBJECT
public:
    CYImageListEdit(QWidget *parent = 0, const QString &name = 0);

    ~CYImageListEdit();

    CYImageList *list() { return mList; }

    virtual bool load(const QString &fileName);
    virtual bool save(const QString &fileName);

    virtual void addImage(int index, QString label, QString fileName);

public slots:
    virtual void setModifieView();
    virtual void setValidateView();
    virtual void addImage();
    virtual void deleteImage();
    void apply();

protected: // Protected methods
  /** Fonction appelée à l'affichage de la boîte. */
  virtual void showEvent(QShowEvent *e);

protected:
    CYImageList * mList;
    int mNbMax;

private:
    Ui::CYImageListEdit *ui;
};

#endif
