//
// C++ Interface: cydataseditlist
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDATASEDITLIST_H
#define CYDATASEDITLIST_H

// QT
#include <QTreeWidgetItem>
// CYLIBS
#include <cywidget.h>

class CYDisplayItem;
class CYDatasListView;
class CYDatasBrowser;

namespace Ui {
    class CYDatasEditList;
}

/**
@short Widget d'édition d'une liste de données.

@author Gérald LE CLEACH
*/
class CYDatasEditList : public CYWidget
{
    Q_OBJECT
    Q_PROPERTY( int maxNbDatas READ maxNbDatas WRITE setMaxNbDatas )
    Q_PROPERTY( bool withColor READ withColor WRITE setWithColor )
    Q_PROPERTY( bool withPen READ withPen WRITE setWithPen )
    Q_PROPERTY( bool withCoef READ withCoef WRITE setWithCoef )
    Q_PROPERTY( bool withOffset READ withOffset WRITE setWithOffset )
    Q_PROPERTY( bool enableEditLabel READ enableEditLabel WRITE setEnableEditLabel )
    Q_PROPERTY( bool enableDelete READ enableDelete WRITE setEnableDelete )

public:
    CYDatasEditList(QWidget *parent = 0, const QString &name = 0);

    ~CYDatasEditList();
    virtual void addData(CYDisplayItem *item, int pos=-1);
    virtual int maxNbDatas() const;
    virtual bool withColor() const;
    virtual bool withPen() const;
    virtual bool withCoef() const;
    virtual bool withOffset() const;
    virtual bool enableEditLabel() const;
    virtual bool enableDelete() const;

    CYDatasListView *list();
    CYDatasBrowser *browser();

    void hideBrowser();

public slots:
    virtual void selectionChanged(const QModelIndex &index);
    virtual void setMaxNbDatas(const int val);
    virtual void setWithColor(const bool val);
    virtual void setWithPen(const bool val);
    virtual void setWithCoef(const bool val);
    virtual void setWithOffset(const bool val);
    virtual void setEnableEditLabel(const bool val);
    virtual void setEnableDelete(const bool val);
    void enabelFilterBox(bool val);
protected:
    bool mEnableEditLabel;
    bool mEnableDelete;

private:
    Ui::CYDatasEditList *ui;
};

#endif
