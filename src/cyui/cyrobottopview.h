/***************************************************************************
                          cyrobottopview.h  -  description
                             -------------------
    begin                : lun nov 1 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYROBOTTOPVIEW_H
#define CYROBOTTOPVIEW_H

// CYLIBS
#include "cydial.h"
// CYLIBS
#include "cydatawdg.h"
//Added by qt3to4:
#include <QString>

class CYRobotTopViewArm;

/** @short Vue du dessus d'un robot.
  * Représente la position angulaire et la course du bras d'un robot.
  * @author LE CLÉACH Gérald
  */

class CYRobotTopView : public QwtDial, public CYDataWdg
{
  Q_OBJECT
  Q_PROPERTY(QByteArray dataName READ dataName WRITE setDataName)
  Q_PROPERTY(QByteArray flagName READ flagName WRITE setFlagName)
  Q_PROPERTY(bool inverseFlag READ inverseFlag WRITE setInverseFlag)
  Q_PROPERTY(QByteArray hideFlagName READ hideFlagName WRITE setHideFlagName)
  Q_PROPERTY(bool inverseHideFlag READ inverseHideFlag WRITE setInverseHideFlag)
  Q_PROPERTY(QByteArray benchType READ benchType WRITE setBenchType)
  Q_PROPERTY(QByteArray lengthDataName READ lengthDataName WRITE setLengthDataName)
  Q_PROPERTY(bool treatChildDataWdg READ treatChildDataWdg WRITE setTreatChildDataWdg)

public:
  CYRobotTopView(QWidget *parent=0, const QString &name=0);
  ~CYRobotTopView();

  /** @return le nom de la donnée traîtée. */
  virtual QByteArray dataName() const;
  /** Saisie le nom de la donnée traîtée. */
  virtual void setDataName(const QByteArray &name);

  /** @return le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual QByteArray flagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'activation de cet objet graphique. */
  virtual void setFlagName(const QByteArray &name);

  /** @return si le flag associé d'activation doit être pris en compte dans le sens inverse. */
  virtual bool inverseFlag() const;
  /** Inverse le sens du flag associé d'activation si \a inverse est à \p true. */
  virtual void setInverseFlag(const bool inverse);

  /** @return le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual QByteArray hideFlagName() const;
  /** Saisie le nom de la donnée flag associée qui gère l'affichage ou non de cet objet graphique. */
  virtual void setHideFlagName(const QByteArray &name);

  /** @return si le flag associé d'affichage doit être pris en compte dans le sens inverse. */
  virtual bool inverseHideFlag() const;
  /** Inverse le sens du flag associé d'affichage si \a inverse est à \p true. */
  virtual void setInverseHideFlag(const bool inverse);

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);

  /** @return le nom de la donnée représentant la longueur du bras déployé en pourcentage de sa course. */
  virtual QByteArray lengthDataName() const;
  /** Saisie le nom de la donnée représentant la longueur du bras déployé en pourcentage de sa course. */
  virtual void setLengthDataName(const QByteArray &name);

  /** @return \a true alors appels aux fonctions de gestion de données des widgets qu'il contient. */
  bool treatChildDataWdg() const { return mTreatChildDataWdg; }
  /** Saisir \a true pour appels aux fonctions de gestion de données des widgets qu'il contient. */
  void setTreatChildDataWdg(const bool val) { mTreatChildDataWdg = val; }

  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

  /** Place @p data comme la donnée supplémentaire du widget. */
  virtual void setData2(CYData *data);

public slots: // Public slots
  /** Charge la valeur de la donnée représentant la position angulaire du bras. */
  virtual void refresh();
  /** Charge la valeur de la donnée représentant la longueur du bras déployé en pourcentage de sa course. */
  virtual void refresh2();
  /** Contrôle le flag d'affichage et affiche en fonction l'objet graphique. */
  virtual void ctrlHideFlag();
  /** Configure le widget en fonction de la donnée représentant la position angulaire du bras. */
  virtual void linkData();
  /** Configure le widget en fonction de la donnée représentant la longueur du bras déployé en pourcentage de sa course. */
  virtual void linkData2();

protected: // Protected methods
  /** Dessine l'intérieur du disque de rotation. */
  virtual void drawContents(QPainter *painter) const;
  /** Traîtement réalisé à chaque changement de valeur. */
  virtual void valueChange();

public slots: // Public slots
  /** Saisie la longueur du bras déployé en pourcentage de sa course. */
  void setLength(double val);

protected: // Protected attributes
  /** Nom de la donnée représentant la longueur du bras déployé en pourcentage de sa course. */
  QString mDataLengthName;
  /** Longueur minimum du bras (non déployé). */
  double mMinLength;
  /** Longueur maximum du bras (entièrement déployé). */
  double mMaxLength;
  /** Autorise ou non le traîtement réalisé lors d'un cha,gement de valeur.
    * Ceci permet de dessiner en une seule fois au lieu de 2, la représentation du robot
    * une fois que la position angulaire et la longueur du bras déployé ont été rafraîchies. */
  bool mEnableValueChange;
  /** Représentation du bras. */
  CYRobotTopViewArm *mArm;
};

#endif
