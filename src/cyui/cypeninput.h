//
// C++ Interface: cypeninput
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYPENINPUT_H
#define CYPENINPUT_H

// CYLIBS
#include <cywidget.h>

class CYPen;

namespace Ui {
		class CYPenInput;
}

/**
@short Widget de configuration d'un crayon.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYPenInput : public CYWidget
{
  Q_OBJECT

public:
    CYPenInput(QWidget *parent=0, const QString &name="cyPenInput");

    ~CYPenInput();
    virtual void setPen( CYPen *pen );

protected:
    CYPen *mPen;
  /** Préfixe du nom des données.  */
  QString mPrefix;

private:
    Ui::CYPenInput *ui;
};

#endif
