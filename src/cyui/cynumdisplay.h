/***************************************************************************
                          cynumdisplay.h  -  description
                             -------------------
    début                  : jeu mai 15 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYNUMDISPLAY_H
#define CYNUMDISPLAY_H

// QT
#include <QMouseEvent>
#include <QFocusEvent>
// CYLIBS
#include "cylineedit.h"

class CYNumDisplay : public CYLineEdit
{
  Q_OBJECT
  Q_ENUMS(Format)
  Q_ENUMS(NumBase)
  Q_PROPERTY(bool showLabel READ showLabel WRITE setShowLabel)
  Q_PROPERTY(double minValue READ minValue WRITE setMinValue)
  Q_PROPERTY(double maxValue READ maxValue WRITE setMaxValue)
  Q_OVERRIDE(double lineStep READ lineStep WRITE setLineStep)
  Q_PROPERTY(NumBase numBase READ numBase WRITE setNumBase)
  Q_PROPERTY(Format format READ format WRITE setFormat)
  Q_PROPERTY(bool showUnit READ showUnit WRITE setShowUnit)

public:
  /** Base numérique en cas de donnée entière positive */
  /** Format d'affichage. */
  enum Format
  {
    /** Affichage normal. */
    basic,
    /** Affichage d'un temps en ##h##m##s. */
    hms,
  };

  /** Construit un afficheur d'une donnée numérique avec comme valeur initiale 0.0. */
  CYNumDisplay(QWidget *parent=0, const QString &name="numDisplay");

  /** Construit un afficheur d'une donnée numérique avec comme valeur initiale \a value.
    * @param value  Valeur initiale.
    * @param parent QWidget parent.
    * @param name   Nom interne de ce widget.
    * @param base   Base numérique utilisée pour l'affichage. */
  CYNumDisplay(double value, QWidget *parent=0, const QString &name=0, long base=10);

  /** Destructeur. */
  ~CYNumDisplay();

  /** @return \a true s'il faut y afficher l'étiquette de la donnée. */
  virtual bool showLabel() const { return mShowLabel; }
  /** Saisir \a true pour y afficher l'étiquette de la donnée. */
  virtual void setShowLabel(const bool val) { mShowLabel = val; setAlwaysOk(true); }

  /** @return la valeur courante. */
  double value();

  /** Fixe la plage de valeurs valides.
    * @param min  Valeur minimum.
    * @param max  Valeur maximum.
    * @param step Taille du pas d'incrémentation.
    * @param dec  Nombre de décimales.
    */
  void setRange(double min, double max, double step=1, int dec=2);

  /** Fixe la valeur minimum. */
  void setMinValue(double min);
  /** @return la valeur minimum. */
  double minValue() const;

  /** Fixe la valeur maximum. */
  void setMaxValue(double max);
  /** @return la valeur maximum. */
  double maxValue() const;

  /** Fixe la taille du pas d'incrémentation/de décrémentation. */
  void setLineStep(double step);
  /** @return la taille du pas courrant. */
  double lineStep() const;

  /** Authorise/inhibe l'affichage de l'unité. */
  void setShowUnit(bool show=false) { mShowUnit = show; }
  /** @return \a true si l'affichage de l'unité est authorisé. */
  bool showUnit() const { return mShowUnit; }

  /** @return la base numérique. */
  NumBase numBase() const { return mNumBase; }
  /** Saisie la base numérique. */
  void setNumBase(const NumBase b);

    /** @return le format d'affichage. */
  Format format() const;
  /** Saisie le format d'affichage de la valeur. */
  void setFormat(const Format fmt);
  /** Saisie le nombre de décimales */
  void setNbDec(const int nb) { mDec = nb; }

  void interpretText();

  /** Ici le passage en lecture seule n'a pas lieu d'être puisqu'un
    * afficheur n'est jamais en écriture. On annule donc ici le code
    * de la classe dont hérite CYNumDisplay, c'est à dire CYLineEdit. */
  virtual void setReadOnly(const bool val);
  /** Place @p data comme donnée du widget. */
  virtual void setData(CYData *data);

signals:
  /** Emis à chaque fois que la valeur change (par appel de setValue() ou
    * par une intéraction de l'utilisateur. */
  void valueChanged(double);
  /** Signale que la donnée courrante vient de changer. */
  void currentDataChanged();

public slots: // Public slots
  /** Place la valeur du contrôleur. */
  void setValue(const double);

  /** Charge la valeur de la donnée traîtée. */
  virtual void refresh();
  /** Charge la valeur constructeur de la donnée traîtée. */
  virtual void designer();

  /** Configure le widget en fonction de la donnée à laquelle il est liée. */
  virtual void linkData();

  /** Change le nom de la donnée courrante. \a Index est la position dans la liste des noms de données disponibles. */
  virtual void setCurrentDataName(int index);
  /** Change la donnée courrante. \a Index est la position dans la liste de données disponibles. */
  virtual void setCurrentData(int index);

protected: // Protected methods
  /* Ce gestionnaire d'événements peut être mis en œuvre pour gérer les changements d'état.
     Qt5: Gestion de QEvent::EnabledChange remplace setEnabled(bool val) qui n'est plus surchargeable. */
  virtual void changeEvent(QEvent* event);
  /** @reimplemented. */
  virtual void doLayout();
  virtual void focusInEvent(QFocusEvent *e);
  virtual void focusOutEvent(QFocusEvent *e);
  virtual void resetEditBox();
  /** Un double-clic permet en mode simulation de changer la valeur de la donnée visualisée */
  virtual void mouseDoubleClickEvent ( QMouseEvent * e );

protected:
  NumBase mNumBase;

private:
  void init(double value=0.0, long base=10);

  /** Vaut \a true s'il faut y afficher l'étiquette de la donnée. */
  bool mShowLabel;

  double mValue, mLower, mUpper, mStep;
  bool mRange;
  int mDec;
  bool mShowUnit;
  Format mFmt;
  int mBase;
};

#endif
