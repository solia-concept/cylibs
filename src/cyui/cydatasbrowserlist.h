/***************************************************************************
                          cydatasbrowserlist.h  -  description
                             -------------------
    begin                : lun nov 22 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYDATASBROWSERLIST_H
#define CYDATASBROWSERLIST_H

// QT
#include <QTreeWidget>
#include <QMouseEvent>
#include <QStringList>
#include <QList>
// CYLIB
#include "cydatasbrowser.h"

class CYCore;
class CYNetHost;
class CYNetLink;
class CYDB;
class CYData;

class CYDatasBrowserListItem : public QObject
{
  Q_OBJECT

public:
  /** Constructeur. */
  CYDatasBrowserListItem(QObject *parent, QTreeWidgetItem *item0, CYData *data);
  /** Destructeur. */
  ~CYDatasBrowserListItem();

public slots:
  void refresh();

private:
  CYData *mData;
  QTreeWidgetItem *mItem;
};

/** @short Liste de l'explorateur de données.
  * @author LE CLÉACH Gérald
  */

class CYDatasBrowserList : public QTreeWidget
{
  Q_OBJECT

public:
  /** Constructeur. */
  CYDatasBrowserList(QWidget *parent, const QString &name);
  /** Destructeur. */
  ~CYDatasBrowserList();
  /** Initialise l'arborescence par rapport à l'hôte \a host. */
  void init(CYNetHost *host);
  /** Initialise l'arborescence par rapport à la connexion réseau \a link.
    * @param only Affiche uniquement l'hôte \a host et la connexion réseau \a link si only vaut \a true. */
  void init(CYNetHost *host, CYNetLink *link, bool only=false);
  /** Initialise l'arborescence par rapport à la base de données \a db. */
  void init(CYNetHost *host, CYNetLink *link, CYDB *db);

public slots:
  /** Deconnexion de l'hôte sélectionné. */
  void disconnectHost();
  /** Rafraîchit l'arborescence par rapport à l'état du réseau. */
  void refresh();
  /** Nouvel élément sélectionné. */
  void newItemSelected(QTreeWidgetItem* item, QTreeWidgetItem *previous);
  /** Change le filtrage des données. */
  void changeFilter(int filter);
  /** Change la chaîne de recherche des données. */
  void changeSearch(QString InputData);
  /** Reconstruit l'arborescence. */
  void rebuild();

protected:
  virtual void mousePressEvent(QMouseEvent *event);
  /** Gestion de la souris lors d'un glisser-déposer. */
  virtual void mouseMoveEvent(QMouseEvent *ev);

protected: // Protected attributes
  /** Group permet de retrouver le (ou les) groupe(s)
    * au sein duquel la donnée doit apparaître dans l'explorateur de
    * données.
    * @short Groupe(s) d'appartenance d'une donnée.
    * @author Gérald LE CLEACH
    */
  class Group
  {
  public:
    /** Décode les différents morceaux de l'information en une liste de groupes. */
    Group(const QString &info, QChar separator)
    {
      QString s = info;
      while (s.length() > 0)
      {
        int sep;
        if ((sep = s.indexOf(separator)) < 0)
        {
          groups.append(new QString(s));
          break;
        }
        else
        {
          groups.append(new QString(s.left(sep)));
          s = s.remove(0, sep + 1);
        }
      }
    }

    ~Group() { }

    /** Retourne le groupe correspondant à l'index. */
    const QString &operator[](int idx)
    {
      if (idx < groups.count())
        return *(groups.at(idx));
      return NULL;
    }

    /** Nombre de morceaux de réponse.*/
    unsigned number() { return (groups.count()); }

  private:
    /** Liste de morceaux de réponse.*/
    QList<QString*> groups;
  } ;

private:
  /** Cette chaine de caractères stoque l'objet trainé. */
  QString dragText;
  QList<CYDatasBrowserListItem*> items;
  /** chaîne de recherche de données. */
  QString mSearchData;

protected: // Protected attributes
  /** Elément de la liste représentant l'hôte en cours. */
  QTreeWidgetItem *mHLVI;
  /** Elément de la liste représentant la connexion en cours. */
  QTreeWidgetItem *mLLVI;
  /** Filtrage actif sur les données. */
  CYDatasBrowser::Filter mFilter;
  /** Hôte unique affiché. */
  CYNetHost *mOnlyHost;
  /** Hôte unique affiché. */
  CYNetLink *mOnlyLink;
  QPoint dragStartPosition;
};

#endif
