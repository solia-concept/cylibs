/***************************************************************************
                          cytabwidget.cpp  -  description
                             -------------------
    begin                : dim fév 29 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cytabwidget.h>

// QT
#include <qtabbar.h>
// CYLIBS
#include "cymessagebox.h"

CYTabWidget::CYTabWidget(QWidget *parent, const QString &name)
  : QTabWidget(parent)
{
  setObjectName(name);
  mModified = false;
  mTreatChildDataWdg = true;
}

CYTabWidget::~CYTabWidget()
{
}

void CYTabWidget::setModified(bool state)
{
  mModified = state;
}

void CYTabWidget::setCurrentChanged(QWidget * widget)
{
  if (isModified())
  {
    QString msg = QString(tr("There are unsaved changes in the active view.\n"
                               "Do you want to apply or discard this changes?"));

    int res = CYMessageBox::warningYesNoCancel(this, msg, tr("Unsaved changes"), tr("&Apply"), tr("&Discard"));

    if (res == CYMessageBox::Yes)
      emit applying();
    if (res == CYMessageBox::No)
    {
      emit refreshing();
      emit validating();
    }
    else if (res == CYMessageBox::Cancel)
      return;
  }
  int index = indexOf(widget);
  emit currentPageIndexChanged(index);
}

//void CYTabWidget::showEvent(QShowEvent *e)
//{
//  QTabWidget::showEvent(e);
//  QString w;
//  if (!count())
//  {
//    w = QString("Tab %1").arg(count()+1);
//    addTab(new QWidget(this, w), w);
//    w = QString("Tab %1").arg(count()+1);
//    addTab(new QWidget(this, w), new QTab(w));
//  }
//}
//
QString CYTabWidget::pageTitle() const
{
  return QTabWidget::tabText(currentIndex());
}

void CYTabWidget::setPageTitle(const QString &title)
{
  QTabWidget::setTabText(currentIndex(), title);
}
