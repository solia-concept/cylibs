#include "cycarddo.h"
#include "cycarddoplugin.h"

#include <QtPlugin>

CYCardDOPlugin::CYCardDOPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void CYCardDOPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool CYCardDOPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *CYCardDOPlugin::createWidget(QWidget *parent)
{
    return new CYCardDO(parent);
}

QString CYCardDOPlugin::name() const
{
    return QLatin1String("CYCardDO");
}

QString CYCardDOPlugin::group() const
{
    return QLatin1String("");
}

QIcon CYCardDOPlugin::icon() const
{
    return QIcon( QPixmap( ":/pixmaps/cycarddo.png" ) );
}

QString CYCardDOPlugin::toolTip() const
{
    return QLatin1String("");
}

QString CYCardDOPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool CYCardDOPlugin::isContainer() const
{
    return false;
}

QString CYCardDOPlugin::domXml() const
{
    return QLatin1String("<widget class=\"CYCardDO\" name=\"cYCardDO\">\n</widget>\n");
}

QString CYCardDOPlugin::includeFile() const
{
    return QLatin1String("cycarddo.h");
}

