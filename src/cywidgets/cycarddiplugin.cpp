#include "cycarddi.h"
#include "cycarddiplugin.h"

#include <QtPlugin>

CYCardDIPlugin::CYCardDIPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void CYCardDIPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool CYCardDIPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *CYCardDIPlugin::createWidget(QWidget *parent)
{
    return new CYCardDI(parent);
}

QString CYCardDIPlugin::name() const
{
    return QLatin1String("CYCardDI");
}

QString CYCardDIPlugin::group() const
{
    return QLatin1String("");
}

QIcon CYCardDIPlugin::icon() const
{
  return QIcon( QPixmap( ":/pixmaps/cycarddi.png" ) );
}

QString CYCardDIPlugin::toolTip() const
{
    return QLatin1String("");
}

QString CYCardDIPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool CYCardDIPlugin::isContainer() const
{
    return false;
}

QString CYCardDIPlugin::domXml() const
{
    return QLatin1String("<widget class=\"CYCardDI\" name=\"cYCardDI\">\n</widget>\n");
}

QString CYCardDIPlugin::includeFile() const
{
    return QLatin1String("cycarddi.h");
}

