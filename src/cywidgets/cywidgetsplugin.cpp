#include <qglobal.h>
#include <qaction.h>
#include <QtPlugin>
#include <QDesignerFormEditorInterface>
#include <QDesignerFormWindowInterface>
#include <QDesignerFormWindowCursorInterface>
#include <QExtensionManager>
#include <QErrorMessage>

#include "cy_designer_plugin.h"

#include "cyled.h"
#include "cycarddi.h"
#include "cycarddo.h"

using namespace CYDesignerPlugin;

CustomWidgetInterface::CustomWidgetInterface( QObject *parent ):
    QObject( parent ),
    d_isInitialized( false )
{
}

bool CustomWidgetInterface::isContainer() const
{
    return false;
}

bool CustomWidgetInterface::isInitialized() const
{
    return d_isInitialized;
}

QIcon CustomWidgetInterface::icon() const
{
    return d_icon;
}

QString CustomWidgetInterface::codeTemplate() const
{
    return d_codeTemplate;
}

QString CustomWidgetInterface::domXml() const
{
    return d_domXml;
}

QString CustomWidgetInterface::group() const
{
    return "Cy Widgets";
}

QString CustomWidgetInterface::includeFile() const
{
    return d_include;
}

QString CustomWidgetInterface::name() const
{
    return d_name;
}

QString CustomWidgetInterface::toolTip() const
{
    return d_toolTip;
}

QString CustomWidgetInterface::whatsThis() const
{
    return d_whatsThis;
}

void CustomWidgetInterface::initialize(
    QDesignerFormEditorInterface *formEditor )
{
    if ( d_isInitialized )
        return;

    QExtensionManager *manager = formEditor->extensionManager();
    if ( manager )
    {
        manager->registerExtensions( new TaskMenuFactory( manager ),
            Q_TYPEID( QDesignerTaskMenuExtension ) );
    }

    d_isInitialized = true;
}


LedInterface::LedInterface( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYLed";
    d_include = "cyled.h";
    d_icon = QPixmap( ":/pixmaps/cyled.png" );
    d_domXml =
        "<widget class=\"CYLed" name=\"led">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>22</width>\n"
        "   <height>22</height>\n"
        "  </rect>\n"
        " </property>\n"
        " <property name=\"lineWidth\">\n"
        "  <number>4</number>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *LedInterface::createWidget( QWidget *parent )
{
    return new CYLed( parent );
}

CardDIPlugin::CardDIPlugin( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYCardDI";
    d_include = "cycardDI.h";
    d_icon = QPixmap( ":/pixmaps/cycardDI.png" );
    d_DImXml =
        "<widget class=\"CYCardDI\" name=\"cardDI\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>150</width>\n"
        "   <height>150</height>\n"
        "  </rect>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *CardDIInterface::createWidget( QWidget *parent )
{
    return new CYCardDI( parent );
}

CardDOPlugin::CardDOPlugin( QObject *parent ):
    CustomWidgetInterface( parent )
{
    d_name = "CYCardDO";
    d_include = "cycarddo.h";
    d_icon = QPixmap( ":/pixmaps/cycarddo.png" );
    d_domXml =
        "<widget class=\"CYCardDO\" name=\"cardDO\">\n"
        " <property name=\"geometry\">\n"
        "  <rect>\n"
        "   <x>0</x>\n"
        "   <y>0</y>\n"
        "   <width>150</width>\n"
        "   <height>150</height>\n"
        "  </rect>\n"
        " </property>\n"
        "</widget>\n";
}

QWidget *CardDOInterface::createWidget( QWidget *parent )
{
    return new CYCardDO( parent );
}

CustomWidgetCollectionInterface::CustomWidgetCollectionInterface(
        QObject *parent ):
    QObject( parent )
{
    d_plugins.append(new LedPlugin(this));
    d_plugins.append(new CardDIPlugin(this));
    d_plugins.append(new CardDOPlugin(this));
}

QList<QDesignerCustomWidgetInterface*>
CustomWidgetCollectionInterface::customWidgets( void ) const
{
    return d_plugins;
}

TaskMenuFactory::TaskMenuFactory( QExtensionManager *parent ):
    QExtensionFactory( parent )
{
}

QObject *TaskMenuFactory::createExtension(
    QObject *object, const QString &iid, QObject *parent ) const
{
    if ( iid == Q_TYPEID( QDesignerTaskMenuExtension ) )
    {
//#ifndef NO_QWT_PLOT
//        if ( CyPlot *plot = qobject_cast<CyPlot*>( object ) )
//            return new TaskMenuExtension( plot, parent );
//#endif
//#ifndef NO_QWT_WIDGETS
//        if ( CyDial *dial = qobject_cast<CyDial*>( object ) )
//            return new TaskMenuExtension( dial, parent );
//#endif
    }

    return QExtensionFactory::createExtension( object, iid, parent );
}


TaskMenuExtension::TaskMenuExtension( QWidget *widget, QObject *parent ):
    QObject( parent ),
    d_widget( widget )
{
    d_editAction = new QAction( tr( "Edit Cy Attributes ..." ), this );
    connect( d_editAction, SIGNAL( triggered() ),
        this, SLOT( editProperties() ) );
}

QList<QAction *> TaskMenuExtension::taskActions() const
{
    QList<QAction *> list;
    list.append( d_editAction );
    return list;
}

QAction *TaskMenuExtension::preferredEditAction() const
{
    return d_editAction;
}

void TaskMenuExtension::editProperties()
{
    const QVariant v = d_widget->property( "propertiesDocument" );
    if ( v.type() != QVariant::String )
        return;

//#ifndef NO_QWT_PLOT
//    QString properties = v.toString();

//    if ( qobject_cast<CyPlot*>( d_widget ) )
//    {
//        PlotDialog dialog( properties );
//        connect( &dialog, SIGNAL( edited( const QString& ) ),
//            SLOT( applyProperties( const QString & ) ) );
//        ( void )dialog.exec();
//        return;
//    }
//#endif

    static QErrorMessage *errorMessage = NULL;
    if ( errorMessage == NULL )
        errorMessage = new QErrorMessage();
    errorMessage->showMessage( "Not implemented yet." );
}

void TaskMenuExtension::applyProperties( const QString &properties )
{
    QDesignerFormWindowInterface *formWindow =
        QDesignerFormWindowInterface::findFormWindow( d_widget );
    if ( formWindow && formWindow->cursor() )
        formWindow->cursor()->setProperty( "propertiesDocument", properties );
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2( CYDesignerPlugin, CustomWidgetCollectionInterface )
#endif
