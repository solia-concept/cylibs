CONFIG      += plugin debug_and_release
TARGET      = $$qtLibraryTarget(cywidgetsplugin)
TEMPLATE    = lib

HEADERS     = \
    cywidgets/cywidgetsplugin.h \
    cywidgets/cycarddoplugin.h \
    cywidgets/cycarddiplugin.h \
    cywidgets/cyledplugin.h
SOURCES     = \
    cywidgets/cycarddoplugin.cpp \
    cywidgets/cywidgetsplugin.cpp \
    cywidgets/cyledplugin.cpp \
    cywidgets/cycarddiplugin.cpp

RESOURCES   = cywidgetsplugin.qrc

LIBS        += -L. 

CONFIG += designer

#greaterThan(QT_MAJOR_VERSION, 4) {
#    QT += designer
#} else {
#    CONFIG += designer
#}

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS    += target

#include(cyled.pri)
#include(cycarddi.pri)
#include(cycarddo.pri)

RESOURCES += \
    cywidgets/cywidgetsplugin.qrc
