#include "cyled.h"
#include "cyledplugin.h"

#include <QtPlugin>

CYLedPlugin::CYLedPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void CYLedPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool CYLedPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *CYLedPlugin::createWidget(QWidget *parent)
{
    return new CYLed(parent);
}

QString CYLedPlugin::name() const
{
    return QLatin1String("CYLed");
}

QString CYLedPlugin::group() const
{
    return QLatin1String("");
}

QIcon CYLedPlugin::icon() const
{
  return QIcon( QPixmap( ":/pixmaps/cyled.png" ) );
}

QString CYLedPlugin::toolTip() const
{
    return QLatin1String("");
}

QString CYLedPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool CYLedPlugin::isContainer() const
{
    return false;
}

QString CYLedPlugin::domXml() const
{
    return QLatin1String("<widget class=\"CYLed\" name=\"cYLed\">\n</widget>\n");
}

QString CYLedPlugin::includeFile() const
{
    return QLatin1String("cyled.h");
}

