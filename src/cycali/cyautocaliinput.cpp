/***************************************************************************
                          cyautocaliinput.cpp  -  description
                             -------------------
    début                  : ven jun 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyautocaliinput.h"
#include "ui_cyautocaliinput.h"

// CYLIBS
#include "cyana.h"
#include "cyadc16.h"
#include "cyadc32.h"
#include "cynumdisplay.h"
#include "cynuminput.h"

CYAutoCaliInput::CYAutoCaliInput(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYAutoCaliInput)
{
  mAna = nullptr;
  mMeasure = nullptr;
  ui->setupUi(this);
}


CYAutoCaliInput::~CYAutoCaliInput()
{
  delete ui;
}

void CYAutoCaliInput::init(CYMeasure *d, bool top)
{
  mMeasure = d;
  mTop = top;
  if (top)
    ui->calval->setDataName(mMeasure->top()->dataName());
  else
    ui->calval->setDataName(mMeasure->low()->dataName());

  if (mMeasure->inherits("CYAna"))
  {
    mAna = qobject_cast<CYAna*>(mMeasure);
    ui->directval->setDataName(mAna->adc16() ? mAna->adc16()->dataName() : mAna->adc32()->dataName());
  }
  else if (mMeasure && mMeasure->direct())
  {
    if (top)
      ui->directval->setDataName(mMeasure->direct()->dataName());
    else
      ui->directval->setDataName(mMeasure->direct()->dataName());
  }

  mTimer->start(250); //  ms
}

void CYAutoCaliInput::accept()
{
  if (mAna)
  {
    if (mAna->adc16())
    {
      if (mTop)
        mAna->adc16()->top()->setVal(mAna->adc16()->val());
      else
        mAna->adc16()->low()->setVal(mAna->adc16()->val());
    }
    else if (mAna->adc32())
    {
      if (mTop)
        mAna->adc32()->top()->setVal(mAna->adc32()->val());
      else
        mAna->adc32()->low()->setVal(mAna->adc32()->val());
    }
  }
  if (mMeasure && mMeasure->direct())
  {
    if (mTop)
      mMeasure->direct()->top()->setVal(mMeasure->direct()->val());
    else
      mMeasure->direct()->low()->setVal(mMeasure->direct()->val());
  }

  CYDialog::accept();
}

void CYAutoCaliInput::setTitle(const QString &title)
{
  ui->GroupBox1->setTitle(title);
}
