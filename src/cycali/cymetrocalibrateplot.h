//
// C++ Interface: cymetrocalibrateplot
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMETROCALIBRATEPLOT_H
#define CYMETROCALIBRATEPLOT_H

// QT
#include <QPainter>
// QWT
#include "qwt_plot.h"
#include "qwt_plot_renderer.h"
// CYLIBS
#include "cyplot.h"

/**
  @short @short Histogramme écart des points d'étalonnage.
  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMetroCalibratePlot : public CYPlot
{
  Q_OBJECT
public:
  CYMetroCalibratePlot(QWidget *parent = 0, const QString &name = 0);

  ~CYMetroCalibratePlot();

public slots:
  void printCurves(QPainter *p,QRectF *rPlot);
};

#endif
