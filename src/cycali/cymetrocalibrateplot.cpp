//
// C++ Implementation: cymetrocalibrateplot
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QWT
#include "qwt_plot_dict.h"
#include "qwt_plot_canvas.h"
#include "qwt_plot_layout.h"
#include "qwt_plot_curve.h"
#include "qwt_plot_marker.h"
#include "qwt_scale_draw.h"
#include "qwt_symbol.h"
#include "qwt_plot_grid.h"
#include "qwt_scale_draw.h"
#include "qwt_text_label.h"
// CYLIBS
#include "cymetrocalibrateplot.h"
#include "cyplotcurve.h"

CYMetroCalibratePlot::CYMetroCalibratePlot(QWidget *parent, const QString &name)
 : CYPlot(parent)
{
  setObjectName(name);
}


CYMetroCalibratePlot::~CYMetroCalibratePlot()
{
}

void CYMetroCalibratePlot::printCurves(QPainter *p, QRectF *rPlot)
{
  int w;

  setAutoReplot(false);

  QwtScaleDraw sd[axisCnt];
  QwtScaleMap map[axisCnt];

  QwtPlotRenderer renderer;
  renderer.setDiscardFlag(QwtPlotRenderer::DiscardBackground, true);
  renderer.setDiscardFlag(QwtPlotRenderer::DiscardCanvasBackground, true);
  renderer.setDiscardFlag(QwtPlotRenderer::DiscardLegend, true);
  renderer.render(this, p, *rPlot);

  // configuration initiale des paramètres géométriques des différents axes
  sd[yLeft].setAlignment(QwtScaleDraw::LeftScale);
  sd[yLeft].move(rPlot->left(), rPlot->top());
  sd[yLeft].setLength(rPlot->height());

  sd[yRight].setAlignment(QwtScaleDraw::RightScale);
  sd[yRight].move(rPlot->left(), rPlot->top());
  sd[yRight].setLength(rPlot->height());

  sd[yLeft].setAlignment(QwtScaleDraw::LeftScale);
  sd[yLeft].move(rPlot->left(), rPlot->top());
  sd[yLeft].setLength(rPlot->height());

  sd[xTop].setAlignment(QwtScaleDraw::TopScale);
  sd[xTop].move(rPlot->left(), rPlot->top());
  sd[xTop].setLength(rPlot->height());

  sd[xBottom].setAlignment(QwtScaleDraw::BottomScale);
  sd[xBottom].move(rPlot->left(), rPlot->top());
  sd[xBottom].setLength(rPlot->height());

  // format des titres des axes
  for (int ax = 0; ax < axisCnt; ax++)
  {
    sd[ax].setScaleDiv(axisScaleDiv(ax));
  }

  p->setFont(titleLabel()->text().font());
  p->setPen(Qt::black);


  // impression du titre
  if (!title().isEmpty())
  {
    QRectF rect;
    p->drawText(*rPlot, Qt::AlignTop | Qt::AlignHCenter | Qt::TextWordWrap, title().text(), &rect);
    // augmente la hauteur du rectangle contenant le scope
    rPlot->setTop(rPlot->top() + rect.height()+10);
  }

  int wLeft, wRight;
  // calcul la place utile aux axes
  if (axisEnabled(yLeft))
  {
    p->setFont(axisTitle(yLeft).font());
    wLeft = p->fontMetrics().height() * 2;
    p->setFont(axisFont(yLeft));
    wLeft += sd[yLeft].maxLabelWidth(p->font());
    rPlot->setLeft(rPlot->left() + wLeft);
  }
  if (axisEnabled(yRight))
  {
    p->setFont(axisTitle(yRight).font());
    wRight = p->fontMetrics().height() * 2;
    p->setFont(axisTitle(yRight).font());
    wRight += sd[yRight].maxLabelWidth(p->font());
    rPlot->setRight(rPlot->right() - wRight);
  }
  if (axisEnabled(xTop))
  {
    p->setFont(axisTitle(xTop).font());
    w = p->fontMetrics().height() * 2;
    p->setFont(axisFont(xTop));
    w += sd[xTop].maxLabelHeight(p->font());
    rPlot->setTop(rPlot->top() + w);
  }
  if (axisEnabled(xBottom))
  {
    p->setFont(axisTitle(xBottom).font());
    w = p->fontMetrics().height() * 2;
    p->setFont(axisFont(xBottom));
    w += sd[xBottom].maxLabelHeight(p->font());
    rPlot->setBottom(rPlot->bottom() - w);
  }

  // construction des plans
  for (int i=0 ; i < axisCnt ; i++)
  {
    // QT5 map[i].setDblRange(axisScale(i)->lBound(), axisScale(i)->hBound(), axisScale(i)->logScale());
    // setAxisOptions(i, QwtAutoScale::Logarithmic);
    map[i].setScaleInterval(axisScaleDiv(i).lowerBound(), axisScaleDiv(i).upperBound());
  }
  map[yLeft].setScaleInterval(rPlot->bottom(), rPlot->top());
  map[yRight].setScaleInterval(rPlot->bottom(), rPlot->top());
  map[xTop].setScaleInterval(rPlot->left(), rPlot->right());
  map[xBottom].setScaleInterval(rPlot->left(), rPlot->right());

  p->setPen(Qt::black);
  p->setBrush(Qt::NoBrush);

  // dessine le contour
  p->drawRect(rPlot->x(), rPlot->y(), rPlot->width() - 1, rPlot->height() - 1);
}
