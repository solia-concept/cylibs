//
// C++ Implementation: cymetrosensorsheetview
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

// QT
#include <QHBoxLayout>
// CYLIBS
#include "cymetrosensorsheetview.h"
#include "cybuttongroup.h"
#include "cytextlabel.h"
#include "cytextinput.h"
#include "cynuminput.h"
#include "cycombobox.h"
#include "cyflaginput.h"
#include "cymeasure.h"
#include "cycore.h"
#include "cys8.h"

CYMetroSensorSheetView::CYMetroSensorSheetView(QWidget *parent, const QString &name)
 : CYWidget(parent, name)
{
  if ( name.isEmpty() )
    setObjectName( "CYMetroSensorSheetView" );
  QGridLayout *CYMetroSensorSheetViewLayout = new QGridLayout( this );

  group = new CYButtonGroup( this, "group" );
  group->setTitle( tr( "Sensor sheet" ) );
  groupLayout = new QGridLayout( group );


  int row=0;
 //--------------------------------------------------
  QSpacerItem *spacer1 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed );
  groupLayout->addItem( spacer1, row, 0 );

 //--------------------------------------------------
  row++;
  calAutoInput = new CYFlagInput( group, "calAutoInput" );
  calAutoInput->setDataName( "SENSOR_CAL_AUTO_#" );
  groupLayout->addWidget( calAutoInput, row, 0, 1, 3 );

 //--------------------------------------------------
  row++;
  QSpacerItem *spacer2 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Fixed );
  groupLayout->addItem( spacer2, row, 1 );

 //--------------------------------------------------
  row++;
  typeLabel = new CYTextLabel( group, "typeLabel" );
  typeLabel->setText( tr( "Type" ) );

  typeLabel->setShowLabel( true );
  groupLayout->addWidget( typeLabel, row, 0 );

  type = new CYTextLabel( group, "type" );
  QFont type_font(  type->font() );
  type_font.setItalic( true );
  type->setFont( type_font );
  groupLayout->addWidget( type, row, 1, 1, 1 );

 //--------------------------------------------------
  row++;
  maxiLabel = new CYTextLabel( group, "maxiLabel" );
  maxiLabel->setText( tr( "High point" ) );
  maxiLabel->setShowLabel( true );
  groupLayout->addWidget( maxiLabel, row, 0 );

  maxi = new CYNumInput( group, "maxi" );
  maxi->setDataName( "EMTOP_#" );
  groupLayout->addWidget( maxi, row, 1, 1, 1 );

 //--------------------------------------------------
  row++;
  miniLabel = new CYTextLabel( group, "miniLabel" );
  miniLabel->setText( tr( "Low point" ) );
  miniLabel->setShowLabel( true );
  groupLayout->addWidget( miniLabel, row, 0 );

  mini = new CYNumInput( group, "mini" );
  mini->setDataName( "EMLOW_#" );
  groupLayout->addWidget( mini, row, 1, 1, 1 );

 //--------------------------------------------------
  row++;
  designerLabel = new CYTextLabel( group, "designerLabel" );
  designerLabel->setDataName( "DESIGNER_#" );
  designerLabel->setShowLabel( true );
  groupLayout->addWidget( designerLabel, row, 0 );

  designerInput = new CYTextInput( group, "designerInput" );
  designerInput->setDataName( "DESIGNER_#" );
  groupLayout->addWidget( designerInput, row, 1, 1, 1 );

 //--------------------------------------------------
  row++;
  pnLabel = new CYTextLabel( group, "pnLabel" );
  pnLabel->setDataName( "PN_#" );
  pnLabel->setShowLabel( true );
  groupLayout->addWidget( pnLabel, row, 0 );

  pnInput = new CYTextInput( group, "pnInput" );
  pnInput->setDataName( "PN_#" );
  groupLayout->addWidget( pnInput, row, 1, 1, 1 );

 //--------------------------------------------------
  row++;
  snLabel = new CYTextLabel( group, "snLabel" );
  snLabel->setDataName( "SN_#" );
  snLabel->setShowLabel( true );
  groupLayout->addWidget( snLabel, row, 0 );

  snInput = new CYTextInput( group, "snInput" );
  snInput->setDataName( "SN_#" );
  groupLayout->addWidget( snInput, row, 1, 1, 1 );

 //--------------------------------------------------
  row++;
  QHBoxLayout *uncertainLayout = new QHBoxLayout();

  uncertainType = new CYComboBox( group, "uncertainType" );
  uncertainType->setSizePolicy( QSizePolicy( (QSizePolicy::Policy)7, (QSizePolicy::Policy)0) );
  uncertainType->sizePolicy().setHeightForWidth(uncertainType->sizePolicy().hasHeightForWidth());
  uncertainType->setDataName( "SENSOR_UT_#" );
  uncertainLayout->addWidget( uncertainType );

  uncertain = new CYNumInput( group, "uncertain" );
  uncertain->setSizePolicy( QSizePolicy( (QSizePolicy::Policy)1, (QSizePolicy::Policy)0) );
  uncertain->sizePolicy().setHeightForWidth(uncertain->sizePolicy().hasHeightForWidth());
  uncertainLayout->addWidget( uncertain );

  groupLayout->addLayout( uncertainLayout, row, 0, 1, 2 );

 //--------------------------------------------------
  row++;
  envLabel = new CYTextLabel( group, "envLabel" );
  envLabel->setDataName( "SENSOR_ENV_#" );
  envLabel->setShowLabel( true );
  groupLayout->addWidget( envLabel, row, 0 );

  envInput = new CYTextInput( group, "envInput" );
  envInput->setDataName( "SENSOR_ENV_#" );
  groupLayout->addWidget( envInput, row, 1, 1, 1 );

 //--------------------------------------------------
  row++;
  periodLabel = new CYTextLabel( group, "periodLabel" );
  periodLabel->setDataName( "SENSOR_PERIOD_#" );
  periodLabel->setShowLabel( true );
  groupLayout->addWidget( periodLabel, row, 0 );

  periodicity = new CYNumInput( group, "periodicity" );
  periodicity->setDataName( "SENSOR_PERIOD_#" );
  groupLayout->addWidget( periodicity, row, 1, 1, 1 );

 //--------------------------------------------------
  row++;
  QHBoxLayout *dateLayout = new QHBoxLayout();

  dateCtrlLabel = new CYTextLabel( group, "dateCtrlLabel" );
  //dateCtrlLabel->setSizePolicy( QSizePolicy( (QSizePolicy::Policy)5, (QSizePolicy::Policy)5 ) );
  dateCtrlLabel->sizePolicy().setHorizontalStretch(3);
  //dateCtrlLabel->sizePolicy().setHeightForWidth(dateCtrlLabel->sizePolicy().hasHeightForWidth());
  dateCtrlLabel->setDataName( "DATE_CTRL_#" );
  dateCtrlLabel->setShowLabel( true );
  dateLayout->addWidget( dateCtrlLabel );

  dateCtrlInput = new CYFlagInput( group, "dateCtrlInput" );
  dateCtrlInput->setDataName( "DATE_CTRL_#" );
 // dateCtrlInput->setSizePolicy( QSizePolicy( (QSizePolicy::Policy)1, (QSizePolicy::Policy)0) );
 // dateCtrlInput->sizePolicy().setHeightForWidth(dateCtrlInput->sizePolicy().hasHeightForWidth());
  dateLayout->addWidget( dateCtrlInput );

  groupLayout->addLayout( dateLayout, row, 0, 1, 2 );

 //--------------------------------------------------
  row++;
  QBoxLayout * l = new QHBoxLayout( group );
  noteLabel = new CYTextLabel( group, "noteLabel" );
  noteLabel->setText(tr("Note"));
  l->addWidget( noteLabel );

  noteInput = new CYTextInput( group, "noteInput" );
  noteInput->setDataName( "NOTING_#" );
  noteInput->sizePolicy().setHorizontalStretch(1);
  l->addWidget( noteInput );

  groupLayout->addLayout( l, row, 0, 1, 2 );

 //--------------------------------------------------
  CYMetroSensorSheetViewLayout->addWidget( group, 0, 0 );
  resize( QSize(307, 459).expandedTo(minimumSizeHint()) );

  nbRow=row+1;
  // signals and slots connections
  connect( uncertainType, SIGNAL( activated(int) ), uncertain, SLOT( setCurrentDataName(int) ) );
}


CYMetroSensorSheetView::~CYMetroSensorSheetView()
{
}

/*!
    \fn CYMetroSensorSheetView::setGenericMark(const QByteArray txt)
 */
void CYMetroSensorSheetView::setGenericMark(const QByteArray txt)
{
  CYWidget::setGenericMark(txt);
  CYMeasure *mes = (CYMeasure *)core->findData(QString(txt));
  type->setText(mes->channelTypeDesc());
  uncertain->addDataName(QString("SENSOR_UEM_%1").arg(genericMark().data()).toUtf8(), 0);
  uncertain->addDataName(QString("SENSOR_UUC_%1").arg(genericMark().data()).toUtf8(), 1);
  uncertain->addDataName(QString("SENSOR_UPC_%1").arg(genericMark().data()).toUtf8(), 2);
  CYS8 *utype = (CYS8 *)core->findData(QString("SENSOR_UT_%1").arg(genericMark().data()));
  uncertain->setCurrentDataName(utype->val());

  int row = nbRow;
  while (!extraWdg.isEmpty())
      delete extraWdg.takeFirst();

  QHashIterator<int, CYData*> it( mes->sensorSheetExtra );
  it.toFront();
  while (it.hasNext())
  {
    it.next();
    CYData *data=it.value();
    CYTextLabel *extraLabel = new CYTextLabel( group, "designerLabel" );
    extraLabel->setDataName( data->dataName() );
    extraLabel->setShowLabel( true );
    extraWdg.append(extraLabel);
    groupLayout->addWidget( extraLabel, row, 0 );
    extraLabel->show();

    switch (data->type())
    {
      case Cy::VFL    :
      case Cy::VS8    :
      case Cy::VS16   :
      case Cy::VS32   :
      case Cy::VS64   :
      case Cy::VU8    :
      case Cy::VU16   :
      case Cy::VU32   :
      case Cy::VU64   :
      case Cy::Word   :
      case Cy::Bool   :
                      {
                        if (data->choiceDict.count()>0)
                        {
                          CYComboBox *extraInput = new CYComboBox( group, "extraInput" );
                          extraInput->setDataName( data->dataName() );
                          extraWdg.append(extraInput);
                          groupLayout->addWidget( extraInput, row, 1, 1, 1 );
                          extraInput->show();
                        }
                        else if (data->isFlag())
                        {
                          CYFlagInput *extraInput = new CYFlagInput( group, "extraInput" );
                          extraInput->setDataName( data->dataName() );
                          extraWdg.append(extraInput);
                          groupLayout->addWidget( extraInput, row, 1, 1, 1 );
                          extraInput->show();
                          break;
                        }
                        else
                        {
                          CYNumInput *extraInput = new CYNumInput( group, "extraInput" );
                          extraInput->setDataName( data->dataName() );
                          extraWdg.append(extraInput);
                          groupLayout->addWidget( extraInput, row, 1, 1, 1 );
                          extraInput->show();
                        }
                        break;
                      }

      case Cy::VF32   :
      case Cy::VF64   :
      case Cy::Time   :
      case Cy::Sec    :
                      {
                        CYNumInput *extraInput = new CYNumInput( group, "extraInput" );
                        extraInput->setDataName( data->dataName() );
                        extraWdg.append(extraInput);
                        groupLayout->addWidget( extraInput, row, 1, 1, 1 );
                        extraInput->show();
                        break;
                      }
      case Cy::String :
                      {
                        CYTextInput *extraInput = new CYTextInput( group, "extraInput" );
                        extraInput->setDataName( data->dataName() );
                        extraWdg.append(extraInput);
                        groupLayout->addWidget( extraInput, row, 1, 1, 1 );
                        extraInput->show();
                        break;
                      }
      default         : CYWARNINGTEXT(QString("CYMetroSensorSheetView::setGenericMark(const QString txt): "
                                              "type de la donnée %1 incorrecte").arg(data->objectName()));
    }
    row++;
  }
}
