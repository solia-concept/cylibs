#ifndef CYMETROSETTINGS_H
#define CYMETROSETTINGS_H

// CYLIBS
#include "cysettingsdialog.h"

namespace Ui {
    class CYMetroSettings;
}

class CYMetroSettings : public CYSettingsDialog
{
public:
  CYMetroSettings(QWidget *parent=0, const QString &name=0);
  ~CYMetroSettings();

  void hideListView();

private:
    Ui::CYMetroSettings *ui;
};

#endif // CYMETROSETTINGS_H
