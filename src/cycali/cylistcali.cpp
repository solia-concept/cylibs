/***************************************************************************
                          cylistcali.cpp  -  description
                             -------------------
    début                  : jeu avr 3 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cylistcali.h"

// QT
#include <QPushButton>
#include <QLayout>
#include <QHeaderView>

CYListCali::CYListCali(QWidget *parent, const QString &name, Qt::WindowFlags f)
  : QDialog(parent, f)
{
  if (name.isEmpty())
    setObjectName("CYListCali");

  setLayout(new QVBoxLayout);

  setWindowTitle(tr("Inputs / Outputs Calibration"));

  list = new QTreeWidget(this);
  list->setSortingEnabled(true);
  QStringList labels;
  labels<<tr("Type");
  labels<<tr("Description");
  labels<<tr("Name");
  labels<<tr("Ana.");
  list->setHeaderLabels(labels);
  list->setAllColumnsShowFocus(true);
  list->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
  layout()->addWidget(list);

  connect(list, &QTreeWidget::itemClicked, this, &CYListCali::calibrate);

  QHBoxLayout *layoutClose = new QHBoxLayout();
  QSpacerItem* spacer = new QSpacerItem( 0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum );
  layoutClose->addItem( spacer );

  QPushButton *pbClose = new QPushButton(tr("&Close"), this);
  layoutClose->addWidget(pbClose);
  layout()->addItem(layoutClose);

  connect(pbClose, SIGNAL(clicked()), SLOT(accept()));
}

CYListCali::~CYListCali()
{
}

