//
// C++ Interface: cymetrosensorsheetview
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMETROSENSORSHEETVIEW_H
#define CYMETROSENSORSHEETVIEW_H

// QT
#include <QLayout>
#include <QGridLayout>
#include <QString>
#include <QList>
// CYLIBS
#include <cywidget.h>

class CYButtonGroup;
class CYFlagInput;
class CYTextInput;
class CYTextLabel;
class CYNumInput;
class CYComboBox;
class CYDataWdg;

/**
	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMetroSensorSheetView : public CYWidget
{
Q_OBJECT
public:
    CYMetroSensorSheetView(QWidget *parent = 0, const QString &name = 0);

    ~CYMetroSensorSheetView();
    virtual void setGenericMark(const QByteArray txt);

    int nbRow;

    QGridLayout *groupLayout;
    CYButtonGroup *group;
  //--------------------------------------------------
    CYFlagInput *calAutoInput;
  //--------------------------------------------------
    CYTextLabel *typeLabel;
    CYTextLabel *type;
  //--------------------------------------------------
    CYTextLabel *maxiLabel;
    CYNumInput  *maxi;
  //--------------------------------------------------
    CYTextLabel *miniLabel;
    CYNumInput  *mini;
  //--------------------------------------------------
    CYTextLabel *designerLabel;
    CYTextInput *designerInput;
  //--------------------------------------------------
    CYTextLabel *pnLabel;
    CYTextInput *pnInput;
  //--------------------------------------------------
    CYTextLabel *snLabel;
    CYTextInput *snInput;
  //--------------------------------------------------
    CYComboBox *uncertainType;
    CYNumInput *uncertain;
  //--------------------------------------------------
    CYTextLabel *envLabel;
    CYTextInput *envInput;
  //--------------------------------------------------
    CYTextLabel *periodLabel;
    CYNumInput *periodicity;
  //--------------------------------------------------
    CYTextLabel *dateCtrlLabel;
    CYFlagInput *dateCtrlInput;
  //--------------------------------------------------
    CYTextLabel *noteLabel;
    CYTextInput *noteInput;

    QList<CYDataWdg*> extraWdg;
};

#endif
