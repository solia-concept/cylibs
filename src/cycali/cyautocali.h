/***************************************************************************
                          cyautocali.h  -  description
                             -------------------
    début                  : ven jun 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYAUTOCALI_H
#define CYAUTOCALI_H

// CYLIBS
#include "cydialog.h"

class CYAna;
class CYMeasure;
class CYAutoCaliInput;

namespace Ui {
    class CYAutoCali;
}

/** @short Boîte de dialogue pour le calibrage automatique d'une E/S.
  * @author Gérald LE CLEACH
  */

class CYAutoCali : public CYDialog
{
  Q_OBJECT
public:
  CYAutoCali(QWidget *parent=0, const QString &name=0);
  ~CYAutoCali();

public slots: // Public slots
  /** Charge les valeurs des données. */
  virtual void refresh();
  /** Initialise la boîte de dialogue avec la donnée à calibrer. */
  void init(CYMeasure *d);
  /** Saisie la valeur haute. */
  void setHighData();
  /** Saisie la valeur basse. */
  void setLowValue();
  /** Saisie les valeurs hautes et basses avec les valeurs constructeur. */
  void setDesignerValue();
  /** Ouvre le manuel de Cylix de manière contextuelle */
  void help();

private: // Private attributes
  /** E/S analogique à calibrer. */
  CYAna *mAna;
  /** E/S numérique à calibrer. */
  CYMeasure *mMeasure;
  /** Boîte de saisie de la valeur haute/basse. */
  CYAutoCaliInput *mInput;

private:
    Ui::CYAutoCali *ui;
};

#endif
