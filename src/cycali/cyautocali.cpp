/***************************************************************************
                          cyautocali.cpp  -  description
                             -------------------
    début                  : ven jun 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyautocali.h"
#include "ui_cyautocali.h"

// QT
#include <QPushButton>
// CYLIBS
#include "cyana.h"
#include "cyadc16.h"
#include "cyadc32.h"
#include "cynumdisplay.h"
#include "cyautocaliinput.h"
#include "cystring.h"
#include "cytextinput.h"
#include "cybuttongroup.h"
#include "cycore.h"
#include "cymessagebox.h"

CYAutoCali::CYAutoCali(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYAutoCali)
{
  mAna = nullptr;
  mMeasure = nullptr;
  ui->setupUi(this);
}

CYAutoCali::~CYAutoCali()
{
  delete ui;
}

void CYAutoCali::init(CYMeasure *d)
{
  mMeasure = d;

  ui->hcalval->setDataName(mMeasure->top()->dataName());
  ui->lcalval->setDataName(mMeasure->low()->dataName());
  ui->calval->setDataName(mMeasure->dataName());
  ui->dhcalval->setNbDec(mMeasure->top()->nbDec());
  ui->dlcalval->setNbDec(mMeasure->low()->nbDec());
  ui->dhcalval->setValue(mMeasure->top()->def());
  ui->dlcalval->setValue(mMeasure->low()->def());
  ui->dhcalval->setSuffix(QString(" %1").arg(mMeasure->unit()));
  ui->dlcalval->setSuffix(QString(" %1").arg(mMeasure->unit()));
  ui->noting->setDataName(mMeasure->calibrateNoting->dataName());

  if (mMeasure->inherits("CYAna"))
  {
    mAna = qobject_cast<CYAna*>(mMeasure);

    if (mAna->adc16())
    {
      ui->hdirectval->setDataName(mAna->adc16()->top()->dataName());
      ui->ldirectval->setDataName(mAna->adc16()->low()->dataName());
      ui->directval->setDataName(mAna->adc16()->dataName());
      ui->dhdirectval->setValue(mAna->adc16()->top()->def());
      ui->dldirectval->setValue(mAna->adc16()->low()->def());
      ui->dhdirectval->setSuffix(QString(" %1").arg(mAna->adc16()->unit()));
      ui->dldirectval->setSuffix(QString(" %1").arg(mAna->adc16()->unit()));
    }
    else if (mAna->adc32())
    {
      ui->hdirectval->setDataName(mAna->adc32()->top()->dataName());
      ui->ldirectval->setDataName(mAna->adc32()->low()->dataName());
      ui->directval->setDataName(mAna->adc32()->dataName());
      ui->dhdirectval->setValue(mAna->adc32()->top()->def());
      ui->dldirectval->setValue(mAna->adc32()->low()->def());
      ui->dhdirectval->setSuffix(QString(" %1").arg(mAna->adc32()->unit()));
      ui->dldirectval->setSuffix(QString(" %1").arg(mAna->adc32()->unit()));
    }
    else
      CYWARNINGTEXT(QString("CYAutoCali::init(CYAna *d): %1").arg(objectName()));
  }
  else
  {
    ui->hdirectval->setDataName(mMeasure->direct()->top()->dataName());
    ui->ldirectval->setDataName(mMeasure->direct()->low()->dataName());
    ui->directval->setDataName(mMeasure->direct()->dataName());
    ui->dhdirectval->setValue(mMeasure->direct()->top()->def());
    ui->dldirectval->setValue(mMeasure->direct()->low()->def());
    ui->dhdirectval->setSuffix(QString(" %1").arg(mMeasure->direct()->unit()));
    ui->dldirectval->setSuffix(QString(" %1").arg(mMeasure->direct()->unit()));
  }

  setGenericMark(mMeasure->dataName());

  if (!mMeasure->calibrateOutdatedEvent())
    ui->dateGroup->hide();

  connect(this, SIGNAL(validate()), mMeasure, SLOT(setCalibrateChanged()));

  mTimer->start(250); // ms
}

void CYAutoCali::setHighData()
{
  mInput = new CYAutoCaliInput(this, "CYAutoCaliInput");
  Q_CHECK_PTR(mInput);
  mInput->setWindowTitle(QString(tr("High")+": %1").arg(windowTitle()));
  mInput->setTitle(tr("High Value"));
  mInput->init(mMeasure, true);
  mInput->exec();
  refresh();
}

void CYAutoCali::setLowValue()
{
  mInput = new CYAutoCaliInput(this, "CYAutoCaliInput");
  Q_CHECK_PTR(mInput);
  mInput->setWindowTitle(QString(tr("Low")+": %1").arg(windowTitle()));
  mInput->setTitle(tr("Low Value"));
  mInput->init(mMeasure, false);
  mInput->exec();
  refresh();
}

void CYAutoCali::setDesignerValue()
{
  if(CYMessageBox::warningYesNo(this, tr("Do you really want to load the designer's values ?")))
  {
    if (mAna)
    {
      // Charges les valeurs constructeurs
      mAna->top()->designer();
      mAna->low()->designer();
      if (mAna->adc16())
      {
        mAna->adc16()->top()->designer();
        mAna->adc16()->low()->designer();
      }
      else if (mAna->adc32())
      {
        mAna->adc32()->top()->designer();
        mAna->adc32()->low()->designer();
      }
      else
        CYWARNINGTEXT(QString("CYManuCali::setDesignerValue()): %1").arg(objectName()));
      refresh();
      mAna->sendCal();
    }
    else if (mMeasure)
    {
      // Charges les valeurs constructeurs
      mMeasure->top()->designer();
      mMeasure->low()->designer();
      mMeasure->direct()->top()->designer();
      mMeasure->direct()->low()->designer();
      refresh();
      mMeasure->sendCal();
    }
  }
}

void CYAutoCali::refresh()
{
  CYDialog::refresh() ;
  if (mAna)
  {
    int adc = mAna->adc16() ? mAna->adc16()->val() : mAna->adc32()->val();

    if (mAna->adc16() && ((adc > mAna->adc16()->low()->min()) && (adc < mAna->adc16()->top()->max())))
    {
      if (adc > mAna->adc16()->low()->max())
        ui->pbLow->setEnabled(false);
      else
        ui->pbLow->setEnabled(true);

      if (adc < mAna->adc16()->top()->min())
        ui->pbTop->setEnabled(false);
      else
        ui->pbTop->setEnabled(true);
    }
    else if (mAna->adc32() && ((adc > mAna->adc32()->low()->min()) && (adc < mAna->adc32()->top()->max())))
    {
      if (adc > mAna->adc32()->low()->max())
        ui->pbLow->setEnabled(false);
      else
        ui->pbLow->setEnabled(true);

      if (adc < mAna->adc32()->top()->min())
        ui->pbTop->setEnabled(false);
      else
        ui->pbTop->setEnabled(true);
    }
  }
  else if (mMeasure && mMeasure->direct())
  {
    CYMeasure *direct = mMeasure->direct();

    if ((direct->val() > direct->low()->min()) &&
        (direct->val() < direct->top()->max()))
    {
      if (direct->val() > direct->low()->max())
        ui->pbLow->setEnabled(false);
      else
        ui->pbLow->setEnabled(true);

      if (direct->val() < direct->top()->min())
        ui->pbTop->setEnabled(false);
      else
        ui->pbTop->setEnabled(true);
    }
  }
  else
  {
    ui->calval->setEnabled(false);
    ui->calval->putSpecialValueText();
    ui->directval->setEnabled(false);
    if (core->simulation())
    {
      ui->pbLow->setEnabled(true);
      ui->pbTop->setEnabled(true);
    }
    else
    {
      ui->pbLow->setEnabled(false);
      ui->pbTop->setEnabled(false);
    }
  }
}

void CYAutoCali::help()
{
  core->invokeHelp( "cydoc", "calibrage-auto" );
}
