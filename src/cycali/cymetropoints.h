//
// C++ Interface: cymetropoints
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMETROPOINTS_H
#define CYMETROPOINTS_H

// QT
#include <QList>
// CYLIBS
#include <cymetropoint.h>

/**
Liste des points d'étalonnage du module de métrologie.

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
template<typename CYMetroPoint> class CYMetroPoints : public QList<CYMetroPoint*>
{
public:
  CYMetroPoints(): QList<CYMetroPoint*>(){ direct=false; };

  ~CYMetroPoints() {};

public:
  virtual int compareItems (CYMetroPoint *item1, CYMetroPoint *item2)
  {
    CYMetroPoint *point1 = item1;
    CYMetroPoint *point2 = item2;

    if (direct)
    {
      if (point1->bdir > point2->bdir)
        return 1;
      if (point1->bdir < point2->bdir)
        return -1;
    }
    else
    {
      if (point1->bench > point2->bench)
        return 1;
      if (point1->bench < point2->bench)
        return -1;
    }
    return 0;
  }

  virtual void sortDirect()
  {
    direct=true;
    std::sort(QList<CYMetroPoint*>::begin(), QList<CYMetroPoint*>::end());
    direct=false;
  }

protected:
  bool direct;
};

#endif
