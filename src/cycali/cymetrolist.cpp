//
// C++ Implementation: cymetrolist
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// CYLIBS
#include "cymetrowin.h"
#include "cymetrolist.h"
#include "cymeasure.h"

CYMetroList::CYMetroList(CYMetroWin *win, QWidget *parent, const QString &name)
 : QTreeWidget(parent),
   mWin(win)
{
  setObjectName(name);
  connect(this, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(changeMeasure(QTreeWidgetItem*,QTreeWidgetItem*)));
}


CYMetroList::~CYMetroList()
{
}

void CYMetroList::changeMeasure(QTreeWidgetItem *item, QTreeWidgetItem *)
{
  if (mWin->editing() || !item)
    return;
  QTreeWidget::setCurrentItem(item);
  mWin->initWithMeasure(item->text(1));
}

void CYMetroList::selectMeasure( QString dataName )
{
  QTreeWidgetItem *item = findItems(dataName, Qt::MatchExactly, 1).first();
  item->setSelected(true);
}

void CYMetroList::setSelected(QTreeWidgetItem * item, bool selected )
{
  if (!item)
    return;

  if (!selected)
    return;

  if (!mWin->measure())
  {
    mWin->initWithMeasure(item->text(1));
    item->setSelected(true);
    // TOCHECK QT5
    //    QTreeWidget::setSelected(item, true);
//    QTreeWidget::setCurrentItem(item);
    return;
  }

  QTreeWidgetItem *current = findItems(mWin->measure()->objectName(), Qt::MatchExactly, 0).first();

  if (item==current)
    return;

  if (!mWin->isMultiple() && mWin->editing() && (!mWin->cancel()))
  {
    item->setSelected(false);
    current->setSelected(true);
  }
  else
  {
    mWin->memEditing();
    item->setSelected(false);
    mWin->initWithMeasure(item->text(1));
  }
}
