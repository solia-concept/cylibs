//
// C++ Implementation: cymetropoint
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cymetropoint.h"
#include "cymeasure.h"

CYMetroPoint::CYMetroPoint(CYMeasure *parent)
 : QObject(parent),
   mMeasure(parent)
{
  fs = qAbs(mMeasure->top()->val() - mMeasure->low()->val());
}


CYMetroPoint::~CYMetroPoint()
{
}

bool CYMetroPoint::operator==(const CYMetroPoint &other) const
{
  return (ref == other.ref) ? true : false;
}

bool CYMetroPoint::operator<(const CYMetroPoint &other) const
{
  return (ref < other.ref) ? true : false;
}

float CYMetroPoint::uncertaintyPC()
{
  return mMeasure->uncertainty(ref, false);
}

float CYMetroPoint::uncertainty()
{
  return mMeasure->uncertainty(ref);
}

float CYMetroPoint::uncertaintyMin()
{
  return mMeasure->uncertaintyMin(ref);
}

float CYMetroPoint::uncertaintyMax()
{
  return mMeasure->uncertaintyMax(ref);
}

float CYMetroPoint::error()
{
  return (float)(bench - ref);
}

float CYMetroPoint::error_fs()
{
  QString txt = QString::number(((100.0*error())/fs), 'f', 3);
  return txt.toFloat();
}

float CYMetroPoint::brut(float gain, float offset)
{
  float res;
  res = (bdir - offset) * gain;
  return res;
}

float CYMetroPoint::error_linearity(float theor)
{
  err_lin = (100.0*(theor-ref)/fs);
  QString txt = QString::number(err_lin, 'f', 3);
  return txt.toFloat();
}

bool CYMetroPoint::isAna()
{
  return mMeasure->inherits("CYAna");
}
