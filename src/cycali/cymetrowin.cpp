//
// C++ Implementation: cymetrowin
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <QLayout>
#include <QShowEvent>
#include <QFrame>
#include <QMenu>
// CYLIBS
#include "cymetrowin.h"
#include "cymetrolist.h"
#include "cymetrocalibrate.h"
#include "cymetrosensorsheet.h"
#include "cymetrosettings.h"
#include "cymeasure.h"
#include "cycore.h"
#include "cyactionmenu.h"

CYMetroWin::CYMetroWin(const QString &name, int index, const QString &label, bool multiple)
 : CYWin(0, name, index, label)
{
  mEditing = false;
  mDesigner = false;
  mMultiple = multiple;

//   setMaximumWidth(1280);
  setWindowTitle(tr("Metrology tool"));
  mSplitter = new QSplitter(this);
  Q_CHECK_PTR(mSplitter);
  mSplitter->setOrientation(Qt::Horizontal);
  mSplitter->setOpaqueResize(true);
  setCentralWidget(mSplitter);

  QFrame *frame = new QFrame(mSplitter);
  frame->setMaximumWidth(100);
  frame->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding));

  QVBoxLayout * vb = new QVBoxLayout( frame );

  mFilterBox = new QComboBox( frame );
  mFilterBox->addItem(tr("With alert"    ), Ctrl);
  mFilterBox->addItem(tr("Without alert" ), NotCtrl);
  mFilterBox->addItem(tr("All"           ), All);
  vb->addWidget( mFilterBox );
  connect ( mFilterBox, SIGNAL(activated(int)), this, SLOT(changeFilter(int)));

  mList = new CYMetroList(this, frame, "CYMetroList");
  Q_CHECK_PTR(mList);
  QStringList labels;
  labels<<tr("Sensors");
  labels<<tr("Name");
  if (mMultiple)
    labels<<"";
  mList->setHeaderLabels(labels);
  mList->hideColumn(1);

  mList->setSelectionMode(QAbstractItemView::SingleSelection);
  vb->addWidget( mList );

  mCalibrate = new CYMetroCalibrate(mSplitter, "CYMetroCalibrate", mMultiple);
  Q_CHECK_PTR(mCalibrate);
  if (!mMultiple)
    connect ( mCalibrate, SIGNAL(editing(bool)), mFilterBox, SLOT(setDisabled(bool)));
  else
    connect ( mCalibrate, SIGNAL(editing(CYMeasure *, bool)), SLOT(setEditing(CYMeasure *,bool)));

  new CYActionMenu( tr("&File"), 0, actionCollection(), "file");
  new CYActionMenu( tr("&Edit"), 0, actionCollection(), "edit");
  new CYActionMenu(tr("&Settings"), actionCollection(), "configure");

  actionQuit = new CYAction(tr("&Quit"          ), "exit"                     ,  Qt::CTRL+Qt::Key_Q, this      , SLOT(close())          , actionCollection(), "metro-quit");
  actionCap  = new CYAction(tr("Sensor sheet"   ), "cysensor_sheet"           ,           0, this      , SLOT(sensorSheet())    , actionCollection(), "metro-sensor");
  actionRaz  = new CYAction(tr("Clear table"    ), "cyclear_list"             ,           0, mCalibrate, SLOT(reset())          , actionCollection(), "metro-raz");
  actionAdd  = new CYAction(tr("Add point"      ), "cytable_insert_row_below" ,           0, mCalibrate, SLOT(addPoint())       , actionCollection(), "metro-add");
  actionDel  = new CYAction(tr("Delete point"   ), "cytable_delete_row"       ,           0, mCalibrate, SLOT(deletePoint())    , actionCollection(), "metro-del");
  actionCal  = new CYAction(tr("Calibrate"      ), "cycalibrate"              ,           0, mCalibrate, SLOT(calibrate())      , actionCollection(), "metro-cal");
  actionVer  = new CYAction(tr("Verification"   ), "cyverification"           ,           0, mCalibrate, SLOT(verification())   , actionCollection(), "metro-ver");
  actionDes  = new CYAction(tr("Designer values"), 0                          ,           0, mCalibrate, SLOT(setDesigner())    , actionCollection(), "metro-des");
  actionSet  = new CYAction(tr( "Metrology settings" ), "configure"           ,           0, this      , SLOT(settings())       , actionCollection(), "metro-set");

  setActionDisable("metro-raz", "CY_METRO_DISABLE_RAZ");
  setActionDisable("metro-add", "CY_METRO_DISABLE_ADD");
  setActionDisable("metro-del", "CY_METRO_DISABLE_DEL");
  setActionDisable("metro-cal", "CY_METRO_DISABLE_CAL");
  setActionDisable("metro-ver", "CY_METRO_DISABLE_VER");
  setActionDisable("metro-des", "CY_METRO_DISABLE_DES");

  readProperties(core->OSUserConfig());
  createGUI();
//  initHelpMenu();

  mCalibrate->hide();

  changeFilter(All);
  connect ( mFilterBox, SIGNAL(activated(int)), this, SLOT(changeFilter(int)));
}


CYMetroWin::~CYMetroWin()
{
}

void CYMetroWin::createGUI(const QString &xmlfile)
{
  CYWin::createGUI(xmlfile);
}

void CYMetroWin::showEvent(QShowEvent *e)
{
  CYWin::showEvent(e);
  if (!measure())
  {
    mList->setSelected(mList->topLevelItem(0), true);
  }
}

bool CYMetroWin::queryClose()
{
  if (mCalibrate->editing())
  {
    if (!mCalibrate->cancel())
      return false; // reste sur l'édition de ce capteur
  }
  QListIterator<CYMeasure *> it(core->metroList);
  while (it.hasNext())
  {
    CYMeasure *mes = it.next();
    if (mes->metroEditing())
    {
      // actualise la vue pour l'édition de ce capteur
      mList->selectMeasure(mes->objectName());
      if (!mCalibrate->cancel())
        return false; // reste sur l'édition de ce capteur
   }
  }
  return CYWin::queryClose();
}

bool CYMetroWin::editing()
{
  return mCalibrate->editing();
}

bool CYMetroWin::cancel()
{
  return mCalibrate->cancel();
}


CYMeasure *CYMetroWin::measure()
{
  CYMeasure *mes = (CYMeasure *)core->findData(mCalibrate->genericMark(), false, false);
  return mes;
}


void CYMetroWin::initWithMeasure(const QString &name)
{
  if (mCalibrate->isHidden())
    mCalibrate->show();
  mCalibrate->initWithMeasure(name);
}

void CYMetroWin::sensorSheet()
{
  CYMeasure *mes = measure();
  if (!mes)
    return;
  CYMetroSensorSheet *dlg = new CYMetroSensorSheet(this, mes->objectName());
  dlg->exec();
  initWithMeasure(mes->objectName());
}

void CYMetroWin::settings()
{
  CYMetroSettings *dlg = new CYMetroSettings(this);
  dlg->hideListView();
  connect(dlg, SIGNAL(validate()), mCalibrate, SLOT(updateList()));
  dlg->exec();
}

void CYMetroWin::changeFilter(int filter)
{
  mList->clear();
  QListIterator<CYMeasure *> it(core->metroList);
  while (it.hasNext())
  {
    CYMeasure *mes=it.next();
    QStringList strings;
    strings<<mes->phys()<<mes->objectName();
    switch (filter)
    {
      case Ctrl :
      {
        if (!mes->calibrateDateCtrl->val())
          continue;
      }
      break;
      case NotCtrl :
      {
        if (mes->calibrateDateCtrl->val())
          continue;
      }
      break;
    }
    QTreeWidgetItem *item = new QTreeWidgetItem(mList, QStringList() << strings << mes->objectName());

    if (mMultiple && mes->metroEditing())
    {
      item->setText(2, "*");
    }
  }
  if (!isHidden() && !measure())
  {
    mList->setSelected(mList->topLevelItem(0), true);
  }
}

void CYMetroWin::setEditing(CYMeasure *mes, bool editing)
{
  if (!mes)
  {
    CYWARNING;
    return;
  }

  if (!mMultiple)
    return;

  QTreeWidgetItem *item = mList->findItems(mes->objectName(), Qt::MatchExactly, 1).first();
  if (!item)
    return;

  mes->setMetroEditing(editing);
  if (editing)
    item->setText(2, "*");
  else
    item->setText(2, "");
}

/** Mémorise l'édition en multi-voies. */
void CYMetroWin::memEditing()
{
  mCalibrate->memEditing();
}
