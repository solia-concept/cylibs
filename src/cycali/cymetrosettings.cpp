#include "cymetrosettings.h"
#include "ui_cymetrosettings.h"

CYMetroSettings::CYMetroSettings(QWidget *parent, const QString &name)
  : CYSettingsDialog(parent,name), ui(new Ui::CYMetroSettings)
{
  ui->setupUi(this);
  connect(ui->listView, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(changePage(QTreeWidgetItem*,QTreeWidgetItem*)));
}

CYMetroSettings::~CYMetroSettings()
{
  delete ui;
}

void CYMetroSettings::hideListView()
{
  ui->listView->hide();
}
