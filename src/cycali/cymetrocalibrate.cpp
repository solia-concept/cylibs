//
// C++ Implementation: cymetrocalibrate
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

// ANSI
#include <unistd.h>
// QT
#include <QDir>
#include <QProcess>
#include <QHeaderView>
#include <QDesktopServices>
#include <QtPrintSupport/QPrinter>
// QWT
#include "qwt_plot_curve.h"
#include "qwt_plot_marker.h"
#include "qwt_abstract_legend.h"
#include "qwt_plot_item.h"
// CYLIBS
#include "cymetrocalibrate.h"
#include "ui_cymetrocalibrate.h"
#include "cymetrosensorsheetview.h"
#include "cymetrocaliinput.h"
#include "cymetrocalibrateplot.h"
#include "cymetropoint.h"
#include "cymeasure.h"
#include "cys8.h"
#include "cyu8.h"
#include "cys16.h"
#include "cytsec.h"
#include "cystring.h"
#include "cycore.h"
#include "cyuser.h"
#include "cydatetime.h"
#include "cycolor.h"
#include "cyadc16.h"
#include "cyadc32.h"
#include "cyapplication.h"
#include "cyplotcurve.h"
#include "cyplot.h"

CYMetroCalibrate::CYMetroCalibrate(QWidget *parent, const QString &name, bool multiple)
  : CYWidget(parent,name), ui(new Ui::CYMetroCalibrate)
{
  mMultiple=multiple;
  mMeasure = 0;

  ui->setupUi(this);
  setEditing(false);
  ui->table->setColumnCount(10);
  mDisableRaz = (CYFlag *)core->findData("CY_METRO_DISABLE_RAZ");
  mDisableAdd = (CYFlag *)core->findData("CY_METRO_DISABLE_ADD");
  mDisableDel = (CYFlag *)core->findData("CY_METRO_DISABLE_DEL");
  mDisableCal = (CYFlag *)core->findData("CY_METRO_DISABLE_CAL");
  mDisableVer = (CYFlag *)core->findData("CY_METRO_DISABLE_VER");
  mDisableDes = (CYFlag *)core->findData("CY_METRO_DISABLE_DES");
  mViewWhole  = (CYFlag *)core->findData("CY_METRO_VIEW_WHOLE");
  mDevCurveColor= (CYColor *)core->findData("CY_METRO_DEV_CURVE_COLOR");
  mDevCurveStyle= (CYS8    *)core->findData("CY_METRO_DEV_CURVE_STYLE");
  mLinCurveColor= (CYColor *)core->findData("CY_METRO_LIN_CURVE_COLOR");
  mLinCurveStyle= (CYS8    *)core->findData("CY_METRO_LIN_CURVE_STYLE");

  // add curves
  curveMax = new QwtPlotCurve(tr("Maxi uncertainty"));
  curveMin = new QwtPlotCurve(tr("Mini uncertainty"));
  curveLin = new QwtPlotCurve(tr("Linearity error (%)"));
  curveErr = new QwtPlotCurve(tr("Error (% FS)"));

  ui->plot->insertCurve(curveMax);
  ui->plot->insertCurve(curveMin);
  ui->plot->insertCurve(curveLin);
  ui->plot->insertCurve(curveErr);

  // Insertion d'un curseur
  cursorX = new QwtPlotMarker();
  cursorX->setLineStyle(QwtPlotMarker::VLine);
  cursorX->attach(ui->plot);

  cursorY1 = new QwtPlotMarker();
  cursorY1->setLineStyle(QwtPlotMarker::HLine);
  cursorY1->attach(ui->plot);

  cursorY2 = new QwtPlotMarker();
  cursorY2->setLineStyle(QwtPlotMarker::HLine);
  cursorY2->attach(ui->plot);
}

void CYMetroCalibrate::initWithMeasure(const QString &name)
{
  setGenericMark(name.toUtf8());
  ui->sensorSheet->setGenericMark(name.toUtf8());
  mMeasure = (CYMeasure *)core->findData(name);
  initWithMeasure();
}

void CYMetroCalibrate::initWithMeasure()
{
  if (!mMeasure)
    CYWARNING;

  if (!mMultiple || !mMeasure->metroEditing())
    setEditing(false);

  mMeasure->metroMoyMES->setFormat(mMeasure->format());
  mMeasure->metroMoyMES->setPrecision(0.0);
  mMeasure->metroMoyMES->setNbDec(mMeasure->format()->nbDec()+1);
  if (mMeasure->inherits("CYAna"))
  {
    if (mMeasure->adc16())
      mMeasure->metroMoyDIR->setFormat(mMeasure->adc16()->format());
    if (mMeasure->adc32())
      mMeasure->metroMoyDIR->setFormat(mMeasure->adc32()->format());
    mMeasure->metroMoyDIR->setNbDec(1);
  }
  else
  {
    mMeasure->metroMoyDIR->setFormat(mMeasure->format());
    mMeasure->metroMoyDIR->setPrecision(0.0);
    mMeasure->metroMoyDIR->setNbDec(mMeasure->format()->nbDec()+1);
  }
  mMeasure->calibrateOperator->setVal(core->user()->title());
  ui->current->setData(mMeasure);
  if (mMeasure->sensorCalAuto->val())
    ui->tmoy->setData(mMeasure->metroTMoy);
  else
    ui->tmoy->hide();

  ui->table->setHorizontalHeaderItem(colMin     , new QTableWidgetItem(tr("Mini\n(%1)").arg(mMeasure->unit())));
  ui->table->setHorizontalHeaderItem(colRef     , new QTableWidgetItem(tr("Standard\n(%1)").arg(mMeasure->unit())));
  ui->table->setHorizontalHeaderItem(colMax     , new QTableWidgetItem(tr("Maxi\n(%1)").arg(mMeasure->unit())));
  ui->table->setHorizontalHeaderItem(colBench   , new QTableWidgetItem(tr("Bench\n(%1)").arg(mMeasure->unit())));
  ui->table->setHorizontalHeaderItem(colEcart   , new QTableWidgetItem(tr("Error\n(%1)").arg(mMeasure->unit())));
  ui->table->setHorizontalHeaderItem(colEcartPE , new QTableWidgetItem(tr("Error\n(% FS)")));
  ui->table->setHorizontalHeaderItem(colDirect  , new QTableWidgetItem(tr("Bench\n(ADC)")));
  ui->table->setHorizontalHeaderItem(colTheor   , new QTableWidgetItem(tr("Straight\nLine\n (%1)").arg(mMeasure->unit())));
  ui->table->setHorizontalHeaderItem(colErrLin  , new QTableWidgetItem(tr("Linearity\nerror\n(% FS)")));
  ui->table->setHorizontalHeaderItem(colTMoy    , new QTableWidgetItem(tr("Averaging\ntime")+"\n"+tr("(sec)")));
  if (!mMeasure->inherits("CYAna"))
    ui->table->setHorizontalHeaderItem(colDirect, new QTableWidgetItem(tr("Gross\n(%1)").arg(mMeasure->unit())));

  viewTable(mViewWhole->val());

  bool editing;
  if (mMultiple && mMeasure->metroEditing())
    editing = true; // restauration des valeurs en cours d'édition
  else
    editing = false;

  nb_pts = editing ? mMeasure->metroNb->tmp() : mMeasure->metroNb->val();
  while (!points.isEmpty())
      delete points.takeFirst();

  for (int i=0; i<nb_pts; i++)
  {
    CYMetroPoint *point = new CYMetroPoint(mMeasure);

    CYF32 *bdir = (CYF32 *)core->findData(QString("METRO_BDIR%1_%2").arg(i+1).arg(mMeasure->objectName()));
    point->bdir = bdir->val();

    CYF32 *bench = (CYF32 *)core->findData(QString("METRO_BENCH%1_%2").arg(i+1).arg(mMeasure->objectName()));
    point->bench = bench->val();

    CYF32 *ref = (CYF32 *)core->findData(QString("METRO_REF%1_%2").arg(i+1).arg(mMeasure->objectName()));
    point->ref = ref->val();

    CYTSec *tmoy = (CYTSec *)core->findData(QString("METRO_TMOY%1_%2").arg(i+1).arg(mMeasure->objectName()));
    point->tmoy = tmoy->val();

    points.append(point);
  }

  linkDatas();
  std::sort(points.begin(), points.end());
  updateList();
  mTimer->start(250);
}


CYMetroCalibrate::~CYMetroCalibrate()
{
}

bool CYMetroCalibrate::editing()
{
  if (mMeasure)
    return mMeasure->metroEditing();
  return false;
}

void CYMetroCalibrate::setReadOnly(bool val)
{
  CYWidget::setReadOnly(val);
  ui->sensorSheet->setReadOnly(true);
}

void CYMetroCalibrate::showEvent(QShowEvent *e)
{
  CYWidget::showEvent(e);
}

void CYMetroCalibrate::addPoint()
{
  if (nb_pts>=mMeasure->metroMaxPoints())
    return;

  mMeasure->metroStartMoy(ui->tmoy->value());

  CYMetroCaliInput *dlg = new CYMetroCaliInput(this);
  dlg->init(mMeasure);
  if (dlg->exec())
  {
    nb_pts++;
    CYMetroPoint *point = new CYMetroPoint(mMeasure);

    CYF32 *bdir = (CYF32 *)core->findData(QString("METRO_BDIR_%1").arg(mMeasure->objectName()));
    point->bdir = bdir->val();

    CYF32 *bench = (CYF32 *)core->findData(QString("METRO_BENCH_%1").arg(mMeasure->objectName()));
    point->bench = bench->val();

    CYF32 *ref = (CYF32 *)core->findData(QString("METRO_REF_%1").arg(mMeasure->objectName()));
    point->ref = ref->val();

    point->tmoy = mMeasure->metroTMoy->val();
    points.append(point);
    std::sort(points.begin(), points.end());
    setEditing(true);
    updateList();
  }
}

void CYMetroCalibrate::deletePoint()
{
  if (nb_pts==0)
    return;
  int row = ui->table->currentRow();
  if (row<0)
    return;

  delete points.takeAt(row);
  ui->table->removeRow(row);
  nb_pts--;
  setEditing(true);
  updateList();
}

void CYMetroCalibrate::reset()
{
  while (!points.isEmpty())
      delete points.takeFirst();
  for (int row=nb_pts; row>0; row--)
    ui->table->removeRow(row);
  nb_pts=0;
  setEditing(true);
  setWindowTitle(tr("Calibrating sheet '%1' of %2 (Modified)").arg(mMeasure->phys()).arg(mMeasure->calibrateLast->val()));
  updateList();
}

void CYMetroCalibrate::setDesigner()
{
  mMeasure->setDesignerMetro();
  initWithMeasure(mMeasure->objectName());
  setEditing(true);
  updateList();
}

void CYMetroCalibrate::setEditing(bool val)
{
  if (mMeasure)
    emit editing(mMeasure, val);
  emit editing(val);
}

void CYMetroCalibrate::memEditing()
{
  mMeasure->metroNb->setTmp(nb_pts);

  if (points.count()!=nb_pts)
  {
    CYWARNINGTEXT(QString("points.count()=%1 ! nb_pts=%2").arg(points.count()).arg(nb_pts));
    return;
  }

  points.sortDirect();

  int i=0;

  QListIterator<CYMetroPoint *> it(points);
  while (it.hasNext())
  {
    CYMetroPoint *point=it.next();
    CYF32 *bdir = (CYF32 *)core->findData(QString("METRO_BDIR%1_%2").arg(i+1).arg(mMeasure->objectName()));
    bdir->setTmp(point->bdir);

    CYF32 *bench = (CYF32 *)core->findData(QString("METRO_BENCH%1_%2").arg(i+1).arg(mMeasure->objectName()));
    bench->setTmp(point->bench);

    CYF32 *ref = (CYF32 *)core->findData(QString("METRO_REF%1_%2").arg(i+1).arg(mMeasure->objectName()));
    ref->setTmp(point->ref);

    CYTSec *tmoy = (CYTSec *)core->findData(QString("METRO_TMOY%1_%2").arg(i+1).arg(mMeasure->objectName()));
    tmoy->setTmp(point->tmoy);
  }

  std::sort(points.begin(), points.end());
}

void CYMetroCalibrate::calibrate()
{
  int res = CYMessageBox::warningYesNo( this, tr("Do you want to validate this calibration for %1 ?").arg(mMeasure->phys()));
  if ( res == CYMessageBox::No )
    return;
  CYFlag *flag = (CYFlag *)core->findData("CY_EVENT_METRO_CAL");
  flag->setVal(true);

  CYString *sensor = (CYString *)core->findData("CY_EVENT_METRO_LAST");
  sensor->setVal(mMeasure->phys());

  mMeasure->metroNb->setVal(nb_pts);

  if (points.count()!=nb_pts)
  {
    CYWARNING;
    return;
  }

  points.sortDirect();

  int i=0;

  QListIterator<CYMetroPoint *> it(points);
  while (it.hasNext())
  {
    CYMetroPoint *point=it.next();

    CYF32 *bdir = (CYF32 *)core->findData(QString("METRO_BDIR%1_%2").arg(i+1).arg(mMeasure->objectName()));
    bdir->setVal(point->bdir);

    CYF32 *bench = (CYF32 *)core->findData(QString("METRO_BENCH%1_%2").arg(i+1).arg(mMeasure->objectName()));
    bench->setVal(point->bench);

    CYF32 *ref = (CYF32 *)core->findData(QString("METRO_REF%1_%2").arg(i+1).arg(mMeasure->objectName()));
    ref->setVal(point->ref);

    CYTSec *tmoy = (CYTSec *)core->findData(QString("METRO_TMOY%1_%2").arg(i+1).arg(mMeasure->objectName()));
    tmoy->setVal(point->tmoy);

    i++;
  }

  update();
  CYDB *db = mMeasure->metroDB;
  db->writeData();
  db->save();

  std::sort(points.begin(), points.end());

  mMeasure->setCalibrateChanged();

  print(true);

  setEditing(false);
  accept();
}

void CYMetroCalibrate::verification()
{
  int res = CYMessageBox::warningYesNoCancel( this, tr("End of verification de %1 !\nIn accordance ?").arg(mMeasure->phys()));
  if ( res == CYMessageBox::Cancel )
    return;
  if ( res == CYMessageBox::Yes )
    mMeasure->calibrateOk->setVal(true);
  else
    mMeasure->calibrateOk->setVal(false);

  CYFlag *flag = (CYFlag *)core->findData("CY_EVENT_METRO_CAL");
  flag->setVal(true);

  CYString *sensor = (CYString *)core->findData("CY_EVENT_METRO_LAST");
  sensor->setVal(mMeasure->phys());

  update();
  mMeasure->setCalibrateControlled();

  print(false);

  accept();
}

void CYMetroCalibrate::accept()
{
  QProcess *proc1 = new QProcess(this);
  QString program = "ps2pdf";
  QStringList arguments;
  arguments << "-sPAPERSIZE=a4" << "/tmp/cymetrocalibrate.ps" << fileName;
  proc1->start(program, arguments);
  QFile file(fileName);
  while (!file.exists());
  sleep(1);

  if (!QDesktopServices::openUrl(fileName))
  {
    CYMessageBox::error(this, tr("Can't open %1").arg(fileName));
    return;
  }

  mMeasure->metroPV->setVal(fileName);
  mMeasure->metroDB->save();

  setEditing(false);
  updateList();
}

bool CYMetroCalibrate::cancel()
{
  CYMeasure *mes = (CYMeasure *)core->findData(genericMark());
  if (CYMessageBox::No ==
      CYMessageBox::warningYesNo(this, tr("Do you want to close the calibration sheet of %1 without saving the latest changes ?").arg(mes->phys())))
    return false;
  mMeasure->calDB->load();
  setEditing(false);
  initWithMeasure();
  return true;
}

void CYMetroCalibrate::updateList()
{
  bool editing = mMeasure->metroEditing();
  bool designer = mMeasure->forcedDesignerMetro();

  disconnect(ui->table, SIGNAL(itemSelectionChanged()), this, SLOT(refreshCursor()));

  int nb = nb_pts;
  double x[nb], err_fs[nb], umax[nb], umin[nb], pmax[nb], pmin[nb], err_lin[nb];

  float gain, offset;
  mMeasure->calcul_gain_offset(gain, offset);

  if ((core->simulation() ||editing) && (nb<mMeasure->metroMaxPoints()))
    mDisableAdd->setVal(designer);
  else
    mDisableAdd->setVal(true);

  if ((core->simulation() ||editing) && (nb>0))
    mDisableDel->setVal(designer);
  else
    mDisableDel->setVal(true);

  if ((core->simulation() || editing || designer) && (nb>=2))
  {
    mDisableCal->setVal(mMeasure->metroCalEnable() ? false: true);
    mDisableVer->setVal(designer);
  }
  else if ((core->simulation() || editing || designer) && (nb==1))
  {
    mDisableCal->setVal(true);
    mDisableVer->setVal(designer);
  }
  else
  {
    mDisableCal->setVal(true);
    mDisableVer->setVal(true);
  }

  mDisableRaz->detectValueChanged();
  mDisableAdd->detectValueChanged();
  mDisableDel->detectValueChanged();
  mDisableCal->detectValueChanged();
  mDisableVer->detectValueChanged();
  mDisableDes->detectValueChanged();

  setReadOnly(!editing);

  ui->summary->setText(mMeasure->metroSummary(editing));

  ui->table->setRowCount(0);

  CYF32 *bdir = (CYF32 *)core->findData(QString("METRO_BDIR_%1").arg(mMeasure->objectName()));
  CYF32 *ref = (CYF32 *)core->findData(QString("METRO_REF_%1").arg(mMeasure->objectName()));
  int row=0;
  float theor;

  QListIterator<CYMetroPoint *> it(points);
  while (it.hasNext())
  {
    CYMetroPoint *point=it.next();

    ui->table->setRowCount(row+1);
    theor = (mMeasure->designerCal() || mMeasure->notCal()) ? point->bench : point->brut(gain, offset);
    x[row] = point->ref;
    umin[row] = point->uncertaintyMin();
    pmax[row] = point->uncertaintyPC();
    pmin[row] = point->uncertaintyPC()*(-1.0);
    umax[row] = point->uncertaintyMax();
    if (mMeasure->sensorCalAuto->val())
    {
      err_fs[row] = point->error_fs();
      err_lin[row] = point->error_linearity(theor);

      ui->table->setText(row, colMin    , ref->toString(umin[row], false, false));
      ui->table->setText(row, colRef    , ref->toString(point->ref, false, false));
      ui->table->setText(row, colMax    , ref->toString(umax[row], false, false));
      ui->table->setText(row, colBench  , ref->toString(point->bench, false, false));
      ui->table->setText(row, colEcart  , ref->toString(point->error(), false, false));
      ui->table->setText(row, colEcartPE, QString::number(err_fs[row], 'f', 3));
      ui->table->setText(row, colDirect , bdir->toString(point->bdir, false, false));
      ui->table->setText(row, colTheor  , ref->toString(theor, false, false));
      ui->table->setText(row, colErrLin , QString::number(err_lin[row], 'f', 3));
      ui->table->setText(row, colTMoy   , mMeasure->metroTMoy->toString(point->tmoy, false, false));
    }
    else
    {
      ui->table->setText(row, colMin    , ref->toString(umin[row], false, false));
      ui->table->setText(row, colRef    , ref->toString(point->ref, false, false));
      ui->table->setText(row, colMax    , ref->toString(umax[row], false, false));
      ui->table->setText(row, colDirect , bdir->toString(point->bdir, false, false));
      ui->table->setText(row, colTheor  , ref->toString(theor, false, false));
      ui->table->setText(row, colErrLin , QString::number(err_lin[row], 'f', 3));
    }
    row++;
  }

  for (int col=0; col<ui->table->columnCount(); col++)
  {
    ui->table->resizeColumnToContents(col);
  }
  QPen pen;

  curveLin->setVisible(mViewWhole->val());

  if (mMeasure->sensorCalAuto->val())
  {
    curveErr->show();
    ui->plot->setAxisTitle(QwtPlot::yLeft, tr("Error (% FS)"));
  }
  else
  {
    curveErr->hide();
    ui->plot->setAxisTitle(QwtPlot::yLeft, tr("Linearity error (% FS)"));
  }

  ui->plot->setAxisTitle(QwtPlot::xBottom, QString("%1 (%2)").arg(mMeasure->format()->physical()).arg(mMeasure->unit()));

  curveMax->setSamples(x, pmax, nb+1, 1.0);
  pen = curveMax->pen();
  pen.setColor(Qt::red);
  pen.setWidth(2);
  curveMax->setPen(pen);

  curveMin->setSamples(x, pmin, nb+1, 1.0);
  pen = curveMin->pen();
  pen.setColor(Qt::red);
  pen.setWidth(2);
  curveMin->setPen(pen);

  if (mViewWhole->val() && (mLinCurveStyle->val()!=QwtPlotCurve::NoCurve))
  {
    curveLin->setSamples(x, err_lin, nb+1, 1.0);
    curveLin->setStyle((QwtPlotCurve::CurveStyle)mLinCurveStyle->val());
    pen = curveLin->pen();
    pen.setColor(mLinCurveColor->val());
    pen.setWidth(4);
    curveLin->setPen(pen);
  }

  if (mMeasure->sensorCalAuto->val() && (mDevCurveStyle->val()!=QwtPlotCurve::NoCurve))
  {
    curveErr->setSamples(x, err_fs, nb+1, 1.0);
    curveErr->setStyle((QwtPlotCurve::CurveStyle)mDevCurveStyle->val());
    pen = curveErr->pen();
    pen.setColor(mDevCurveColor->val());
    pen.setWidth(4);
    curveErr->setPen(pen);
  }

  cursorY2->setVisible(mViewWhole->val());
//  if (mViewWhole->val())
//  {
//    cursorY2 = new QwtPlotMarker();
//    cursorY2->setLineStyle(QwtPlotMarker::HLine);
//    cursorY2->attach(ui->plot);
//  }

  QColor cursorColor = mDevCurveColor->val();
  QPen cursorPen;
  cursorPen.setWidth(2);
  cursorPen.setStyle(Qt::DashLine);
  cursorPen.setColor(cursorColor);
  cursorX->setLinePen(cursorPen);
  cursorY1->setLinePen(cursorPen);
  cursorPen.setColor(cursorColor);
  cursorPen.setColor(mLinCurveColor->val());

  // Insertion d'un axe de référence pour y gauche = 0
  yLeftRef = new QwtPlotMarker();
  yLeftRef->setLineStyle(QwtPlotMarker::HLine);
  yLeftRef->attach(ui->plot);
  QColor yLeftRefColor = Qt::darkGray;
  QPen yLeftRefPen;
  yLeftRefPen.setWidth(2);
  yLeftRefPen.setColor(yLeftRefColor);
  yLeftRef->setLinePen(yLeftRefPen);
  yLeftRef->setYValue(0.0);

  if (ui->plot->legend())
    ui->plot->legend()->show();

  ui->plot->replot();
  connect(ui->table, SIGNAL(itemSelectionChanged()), this, SLOT(refreshCursor()));
}

void CYMetroCalibrate::refreshCursor()
{
  int row = ui->table->currentRow();
  if (row<0)

  if (!points.count())
    return;

  CYMetroPoint *point=points.at(row);
  if (!point)
    return;

  cursorX->setXValue(point->ref);
  cursorY1->setYValue(point->error_fs());
  if (mViewWhole->val())
      cursorY2->setYValue(point->error_linearity());
  ui->plot->replot();
}

void CYMetroCalibrate::setTableBold(int row, int col, bool enable)
{
  QWidget *wdg = ui->table->cellWidget( row, col );
  if (!wdg)
    return;
  QFont font = wdg->font();
  font.setBold(enable);
  wdg->setFont(font);
}

void CYMetroCalibrate::print(bool calibration)
{
  QPrinter *printer = new QPrinter();
  viewTable(true);
  hide();
  show();

  // Donne un nom au document
  QDateTime dt = (calibration) ? (*mMeasure->calibrateLast) : (*mMeasure->calibrateLastCtrl);
  QString date = QString("%1").arg(dt.date().toString(Qt::ISODate));
  QString time = QString("%1").arg(dt.time().toString("hh-mm-ss"));
  CYString *dir = (CYString *)core->findData("CY_METRO_DIR");

  docName = tr("%1_%2_%3").arg(mMeasure->phys()).arg(date).arg(time);
  if (calibration)
    docName.append(tr("_calibrating"));
  else
    docName.append(tr("_verification"));

  QString path = tr("%1/%2").arg(dir->val()).arg(mMeasure->phys());

  if (!QDir(path).exists())
    core->mkdir(path);

  fileName = tr("%1/%2.pdf").arg(path).arg(docName);
  printer->setOutputFileName("/tmp/cymetrocalibrate.ps");
  printer->setColorMode(QPrinter::Color);
  printer->setPageOrientation(QPageLayout::Portrait);

  printer->setDocName(docName);

  // Donne le nom du créateur
  printer->setCreator(cyapp->caption());

  // construit un dessin pour remplir l'objet printer.
  QPainter painter;

  //-------------------------------------------------------------------------------------
  // Début du dessin
  painter.begin(printer);

  QFont font = painter.font();
  QPen pen = painter.pen();
  for (int num_page=1; num_page<=2; num_page++)
  {
    if (num_page>1)
    {
      printer->newPage();
    }
    newPage(&painter, printer, num_page);
    painter.setFont(font);
    painter.setPen(pen);
  }
  // fin du dessin, celui-ci envoye automatiquement les données à l'imprimante
  painter.end();
  //-------------------------------------------------------------------------------------
  viewTable(mViewWhole->val());
}

void CYMetroCalibrate::newPage(QPainter *p, QPrinter *printer, int &num_page)
{
  QRectF r, rTmp, rGraph;

  // abandon si la taille du capture est nul ou si l'une des marges d'impression est nulle
  if ((printer->width() == 0) || (printer->height() == 0) || this->size().isNull())
    return;

  // Prise en compte des marges d'impression
  r = printer->pageRect(QPrinter::DevicePixel);
  // correction pageRect trop grand
  r.setWidth(r.width()-printer->pageLayout().margins().right()*2);
  r.setHeight(r.height()-printer->pageLayout().margins().bottom()*2);

  if (r.isNull()) return;

  // Les opérations QPainter pour la page i
  rTmp = r;

  // Création de l'en-tête
  printHeader(p, &rTmp);

  // Création du pied de page
  printFooter(p, &rTmp, num_page);

  int hline = p->fontMetrics().height()+1;
  if (num_page==1)
  {
    // Passage de lignes
    rTmp.setTop(rTmp.top()+hline*2);

    // Création de zone d'informations étalonnage
    mMeasure->printInfoCal(p, &rTmp);

    // Passe une ligne
    rTmp.setTop(rTmp.top()+hline*2);

    // Création de la fiche capteur
    mMeasure->printSensorSheet(p, &rTmp);

//     // Passe une ligne
//     rTmp.setTop(rTmp.top()+hline);

  }

  if (num_page==2)
  {
    // Création de la zone ui->tableau
    ui->table->print(p, &rTmp);

    // Création de la zone d'affichage des légendes
    printLegends(p, &rTmp);

    // Création de la zone graphique
    rGraph.setRect(rTmp.left(), rTmp.top(), rTmp.width(), rTmp.height());
    ui->plot->printCurves(p, &rGraph);
  }
}

void CYMetroCalibrate::printHeader(QPainter *p, QRectF *r)
{
  QRectF rect;
  QRectF rHeader = QRectF(r->left(), r->top(), r->width(), r->height());

  if (core)
  {
    qreal wlogo    = 0;
    qreal hlogo    = 0;
    qreal htext    = 0;
    qreal hbench   = 0;
    qreal hproject = 0;

    p->setBackground(Qt::blue);
    QPixmap logo = core->customerLogo();
    // force la taille à 64
    logo = logo.scaledToHeight(64, Qt::SmoothTransformation);
    p->drawPixmap(rHeader.left(), rHeader.top(), logo);
    wlogo = logo.width();
    hlogo = logo.height();

    QString bench = core->printingHeader()+ " - " + core->customerRef() + "\n" + core->testType();
    p->drawText(QRectF(rHeader.left()+wlogo, rHeader.top(), rHeader.width()-wlogo, hlogo), Qt::AlignCenter|Qt::TextWordWrap, bench, &rect);
    hbench = rect.height();

    QPen pen = p->pen();
    QColor color = pen.color();
    pen.setColor(Qt::blue);
    p->setPen(pen);
    QString text;
    if (mMeasure->addr()!="")
      text = mMeasure->phys() + " : " +mMeasure->label()+" ["+mMeasure->elec()+" - "+mMeasure->addr()+"]";
    else
      text = mMeasure->phys() + " : " +mMeasure->label()+" ["+mMeasure->elec()+"]";
    p->drawText(QRectF(rHeader.left()+wlogo, rHeader.top()-hbench, rHeader.width()-wlogo, hlogo), Qt::AlignCenter|Qt::TextWordWrap, text, &rect);
    hproject = rect.height();
    pen.setColor(color);
    p->setPen(pen);

    // ajuste la hauteur du rectangle contenant l'en-tête
    htext = hbench + hproject;
    rHeader.setHeight(qMax(hlogo, htext)+20);
  }
  else
    rHeader.setHeight(0);

  // diminue la hauteur du rectangle disponible
  int height = r->height() - rHeader.height();
  r->setRect(r->left(), rHeader.bottom(), r->width(), height);
}

void CYMetroCalibrate::printFooter(QPainter *p, QRectF *r, int page)
{
  QRectF rect;
  QRectF rFooter = QRectF(r->left(), r->bottom(), r->width(), r->height());

  if (core)
  {
    qreal h      = 0;
    qreal wlogo  = 0;
    qreal wbench = 0;
    qreal wpage  = 0;
    qreal wdate  = 0;

    // geometries du logo
    QPixmap logo = core->designerLogo();
    // force la taille à 32
    logo = logo.scaledToHeight(32, Qt::SmoothTransformation);
    wlogo = logo.width()+2;
    h = logo.height()+2;

    // geometries du numéro de page
    p->drawText(QRectF(rFooter.right()-wpage, rFooter.top(), rFooter.width()-wlogo-wbench, 0), Qt::AlignCenter, tr("Page %1/%2").arg(99).arg(99), &rect);
    wpage = rect.width();
    h = qMax(h, rect.height());

    // geometries de la reference constructeur
    QString bench = core->designerRef();
    p->drawText(QRectF(rFooter.left()+wlogo, rFooter.top(), rFooter.width()-wlogo-wdate-wpage, 0), Qt::AlignCenter|Qt::TextWordWrap, bench, &rect);
    wbench = rect.width();
    h = qMax(h, rect.height());

    // geometries du nom du fichier
    p->drawText(QRectF(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.width()-wlogo-wdate-wbench, 0), Qt::AlignCenter|Qt::TextWordWrap, docName, &rect);
    h = qMax(h, rect.height());

    // repositionne le rectangle pied de page
    rFooter.setRect(r->left(), r->bottom()-h, r->width(), r->height());

    // dessine le cadre
    rFooter.setHeight(h);
    p->drawRect(rFooter);

    // dessine le logo
    p->drawPixmap(rFooter.left()+1, rFooter.top()+1, logo);

    // dessine la reference constructeur
    p->drawLine(rFooter.left()+wlogo, rFooter.top(), rFooter.left()+wlogo, rFooter.bottom());
    p->drawText(QRectF(rFooter.left()+wlogo, rFooter.top(), wbench, h), Qt::AlignCenter|Qt::TextWordWrap, bench, &rect);

    // dessine le nom du fichier
    p->drawLine(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.left()+wlogo+wbench, rFooter.bottom());
    p->drawText(QRectF(rFooter.left()+wlogo+wbench, rFooter.top(), rFooter.width()-wlogo-wdate-wbench, h), Qt::AlignCenter|Qt::TextWordWrap, docName, &rect);

    // dessine le numéro de page
    p->drawLine(rFooter.right()-wpage, rFooter.top(), rFooter.right()-wpage, rFooter.bottom());
    p->drawText(QRectF(rFooter.right()-wpage, rFooter.top(), wpage, h), Qt::AlignCenter, tr("Page %1/2").arg(page), &rect);
  }
  else
    rFooter.setHeight(0);

  // diminue la hauteur du rectangle disponible
  int height = r->height() - rFooter.height();
  r->setRect(r->left(), r->top(), r->width(), height);
}


void CYMetroCalibrate::printLegends(QPainter *p, QRectF *r)
{
  QRectF rect;
  QwtPlotItemList curveList = ui->plot->itemList(QwtPlotItem::Rtti_PlotCurve);
  int nb = curveList.count();
  qreal h  = 0;   // hauteur totale


  // COURBES
  qreal wc1 = 35;  // largeur d'un rectangle contenant un trait de legende courbe
  qreal wc2 = 0;   // largeur d'un rectangle maximum contenant un texte de legende courbe
  // determine largeur d'un rectangle maximum contenant un texte de legende courbe
  // et la hauteur totale des textes de legende courbe
  for (int i=0; i<nb; i++)
  {
    QwtPlotCurve *curve = (QwtPlotCurve *)curveList.at(i);
    QString title = curve->title().text();
    p->drawText(QRectF(0, 0, r->width()-wc1, 0), Qt::AlignTop|Qt::AlignLeft|Qt::TextWordWrap, title, &rect);
    wc2 = qMax(wc2, rect.width());
    h = qMax(h, rect.height());
  }

  int offset =(r->width()-nb*(wc1 + wc2))/2;
  // dessine pour les courbes
  for (int i=0; i<nb; i++)
  {
    int init = r->left() + offset + i*(wc1 + wc2);
    QRectF r1 = QRectF(init, r->bottom()-h, r->width(), h);

    // trait de légende d'une courbe
    QPen oldPen = p->pen();
    QwtPlotCurve *curve = (QwtPlotCurve *)curveList.at(i);
    p->setPen(curve->pen());
    int hor = r1.bottom() + (r1.top()- r1.bottom())/2;
    p->drawLine(r1.left(), hor, r1.left()+wc1-5, hor);
    p->setPen(oldPen);

    // texte de légende d'une courbe
    QString title = curve->title().text();
    p->drawText(QRectF(r1.left()+wc1, r1.top(), wc2, r1.height()), Qt::AlignTop|Qt::AlignLeft|Qt::TextWordWrap, title, &rect);
  }

  r->setHeight(r->height()-h);
}

void CYMetroCalibrate::viewTable(bool whole)
{
  char fl_auto = mMeasure->sensorCalAuto->val();
  if (whole)             ui->table->showColumn(colMin  );
  else                   ui->table->hideColumn(colMin  );

  if (whole)             ui->table->showColumn(colMax  );
  else                   ui->table->hideColumn(colMax  );

  if (fl_auto)           ui->table->showColumn(colBench  );
  else                   ui->table->hideColumn(colBench  );

  if (fl_auto && whole)  ui->table->showColumn(colEcart  );
  else                   ui->table->hideColumn(colEcart  );

  if (fl_auto)           ui->table->showColumn(colEcartPE);
  else                   ui->table->hideColumn(colEcartPE);

  if (!fl_auto || whole) ui->table->showColumn(colTheor  );
  else                   ui->table->hideColumn(colTheor  );

  if (fl_auto)           ui->table->showColumn(colTMoy   );
  else                   ui->table->hideColumn(colTMoy   );
}
