//
// C++ Interface: cymetrosensorsheet
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMETROSENSORSHEET_H
#define CYMETROSENSORSHEET_H

// CYLIBS
#include <cydialog.h>

namespace Ui {
		class CYMetroSensorSheet;
}

/**
@short Fenêtre de saisie fiche capteur de métrologie.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMetroSensorSheet : public CYDialog
{
Q_OBJECT
public:
    CYMetroSensorSheet(QWidget *parent = 0, const QString &name = 0);

    ~CYMetroSensorSheet();

public slots:
    /** Applique les modifications et ferme la boîte de dialogue.*/
    virtual void accept();

private:
    Ui::CYMetroSensorSheet *ui;
};

#endif
