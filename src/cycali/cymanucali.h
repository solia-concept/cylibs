/***************************************************************************
                          cymanucali.h  -  description
                             -------------------
    début                  : ven jun 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYMANUCALI_H
#define CYMANUCALI_H

// CYLIBS
#include "cydialog.h"

class CYAna;
class CYMeasure;
class CYManuCaliInput;

namespace Ui {
    class CYManuCali;
}

/** @short Boîte de dialogue pour le calibrage manuel d'une E/S.
  * @author Gérald LE CLEACH
  */

class CYManuCali : public CYDialog
{
  Q_OBJECT
public:
  CYManuCali(QWidget *parent=0, const QString &name=0);
  ~CYManuCali();

public slots: // Public slots
  /** Initialise la boîte de dialogue avec la donnée analogique à calibrer. */
  void init(CYMeasure *d);
  /** Saisie la valeur haute. */
  void setHighData();
  /** Saisie la valeur basse. */
  void setLowValue();
  /** Saisie les valeurs hautes et basses avec les valeurs constructeur. */
  void setDesignerValue();
  /** Ouvre le manuel de Cylix de manière contextuelle */
  void help();
private: // Private attributes
  /** E/S analogique à calibrer. */
  CYAna *mAna;
  /** E/S numérique à calibrer. */
  CYMeasure *mMeasure;

  /** Boîte de saisie de la valeur haute/basse. */
  CYManuCaliInput *mInput;

private:
    Ui::CYManuCali *ui;
};

#endif
