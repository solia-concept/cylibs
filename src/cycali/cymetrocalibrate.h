//
// C++ Interface: cymetrocalibrate
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMETROCALIBRATE_H
#define CYMETROCALIBRATE_H

// QT
#include <QPrinter>
// CYLIBS
#include "cywidget.h"
#include "cymetropoints.h"

class QwtPlotCurve;
class QwtPlotMarker;
class CYMeasure;
class CYU8;
class CYS8;
class CYColor;
class CYFlag;

namespace Ui {
		class CYMetroCalibrate;
}

/**
@short Widget d'étalonnage capteur

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMetroCalibrate : public CYWidget
{
Q_OBJECT

  /** Énumération des colonnes */
  enum Column
  {
    colMin,
    colRef,
    colMax,
    colBench,
    colEcart,
    colEcartPE,
    colDirect,
    colTheor,
    colErrLin,
    colTMoy,
  };

public:
    CYMetroCalibrate(QWidget *parent = 0, const QString &name = 0, bool multiple=false);

    ~CYMetroCalibrate();

  /** Initialise le widget avec la mesure ayant pour nom \a mesName. */
  virtual void initWithMeasure(const QString &name);
  /** Initialise le widget avec la mesure courante. */
  virtual void initWithMeasure();

  /** @return \a true si en cours d'édition .*/
  bool editing();
  /** @return le nombre de points .*/
  int nbPoints() { return nb_pts; }
  /** Affichage complet du tableau suivant \a whole. */
  void viewTable(bool whole);

public slots: // Public slots
  /** Met le widget en lecture seule ou pas suivant \a val. */
  virtual void setReadOnly(bool val);
  /** Ouvre une fenêtre de saisie d'un point d'étalonnage. */
  void addPoint();
  /** Supprime le point d'étalonnage de la ligne sélectionnée. */
  void deletePoint();
  /** Remet à zéro la liste des point d'étalonnage. */
  void reset();
  /** Mémorise l'édition en multi-voies. */
  virtual void memEditing();
  /** Envoie l'étalonnage au régulateur. */
  virtual void calibrate();
  /** Valide ou non la l'étalonnage. */
  virtual void verification();
  /** Force les valeurs constructeur du capteur sélectionné. */
  virtual void setDesigner();
  virtual void accept();
  virtual bool cancel();
  virtual void updateList();
  virtual void refreshCursor();
  /** Impression */
  void print(bool calibration);

  void setEditing(bool val);

signals :
  void editing(bool val);
  void editing(CYMeasure *mes, bool val);

protected: // Protected methods
  /** Fonction appelée à l'affichage du widget. */
  virtual void showEvent(QShowEvent *e);
  /** Met le texte d'une cellule en gras ou non selon \a enable
    * @param row  ligne de la cellule.
    * @param col  colonne de la cellule.
    */
  void setTableBold(int row, int col, bool enable=true);

  /** Impression d'une page
    * @param page Nombre de pages déjà imprimés. */
  void newPage(QPainter *p, QPrinter *printer, int &page);
  /** Création de l'en-tête. */
  void printHeader(QPainter *p, QRectF *r);
  /** Création du pied de page. */
  void printFooter(QPainter *p, QRectF *r, int page);
  /** Impression des légendes des courbes. */
  void printLegends(QPainter *p, QRectF *r);

protected: // Protected attributes
  CYMeasure *mMeasure;

  CYFlag *mDisableRaz;
  CYFlag *mDisableAdd;
  CYFlag *mDisableDel;
  CYFlag *mDisableCal;
  CYFlag *mDisableVer;
  CYFlag *mDisableDes;
  CYFlag *mViewWhole;
  CYColor *mDevCurveColor;
  CYS8   *mDevCurveStyle;
  CYColor *mLinCurveColor;
  CYS8   *mLinCurveStyle;

  int nb_pts;
  int nb_curves;
  float gain;
  float offset;

  QwtPlotCurve *curveMax;
  QwtPlotCurve *curveMin;
  QwtPlotCurve *curveLin;
  QwtPlotCurve *curveErr;

  QwtPlotMarker *cursorX;
  QwtPlotMarker *cursorY1;
  QwtPlotMarker *cursorY2;
  QwtPlotMarker *yLeftRef;

  bool mMultiple;

  CYMetroPoints<CYMetroPoint> points;

  QString docName;
  QString fileName;

private:
    Ui::CYMetroCalibrate *ui;
};

#endif
