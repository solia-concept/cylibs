/***************************************************************************
                          cymanucaliinput.h  -  description
                             -------------------
    début                  : ven jun 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYMANUCALIINPUT_H
#define CYMANUCALIINPUT_H

// CYLIBS
#include "cydialog.h"

class CYAna;
class CYMeasure;

namespace Ui {
    class CYManuCaliInput;
}

/** @short Boîte de saisie d'une valeur basse/haute en calibrage automatique.
  * @author Gérald LE CLEACH
  */

class CYManuCaliInput : public CYDialog
{
  Q_OBJECT
public:
  CYManuCaliInput(QWidget *parent=0, const QString &name=0);
  ~CYManuCaliInput();

public slots: // Public slots
  /**
   * Initialise la boîte de dialogue avec la donnée à calibrer.
   * @param d    Donnée à calibrer.
   * @param top  Si \a true, valeur haute à calibrer, sinon valeur basse à calibrer.
   */
  void init(CYMeasure *d, bool top=true);
  /** Applique les modifications et ferme la boîte de dialogue. */
  void accept();
  /** Saisie le titre. */
  void setTitle(const QString &title);

private:
  Ui::CYManuCaliInput *ui;

  /** E/S numérique à calibrer. */
  CYMeasure *mMeasure;
};

#endif
