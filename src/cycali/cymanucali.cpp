/***************************************************************************
                          cymanucali.cpp  -  description
                             -------------------
    début                  : ven jun 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cymanucali.h"
#include "ui_cymanucali.h"

// QT
#include <QPushButton>
// CYLIBS
#include "cycore.h"
#include "cyana.h"
#include "cyadc16.h"
#include "cyadc32.h"
#include "cynumdisplay.h"
#include "cymanucaliinput.h"
#include "cystring.h"
#include "cytextinput.h"
#include "cybuttongroup.h"
#include "cymessagebox.h"

CYManuCali::CYManuCali(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYManuCali)
{
  mAna = nullptr;
  mMeasure = nullptr;
  ui->setupUi(this);
}

CYManuCali::~CYManuCali()
{
  delete ui;
}

void CYManuCali::init(CYMeasure *d)
{
  mMeasure = d;
  if (mMeasure->inherits("CYAna"))
  {
    mAna = qobject_cast<CYAna*>(mMeasure);

    ui->hcalval->setDataName(mAna->top()->dataName());
    ui->lcalval->setDataName(mAna->low()->dataName());
    ui->dhcalval->setNbDec(mAna->top()->nbDec());
    ui->dlcalval->setNbDec(mAna->low()->nbDec());
    ui->dhcalval->setValue(mAna->top()->def());
    ui->dlcalval->setValue(mAna->low()->def());
    ui->dhcalval->setSuffix(QString(" %1").arg(mAna->unit()));
    ui->dlcalval->setSuffix(QString(" %1").arg(mAna->unit()));
    ui->noting->setDataName(mAna->calibrateNoting->dataName());

    if (mAna->adc16())
    {
      ui->hdirectval->setDataName(mAna->adc16()->top()->dataName());
      ui->ldirectval->setDataName(mAna->adc16()->low()->dataName());
      ui->dhdirectval->setValue(mAna->adc16()->top()->def());
      ui->dldirectval->setValue(mAna->adc16()->low()->def());
      ui->dhdirectval->setSuffix(QString(" %1").arg(mAna->adc16()->unit()));
      ui->dldirectval->setSuffix(QString(" %1").arg(mAna->adc16()->unit()));
    }
    else if (mAna->adc32())
    {
      ui->hdirectval->setDataName(mAna->adc32()->top()->dataName());
      ui->ldirectval->setDataName(mAna->adc32()->low()->dataName());
      ui->dhdirectval->setValue(mAna->adc32()->top()->def());
      ui->dldirectval->setValue(mAna->adc32()->low()->def());
      ui->dhdirectval->setSuffix(QString(" %1").arg(mAna->adc32()->unit()));
      ui->dldirectval->setSuffix(QString(" %1").arg(mAna->adc32()->unit()));
    }
    else
      CYWARNINGTEXT(QString("CYManuCali::init(CYAna *d): %1").arg(objectName()));
  }
  else
  {
    ui->hcalval->setDataName(mMeasure->top()->dataName());
    ui->lcalval->setDataName(mMeasure->low()->dataName());
    ui->dhcalval->setNbDec(mMeasure->top()->nbDec());
    ui->dlcalval->setNbDec(mMeasure->low()->nbDec());
    ui->dhcalval->setValue(mMeasure->top()->def());
    ui->dlcalval->setValue(mMeasure->low()->def());
    ui->dhcalval->setSuffix(QString(" %1").arg(mMeasure->unit()));
    ui->dlcalval->setSuffix(QString(" %1").arg(mMeasure->unit()));
    ui->noting->setDataName(mMeasure->calibrateNoting->dataName());

    ui->hdirectval->setDataName(mMeasure->direct()->top()->dataName());
    ui->ldirectval->setDataName(mMeasure->direct()->low()->dataName());
    ui->dhdirectval->setValue(mMeasure->direct()->top()->def());
    ui->dldirectval->setValue(mMeasure->direct()->low()->def());
    ui->dhdirectval->setSuffix(QString(" %1").arg(mMeasure->direct()->unit()));
    ui->dldirectval->setSuffix(QString(" %1").arg(mMeasure->direct()->unit()));
  }

  setGenericMark(mMeasure->dataName());

  if (!mMeasure->calibrateOutdatedEvent())
    ui->dateGroup->hide();

  connect(this, SIGNAL(validate()), mMeasure, SLOT(setCalibrateChanged()));

  mTimer->start(250); // ms
}

void CYManuCali::setHighData()
{
  mInput = new CYManuCaliInput(this, "CYManuCaliInput");
  Q_CHECK_PTR(mInput);
  mInput->setWindowTitle(QString(tr("High")+": %1").arg(windowTitle()));
  mInput->setTitle(tr("High Value"));
  mInput->init(mMeasure, true);
  mInput->exec();
  refresh();
}

void CYManuCali::setLowValue()
{
  mInput = new CYManuCaliInput(this, "CYManuCaliInput");
  Q_CHECK_PTR(mInput);
  mInput->setWindowTitle(QString(tr("Low")+": %1").arg(windowTitle()));
  mInput->setTitle(tr("Low Value"));
  mInput->init(mMeasure, false);
  mInput->exec();
  refresh();
}

void CYManuCali::setDesignerValue()
{
  if(CYMessageBox::warningYesNo(this, tr("Do you really want to load the designer's values ?")))
  {
    if (mAna)
    {
      // Charges les valeurs constructeurs
      mAna->top()->designer();
      mAna->low()->designer();
      if (mAna->adc16())
      {
        mAna->adc16()->top()->designer();
        mAna->adc16()->low()->designer();
      }
      else if (mAna->adc32())
      {
        mAna->adc32()->top()->designer();
        mAna->adc32()->low()->designer();
      }
      else
        CYWARNINGTEXT(QString("CYManuCali::setDesignerValue()): %1").arg(objectName()));
      refresh();
      mAna->sendCal();
    }
    else if (mMeasure)
    {
      // Charges les valeurs constructeurs
      mMeasure->top()->designer();
      mMeasure->low()->designer();
      mMeasure->direct()->top()->designer();
      mMeasure->direct()->low()->designer();
      refresh();
      mMeasure->sendCal();
    }
  }
}

void CYManuCali::help()
{
  core->invokeHelp( "cydoc", "calibrage-manu" );
}
