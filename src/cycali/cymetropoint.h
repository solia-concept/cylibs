//
// C++ Interface: cymetropoint
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMETROPOINT_H
#define CYMETROPOINT_H

#include <qobject.h>
// CYLIBS
#include "cytypes.h"

class CYMeasure;

/**
@short Point d'étalonnage du module métrologie.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMetroPoint : public QObject
{
Q_OBJECT
public:
  CYMetroPoint(CYMeasure *parent);

  ~CYMetroPoint();

  bool operator==(const CYMetroPoint &) const;
  bool operator<(const CYMetroPoint &) const;

  /** @return l'incertitude en % du capteur du point. */
  virtual float uncertaintyPC();
  /** @return l'incertitude absolue en unité capteur du point. */
  virtual float uncertainty();
  /** @return l'incertitude min en unité capteur du point. */
  virtual float uncertaintyMin();
  /** @return l'incertitude max en unité capteur du point. */
  virtual float uncertaintyMax();
  /** @return l'erreur du capteur du banc / capteur étalon. */
  virtual float error();
  /** @return l'erreur du capteur du banc / capteur étalon sur la pleine échelle. */
  virtual float error_fs();
  /** @return la valeur brute théorique du capteur en fonction de \a gain et \a offset. */
  virtual float brut(float gain, float offset);
  /** @return l'erreur de linarité / à la valeur brute théorique du capteur. */
  virtual float error_linearity(float brut);
  /** @return la dernière erreur de linarité du capteur calculée. */
  virtual float error_linearity() { return err_lin; } 
  /** @return \A true en cas de mesure analogique. */
  bool isAna();

  double bench, ref, fs, bdir;
  f32 tmoy;
  float err_lin;

protected:
    CYMeasure *mMeasure;
};

#endif
