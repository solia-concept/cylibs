/***************************************************************************
                          cymanucaliinput.cpp  -  description
                             -------------------
    début                  : ven jun 27 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cymanucaliinput.h"
#include "ui_cymanucaliinput.h"

// CYLIBS
#include "cyana.h"
#include "cyadc16.h"
#include "cyadc32.h"
#include "cystring.h"
#include "cynumdisplay.h"
#include "cynuminput.h"
#include "cytextinput.h"

CYManuCaliInput::CYManuCaliInput(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYManuCaliInput)
{
  ui->setupUi(this);
}


CYManuCaliInput::~CYManuCaliInput()
{
  delete ui;
}

void CYManuCaliInput::init(CYMeasure *d, bool top)
{
  mMeasure = d;
  if (mMeasure->inherits("CYAna"))
  {
    CYAna* ana = qobject_cast<CYAna*>(mMeasure);

    if (ana->adc16())
    {
      if (top)
      {
        ui->calval->setDataName(ana->top()->dataName());
        ui->adcnew->setDataName(ana->adc16()->top()->dataName());
        ui->directval->setDataName(ana->adc16()->dataName());
      }
      else
      {
        ui->calval->setDataName(ana->low()->dataName());
        ui->adcnew->setDataName(ana->adc16()->low()->dataName());
        ui->directval->setDataName(ana->adc16()->dataName());
      }
    }
    if (ana->adc32())
    {
      if (top)
      {
        ui->calval->setDataName(ana->top()->dataName());
        ui->adcnew->setDataName(ana->adc32()->top()->dataName());
        ui->directval->setDataName(ana->adc32()->dataName());
      }
      else
      {
        ui->calval->setDataName(ana->low()->dataName());
        ui->adcnew->setDataName(ana->adc32()->low()->dataName());
        ui->directval->setDataName(ana->adc32()->dataName());
      }
    }
  }
  else if (mMeasure && mMeasure->direct())
  {
    if (top)
    {
      ui->calval->setDataName(mMeasure->top()->dataName());
      ui->adcnew->setDataName(mMeasure->direct()->top()->dataName());
      ui->directval->setDataName(mMeasure->direct()->dataName());
    }
    else
    {
      ui->calval->setDataName(mMeasure->low()->dataName());
      ui->adcnew->setDataName(mMeasure->direct()->low()->dataName());
      ui->directval->setDataName(mMeasure->direct()->dataName());
    }
  }

  mTimer->start(250); //  ms
}

void CYManuCaliInput::accept()
{
  CYDialog::accept();
}

void CYManuCaliInput::setTitle(const QString &title)
{
  ui->GroupBox1->setTitle(title);
}
