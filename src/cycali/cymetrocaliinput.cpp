//
// C++ Implementation: cymetrocaliinput
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// QT
#include <QPushButton>
// CYLIBS
#include "cymetrocaliinput.h"
#include "ui_cymetrocaliinput.h"
#include "cymeasure.h"
#include "cynumdisplay.h"
#include "cynuminput.h"
#include "cycore.h"
#include "cyadc16.h"
#include "cytextlabel.h"
#include "cytsec.h"

CYMetroCaliInput::CYMetroCaliInput(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYMetroCaliInput)
{
  ui->setupUi(this);
}


CYMetroCaliInput::~CYMetroCaliInput()
{
  delete ui;
}

void CYMetroCaliInput::init(CYMeasure *mes)
{
  mMeasure = mes;

  setWindowTitle(tr("Calibrating point '%1'").arg(mes->objectName()));
  ui->group->setTitle(tr("%1 : New point").arg(mes->label()));
  ui->newval->setDataName(QString("METRO_REF_%1").arg(mes->objectName()).toUtf8());
  if (mes->sensorCalAuto->val())
  {
    ui->current->setDataName(mes->dataName());

    ui->moy->setDataName(mes->metroMoyMES->dataName());
    ui->reste_label->setDataName(mes->metroMoyTime->dataName());
    ui->reste->setDataName(mes->metroMoyTime->dataName());
    ui->direct->setDataName(mes->metroMoyDIR->dataName());
    ui->direct_manual->hide();
    ui->direct_manual_label->hide();
  }
  else
  {
    ui->current->setDataName(mes->dataName());

    CYF32 *bdir = (CYF32 *)core->findData(QString("METRO_BDIR_%1").arg(mMeasure->objectName()));
    ui->direct_manual->setDataName(bdir->dataName());
    ui->moy->hide();
    ui->reste_label->hide();
    ui->reste->hide();
    ui->direct->hide();
    ui->direct_label->hide();
    ui->moy_label->hide();
  }
  if (mMeasure->inherits("CYAna"))
    ui->direct_label->setText(tr("ADC"));

  linkDatas();
  if (mes->top()->def() > mes->low()->def())
  {
    ((CYF32 *)ui->newval->CYDataWdg::data())->setMax(mes->EMtop()->max()); //min/max spinbox
    ((CYF32 *)ui->newval->CYDataWdg::data())->setMin(mes->EMlow()->min());
  }
  else
  {
    ((CYF32 *)ui->newval->CYDataWdg::data())->setMax(mes->EMlow()->max());
    ((CYF32 *)ui->newval->CYDataWdg::data())->setMin(mes->EMtop()->min());
  }
  mTimer->start(250); //  ms
}

void CYMetroCaliInput::apply()
{
  CYDialog::apply();

  if (mMeasure->sensorCalAuto->val())
  {
    CYF32 *bdir = (CYF32 *)core->findData(QString("METRO_BDIR_%1").arg(mMeasure->objectName()));
    bdir->setVal(ui->direct->value());

    CYF32 *bench = (CYF32 *)core->findData(QString("METRO_BENCH_%1").arg(mMeasure->objectName()));
    bench->setVal(ui->moy->value());
  }
}

void CYMetroCaliInput::refresh()
{
  CYDialog::refresh();
  ui->pbOk->setEnabled(core->simulation() || mMeasure->metroMoyOk() || !mMeasure->sensorCalAuto->val());
}
