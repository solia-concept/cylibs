//
// C++ Interface: cymetrolist
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMETROLIST_H
#define CYMETROLIST_H

// QT
#include <QTreeWidget>

class CYMetroWin;

/**
@short Boite de métrologie listant les capteurs analogiques et numériques.
  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMetroList : public QTreeWidget
{
  Q_OBJECT
public:
  /** Filtre sur d'affichage de mesures. */
  enum Filter
  {
    /** Affiche les mesures à contrôler. */
    Ctrl,
    /** Affiche les mesures sans contrôle. */
    NotCtrl,
    /** Affiche les données. */
    All,
  };

  CYMetroList(CYMetroWin *win, QWidget *parent = 0, const QString &name = 0);

  ~CYMetroList();

  void setSelected( QTreeWidgetItem * item, bool selected );
  void selectMeasure( QString dataName );

public slots:
  void changeMeasure(QTreeWidgetItem *item, QTreeWidgetItem *previous);

protected:
  CYMetroWin *mWin;
};

#endif
