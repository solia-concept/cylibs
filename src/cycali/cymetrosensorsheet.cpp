//
// C++ Implementation: cymetrosensorsheet
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// CYLIBS
#include "cymetrosensorsheet.h"
#include "ui_cymetrosensorsheet.h"
#include "cymetrosensorsheetview.h"
#include "cymeasure.h"
#include "cycore.h"
#include "cymessagebox.h"

CYMetroSensorSheet::CYMetroSensorSheet(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYMetroSensorSheet)
{
  ui->setupUi(this);
  setGenericMark(name.toUtf8());
  ui->metroSensorSheetView->setGenericMark(name.toUtf8());
  CYMeasure *mes = (CYMeasure *)core->findData(name);
  setWindowTitle(tr("Edit sensor sheet '%1'").arg(mes->objectName()));
}


CYMetroSensorSheet::~CYMetroSensorSheet()
{
  delete ui;
}


void CYMetroSensorSheet::accept()
{
/*  if (metroSensorSheetWdg->maxi->value()<metroSensorSheetWdg->mini->value())
  {
    CYWARNINGTEXT(tr("The maximum of measuring range should be higher than minimum !"));
    return;
  } a_voir */
  CYMeasure *mes = (CYMeasure *)core->findData(objectName());

  QString msg = QString(tr("Are you sure to validate this sensor sheet ?\n"
                             "If yes, it is recommended to redo the calibration !"));

  if (CYMessageBox::Yes == CYMessageBox::warningYesNo(this, msg, tr("Change sensor sheet '%1'").arg(mes->objectName())))
  {
    CYDialog::accept();
    mes->setCalibrateNext();
  }
}
