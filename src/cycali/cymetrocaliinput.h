//
// C++ Interface: cymetrocaliinput
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMETROCALIINPUT_H
#define CYMETROCALIINPUT_H

#include "cydialog.h"

class CYMeasure;

namespace Ui {
		class CYMetroCaliInput;
}

/**
@short Boite de saisie d'un point d'étalonnage.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMetroCaliInput : public CYDialog
{
Q_OBJECT
public:
    CYMetroCaliInput(QWidget *parent = 0, const QString &name = 0);

    ~CYMetroCaliInput();

    /** Initialise la boîte avec la mesure \a mes. */
    virtual void init(CYMeasure *mes);

public slots: // Public slots
  /** Applique les données. */
  virtual void apply();
  /** Rafraîchit la vue. */
  virtual void refresh();

protected:
  CYMeasure *mMeasure;

private:
    Ui::CYMetroCaliInput *ui;
};

#endif
