//
// C++ Interface: cymetrowin
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMETROWIN_H
#define CYMETROWIN_H

// QT
#include <QSplitter>
#include <QComboBox>
#include <QTreeWidget>
// CYLIBS
#include <cywin.h>

class CYMetroList;
class CYMetroCalibrate;
class CYMeasure;

/** @short Fenêtre de métrologie.
  * Dans cette fenêtre il est possible de sélectionner une mesure et
  * d'en faire un étalonnage ou une vérificattion.
  * @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
  */
class CYMetroWin : public CYWin
{
  Q_OBJECT
public:
  /** Filtre sur d'affichage de mesures. */
  enum Filter
  {
    /** Affiche les mesures à contrôler. */
    Ctrl,
    /** Affiche les mesures sans contrôle. */
    NotCtrl,
    /** Affiche les données. */
    All,
  };
  /** Construction de la fenêtre d'affichage des journaux d'évènements.
    * @param name     Nom de la fenêtre.
    * @param index    Index de la fenêtre.
    * @param label    Libellé de la fenêtre. */
  CYMetroWin(const QString &name, int index, const QString &label, bool multiple=false);

  ~CYMetroWin();

  virtual void createGUI(const QString &xmlfile="cymetrowinui.rc");

  /** @return la mesure sélectionnée*/
  CYMeasure *measure();

  /** @return \a true si la fenêtre gère du multi-voies. */
  bool isMultiple() { return mMultiple; }

  virtual bool queryClose();

  /** @return \a true si en cours d'édition .*/
  bool editing();
  virtual bool cancel();

public slots:
  /** Initialise le widget avec la mesure ayant pour nom \a mesName. */
  virtual void initWithMeasure(const QString &name);
  /** Edite la feuille capteur du capteur sélectionné. */
  virtual void sensorSheet();
  /** Ouvre la boîte de configuration de l'outil de métrologie. */
  virtual void settings();
  /** Met à jour l'affichage de la liste des mesures en fonction du filtre*/
  void changeFilter(int);
  /** Saisie l'état d'édition d'un capteur. */
  void setEditing(CYMeasure *mes, bool editing);
  /** Mémorise l'édition en multi-voies. */
  virtual void memEditing();

protected: // Protected methods
  /** Fonction appelée à l'affichage de la fenêtre. */
  virtual void showEvent(QShowEvent *e);

protected: // Protected attributes
  /** Splitter entre le CYAcquisitionBrowser et le CYAcquisitionWidget. */
  QSplitter *mSplitter;
  /** Liste de mesures. */
  CYMetroList *mList;
  /** Widget d'acquisition. */
  CYMetroCalibrate *mCalibrate;
  /** Elément sélectionné dans la liste .*/
  QTreeWidgetItem *mCurrentItem;

  CYAction *actionQuit;
  CYAction *actionCap;
  CYAction *actionRaz;
  CYAction *actionAdd;
  CYAction *actionDel;
  CYAction *actionCal;
  CYAction *actionVer;
  CYAction *actionDes;
  CYAction *actionSet;

  bool mEditing;
  bool mDesigner;
  bool mMultiple;

  /** Boîte de sélection du type de données. */
  QComboBox *mFilterBox;
};

#endif
