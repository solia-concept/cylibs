/***************************************************************************
                          cylistcali.h  -  description
                             -------------------
    début                  : jeu avr 3 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYLISTCALI_H
#define CYLISTCALI_H

// QT
#include <QDialog>
#include <QVariant>
#include <QLayout>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QGridLayout>
// CYLIBS
#include <cyautocali.h>
#include <cymanucali.h>

/** CYListCali liste tous les entrées/sorties pouvant être calibrer. Le calibrage
  * d'un capteur est rendu possible dès que l'élément dans la liste correspondant
  * à celui-ci est activé par un click de souris ou une pression sur la touche 
  * ENTER du clavier.
  * @short Liste les capteurs à calibrer.
  * @author Gérald LE CLEACH
  */

class CYListCali : public QDialog
{
  Q_OBJECT

public:
  CYListCali(QWidget *parent=0, const QString &name=0, Qt::WindowFlags f = Qt::WindowFlags());
  ~CYListCali();

  QTreeWidget*list;

public slots:
  /** Demande le calibrage du capteur \a sensor sélectionné dans la liste. */
  virtual void calibrate(QTreeWidgetItem * item, int column) { Q_UNUSED(item) Q_UNUSED(column) }

protected:
  CYAutoCali  *autoDlg;
  CYManuCali  *manuDlg;
  QWidget     *edit;
  QGridLayout *grid;
};

#endif
