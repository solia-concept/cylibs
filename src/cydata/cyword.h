/***************************************************************************
                          cyword.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYWORD_H
#define CYWORD_H

// CYLIBS
#include "cytype.h"

/** CYWord est un mot de 16 bits.
  * @short Mot de 16 bits.
  * @author Gérald LE CLEACH
  */

class CYWord : public CYType<u16>
{
  Q_OBJECT

public:
  /** Création d'un mot de 16 bits.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Pointe sur la valeur physique.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYWord(CYDB *db, const QString &name, u16 *val, u16 def=0, int format=0, Mode mode=Sys);

  ~CYWord();

  /** @return l'état du \a bit. */
  bool bit(const unsigned short bit);

   /** @return la valeur \a val en chaîne de caractères.
     * @param binary En mode binaire syute de 0 et 1, sinon en décimale. */
  virtual QString toString(bool binary=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

public slots: // Public slots
  /** Saisie la valeur du \a bit dans le buffer du mot. */
  void setBit(const unsigned short bit, const bool val=false);
};

#endif
