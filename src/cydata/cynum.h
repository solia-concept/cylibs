/***************************************************************************
                          cynum.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYNUM_H
#define CYNUM_H

// GNU C
#include <limits.h>
#include <math.h>
// QT
#include <QQueue>
#include <QMap>
// CYLIBS
#include "cymaths.h"
#include "cytype.h"
#include "cydb.h"
#include "cyformat.h"

/**
 * CYNum est une donnée numérique.
 * @short Donnée numérique.
 * @author Gérald LE CLEACH
 */

template<class T>
class CYNum : public CYType<T>
{
public:
  /** Attr1 est une structure de saisie simple des attributs d'une donnée numérique. */
  template<class T1>
  class Attr1
  {
  public:
    /** Nom. */
    const QString &name;
    /** Valeur de la donnée. */
    T1 *val;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
  };

  /** Attr2 est une structure de saisie simple des attributs d'une donnée numérique
    * avec saisie de la description. */
  template<class T2>
  class Attr2
  {
  public:
    /** Nom. */
    const QString &name;
    /** Valeur de la donnée. */
    T2 *val;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Etiquette. */
    QString label;
  };

  /** Attr3 est une structure de saisie des attributs d'une donnée numérique avec
    * saisie de la description et de la valeur par défaut. */
  template<class T3>
  class Attr3
  {
  public:
    /** Nom. */
    const QString &name;
    /** Valeur de la donnée. */
    T3 *val;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Valeur par défaut. */
    T3 def;
    /** Etiquette. */
    QString label;
  };

  /** Attr4 est une structure de saisie des attributs d'une donnée numérique avec
    * saisie de la description et des valeurs par défaut, mini, maxi. */
  template<class T4>
  class Attr4
  {
  public:
    /** Nom. */
    const QString &name;
    /** Valeur de la donnée. */
    T4 *val;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Valeur par défaut. */
    T4 def;
    /** Valeur mini. */
    T4 min;
    /** Valeur maxi. */
    T4 max;
    /** Etiquette. */
    QString label;
  };

  /** Structure de saisie simple des attributs d'une donnée numérique référencée par un index.
    * L'index permet d'utiliser le même tableau de saisie pour plusieurs données de même index.
    * Le pointeur de la valeur courante de chacune des données est saisie dans le constructeur
    * au moyen d'une macro et de l'index. */
  template<class T1>
  class AttrIndex1
  {
  public:
    /** Nom. */
    const QString &name;
    /** Index de la donnée. */
    int index;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
  };

  /** Structure de saisie simple des attributs d'une donnée numérique référencée par un index
    * avec saisie de la description.
    * L'index permet d'utiliser le même tableau de saisie pour plusieurs données de même index.
    * Le pointeur de la valeur courante de chacune des données est saisie dans le constructeur
    * au moyen d'une macro et de l'index. */
  template<class T2>
  class AttrIndex2
  {
  public:
    /** Nom. */
    const QString &name;
    /** Index de la donnée. */
    int index;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Etiquette. */
    QString label;
  };

  /** Structure de saisie des attributs d'une donnée numérique référencée par un index avec
    * saisie de la description et de la valeur par défaut.
    * L'index permet d'utiliser le même tableau de saisie pour plusieurs données de même index.
    * Le pointeur de la valeur courante de chacune des données est saisie dans le constructeur
    * au moyen d'une macro et de l'index. */
  template<class T3>
  class AttrIndex3
  {
  public:
    /** Nom. */
    const QString &name;
    /** Index de la donnée. */
    int index;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Valeur par défaut. */
    T3 def;
    /** Etiquette. */
    QString label;
  };

  /** Attr4 est une structure de saisie des attributs d'une donnée numérique référencée par un index avec
    * saisie de la description et des valeurs par défaut, mini, maxi.
    * L'index permet d'utiliser le même tableau de saisie pour plusieurs données de même index.
    * Le pointeur de la valeur courante de chacune des données est saisie dans le constructeur
    * au moyen d'une macro et de l'index. */
  template<class T4>
  class AttrIndex4
  {
  public:
    /** Nom. */
    const QString &name;
    /** Index de la donnée. */
    int index;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Valeur par défaut. */
    T4 def;
    /** Valeur mini. */
    T4 min;
    /** Valeur maxi. */
    T4 max;
    /** Etiquette. */
    QString label;
  };

  /** Création d'une donnée numérique.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation.
    * @param type   Type de valeur. */
  CYNum(CYDB *db, const QString &name, int format=0, Cy::Mode m=Cy::Sys, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec sa valeur.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation.
    * @param type   Type de valeur. */
  CYNum(CYDB *db, const QString &name, T *val, int format=0, Cy::Mode m=Cy::Sys, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec sa valeur ainsi que sa valeur par défaut.
    * @param db    Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation.
    * @param type   Type de valeur. */
  CYNum(CYDB *db, const QString &name, T *val, const T def, int format=0, Cy::Mode mode=Cy::Sys, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation.
    * @param type   Type de valeur. */
  CYNum(CYDB *db, const QString &name, T *val, const T def, const T min, const T max, int format=0, Cy::Mode mode=Cy::Sys, Cy::ValueType type=Cy::Undef);

  /** Création d'une donnée numérique avec les attributs Attr1.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param type  Type de valeur. */
  CYNum(CYDB *db, const QString &name, Attr1<T> *attr, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec les attributs Attr2.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param type  Type de valeur. */
  CYNum(CYDB *db, const QString &name, Attr2<T> *attr, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec les attributs Attr3.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param type  Type de valeur. */
  CYNum(CYDB *db, const QString &name, Attr3<T> *attr, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec les attributs Attr3.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param type  Type de valeur. */
  CYNum(CYDB *db, const QString &name, Attr4<T> *attr, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec les attributs AttrIndex1.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param attr  Attributs de la donnée.
    * @param type  Type de valeur. */
  CYNum(CYDB *db, const QString &name, T *val, AttrIndex1<T> *attr, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec les attributs AttrIndex2.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param attr  Attributs de la donnée.
    * @param type  Type de valeur. */
  CYNum(CYDB *db, const QString &name, T *val, AttrIndex2<T> *attr, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec les attributs AttrIndex3.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param attr  Attributs de la donnée.
    * @param type  Type de valeur. */
  CYNum(CYDB *db, const QString &name, T *val, AttrIndex3<T> *attr, Cy::ValueType t=Cy::Undef);

  /** Création d'une donnée numérique avec les attributs AttrIndex3.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param attr  Attributs de la donnée.
    * @param type  Type de valeur. */
  CYNum(CYDB *db, const QString &name, T *val, AttrIndex4<T> *attr, Cy::ValueType t=Cy::Undef);

  ~CYNum();

  /** Initialise les min max lorsqu'ils ne sont pas saisis. */
  virtual void initMinMax();

  /** @return la valeur arrondie. */
  virtual T val();
  /** @return la valeur sans arrondie. */
  virtual T real();
  /** @return la valeur maxi. */
  virtual T max();
  /** @return la valeur mini. */
  virtual T min();
  /** @return la valeur par défaut du constructeur. */
  virtual T def();

  /** Saisie une valeur. */
  virtual void setVal(T val);
  /**
   * @brief Import contrôlé d'une valeur.
   * @param val Valeur à charger
   * En cas de dépassement d'une limite de la donnée, la valeur à charger
   * est alors forcée à la valeur de la limite dépassée.
   */
  virtual void import(T val);
  /** Saisie la valeur maxi. */
  virtual void setMax(T max);
  /** Saisie la valeur mini. */
  virtual void setMin(T min);
  /** Saisie la valeur par défaut du constructeur. */
  virtual void setDef(T def);
  /** Vérifie la valeur par défaut du constructeur par rarpport aux limites. */
  virtual void checkDef();

  /** @return le nombre de chiffres après la virgule. */
  virtual int nbDec();

  /** @return le préfixe courant. Celui-ci est définie en fonction de la section en cours
    * de traîtement. */
  virtual QString prefix();

  /** @return le suffixe courant. Celui-ci est définie en fonction de la section en cours
    * de traîtement. */
  virtual QString suffix();

  /** @return la section de la donnée exponentielle en traîtement. */
  CYData::Section section();
  /** Saisie la section de la donnée exponentielle à traîter. */
  void setSection(CYData::Section section);
  /** Initialise à la section maxi de traîtement de la donnée exponentielle. */
  void initToSectionMaxi();
  /** Initialise à la section mini de traîtement de la donnée exponentielle. */
  void initToSectionMini();
  /** @return \a true si la section courrante est la section maxi de traîtement de la donnée exponentielle. */
  bool isSectionMaxi();
  /** @return \a true si la section courrante est la section mini de traîtement de la donnée exponentielle. */
  bool isSectionMini();
  /** Passe à la section maxi de traîtement de la donnée exponentielle.
    * @param val Mise à jour de la valeur de la section courante avant changement de section. */
  bool sectionMaxi(const double val);
  /** Passe à la section mini de traîtement de la donnée exponentielle.
    * @param val Mise à jour de la valeur de la section courante avant changement de section. */
  bool sectionMini(const double val);
  /** Passe à la section suppérieure de traîtement de la donnée exponentielle.
    * @param val Mise à jour de la valeur de la section courante avant changement de section. */
  bool sectionUp(const double val);
  /** Passe à la section inférieure de traîtement de la donnée exponentielle.
    * @param val Mise à jour de la valeur de la section courante avant changement de section. */
  bool sectionDown(const double val);
  /** Passe à la section de traîtement de la donnée exponentielle située à droite du texte \a left. */
  bool section(QString left);

  /** Saisie une valeur de section de donnée exponentielle en cours de traîtement. */
  void setValSection(const double value);
  /** @return la valeur de la section de donnée exponentielle en cours de traîtement. */
  double valSection();
  /** @return la valeur par défaut du constructeur de la section de donnée exponentielle en cours de traîtement. */
  double defSection();
  /** @return la valeur mini de la section de donnée exponentielle en cours de traîtement. */
  double minSection();
  /** @return la valeur mini de la section de donnée exponentielle en cours de traîtement. */
  double maxSection();
  /** @return la précision de la section de donnée exponentielle en cours de traîtement. */
  double precisionSection();

  /** @return la valeur temporaire. */
  virtual double tmp();
  /** Saisie une valeur temporaire. */
  virtual void setTmp(const double val);

  /** Charge la valeur constructeur. */
  virtual void designer();

  /** @return la valeur courrante en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(bool withUnit=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);
  /** @return la valeur \a val en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(double val, bool withUnit, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

  /** @return l'aide à la visualisation.
    * @param whole    Aide entière ex: pour les données numériques avec les bornes, la précision si il y en a une et la valeur par défaut.
    * @param oneLine  Aide uniquement composée de la première ligne.
    * @param withGroup  Aide avec le groupe de la donnée.
    * @param withValue  Aide avec la valeur courante. */
  virtual QString displayHelp(bool whole=true, bool oneLine=false, bool withGroup=true, bool withValue=false);
  /** @return l'aide à la saisie.
    * @param whole      Aide entière ex: pour les données numériques avec les bornes, la précision si il y en a une.
    * @param withGroup  Aide avec le groupe de la donnée. */
  virtual QString inputHelp(bool whole=true, bool withGroup=true);

  /** Vérifie si la donnée respecte les limites de la données. */
  virtual bool isValid();

  /** @return la mantisse de la valeur temporaire. */
  double mantissa();
  /** @return l'exposant de la valeur temporaire. */
  int exponent();

  /** @return la mantisse de la valeur \a val. */
  double mantissa(const double val);
  /** Saisie la mantisse. */
  void setMantissa(const double val);
  /** @return l'exposant de la valeur \a val. */
  int exponent(const double val);
  /** Saisie l'exposant. */
  void setExponent(const int val);

  /** Ajoute dans le buffer les \nb valeurs nouvellement lues (nombre de structures lues). */
  virtual void buffering(int nb);
  /** Bufferisation en trigger */
  virtual void bufferingTrigger(T val);

  /** @return l'échantillon d'index \a i dans la structure \a str_fifo. */
  T sample(int str_fifo=0, int i=0);
  /** Saisie la valeur \a val pour l'échantillon d'index \a i dans la structure \a str_fifo. */
  T setSampleVal(T val, int str_fifo=0, int i=0);
  /** Saisie l'offset pour retrouver l'échantillon en fonction du dernier lu par le module. */
  virtual void setSampleOffsetStruct( int offset) { mSampleOffsetStruct = offset; }
  /** @return l'offset pour retrouver l'échantillon en fonction du dernier lu par le module. */
  virtual int sampleOffsetStruct() { return mSampleOffsetStruct; }

  /** Saisie la précision.
    * @param precision  : 0.2, 1.0, 5.0, 10.0, 1, 5, 10 etc... */
  virtual void setPrecision(const double precision);

  /** Calcule le nombre de chiffres. */
  virtual void calculNbDigits();

  /** @return le nombre de chiffres total. */
  virtual int nbDigit(Cy::NumBase base=Cy::BaseData) { Q_UNUSED(base) return mNbDigit; }
//  virtual int nbDigit(Cy::NumBase base=Cy::BaseData) {  return CYNum<T>::toString(CYNum<T>::max(), false, true, base).length(); }
  /** Saisie le nombre de chiffres total. */
  virtual void setNbDigit(int nb);
  /** @return la chaîne de caractères d'affichage de défaut. */
  virtual QString stringDef() { return mStringDef; }
  /** Saisie la chaîne de caractères d'affichage de défaut.
      Saisir 0 pour annuler cette gestion. */
  virtual void setStringDef(QString txt) { mStringDef = txt; }

  /** @return une ligne donnée pour le fichier d'export des données avec les
    * champs suivants espacés d'une tabulation:
    * Hôte(s), connexion(s), groupe, nom, description, valeur, def, min, max, aide.
    * @see CYDB::exportDatas() */
  virtual QString exportData();

  virtual void init();
  virtual void initFIFO();
  /** Activation ou non de la gestion de FIFO. */
  virtual void setEnableFIFO(bool val);
  /** @return \a true si la gestion de FIFO est active. */
  bool enableFIFO() { return mEnableFIFO; }
  /** Empile la valeur \a val dans la FIFO en lecture ou la première libre. */
  virtual void enqueueFIFO(T val);
  /** Dépile une valeur de la FIFO à lire puis la libère si elle devient vide.
    * @return la valeur dépilée si \a ok est à true. */
  virtual T dequeueFIFO(bool &ok);

protected: // Protected methods
  /** Calcule les valeurs des différentes sections. */
  void sections();

protected: // Protected attributes
  int mSizeStruct;
  int mSizeType;
  /** Offset pour retrouver l'échantillon en fonction du dernier lu par le module. */
  int mSampleOffsetStruct;

  /** Valeur mini. */
  T mMin;
  /** Valeur maxi. */
  T mMax;
  /** Nombre de chiffres total. */
  int mNbDigit;
  /** Chaîne de caractères d'affichage de défaut. */
  QString mStringDef;

  /** Section de la donnée exponentielle en cours de traîtement. */
  CYData::Section mSection;
  /** Mantisse. */
  double mMantissa;
  /** Exposant. */
  int mExponent;

  /** Activation ou non des FIFO. */
  bool mEnableFIFO;
  QQueue<T> mFIFO;
};

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, int format, Cy::Mode mode, Cy::ValueType type)
  : CYType<T>(db, name, format, mode, type)
{
  CYType<T>::mVal = new T;
  initMinMax();
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);
  mSection = CYType<T>::Mantissa;

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = CYNum<T>::val();
  calculNbDigits();


  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, T *val, int format, Cy::Mode mode, Cy::ValueType type)
  : CYType<T>(db, name, format, mode, type)
{
  CYType<T>::mDef  = 0;
  CYType<T>::mVal  = val;
  *CYType<T>::mVal = CYType<T>::mDef;
  mSection = CYType<T>::Mantissa;

  initMinMax();
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = CYNum<T>::val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, T *val, const T def, int format, Cy::Mode mode, Cy::ValueType type)
  : CYType<T>(db, name, format, mode, type)
{
  CYType<T>::mDef  = def;
  CYType<T>::mVal  = val;
  *CYType<T>::mVal = CYType<T>::mDef;
  mSection = CYType<T>::Mantissa;

  initMinMax();
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = CYNum<T>::val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, T *val, const T def, const T min, const T max, int format, Cy::Mode mode, Cy::ValueType type)
  : CYType<T>(db, name, format, mode, type)
{
  CYType<T>::mDef = def;
  mMin = min;
  mMax = max;
  mSection = CYType<T>::Mantissa;

  if (mMin > mMax)
    CYFATALTEXT(QString("min > max : %1").arg(name));

  if (CYType<T>::mDef < mMin)
    CYFATALTEXT(QString("def < min : %1").arg(name));

  if (CYType<T>::mDef > mMax)
    CYFATALTEXT(QString("def > max : %1").arg(name));

  CYType<T>::mVal  = val;
  *CYType<T>::mVal = CYType<T>::mDef;

  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = CYNum<T>::val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, Attr1<T> *attr, Cy::ValueType type)
  : CYType<T>(db, name, attr->format, attr->mode, type)
{
  CYType<T>::mVal   = attr->val;
  mSection = CYType<T>::Mantissa;

  initMinMax();
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, Attr2<T> *attr, Cy::ValueType type)
  : CYType<T>(db, name, attr->format, attr->mode, type)
{
  CYType<T>::mVal   = attr->val;
  CYType<T>::mLabel = attr->label;
  mSection = CYType<T>::Mantissa;

  initMinMax();
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, Attr3<T> *attr, Cy::ValueType type)
  : CYType<T>(db, name, attr->format, attr->mode, type)
{
  CYType<T>::mDef   = attr->def;
  CYType<T>::mVal   = attr->val;
  CYType<T>::mLabel = attr->label;
  mSection = CYType<T>::Mantissa;

  initMinMax();
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, Attr4<T> *attr, Cy::ValueType type)
  : CYType<T>(db, name, attr->format, attr->mode, type)
{
  CYType<T>::mDef   = attr->def;
  CYType<T>::mVal   = attr->val;
  mSection = CYType<T>::Mantissa;

  mMin   = attr->min;
  mMax   = attr->max;
  CYType<T>::mLabel = attr->label;
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, T *val, AttrIndex1<T> *attr, Cy::ValueType type)
  : CYType<T>(db, name, attr->format, attr->mode, type)
{
  CYType<T>::mVal   = val;
  mSection = CYType<T>::Mantissa;

  initMinMax();
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, T *val, AttrIndex2<T> *attr, Cy::ValueType type)
  : CYType<T>(db, name, attr->format, attr->mode, type)
{
  CYType<T>::mVal   = val;
  CYType<T>::mLabel = attr->label;
  mSection = CYType<T>::Mantissa;

  initMinMax();
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, T *val, AttrIndex3<T> *attr, Cy::ValueType type)
  : CYType<T>(db, name, attr->format, attr->mode, type)
{
  CYType<T>::mDef   = attr->def;
  CYType<T>::mVal   = val;
  CYType<T>::mLabel = attr->label;
  mSection = CYType<T>::Mantissa;

  initMinMax();
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::CYNum(CYDB *db, const QString &name, T *val, AttrIndex4<T> *attr, Cy::ValueType type)
  : CYType<T>(db, name, attr->format, attr->mode, type)
{
  CYType<T>::mDef   = attr->def;
  CYType<T>::mVal   = val;
  mMin   = attr->min;
  mMax   = attr->max;
  CYType<T>::mLabel = attr->label;
  mSizeStruct = db->sizeStruct();
  mSampleOffsetStruct = mSizeStruct;
  mSizeType   = sizeof(T);
  mSection = CYType<T>::Mantissa;

  if (CYType<T>::isReadFast())
    CYType<T>::mUseBuffer = true;

  CYType<T>::mOldVal = val();
  calculNbDigits();

  setVal(cyRound(*CYType<T>::mVal, CYType<T>::precision()));
  setTmp(*CYType<T>::mVal);
  initFIFO();
}

template<class T>
inline CYNum<T>::~CYNum()
{
}

template<class T>
inline void CYNum<T>::init()
{
}

template<class T>
inline void CYNum<T>::initMinMax()
{
  CYType<T>::mAutoMinMax = true;

  switch (CYType<T>::mType)
  {
  case Cy::VFL    :
    mMin = 0;
    mMax = 1;
    break;
  case Cy::VU8    :
    mMin = 0;
    mMax = UINT8_MAX;
    break;
  case Cy::VS8    :
    mMin = INT8_MIN;
    mMax = INT8_MAX;
    break;
  case Cy::VU16   :
    mMin = 0;
    mMax = (T)UINT16_MAX;
    break;
  case Cy::VS16   :
    mMin = (T)INT16_MIN;
    mMax = (T)INT16_MAX;
    break;
  case Cy::VU32   :
    mMin = 0;
    mMax = (T)UINT32_MAX;
    break;
  case Cy::VS32   :
    mMin = (T)INT32_MIN;
    mMax = (T)INT32_MAX;
    break;
  case Cy::VU64   :
    mMin = 0;
    mMax = (T)UINT64_MAX;
    break;
  case Cy::VS64   :
    mMin = (T)INT64_MIN;
    mMax = (T)INT64_MAX;
    break;
  case Cy::VF32   :
  case Cy::VF64   : mMin = (-1e10)/(pow(10, nbDec()));
    mMax = (1e10)/(pow(10, nbDec()));
    break;
  default     :     CYFATALDATA(CYType<T>::objectName());
  }
  if (!CYType<T>::mIsSigned)
    mMin = 0;

  // Bornes les min max calculés par rapport au format
  if ( CYData::autoMinMaxFormat() )
  {
    if (mMin<CYType<T>::mFormat->min())
      mMin = (T)CYType<T>::mFormat->min();
    if (mMax>CYType<T>::mFormat->max())
      mMax = (T)CYType<T>::mFormat->max();
  }
  CYData::setSimulMin(min());
  CYData::setSimulMax(max());
}

template<class T>
inline T CYNum<T>::val()
{
  if (CYType<T>::mVal==0)
    return 0;
  return (T)cyRound((*CYType<T>::mVal), CYType<T>::precision());
}

template<class T>
inline T CYNum<T>::real() { return *CYType<T>::mVal; }

template<class T>
inline T CYNum<T>::max() { return cyRoundInf(mMax, CYType<T>::precision()); }

template<class T>
inline T CYNum<T>::min() { return cyRoundSup(mMin, CYType<T>::precision()); }

template<class T>
inline T CYNum<T>::def() { return cyRound(CYType<T>::mDef, CYType<T>::precision()); }

template<class T>
inline void CYNum<T>::setVal(T val) { CYType<T>::setVal(val); }

template<class T>
inline void CYNum<T>::import(T val)
{
  if (val>CYNum<T>::mMax)
    CYType<T>::setVal(CYNum<T>::mMax);
  else if (val<CYNum<T>::mMin)
    CYType<T>::setVal(CYNum<T>::mMin);
  else
    CYType<T>::setVal(val);
}

template<class T>
inline void CYNum<T>::setMax(T max) { mMax = max; calculNbDigits(); }

template<class T>
inline void CYNum<T>::setMin(T min) { mMin = min; calculNbDigits(); }

template<class T>
inline void CYNum<T>::setDef(T def)
{
  CYType<T>::mDef = def;

  if (CYType<T>::def()>max() || CYType<T>::def()<min())
    CYType<T>::setDataCheckDef();
}

template<class T>
inline void CYNum<T>::checkDef()
{
  if (CYType<T>::def()>max())
    CYWARNINGTEXT(QObject::tr("Designer value of %1:%2 is higher than maximum value (%3>%4) !").arg(CYType<T>::db()->objectName()).arg(CYType<T>::objectName()).arg(CYType<T>::def()).arg(max()));
  if (CYType<T>::def()<min())
    CYWARNINGTEXT(QObject::tr("Designer value of %1:%2 is smaller than minimum value (%3<%4) !").arg(CYType<T>::db()->objectName()).arg(CYType<T>::objectName()).arg(CYType<T>::def()).arg(min()));
}

template<class T>
inline double CYNum<T>::tmp()
{
  return CYType<T>::mTmp;
}

template<class T>
inline void CYNum<T>::setTmp(const double val)
{
  CYType<T>::mTmp = val;
  sections();
}

template<class T>
inline void CYNum<T>::designer()
{
  setVal(CYType<T>::mDef);
  CYType<T>::mTmp = (double)(*CYType<T>::mVal);
}

template<class T>
inline void CYNum<T>::buffering(int nb)
{
  if (!CYType<T>::mUseBuffer || CYType<T>::mManualBuffer)
    return;

  if (!CYType<T>::isEnabled())
    return;

  T val;
  if (!CYType<T>::isReadFast())
  {
    // communication non-synchronisée: bufferisation de la valeur courante suivant timer superviseur
    int nbBufferingAsyncCom = CYType<T>::nbBufferingAsyncCom();
    if (!nbBufferingAsyncCom)
      return;// ne rien faire si le temps de bufferiser n'est pas atteint

    if (CYData::simulation() && CYData::simulType()!=CYData::SimulType::None)
      val = (T)CYType<T>::bufferSimulation(CYType<T>::mIdBuffer);
    else
      val = *CYType<T>::mVal;
    setVal(val);

    // Compensation du timer si nbBufferingAsyncCom > 1 par recopie de la valeur courante sur les écnantillons perdus
    for (int cpt=0; cpt<nbBufferingAsyncCom; cpt++)
    {
      // Attention: l'exécution de cette boucle donne des courbes non temps réel sous Windows
      // si elle est se trouve comme anciennement de manière générique avec la communication synchronisée.
      if (CYType<T>::mIdBuffer==0)
        CYType<T>::mIdBuffer = (CYType<T>::mBufferSize/2) -1;
      else
        CYType<T>::mIdBuffer--;
      if (CYType<T>::mIdBuffer==0)
        CYType<T>::mBufferFull=true;

      CYType<T>::mBuffer[CYType<T>::mIdBuffer] = val;
      CYType<T>::mBuffer[CYType<T>::mIdBuffer + (CYType<T>::mBufferSize/2)] = val;
    }
    bufferingTrigger(val);
    enqueueFIFO(val);
  }
  else
  {
    // communication synchronisée: bufferisation des valeurs transmises par FIFO et écantillons
    for (int str_fifo=0; str_fifo<nb; str_fifo++)
    {
      for(uint i=0; i<CYType<T>::mSampleSize; i++)
      {
        if (CYData::simulation() && CYData::simulType()!=CYData::SimulType::None)
        {
          val = (T)CYType<T>::bufferSimulation(CYType<T>::mIdBuffer);
#if defined(Q_OS_LINUX) && defined(QT_DEBUG)
          setSampleVal(val, str_fifo, i);
#endif
        }
        else
          val = sample(str_fifo, i);

        if (CYType<T>::burstsBufferEnable(str_fifo, i))
        {
          enqueueFIFO(val);

          if (CYType<T>::mIdBurstsBuffer==0)
            CYType<T>::mIdBurstsBuffer = (CYType<T>::mBurstsBufferSize/2) -1;
          else
            CYType<T>::mIdBurstsBuffer--;
          if (CYType<T>::mIdBurstsBuffer==0)
            CYType<T>::mBurstsBufferFull=true;

          CYType<T>::mBurstsBuffer[CYType<T>::mIdBurstsBuffer] = val;
          CYType<T>::mBurstsBuffer[CYType<T>::mIdBurstsBuffer + (CYType<T>::mBurstsBufferSize/2)] = val;

          if (!CYType<T>::mBurstsBufferState || (CYType<T>::mCptBurstsBuffer >= CYType<T>::mBurstsRatio))
          {
            CYType<T>::mBurstsBufferState = true;
            CYType<T>::mCptBurstsBuffer = 0;

            if (CYType<T>::mIdBuffer==0)
              CYType<T>::mIdBuffer = (CYType<T>::mBufferSize/2) -1;
            else
              CYType<T>::mIdBuffer--;
            if (CYType<T>::mIdBuffer==0)
              CYType<T>::mBufferFull=true;

            CYType<T>::mBuffer[CYType<T>::mIdBuffer] = val;
            CYType<T>::mBuffer[CYType<T>::mIdBuffer + (CYType<T>::mBufferSize/2)] = val;
          }

          CYType<T>::mCptBurstsBuffer++;
        }
        else
        {
          CYType<T>::mBurstsBufferState = false;
          if (CYType<T>::mDiscontBuffering) // bufferisation discontinue
          {
            uint id = CYType<T>::discontSampleId(str_fifo, i);
            uint num = CYType<T>::discontBufferNumber(str_fifo, i);
            if (id < CYType<T>::bufferSize())
            {
              if (num!=CYType<T>::mDiscontBufferNum)
              {
                if (CYType<T>::mDiscontBufferNum>0)
                {
                  // fin bufferisation discontinue
                  if (CYType<T>::discontBufferState==CYType<T>::Filling)
                    CYType<T>::endDiscontBuffer();
                  else
                  {
                    CYType<T>::endDiscontBuffer();
                    CYMESSAGETEXT(QString("%1: récupération du contenu du buffer précédent trop long").arg(CYType<T>::objectName()))
                  }
                }
              }

              if (CYType<T>::discontBufferState==CYType<T>::Filling)
              {
                // buffer courant en remplissage
                CYType<T>::mIdBuffer=id;
                CYType<T>::mBuffer[id] = val;
                CYType<T>::mDiscontBufferNum = num;
              }

              // remplissage buffer tampon
              // (utile le temps de traiter le buffer courant)
              CYType<T>::mDiscontBufferId = id;
              CYType<T>::mDiscontBuffer[id] = val;
            }
            else
              CYMESSAGETEXT(QString("%1: ditm %2 > %3 bufferSize").arg(CYType<T>::objectName()).arg(id).arg(CYType<T>::bufferSize()))
          }
          else // bufferisation continue
          {
            if (CYType<T>::mIdBuffer==0)
              CYType<T>::mIdBuffer = (CYType<T>::mBufferSize/2) -1;
            else
              CYType<T>::mIdBuffer--;
            if (CYType<T>::mIdBuffer==0)
              CYType<T>::mBufferFull=true;

            CYType<T>::mBuffer[CYType<T>::mIdBuffer] = val;
            CYType<T>::mBuffer[CYType<T>::mIdBuffer + (CYType<T>::mBufferSize/2)] = val;
          }
          bufferingTrigger(val);
          enqueueFIFO(val);
        }
      }
    }
  }
}

template<class T>
inline void CYNum<T>::bufferingTrigger(T val)
{
  if (CYType<T>::mTriggerEnabled) // si Trigger actif
  {
    if (!CYType<T>::mExtTrigger) // si Trigger externe actif
    {
      if (CYType<T>::mTriggerState==false) // trigger non déclenché
      {
        CYType<T>::mTriggerBuffer[CYType<T>::mIdBuffer] = val;
        CYType<T>::mTriggerBuffer[CYType<T>::mIdBuffer + (CYType<T>::mBufferSize/2)] = val;

        if (!CYType<T>::mTriggerStarted) // première valeur bufferisée
        {
          CYType<T>::mTriggerStarted=true;
          CYType<T>::mTriggerPrec=val; // mémorisation de valeur pour détection pente
        }
        else
        {
          // détection en montée
          if ((CYType<T>::mTriggerSlope==Cy::Rising)
              && (val>CYType<T>::mTriggerPrec)
              && (val>CYType<T>::mTriggerLevel))
            CYType<T>::mTriggerState=true;

          // détection en descente
          if ((CYType<T>::mTriggerSlope==Cy::Falling)
              && (val<CYType<T>::mTriggerPrec)
              && (val<CYType<T>::mTriggerLevel))
            CYType<T>::mTriggerState=true;

          if (CYType<T>::mTriggerState)// déclenchement trigger
          {
            CYType<T>::mTriggerIdBuffer = CYType<T>::mIdBuffer;
            CYType<T>::mPostTriggerIdBuffer = CYType<T>::mIdBuffer+(int)((CYType<T>::mPostTrigger*(CYType<T>::mBufferSize/2))/100.0);
            if (CYType<T>::mPostTriggerIdBuffer>(CYType<T>::mBufferSize/2))
              CYType<T>::mPostTriggerIdBuffer=CYType<T>::mPostTriggerIdBuffer-(CYType<T>::mBufferSize/2);
            emit CYData::trigger();
          }
          CYType<T>::mTriggerPrec=val; // mémorisation de valeur pour détection pente
        }
      }
      else if (!CYType<T>::mTriggerBufferFull) // bufferisation
      {
        if (CYType<T>::mIdBuffer==CYType<T>::mPostTriggerIdBuffer) // fin du bufferisation trigger
          CYType<T>::mTriggerBufferFull=true;
        else
        {
          CYType<T>::mTriggerBuffer[CYType<T>::mIdBuffer] = val;
          CYType<T>::mTriggerBuffer[CYType<T>::mIdBuffer + (CYType<T>::mBufferSize/2)] = val;
          CYType<T>::mTriggerBufferCpt++;
        }
      }
    }
    else // si Trigger externe actif
    {
      if (CYType<T>::mTriggerState==false) // trigger non déclenché
      {
        CYType<T>::mTriggerBuffer[CYType<T>::mIdBuffer] = val;
        CYType<T>::mTriggerBuffer[CYType<T>::mIdBuffer + (CYType<T>::mBufferSize/2)] = val;
        if ((CYType<T>::mExtTrigger->triggerState()==true) &&
            (CYType<T>::mExtTrigger->postTriggerIdBuffer()!=-1))// déclenchement trigger
        {
          CYType<T>::mTriggerIdBuffer=CYType<T>::mExtTrigger->triggerIdBuffer();
          CYType<T>::mPostTriggerIdBuffer=CYType<T>::mExtTrigger->postTriggerIdBuffer();
          CYType<T>::mTriggerState=true;
          emit CYData::trigger();
        }
      }
      else if (!CYType<T>::mTriggerBufferFull) // bufferisation
      {
        if (CYType<T>::mIdBuffer==CYType<T>::mPostTriggerIdBuffer) // fin du bufferisation trigger
          CYType<T>::mTriggerBufferFull=true;
        else
        {
          CYType<T>::mTriggerBuffer[CYType<T>::mIdBuffer] = val;
          CYType<T>::mTriggerBuffer[CYType<T>::mIdBuffer + (CYType<T>::mBufferSize/2)] = val;
        }
      }
    }
  }
}

template<class T>
inline T CYNum<T>::sample(int str_fifo, int i)
{
  char *ptr;
  ptr = (char*)CYType<T>::mVal;
  ptr+= str_fifo*mSampleOffsetStruct + (i*sizeof(T));
  return ((*((T*)ptr)));
}

template<class T>
inline T CYNum<T>::setSampleVal(T val, int str_fifo, int i)
{
  char *ptr;
  ptr = (char*)CYType<T>::mVal;
  ptr+= str_fifo*mSampleOffsetStruct + (i*sizeof(T));
  T *ptrVal = (T*)ptr;
  *ptrVal = val;
}

template<class T>
inline int CYNum<T>::nbDec()
{
  if (CYType<T>::isExponential() && isSectionMini())
    return 0;
  else
    return CYType<T>::nbDec();
}

template<class T>
inline QString CYNum<T>::toString(bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  return toString(val(), withUnit, ctrlValid, base);
}

template<class T>
inline QString CYNum<T>::toString(double val, bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  QString suffix;
  if (withUnit)
    suffix.append(" "+CYType<T>::unit());

  base = ( base==Cy::BaseData ) ? CYType<T>::numBase() : base;
  if ((base!=Cy::Hexadecimal) && (base!=Cy::Binary) && (base!=Cy::Decimal))
    base = Cy::Decimal;

  if (ctrlValid && !CYType<T>::isValid())
    return QString("%1%2").arg(stringDef()).arg(suffix);
  else
  {
    if (CYType<T>::mFormat->isExponential())
      return QString("%1e%2%3").arg(mantissa(val), 0, 'f', CYType<T>::mFormat->nbDec()).arg(exponent(val)).arg(suffix);
    else if (base==Cy::Decimal)
      return QString("%1%2").arg(val, 0, 'f', nbDec()).arg(suffix);
    else
      return QString("%1%2").arg((long)val, 0, base).arg(suffix);
  }
}

template<class T>
inline QString CYNum<T>::displayHelp(bool whole, bool oneLine, bool withGroup, bool withValue)
{
  QString min = toString(CYNum<T>::min(), false, false);
  QString max = toString(CYNum<T>::max(), false, false);
  bool choiceHelp = false;

  QString tmp;
  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(CYType<T>::group()));
  if (CYType<T>::label()!="")
    tmp.append(CYType<T>::label());
  if (CYType<T>::phys()!="")
    tmp.append(QString(" [%1]").arg(CYType<T>::phys()));
  if (CYType<T>::elec()!="")
    tmp.append(QString(" [%1]").arg(CYType<T>::elec()));
  if (CYType<T>::addr()!="")
    tmp.append(QString(" [%1]").arg(CYType<T>::addr()));
  if (withValue)
  {
    if (CYNum<T>::hasChoice())
      tmp.append(QString(" = <b>%1</b>").arg(CYNum<T>::choiceLabel()));
    else
      tmp.append(QString(" = <b>%1</b>").arg(CYNum<T>::toString(true, false)));
  }

  if ( !oneLine )
  {
    if (!tmp.isEmpty())
      tmp.append("<hr>");
    else
      tmp.append("<br>");
    if (CYType<T>::help()!="")
      tmp.append(CYType<T>::help());

    if (!CYType<T>::choiceDict.isEmpty() && CYType<T>::choiceDictHelp())
    {
      QStringList list;
      QMapIterator<int, CYDataChoice*> it( CYType<T>::choiceDict );
      while (it.hasNext())
      {
        it.next();
        CYDataChoice * choice = it.value();
        if (choice)
          list<<QString("%1").arg(choice->value);
      }
      list.sort();

      for ( QStringList::Iterator it2 = list.begin(); it2 != list.end(); ++it2 )
      {
        QString text = *it2;
        int index=text.toInt();
        CYDataChoice * choice =CYType<T>::choiceDict[index];

        if (!choiceHelp)
        {
          choiceHelp = true;
          tmp.append(QObject::tr("Choice:")+"<table>");
        }
        if (choice->label.isEmpty())
        {
          if (!choice->help.isEmpty())
            tmp.append("<tr><td>"+QString::number(choice->value)+": </td><td>"+choice->help+"</td></tr>");
          else
            tmp.append("<tr><td>"+QString::number(choice->value)+"</td></tr>");
        }
        else
        {
          if (!choice->help.isEmpty())
            tmp.append("<tr><td>"+choice->label+": </td><td>"+choice->help+"</td></tr>");
          else
            tmp.append("<tr><td>"+choice->label+"</td></tr>");
        }
      }
      if (choiceHelp)
        tmp.append("</table>");
    }

    if (whole)
    {
      QString tmp2;
      if (!CYType<T>::mAutoMinMax)
        tmp2.append("<li>"+QObject::tr("Value from %1 to %2 %3.").arg(min).arg(max).arg(CYType<T>::unit())+"</li>");
      if (CYType<T>::precision())
        tmp2.append("<li>"+QObject::tr("Precision is %1 %2.").arg(CYType<T>::precision()).arg(CYType<T>::unit())+"</li>");
      if (!tmp2.isEmpty())
        tmp.append("<ul>"+tmp2+"</ul>");
    }

    tmp.append(CYType<T>::note());
  }
  return tmp;
}

template<class T>
inline QString CYNum<T>::inputHelp(bool whole, bool withGroup)
{
  QString min = toString(CYNum<T>::min(), false, false);
  QString max = toString(CYNum<T>::max(), false, false);
  QString def = toString(CYNum<T>::def(), false, false);
  bool choiceHelp = false;

  QString tmp = "";
  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(CYType<T>::group()));
  if (CYType<T>::label()!="")
    tmp.append(CYType<T>::label());
  if (CYType<T>::phys()!="")
    tmp.append(QString(" [%1]").arg(CYType<T>::phys()));
  if (CYType<T>::elec()!="")
    tmp.append(QString(" [%1]").arg(CYType<T>::elec()));
  if (CYType<T>::addr()!="")
    tmp.append(QString(" [%1]").arg(CYType<T>::addr()));
  if (!tmp.isEmpty())
    tmp.append("<hr>");
  else
    tmp.append("<br>");
  if (CYType<T>::help()!="")
  {
    tmp.append("<br>");
    tmp.append(CYType<T>::help());
  }

  QString designerLabel;

  if (!CYType<T>::choiceDict.isEmpty() && CYType<T>::choiceDictHelp())
  {
    tmp.append("<br>");
    QStringList list;
    QMapIterator<int, CYDataChoice*> it( CYType<T>::choiceDict );
    while (it.hasNext())
    {
      it.next();
      CYDataChoice * choice = it.value();
      if (choice)
        list<<QString("%1").arg(choice->value);
    }
    list.sort();

    for ( QStringList::Iterator it2 = list.begin(); it2 != list.end(); ++it2 )
    {
      QString text = *it2;
      int index=text.toInt();
      CYDataChoice * choice =CYType<T>::choiceDict[index];
      if (!choice)
        continue;
      if (!choiceHelp)
      {
        choiceHelp = true;
        tmp.append(QObject::tr("Choice:")+"<table>");
      }

      if (choice->label.isEmpty())
      {
        if (!choice->help.isEmpty())
          tmp.append("<tr><td>"+QString::number(choice->value)+": </td><td>"+choice->help+"</td></tr>");
        else
          tmp.append("<tr><td>"+QString::number(choice->value)+"</td></tr>");
      }
      else
      {
        if (!choice->help.isEmpty())
          tmp.append("<tr><td>"+choice->label+": </td><td>"+choice->help+"</td></tr>");
        else
          tmp.append("<tr><td>"+choice->label+"</td></tr>");
      }

      if (choice->value==(int)(CYNum<T>::mDef))
        designerLabel = choice->label;
    }

    if (choiceHelp)
      tmp.append("</table>");
    if (CYType<T>::helpDesignerValue() && !designerLabel.isEmpty())
      tmp.append(QObject::tr("Designer value:")+" "+designerLabel);
  }
  else if (whole)
  {
    QString tmp2;
    if (!CYType<T>::mAutoMinMax)
      tmp2.append("<li>"+QObject::tr("Value from %1 to %2 %3.").arg(min).arg(max).arg(CYType<T>::unit())+"</li>");
    if (CYType<T>::precision())
      tmp2.append("<li>"+QObject::tr("Precision is %1 %2.").arg(CYType<T>::precision()).arg(CYType<T>::unit())+"</li>");
    if (CYType<T>::helpDesignerValue())
      tmp2.append("<li>"+QObject::tr("Designer value: %1 %2.").arg(def).arg(CYType<T>::unit())+"</li>");
    if (!tmp2.isEmpty())
      tmp.append("<ul>"+tmp2+"</ul>");
  }

  tmp.append(CYType<T>::note());
  return tmp;
}

template<class T>
inline void CYNum<T>::calculNbDigits()
{
  QString min = QString("%1").arg((double)CYNum<T>::min(), 0, 'f', nbDec());
  QString max = QString("%1").arg((double)CYNum<T>::max(), 0, 'f', nbDec());

  uint lengthMin = min.length();
  uint lengthMax = max.length();

  uint maxLength;
  if (lengthMin>lengthMax)
    maxLength = lengthMin;
  else
    maxLength = lengthMax;

  int nbDigit;
  if (nbDec()>0)
    nbDigit = maxLength-1;
  else
    nbDigit = maxLength;

  setNbDigit(nbDigit);

  CYType<T>::calculNbDigits();
}

template<class T>
inline QString CYNum<T>::prefix()
{
  return (mSection == CYType<T>::Mantissa) ? "" : QString("%1e").arg(mMantissa, 0, 'f',  CYType<T>::mFormat->nbDec());
}

template<class T>
inline QString CYNum<T>::suffix()
{
  return (mSection == CYType<T>::Mantissa) ? QString("e%1 %2").arg(mExponent).arg(CYType<T>::unit()) : QString(" %1").arg(CYType<T>::unit());
}

template<class T>
inline void CYNum<T>::setSection(CYData::Section section)
{
  mSection = section;
}

template<class T>
inline CYData::Section CYNum<T>::section()
{
  return mSection;
}

template<class T>
inline void CYNum<T>::initToSectionMaxi()
{
  mSection = CYType<T>::Mantissa;
}

template<class T>
inline void CYNum<T>::initToSectionMini()
{
  mSection = CYType<T>::Exponent;
}

template<class T>
inline bool CYNum<T>::isSectionMaxi()
{
  return (mSection == CYType<T>::Mantissa) ? true : false;
}

template<class T>
inline bool CYNum<T>::isSectionMini()
{
  return (mSection == CYType<T>::Exponent) ? true : false;
}

template<class T>
inline bool CYNum<T>::sectionMaxi(const double val)
{
  setValSection(val);
  mSection = CYType<T>::Mantissa;
  return true;
}

template<class T>
inline bool CYNum<T>::sectionMini(const double val)
{
  setValSection(val);
  mSection = CYType<T>::Exponent;
  return true;
}

template<class T>
inline bool CYNum<T>::sectionUp(const double val)
{
  return (isSectionMaxi()) ? false : sectionMaxi(val);
}

template<class T>
inline bool CYNum<T>::sectionDown(const double val)
{
  return (isSectionMini()) ? false : sectionMini(val);
}

template<class T>
inline bool CYNum<T>::section(QString left)
{
  CYData::Section sect;

  if (!left.contains("e"))
    sect = CYType<T>::Mantissa;
  else
    sect = CYType<T>::Exponent;

  if (mSection != sect)
  {
    mSection = sect;
    return true;
  }
  return false;
}

template<class T>
inline void CYNum<T>::setValSection(const double value)
{
  if (mSection == CYType<T>::Mantissa)
    setMantissa(value);
  else
    setExponent((int)value);
}

template<class T>
inline double CYNum<T>::valSection()
{
  return (mSection == CYType<T>::Mantissa) ? mMantissa : mExponent;
}

template<class T>
inline double CYNum<T>::defSection()
{
  return (mSection == CYType<T>::Mantissa) ? mantissa(CYType<T>::mDef) : exponent(CYType<T>::mDef);
}

template<class T>
inline double CYNum<T>::minSection()
{
  if (mSection == CYType<T>::Exponent) // retourne l'exposant minimum
  {
    if (tmp()<0.0)
    {
      if ((min()<0.0) && (max()<0.0))
        return exponent(max());
      else if (min()<0.0)
        return (exponent(min())>0) ? -20 : exponent(min());
    }
    else
    {
      if ((min()>0.0) && (max()>0.0))
        return exponent(min());
      else if (max()>0.0)
        return -20;
    }
  }
  else // retourne la mantisse minimum
  {
    if (tmp()<0.0)
    {
      if ((min()<0.0) && (max()<0.0))
      {
        if (exponent(tmp())==exponent(min()))
          return mantissa(min());
        else
          return -9.99999999;
      }
      else if (min()<0.0)
      {
        if (exponent(tmp())==exponent(min()))
          return mantissa(min());
        else
          return -9.99999999;
      }
    }
    else
    {
      if ((min()>0.0) && (max()>0.0))
      {
        if (exponent(tmp())==exponent(min()))
          return mantissa(min());
        else
          return 1.0;
      }
      else if (max()>0.0)
      {
        if (exponent(tmp())==exponent(max()))
          return 1.0;
        else
          return -9.99999999;
      }
    }
  }
  return 0.0;
}

template<class T>
inline double CYNum<T>::maxSection()
{
  if (mSection == CYType<T>::Exponent) // retourne l'exposant maximum
  {
    if (tmp()<0.0)
    {
      if ((min()<0.0) && (max()<0.0))
        return exponent(min());
      else if (min()<0.0)
        return (exponent(min())>0) ? exponent(min()) : 20;
    }
    else
    {
      if ((min()>0.0) && (max()>0.0))
        return exponent(max());
      else if (max()>0.0)
        return exponent(max());
    }
  }
  else // retourne la mantisse minimum
  {
    if (tmp()<0.0)
    {
      if ((min()<0.0) && (max()<0.0))
      {
        if (exponent(tmp())==exponent(max()))
          return mantissa(max());
        else
          return 9.99999999;
      }
      else if (min()<0.0)
      {
        if (exponent(tmp())==exponent(max()))
          return mantissa(max());
        else
          return 9.99999999;
      }
    }
    else
    {
      if ((min()>0.0) && (max()>0.0))
      {
        if (exponent(tmp())==exponent(max()))
          return mantissa(max());
        else
          return 9.99999999;
      }
      else if (max()>0.0)
      {
        if (exponent(tmp())==exponent(max()))
          return mantissa(max());
        else
          return 9.99999999;
      }
    }
  }
  return 0.0;
}

template<class T>
inline double CYNum<T>::precisionSection()
{
  return (mSection == CYType<T>::Mantissa) ? CYType<T>::precision() : 0;
}

template<class T>
inline void CYNum<T>::sections()
{
  mantissa();
  exponent();
}

template<class T>
inline void CYNum<T>::setMantissa(const double val)
{
  mMantissa = val;
  QString txt = QString("%1e%2").arg(mMantissa).arg(mExponent);
  CYType<T>::mTmp = cyExpFromString(txt, mMantissa, mExponent);
}

template<class T>
inline void CYNum<T>::setExponent(const int val)
{
  mExponent = val;
  QString txt = QString("%1e%2").arg(mMantissa).arg(mExponent);
  CYType<T>::mTmp = cyExpFromString(txt, mMantissa, mExponent);
}

template<class T>
inline bool CYNum<T>::isValid()
{
  if (CYType<T>::isValid())
  {
    T tmp = (T)CYType<T>::mTmp;
    if ((tmp>=mMin) && (tmp<=mMax))
      return true;
    return false;
  }
  return false;
}

template<class T>
inline double CYNum<T>::mantissa()
{
  mMantissa = mantissa(CYType<T>::mTmp);
  return mMantissa;
}

template<class T>
inline int CYNum<T>::exponent()
{
  mExponent = exponent(CYType<T>::mTmp);
  return mExponent;
}

template<class T>
inline double CYNum<T>::mantissa(const double val)
{
  return cyMantissa(val);
}

template<class T>
inline int CYNum<T>::exponent(const double val)
{
  return cyExponent(val);
}

template<class T>
inline void CYNum<T>::setPrecision(const double precision)
{
  CYType<T>::setPrecision(precision);
  calculNbDigits();
}

template<class T>
inline void CYNum<T>::setNbDigit(int nb)
{
  mNbDigit = nb;

  if (CYType<T>::isExponential())
  {
    int nbDec = CYType<T>::mFormat->nbDec();
    mStringDef = "-";
    mNbDigit = 1;
    if (nbDec)
    {
      mStringDef.append(".");
      mNbDigit++;
    }
    for (int i=0; i<nbDec; i++)
    {
      mStringDef.append('-');
      mNbDigit++;
    }
    mStringDef.append('e');
    mNbDigit++;

    QString min = QString("%1").arg(CYNum<T>::exponent(CYNum<T>::min()));
    QString max = QString("%1").arg(CYNum<T>::exponent(CYNum<T>::max()));

    uint lengthMin = min.length();
    uint lengthMax = max.length();

    uint maxLength;
    if (lengthMin>lengthMax)
      maxLength = lengthMin;
    else
      maxLength = lengthMax;
    mNbDigit += maxLength;

    if (min.contains('-') && max.contains('-'))
    {
      mStringDef.append('-');
      maxLength--;
    }
    else if (min.contains('-') || max.contains('-'))
    {
      // TODO KDE3->QT3 mStringDef.append('±');
      mStringDef.append(' ');
      maxLength--;
    }

    for (uint i=0; i<maxLength; i++)
      mStringDef.append('-');
  }
  else
  {
    if ((mNbDigit-nbDec())<0)
      CYFATALDATA(CYType<T>::objectName())

          mStringDef.fill('-', mNbDigit);

    if (nbDec())
      mStringDef.insert((mNbDigit-nbDec()), ".");

    int tmp = mNbDigit-nbDec();
    while (tmp > 3)
    {
      mStringDef.insert((tmp-3), " ");
      tmp -= 3;
    }
  }
}

template<class T>
inline QString CYNum<T>::exportData()
{
  QString values = QString("%1\t%2\t%3\t%4").arg(toString()).arg(toString(def())).arg(toString(min())).arg(toString(max()));
  QString link = CYNum<T>::linksName().isEmpty() ? " " : CYNum<T>::linksName();
  QString group = CYNum<T>::group().isEmpty() ? " " : CYNum<T>::group();

  QString line = QString("%1\t%2\t%3\t%4\t%5\t%6\t%7")
      .arg(CYNum<T>::hostsName()).arg(link).arg(group)
      .arg(CYNum<T>::objectName()).arg(CYNum<T>::label()).arg(values).arg(CYNum<T>::help());

  if (line.count("\t")>9)
    CYMESSAGETEXT(line);
  if (line.count("\n")>1)
    CYMESSAGETEXT(line);

  line.replace("\n", " ");
  line.append("\n");

  return line;
}

template<class T>
inline void CYNum<T>::initFIFO()
{
  mEnableFIFO=false;
}

template<class T>
inline void CYNum<T>::setEnableFIFO(bool val)
{
  mEnableFIFO=val;
  mFIFO.clear();
}

template<class T>
inline void CYNum<T>::enqueueFIFO(T val)
{
  if (!mEnableFIFO)
    return;

  mFIFO.enqueue(val);
}

template<class T>
inline T CYNum<T>::dequeueFIFO(bool &ok)
{
  ok=false;
  T val=0;

  if (!mFIFO.isEmpty())
  {
    val=mFIFO.dequeue();
    ok=true;
  }
  return val;
}

#endif
