/***************************************************************************
                          cypidk.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYPIDK_H
#define CYPIDK_H

// CYLIBS
#include "cyf32.h"

/** CYPIDK représente le facteur intégral/dérivée d'un PID.
  * @short Facteur intégral/dérivée de PID.
  * @author Gérald LE CLEACH
  */

class CYPIDK : public CYF32
{
  Q_OBJECT
public:
  /** Création d'un facteur intégral/dérivée de PID.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param def   Valeur par défaut.
    * @param min   Limite inférieure.
    * @param max   Limite supérieure. */
  CYPIDK(CYDB *db, const QString &name, f32 *val, const f32 def=0.0, const f32 min=0.0, const f32 max=900.0);

  ~CYPIDK();
};

#endif
