//
// C++ Implementation: cydate
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cydatetime.h"


CYDateTime::CYDateTime(CYDB *db, const QString &name, s64 *val, double base, s64 def, QString min, QString max, QString label, Cy::Mode mode)
  : CYS64(db, name, val),
    mBase(base)
{
  mDef  = def;
  mMin  = toSinceEpoch(min);
  mMax  = toSinceEpoch(max);
  setLabel(label);
  setMode(mode);
}

CYDateTime::CYDateTime(CYDB *db, const QString &name, QString *val, QString def, QString min, QString max, double base, QString label, Cy::Mode mode)
  : CYS64(db, name, new s64),
    mBase(base)
{
  *mVal = toSinceEpoch(*val);
  mDef  = toSinceEpoch(def);
  mMin  = toSinceEpoch(min);
  mMax  = toSinceEpoch(max);
  setLabel(label);
  setMode(mode);
}

CYDateTime::CYDateTime(CYDB *db, const QString &name, char *ptrChar, int nbChar, double base, QString label, Cy::Mode mode)
  : CYS64(db, name, new s64),
    mBase(base)
{
  char buf[nbChar];
  sprintf(buf,"%s", ptrChar);
  *mVal = toSinceEpoch(QString(buf));
  mDef  = toSinceEpoch("2000-01-01T00:00:00");
  mMin  = toSinceEpoch("2000-01-01T00:00:00");
  mMax  = toSinceEpoch("2100-12-31T23:59:59");
  setLabel(label);
  setMode(mode);
}

CYDateTime::CYDateTime(CYDB *db, const QString &name, AttrString *attr)
  : CYS64(db, name, new s64),
    mBase(attr->base)
{
  char buf[attr->nbChar];
  sprintf(buf,"%s", attr->ptrChar);
  *mVal = toSinceEpoch(QString(buf));
  mDef  = toSinceEpoch("2000-01-01T00:00:00");
  mMin  = toSinceEpoch("2000-01-01T00:00:00");
  mMax  = toSinceEpoch("2100-12-31T23:59:59");
  setLabel(attr->label);
  setMode(attr->mode);
}

CYDateTime::~CYDateTime()
{
}

QDateTime CYDateTime::fromString(QString val)
{
  return QDateTime::fromString(val, Qt::ISODate);
}

s64 CYDateTime::toSinceEpoch(const QDateTime dt)
{
  bool ok;
  s64 res = toSinceEpoch(dt, mBase, &ok);
  if (!ok)
    CYWARNINGTEXT(QString("%1: Base inconnue %2").arg(objectName()).arg(mBase));

  return res;
}

s64 CYDateTime::toSinceEpoch(const QDateTime dt, double base, bool *ok)
{
  s64 res;
  *ok = true;
  if (base==BASE_SEC)
    res = dt.toSecsSinceEpoch();
  else if (base==BASE_MSEC)
    res = dt.toMSecsSinceEpoch();
  else
    *ok = false;

  return res;
}

s64 CYDateTime::toSinceEpoch(QString txt)
{
  if (txt.isEmpty())
    return 0;

  QDateTime dt = CYDateTime::fromString(txt);
  if (!dt.isValid())
    CYWARNINGTEXT(QString("%1: impossible de convertir la chaine de caractères %2 en horodatage").arg(objectName()).arg(txt));

  return toSinceEpoch(dt);
}

void CYDateTime::setSinceEpoch(const s64 val)
{
  if (mBase==BASE_SEC)
    setSecsSinceEpoch(val);
  else if (mBase==BASE_MSEC)
    setMSecsSinceEpoch(val);
  else
    CYWARNINGTEXT(QString("%1: Base inconnue %2").arg(objectName()).arg(mBase));
}

void CYDateTime::setVal(QString val)
{
  setVal(toSinceEpoch(val));
}

void CYDateTime::setVal(QDateTime val)
{
  setVal(toSinceEpoch(val));
}

void CYDateTime::setVal(s64 val)
{
  mTmp = static_cast<double>(val);
  setSinceEpoch(val);
  CYS64::setVal(val);
}

void CYDateTime::setTmp(const double val)
{
  mTmp = val;
  setSinceEpoch(static_cast<s64>(val));
}

QString CYDateTime::toString(bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  Q_UNUSED(base)
  return toString((double)real(), withUnit, ctrlValid);
}

QString CYDateTime::toString(double val, bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  Q_UNUSED(withUnit)
  Q_UNUSED(base)

  QString txt;
  if (ctrlValid && !CYS64::isValid())
    txt = stringDef();
  else
  {
    if (mBase==BASE_SEC)
      txt = QDateTime::fromSecsSinceEpoch(val, Qt::LocalTime).toString(Qt::ISODate);
    else if (mBase==BASE_MSEC)
      txt = QDateTime::fromMSecsSinceEpoch(val, Qt::LocalTime).toString(Qt::ISODateWithMs);
    else
      CYWARNINGTEXT(QString("%1: Base inconnue %2").arg(objectName()).arg(mBase));
  }
  txt.replace("T", " ");
  return txt;
}
