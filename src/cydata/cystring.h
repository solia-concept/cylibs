/***************************************************************************
                          cystring.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYSTRING_H
#define CYSTRING_H

// QT
#include <QString>
#include <QCryptographicHash>
// CYLIBS
#include "cytype.h"

class CYSimpleCrypt;

/** CYString est une donnée alpha-numérique.
  * @short Donnée alpha-numérique.
  * @author Gérald LE CLEACH
  */

class CYString : public CYType<QString>
{
  Q_OBJECT
public:
  /** Attr est la structure de saisie des attributs d'une donnée alpha-numérique. */
  struct Attr
  {
    /** Nom. */
    const QString &name;
    /** Pointe sur la chaine de caractères. */
    char *ptrChar;
    /** Nombre de caractères. */
    int nbChar;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Etiquette. */
    QString label;
  };

  /** Création d'une donnée alpha-numérique.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param label Etiquette.
    * @param mode  Mode d'utilisation. */
  CYString(CYDB *db, const QString &name, QString *val = 0, QString label = 0, Cy::Mode mode = Sys);

  /** Création d'une donnée alpha-numérique .
    * @param db       Base de données parent.
    * @param name     Nom de la donnée.
    * @param ptrChar  Pointe sur la chaine de caractères.
    * @param nbChar   Nombre de caractères.
    * @param label    Etiquette.
    * @param mode     Mode d'utilisation. */
    CYString(CYDB *db, const QString &name, char *ptrChar, int nbChar, QString label = 0, Cy::Mode mode = Sys);

  /** Création d'une donnée alpha-numérique.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYString(CYDB *db, const QString &name, Attr *attr);

  ~CYString();

  /** @return la valeur. */
  virtual QString val();
  /** Saisie une valeur. */
  virtual void setVal(QString val);

  /** Saisie l'objet d'encriptage de la valeur. */
  virtual void setCryptographicHash(QCryptographicHash *hash);
  /** Saisie l'objet d'encriptage simple de la valeur. */
  virtual void setSimpleCrypt(CYSimpleCrypt *simpleCrypt);
  /** @return le text \a txt décriptée. */
  virtual QString simpleDecrypt(QString txt);
  /** @return la valeur décriptée. */
  virtual QString simpleDecrypt();

  /** @return la valeur \a val en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(bool withUnit = true, bool ctrlValid = true, Cy::NumBase base = Cy::BaseData);

  /** @return l'aide à la saisie.
    * @param whole Aide entière avec la valeur par défaut.
    * @param bool     Aide avec le groupe de la donnée. */
  virtual QString inputHelp(bool whole = true, bool withGroup=true);

  /** @return \a true si la section \a section dans la chaîne de caractères sectionnée par des séparateurs \a sep existe. */
  bool findSection(const QString &section, const QString &sep);

  /** Saisie le nombre de caractères de la chaîne.*/
  virtual void setNbChar( int nb ) { mNbChar = nb; }
  /** @return le nombre de caractères de la chaîne. S'il vaut -1 alors c'est que le nombre de charactères est ilimités. */
  virtual int nbChar() { return mNbChar; }

  /** @return le pointeur de la chaine de caractères. */
  const char *ptrChar() { return mPtrChar; }

protected: // Protected attributes
  /** Pointe sur la chaine de caractères. */
  char *mPtrChar;
  /** Nombre de caractères. */
  int mNbChar;

  QCryptographicHash *mCryptographicHash;
  CYSimpleCrypt *mSimpleCrypt;

public slots:
  /** Ajoute \a txt en début */
  virtual void prepend(QString txt);
  /** Ajoute \a txt en fin */
  virtual void append(QString txt);
};

#endif
