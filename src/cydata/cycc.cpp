//
// C++ Implementation: cycc
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cycc.h"

// CYLIBS
#include "cycore.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cycommand.h"
#include "cystring.h"
#include "cydblibs.h"

CYCC::CYCC(CYDB *db, const QString &name, t_cu64 *cnt, Attr1 *attr)
  : CYDatasGroup(db, name, attr->idNet, attr->group)
{
  mLabel = attr->label;
  CYU64 *data;

  data = new CYU64(db, QString("TOTAL_%1").arg(name), &(cnt->val), attr->format, attr->mode);
  mTotalData = static_cast<CYData*>(data);
  mTotalData->setLabel(mLabel);

  data = new CYU64(db, QString("PARTIAL_%1").arg(name), &(cnt->tmp), attr->format, attr->mode);
  mPartialData = static_cast<CYData*>(data);
  mPartialData->setLabel(mLabel);

  mThresholdData = new CYU64(db, QString("THRESHOLD_%1").arg(name), new u64, attr->format, attr->mode);
  mThresholdData->setLabel(mLabel);

  mNoteData = new CYString(db, QString("NOTE_%1").arg(name));
  mNoteData->setLabel(mLabel);

  init();
}

CYCC::CYCC(CYDB *db, const QString &name, t_cu32 *cnt, Attr1 *attr)
  : CYDatasGroup(db, name, attr->idNet, attr->group)
{
  mLabel = attr->label;
  CYU32 *data;

  data = new CYU32(db, QString("TOTAL_%1").arg(name), (&cnt->val), attr->format, attr->mode);
  mTotalData = static_cast<CYData*>(data);
  mTotalData->setLabel(mLabel);

  data = new CYU32(db, QString("PARTIAL_%1").arg(name), (&cnt->tmp), attr->format, attr->mode);
  mPartialData = static_cast<CYData*>(data);
  mPartialData->setLabel(mLabel);

  mThresholdData = new CYU64(db, QString("THRESHOLD_%1").arg(name), new u64, attr->format, attr->mode);
  mThresholdData->setLabel(mLabel);

  mNoteData = new CYString(db, QString("NOTE_%1").arg(name));
  mNoteData->setLabel(mLabel);

  init();
}

CYCC::CYCC(CYDB *db, const QString &name, t_cu64 *cnt, int idNet, Cy::Mode mode, int format, QString group, QString label)
  : CYDatasGroup(db, name, idNet, group)
{
  mLabel = label;
  CYU64 *data;

  data = new CYU64(db, QString("TOTAL_%1").arg(name), &(cnt->val), format, mode);
  mTotalData = static_cast<CYData*>(data);
  mTotalData->setLabel(mLabel);

  data = new CYU64(db, QString("PARTIAL_%1").arg(name), &(cnt->tmp), format, mode);
  mPartialData = static_cast<CYData*>(data);
  mPartialData->setLabel(mLabel);

  mThresholdData = new CYU64(db, QString("THRESHOLD_%1").arg(name), new u64, format, mode);
  mThresholdData->setLabel(mLabel);

  mNoteData = new CYString(db, QString("NOTE_%1").arg(name));
  mNoteData->setLabel(mLabel);

  init();
}

CYCC::CYCC(CYDB *db, const QString &name, t_cu32 *cnt, int idNet, Cy::Mode mode, int format, QString group, QString label)
  : CYDatasGroup(db, name, idNet, group)
{
  mLabel = label;
  CYU32 *data;

  data = new CYU32(db, QString("TOTAL_%1").arg(name), (&cnt->val), format, mode);
  mTotalData = static_cast<CYData*>(data);
  mTotalData->setLabel(mLabel);

  data = new CYU32(db, QString("PARTIAL_%1").arg(name), (&cnt->tmp), format, mode);
  mPartialData = static_cast<CYData*>(data);
  mPartialData->setLabel(mLabel);

  mThresholdData = new CYU64(db, QString("THRESHOLD_%1").arg(name), new u64, format, mode);
  mThresholdData->setLabel(mLabel);

  mNoteData = new CYString(db, QString("NOTE_%1").arg(name));
  mNoteData->setLabel(mLabel);

  init();
}

CYCC::~CYCC()
{
}

/*!
    \fn CYCnt::init()
 */
void CYCC::init()
{
  if ( mGroup.isEmpty() )
    mGroup = tr("Maintenance")+':'+tr("Cycles/switching counter");
  else
    mGroup = mGroup+':'+tr("Maintenance")+':'+tr("Cycles/switching counter");

  mHelp  = tr("Counter used for maintenance");

  mTotalData->setGroup(mGroup+':'+tr("Total"));
  mTotalData->setHelp(mHelp);

  mPartialData->setGroup(mGroup+':'+tr("Partial"));
  mPartialData->setHelp(mHelp);

  mThresholdData->setGroup(mGroup+':'+tr("Alert threshold"));
  mThresholdData->setHelp(tr("An alert is generated if the partial counter reaches this value."));
  mThresholdData->addNote(tr("A null value disable the control."));

  mNoteData->setGroup(mGroup+':'+tr("Note"));
  mNoteData->setHelp(mHelp);

  mTotalData->setPartialData( mPartialData );
  mTotalData->setNoteData( mNoteData );
  mTotalData->setThresholdData( mThresholdData );

  QString from = tr("Maintenance");
  QString help, helpEnd;
  flg *val = new flg;
  (*val) = false;
  help = tr("Indicates that the partial counter reached the alert threshold. This threshold is settable in the maintenance counters window.");
  alert = mPartialData->CYData::initAlert(core->eventsGeneratorSPV, QString("FA_THRESHOLD_%1").arg(objectName()), val, helpEnd, SI_1, REC|SCR|BST, QString(mGroup+":"+mLabel), from, 0, help);
  setCtrlEvent(alert);
}


/*!
    \fn CYCC::initResetData(CYDB *db, flg *val)
 */
void CYCC::initResetData(CYDB *db, flg *val)
{
  mResetData = new CYCommand(db, QString("RESET_%1").arg(objectName()), val, Cy::Monostable);
  mResetData->setLabel(mLabel);
  mResetData->setGroup(mGroup+':'+tr("Reset"));
  mTotalData->setResetData( mResetData );
}


/*! @return la donnée de RAZ.
    \fn CYCC::resetData()
 */
CYCommand * CYCC::resetData()
{
  return mResetData;
}

void CYCC::setCtrlEvent(CYEvent *ev)
{
  ctrlEvent = ev;
  ctrlEvent->setReportUp(tr("Alert"));
  ctrlEvent->setLocal(true);
  QTimer *timer = new QTimer;
  timer->start(5000);
  connect(timer, SIGNAL(timeout()), SLOT(ctrlPartial()));
}


/*! Contrôle que le compteur partiel ne dépasse pas la valeur de contrôle
    \fn CYCC::ctrlPartial()
 */
void CYCC::ctrlPartial()
{
  if (!core->started() || !db()->linksOn())
    return;

  if (mThresholdData->val()==0)
    return;

  if (!ctrlEvent)
    return;

  CYU64 *data = static_cast<CYU64 *>(mPartialData);
  if (data->val()>mThresholdData->val())
    ctrlEvent->setVal(true);
  else
    ctrlEvent->setVal(false);
}
