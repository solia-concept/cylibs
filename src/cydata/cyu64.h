/***************************************************************************
                          cyu64.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYU64_H
#define CYU64_H

// CYLIBS
#include "cynum.h"

/** CYU64 est une donnée entière non-signée de 64 bits.
  * @short Donnée entière de 64 bits.
  * @author Gérald LE CLEACH
  */

class CYU64 : public CYNum<u64>
{
public:
  /** Création d'une donnée entière non-signée de 64 bits.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU64(CYDB *db, const QString &name, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière non-signée de 64 bits avec sa valeur.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU64(CYDB *db, const QString &name, u64 *val, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 64 bits avec sa valeur ainsi que sa valeur par défaut.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU64(CYDB *db, const QString &name, u64 *val, const u64 def, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 64 bits avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU64(CYDB *db, const QString &name, u64 *val, const u64 def, const u64 min, const u64 max, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière non-signée de 64 bits avec les attributs Attr1.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU64(CYDB *db, const QString &name, CYNum<u64>::Attr1<u64> *attr);

  /** Création d'une donnée entière non-signée de 64 bits avec les attributs Attr2.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU64(CYDB *db, const QString &name, CYNum<u64>::Attr2<u64> *attr);

  /** Création d'une donnée entière non-signée de 64 bits avec les attributs Attr3.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU64(CYDB *db, const QString &name, CYNum<u64>::Attr3<u64> *attr);

  /** Création d'une donnée entière non-signée de 64 bits avec les attributs Attr4.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU64(CYDB *db, const QString &name, CYNum<u64>::Attr4<u64> *attr);

  ~CYU64();

  /** @return le nombre de chiffres total. */
  virtual int nbDigit(Cy::NumBase base=Cy::BaseData);

   /** @return la valeur courrante en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(bool withUnit=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

  virtual QString toString(double val, bool withUnit, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);
};

#endif
