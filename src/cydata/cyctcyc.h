/***************************************************************************
                          cyctcyc.h  -  description
                             -------------------
    begin                : mer nov 3 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYCTCYC_H
#define CYCTCYC_H

// QT
#include <QObject>
// CYLIBS
#include "cyct.h"

class CYU32;

/** @short Contrôle tolérances en cycles.
  * @author LE CLÉACH Gérald
  */

class CYCTCyc : public CYCT
{
  Q_OBJECT

public:
   /** Attr est la structure de saisie des attributs d'un groupe de contrôle tolérances en cycles. */
  struct Attr1
  {
    /** Nom du groupe (préfixe du nom des données). */
    const QString &name;
    /** Index réseau correspondant à l'index des valeurs dans la structure réseau. */
    int idNet;
    /** Valeur constructeur pour le nombre de cycles d'inhibition en chaine de caractères. */
    u32 inh_def;
    /** Valeur constructeur pour le nombre de cycles maximum de stabilisation en chaine de caractères. */
    u32 stab_def;
    /** Valeur constructeur pour le nombre de cycles maximum d'alerte avant défaut. */
    u32 ale_def;
    /** Valeur constructeur pour le défaut si temps maxi hors tolerance d'alerte atteint. */
    flg  fl_arret_ale_def;
    /** Valeur constructeur pour le défaut si hors tolerance de défaut. */
    flg  fl_arret_def_def;
    /** Etiquette du contrôle (nom du groupe des données relatives). */
    QString label;
    /** Numéro du format d'affichage dans l'application de la mesure. */
    int format;
    /** Accès au paramètrage. */
    QString settings;
  };

   /** Attr est la structure de saisie des attributs d'un groupe de contrôle tolérances en cycles. */
  struct Attr2
  {
    /** Nom du groupe (préfixe du nom des données). */
    const QString &name;
    /** Index réseau correspondant à l'index des valeurs dans la structure réseau. */
    int idNet;
    /** Valeur constructeur pour le nombre de cycles d'inhibition en chaine de caractères. */
    u32 inh_def;
    /** Valeur minimum pour le nombre de cycles d'inhibition en chaine de caractères. */
    u32 inh_min;
    /** Valeur maximum pour le nombre de cycles d'inhibition en chaine de caractères. */
    u32 inh_max;
    /** Valeur constructeur pour le nombre de cycles maximum de stabilisation en chaine de caractères. */
    u32 stab_def;
    /** Valeur minimum pour le nombre de cycles maximum de stabilisation en chaine de caractères. */
    u32 stab_min;
    /** Valeur maximum pour le nombre de cycles maximum de stabilisation en chaine de caractères. */
    u32 stab_max;
    /** Valeur constructeur pour le nombre de cycles maximum d'alerte avant défaut. */
    u32 ale_def;
    /** Valeur minimum pour le nombre de cycles maximum d'alerte avant défaut. */
    u32 ale_min;
    /** Valeur maximum pour le nombre de cycles maximum d'alerte avant défaut. */
    u32 ale_max;
    /** Valeur constructeur pour le défaut si temps maxi hors tolerance d'alerte atteint. */
    flg  fl_arret_ale_def;
    /** Valeur constructeur pour le défaut si hors tolerance de défaut. */
    flg  fl_arret_def_def;
    /** Etiquette du contrôle (nom du groupe des données relatives). */
    QString label;
    /** Numéro du format d'affichage dans l'application de la mesure. */
    int format;
    /** Accès au paramètrage. */
    QString settings;
  };

 /** Création d'un groupe de contrôle tolérances en cycles.
    * @param db               Base de données parent.
    * @param name             Nom du groupe (préfixe du nom des données).
    * @param idNet            Index réseau correspondant à l'index des valeurs dans la structure réseau.
    * @param values           Pointe sur les valeurs.
    * @param inh_def          Valeur constructeur pour le nombre de cycles d'inhibition.
    * @param stab_def         Valeur constructeur pour le nombre de cycles maximum de stabilisation.
    * @param ale_def          Valeur constructeur pour le nombre de cycles maximum d'alerte avant défaut.
    * @param fl_arret_ale_def Valeur constructeur pour le défaut si temps maxi hors tolerance d'alerte atteint.
    * @param fl_arret_def_def Valeur constructeur pour le défaut si hors tolerance de défaut.
    * @param label            Etiquette du contrôle  (nom du groupe des données relatives).
    * @param format           Numéro du format d'affichage dans l'application de la mesure.
    * @param settings         Accès au paramètrage. */
  CYCTCyc(CYDB *db, const QString &name, int idNet, t_ct *values, u32 inh_def, u32 stab_def, u32 ale_def, flg fl_arret_ale_def, flg fl_arret_def_def, QString label, int format=0, QString settings=0);

 /** Création d'un groupe de contrôle tolérances en cycles.
    * @param db     Base de données parent.
    * @param name   Nom du groupe (préfixe du nom des données).
    * @param values Pointe sur les valeurs.
    * @param attr   Attributs du groupe. */
  CYCTCyc(CYDB *db, const QString &name, t_ct *values, Attr1 *attr);

 /** Création d'un groupe de contrôle tolérances en cycles.
    * @param db     Base de données parent.
    * @param name   Nom du groupe (préfixe du nom des données).
    * @param values Pointe sur les valeurs.
    * @param attr   Attributs du groupe. */
  CYCTCyc(CYDB *db, const QString &name, t_ct *values, Attr2 *attr);

  ~CYCTCyc();

  /** @return le nombre de cycles d'inhibition avant contrôle. */
  CYU32 *inhibitionCycles() { return mInhibitionCycles; }
  /** @return le nombre de cycles maximum de stabilisation avant contrôle. */
  CYU32 *stabilisationCycles() { return mStabilisationCycles; }
  /** @return le nombre de cycles maximum hors tolérance d'alerte avant défaut. */
  CYU32 *alerteCycles() { return mAlerteCycles; }

  /** Initialise l'alerte sur max devenue défaut. */
  virtual CYEvent *initFaultAlertMax( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" > MAX (Max. alert)")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was higher than the maximum tolerated value, which produced an alert. Also the maximum number of cycles of alert has been reached and thus produced a fault."));
  /** Initialise l'alerte sur min devenue défaut. */
  virtual CYEvent *initFaultAlertMin( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" < MIN (Max. alert)")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was lower than the minimum tolerated value, which produced an alert. Also the maximum number of cycles of alert has been reached and thus produced a fault."));

private: // Private methods
  /** Initialisation */
  void init();

protected: // Protected attributes
  /** Nombre de cycles d'inhibition avant contrôle. */
  CYU32 *mInhibitionCycles;
  /** Nombre de cycles maximum de stabilisation avant contrôle. */
  CYU32 *mStabilisationCycles;
  /** Nombre de cycles maximum hors tolérance d'alerte avant défaut. */
  CYU32 *mAlerteCycles;
};

#endif
