/***************************************************************************
                          cys64.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYS64_H
#define CYS64_H

// CYLIBS
#include "cynum.h"

/** CYS64 est une donnée entière de 64 bits.
  * @short Donnée entière de 64 bits.
  * @author Gérald LE CLEACH
  */

class CYS64 : public CYNum<s64>
{
public:
  /** Création d'une donnée entière de 64 bits.
    * @param db     Base de données parent.   
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS64(CYDB *db, const QString &name, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 64 bits avec sa valeur.
    * @param db     Base de données parent.   
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS64(CYDB *db, const QString &name, s64 *val, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 64 bits avec sa valeur ainsi que sa valeur par défaut.
    * @param db     Base de données parent.   
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS64(CYDB *db, const QString &name, s64 *val, const s64 def, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 64 bits avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.   
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS64(CYDB *db, const QString &name, s64 *val, const s64 def, const s64 min, const s64 max, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 64 bits avec les attributs Attr1.
    * @param db    Base de données parent.   
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS64(CYDB *db, const QString &name, CYNum<s64>::Attr1<s64> *attr);

  /** Création d'une donnée entière de 64 bits avec les attributs Attr2.
    * @param db    Base de données parent.   
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS64(CYDB *db, const QString &name, CYNum<s64>::Attr2<s64> *attr);

  /** Création d'une donnée entière de 64 bits avec les attributs Attr3.
    * @param db    Base de données parent.   
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS64(CYDB *db, const QString &name, CYNum<s64>::Attr3<s64> *attr);

  /** Création d'une donnée entière de 64 bits avec les attributs Attr4.
    * @param db    Base de données parent.   
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS64(CYDB *db, const QString &name, CYNum<s64>::Attr4<s64> *attr);

  ~CYS64();
};

#endif
