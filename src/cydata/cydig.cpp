/***************************************************************************
                          cydig.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cydig.h"

// CYLIBS
#include "cyword.h"

CYDig::CYDig(CYDB *db, const QString &name, AttrBit *attr, int c)
: CYBool(db, name, attr->word, attr->bit, 0, attr->mode),
  mCard(c)
{
}

CYDig::CYDig(CYDB *db, const QString &name, AttrFlag1 *attr, int c)
: CYBool(db, name, attr->flag),
  mCard(c)
{
}

CYDig::CYDig(CYDB *db, const QString &name, AttrFlag2 *attr, int c)
: CYBool(db, name, attr->flag, 0, attr->mode),
  mCard(c)
{
}

CYDig::CYDig(CYDB *db, const QString &name, CYWord *word, short bit, int c)
: CYBool(db, name, word, bit),
  mCard(c)
{
}

CYDig::CYDig(CYDB *db, const QString &name, flg *flag, int c)
: CYBool(db, name, flag),
  mCard(c)
{
}

CYDig::~CYDig()
{
}
