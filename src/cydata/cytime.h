/***************************************************************************
                          cytime.h  -  description
                             -------------------
    début                  : mer mai 7 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYTIME_H
#define CYTIME_H

// QT
#include <QObject>
// CYLIBS
#include "cyu32.h"

/** CYTime est représente une donnée temporelle.
  * Sa valeur peut être donnée en heure, minute, seconde ou milli-seconde.
  * @short Donnée temporelle.
  * @author Gérald LE CLEACH
  */

class CYTime : public CYU32
{
  Q_OBJECT
public:
  /** Création d'une donnée temporelle avec sa valeur.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Format d'affichage temporel.
    * @param base   Base de temps en ms (ex 1000.0 = 1 sec).
    * @param mode   Mode d'utilisation. */
   CYTime(CYDB *db, const QString &name, u32 *val, Cy::TimeFormat format, double base=BASE_MSEC, Mode m=Sys);

  /** Création d'une donnée temporelle avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut (ex: "0h10m5s", "10m5s", "05s0ms").
    * @param min    Valeur mini (ex: "0h0m0s", "0m0s", "0s250ms").
    * @param max    Valeur maxi (ex: "10h0m0s", "150m0s", "150s0ms").
    * @param base   Base de temps en ms (ex 1000.0 = 1 sec).
    * @param mode   Mode d'utilisation. */
   CYTime(CYDB *db, const QString &name, u32 *val, QString def, QString min, QString max, double base=BASE_MSEC, Mode m=Sys);

  /** Création d'une donnée temporelle avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Format d'affichage temporel.
    * @param base   Base de temps en ms (ex 1000.0 = 1 sec).
    * @param mode   Mode d'utilisation. */
   CYTime(CYDB *db, const QString &name, u32 *val, const u32 def, const u32 min, const u32 max, Cy::TimeFormat format, double base=BASE_MSEC, Mode m=Sys);

  /** Création d'une donnée temporelle en seconde avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param base  Base de temps en ms (ex 1000.0 = 1 sec).
    * @param mode  Mode d'utilisation. */
   CYTime(CYDB *db, const QString &name, u32 *val, double base=BASE_MSEC, Mode m=Sys);

  ~CYTime();

  /** @return la valeur arrondie. */
  virtual u32 val();
  /** @return la valeur sans arrondie. */
  virtual u32 real();
  /** @return la valeur maxi. */
  virtual u32 max();
  /** @return la valeur mini. */
  virtual u32 min();
  /** @return la valeur par défaut du constructeur. */
  virtual u32 def();

  /** Saisie une valeur exprimée dans la base de temps de la donnée.*/
  virtual void setVal(u32 val);
  /** Saisie une valeur exprimée en millisecondes dans la BT de la donnée.*/
  virtual void setms(u32 val);
  /** Saisie une valeur exprimée en secondes dans la BT de la donnée.*/
  virtual void sets(u32 val);
  /** Saisie une valeur exprimée en minutes dans la BT de la donnée.*/
  virtual void setmin(u32 val);
  /** Saisie une valeur exprimée en heures dans la BT de la donnée.*/
  virtual void sethr(u32 val);

  /**
   * @brief Import contrôlé d'une valeur.
   * @param val Valeur à charger
   * En cas de dépassement d'une limite de la donnée, la valeur à charger
   * est alors forcée à la valeur de la limite dépassée.
   */
  virtual void import(u32 val);
  /** Saisie la valeur par défaut du constructeur. */
  virtual void setDef(u32 val);
  /** Saisie la valeur mini. */
  virtual void setMin(u32 val);
  /** Saisie la valeur maxi. */
  virtual void setMax(u32 val);
  /** Charge la valeur constructeur. */
  virtual void designer();
  /** Vérifie la valeur par défaut du constructeur par rarpport aux limites. */
  virtual void checkDef();

  /** @return l'aide à la visualisation.
    * @param whole    Aide entière ex: pour les données numériques avec les bornes, la précision si il y en a une et la valeur par défaut.
    * @param oneLine  Aide uniquement composée de la première ligne.
		* @param withGroup	Aide avec le groupe de la donnée.
		* @param withValue	Aide avec la valeur courante. */
	virtual QString displayHelp(bool whole=true, bool oneLine=false, bool withGroup=true, bool withValue=false);
  /** @return l'aide à la saisie.
		* @param whole		  Aide entière ex: pour les données numériques avec les bornes, la précision si il y en a une.
		* @param withGroup	Aide avec le groupe de la donnée. */
  virtual QString inputHelp(bool whole=true, bool withGroup=true);

  /** @return le format d'affichage temporel. */
  Cy::TimeFormat timeFormat() { return mTimeFormat; }
  /** @return la base de temps. */
  double base() { return mBase; }
  /** Saisie la base de temps. */
  void setBase(double val) { mBase = val; }

  /** @return la valeur en cours en chaîne de caractères (ex : 864h24m56s).
    * @param whole @return la chaîne de caractères entière (ex : 0h0m56s).
                   ou seulement la minimale (ex : 56 sec).
    * @param base  Pas utilisée dans cette classe (permet uniquement d'assurrer l'enrichissement de la méthode).
    */
  QString toString(bool whole=false, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);
  /** @return la valeur \a val en chaîne de caractères (ex : 864h24m56s).
    * @param base  Pas utilisée dans cette classe (permet uniquement d'assurrer l'enrichissement de la méthode).
    */
  QString toString(double val, bool whole=false, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);
  /** @return la chaîne de caractères d'affichage de défaut. */
  virtual QString stringDef();
  /** Saisie la chaîne de caractères d'affichage de défaut.
      Saisir 0 pour annuler cette gestion. */
  virtual void setStringDef(QString txt) { mStringDef = txt; }

  /** @return le préfixe courant. Celui-ci est définie en fonction du format de la donnée
    * et de la section en cours de traîtement.
    * Par exemple si la valeur vaut 350000 avec une base de temps en secondes nous avons :
    *   - 97  heures
    *   - 13  minutes
    *   - 19  secondes
    *   - 800 millisecondes
    * Si la section en cours de traîtement vaut \a Minutes et que le format est \a hms la
    * chaînes de caractères retournées comme préfixe sera :
    *   => "97h". */
  virtual QString prefix();

  /** @return le suffixe courant. Celui-ci est définie en fonction du format de la donnée
    * et de la section en cours de traîtement.
    * Par exemple si la valeur vaut 350000 avec une base de temps en secondes nous avons :
    *   - 97  heures
    *   - 13  minutes
    *   - 19  secondes
    *   - 800 millisecondes
    * Si la section en cours de traîtement vaut \a Minutes et que le format est \a hms la
    * chaînes de caractères retournées comme suffixe sera :
    *   => "m19s". */
  virtual QString suffix();

  /** @return la section de la tempo en traîtement. */
  CYData::Section section();
  /** Saisie la section de la tempo à traîter. */
  void setSection(CYData::Section section);
  /** @return \a true si la section courrante est la section maxi de traîtement de la tempo. */
  bool isSectionMaxi();
  /** Passe à la section maxi de traîtement de la tempo.
    * @param val Mise à jour de la valeur de la section courante avant changement de section. */
  bool sectionMaxi(const double val);
  /** Passe à la section mini de traîtement de la tempo.
    * @param val Mise à jour de la valeur de la section courante avant changement de section. */
  bool sectionMini(const double val);
  /** Passe à la section suppérieure de traîtement de la tempo.
    * @param val Mise à jour de la valeur de la section courante avant changement de section. */
  bool sectionUp(const double val);
  /** Passe à la section inférieure de traîtement de la tempo.
    * @param val Mise à jour de la valeur de la section courante avant changement de section. */
  bool sectionDown(const double val);
  /** Passe à la section de traîtement de la tempo située à droite du texte \a left. */
  bool section(QString left);

  /** Saisie une valeur de section de tempo en cours de traîtement. */
  void setValSection(const int value);
  /** @return la valeur de la section de tempo en cours de traîtement. */
  int valSection();
  /** @return la valeur par défaut du constructeur de la section de tempo en cours de traîtement. */
  int defSection();
  /** @return la valeur mini de la section de tempo en cours de traîtement. */
  int minSection();
  /** @return la valeur mini de la section de tempo en cours de traîtement. */
  int maxSection();
  /** @return la précision de la section de tempo en cours de traîtement. */
  double precisionSection();

  /** Saisie une valeur temporaire. */
  void setTmp(const double val);
  /** @return la valeur temporaire. */
  double tmp();

  /** Vérifie si la tempo respecte les limites de la données. */
  bool isValid();
  /** @return le nombre de sections de tempo. */
  int nbSections();

  /** @return les heures de la valeur temporaire. */
  int hours();
  /** @return les minutes de la valeur temporaire. */
  int minutes();
  /** @return les secondes de la valeur temporaire. */
  int seconds();
  /** @return les millisecondes de la valeur temporaire. */
  int milliseconds();

  /** @return les heures de la valeur \a val. */
  int hours(const double val);
  /** @return les minutes de la valeur \a val. */
  int minutes(const double val);
  /** @return les secondes de la valeur \a val. */
  int seconds(const double val);
  /** @return les millisecondes de la valeur \a val. */
  int milliseconds(const double val);

  /** @return la valeur temporaire en heures. */
  double h();
  /** @return la valeur temporaire en minutes. */
  double m();
  /** @return la valeur temporaire en secondes. */
  double s();
  /** @return la valeur temporaire en millisecondes. */
  double ms();
  /** @return la valeur \a val en millisecondes. */
  double ms(const double val);

public slots: // Public slots
  /** Saisie le nombre d'heures. */
  void setHours(const int val);
  /** Saisie le nombre de minutes. */
  void setMinutes(const int val);
  /** Saisie le nombre de millisecondes. */
  void setMilliseconds(const int val);
  /** Saisie le nombre de secondes. */
  void setSeconds(const int val);

protected: // Protected methods
  /** @return la valeur \a val transformée en fonction de
    * la base et de l'unité. */
  u32 input(const u32 val);
  /** @return la valeur \a val transformée en fonction de
    * la base et de l'unité. */
  u32 output(const u32 val);

private: // Private methods
  /** Initialisation de la donnée. */
  void init();
  /** Calcule les valeurs des différentes sections du temps. */
  void sections();
  /** @return la valeur temporelle correspondant à la chaîne
    * de caractères \a string et en déduit le format de la donnée. */
  u32 stringTimeToVal(QString &string);

private: // Private attributes
  /** Section de la tempo en cours de traîtement. */
  CYData::Section mSection;
  /** Format d'affichage temporel. */
  Cy::TimeFormat mTimeFormat;
  /** Format d'affichage temporel temporaire. */
  Cy::TimeFormat mTimeFormatTmp;

  /** Base de temps. */
  double mBase;

  /** Nombre d'heures */
  int mHours;
  /** Nombre de minutes. */
  int mMinutes;
  /** Nombre de secondes. */
  int mSeconds;
  /** Nombre de millisecondes. */
  int mMilliseconds;
};

#endif
