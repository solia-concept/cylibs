#include "cydatachoice.h"

CYDataChoice::CYDataChoice(QObject *parent) :
  QObject(parent)
{
}

//! Assignment operator
CYDataChoice &CYDataChoice::operator=( const CYDataChoice & other )
{
  value = other.value;
  label = other.label;
  pixmap= other.pixmap;
  return *this;
}
