/***************************************************************************
                          cyu32.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYU32_H
#define CYU32_H

// CYLIBS
#include "cynum.h"

/** CYU32 est une donnée entière non-signée de 32 bits.
  * @short Donnée entière de 32 bits.
  * @author Gérald LE CLEACH
  */

class CYU32 : public CYNum<u32>
{
public:
  /** Création d'une donnée entière non-signée de 32 bits.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU32(CYDB *db, const QString &name, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière non-signée de 32 bits avec sa valeur.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU32(CYDB *db, const QString &name, u32 *val, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 32 bits avec sa valeur ainsi que sa valeur par défaut.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU32(CYDB *db, const QString &name, u32 *val, const u32 def, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 32 bits avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU32(CYDB *db, const QString &name, u32 *val, const u32 def, const u32 min, const u32 max, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière non-signée de 32 bits avec les attributs Attr1.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU32(CYDB *db, const QString &name, CYNum<u32>::Attr1<u32> *attr);

  /** Création d'une donnée entière non-signée de 32 bits avec les attributs Attr2.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU32(CYDB *db, const QString &name, CYNum<u32>::Attr2<u32> *attr);

  /** Création d'une donnée entière non-signée de 32 bits avec les attributs Attr3.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU32(CYDB *db, const QString &name, CYNum<u32>::Attr3<u32> *attr);

  /** Création d'une donnée entière non-signée de 32 bits avec les attributs Attr4.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU32(CYDB *db, const QString &name, CYNum<u32>::Attr4<u32> *attr);

  ~CYU32();

  /** @return le nombre de chiffres total. */
  virtual int nbDigit(Cy::NumBase base=Cy::BaseData);

   /** @return la valeur courrante en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(bool withUnit=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

  virtual QString toString(double val, bool withUnit, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);
};

#endif
