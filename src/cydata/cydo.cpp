/***************************************************************************
                          cydo.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cydo.h"
#include "cydb.h"

CYDO::CYDO(CYDB *db, const QString &name, AttrBit *attr, int c)
  : CYDig(db, name, attr, c)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Digital output"));
  mLabel = attr->elec;
  mElec  = attr->elec;
  if (mMode == Cy::Free)
  {
    mPhys  = tr("Unplugged");
    mHelp  = "-----";
  }
  else
  {
    mPhys  = attr->phys;
    mHelp  = attr->help;
  }
}

CYDO::CYDO(CYDB *db, const QString &name, AttrFlag1 *attr, int c)
  : CYDig(db, name, attr, c)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Digital output"));
  mLabel = attr->phys;
  mPhys  = attr->phys;
  mHelp  = attr->help;
}

CYDO::CYDO(CYDB *db, const QString &name, AttrFlag2 *attr, int c)
  : CYDig(db, name, attr, c)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Digital output"));

  mLabel = attr->elec;
  mElec  = attr->elec;
  if (mMode == Cy::Free)
  {
    mPhys  = tr("Unplugged");
    mHelp  = "-----";
  }
  else
  {
    mPhys  = attr->phys;
    mHelp  = attr->help;
  }
}

CYDO::CYDO(CYDB *db, const QString &name, flg *flag, Cy::Mode mode, QString elec, QString phys, QString help, int c)
  : CYDig(db, name, flag, c)
{
   mMode=mode;
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Digital output"));
  mLabel = elec;
  mElec  = elec;
  if (mMode == Cy::Free)
  {
    mPhys  = tr("Unplugged");
    mHelp  = "-----";
  }
  else
  {
    mPhys  = phys;
    mHelp  = help;
  }
}
CYDO::CYDO(CYDB *db, const QString &name, CYWord *word, short bit, int c)
  : CYDig(db, name, word, bit, c)
{
}

CYDO::CYDO(CYDB *db, const QString &name, flg *flag, int c)
  : CYDig(db, name, flag, c)
{
}

CYDO::~CYDO()
{
}

void CYDO::initForcing(CYBool *forcing)
{
  mDataForcing = (CYData *)forcing;
  mDataForcing->setPhys(phys());
  mDataForcing->setAddr(addr());
  mDataForcing->setLabel(label());
  mDataForcing->setHelp(help());
}

bool CYDO::forcing()
{
  if(mDataForcing)
  {
    CYBool *d = (CYBool *)mDataForcing;
    return d->val();
  }
  else
    return false;
}

void CYDO::setForcing(bool val)
{
  if(mDataForcing)
  {
    CYBool *d = (CYBool *)mDataForcing;
    d->setVal(val);
  }
}

bool CYDO::writeForcing()
{
  if(mDataForcing)
  {
    CYBool *d = (CYBool *)mDataForcing;
    return d->writeData();
  }
  else
  {
    CYFATALDATA(objectName());
    return false;
  }
}

CYDB *CYDO::dbForcing()
{
  if(mDataForcing)
    return mDataForcing->db();
  else
  {
    CYFATALDATA(objectName());
    return 0;
  }
}

void CYDO::initForcingValue()
{
  if(mDataForcing)
  {
    CYBool *d = (CYBool *)mDataForcing;
    d->setVal(val());
  }
}
