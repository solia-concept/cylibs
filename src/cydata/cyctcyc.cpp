/***************************************************************************
                          cyctcyc.cpp  -  description
                             -------------------
    begin                : mer nov 3 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyctcyc.h"

// CYLIBS
#include "cycore.h"
#include "cyu32.h"
#include "cyflag.h"

CYCTCyc::CYCTCyc(CYDB *db, const QString &name, int idNet, t_ct *values, u32 inh_def, u32 stab_def, u32 ale_def, flg fl_arret_ale_def, flg fl_arret_def_def, QString label, int format, QString settings)
  : CYCT(db, name, idNet, values, fl_arret_ale_def, fl_arret_def_def, label, format, settings)
{
  mInhibitionCycles = new CYU32(db, QString("%1_INH").arg(name), &values->inh, inh_def, 0, 100);
  mStabilisationCycles = new CYU32(db, QString("%1_STAB").arg(name), &values->stab, stab_def, 0, 100);
  mAlerteCycles = new CYU32(db, QString("%1_ALE").arg(name), &values->ale, ale_def, 0, 10);
  init();
}

CYCTCyc::CYCTCyc(CYDB *db, const QString &name, t_ct *values, Attr1 *attr)
  : CYCT(db, name, attr->idNet, values, attr->fl_arret_ale_def, attr->fl_arret_def_def, attr->label, attr->format, attr->settings)
{
  mInhibitionCycles = new CYU32(db, QString("%1_INH").arg(name), &values->inh, attr->inh_def, 0, 100);
  mStabilisationCycles = new CYU32(db, QString("%1_STAB").arg(name), &values->stab, attr->stab_def, 0, 100);
  mAlerteCycles = new CYU32(db, QString("%1_ALE").arg(name), &values->ale, attr->ale_def, 0, 10);
  init();
}

CYCTCyc::CYCTCyc(CYDB *db, const QString &name, t_ct *values, Attr2 *attr)
  : CYCT(db, name, attr->idNet, values, attr->fl_arret_ale_def, attr->fl_arret_def_def, attr->label, attr->format, attr->settings)
{
  mInhibitionCycles = new CYU32(db, QString("%1_INH").arg(name), &values->inh, attr->inh_def, attr->inh_min, attr->inh_max);
  mStabilisationCycles = new CYU32(db, QString("%1_STAB").arg(name), &values->stab, attr->stab_def, attr->stab_min, attr->stab_max);
  mAlerteCycles = new CYU32(db, QString("%1_ALE").arg(name), &values->ale, attr->ale_def, attr->ale_min, attr->ale_max);
  init();
}

CYCTCyc::~CYCTCyc()
{
}

void CYCTCyc::init()
{
  mInhibitionCycles->setLabel(tr("Number of inhibition cycles before control (T0)"));
  mInhibitionCycles->addNote(tr("The minimum number of cycles before control is T0"));
  mInhibitionCycles->addNote(tr("The maximum number of cycles before control is T0+T1"));
  mInhibitionCycles->setGroup(mGroup);

  mStabilisationCycles->setLabel(tr("Supplementary cycles before control (T1)"));
  mStabilisationCycles->setHelp(tr("The control start when the measure in inside the tolerances, or when this optional inhibition is overreached"));
  mStabilisationCycles->addNote(tr("The minimum number of cycles before control is T0"));
  mStabilisationCycles->addNote(tr("The maximum number of cycles before control is T0+T1"));
  mStabilisationCycles->setGroup(mGroup);

  mAlerteStop->setLabel(tr("Stop test if maximum alert ?"));
  mAlerteStop->setHelp(0, tr("The bench will be stopped if the max number of cycles alert is reached."));
  mAlerteStop->setGroup(mGroup);

  mAlerteCycles->setLabel(tr("Maximum number of cycles out of alert tolerances"));
  mAlerteCycles->setHelp(tr("Maximum outside tolerances number of cycles for control."));
  mAlerteCycles->addNote(tr("If reached => fault!"));
  mAlerteCycles->setGroup(mGroup);

  mFaultStop->setLabel(tr("Stop test if out of fault tolerances ?"));
  mFaultStop->setHelp(0, tr("The bench will be stopped if fault tolerance is reached."));
  mFaultStop->setGroup(mGroup);
}

CYEvent *CYCTCyc::initFaultAlertMax(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  return CYCT::initFaultAlertMax(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
}


CYEvent *CYCTCyc::initFaultAlertMin(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  return CYCT::initFaultAlertMin(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
}
