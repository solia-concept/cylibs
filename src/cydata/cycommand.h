/***************************************************************************
                          cycommand.h  -  description
                             -------------------
    début                  : jeu jun 19 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCOMMAND_H
#define CYCOMMAND_H

#include <cyflag.h>

class CYEvent;

/** CYCommand est une commande bistable/monostable.
  * @short Commande bistable/monostable.
  * @author Gérald LE CLEACH
  */

class CYCommand : public CYFlag
{
  Q_OBJECT
public:
  /** Attr1 est la structure de saisie des attributs d'une commande bistable/monostable. */
  struct Attr1
  {
    /** Nom. */
    const QString &name;
    /** Pointe sur la valeur physique. */
    flg *val;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Etiquette. */
    QString label;
    /** Aide. */
    QString help;
  };

  /** Attr2 est la structure de saisie des attributs d'une commande bistable/monostable. */
  struct Attr2
  {
    /** Nom. */
    const QString &name;
    /** Index. */
    int index;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Etiquette. */
    QString label;
    /** Aide. */
    QString help;
  };

  /** Attr3 est la structure de saisie des attributs d'une commande bistable/monostable avec gestion d'évènement. */
  struct Attr3
  {
    /** Nom. */
    const QString &name;
    /** Index. */
    int index;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Vaut \a true si la commande génère un évènement. */
    bool withEvent;
    /** Etiquette. */
    QString label;
    /** Aide. */
    QString help;
  };

  /** Création d'une commande bistable/monostable.
    * @param db   Base de données parent.
    * @param name Nom de la donnée.
    * @param val  Pointe sur la valeur physique.
    * @param mode Mode d'utilisation (Bistable/Monostable). */
  CYCommand(CYDB *db, const QString &name, flg *val, Mode mode);

  /** Création d'une commande bistable/monostable.
    * @param db   Base de données parent.
    * @param name Nom de la donnée.
    * @param attr Attributs de la donnée. */
  CYCommand(CYDB *db, const QString &name, Attr1 *attr);

  /** Création d'une commande bistable/monostable.
    * @param db   Base de données parent.
    * @param name Nom de la donnée.
    * @param val  Pointe sur la valeur physique grâce à l'index.
    * @param attr Attributs de la donnée. */
  CYCommand(CYDB *db, const QString &name, flg *val, Attr2 *attr);

  /** Création d'une commande bistable/monostable.
    * @param db   Base de données parent.
    * @param name Nom de la donnée.
    * @param val  Pointe sur la valeur physique grâce à l'index.
    * @param attr Attributs de la donnée. */
  CYCommand(CYDB *db, const QString &name, flg *val, Attr3 *attr);

  ~CYCommand();

  /** @return l'état. */
  bool state();

  /** @return le type physique. */
  virtual QString physicalType();

  /** Ecriture de la donnée. */
  virtual bool writeData();

  bool withEvent();
  void setEvent( CYEvent *event );
  CYEvent *event();

public slots: // Public slots
  /** Met à l'état actif. */
  void setOn();
  /** Met à l'état désactif. */
  void setOff();
    void postWrited();
  
private: // Private methods
  /** Initialise la donnée. */
  void init();

protected:
    bool mWithEvent;
    CYEvent *mEvent;
};

#endif
