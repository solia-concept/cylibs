/***************************************************************************
                          cyao.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyao.h"

// CYLIBS
#include "cyflag.h"
#include "cyadc16.h"

CYAO::CYAO(CYDB *db, const QString &name, Attr16 *attr, int c)
  : CYAna(db, name, attr, c, false)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Analog output"));
}

CYAO::CYAO(CYDB *db, const QString &name, Attr32 *attr, int c)
  : CYAna(db, name, attr, c, false)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Analog output"));
}

CYAO::CYAO(CYDB *db, const QString &name, Attr16ADC *attr, int c)
  : CYAna(db, name, attr, c, false)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Analog output"));
}

CYAO::CYAO(CYDB *db, const QString &name, Attr32ADC *attr, int c)
  : CYAna(db, name, attr, c, false)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Analog output"));
}

CYAO::CYAO(CYDB *db, const QString &name, f32 *val, s16 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int c)
  : CYAna(db, name, val, adc, flg, elec, phys, mode, format, ldef, tdef, label, c, false)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Analog output"));
}

CYAO::CYAO(CYDB *db, const QString &name, f32 *val, s32 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int c)
  : CYAna(db, name, val, adc, flg, elec, phys, mode, format, ldef, tdef, label, c, false)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Analog output"));
}

CYAO::~CYAO()
{
}
