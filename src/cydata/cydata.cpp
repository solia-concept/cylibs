/***************************************************************************
                          cydata.cpp  -  description
                             -------------------
    début                  : Wed Mar 12 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cydata.h"

// ANSI
#include <math.h>
// QT
#include <QPixmap>
#include <QRegularExpression>
// CYLIBS
#include "cydb.h"
#include "cyflag.h"
#include "cycore.h"
#include "cyformat.h"
#include "cys8.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cystring.h"
#include "cybool.h"
#include "cyword.h"
#include "cycolor.h"
#include "cyprofibusitem.h"
#include "cyevent.h"
#include "cymeasuresetting.h"
#include "cydiscontbuffering.h"
#include "cymessagebox.h"

CYData::CYData(CYDB *db, const QString &name, int format, Mode mode, Cy::ValueType type)
  : QObject((QObject *)db),
    mDB(db),
    mNumFormat(format),
    mMode(mode),
    mType(type)
{
  setObjectName(db->prefixName()+name+db->suffixName());
  init();
}

CYData::~CYData()
{
  delete mFormat;
  // TOCHECK QT5
  //  delete [] mBuffer;
  //  delete [] mBurstsBuffer;
  //  delete [] mTriggerBuffer;
}

QByteArray CYData::dataName()
{
  return objectName().toUtf8();
}

void CYData::init()
{
  mCfg       = 0;
  mFl        = 0;
  mRising    = false;
  mIsNum     = true;
  mIsInt     = true;
  mIsSigned  = true;
  mIsCnt     = false;
  mIsTim     = false;
  mIsTmp     = true;
  mIsFlag    = false;
  mPhys      = "";
  mElec      = "";

  mHelp      = "";
  mAlwaysOk  = false;
  mVolatile  = false;
  mFormat    = 0;
  mFormatData = 0;

  mAdc16 = 0;
  mAdc32 = 0;
  mDataForcing = 0;

  mResetData     = 0;
  mPartialData   = 0;
  mThresholdData = 0;
  mNoteData      = 0;

  mTmp =0.0;

  //   mBuffer     = 0;
  mUseBuffer  = false;
  mInitBufferVal = (core) ? core->initBufferVal() : NAN;
  mIdBuffer   = 0;
  mBufferFull = false;
  discontBufferState = Filling;
  mTableBufferNb  = 0;
  mTableBufferCpt = 0;
  mTableBuffering = false;
  mManualBuffer = false;

  mDiscontBuffering = 0;
  mDiscontBufferId = 0;
  mDiscontBufferNum = 0;

  mBurstsBufferFlag = 0;
  mIdBurstsBuffer   = 0;
  mBurstsBufferSize = 0;
  mBurstsRatio      = 0;
  mBurstsBufferState= false;
  mCptBurstsBuffer  = 0;
  mBurstsBufferFull = false;

  mChoicetrue  = tr("Yes");
  mChoicefalse = tr("No");

  mEnableColor  = Qt::white;
  mDisableColor = Qt::gray;
  mInputColor   = Qt::black;

  mAutoMinMax = false;
  mAutoMinMaxFormat = true;

  mProfibusInfo = 0;

  mHelpDesignerValueFromDB = true;
  mHelpDesignerValue       = false;

  mNotSpecialValueText = false;

  mNumBase = Decimal;

  mChoiceDictHelp = true;

  mSetting =0;
  mTriggerEnabled = false;
  mTriggerState = false;
  mTriggerIdBuffer = -1;
  mPostTriggerIdBuffer = -1;
  mExtTrigger = 0;

  mPrefixLabel = mDB->prefixLabel();
  mSuffixLabel = mDB->suffixLabel();

  // simulation
  mSimulType = SimulType::None;
  mSimulPeriode = 500.0;
  mSimulPhase = 1.0;
  mSimulkA = 1.0;

  mLimitedIncrease = 0.0;
  mLimitedDecrease = 0.0;

  setFormat(mNumFormat);

  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());

  initType();
  addToDB();

  connect(mDB, SIGNAL(datasRead()), this, SLOT(detectValueChanged()));
  connect(this, SIGNAL(valueChanged()), mDB, SLOT(setModified()));
}

void CYData::addToDB()
{
  if (mDB->isGlobal())
  {
    if (CYData *d = mDB->allDatas().value(objectName()))
      CYFATALTEXT(QString("DB1: %1 DB2: %2 => NOM DE DONNEE '%3' EXISTE DEJA DANS LA BASE DE DONNEES").arg(d->db()->objectName()).arg(mDB->objectName()).arg(objectName()));
  }
  else
  {
    if (mDB->datas.value(objectName()))
      CYFATALTEXT(QString("NOM DE DONNEE '%1' EXISTE DEJA DANS LA BASE DE DONNEES '%2'").arg(objectName()).arg(mDB->objectName()));
  }

  mDB->addToDB(this);
}

bool CYData::variationTooAbrupt(double newVal)
{
  double val =valToDouble();
  double limit;

  QString caption = QString("<b>%1: %2</b><hr>").arg(group(false)).arg(label());

  limit = limitedIncrease();
  if (limit<0.0)
    limit *= -1.0; // inversion de sens
  if (limit>0.0)
  {
    if (newVal > cyVariation(val, limit))
    {
      CYMessageBox::sorry(0, caption+
                          tr("The increase in value is too abrupt. "
                             "The value of this data can only be increased by a maximum of %1\% of its current value.")
                          .arg(limit));
      return true;
    }
  }

  limit = limitedDecrease();
  if (limit>0.0)
    limit *= -1.0; // inversion de sens
  if (limit<0.0)
  {
    if (newVal < cyVariation(val, limit))
    {
      CYMessageBox::sorry(0, caption+
                          tr("The decrease in value is too abrupt. "
                             "The value of this data can only be decreased by a maximum of %1\% of its current value.")
                          .arg(limit));
      return true;
    }
  }
  return false;
}

void CYData::setFlag(CYFlag *flag)
{
  if (flag->group().isEmpty())
    flag->setGroup( group() + ":" + tr("Control") );
  if (flag->label().isEmpty())
    flag->setLabel( label() );

  mFl = flag;
}

QString CYData::hostsName()
{
  return mDB->hostsName();
}

QString CYData::linksName()
{
  return mDB->linksName();
}

void CYData::calculNbDigits()
{
  emit formatUpdated();
}

QString CYData::unit()
{
  return mFormat->unit();
}

QString CYData::physical()
{
  return mFormat->physical();
}

QString CYData::physicalType()
{
  return mFormat->physicalType();
}

int CYData::nbDigit(Cy::NumBase base)
{
  Q_UNUSED(base)
  return mFormat->nbDigit();
}

int CYData::nbDec()
{
  return mFormat->nbDec();
}

double CYData::precision()
{
  return mFormat->precision();
}

void CYData::setExponential(bool val)
{
  mFormat->setExponential(val);
}

bool CYData::isExponential()
{
  return mFormat->isExponential();
}

void CYData::copyFormatLabel(CYData *src)
{
  setFormatLabel(src->format(), src->label());
}

void CYData::setFormatLabel(CYFormat *fmt, QString label)
{
  setLabel(label);
  setFormat(fmt);
}

void CYData::setFormatData(QString name)
{
  CYData *data = core->findData(name, false);
  if (data)
    setFormatData(data);
}

void CYData::setFormatData(CYData *data)
{
  if (mFormatData)
  {
    disconnect(mFormatData, SIGNAL(formatUpdated()), this, SLOT(updateFormatData()));
  }
  mFormatData = data;
  updateFormatData();
  connect(mFormatData, SIGNAL(formatUpdated()), this, SLOT(updateFormatData()));
}

void CYData::updateFormatData()
{
  if (mFormatData)
    setFormat(mFormatData->format());
}

void CYData::setFormat(int format)
{
  if (core)
  {
    CYFormat *fmt;
    if (!(fmt = core->formats.value(format)))
      CYFATALTEXT(QString("STR: %1 => FORMAT DE LA DONNEE '%2' INEXISTANT").arg(mDB->objectName()).arg(objectName()))
          setFormat(fmt);
  }
  else
    setFormat(new CYFormat(this, QString("FMT_%1").arg(objectName()), "", "", 0.0, INT8_MAX));
}

void CYData::setFormat(CYFormat *fmt)
{
  mSenderFormat = fmt;

  if (!mFormat)
  {
    mFormat = new CYFormat(this, QString("%1_%2").arg(objectName()).arg(fmt->objectName()), fmt->unit(), fmt->physical(),
                           fmt->min(), fmt->max(), fmt->nbDec(), fmt->precision(),
                           fmt->bufferSize(true) , fmt->acquisitionTime(true) ,
                           fmt->bufferSize(false), fmt->acquisitionTime(false), fmt->sampleSize(false),
                           fmt->burstsBufferSize(), fmt->burstsRatio());
    mFormat->setExponential(fmt->isExponential());
  }
  else
    mFormat->copy(fmt);

  if (fmt->physical()=="")
  {
    switch (mType)
    {
      case VU8    : mFormat->setMin(0);
        mFormat->setMax( (fmt->max()<UINT8_MAX) ? fmt->max() : UINT8_MAX );
        mFormat->setPhysical(tr("Integer"));
        mFormat->setNbDec(0);
        break;
      case VS8    : if (isFlag())
        {
          mFormat->setMin(0);
          mFormat->setMax(1);
          mFormat->setPhysical(tr("Boolean"));
        }
        else
        {
          mFormat->setMin( (fmt->min()>INT16_MIN) ? fmt->min() : INT16_MIN );
          mFormat->setMax( (fmt->max()<INT16_MAX) ? fmt->max() : INT16_MAX );
          mFormat->setPhysical(tr("Integer"));
        }
        mFormat->setNbDec(0);
        break;
      case VU16   : mFormat->setMin(0);
        mFormat->setMax( (fmt->max()<UINT16_MAX) ? fmt->max() : UINT16_MAX );
        mFormat->setPhysical(tr("Integer"));
        mFormat->setNbDec(0);
        break;
      case VS16   : mFormat->setMin( (fmt->min()>INT16_MIN) ? fmt->min() : INT16_MIN );
        mFormat->setMax( (fmt->max()<INT16_MAX) ? fmt->max() : INT16_MAX );
        mFormat->setPhysical(tr("Integer"));
        mFormat->setNbDec(0);
        break;
      case VU32   : mFormat->setMin(0);
        mFormat->setMax( (fmt->max()<UINT32_MAX) ? fmt->max() : UINT32_MAX );
        mFormat->setPhysical(tr("Integer"));
        mFormat->setNbDec(0);
        break;
      case VS32   : mFormat->setMin( (fmt->min()>INT32_MIN) ? fmt->min() : INT32_MIN );
        mFormat->setMax( (fmt->max()<INT32_MAX) ? fmt->max() : INT32_MAX );
        mFormat->setPhysical(tr("Integer"));
        mFormat->setNbDec(0);
        break;
      case VU64   : mFormat->setMin(0);
        mFormat->setMax( (fmt->max()<UINT64_MAX) ? fmt->max() : UINT64_MAX );
        mFormat->setPhysical(tr("Integer"));
        mFormat->setNbDec(0);
        break;
      case VS64   : mFormat->setMin( (fmt->min()>INT64_MIN) ? fmt->min() : INT64_MIN );
        mFormat->setMax( (fmt->max()<INT64_MAX) ? fmt->max() : INT64_MAX );
        mFormat->setPhysical(tr("Integer"));
        mFormat->setNbDec(0);
        break;
      case VF32   :
      case VF64   : mFormat->setMin((-1e8)/(pow(10, mFormat->nbDec())));
        mFormat->setMax((1e8)/(pow(10, mFormat->nbDec())));
        mFormat->setPhysical(tr("Floating"));
        break;
      case Time   : mFormat->setPhysical(tr("Time"));
        mFormat->setNbDec(0);
        break;
      case Word   : mFormat->setPhysical(tr("Word"));
        mFormat->setNbDec(0);
        break;
      case Bool   : mFormat->setPhysical(tr("Boolean"));
        mFormat->setNbDec(0);
        break;
      case String : mFormat->setPhysical(tr("Text"));
        mFormat->setNbDec(0);
        break;
      case Color  : mFormat->setPhysical(tr("Color"));
        mFormat->setNbDec(0);
        break;
      case VFL    :
        mFormat->setMin(0);
        mFormat->setMax(1);
        mFormat->setPhysical(tr("Boolean"));
        break;
      case Undef  :
      default     : CYFATAL;
    }
  }

  emit formatUpdated();
}

void CYData::setGroup(const QString g)
{
  mGroup = g;
  mGroup.replace("::", ":");
}

void CYData::setLabel(const QString l)
{
  mLabel = l;
}

QString CYData::label(bool whole)
{
  QString txt = mLabel;
  if (whole)
  {
    txt.prepend(mPrefixLabel);
    txt.append(mSuffixLabel);
  }
  return txt;
}

void CYData::setHelp(const QString h)
{
  mHelp = h;
}

void CYData::setUnit(const QString u)
{
  if (mFormat)
    mFormat->setUnit(u);
}

void CYData::setPhysical(const QString p)
{
  mFormat->setPhysical(p);
}

void CYData::setNbDigit(int nb)
{
  mFormat->setNbDigit(nb);
}

void CYData::setNbDec(int nb)
{
  mFormat->setNbDec(nb);
  calculNbDigits();
}

void CYData::setPrecision(const double precision)
{
  if (mFormat)
    mFormat->setPrecision(precision);
  else
    CYFATALDATA(objectName());
}

void CYData::setEnableColor (const QColor &c)
{
  mEnableColor = c;
  emit enableColorChanged(c);
}

void CYData::setDisableColor (const QColor &c)
{
  mDisableColor = c;
  emit disableColorChanged(c);
}

bool CYData::readData()
{
  if (mDB->isOk())
  {
    bool ret = mDB->readData();
    return ret;
  }
  else
    return false;
}

bool CYData::writeData()
{
  if (db()->isWriteMode())
    return mDB->writeData();
  else
  {
    CYMESSAGETEXT(tr("%1 cannot be wrote because not in write mode !").arg(objectName()));
    return false;
  }
}

bool CYData::isOk(bool testFlag)
{
  if (core && core->user() && core->simulation())
    return true;
  else if (testFlag && flag() && !flag()->val())
    return false;
  return (mAlwaysOk || mDB->isOk());
}

void CYData::setStringDef(QString txt)
{
  mFormat->setStringDef(txt);
}

QString CYData::stringDef()
{
  return mFormat->stringDef();
}

void CYData::initType()
{
  switch (mType)
  {
    case VU8    : mTypesString = ("u8")     ; mIsSigned = false; break;
    case VS8    : mTypesString = ("s8")     ; mIsSigned = true ; break;
    case VU16   : mTypesString = ("u16")    ; mIsSigned = false; break;
    case VS16   : mTypesString = ("s16")    ; mIsSigned = true ; break;
    case VU32   : mTypesString = ("u32")    ; mIsSigned = false; break;
    case VS32   : mTypesString = ("s32")    ; mIsSigned = true ; break;
    case VF32   : mTypesString = ("f32")    ; mIsSigned = true ; mIsInt = false; break;
    case VU64   : mTypesString = ("u64")    ; mIsSigned = false; break;
    case VS64   : mTypesString = ("s64")    ; mIsSigned = true ; break;
    case VF64   : mTypesString = ("f64")    ; mIsSigned = true ; mIsInt = false; break;
    case Time   : mTypesString = ("u32")    ; mIsSigned = false; break;
    case Word   : mTypesString = ("u32")    ; mIsSigned = false; mIsNum = false; break;
    case Bool   : mTypesString = ("bool")   ; mIsSigned = false; mIsNum = false; break;
    case String : mTypesString = ("QString"); mIsSigned = false; mIsNum = false; break;
    case Color  : mTypesString = ("QColor" ); mIsSigned = false; mIsNum = false; break;
    case VFL    : mTypesString = ("flg")     ; mIsSigned = false; break;
    case Undef  :
    default     : CYFATALDATA(objectName());
  }
}

QString CYData::title()
{
  if (core != 0)
    if (core->hostList.count()>2)
      return QString("%1: %2: %3").arg(hostsName()).arg(group()).arg(label());
  return QString("%1: %2").arg(group()).arg(label());
}

QString CYData::markupHelpData(QString name)
{
  return QString("<CYData name=\"%1\">").arg(name);
}

void CYData::replaceMarkupHelpData(QString &txt)
{
  // recherche des balises de données
  QRegularExpression regex1("<CYData(.*?)>");
  QRegularExpressionMatch match1 = regex1.match(txt);
  for (int i = 0; i <= match1.lastCapturedIndex(); ++i)
  {
    QString markup = match1.captured(i);
    if (!markup.isEmpty())
    {
      // recherche le nom de la donnée de la balise trouvée
      QRegularExpression regex2(" name=\"(.*?)\"");
      QRegularExpressionMatch match2 = regex2.match(markup);
      QString name = match2.captured(1);
      if (!name.isEmpty())
      {
        CYData *data = core->findData(name);
        // remplace la balise de donnée par l'aide en une ligne avec sa valeur
        txt.replace(markup, data->displayHelp(false, true, false, true));
      }
      else
        CYWARNINGTEXT(tr("Error of help data markup %1").arg(markup))
      // Transforme d'éventuelles balises de données restante
      replaceMarkupHelpData(txt);
    }
  }
}

QString CYData::help()
{
  QString txt = mHelp;
  if (!txt.isEmpty())
  {
    // Transforme d'éventuelles balises de données
    replaceMarkupHelpData(txt);
  }
  return txt;
}

QString CYData::helpHTML()
{
  QString txt = help();
  if (!txt.isEmpty())
  {
    txt = txt.replace("\n","<br>");
    txt = core->htmlDocBookMarkUrl(txt);
  }
  return txt;
}

QString CYData::group(bool withDBGroup)
{
  if (!withDBGroup || mDB->group().isEmpty())
    return mGroup;
  else if (mGroup.isEmpty())
    return QString("%1").arg(mDB->group());
  else
    return QString("%1:%2").arg(mDB->group()).arg(mGroup);
}

QString CYData::groupSection(Cy::GroupSection choice)
{
  switch (choice)
  {
    case No         : return "";
    case lastGroup  :
    {
      if (group().indexOf(":"))
        return group().section(":", -1);
      else
        return group();
    }
    case underGroup : return group( false );
    case wholeGroup : return group( true );
    default         : return "";
  }
}

QString CYData::displayHelp(bool whole, bool oneLine, bool withGroup, bool withValue)
{
  Q_UNUSED(whole)
  QString tmp;
  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(group()));
  tmp.append(label());
  if (phys()!="")
    tmp.append(QString(" [%1]").arg(phys()));
  if (elec()!="")
    tmp.append(QString(" [%1]").arg(elec()));
  if (addr()!="")
    tmp.append(QString(" [%1]").arg(addr()));
  if (withValue)
    tmp.append(QString(" = <b>%1</b>").arg(toString()));

  if ( !oneLine )
  {
    if (!tmp.isEmpty())
      tmp.append("<hr>");
    else
      tmp.append("<br>");
    if (help()!="")
      tmp.append(help());
    if ( !note().isEmpty() ) tmp.append("<br>"+note());
  }

  return tmp;
}

QString CYData::inputHelp(bool whole, bool withGroup)
{
  return displayHelp(whole, withGroup);
}

QString CYData::note(Qt::TextFormat format, bool header)
{
  QString tmp;
  if (format==Qt::RichText)
  {
    if (!mNotes.isEmpty())
    {
      tmp.append("<i>");

      if (header)
      {
        if (mNotes.count()==1)
          tmp.append(tr("Note :"));
        else
          tmp.append(tr("Notes :"));
      }

      tmp.append("<ul type=square>");

      for (QStringList::Iterator it = mNotes.begin(); it != mNotes.end(); ++it)
        tmp.append("<li>"+(*it)+"</li>");

      tmp.append("</i>");
      tmp.append("</ul>");
    }
  }
  else if (format==Qt::PlainText)
  {
    if (!mNotes.isEmpty())
    {
      if (header)
      {
        if (mNotes.count()==1)
          tmp.append(tr("Note :"));
        else
          tmp.append(tr("Notes :"));
        tmp.append("\n");
      }

      for (QStringList::Iterator it = mNotes.begin(); it != mNotes.end(); ++it)
        tmp.append("- \t"+(*it)+"\n");
    }
  }
  return tmp;
}

QString CYData::cydocNote()
{
  QString tmp;
  if (!mNotes.isEmpty())
  {
    tmp.append("<emphasis>");

    if (mNotes.count()==1)
      tmp.append(*mNotes.begin());
    else
    {
      tmp.append("<itemizedlist spacing=\"compact\">");
      for (QStringList::Iterator it = mNotes.begin(); it != mNotes.end(); ++it)
        tmp.append(QString("<listitem><para>%1</para></listitem>").arg(*it));
      tmp.append("</itemizedlist>");
    }
    tmp.append("</emphasis>");
  }
  return tmp;
}

QString CYData::infoCY(QColor color)
{
  QString tmp = QString("CYLIX: %1 (%2 %3)").arg(objectName()).arg(mDB->objectName()).arg(mTypesString);
  return QString("<nobr><i><font color=""%1"">").arg(color.name()) + tmp +"</font></i></nobr>";
}

void CYData::setDataCheckDef()
{
  if (core)
    core->addDataCheckDef(this);
}

void CYData::load()
{
  mDB->load();
}

void CYData::save()
{
  mDB->save();
}

float CYData::acquisitionTime()
{
  return mFormat->acquisitionTime(!isReadFast());
}

bool CYData::nbBufferingAsyncCom()
{
  int nb = 0;
  if (!mTimerBuffer.isValid())
  {
    nb = 1; // premier point à bufferiser
    mTimerBuffer.start();
    mTimerBufferCompensation = 0.0;
  }
  else
  {
    uint elapsed = mTimerBuffer.elapsed();
    elapsed += mTimerBufferCompensation;
    float acquisitionTime = mFormat->acquisitionTime(!isReadFast());
    if (elapsed >= acquisitionTime)
    {
      mTimerBuffer.restart();
      // nouveau point à bufferiser au moins une fois, voir plus en cas
      // de compensation de timer sous Windows.
      nb = elapsed / acquisitionTime;
      mTimerBufferCompensation = elapsed - nb*acquisitionTime;
    }
  }

  return nb;
}

bool CYData::isReadFast()
{
  return mDB->isReadFast();
}

double CYData::initBufferVal()
{
  return mInitBufferVal;
}

short CYData::sampleSize()
{
  return mSampleSize;
}

void CYData::setBufferVal(const int &id, const double &val)
{
  mBuffer[id] = val;
}

void CYData::useBuffer(bool val)
{
  mUseBuffer = val;

  if (mTableBufferNb) // bufferisation d'un tableau
  {
    mBufferSize = tableBufferSize();
    mSampleSize = 1;
  }
  else if (mDiscontBuffering) // bufferisation temporelle discontinue
  {
    mBufferSize = mFormat->bufferSize(!isReadFast());
    mSampleSize = mFormat->sampleSize(!isReadFast());
  }
  else if (mManualBuffer) // bufferisation manuelle du buffer
  {
    mBufferSize = mFormat->bufferSize(!isReadFast());
    mSampleSize = mFormat->sampleSize(!isReadFast());
  }
  else // bufferisation temporelle continue
  {
    mBufferSize = mFormat->bufferSize(!isReadFast())*2;
    mSampleSize = mFormat->sampleSize(!isReadFast());
  }
  mBuffer.resize(mBufferSize);
  mBuffer.fill(mInitBufferVal, mBufferSize);
}

void CYData::setBurstsBuffer(QByteArray flagName)
{
  setBurstsBuffer((CYFlag *)core->findData(flagName));
}

void CYData::setBurstsBuffer(CYFlag *flag)
{
  mBurstsBufferFlag = flag;
  // TOCHECK QT5  if (!mBurstsBuffer)
  {
    mBurstsBufferSize = mFormat->burstsBufferSize()*2;
    mBurstsBuffer.resize(mBurstsBufferSize);
    mBurstsRatio = mFormat->burstsRatio();
  }
}

bool CYData::burstsBufferEnable(int str, int i)
{
  if (mBurstsBufferFlag && mBurstsBufferFlag->sample(str, i))
  {
    return true;
  }
  else
    return false;
}

double *CYData::bufferCurrent()
{
  if (mBuffer.empty())
    return 0;
  if (mDiscontBuffering)
    return &mBuffer[0];
  return &mBuffer[mIdBuffer];
}

double *CYData::burstsBufferCurrent()
{
  if (mBurstsBuffer.empty())
    return 0;
  return &mBurstsBuffer[mIdBurstsBuffer];
}

double *CYData::bufferTrigger()
{
  if (!triggerBufferFull())
    return &mTriggerBuffer[mIdBuffer];
  else
    return &mTriggerBuffer[mPostTriggerIdBuffer];
}

void CYData::setAlwaysOk(bool state)
{
  mAlwaysOk = state;
}

bool CYData::reading()
{
  return mDB->reading();
}

bool CYData::isValid()
{
  if (mAlwaysOk)
    return true;
  if (isOk())
  {
    if (!flag())
      return true;
    else if (flag()->val())
      return true;
    else
      return false;
  }
  else
    return false;
}

void CYData::setValueChanged(bool state)
{
  if (state)
  {
    emit valueRising(mRising);
    emit valueChanged();
  }
}

void CYData::designer(QString dataName)
{
  CYData *data = core->findData(dataName);
  designer(data);
}

void CYData::designer(CYData *data)
{
  data->designer();
}

void CYData::copy(QString srcDataName, QString destDataName, bool limit)
{
  CYData *src = core->findData(srcDataName);
  CYData *dest = core->findData(destDataName);
  copy(src, dest, limit);
}

void CYData::copy(CYData *src, CYData *dest, bool limit)
{
  dest->copyFrom(src, limit);
}

void CYData::copyFrom(CYData *src, bool limit)
{
  QString error;
  copyFrom(src, error, true, limit);
}

void CYData::copyFrom(CYData *src, QString &error, bool designer, bool limit)
{
  switch (type())
  {
    case Cy::VFL    :
    {
      CYFlag *dataSrc  = (CYFlag *)src;
      CYFlag *dataDest = (CYFlag *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *dataSrc  = (CYS8 *)src;
      CYS8 *dataDest = (CYS8 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *dataSrc  = (CYS16 *)src;
      CYS16 *dataDest = (CYS16 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *dataSrc  = (CYS32 *)src;
      CYS32 *dataDest = (CYS32 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *dataSrc  = (CYS64 *)src;
      CYS64 *dataDest = (CYS64 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VU8    :
    {
      CYU8 *dataSrc  = (CYU8 *)src;
      CYU8 *dataDest = (CYU8 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *dataSrc  = (CYU16 *)src;
      CYU16 *dataDest = (CYU16 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *dataSrc  = (CYU32 *)src;
      CYU32 *dataDest = (CYU32 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *dataSrc  = (CYU64 *)src;
      CYU64 *dataDest = (CYU64 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *dataSrc  = (CYF32 *)src;
      CYF32 *dataDest = (CYF32 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *dataSrc  = (CYF64 *)src;
      CYF64 *dataDest = (CYF64 *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::Time   :
    {
      CYTime *dataSrc  = (CYTime *)src;
      CYTime *dataDest = (CYTime *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::Sec    :
    {
      CYTSec *dataSrc  = (CYTSec *)src;
      CYTSec *dataDest = (CYTSec *)this;
      if ( !limit )
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      else if ( dataSrc->val() > dataDest->max() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" > "+dataDest->toString(dataDest->max(), true, true)+"=>"+dataDest->toString() );
      }
      else if ( dataSrc->val() < dataDest->min() )
      {
        if ( designer )
          dataDest->designer();
        error.append( dataDest->label()+": "+dataSrc->toString()+" < "+dataDest->toString(dataDest->min(), true, true)+"=>"+dataDest->toString() );
      }
      else
      {
        dataDest->setOldVal(dataDest->val());
        dataDest->setVal(dataSrc->val());
      }
      break;
    }
    case Cy::String :
    {
      CYString *dataSrc  = (CYString *)src;
      CYString *dataDest = (CYString *)this;
      dataDest->setOldVal(dataDest->val());
      dataDest->setVal(dataSrc->val());
      break;
    }
    case Cy::Color  :
    {
      CYColor *dataSrc  = (CYColor *)src;
      CYColor *dataDest = (CYColor *)this;
      dataDest->setOldVal(dataDest->val());
      dataDest->setVal(dataSrc->val());
      break;
    }
    case Cy::Bool   :
    {
      CYBool *dataSrc  = (CYBool *)src;
      CYBool *dataDest = (CYBool *)this;
      dataDest->setOldVal(dataDest->val());
      dataDest->setVal(dataSrc->val());
      break;
    }
    case Cy::Word   :
    {
      CYWord *dataSrc  = (CYWord *)src;
      CYWord *dataDest = (CYWord *)this;
      dataDest->setOldVal(dataDest->val());
      dataDest->setVal(dataSrc->val());
      break;
    }
    default         : CYWARNINGTEXT(QString("virtual void copyFrom(CYData *src): "
                                            "=> type de la donnée %1 incorrecte")
                                    .arg(src->objectName()));
  }
}


void CYData::initOldVal()
{
  switch (type())
  {
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VU8    :
    {
      CYU8 *data = (CYU8 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::Time   :
    {
      CYTime *data = (CYTime *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::Sec    :
    {
      CYTSec *data = (CYTSec *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::String :
    {
      CYString *data = (CYString *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::Color  :
    {
      CYColor *data = (CYColor *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)this;
      data->setOldVal(data->val());
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)this;
      data->setOldVal(data->val());
      break;
    }
    default         : CYWARNINGTEXT(QString("virtual void initOldVal(): "
                                            "=> type de la donnée %1 incorrecte")
                                    .arg(objectName()));
  }
}

void CYData::loadOldVal()
{
  switch (type())
  {
    case Cy::VFL    :
    {
      CYFlag *data = (CYFlag *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VS8    :
    {
      CYS8 *data = (CYS8 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VS16   :
    {
      CYS16 *data = (CYS16 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VS32   :
    {
      CYS32 *data = (CYS32 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VS64   :
    {
      CYS64 *data = (CYS64 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VU8    :
    {
      CYU8 *data = (CYU8 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VU16   :
    {
      CYU16 *data = (CYU16 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VU32   :
    {
      CYU32 *data = (CYU32 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VU64   :
    {
      CYU64 *data = (CYU64 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VF32   :
    {
      CYF32 *data = (CYF32 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::VF64   :
    {
      CYF64 *data = (CYF64 *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::Time   :
    {
      CYTime *data = (CYTime *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::Sec    :
    {
      CYTSec *data = (CYTSec *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::String :
    {
      CYString *data = (CYString *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::Color  :
    {
      CYColor *data = (CYColor *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::Bool   :
    {
      CYBool *data = (CYBool *)this;
      data->setVal(data->oldVal());
      break;
    }
    case Cy::Word   :
    {
      CYWord *data = (CYWord *)this;
      data->setVal(data->oldVal());
      break;
    }
    default         : CYWARNINGTEXT(QString("virtual void initOldVal(): "
                                            "=> type de la donnée %1 incorrecte")
                                    .arg(objectName()));
  }
}

void CYData::initProfibus(void *ptr)
{
  mProfibusInfo = core->profibusItem(ptr);
}

int CYData::profibusRN()
{
  if ( !mProfibusInfo )
    return -1;
  return mProfibusInfo->RN();
}

int CYData::profibusAdr()
{
  if ( !mProfibusInfo )
    return -1;
  return mProfibusInfo->adress();
}

int CYData::profibusByte()
{
  if ( !mProfibusInfo )
    return -1;
  return mProfibusInfo->byte();
}

QString CYData::profibusByteString()
{
  if (profibusByte()<10)
    return QString("0%1").arg(profibusByte());
  else
    return QString("%1").arg(profibusByte());
}

int CYData::profibusBit()
{
  if ( !mProfibusInfo )
    return -1;
  return mProfibusInfo->bit();
}


/*! @return la couleur courante d'affichage.
    \fn CYData::currentColor()
 */
QColor CYData::currentColor()
{
  if ( isOk() )
    return mEnableColor;
  else
    return mDisableColor;
}

bool CYData::hasChoice()
{
  if (choiceDict.count()>0)
    return true;
  return false;
}


void CYData::clearChoice()
{
  choiceDict.clear();
}


/*! Saisie le libellé \a label et l'aide \a help relatifs à la valeur \a value.
    \fn CYData::void setChoice(int value, QString label, QString help)
 */
void CYData::setChoice(int value, QString label, QString help)
{
  CYDataChoice *choice = choiceDict[ value ];

  if ( choice )
  {
    choice->value = value;
    choice->label = label;
    choice->help = help;
  }
  else
  {
    choice = new CYDataChoice;
    choice->value = value;
    choice->label = label;
    choice->help = help;
    choiceDict.insert(value, choice);
  }
}

/*! Saisie l'illustration \a pixmap, le libellé \a label et l'aide \a help relatifs à la valeur \a value.
    \fn CYData::void setChoice(int value, QPixmap pixmap, QString label, QString help)
 */
void CYData::setChoice(int value, QPixmap pixmap, QString label, QString help)
{
  CYDataChoice *choice = choiceDict[ value ];
  if ( choice )
  {
    choice->value = value;
    choice->pixmap = pixmap;
    choice->label = label;
    choice->help = help;
  }
  else
  {
    choice = new CYDataChoice;
    choice->value = value;
    choice->pixmap = pixmap;
    choice->label = label;
    choice->help = help;
    choiceDict.insert(value, choice);
  }
}


/*! Saisie le libellé \a label relatif à la valeur \a value.
    \fn CYData::void setChoiceLabel(int value, QString label)
 */
void CYData::setChoiceLabel(int value, QString label)
{
    if (value > this->maxToDouble())    // erreur dans dbcyc1.cpp : nombre de choix déclarés > max
        CYFATAL;

  CYDataChoice *choice = choiceDict[ value ];

  if ( choice )
  {
    choice->value = value;
    choice->label = label;
  }
  else
  {
    choice = new CYDataChoice;
    choice->value = value;
    choice->label = label;
    choiceDict.insert(value, choice);
  }
}


/*! @return le libellé courant.
    \fn CYData::choiceLabel()
 */
QString CYData::choiceLabel()
{
  CYDataChoice *choice = choiceDict[ (int)mTmp ];
  if ( choice )
    return choice->label;
  else
    return 0;
}


/*! @return le libellé relatif à la valeur \a value.
    \fn CYData::choiceLabel(int value)
 */
QString CYData::choiceLabel(int value)
{
  CYDataChoice *choice = choiceDict[ value ];
  if ( choice )
    return choice->label;
  else
    return 0;
}


/*! Saisie l'aide \a help relative à la valeur \a value.
    \fn CYData::void setChoiceHelp(int value, QString help)
 */
void CYData::setChoiceHelp(int value, QString help)
{
  CYDataChoice *choice = choiceDict[ value ];
  if ( choice )
  {
    choice->value = value;
    choice->help = help;
  }
  else
  {
    choice = new CYDataChoice;
    choice->value = value;
    choice->help = help;
    //QT5delete choiceDict.take(value);
    choiceDict.insert(value, choice);
  }
}


/*! @return l'aide relative à la valeur \a value.
    \fn CYData::choiceHelp(int value)
 */
QString CYData::choiceHelp(int value)
{
  CYDataChoice *choice = choiceDict[ value ];
  if ( choice )
    return choice->help;
  else
    return 0;
}


/*! Saisie l'illustration \a label relative à la valeur \a value.
    \fn CYData::void setChoicePixmap(int value, QString pixmap)
 */
void CYData::setChoicePixmap(int value, QPixmap pixmap)
{
  CYDataChoice *choice = choiceDict[ value ];
  if ( choice )
  {
    choice->value = value;
    choice->pixmap = pixmap;
  }
  else
  {
    choice = new CYDataChoice;
    choice->value = value;
    choice->pixmap = pixmap;
    //QT5delete choiceDict.take(value);
    choiceDict.insert(value, choice);
  }
}


/*! @return l'illustration \a relative à la valeur \a value.
    \fn CYData::choicePixmap(int value)
 */
QPixmap CYData::choicePixmap(int value)
{
  CYDataChoice *choice = choiceDict[ value ];
  if ( choice )
    return choice->pixmap;
  else
    return QPixmap();
}


/*!
    \fn CYData::simulation()
 */
bool CYData::simulation()
{
  return core->simulation();
}


/*! Saisir \a true pour afficher la valeur constructeur dans l'aide de saisie.
    \fn CYData::setHelpDesignerValue(bool val)
 */
void CYData::setHelpDesignerValue(bool val)
{
  mHelpDesignerValueFromDB = false;
  mHelpDesignerValue = val;
}


/*! @return \a true si la valeur constructeur est affichée dans l'aide de saisie.
    \fn CYData::helpDesignerValue()
 */
bool CYData::helpDesignerValue()
{
  if ( mHelpDesignerValueFromDB )
    return db()->helpDesignerValue();
  return mHelpDesignerValue;
}


/*! @return \a true si \a fmt est le format d'affichage d'origine
    \fn CYData::isFromFormat(CYFormat *fmt)
 */
bool CYData::isFromFormat(CYFormat *fmt)
{
  return ( mSenderFormat == fmt ) ? true : false;
}


/*! Change la taille, la période et le nombre d'échantillons du buffer
    \fn CYData::changeBuffer(int size, float time, int sample)
 */
void CYData::changeBuffer(int size, float time, int sample)
{
  format()->setBufferSize( size, !isReadFast() );
  format()->setAcquisitionTime( time, !isReadFast() );
  format()->setSampleSize( sample );
  useBuffer( mUseBuffer );
}

void CYData::setTableBuffer(QString dataName)
{
  setTableBuffer((CYS32 *)core->findData(dataName));
}

void CYData::setTableBuffer(CYS32 *nb)
{
  mTableBufferNb=nb;
  useBuffer( mUseBuffer );
}

int CYData::tableBufferSize()
{
  if (!mTableBufferNb)
    return 0;
  return mTableBufferNb->max();
}

int CYData::tableBufferNb()
{
  if (!mTableBufferNb)
    return 0;
  return mTableBufferNb->val();
}

int CYData::tableBufferCpt()
{
  if (!mTableBufferNb)
    return 0;
  return mTableBufferCpt;
}

void CYData::setDiscontBuffering(CYDiscontBuffering *discontBuffering)
{
  mDiscontBuffering=discontBuffering;
  mDiscontBuffering->addData(this);
  mDiscontBuffer.resize(bufferSize());
}

unsigned int CYData::discontSampleId(int str_fifo, int i)
{
  if (!mDiscontBuffering)
    return 0;
  return mDiscontBuffering->sampleIndex(str_fifo, i);
}

unsigned int CYData::discontBufferNumber(int str_fifo, int i)
{
  if (!mDiscontBuffering)
    return 0;
  return mDiscontBuffering->sampleNumber(str_fifo, i);
}

void CYData::endDiscontBuffer()
{
  // bufferisation précédente capturée => RAZ buffer tampon
  // prépare buffer tampon pour prochaine bufferisation
  mDiscontBuffer.fill(mInitBufferVal, bufferSize());
  // buffer courant à capturer
  discontBufferState = Filled;
}

void CYData::releaseDiscontBuffer()
{
  std::copy(mDiscontBuffer.begin(), mDiscontBuffer.end(), mBuffer.begin());
  mIdBuffer = mDiscontBufferId;
  discontBufferState = Filling;
  mDiscontBufferNum = 0;
}

void CYData::initDiscontBuffer()
{
  mDiscontBufferId = 0;
  mDiscontBuffer.fill(mInitBufferVal, bufferSize());
  mIdBuffer = 0;
  mBuffer.fill(mInitBufferVal, bufferSize());
  discontBufferState = Filling;
}

/*! Saisie la base pour les données numériques entières positives.
    Par défaut celle-ci est en décimale.
    \fn CYData::setNumBase( NumBase val )
 */
void CYData::setNumBase( NumBase val )
{
  mNumBase = val;
}


/*! @return la base pour les données numériques entières positives.
    Par défaut celle-ci est en décimale.
    \fn CYData::numBase()
 */
Cy::NumBase CYData::numBase()
{
  return mNumBase;
}


/*! Renseigne le générateurs d'évènements relatifs à cette donnée
    \fn CYData::setEventsGenerator(CYEventsGenerator *generator)
 */
void CYData::setEventsGenerator(CYEventsGenerator *generator)
{
  QListIterator<CYEvent *> it(mEvents);
  while (it.hasNext())
  {
    CYEvent *event = it.next();
    event->setGenerator(generator);
  }
}


CYEvent *CYData::initEvent( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = core->initEvent(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  mEvents.append(event);
  return event;
}


CYEvent *CYData::initAlert( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = core->initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  mEvents.append(event);
  return event;
}


CYEvent *CYData::initFault( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = core->initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  mEvents.append(event);
  return event;
}


int CYData::valToInt()
{
  switch (type())
  {
    case Cy::VFL    :
    {
      return (int)((CYFlag *)this)->val();
    }
    case Cy::VS8    :
    {
      return (int)((CYS8 *)this)->val();
    }
    case Cy::VS16   :
    {
      return (int)((CYS16 *)this)->val();
    }
    case Cy::VS32   :
    {
      return (int)((CYS32 *)this)->val();
    }
    case Cy::VS64   :
    {
      return (int)((CYS64 *)this)->val();
    }
    case Cy::VU8    :
    {
      return (int)((CYU8  *)this)->val();
    }
    case Cy::VU16   :
    {
      return (int)((CYU16 *)this)->val();
    }
    case Cy::VU32   :
    {
      return (int)((CYU32 *)this)->val();
    }
    case Cy::VU64   :
    {
      return (int)((CYU64 *)this)->val();
    }
    case Cy::VF32   :
    {
      return (int)((CYF32 *)this)->val();
    }
    case Cy::VF64   :
    {
      return (int)((CYF64 *)this)->val();
    }
    case Cy::Time   :
    {
      return (int)((CYTime *)this)->val();
    }
    case Cy::Sec    :
    {
      return (int)((CYTSec *)this)->val();
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName());
      return 0;
  }
  return 0;
}

double CYData::valToDouble()
{
  switch (type())
  {
    case Cy::VFL    :
    {
      return (double)((CYFlag *)this)->val();
    }
    case Cy::VS8    :
    {
      return (double)((CYS8 *)this)->val();
    }
    case Cy::VS16   :
    {
      return (double)((CYS16 *)this)->val();
    }
    case Cy::VS32   :
    {
      return (double)((CYS32 *)this)->val();
    }
    case Cy::VS64   :
    {
      return (double)((CYS64 *)this)->val();
    }
    case Cy::VU8    :
    {
      return (double)((CYU8  *)this)->val();
    }
    case Cy::VU16   :
    {
      return (double)((CYU16 *)this)->val();
    }
    case Cy::VU32   :
    {
      return (double)((CYU32 *)this)->val();
    }
    case Cy::VU64   :
    {
      return (double)((CYU64 *)this)->val();
    }
    case Cy::VF32   :
    {
      return (double)((CYF32 *)this)->val();
    }
    case Cy::VF64   :
    {
      return (double)((CYF64 *)this)->val();
    }
    case Cy::Time   :
    {
      return (double)((CYTime *)this)->val();
    }
    case Cy::Sec    :
    {
      return (double)((CYTSec *)this)->val();
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName());
      return 0.0;
  }
  return 0.0;
}


double CYData::maxToDouble()
{
  switch (type())
  {
    case Cy::VFL    :
    {
      return (double)((CYFlag *)this)->max();
    }
    case Cy::VS8    :
    {
      return (double)((CYS8 *)this)->max();
    }
    case Cy::VS16   :
    {
      return (double)((CYS16 *)this)->max();
    }
    case Cy::VS32   :
    {
      return (double)((CYS32 *)this)->max();
    }
    case Cy::VS64   :
    {
      return (double)((CYS64 *)this)->max();
    }
    case Cy::VU8    :
    {
      return (double)((CYU8  *)this)->max();
    }
    case Cy::VU16   :
    {
      return (double)((CYU16 *)this)->max();
    }
    case Cy::VU32   :
    {
      return (double)((CYU32 *)this)->max();
    }
    case Cy::VU64   :
    {
      return (double)((CYU64 *)this)->max();
    }
    case Cy::VF32   :
    {
      return (double)((CYF32 *)this)->max();
    }
    case Cy::VF64   :
    {
      return (double)((CYF64 *)this)->max();
    }
    case Cy::Time   :
    {
      return (double)((CYTime *)this)->max();
    }
    case Cy::Sec    :
    {
      return (double)((CYTSec *)this)->max();
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName());
      return 0.0;
  }
  return 0.0;
}

double CYData::minToDouble()
{
  switch (type())
  {
    case Cy::VFL    :
    {
      return (double)((CYFlag *)this)->min();
    }
    case Cy::VS8    :
    {
      return (double)((CYS8 *)this)->min();
    }
    case Cy::VS16   :
    {
      return (double)((CYS16 *)this)->min();
    }
    case Cy::VS32   :
    {
      return (double)((CYS32 *)this)->min();
    }
    case Cy::VS64   :
    {
      return (double)((CYS64 *)this)->min();
    }
    case Cy::VU8    :
    {
      return (double)((CYU8  *)this)->min();
    }
    case Cy::VU16   :
    {
      return (double)((CYU16 *)this)->min();
    }
    case Cy::VU32   :
    {
      return (double)((CYU32 *)this)->min();
    }
    case Cy::VU64   :
    {
      return (double)((CYU64 *)this)->min();
    }
    case Cy::VF32   :
    {
      return (double)((CYF32 *)this)->min();
    }
    case Cy::VF64   :
    {
      return (double)((CYF64 *)this)->min();
    }
    case Cy::Time   :
    {
      return (double)((CYTime *)this)->min();
    }
    case Cy::Sec    :
    {
      return (double)((CYTSec *)this)->min();
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName());
      return 0.0;
  }
  return 0.0;
}

double CYData::defToDouble()
{
  switch (type())
  {
    case Cy::VFL    :
    {
      return (double)((CYFlag *)this)->def();
    }
    case Cy::VS8    :
    {
      return (double)((CYS8 *)this)->def();
    }
    case Cy::VS16   :
    {
      return (double)((CYS16 *)this)->def();
    }
    case Cy::VS32   :
    {
      return (double)((CYS32 *)this)->def();
    }
    case Cy::VS64   :
    {
      return (double)((CYS64 *)this)->def();
    }
    case Cy::VU8    :
    {
      return (double)((CYU8  *)this)->def();
    }
    case Cy::VU16   :
    {
      return (double)((CYU16 *)this)->def();
    }
    case Cy::VU32   :
    {
      return (double)((CYU32 *)this)->def();
    }
    case Cy::VU64   :
    {
      return (double)((CYU64 *)this)->def();
    }
    case Cy::VF32   :
    {
      return (double)((CYF32 *)this)->def();
    }
    case Cy::VF64   :
    {
      return (double)((CYF64 *)this)->def();
    }
    case Cy::Time   :
    {
      return (double)((CYTime *)this)->def();
    }
    case Cy::Sec    :
    {
      return (double)((CYTSec *)this)->def();
    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName());
      return 0.0;
  }
  return 0.0;
}

void CYData::initTrigger()
{
  mExtTrigger = 0;
  mTriggerBufferCpt = 0;
  mTriggerIdBuffer = -1;
  mPostTriggerIdBuffer = -1;
  mTriggerStarted = false;
  if (mTriggerEnabled) // désactive le trigger déjà actif
    emit triggerCancel();
  mTriggerBuffer.fill(mInitBufferVal, mBufferSize);
  mTriggerBuffer.resize(mBufferSize);
  //   mTriggerBuffer.fill(mInitBufferVal, mBufferSize);
  //   mTriggerBuffer.duplicate(mBuffer);
  //   mTriggerBuffer.resize(mBufferSize);
  mTriggerBufferFull=false;
  mTriggerEnabled = true;
  mTriggerState=false;
}

void CYData::startTrigger(double level, double post, Cy::Change slope)
{
  initTrigger();
  mTriggerLevel = level;
  mPostTrigger = post;
  mTriggerSlope = slope;
}

void CYData::stopTrigger()
{
  mTriggerEnabled = false;
  mTriggerState=false;
  mTriggerBuffer.resize(0);
}

void CYData::setExtTrigger(CYData *data)
{
  initTrigger();
  mExtTrigger = data;
}

int CYData::triggerIdBuffer()
{
  return mTriggerIdBuffer;
}

int CYData::postTriggerIdBuffer()
{
  return mPostTriggerIdBuffer;
}

bool CYData::isEnabled()
{
  if (mSetting)
    return mSetting->isEnabled();
  return true;
}

double CYData::bufferSimulation(int id)
{
  if (simulType()==SimulType::None)
    return mInitBufferVal;

  double val;
  double delta = mSimulMax-mSimulMin;
  if (tableBufferSize())
  {
    val = id*delta/(20*tableBufferSize());
  }
  else
  {
    double pi = 3.1415;
    int t = id+(mSimulPeriode*(mSimulPhase/360.0));
    val = mSimulMin+( delta/2*(1 + sin((2*pi)/mSimulPeriode*t)) );
    val = val*mSimulkA;
  }
  return val;
}

void CYData::setSimulMax(double val)
{
  mSimulMax = val;
}

void CYData::setSimulMin(double val)
{
  mSimulMin = val;
}

void CYData::setSimulPeriode(double val)
{
  mSimulPeriode = val;
}

void CYData::setSimulPhase(double val)
{
  mSimulPhase = val;
}

void CYData::setSimulkA(double val)
{
  mSimulkA = val;
}
