/***************************************************************************
                          cyao.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYAO_H
#define CYAO_H

// CYLIBS
#include "cyana.h"

/** CYAO est une sortie ANA.
  * @short Sortie ANA.
  * @author Gérald LE CLEACH
  */

class CYAO : public CYAna
{
  Q_OBJECT
public:
  /** Création d'une sortie ANA 16bits.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie ANA.
    * @param attr  Attributs de la sortie ANA.
    * @param c     Numéro de la carte ANA. */
  CYAO(CYDB *db, const QString &name, Attr16 *attr, int c=-1);

  /** Création d'une sortie ANA 32bits.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie ANA.
    * @param attr  Attributs de la sortie ANA.
    * @param c     Numéro de la carte ANA. */
  CYAO(CYDB *db, const QString &name, Attr32 *attr, int c=-1);

  /** Création d'une sortie ANA 16bits.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie ANA.
    * @param attr  Attributs de la sortie ANA.
    * @param c     Numéro de la carte ANA. */
  CYAO(CYDB *db, const QString &name, Attr16ADC *attr, int c=-1);

  /** Création d'une sortie ANA 32bits.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie ANA.
    * @param attr  Attributs de la sortie ANA.
    * @param c     Numéro de la carte ANA. */
  CYAO(CYDB *db, const QString &name, Attr32ADC *attr, int c=-1);

  /** Création d'une sortie ANA jusqu'à 15bits (16 - bit de parité).
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param adc    Valeur de la donnée ADC.
    * @param flg     Flag de validité.
    * @param elec   Repère électrique.
    * @param phys   Repère physique.
    * @param mode   Mode d'affichage.
    * @param format Numéro du format d'affichage dans l'application.
    * @param ldef   Point bas par défaut.
    * @param tdef   Point haut par défaut.
    * @param label  Etiquette.
    * @param card   Numéro de la carte. */
  CYAO(CYDB *db, const QString &name, f32 *val, s16 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int card=-1);

  /** Création d'une sortie ANA jusqu'à 31bits (32 - bit de parité).
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param adc    Valeur de la donnée ADC.
    * @param flg     Flag de validité.
    * @param elec   Repère électrique.
    * @param phys   Repère physique.
    * @param mode   Mode d'affichage.
    * @param format Numéro du format d'affichage dans l'application.
    * @param ldef   Point bas par défaut.
    * @param tdef   Point haut par défaut.
    * @param label  Etiquette.
    * @param card   Numéro de la carte. */
  CYAO(CYDB *db, const QString &name, f32 *val, s32 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int card=-1);

  ~CYAO();
};

#endif
