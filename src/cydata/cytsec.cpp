/***************************************************************************
                          cytsec.cpp  -  description
                             -------------------
    début                  : lun mai 12 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cytsec.h"

// CYLIBS
#include "cytime.h"

CYTSec::CYTSec(CYDB *db, const QString &name, u32 *val, Cy::Mode mode, double base)
  : CYF32(db, name, new f32, 0.0, 0, mode),
    mBase(base)
{
  if ( (mBase<1.0) || (mBase>1000000.0) )
    CYFATALDATA(name)

  mPtr  = val;
  *mPtr = (u32)(mDef*mBase+0.5);
  mType = Sec;
  mFormat = new CYFormat(this, QString("%1_FMT").arg(name), tr("sec"), tr("Time"), mMin, mMax, 1, 0.0);
}

CYTSec::CYTSec(CYDB *db, const QString &name, u32 *val, const f32 def, const f32 min, const f32 max, Cy::Mode mode, double base)
  : CYF32(db, name, new f32, def, min, max, 0, mode),
    mBase(base)
{
  if ( (mBase<1.0) || (mBase>1000000.0) )
    CYFATALDATA(name)

  mPtr  = val;
  *mPtr = (u32)(mDef*(1000.0/mBase)+0.5);
  mType = Sec;
  mDef  = def;
  mMin  = min;
  mMax  = max;
  mFormat = new CYFormat(this, QString("%1_FMT").arg(name), tr("sec"), tr("Time"), mMin, mMax, 1, 0.0);
}

CYTSec::~CYTSec()
{
}

void CYTSec::setVal(const f32 value)
{
  *mVal = value;
  mTmp = value;
  *mPtr = (u32)(value*(1000.0/mBase)+0.5);
}

void CYTSec::import(f32 val)
{
  if (val>max())
    setVal(max());
  else if (val<min())
    setVal(min());
  else
    setVal(val);
}

f32 CYTSec::val()
{
  *mVal = (f32)( ((*mPtr)*mBase/1000.0) );
  return CYF32::val();
}

f32 CYTSec::real()
{
  *mVal = (f32)( (*mPtr)*mBase/(1000.0) );
  return CYF32::real();
}

u32 CYTSec::ms()
{
  return ms(mTmp);
}

u32 CYTSec::ms(const double val)
{
  return (u32)((val)*mBase*1000.0);
}

void CYTSec::designer()
{
  setVal(mDef);
}
