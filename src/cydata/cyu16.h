/***************************************************************************
                          cyu16.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYU16_H
#define CYU16_H

// CYLIBS
#include "cynum.h"

/** CYU16 est une donnée entière non-signée de 16 bits.
  * @short Donnée entière de 16 bits.
  * @author Gérald LE CLEACH
  */

class CYU16 : public CYNum<u16>
{
public:
  /** Création d'une donnée entière non-signée de 16 bits.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU16(CYDB *db, const QString &name, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière non-signée de 16 bits avec sa valeur.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU16(CYDB *db, const QString &name, u16 *val, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 16 bits avec sa valeur ainsi que sa valeur par défaut.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU16(CYDB *db, const QString &name, u16 *val, const u16 def, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 16 bits avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param mode   Mode d'utilisation. */
  CYU16(CYDB *db, const QString &name, u16 *val, const u16 def, const u16 min, const u16 max, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière non-signée de 16 bits avec les attributs Attr1.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU16(CYDB *db, const QString &name, CYNum<u16>::Attr1<u16> *attr);

  /** Création d'une donnée entière non-signée de 16 bits avec les attributs Attr2.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU16(CYDB *db, const QString &name, CYNum<u16>::Attr2<u16> *attr);
  /** Création d'une donnée entière non-signée de 16 bits avec les attributs Attr3.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU16(CYDB *db, const QString &name, CYNum<u16>::Attr3<u16> *attr);

  /** Création d'une donnée entière non-signée de 16 bits avec les attributs Attr4.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU16(CYDB *db, const QString &name, CYNum<u16>::Attr4<u16> *attr);

  ~CYU16();

  /** @return le nombre de chiffres total. */
  virtual int nbDigit(Cy::NumBase base=Cy::BaseData);

   /** @return la valeur courrante en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(bool withUnit=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

  virtual QString toString(double val, bool withUnit, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);
};

#endif
