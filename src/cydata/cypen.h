//
// C++ Interface: cypen
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYPEN_H
#define CYPEN_H

// QT
#include <QPicture>
#include <QPen>
// CYLIBS
#include "cy.h"
#include "cydatasgroup.h"

class CYU8;
class CYU32;
class CYColor;

/**
@short Ensemble de données définissant un crayon.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYPen : public CYDatasGroup
{
  Q_OBJECT
public:
  /** Création d'un crayon.
    * @param db     Base de données parent.
    * @param name   Nom du crayon.
    * @param label  Libellé du crayon.
    * @param pen    Crayon.
    * @param mode   Mode d'utilisation. */
    CYPen(CYDB *db, const QString &name, QString label, QPen pen=QPen(), Cy::Mode mode=Cy::Sys);

    ~CYPen();

    virtual QPicture picture(QSize size);

    virtual void setColor( QColor color );

    virtual void setPen( QPen pen );
    QPen pen();

protected:
    CYU8    * mStyle;
    CYU32   * mWidth;
    CYColor * mColor;
};

#endif
