/***************************************************************************
                          cydb.cpp  -  description
                             -------------------
    début                  : sam sep 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cydb.h"

// QT
#include <QDomElement>
#include <QClipboard>
#include <QTextStream>
// SYS
#include <sys/stat.h>
// CYLIBS
#include "cycore.h"
#include "cyuser.h"
#include "cyrecord.h"
#include "cynetmodule.h"
#include "cynethost.h"
#include "cynetlink.h"
#include "cydata.h"
#include "cymaths.h"
#include "cyflag.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cytime.h"
#include "cytsec.h"
#include "cybool.h"
#include "cyword.h"
#include "cystring.h"
#include "cycolor.h"
#include "cydatetime.h"
#include "cyacquisitionfast.h"
#include "cydiscontbuffering.h"
#include "cymessagebox.h"

CYDB::CYDB(QObject *parent, const QString &name, int no, int msec, bool global, const QString &prefix, const QString &suffix)
  : QObject(parent),
    mNo(no),
    mRefreshTime(msec),
    mGlobal(global),
    mPrefixName(prefix),
    mSuffixName(suffix)
{
  setObjectName(prefix+name+suffix);

  if (core)
  {
    mFileName = QString(core->appSettingDir()+"%1.cydata").arg(objectName());
    hosts.append(core->localHost());
  }
  init();
}

CYDB::CYDB(CYNetHost *host, const QString &name, int no, int msec, bool global, const QString &prefix, const QString &suffix)
  : QObject((QObject*)host),
    mNo(no),
    mRefreshTime(msec),
    mGlobal(global),
    mPrefixName(prefix),
    mSuffixName(suffix)
{
  setObjectName(prefix+name+suffix);
  if (core)
    mFileName = QString(core->appSettingDir()+"%1.cydata").arg(objectName());
  hosts.append(host);
  init();
}

CYDB::CYDB(CYNetHost *host1, CYNetHost *host2, const QString &name, int no, int msec, bool global, const QString &prefix, const QString &suffix)
  : QObject((QObject*)host1),
    mNo(no),
    mRefreshTime(msec),
    mGlobal(global),
    mPrefixName(prefix),
    mSuffixName(suffix)
{
  setObjectName(prefix+name+suffix);
  if (core)
    mFileName = QString(core->appSettingDir()+"%1.cydata").arg(objectName());
  hosts.append(host1);
  hosts.append(host2);
  init();
}

CYDB::~CYDB()
{
}

void CYDB::init()
{
  mBufStruct  = 0;
  mRecord     = 0;
  mAutoLoad   = false;
  mOk         = true;
  mEnableRead = true;
  mPauseRead  = false;
  mReading    = false;
  mModified   = false;
  mAlwaysOk   = false;
  mIsReadFast = false;
  mSaveWithApply = true;
  mWriteWithApply = true;
  mDOMTagDatas = "Data";
  mHelpDesignerValue = true;
  mIsTCP      = false;
  mSavingWithPrefix = false;
  mSavingWithSuffix = false;
  mVersionCYDOM = QString(core->version()).section( '.', 0, 1 );

  if (core)
  {
    if (mGlobal)
    {
      if (mNo>-1)
      {
        mSizeReadFIFO  = core->module->sizeReadFIFO(mNo);
        mSizeWriteFIFO = core->module->sizeWriteFIFO(mNo);
      }

      connect(core, SIGNAL(resizeAllDB()), SLOT(resizeDB()));
      connect(this, SIGNAL(waitOperator(int)), core, SLOT(waitOperator(int)));

      core->dbList.insert(objectName(), this);
    }

/*    connect(core, SIGNAL(moduleStarting()), SLOT(startRefreshTimer()));*/
    connect(core, SIGNAL(simulationOn()), SLOT(startRefreshTimer()));
  }
}

void CYDB::startRefreshTimer()
{
  if (mRefreshTime>0)
  {
    CYMESSAGETEXT(tr("Starting refresh timer datas base %1 at %2 ms").arg(objectName()).arg(mRefreshTime));
    mTime = new QTimer(this);
    mTime->start(mRefreshTime);
    connect(mTime, SIGNAL(timeout()), this, SLOT(refresh()));
  }
}

bool CYDB::isOk()
{
  if (core && core->user() && core->simulation())
    return true;
  return (mAlwaysOk | mOk);
}

void *CYDB::ptrStruct()
{
  if ((core != 0) && (mNo>-1))
    return core->module->ptrStruct(mNo);
  else
    return 0;
}

int CYDB::sizeStruct()
{
  if ((core != 0) && (mNo>-1))
    return core->module->sizeStruct(mNo);
  else
    return 0;
}

/*! @return \a TRUE si en lecture
    \fn CYDB::isReadMode()
 */
bool CYDB::isReadMode()
{
  if (core->simulation() || (mNo<0)) // pas de mode en local
    return true;
  return core->module->isReadMode(mNo);
}


/*! @return \a TRUE si en écriture
    \fn CYDB::isWriteMode()
 */
bool CYDB::isWriteMode()
{
  if (core->simulation() || (mNo<0)) // pas de mode en local
    return true;
  return core->module->isWriteMode(mNo);
}

/*! @return \a true si modbus sinon R422M ou TCP
    \fn CYDB::isModbus()
 */
bool CYDB::isModbus()
{
  if (isTCP())
    return false;
  else
    return core->module->isModbus();
}


/*! @return \a true si TCP sinon R422M ou Modbus
    \fn CYDB::isTCP()
 */
bool CYDB::isTCP()
{
  return mIsTCP;
}


void *CYDB::buffer()
{
  if (isModbus())
    return core->module->buffer(mNo);
  else if (isTCP())
    return core->module->buffer(mNo);
  else // buffer dans r422m
    return 0;
}

//QString CYDB::hostName(int index)
//{
//  CYNetHost *host = hosts.at(index);
//  if (host)
//    return host->objectName();
//  else
//    return 0;
//}

QString CYDB::hostsName()
{
  QString txt;
  QListIterator<CYNetHost *> it(hosts);
  while (it.hasNext())
  {
    CYNetHost *host = it.next();
    if (!txt.isEmpty())
      txt.append(", ");
    txt.append(host->objectName());
  }
  return txt;
}

CYNetHost *CYDB::host(QString hostName)
{
  QListIterator<CYNetHost *> it(hosts);
  while (it.hasNext())
  {
    CYNetHost *host = it.next();
    if (host->objectName() == hostName)
      return host;
  }
  return 0;
}


//QString CYDB::linkName()
//{
//  if (mLink)
//    return mLink->objectName();
//  return 0;
//}

QString CYDB::linksName()
{
  QString txt;
  QListIterator<CYNetLink *> it(mLinks);
  while (it.hasNext())
  {
    CYNetLink *link = it.next();
    if (!txt.isEmpty())
      txt.append(", ");
    txt.append(link->objectName());
  }
  return txt;
}

CYNetLink *CYDB::link(QString linkName)
{
  QListIterator<CYNetLink *> it(mLinks);
  while (it.hasNext())
  {
    CYNetLink *link = it.next();
    if (link->objectName() == linkName)
      return link;
  }
  return 0;
}

QHash<QString, CYData*> CYDB::allDatas()
{
  if (core != 0)
    return core->db;
  else
    CYFATALTEXT(QString("%1").arg(objectName()));
}

void CYDB::addToDB(CYData *data)
{
  datas.insert(data->objectName(), data);
  if (core && mGlobal)
  {
    //delete core->db.take(data->objectName());
    core->db.insert(data->objectName(), data);
  }
  if (data->mode()==Cy::User)
  {
    data->useBuffer(true);
    bufDatas.insert(data->objectName(), data);
  }
}

bool CYDB::linksOn()
{
  QListIterator<CYNetLink *> it(mLinks);
  while (it.hasNext())
  {
    CYNetLink *link = it.next();
    if (!link->isOn())
      return false;
  }
  return true;
}

bool CYDB::readData(unsigned int index)
{
  if (mPauseRead)
    return false;

  if (!isReadMode())
    return false;

  mBufStruct = 0;

  mReading = true;

  CYNetLink *link = mLinks.at(0);
  if (!link)
  {
    mOk = false;
    return false;
  }

  if (mEnableRead && (core != 0))
  {
    mEnableRead = false;
    int nb;
    if (mNo>-1) // données externes indexées sur réseau
    {
      nb = core->readData(mNo, index, link->getCOM());
    }
    else // données locales traîtées en interne
      nb = readLocal();

    mBufStruct = nb;
    if (!link->isOn() && (!core->simulation() && (nb>=0)))
      mOk = false;
    else
    {   
      mOk = true;
      if (core->simulation())
      {
        QHashIterator<QString, CYData *> i(bufDatas);
        i.toFront();
        while (i.hasNext())
        {
          i.next();
          CYData *data=i.value();
          nb = refreshTime()/(data->acquisitionTime()*data->sampleSize());
          if (nb==0)
            nb=1;
          break;
        }
      }

      if (!core->simulation())
      {
        for (int i=0; i<nb; i++)
          readFlags(i);
      }

      QHashIterator<QString, CYData *> i(bufDatas);
      i.toFront();
      while (i.hasNext())
      {
        i.next();                   // must come first
        switch (i.value()->type())
        {
          case Cy::VFL    :
                          {
                            CYFlag *data = (CYFlag *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VS8    :
                          {
                            CYS8 *data = (CYS8 *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VS16   :
                          {
                            CYS16 *data = (CYS16 *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VS32   :
                          {
                            CYS32 *data = (CYS32 *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VS64   :
                          {
                            CYS64 *data = (CYS64 *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VU8    :
                          {
                            CYU8  *data = (CYU8  *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VU16   :
                          {
                            CYU16 *data = (CYU16 *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VU32   :
                          {
                            CYU32 *data = (CYU32 *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VU64   :
                          {
                            CYU64 *data = (CYU64 *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VF32   :
                          {
                            CYF32 *data = (CYF32 *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::VF64   :
                          {
                            CYF64 *data = (CYF64 *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::Time   :
                          {
                            CYTime *data = (CYTime *)i.value();
                            data->buffering(nb);
                            break;
                          }
          case Cy::Sec    :
                          {
                            CYTSec *data = (CYTSec *)i.value();
                            data->buffering(nb);
                            break;
                          }
          default         : CYWARNINGTEXT(index+", "+objectName()+", "+i.value()->objectName());
        }
      }
      if (core->simulation() && nb < 0)
        nb = 1;
      for (int i=0; i<nb; i++)
      {
        // Traîtement des acquisitions
        QListIterator<CYAcquisitionFast *> it(mAcquisitions);
        while (it.hasNext())
        {
          CYAcquisitionFast *acquisition = it.next();
          acquisition->buffering(i);
        }

        // Traîtement des bufferisations discontinues
        QListIterator<CYDiscontBuffering *> it2(mDiscontufferings);
        while (it2.hasNext())
        {
          CYDiscontBuffering *discountBuffering = it2.next();
          discountBuffering->ctrlFilled();
        }
      }

      emit datasRead();
    }
    mEnableRead = true;
  }
  mReading = false;
  return mOk;
}

bool CYDB::writeData(unsigned int index)
{
  if (!isWriteMode())
     return false;

  bool res;
   if (core != 0)
   {
     if (mNo>-1) // données externes indexées sur réseau
       res = core->writeData(mNo, index);
     else // données locales traîtées en interne
       res = writeLocal();
    emit datasWrited();
    return res;
  }
  return false;
}

void CYDB::setRecord(const QString &filename)
{
  if (mNo>-1)
    mRecord = new CYRecord(ptrStruct(), sizeStruct(), filename, this, "mRecord", sizeReadFIFO()+sizeWriteFIFO());
}

void CYDB::designer()
{
  CYData *d;
  QHashIterator<QString, CYData *> i(datas);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    d = i.value();
    switch(d->type())
    {
      case Cy::VFL    :
                      {
                        CYFlag *data = (CYFlag *)d;
                        data->designer();
                        break;
                      }
      case Cy::VS8    :
                      {
                        CYS8 *data = (CYS8 *)d;
                        data->designer();
                        break;
                      }
      case Cy::VS16   :
                      {
                        CYS16 *data = (CYS16 *)d;
                        data->designer();
                        break;
                      }
      case Cy::VS32   :
                      {
                        CYS32 *data = (CYS32 *)d;
                        data->designer();
                        break;
                      }
      case Cy::VS64   :
                      {
                        CYS64 *data = (CYS64 *)d;
                        data->designer();
                        break;
                      }
      case Cy::VU8    :
                      {
                        CYU8  *data = (CYU8  *)d;
                        data->designer();
                        break;
                      }
      case Cy::VU16   :
                      {
                        CYU16 *data = (CYU16 *)d;
                        data->designer();
                        break;
                      }
      case Cy::VU32   :
                      {
                        CYU32 *data = (CYU32 *)d;
                        data->designer();
                        break;
                      }
      case Cy::VU64   :
                      {
                        CYU64 *data = (CYU64 *)d;
                        data->designer();
                        break;
                      }
      case Cy::VF32   :
                      {
                        CYF32 *data = (CYF32 *)d;
                        data->designer();
                        break;
                      }
      case Cy::VF64   :
                      {
                        CYF64 *data = (CYF64 *)d;
                        data->designer();
                        break;
                      }
      case Cy::Time   :
                      {
                        CYTime *data = (CYTime *)d;
                        data->designer();
                        break;
                      }
      case Cy::Sec    :
                      {
                        CYTSec *data = (CYTSec *)d;
                        data->designer();
                        break;
                      }
      case Cy::Word   :
                      {
                        CYWord *data = (CYWord *)d;
                        data->designer();
                        break;
                      }
      case Cy::Bool   :
                      {
                        CYBool *data = (CYBool *)d;
                        data->designer();
                        break;
                      }
      case Cy::String :
                      {
                        CYString *data = (CYString *)d;
                        data->designer();
                        break;
                      }
      case Cy::Color :
                      {
                        CYColor *data = (CYColor *)d;
                        data->designer();
                        break;
                      }
      default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+d->objectName());
                        return;
    }
  }
}

int CYDB::load()
{
  designer();

  if (mFileName.isEmpty())
    return 0;
  int res = load(mFileName);
  mModified = false;

  return res;
}

int CYDB::load(QString &fileName)
{
  QFile file(fileName);

  if (!file.open(QIODevice::ReadOnly))
  {
    CYMESSAGETEXT(fileName);
    return 1;
  }

  QString errorMsg;
  int errorLine;
  int errorColumn;

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  if (!doc.setContent(&file, &errorMsg, &errorLine, &errorColumn))
  {
    CYWARNINGTEXT(QString("The file %1 does not contain valid XML ! %2: %3 %4").arg(fileName).arg(errorMsg).arg(errorLine).arg(errorColumn))
    file.remove();
    save(fileName);
    return 1;
  }
  // Vérifie le type propre du document.
  if (doc.doctype().name() != "CYLIXData")
  {
    CYWARNINGTEXT(tr("The file %1 does not contain a valid"
                       "definition, which must have a document type ").arg(fileName)
                      +"'CYLIXData'");
    file.remove();
    save(fileName);
    return 1;
  }

  // Charge les informations sur la base de données.
  QDomElement db = doc.documentElement();

  // Version de la librairie lors de la sauvegarde du document.
  mVersionCYDOM = db.attribute( "CYDOM" );

  if (!loadFromDOM(db))
    return -3;

  if ( core->notifyDatasLoading() )
    emit loaded();
  return 2;
}

bool CYDB::loadFromDOM(QDomElement &dom, bool def)
{
  if (def)
    designer();
  // Charge les données.
  QDomElement db;
  QDomNodeList list = dom.childNodes();
  bool found = false;
  for (int i=0; i<list.count(); ++i)
  {
    QDomElement item = list.item(i).toElement();
    if (item.tagName()=="DB")
    {
      db = item;
      found = true;
      break;
    }
  }
  if (!found)
    return true;

  list = db.elementsByTagName(DOMTagDatas());
  for (int i=0; i<list.count(); ++i)
  {
    bool ok;
    CYData *d;
    QDomElement item = list.item(i).toElement();
    QString dataName = item.attribute("name");
    if (!mSavingWithPrefix)
      dataName.prepend(mPrefixName);
    if (!mSavingWithSuffix)
      dataName.append(mSuffixName);
    if (!(d = datas.value(dataName)) && !(d = findWithOldName(dataName)))
    {
//      CYDEBUG TODO CYMESSAGETEXT(QString("Chargement du fichier %1 => %2 nom de donnée inconnue dans la base %3").arg(mFileName).arg(dataName).arg(objectName()));
    }
    else if (!d->isVolatile())
    {
      switch(d->type())
      {
        case Cy::VFL    :
                        {
                          CYFlag *data = (CYFlag *)d;
                          data->import((flg)item.attribute("value").toShort(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::VS8    :
                        {
                          CYS8 *data = (CYS8 *)d;
                          data->import((s8)item.attribute("value").toShort(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::VS16   :
                        {
                          CYS16 *data = (CYS16 *)d;
                          data->import((s16)item.attribute("value").toShort(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::VS32   :
                        {
                          CYS32 *data = (CYS32 *)d;
                          data->import((s32)item.attribute("value").toLong(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::VS64   :
                        {
                          CYS64 *data = (CYS64 *)d;
                          s64 val = (s64)item.attribute("value").toLong(&ok);
                          if (!ok)
                          {
                            // importe une valeur d'une donnée CYDateTime anciennement de type Cy::String
                            CYDateTime *dt = (CYDateTime *)d;
                            val = dt->toSinceEpoch(item.attribute("value"));
                          }
                          data->import(val);
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          break;
                        }
        case Cy::VU8    :
                        {
                          CYU8  *data = (CYU8  *)d;
                          data->import((u8)item.attribute("value").toUShort(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::VU16   :
                        {
                          CYU16 *data = (CYU16 *)d;
                          data->import((u16)item.attribute("value").toUShort(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::VU32   :
                        {
                          CYU32 *data = (CYU32 *)d;
                          u32 val = (u32)item.attribute("value").toULong(&ok);
                          if (!ok)
                          {
                            // importe une valeur d'une donnée CYU32 anciennement CYDateTime
                            QDateTime dt = CYDateTime::fromString(item.attribute("value"));
                            ok = dt.isValid();
                            val = dt.toSecsSinceEpoch();
                          }
                          data->import(val);
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          break;
                        }
        case Cy::VU64   :
                        {
                          CYU64 *data = (CYU64 *)d;
                          data->import((u64)item.attribute("value").toULong(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::VF32   :
                        {
                          CYF32 *data = (CYF32 *)d;
                          data->import((f32)item.attribute("value").toFloat(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::VF64   :
                        {
                          CYF64 *data = (CYF64 *)d;
                          data->import((f64)item.attribute("value").toDouble(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::Time   :
                        {
                          CYTime *data = (CYTime *)d;
                          QString txt = item.attribute("value");
                          u32 val = txt.toULong(&ok);
                          if (!ok)
                          {
                            f32 fval = txt.toFloat(&ok);
                            if (ok)
                              val = 1000*fval; // import d'une ancienne donnée CYTSec
                          }
                          data->import(val);
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::Sec    :
                        {
                          CYTSec *data = (CYTSec *)d;
                          data->import((f32)item.attribute("value").toFloat(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::Word   :
                        {
                          CYWord *data = (CYWord *)d;
                          data->setVal((u16)item.attribute("value").toUShort(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::Bool   :
                        {
                          CYBool *data = (CYBool *)d;
                          data->setVal((bool)item.attribute("value").toShort(&ok));
                          data->setOldVal(data->val());
                          data->setTmp(data->val());
                          if (!ok)
                            CYWARNINGTEXT(objectName()+" -> "+d->objectName());
                          break;
                        }
        case Cy::String :
                        {
                          CYString *data = (CYString *)d;
                          QString txt = data->simpleDecrypt(item.attribute("value"));
                          data->setVal(txt);
                          data->setOldVal(data->simpleDecrypt());
                          break;
                        }
        case Cy::Color :
                        {
                          CYColor *data = (CYColor *)d;
                          data->setVal(item.attribute("value"));
                          data->setOldVal(data->val());
                          break;
                        }
        default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+d->objectName());
                          return false;
      }
    }
  }
  return true;
}

int CYDB::save()
{
  if (mFileName.isEmpty())
    return 0;
  return save(mFileName);
}

int CYDB::save(QString &fileName)
{
  QDomDocument doc("CYLIXData");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  // Enregistre les informations sur la base de données.
  QDomElement db = doc.createElement("DataBase");
  doc.appendChild(db);

  db.setAttribute( "CYDOM", core->version() );
  mVersionCYDOM = core->version();

  if (!saveToDOM(doc, db))
    return -1;

  QFile file(fileName);
  if (!file.open(QIODevice::WriteOnly))
  {
    CYWARNINGTEXT(tr("Can't save file %1!").arg(fileName));
    return -2;
  }
  QTextStream s(&file);
  s.setCodec("UTF-8");
  s << doc;
  file.close();
  core->backupFile(fileName);
  emit saved();
  return 1;
}

bool CYDB::saveToDOM(QDomDocument &doc, QDomElement &db)
{
 // Enregistre les données.
  QDomElement list = doc.createElement("DB");
  db.appendChild(list);
  CYData *d;
  QHashIterator<QString, CYData *> i(datas);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    d = i.value();
    QString savingName = d->objectName();

    if (!mSavingWithPrefix)
      savingName.remove(mPrefixName);

    if (!mSavingWithSuffix)
      savingName.remove(mSuffixName);

    if (!d->isVolatile())
    {
      switch(d->type())
      {
        case Cy::VFL    :
                        {
                          CYFlag *data = (CYFlag *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::VS8    :
                        {
                          CYS8 *data = (CYS8 *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::VS16   :
                        {
                          CYS16 *data = (CYS16 *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::VS32   :
                        {
                          CYS32 *data = (CYS32 *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::VS64   :
                        {
                          CYS64 *data = (CYS64 *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", static_cast<qint64>(data->val()));
                          }
                          break;
                        }
        case Cy::VU8    :
                        {
                          CYU8  *data = (CYU8  *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::VU16   :
                        {
                          CYU16 *data = (CYU16 *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::VU32   :
                        {
                          CYU32 *data = (CYU32 *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::VU64   :
                        {
                          CYU64 *data = (CYU64 *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", static_cast<qint64>(data->val()));
                          }
                          break;
                        }
        case Cy::VF32   :
                        {
                          CYF32 *data = (CYF32 *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::VF64   :
                        {
                          CYF64 *data = (CYF64 *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", QString("%1").arg(data->val()));
                          }
                          break;
                        }
        case Cy::Time   :
                        {
                          CYTime *data = (CYTime *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            u32 val = data->val();
                            item.setAttribute("value", val);
                          }
                          break;
                        }
        case Cy::Sec    :
                        {
                          CYTSec *data = (CYTSec *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::Word   :
                        {
                          CYWord *data = (CYWord *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::Bool   :
                        {
                          CYBool *data = (CYBool *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::String :
                        {
                          CYString *data = (CYString *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->val());
                          }
                          break;
                        }
        case Cy::Color :
                        {
                          CYColor *data = (CYColor *)d;
                          if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          {
                            QDomElement item = doc.createElement(DOMTagDatas());
                            list.appendChild(item);
                            item.setAttribute("name", savingName);
                            item.setAttribute("value", data->toString());
                          }
                          break;
                        }
        default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+d->objectName());
      }
    }
  }

  return true;
}

bool CYDB::autoLoad()
{
  return mAutoLoad;
}

void CYDB::resizeDB()
{
  if (datas.isEmpty())
  {
    CYMESSAGETEXT(QString("void CYDB::resizeDB(): %1 vide").arg(objectName()));
    return;
  }

  int res = cyMinPrime(datas.count()+1);
  if (res!=-1)
    datas.reserve(res);
  else
    CYFATALOBJECT(objectName());
}

void CYDB::setFileName(const QString &filename)
{
  mFileName = filename;
}

void CYDB::refresh()
{
  readData();
}

int CYDB::refreshTime()
{
  return mRefreshTime;
}

void CYDB::initForcingValues()
{
  CYData *d;
  QHashIterator<QString, CYData *> i(datas);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    d = i.value();
    d->initForcingValue();
  }
}

void CYDB::readFlags(int str)
{
  Q_UNUSED(str)
}

void CYDB::addAcquisition(CYAcquisitionFast *acq)
{
  mAcquisitions.append(acq);
}

void CYDB::addDiscontBuffering(CYDiscontBuffering *buf)
{
  mDiscontufferings.append(buf);
}

void CYDB::apply()
{
  if (!mModified)
    return;

  if (mWriteWithApply)
    writeData();

  if (mSaveWithApply)
    save();

  mModified = false;
}

void CYDB::setModified()
{
  mModified = true;
}

void CYDB::addHost(CYNetHost *host)
{
  hosts.append(host);
}

bool CYDB::isHostDB(CYNetHost *host)
{
  QListIterator<CYNetHost *> it(hosts);
  while (it.hasNext())
  {
    CYNetHost *h = it.next();
    if (h==host)
      return true;
  }
  return false;
}


/*! Copie la base de données dans le presse-papier
    \fn CYDB::copy()
 */
void CYDB::copy()
{
  mCopying = true;
  QClipboard* clip = QApplication::clipboard();
  clip->setText(toXML());
  mCopying = false;
}


/*! Colle le contenu du presse-papier
    \fn CYDB::paste()
 */
void CYDB::paste()
{
  mPasting = true;
  QClipboard* clip = QApplication::clipboard();
  fromXML(clip->text());
  mPasting = false;
}


/*! Convertit les valeurs de la base de données en langage XML.
    \fn CYDB::toXML()
 */
QString CYDB::toXML()
{
  /* Nous construisons une description XML de la cellule. */
  QDomDocument doc("CYLIXData");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  // Enregistre les informations sur la base de données.
  QDomElement db = doc.createElement("DataBase");
  doc.appendChild(db);
  if (!saveToDOM(doc, db))
    return 0;

  return doc.toString();
}


/*! Charge les valeurs de la base de données à partir d'un texte en langage XML.
    \fn CYDB::fromXML(QString text)
 */
bool CYDB::fromXML(QString text)
{
  QDomDocument doc;
  /* Récupère le texte dans le presse-papiers et vérifie la valididé de
   * l'en-tête XML et du type propre du document. */
  if (!doc.setContent(text) || doc.doctype().name() != "CYLIXData")
  {
    CYMessageBox::sorry(0, tr("The clipboard does not contain a valid datas base description."));
    return false;
  }

  QDomElement el = doc.documentElement();
  if (!loadFromDOM(el))
    return false;

  return true;
}


/*! @return l'étiquette relative au données dans un DOM
    \fn CYDB::DOMTagDatas()
 */
QString CYDB::DOMTagDatas()
{
  return mDOMTagDatas;
}

/*! Saisie l'étiquette relative au données dans un DOM
    \fn CYDB::setDOMTagDatas(QString tag)
 */
void CYDB::setDOMTagDatas(QString tag)
{
  mDOMTagDatas = tag;
}


/*! Saisir \a true pour afficher la valeur constructeur dans l'aide de saisie.
    \fn CYDB::setHelpDesignerValue(bool val)
 */
void CYDB::setHelpDesignerValue(bool val)
{
  mHelpDesignerValue = val;
}


/*! @return \a true si la valeur constructeur est affichée dans l'aide de saisie.
    \fn CYDB::helpDesignerValue()
 */
bool CYDB::helpDesignerValue()
{
  return mHelpDesignerValue;
}


/*!	Change le nom d'une donnée. L'ancien nom est sauvegardé pour conserver une compatibilité acsendante. Il est ainsi possible, lors de la lecture d'un ancien fichier de base de données de trouver le nouveau nom de la données ayant pour ancien nom celui enregistré dans ce fichier.
    \fn CYDB::changeDataName(QString oldName, QString newName)
 */
void CYDB::changeDataName(QString oldName, QString newName)
{
  if (oldName==newName)
    CYWARNINGTEXT(TRFR("L'ancien nom '%1' est identique au nouveau '%2'").arg(oldName).arg(newName))
  mOldDataName.insert( oldName, new QString( newName ) );
}


/*!	@return la donnée ayant pour ancien nom \a oldName.
    \fn CYDB::findWithOldName( QString oldName )
 */
CYData * CYDB::findWithOldName( QString oldName )
{
  QString *newName = mOldDataName.value( oldName );
  if ( !newName )
    return 0;
  return datas.value( *newName );
}


/*! @return \a true si tous les données ont pour valeurs la valeur constructeur.
    \n Ceci permet par exemple d'éviter un enregistrement inutile de la base de données.
    \fn CYDB::hasOnlyDesignerValues()
 */
bool CYDB::hasOnlyDesignerValues()
{
  CYData *d;
  QHashIterator<QString, CYData *> i(datas);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    d = i.value();
    switch(d->type())
    {
      case Cy::VFL    :
                      {
                        CYFlag *data = (CYFlag *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VS8    :
                      {
                        CYS8 *data = (CYS8 *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VS16   :
                      {
                        CYS16 *data = (CYS16 *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VS32   :
                      {
                        CYS32 *data = (CYS32 *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VS64   :
                      {
                        CYS64 *data = (CYS64 *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VU8    :
                      {
                        CYU8  *data = (CYU8  *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VU16   :
                      {
                        CYU16 *data = (CYU16 *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VU32   :
                      {
                        CYU32 *data = (CYU32 *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VU64   :
                      {
                        CYU64 *data = (CYU64 *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VF32   :
                      {
                        CYF32 *data = (CYF32 *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::VF64   :
                      {
                        CYF64 *data = (CYF64 *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::Time   :
                      {
                        CYTime *data = (CYTime *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::Sec    :
                      {
                        CYTSec *data = (CYTSec *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::Word   :
                      {
                        CYWord *data = (CYWord *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::Bool   :
                      {
                        CYBool *data = (CYBool *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::String :
                      {
                        CYString *data = (CYString *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      case Cy::Color :
                      {
                        CYColor *data = (CYColor *)d;
                        if ((data->val()!=data->def()) || !data->helpDesignerValue())
                          return false;
                        break;
                      }
      default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+d->objectName());
                        return false;
    }
  }

  return true;
}


bool CYDB::removeData(QString dataName)
{
  CYData *data = datas.value(dataName);
  if (data)
  {
    removeData(data);
    return true;
  }
  else
  {
    CYWARNINGTEXT(tr("Cannot remove %1 from %2").arg(dataName).arg(objectName()));
    return false;
  }
}

void CYDB::removeData(CYData *data)
{
  datas.remove(data->objectName());
  if (data->mode()==Cy::User)
  {
    bufDatas.remove(data->objectName());
  }
  if (core && mGlobal)
    core->db.remove(data->objectName());
  else
    delete data;
}

QString CYDB::inputHelp(CYData *d, bool whole, bool withGroup)
{
  switch(d->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::Word   :
                    {
                      CYWord *data = (CYWord *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::String :
                    {
                      CYString *data = (CYString *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    case Cy::Color :
                    {
                      CYColor *data = (CYColor *)d;
                      return data->inputHelp(whole, withGroup);
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+d->objectName());
                      return "";
  }
}

QString CYDB::exportDatas()
{
  CYData *d;
  QString lines, txt;
  QHashIterator<QString, CYData *> i(datas);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    d = i.value();
    lines.append(d->exportData());
  }

  return lines;
}

void CYDB::createEntitiesDocBook()
{
  CYData *d;
  QHashIterator<QString, CYData *> i(datas);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    d = i.value();
    core->addDocBookEntity(d->objectName(), core->html2DocBook(inputHelp(d, true, false)));
  }
}

const QString &CYDB::prefixName()
{
  return mPrefixName;
}

const QString &CYDB::suffixName()
{
  return mSuffixName;
}
