//
// C++ Interface: cycolor
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYCOLOR_H
#define CYCOLOR_H


// QT
#include <qcolor.h>
// CYLIBS
#include "cytype.h"

/** CYColor est une donnée couleur.
  * @short Donnée couleur.
  * @author Gérald LE CLEACH
  */

class CYColor : public CYData
{
  Q_OBJECT
public:
  /** Attr est la structure de saisie des attributs d'une donnée couleur. */
  struct Attr
  {
    /** Nom. */
    const QString &name;
    /** Pointe sur le nom d'une couleur Qt. */
    const char *color;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Etiquette. */
    QString label;
  };

  /** Création d'une donnée couleur.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param label Etiquette.
    * @param mode  Mode d'utilisation. */
  CYColor(CYDB *db, const QString &name, QColor *val=0, QString label=0, Cy::Mode mode=Sys);

  /** Création d'une donnée couleur.
    * @param db       Base de données parent.
    * @param name     Nom de la donnée.
    * @param color    Pointe sur le nom d'une couleur Qt.
    * @param label    Etiquette.
    * @param mode     Mode d'utilisation. */
  CYColor(CYDB *db, const QString &name, const char *color, QString label=0, Cy::Mode mode=Sys);

  /** Création d'une donnée couleur.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYColor(CYDB *db, const QString &name, Attr *attr);

  ~CYColor();

  /** @return la valeur. */
  virtual QColor val();
  /** Saisie une valeur. */
  virtual void setVal(QColor val);

  /** @return la valeur courrante en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(bool withUnit=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

  /** @return la valeur \a val en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(QColor val, bool withUnit=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

  /** @return l'aide à la saisie.
    * @param whole Aide entière avec la valeur par défaut.
    * @param bool     Aide avec le groupe de la donnée. */
  virtual QString inputHelp(bool whole=true, bool withGroup=true);

  /** Détection de changement de valeur. */
  virtual bool detectValueChanged();

  /** @return la valeur par défaut du constructeur. */
  virtual QColor def() { return mDef; }

  /** @return l'ancienne valeur. */
  virtual QColor oldVal() { return mOldVal; }
  /** Saisie l'ancienne valeur. */
  virtual void setOldVal(QColor val) { mOldVal = val; }

  /** @return une ligne donnée pour le fichier d'export des données avec les 
    * champs suivants espacés d'une tabulation:
    * Hôte(s), connexion(s), groupe, nom, description, valeur, def, min, max, aide.
    * @see CYData::exportDatas() */
  virtual QString exportData();

protected: // Protected attributes
  /** Valeur. */
  QColor *mVal;
  /** Valeur par défaut du constructeur. */
  QColor mDef;
  /** Ancienne valeur. */
  QColor mOldVal;
};

#endif
