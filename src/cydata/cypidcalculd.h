/***************************************************************************
                          cypidcalculd.h  -  description
                             -------------------
    début                  : lun mai 12 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYPIDCALCULD_H
#define CYPIDCALCULD_H

// CYLIBS
#include "cyflag.h"

/** CYPIDCalculD représente le flag de calcul de la dérivée d'un PID.
  * S'il vaut 1 alors calcul de la dérivée sur mesure sinon sur écart.
  * @short Flag de dérive de PID.
  * @author Gérald LE CLEACH
  */

class CYPIDCalculD : public CYFlag
{
  Q_OBJECT
public:
  /** Création d'un flag de calcul de la dérivée de PID.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param def   Valeur par défaut. */
  CYPIDCalculD(CYDB *db, const QString &name, flg *val, const flg def=0);

  ~CYPIDCalculD();
};

#endif
