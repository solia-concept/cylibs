/***************************************************************************
                          cydb.h  -  description
                             -------------------
    début                  : sam sep 6 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDB_H
#define CYDB_H

// QT
#include <QHash>
#include <QFile>
#include <QDomElement>
#include <QList>
#include <QTimer>
// CYLIBS
#include "cytypes.h"

class CYCore;
class CYRecord;
class CYNetHost;
class CYNetLink;
class CYAcquisitionFast;
class CYDiscontBuffering;
class CYData;

/** CYDB est une base de données. Elle est capable de gèrer les mises à jour des
  * données d'une structure passant sur le réseau à l'aide du CYNetHost qui lui
  * est rattaché. Elle peut également contenir des données qui ne passent pas sur
  * le réseau. Par défaut elle est rattachée à la base de données de l'application
  * afin de faciliter l'accès des données aux différent objets. Cet accès se fait
  * par une recherche dans la base de données globale de la donnée grâce à son nom.
  * Ceci interdit donc d'avoir différentes données avec le même nom. Dans certains
  * cas ceci peut être gênant et c'est pourquoi il est possible de ne pas rattacher
  * automatiquement cette base de données à la base de donnée globale.
  * @short Base de données.
  * @author Gérald LE CLEACH
  */

class CYDB : public QObject
{
  Q_OBJECT

public:
  /** Construit une base de données locale.
    * @param parent Parent.
    * @param name   Nom.
    * @param no     Numéro de structure (si<0 => pas de structure réseau).
    * @param msec   Tempo de rafraîchissement en ms.
    * @param global Fait partie de la base de données globale.
    * @param prefix Préfix du nom de cette base ainsi que celui de toutes ses données
    * @param suffix Suffix du nom de cette base ainsi que celui de toutes ses données */
  CYDB(QObject *parent, const QString &name=0, int no=-1, int msec=0, bool global=false, const QString &prefix=0, const QString &suffix=0);

  /** Construit une base de données réseau.
    * @param host   Hôte de rattachement.
    * @param name   Nom.
    * @param no     Numéro de structure (si<0 => pas de structure réseau).
    * @param msec   Tempo de rafraîchissement en ms.
    * @param global Fait partie de la base de données globale.
    * @param prefix Préfix du nom de cette base ainsi que celui de toutes ses données
    * @param suffix Suffix du nom de cette base ainsi que celui de toutes ses données */
  CYDB(CYNetHost *host, const QString &name=0, int no=-1, int msec=0, bool global=true, const QString &prefix=0, const QString &suffix=0);

  /** Construit une base de données réseau avec 2 hôtes.
    * @param host1  Hôte de rattachement 1.
    * @param host2  Hôte de rattachement 2.
    * @param name   Nom.
    * @param no     Numéro de structure (si<0 => pas de structure réseau).
    * @param msec   Tempo de rafraîchissement en ms.
    * @param global Fait partie de la base de données globale.
    * @param prefix Préfix du nom de cette base ainsi que celui de toutes ses données
    * @param suffix Suffix du nom de cette base ainsi que celui de toutes ses données */
  CYDB(CYNetHost *host1, CYNetHost *host2, const QString &name=0, int no=-1, int msec=0, bool global=true, const QString &prefix=0, const QString &suffix=0);

  ~CYDB();

  /** @return  \a true si cette base fait partie de la base de données globale. */
  bool isGlobal() { return mGlobal; }

  /** @return le pointeur sur la structure réseau si celle-ci existe,
    * sinon retourne 0. */
  void *ptrStruct();
  /** @return la taille de la structure réseau si celle-ci existe,
    * sinon retourne 0. */
  virtual int sizeStruct();
  /** @return le numéro de la structure réseau si celle-ci existe,
    * sinon retourne un numéro négatif (par défaut -1)). */
  int noStruct() { return mNo; }
  /** @return le nombre de structures dans le buffer. */
  int bufStruct() { return mBufStruct; }

  bool isReadMode();
  bool isWriteMode();
  bool isModbus();
  bool isTCP();
  /** @return le pointeur du buffer Modbus/TCP de la structure. */
  void *buffer();

  //  /** @return le nom de l'hôte rattaché. */
  //  QString linkName();
  /** @return le nom connnections réseau rattachées. */
  QString linksName();
  /** @return la connnection réseau. */
  CYNetLink *link(int index=0) { return mLinks.at(index); }
  /** @return la connnection réseau ayant pour nom \a linkName. */
  CYNetLink *link(QString linkName);
  /** Saisie la connnection réseau. */
  void setLink(CYNetLink *l) { mLinks.append(l); }

  //  /** @return le nom de l'hôte rattaché. */
  //  QString hostName(int index=0);
  /** @return le nom des hôtes rattachés. */
  QString hostsName();
  /** @return l'hôte rattaché. */
  CYNetHost *host(int index=0) { return hosts.at(index); }
  /** @return l'hôte ayant pour nom \a hostName. */
  CYNetHost *host(QString hostName);

  /** @return le nom du fichier de stoquage de données. */
  QString fileName() { return mFileName; }

  /** Ajoute une donnée aux données locales et globales. */
  virtual void addToDB(CYData *data);
  /** Données locales. */
  QHash<QString, CYData*> datas;
  /** Données locales bufférisées. */
  QHash<QString, CYData*> bufDatas;
  /** @return toutes les données de l'application (données globales). */
  QHash<QString, CYData*> allDatas();

  /** @return la validité des données. */
  virtual bool isOk();
  /** Ses donnéee sont toujours valides. */
  void setAlwaysOk(bool state) { mAlwaysOk = state; }

  //  /** Sélectionne la structure à gérer dans un tableau de structures.
  //    * @param index index du tableau. */
  //  void selectStr(unsigned int index);

  /** @return la taille de la FIFO d'acquisition propre à la structure réseau. */
  short sizeReadFIFO() { return mSizeReadFIFO; }
  /** @return la taille de la FIFO d'émission propre à la structure réseau. */
  short sizeWriteFIFO() { return mSizeWriteFIFO; }

  /** Charge la base de données avec les valeurs constructeur. */
  virtual void designer();
  /** Charge la base de données avec les valeurs contenues dans le fichier de sauvegarde. */
  virtual int load();
  /** Charge la base de données avec les valeurs contenues dans le fichier passé en paramètre. */
  virtual int load(QString &fileName);
  /** Charge la base de données avec les valeurs à partir d'un QDomElement.
    * @param def Précharge avec les valeurs constructeurs.*/
  virtual bool loadFromDOM(QDomElement &db, bool def=true);
  /** Sauvegarde les valeurs de la base de données dans un fichier de sauvegarde. */
  virtual int save();
  /** Sauvegarde les valeurs de la base de données dans un fichier passé en paramètre. */
  virtual int save(QString &fileName);
  /** Sauvegarde les valeurs de la base de données dans un QDomElement. */
  virtual bool saveToDOM(QDomDocument &doc, QDomElement &db);
  bool hasOnlyDesignerValues();

  /** @return \a true si l'état de la connection de toutes les coms de la base OK. */
  virtual bool linksOn();
  /** Lecture des données.
    * @param index Index de la structure réseau.
    *              (uniquement pour les tableaux de structures réseau). */
  virtual bool readData(unsigned int index=0);
  /** Lecture données locales. */
  virtual int readLocal() { return 0; }
  /** Ecriture des données.
    * @param index Index de la structure réseau.
    *              (uniquement pour les tableaux de structures réseau). */
  virtual bool writeData(unsigned int index=0);
  /** Ecriture données locales. */
  virtual bool writeLocal() { return false; }

  /** @return le temps de rafraîchissement en ms de la base de donnée. */
  int refreshTime();
  /** @return \a true si la base de donnée est en cours de lecture. */
  bool reading() { return mReading; }
  /** Initialise les valeurs des données de forçage. */
  virtual void initForcingValues();
  /** Ajoute une acquisition rapide. */
  virtual void addAcquisition(CYAcquisitionFast *acq);
  /** Ajoute une bufferisation discontinue. */
  virtual void addDiscontBuffering(CYDiscontBuffering *buf);
  /** @return \a true si la base de données est modifiée @see setModified(). */
  bool isModified() { return mModified; }

  /** @return \a  \a true s'il s'agit d'une base de données en lecture rapide. */
  bool isReadFast() { return mIsReadFast; }

  /** Saisir \a true pour sauvegarder lors de l'application des données. */
  void setSaveWithApply(bool enable) { mSaveWithApply = enable; }
  /** @return \a true si une sauvegarde est faite lors de l'application des données. */
  bool saveWithApply() { return mSaveWithApply; }

  /** Saisir \a true pour envoyer au RN lors de l'application des données. */
  void setWriteWithApply(bool enable) { mWriteWithApply = enable; }
  /** @return \a true si un envoi au RN est fait lors de l'application des données. */
  bool writeWithApply() { return mWriteWithApply; }
  virtual QString DOMTagDatas();

  /** Lecture des flags. Cette fonction est appelée après la lecture
    * de la structure afin de mettre à jour les flags par rapport à un
    * tableau de mots dans la structure.
    * @param str  Index de la structure réseau.
    *             (uniquement pour les tableaux de structures réseau).*/
  virtual void readFlags(int str=0);
  void setHelpDesignerValue(bool val);
  bool helpDesignerValue();
  void changeDataName(QString oldName, QString newName);
  CYData * findWithOldName(QString oldName);
  void setSavingDataName(QString savingName, QString dataName);
  CYData * findWithSavingName( QString savingName );


  /** Suppression d'une donnée ayant pour nom \a dataName. */
  virtual bool removeData(QString dataName);
  /** Suppression d'une donnée */
  virtual void removeData(CYData *data);


  /** @return l'aide à la saisie de la donnée.
      * @param whole    Aide entière ex: pour les données numériques avec les bornes, la précision si il y en a une.
      * @param bool     Aide avec le groupe de la donnée. */
  virtual QString inputHelp(CYData *d, bool whole=true, bool withGroup=true);

  /** @return les données de cette base de données formatées par ligne pour un export en fichier CSV.
      * @see CYData::exportData() */
  virtual QString exportDatas();
  /** Chaque entité a pour nom celui d'une donnée de Cylix et comme valeur l'équivalent en XML de l'aide en bulle de saisie. Il est ainsi possible de récupérer le texte d'aide des sources de Cylix pour l'intégrer dans le manuel en ligne.
    @see CYCore::createEntitiesDocBook() */
  virtual void createEntitiesDocBook();

  /*! \return le préfix du nom de cette base ainsi que celui de toutes ses données. */
  const QString &prefixName();
  /*! \return le suffix du nom de cette base ainsi que celui de toutes ses données. */
  const QString &suffixName();

  /** @return le sous-groupe d'appartenance. */
  virtual QString underGroup() { return mUnderGroup; }
  /// @return le complément d'information ajouté au début des libellés des données lors de leur modification.
  virtual QString prefixLabel() { return mPrefixLabel; }
  /// @return le complément d'information ajouté à la fin des libellés des données lors de leur modification.
  virtual QString suffixLabel() { return mSuffixLabel; }

public slots: // Public slots
  /** Rafraîchit les valeurs des données entrantes. */
  virtual void refresh();
  /** Saisie le nom du fichier de stoquage de données. */
  void setFileName(const QString &filename);
  /** Redimensionne la taille de la base de données */
  virtual void resizeDB();
  /** @return \a true si le chargement automatique des données dès
    * l'établissement des communications est authorisé. */
  bool autoLoad();
  /** Saisie le nom du fichier de sauvegarde. */
  virtual void setRecord(const QString &filename);
  /** Applique les nouvelles valeurs des données. Emet sur le réseau
    * ces valeurs et les sauvegarde si nécessaire dans un fichier. */
  virtual void apply();
  /** Place la base de données comme étant modifiée. Cette fonctione
    * est appelée lorsque une de ces donnée change de valeur. Pour valider
    * ce changement il faut utiliser @see apply(). */
  virtual void setModified();
  /** @return \a true si l'hôte \host est bien un hôte de cette base de données. */
  virtual bool isHostDB(CYNetHost *host);
  /** Ajoute un hôte. */
  virtual void addHost(CYNetHost *host);

  /** @return le groupe d'appartenance. */
  virtual QString group() { return mGroup; }
  /** Saisie le groupe d'appartenance. */
  virtual void setGroup(QString txt) { mGroup = txt; }

  /** Saisie le sous-groupe d'appartenance.
    * Le sous-groupe permet d'initialiser un texte une seule fois avant la définition des données relatives à ce sous-groupe.
    * Lors de la création de la donnée celle-ci utilisera ce texte par défaut comme sous-groupe. */
  virtual void setUnderGroup(const QString &txt, const QString &prefixLabel=0, const QString &suffixLabel=0)
  {
    mUnderGroup = txt;
    mPrefixLabel = prefixLabel;
    mSuffixLabel = suffixLabel;
  }
  /// Saisie le complément d'information ajouté au début des libellés des données lors de leur modification.
  virtual void setPrefixLabel(const QString &txt) { mPrefixLabel = txt; }
  /// Saisie le complément d'information ajouté à la fin des libellés des données lors de leur modification.
  virtual void setSuffixLabel(const QString &txt) { mSuffixLabel = txt; }

  /** Démarre ler timer de rafraîchissement. */
  virtual void startRefreshTimer();
  virtual void copy();
  virtual void paste();
  virtual QString toXML();
  virtual bool fromXML(QString text);
  virtual void setDOMTagDatas(QString tag);

signals:
  /** Signale que la base de données vient d'être modifiée. */
  void update();
  /** Emet un signal d'attente opérateur.
    * @param id Identificateur de l'attente. */
  void waitOperator(int id);
  void datasRead();
  void datasWrited();
  void loaded();
  void saved();

private: // Private methods
  /** Initialise la base de données. */
  void init();

public: // Public attributes
  /** Hôte(s) de rattachement. */
  QList<CYNetHost*> hosts;

protected: // Protected attributes
  /** Numéro de structure réseau. */
  int  mNo;

  /** Connection(s) réseau. */
  QList<CYNetLink*> mLinks;

  /** Sauvegarde binaire d'une structure réseau. */
  CYRecord  *mRecord;
  /** Nom du fichier de stoquage de données. */
  QString mFileName;

  /** Taille de la FIFO d'acquisition propre à la structure réseau. */
  short mSizeReadFIFO;
  /** Taille de la FIFO d'émission propre à la structure réseau. */
  short mSizeWriteFIFO;

  /** Validité des données. */
  bool mOk;
  /** SI \a true cette donnée est toujours valide. */
  bool mAlwaysOk;
  /** Permet un chargement automatique des hôtes dès l'établissement des communications. */
  bool mAutoLoad;
  /** Vaut \a true s'il s'agit d'une base de données en lecture rapide. */
  bool mIsReadFast;

  /** Timer de rafraîchissement. */
  QTimer *mTime;
  /** Temps de rafraîchissement en ms de la base de donnée. */
  int mRefreshTime;

  /** Nombre de structures dans le buffer. */
  int mBufStruct;
  /** Fait partie de la base de données globale. */
  bool mGlobal;
  /** Flag d'authorisation de lecture des données. Ce flag est mis à 0 lors de
    * la bufférisation des données afin d'éviter toutes interférences entre
    * cette action et la mise à jour des valeurs lors de la lecture. */
  bool mEnableRead;
  /** Flag permettant la mise en pause de la lecture.  */
  bool mPauseRead;
  /** Vaut \a true lors de la lecture. */
  bool mReading;
  /** Liste d'acquisitions rapides rattachées à cette base de données. */
  QList<CYAcquisitionFast*> mAcquisitions;
  /** Liste des bufferisations discontinues rattachées à cette base de données. */
  QList<CYDiscontBuffering*> mDiscontufferings;

  /** Indique si la base de données est modifiée @see setModified(). */
  bool mModified;
  /** Groupe d'appartenance. */
  QString mGroup;
  /** Sous-groupe d'appartenance. */
  QString mUnderGroup;
  /** Vaut \a true pour sauvegarder lors de l'application des données. */
  bool mSaveWithApply;
  /** Vaut \a TRUE pour envoyer au RN lors de l'application des données. */
  bool mWriteWithApply;
  bool mCopying;
  int mPasting;
  QString mDOMTagDatas;
  bool mHelpDesignerValue;
  QHash<QString, QString*> mOldDataName;

  bool mIsTCP;

  /** Permet d'ajouter automatiquement un préfix au nom des données à leur création. */
  QString mPrefixName;
  /** Permet de recharger le fichier par une autre base de données ayant
    * les mêmes nom de données mais avec un préfix différents.
    * En cas de projets courants multiples par exemple pour un projet par poste avec
    * donc des données indicées par poste, le chargement d'un projet édité à partir
    * d'un poste est possible pour un autre poste. */
  bool mSavingWithPrefix;

  /** Permet d'ajouter automatiquement un suffix nom des données à leur création. */
  QString mSuffixName;
  /** Permet de recharger le fichier par une autre base de données ayant
    * les mêmes nom de données mais avec un suffix différents.
    * En cas de projets courants multiples par exemple pour un projet par poste avec
    * donc des données indicées par poste, le chargement d'un projet édité à partir
    * d'un poste est possible pour un autre poste. */
  bool mSavingWithSuffix;
  /** Version de CYLIBS lors de l'enregistrement du fichier de sauvegarde */
  QString mVersionCYDOM;

  /// Complément d'information ajouté au début des libellés des données lors de leur modification.
  QString mPrefixLabel;
  /// Complément d'information ajouté à la fin des libellés des données lors de leur modification.
  QString mSuffixLabel;
};

#endif
