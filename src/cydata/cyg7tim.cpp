/***************************************************************************
                          cyg7tim.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyg7tim.h"

CYG7Tim::CYG7Tim(CYDB *db, const QString &name, Attr *attr)
  : CYTime(db, name, attr->val, attr->format, attr->base, attr->mode)
{
  mG7    = attr->grafcet;
  mNo    = attr->no;
  mHelp  = attr->help;
  mLabel = tr("Timer %1 of grafcet %2").arg(mNo).arg(mG7);
  mGroup = tr("Grafcet timer");
}

CYG7Tim::~CYG7Tim()
{
}
