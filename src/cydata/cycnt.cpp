/***************************************************************************
                          cycnt.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cycnt.h"

// CYLIBS
#include "cystring.h"

CYCnt::CYCnt(CYDB *db, const QString &name, t_cu32 *val, int format, Mode mode)
  : CYU32(db, name, &(val->val), format, mode)
{
  mGroup = tr("Total counter");
  mHelp  = tr("Counter used for maintenance");

  tmp = new CYU32(db, QString("TMP_%1").arg(name), &(val->tmp), mNumFormat, mMode);
  tmp->setLabel(mLabel);
  tmp->setGroup(tr("Partial counter"));
  tmp->setHelp(mHelp);

  note = new CYString(db, QString("NOTE_%1").arg(name));
  note->setLabel(mLabel);
  note->setHelp(mHelp);
  note->setGroup(tr("Note"));
}

CYCnt::CYCnt(CYDB *db, const QString &name, t_cu32 *cnt, QString label, int format, Cy::Mode mode)
  : CYU32(db, name, format, mode)
{
  mLabel = label;
  mGroup = tr("Total counter");
  mHelp  = tr("Counter used for maintenance");
  mVal   = &(cnt->val);

  tmp = new CYU32(db, QString("TMP_%1").arg(name), &(cnt->tmp), mNumFormat, mMode);
  tmp->setLabel(mLabel);
  tmp->setGroup(tr("Partial counter"));
  tmp->setHelp(mHelp);

  note = new CYString(db, QString("NOTE_%1").arg(name));
  note->setLabel(mLabel);
  note->setHelp(mHelp);
  note->setGroup(tr("Note"));
}

CYCnt::CYCnt(CYDB *db, const QString &name, Attr *attr)
  : CYU32(db, name, attr->format, attr->mode)
{
  mGroup = tr("Total counter");
  mHelp  = tr("Counter used for maintenance");
  mVal   = &(attr->cnt->val);

  tmp = new CYU32(db, QString("TMP_%1").arg(name), &(attr->cnt->tmp), mNumFormat, mMode);
  tmp->setLabel(mLabel);
  tmp->setGroup(tr("Partial counter"));
  tmp->setHelp(mHelp);

  note = new CYString(db, QString("NOTE_%1").arg(name));
  note->setLabel(mLabel);
  note->setHelp(mHelp);
  note->setGroup(tr("Note"));
}

CYCnt::~CYCnt()
{
}
