/***************************************************************************
                          cymeasure.cpp  -  description
                             -------------------
    début                  : mar sep 9 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cymeasure.h"

// QT
#include <QPainter>
// CYLIBS
#include "cytsec.h"
#include "cyflag.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cyu32.h"
#include "cystring.h"
#include "cydatetime.h"
#include "cyevent.h"
#include "cycommand.h"
#include "cycore.h"
#include "cyadc16.h"
#include "cyuser.h"
#include "cymeasuresetting.h"

CYMeasure::CYMeasure(CYDB *db, const QString &name, f32 *val, flg *flg, int format, Mode mode)
  : CYF32(db, name, val, format, mode)
{
  init();
  mMetroEnable = false;
  mMetroCalEnable = true;
  mMetroEditing = false;
  mMetroDesigner = false;

  mFl = new CYFlag(db, QString("FL_%1").arg(name), flg);
  mFl->setLabel(tr("Flag"));
  setGroup(mGroup);
}

CYMeasure::CYMeasure(CYDB *db, const QString &name, Attr *attr)
  : CYF32(db, name, attr->val, attr->format, attr->mode)
{
  init();

  mMetroEnable = false;
  mMetroCalEnable = true;
  mMetroEditing = false;
  mMetroDesigner = false;

  mFl = new CYFlag(db, QString("FL_%1").arg(name), attr->flag);
  mFl->setLabel(tr("Flag"));
  setLabel(attr->label);
  setGroup(mGroup);
}

CYMeasure::CYMeasure(CYDB *db, const QString &name, Attr2 *attr)
  : CYF32(db, name, attr->val, attr->format, attr->mode)
{
  init();

  mMetroEnable = false;
  mMetroCalEnable = true;
  mMetroEditing = false;
  mMetroDesigner = false;

  mElec = attr->elec;
  mPhys = attr->phys;

  mFl = new CYFlag(db, QString("FL_%1").arg(name), attr->flag);
  mFl->setLabel(tr("Flag"));
  setLabel(attr->label);
  setGroup(mGroup);
}

CYMeasure::CYMeasure(CYDB *db, const QString &name, AttrMetro *attr)
  : CYF32(db, name, attr->val, attr->format, attr->mode)
{
  init();

  mMetroEnable = false;
  mMetroCalEnable = true;
  mMetroEditing = false;
  mMetroDesigner = false;

  mElec = attr->elec;
  mPhys = attr->phys;

  mUncertainTypeDef=attr->mUncertainType;
  mUncertainValDef=attr->mUncertainVal;

  db->setUnderGroup(group()+":"+label());
  mLow = new CYF32(db, QString("LOW_%1").arg(name), new f32, attr->ldef, mNumFormat);
  mLow->setFormat(format());
  mTop = new CYF32(db, QString("TOP_%1").arg(name), new f32, attr->tdef, mNumFormat);
  mTop->setFormat(format());
  mFl = new CYFlag(db, QString("FL_%1").arg(name), attr->flag);
  mFl->setLabel(tr("Flag"));
  db->setUnderGroup(0);
  setLabel(attr->label);
}

CYMeasure::~CYMeasure()
{
}

void CYMeasure::init()
{
  calibrateLast = 0;
  calibrateLastCtrl = 0;
  calibrateNext = 0;
  calibrateDateCtrl = 0;
  calibrateNoting =0;
  calibrateChanged =0;
  calibrateOutdated=0;
  mSetting=0;
  mCtrlForcing = 0;
  mTop = 0;
  mLow = 0;
  metroDB = 0;
  mDirect = 0;
}

void CYMeasure::setLabel(const QString l)
{
  CYF32::setLabel(l);
}

void CYMeasure::setGroup(const QString g)
{
  CYF32::setGroup(g);
  mFl->setGroup(g+":"+label());
  if (mTop)
    mTop->setGroup(g+":"+label());
  if (mLow)
    mLow->setGroup(g+":"+label());
  if (mDirect)
  {
    if (mDirect->mTop)
      mDirect->mTop->setGroup(g+":"+mDirect->label());
    if (mDirect->mLow)
      mDirect->mLow->setGroup(g+":"+mDirect->label());
  }

  if (calibrateNext)
    calibrateNext->setGroup(g);
  if (calibrateDateCtrl)
    calibrateDateCtrl->setGroup(g);
  if (calibrateNoting)
    calibrateNoting->setGroup(g);
}

f32 CYMeasure::forcing()
{
  if(mDataForcing==0)
  {
    CYWARNINGTEXT(objectName());
  }
  else
  {
    CYF32 *d = (CYF32 *)mDataForcing;
    return d->val();
  }
  return -1;
}

void CYMeasure::setForcing(f32 val)
{
  if(mDataForcing==0)
  {
    CYWARNINGTEXT(objectName());
  }
  else
  {
    CYF32 *d = (CYF32 *)mDataForcing;
    d->setVal(val);
  }
}

void CYMeasure::setCtrlForcing(int value)
{
  if (!mCtrlForcing)
    return;
  mCtrlForcing->setVal(value);
  CYF32 *output = mOutputForcing[value];
  if (!output)
    CYFATAL
  mDataForcing = output;
  CYF32 *input = mInputForcing[value];
  output->setVal(input->val());
  emit ctrlForcing((int)value);
}

/*! Initialise le forçage.
  @param ctrl Donnée de contrôle de forçage envoyé au régulateur.
  \fn CYMeasure::initForcing(CYFlag *ctrl)
 */
void CYMeasure::initForcing(CYFlag *ctrl)
{
  mCtrlForcing = ctrl;
}

/*! Ajoute un type de forçage.
    @param output Donnée de forçage.
    @param input  Donnée d'initialisation.
    @param desc   Description du forçage.
    \fn CYMeasure::addForcingData(CYData *data, CYData *init, QString desc)
 */
void CYMeasure::addForcing(CYF32 *output, CYF32 *input, QString desc)
{
  mOutputForcing.insert(mOutputForcing.count(), output);
  mInputForcing.insert(mInputForcing.count(), input);
  mDescForcing.insert(mDescForcing.count(), new QString(desc));
  if (nbForcing()==1)
    setCtrlForcing(0);
}


/*! Initialise la valeur de forçage.
    \fn CYMeasure::initForcingValue()
 */
void CYMeasure::initForcingValue()
{
  if (!mCtrlForcing)
    return;
  mCtrlForcing->val();
  CYF32 *output = mOutputForcing[mCtrlForcing->val()];
  if (!output)
    CYFATAL
  mDataForcing = output;
  CYF32 *input = mInputForcing[mCtrlForcing->val()];
  output->setVal(input->val());
}

void CYMeasure::setCali(CYDB *db, QString date)
{
  calDB = db;
  mInitCalLast = date;
  initCali();
}

void CYMeasure::setCali(CYDB *db, CYMeasure *direct, t_calnum32 *vcal, f32 low, f32 top, f32 lowDirect, f32 topDirect, QString date)
{
  calDB = db;
  db->setUnderGroup(group()+":"+label());

  mInitCalLast = date;
  mDirect = direct;
  mDirect->setEnableColor(Qt::cyan);
  mDirect->setDisableColor(Qt::red);

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLdef = low;
  mTdef = top;
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mDirect->mLow = new CYF32(db, QString("LOW_%1").arg(mDirect->objectName()), &(vcal->captmin), mDirect->numFormat(), Cy::Sys);
  mDirect->mLow->setDef(lowDirect);

  mDirect->mTop = new CYF32(db, QString("TOP_%1").arg(mDirect->objectName()), &(vcal->captmax), mDirect->numFormat(), Cy::Sys);
  mDirect->mTop->setDef(topDirect);

  initCali();
  db->setUnderGroup(0);
}

CYEvent *CYMeasure::initFault(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  if (addr()!="")
    desc = desc+QString(" (%1-%2-%3):\n%4 !").arg(phys()).arg(elec()).arg(addr()).arg(label());
  else
    desc = desc+QString(" (%1-%2):\n%3 !").arg(phys()).arg(elec()).arg(label());
  CYEvent *event = CYData::initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  return event;
}


CYEvent *CYMeasure::initAlert(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  if (addr()!="")
    desc = desc+QString(" (%1-%2-%3):\n%4 !").arg(phys()).arg(elec()).arg(addr()).arg(label());
  else
    desc = desc+QString(" (%1-%2):\n%3 !").arg(phys()).arg(elec()).arg(label());
  CYEvent *event = CYData::initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  return event;
}

void CYMeasure::initCali()
{
  CYDB *db = calDB;

  db->changeDataName( QString("DATE_%1").arg(objectName()), QString("NEXT_CAL_%1").arg(objectName()) );

  db->setUnderGroup(group()+":"+label());

  //étendue de mesure
  mEMLow = new CYF32(db, QString("EMLOW_%1").arg(objectName()), new f32, mNumFormat);
  mEMTop = new CYF32(db, QString("EMTOP_%1").arg(objectName()), new f32, mNumFormat);
  mEMLow->setFormat(format());
  mEMTop->setFormat(format());
  mEMLow->setDef(mEMLdef);
  mEMTop->setDef(mEMTdef);

  calibrateLast    = new CYDateTime(db, QString("LAST_CAL_%1").arg(objectName()), new QString(mInitCalLast));

  calibrateLastCtrl= new CYDateTime(db, QString("LAST_CAL_CTRL_%1").arg(objectName()), new QString("2000-01-01T00:00:00"));
  QDateTime dt(CYDateTime::fromString(mInitCalLast));
  dt = dt.addMonths(12);
  QString txt = dt.date().toString(Qt::ISODate);
  calibrateNext    = new CYDateTime(db, QString("NEXT_CAL_%1").arg(objectName()), new QString(txt), txt);
  calibrateNext->setHelpDesignerValue(false);

  calibrateDateCtrl= new CYFlag(db, QString("DATE_CTRL_%1").arg(objectName()), new flg, false);
  calibrateNoting  = new CYString(db, QString("NOTING_%1").arg(objectName()));

  QString desc;
  if (!addr().isEmpty())
    desc = QString(" [%1]").arg(addr());
  if (!phys().isEmpty() && !elec().isEmpty())
    desc.append(QString(" (%1-%2):\n%3").arg(phys()).arg(elec()).arg(label()));
  else if (phys().isEmpty() && !elec().isEmpty())
    desc.append(QString(" (%1):\n%2").arg(elec()).arg(label()));
  else if (!phys().isEmpty() && elec().isEmpty())
    desc.append(QString(" (%1):\n%2").arg(phys()).arg(label()));
  else
    desc.append(QString(" \n%1").arg(label()));

  QString from = tr("Calibration");
  QString help, helpEnd;
  flg *val_tocal = new flg;
  (*val_tocal) = false;
  help = tr("Indicates that the date of calibration of the sensor is exceeded.");
  CYEvent *event_tocal = CYData::initAlert(core->eventsGeneratorSPV, QString(objectName())+"_DATE_CAL", val_tocal, helpEnd, SI_1, REC|SCR|BST, QString(tr("Sensor to calibrate")+desc), from, 0, help);
  setCalibrateOutdatedEvent(event_tocal);

  flg *val_caled = new flg;
  (*val_caled) = false;
  help = tr("Indicates that the sensor was calibrated to the onset of this event.");
  CYEvent *event_caled = CYData::initEvent(core->eventsGeneratorSPV, QString(objectName())+"_CAL", val_caled, helpEnd, SI_1, REC|RAZ, QString(tr("Sensor calibrated")+desc), from, 0, help);
  event_caled->setEnableColor(Qt::blue);
  setCalibrateChangedEvent(event_caled);

  calibrateOk = new CYFlag(calDB, QString("CAL_OK_%1").arg(objectName()), new flg, false);
  calibrateOk->setLabel(tr("Flag of validity of the calibration"));

  initCalText();
  db->setUnderGroup(0);
}

void CYMeasure::initCalText()
{
  if (mTop)
  {
    mTop->setLabel(tr("High value")+" "+elec());
    mTop->setHelp(mHelp);
  }

  if (mLow)
  {
    mLow->setLabel(tr("Low value")+" "+elec());
    mLow->setHelp(mHelp);
  }
  if (mDirect)
  {
    if (mDirect->mTop)
    {
      mDirect->mTop->setLabel(tr("High value (not calibrated)")+" "+elec());
      mDirect->mTop->setHelp(mHelp);
      mDirect->mTop->addNote(tr("Maximum value of device usage range."));
    }

    if (mDirect->mLow)
    {
      mDirect->mLow->setLabel(tr("Low value (not calibrated)")+" "+elec());
      mDirect->mLow->setHelp(mHelp);
      mDirect->mLow->addNote(tr("Minimum value of device usage range."));
    }
  }

  if (calibrateLast)
  {
    calibrateLast->setLabel(tr("Date of last calibration"));
  }

  if (calibrateLastCtrl)
  {
    calibrateLastCtrl->setLabel(tr("Date of last verification"));
  }

  if (calibrateNext && !mMetroEnable)
  {
    calibrateNext->setLabel(tr("Date of next calibration"));
    calibrateNext->setHelp(tr("If this date is expired an alert will be generated to remind to calibrate the sensor %1.").arg(phys()));
  }
  if (calibrateNext && mMetroEnable)
  {
    calibrateNext->setLabel(tr("Date of next verification"));
    calibrateNext->setHelp(tr("If this date is expired an alert will be generated to remind to calibrate the sensor %1.").arg(phys()));
  }

  if (calibrateDateCtrl && !mMetroEnable)
  {
    calibrateDateCtrl->setLabel(tr("Calibration control"));
    calibrateDateCtrl->setHelp(mHelp);
  }
  if (calibrateDateCtrl && mMetroEnable)
  {
    calibrateDateCtrl->setLabel(tr("Alert for next verification"));
    calibrateDateCtrl->setHelp(tr("Generates an alert in case of overtaking of the date of next verification."));
    metroList.append(calibrateDateCtrl);
  }

  if (calibrateNoting)
  {
    calibrateNoting->setLabel(tr("Note")+" "+elec());
    calibrateNoting->setHelp(mHelp);
  }
}

void CYMeasure::setCalMetro(CYDB *db, int max_pts, s16 *nb_pts, t_cal_metro points[])
{
  mMetroEnable = true;

  metroDB = db;
  core->addMetro(this);
  metroDB->setWriteWithApply(false);
  mMetroMaxPts = max_pts;

  QString group = tr("Metrology")+":"+label();

  metroNb = new CYS16(metroDB, QString("METRO_NB_%1").arg(objectName()), nb_pts, 2, 2, max_pts);
  metroNb->setLabel(tr("Number of points used for calibration"));
  metroList.append(metroNb);

  metroDB->setUnderGroup(group+":"+tr("Direct bench sensor"));
  CYF32 *bdir = new CYF32(metroDB, QString("METRO_BDIR_%1").arg(objectName()), new f32);
  bdir->setFormat(format());
  bdir->setPrecision(0.0);
  bdir->setNbDec(format()->nbDec()+1);
  bdir->setLabel(tr("New point"));
  bdir->setVolatile(true);
  metroList.append(bdir);

  metroDB->setUnderGroup(group+":"+tr("Bench sensor"));
  CYF32 *bench = new CYF32(metroDB, QString("METRO_BENCH_%1").arg(objectName()), new f32, numFormat());
  bench->setFormat(format());
  bench->setPrecision(0.0);
  bench->setNbDec(format()->nbDec()+1);
  bench->setLabel(tr("New point"));
  bench->setVolatile(true);
  metroList.append(bench);

  metroDB->setUnderGroup(group+":"+tr("Reference sensor"));
  CYF32 *ref = new CYF32(metroDB, QString("METRO_REF_%1").arg(objectName()), new f32, EMlow()->val(), EMlow()->val(), EMtop()->val(), numFormat());
  ref->setFormat(format());
  ref->setPrecision(0.0);
  ref->setNbDec(format()->nbDec()+1);
  ref->setLabel(tr("New point"));
  ref->setVolatile(true);
  metroList.append(ref);

  for (int i=1; i<=mMetroMaxPts; i++)
  {
    CYF32 *bdir;
    CYF32 *bench;
    CYF32 *ref;
    t_cal_metro *point = &points[i-1];
    if (i==1)
    {
      bdir  = new CYF32(metroDB, QString("METRO_BDIR%1_%2").arg(i).arg(objectName()) , &point->val_capteur, low()->val(), low()->val(), top()->val(), numFormat());
      bench = new CYF32(metroDB, QString("METRO_BENCH%1_%2").arg(i).arg(objectName()), new f32            , low()->val(), low()->val(), top()->val(), numFormat());
      ref   = new CYF32(metroDB, QString("METRO_REF%1_%2").arg(i).arg(objectName())  , &point->val_etalon , low()->val(), EMlow()->val(), EMtop()->val(), numFormat());
    }
    else
    {
      bdir  = new CYF32(metroDB, QString("METRO_BDIR%1_%2").arg(i).arg(objectName()) , &point->val_capteur, top()->val(), low()->val(), top()->val(), numFormat());
      bench = new CYF32(metroDB, QString("METRO_BENCH%1_%2").arg(i).arg(objectName()), new f32            , top()->val(), low()->val(), top()->val(), numFormat());
      ref   = new CYF32(metroDB, QString("METRO_REF%1_%2").arg(i).arg(objectName())  , &point->val_etalon , top()->val(), EMlow()->val(), EMtop()->val(), numFormat());
    }
    bdir->setGroup(group+":"+tr("Direct bench values"));
    bdir->setFormat(format());
    bdir->setLabel(tr("Point %1").arg(i));
    metroList.append(bdir);

    bench->setGroup(group+":"+tr("Bench values"));
    bench->setFormat(format());
    bench->setLabel(tr("Point %1").arg(i));
    metroList.append(bench);

    ref->setGroup(group+":"+tr("Reference values"));
    ref->setFormat(format());
    ref->setLabel(tr("Point %1").arg(i));
    metroList.append(ref);

    CYTSec *tmoy = new CYTSec(metroDB, QString("METRO_TMOY%1_%2").arg(i).arg(objectName()), new u32);
    tmoy->setLabel(tr("Point %1").arg(i));
    tmoy->setGroup(group+":"+tr("Averaging times"));
    metroList.append(tmoy);
  }
  initCalMetro();
}

void CYMeasure::setFoncMetro(s16 index, CYS16 *imes, CYTSec *tmoy, CYCommand *cmoy, CYFlag *moy_ok, CYF32 *prec)
{
  mMetroIndex   = index;
  mMetroIMes    = imes;
  metroTMoy     = tmoy;
  mMetroCMoy    = cmoy;
  mMetroMoyOk   = moy_ok;
  mMetroMoyPrec = prec;
}

void CYMeasure::initCalMetro()
{
  metroDB->setUnderGroup(tr("Metrology")+":"+label());

  sensorCalAuto = new CYFlag(metroDB, QString("SENSOR_CAL_AUTO_%1").arg(objectName()), new flg, true);
  metroList.append(sensorCalAuto);
  sensorCalAuto->setLabel(tr("Calibrating mode"));
  sensorCalAuto->setChoiceFalse(tr("Manual calibrating"));
  sensorCalAuto->setChoiceTrue(tr("Automatic calibrating"));
  sensorCalAuto->setHelp(0,
                         tr("Adding a calibration point by reading the average of the direct value of the sensor."),
                         tr("Adding a calibration point by entering the direct value of the sensor."));

  sensorDesigner = new CYString(metroDB, QString("DESIGNER_%1").arg(objectName()), new QString, tr("Manufacter"));
  metroList.append(sensorDesigner);

  sensorPN = new CYString(metroDB, QString("PN_%1").arg(objectName()), new QString, tr("P/N (Type)"));
  metroList.append(sensorPN);

  sensorSN = new CYString(metroDB, QString("SN_%1").arg(objectName()), new QString, tr("S/N (Serial N)"));
  metroList.append(sensorSN);

  sensorUT = new CYS8(metroDB, QString("SENSOR_UT_%1").arg(objectName()), new s8, mUncertainTypeDef, UEM, UPC);
  metroList.append(sensorUT);
  sensorUT->setLabel(tr("Uncertainty type"));
  sensorUT->setChoice(UEM, tr("Uncertainty - % FS"         ), tr("The total measurement uncertainty of the sensor is expressed as a percentage of the Full Scale of the sensor. It is then fixed for each measurement and can also be expressed in sensor unit.") );
  sensorUT->setChoice(UUC, tr("Uncertainty - fixed value"  ), tr("The total measurement uncertainty of the sensor is expressed in sensor unit. It is then fixed for each measurement and can also be expressed as a percentage of the Full Scale of the sensor."));
  sensorUT->setChoice(UPC, tr("Uncertainty - % of reading" ), tr("The total measurement uncertainty of the sensor is expressed as a percentage of the read measure.") );

  sensorUEM = new CYF32(metroDB, QString("SENSOR_UEM_%1").arg(objectName()), new f32, mUncertainValDef, 0, 100);
  metroList.append(sensorUEM);
  sensorUEM->setUnit("%");
  sensorUEM->setLabel(sensorUT->choiceLabel(UEM));
  sensorUEM->setHelp(sensorUT->choiceHelp(UEM));

  sensorUUC = new CYF32(metroDB, QString("SENSOR_UUC_%1").arg(objectName()), new f32, mUncertainValDef, format()->min(), format()->max());
  metroList.append(sensorUUC);
  sensorUUC->setFormat(format());
  sensorUUC->setLabel(sensorUT->choiceLabel(UUC));
  sensorUUC->setHelp(sensorUT->choiceHelp(UUC));

  sensorUPC = new CYF32(metroDB, QString("SENSOR_UPC_%1").arg(objectName()), new f32, mUncertainValDef, 0, 100);
  metroList.append(sensorUPC);
  sensorUPC->setUnit("%");
  sensorUPC->setLabel(sensorUT->choiceLabel(UPC));
  sensorUPC->setHelp(sensorUT->choiceHelp(UPC));

  sensorEnv = new CYString(metroDB, QString("SENSOR_ENV_%1").arg(objectName()), new QString, tr("Environment"));
  metroList.append(sensorEnv);

  sensorPeriod = new CYU32(metroDB, QString("SENSOR_PERIOD_%1").arg(objectName()), new u32, 12, 0, 120);
  sensorPeriod->setUnit(tr("month(s)"));
  sensorPeriod->setLabel(tr("Periodicity of control"));
  metroList.append(sensorPeriod);

  QDateTime dt(calibrateLast->date());
  dt = dt.addMonths(sensorPeriod->val());
  calibrateNext->setVal(dt.toString(Qt::ISODate));

  calibrateOperator = new CYString(metroDB, QString("CAL_OPERATOR_%1").arg(objectName()), new QString, tr("Operator"));
  metroList.append(calibrateOperator);

  calibrateRefEquip = new CYString(metroDB, QString("CAL_REF_EQUIP_%1").arg(objectName()), new QString, tr("Equipment reference"));
  metroList.append(calibrateRefEquip);
  calibrateRefEquip->setNbChar(100);

  calibrateProcedure = new CYString(metroDB, QString("CAL_PROCEDURE_%1").arg(objectName()), new QString(tr("Designer values")), tr("Procedure"));
  metroList.append(calibrateProcedure);
  calibrateProcedure->setNbChar(100);

  calibrateComment= new CYString(metroDB, QString("CAL_COMMENT_%1").arg(objectName()), new QString, tr("Comment"));
  metroList.append(calibrateComment);

  metroPV= new CYString(metroDB, QString("METRO_PV_%1").arg(objectName()), new QString, tr("Report file"));

  setCalibrateNext();
}

void CYMeasure::setDesignerMetro()
{
  mMetroDesigner=true;
  QListIterator<CYData *> it(metroList);
  while (it.hasNext())
  {
    CYData *data = it.next();
    data->designer();
  }

  calibrateOperator->setVal(core->user()->title());
}

void CYMeasure::setCalibrateChanged()
{
  if (!calibrateChanged)
    return;
  calibrateChanged->setVal(true);
  calibrateOutdated->setVal(false);
  mMemCalLast = (QDateTime)(*calibrateLast);
  calibrateLast->setVal(QDateTime::currentDateTime().toString(Qt::ISODate));
  setCalibrateNext();
  emit calibratingChanged();
}

bool CYMeasure::forcedDesignerMetro()
{
  return mMetroDesigner;
}

void CYMeasure::setCalibrateControlled()
{
  if (!calibrateChanged)
    return;
  calibrateChanged->setVal(true);
  calibrateOutdated->setVal(false);
  mMemCalLastCtrl= (QDateTime)(*calibrateLastCtrl);
  calibrateLastCtrl->setVal(QDateTime::currentDateTime().toString(Qt::ISODate));
  setCalibrateNext();
  emit calibratingChanged();
}

void CYMeasure::setCalibrateNext()
{
  if (!mMetroEnable)
    return;
  QDate date;
  if ((*calibrateLastCtrl) > (*calibrateLast))
    date = calibrateLastCtrl->date();
  else
    date = calibrateLast->date();

  date = date.addMonths(sensorPeriod->val());
  calibrateNext->setVal(date.toString(Qt::ISODate));
  calibrateNext->save();
}

void CYMeasure::ctrlCalibrateDate()
{
  if (!core->started() || !db()->linksOn())
    return;

  if (!calibrateOutdated || !calibrateDateCtrl)
    return;
  if (calibrateDateCtrl->val())
  {
    if (QDate::currentDate() > calibrateNext->date())
      calibrateOutdated->setVal(true);
    else
      calibrateOutdated->setVal(false);
  }
  else
    calibrateOutdated->setVal(false);
}


void CYMeasure::setCalibrateChangedEvent(CYEvent *ev)
{
  calibrateChanged = ev;
  calibrateChanged->setReportUp(tr("Event"));
  core->eventsGeneratorSPV->addToEventsList(calibrateChanged);
  calibrateChanged->setLocal(true);
}


void CYMeasure::setCalibrateOutdatedEvent(CYEvent *ev)
{
  calibrateOutdated = ev;
  calibrateOutdated->setReportUp(tr("Alert"));
  core->eventsGeneratorSPV->addToEventsList(calibrateOutdated);
  calibrateOutdated->setLocal(true);
  QTimer *timer = new QTimer;
  timer->start(5000);
  connect(timer, SIGNAL(timeout()), SLOT(ctrlCalibrateDate()));
}

void CYMeasure::metroStartMoy(f32 tmoy)
{
  metroTMoy->setVal(tmoy);
  mMetroIMes->setVal(mMetroIndex);
  QString txt = "0.";
  for (int i=1; i<format()->nbDec()+1; i++)
    txt.append("0");
  txt.append("1");
  mMetroMoyPrec->setVal(txt.toFloat());
  mMetroCMoy->setOn();
}

bool CYMeasure::metroMoyOk()
{
  return mMetroMoyOk->val();
}

void CYMeasure::setMetroEditing(bool val)
{
  mMetroEditing=val;
  if (!val)
    mMetroDesigner=false;
}

QString CYMeasure::channelTypeDesc()
{
  return tr("Numeric");
}

float CYMeasure::uncertainty(float val, bool unit)
{
  if (unit)
  {
    switch (sensorUT->val())
    {
      case UEM : return fabs((sensorUEM->val()*(mTop->val()-mLow->val()))/100.0);
      case UUC : return fabs(sensorUUC->val());
      case UPC : return fabs((sensorUPC->val()*val)/100.0);
      default : CYFATAL;
    }
  }
  else
  {
    switch (sensorUT->val())
    {
      case UEM : return fabs(sensorUEM->val());
      case UUC : return fabs(sensorUUC->val()*100.0)/(mTop->val()-mLow->val());
      case UPC : return fabs(sensorUPC->val()*val/(mTop->val()-mLow->val()));
      default : CYFATAL;
    }
  }
}

float CYMeasure::uncertaintyMin(float val)
{
  float res;
  res = val - uncertainty(val);
  if (((min()<0.0) && (res>0.0)) || ((min()>0.0) && (res<0.0)))
    res = 0.0;
  return res;
}

float CYMeasure::uncertaintyMax(float val)
{
  float res;
  res = val + uncertainty(val);
  if (((max()<0.0) && (res>0.0)) || ((max()>0.0) && (res<0.0)))
    res = 0.0;
  return res;
}

void CYMeasure::calcul_gain_offset(float &gain, float &offset)
{
  gain=1.0;
  offset=0.0;
}

void CYMeasure::setMoyMetro(CYTSec *tmoy, CYMeasure *mes, CYMeasure *dir)
{
  metroMoyTime = tmoy;
  metroMoyMES = mes;
  metroMoyDIR = dir;
}

void CYMeasure::printSensorSheet(QPainter *p,QRectF *r)
{
  QRectF rect;
  QRectF rSheet = QRectF(r->left(), r->top(), r->width(), 0);
  QFont old_font = p->font();
  QFont font = p->font();
  QPen pen = p->pen();
  QColor color = pen.color();

//   QFont font("Sans", 8);
//   p->setFont(font);
  int hline = p->fontMetrics().height()+1;

  font.setBold(true);
  font.setItalic(true);
  font.setUnderline(true);
  p->setFont(font);

  p->drawText(rSheet.left(), rSheet.top(), tr("Sensor sheet"));
  rSheet.setBottom(rSheet.bottom()+hline);

  p->setFont(old_font);

  int loffset=20;
  int voffset=350;

  if (!sensorCalAuto->val())
  {
    pen.setColor(Qt::red);
    p->setPen(pen);
    p->drawText(rSheet.left()+voffset, rSheet.bottom(), sensorCalAuto->choiceLabel());
    pen.setColor(color);
    p->setPen(pen);
  }
  else
    p->drawText(rSheet.left()+voffset, rSheet.bottom(), sensorCalAuto->choiceLabel());
  rSheet.setBottom(rSheet.bottom()+hline);


  p->drawText(rSheet.left()+loffset, rSheet.bottom(), tr("Type"));
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), channelTypeDesc());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), tr("High value (FS)"));
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), mTop->toString());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), tr("Low value (FS)"));
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), mLow->toString());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), sensorDesigner->label());
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), sensorDesigner->val());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), sensorPN->label());
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), sensorPN->val());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), sensorSN->label());
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), sensorSN->val());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset     , rSheet.bottom(), sensorUT->choiceLabel(sensorUT->val()));
  switch (sensorUT->val())
  {
    case UEM : p->drawText(rSheet.left()+voffset , rSheet.bottom(), sensorUEM->toString()); break;
    case UUC : p->drawText(rSheet.left()+voffset , rSheet.bottom(), sensorUUC->toString()); break;
    case UPC : p->drawText(rSheet.left()+voffset , rSheet.bottom(), sensorUPC->toString()) ; break;
    default : CYFATAL;
  }
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), sensorEnv->label());
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), sensorEnv->val());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), sensorPeriod->label());
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), sensorPeriod->toString());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), calibrateDateCtrl->label());
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), calibrateDateCtrl->choiceLabel(calibrateDateCtrl->val()));
  rSheet.setBottom(rSheet.bottom()+hline);

  QHashIterator<int, CYData*> it( sensorSheetExtra );
  while (it.hasNext())
  {
    it.next();
    CYData *data=it.value();
    p->drawText(rSheet.left()+loffset, rSheet.bottom(), data->label());
    switch (data->type())
    {
      case Cy::VFL    :
      case Cy::VS8    :
      case Cy::VS16   :
      case Cy::VS32   :
      case Cy::VS64   :
      case Cy::VU8    :
      case Cy::VU16   :
      case Cy::VU32   :
      case Cy::VU64   :
      case Cy::Word   :
      case Cy::Bool   :
                      {
                        if (data->choiceDict.count()>0)
                        {
                          p->drawText(rSheet.left()+voffset, rSheet.bottom(), data->choiceLabel());
                        }
                        else if (data->isFlag())
                        {
                          p->drawText(rSheet.left()+voffset, rSheet.bottom(), data->choiceLabel());
                        }
                        else
                        {
                          p->drawText(rSheet.left()+voffset, rSheet.bottom(), data->toString());
                        }
                        break;
                      }

      case Cy::VF32   :
      case Cy::VF64   :
      case Cy::Time   :
      case Cy::Sec    :
                      {
                        p->drawText(rSheet.left()+voffset, rSheet.bottom(), data->toString());
                        break;
                      }
      case Cy::String :
                      {
                        p->drawText(rSheet.left()+voffset, rSheet.bottom(), data->toString());
                        break;
                      }
      default         : CYWARNINGTEXT(QString("Data type %1").arg(data->objectName()));
    }

    rSheet.setBottom(rSheet.bottom()+hline);
  }

  p->setFont(old_font);

  r->setTop(rSheet.bottom());
}

void CYMeasure::printInfoCal(QPainter *p,QRectF *r)
{
  QRectF rect;
  QRectF rSheet = QRectF(r->left(), r->top(), r->width(), 0);
  QFont old_font = p->font();
  QFont font = p->font();
  QPen old_pen = p->pen();
  QPen pen = p->pen();
  int hline = p->fontMetrics().height()+1;

  font.setBold(true);
  font.setItalic(true);
  font.setPointSize(13);
//   font.setUnderline(true);
  p->setFont(font);

  int loffset=20;
  int voffset=350;
  QString txt;
  if ((*calibrateLastCtrl) > (*calibrateLast)) // VERIFICATION
  {
    if (calibrateOk->val())
      txt = tr("Verification in accordance");
    else
    {
      txt = tr("Verification not in accordance");
      pen.setColor(Qt::red);
      p->setPen(pen);
    }
    p->drawText(QRectF(rSheet.left(), rSheet.top(), rSheet.width(), 0), Qt::TextWordWrap, txt, &rect);
    p->drawText(rSheet.left()+(rSheet.width()-rect.width())/2, rSheet.top(), txt);
    rSheet.setBottom(rSheet.bottom()+hline*2);
    p->setPen(old_pen);
    p->setFont(old_font);

    font = p->font();
    font.setBold(true);
    p->setFont(font);
    txt = (*calibrateLastCtrl).QDateTime::toString(Qt::ISODate);
    p->drawText(QRectF(rSheet.left(), rSheet.bottom(), rSheet.width(), 0), Qt::TextWordWrap, txt, &rect);
    p->drawText(rSheet.left()+(rSheet.width()-rect.width())/2, rSheet.bottom(), txt);
    rSheet.setBottom(rSheet.bottom()+hline*2);

    font.setItalic(true);
    font.setUnderline(true);
    p->setFont(font);
    p->drawText(rSheet.left(), rSheet.bottom(), tr("Summary"));
    rSheet.setBottom(rSheet.bottom()+hline*2);
    p->setFont(old_font);

    if (!calibrateLast->toString().startsWith("2000-01-01"))
    {
      p->drawText(rSheet.left()+loffset, rSheet.bottom(), tr("Last calibration")+": ");
      p->drawText(rSheet.left()+voffset, rSheet.bottom(), (*calibrateLast).QDateTime::toString(Qt::ISODate));
      rSheet.setBottom(rSheet.bottom()+hline);
    }
    if (!mMemCalLastCtrl.toString(Qt::ISODate).startsWith("2000-01-01"))
    {
      p->drawText(rSheet.left()+loffset, rSheet.bottom(), tr("Last verification")+": ");
      p->drawText(rSheet.left()+voffset, rSheet.bottom(), mMemCalLastCtrl.QDateTime::toString(Qt::ISODate));
      rSheet.setBottom(rSheet.bottom()+hline);
    }
    if (!calibrateNext->toString().startsWith("2000-01-01"))
    {
      p->drawText(rSheet.left()+loffset, rSheet.bottom(), tr("Next verification")+": ");
      p->drawText(rSheet.left()+voffset, rSheet.bottom(), (*calibrateNext).date().toString(Qt::ISODate));
      rSheet.setBottom(rSheet.bottom()+hline);
    }
  }
  else // ÉTALONNAGE
  {
    txt = tr("Calibrating");
    p->drawText(QRectF(rSheet.left(), rSheet.top(), rSheet.width(), 0), Qt::TextWordWrap, txt, &rect);
    p->drawText(rSheet.left()+(rSheet.width()-rect.width())/2, rSheet.top(), txt);
    rSheet.setBottom(rSheet.bottom()+hline*2);
    p->setPen(old_pen);
    p->setFont(old_font);

    font = p->font();
    font.setBold(true);
    p->setFont(font);
    txt = (*calibrateLast).QDateTime::toString(Qt::ISODate);
    p->drawText(QRectF(rSheet.left(), rSheet.bottom(), rSheet.width(), 0), Qt::TextWordWrap, txt, &rect);
    p->drawText(rSheet.left()+(rSheet.width()-rect.width())/2, rSheet.bottom(), txt);
    rSheet.setBottom(rSheet.bottom()+hline*2);

    font.setItalic(true);
    font.setUnderline(true);
    p->setFont(font);
    p->drawText(rSheet.left(), rSheet.bottom(), tr("Summary"));
    rSheet.setBottom(rSheet.bottom()+hline*2);
    p->setFont(old_font);

    if (!mMemCalLast.toString(Qt::ISODate).startsWith("2000-01-01"))
    {
      p->drawText(rSheet.left()+loffset, rSheet.bottom(), tr("Last calibration")+": ");
      p->drawText(rSheet.left()+voffset, rSheet.bottom(), mMemCalLast.QDateTime::toString(Qt::ISODate));
      rSheet.setBottom(rSheet.bottom()+hline);
    }
    if (!calibrateLastCtrl->toString().startsWith("2000-01-01"))
    {
      p->drawText(rSheet.left()+loffset, rSheet.bottom(), tr("Last verification")+": ");
      p->drawText(rSheet.left()+voffset, rSheet.bottom(), (*calibrateLastCtrl).QDateTime::toString(Qt::ISODate));
      rSheet.setBottom(rSheet.bottom()+hline);
    }
    if (!calibrateNext->toString().startsWith("2000-01-01"))
    {
      p->drawText(rSheet.left()+loffset, rSheet.bottom(), tr("Next verification")+": ");
      p->drawText(rSheet.left()+voffset, rSheet.bottom(), (*calibrateNext).date().toString(Qt::ISODate));
      rSheet.setBottom(rSheet.bottom()+hline);
    }
  }

  rSheet.setBottom(rSheet.bottom()+hline*2);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), calibrateOperator->label());
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), calibrateOperator->val());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), calibrateProcedure->label());
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), calibrateProcedure->val());
  rSheet.setBottom(rSheet.bottom()+hline);

  p->drawText(rSheet.left()+loffset, rSheet.bottom(), calibrateRefEquip->label());
  p->drawText(rSheet.left()+voffset, rSheet.bottom(), calibrateRefEquip->val());
  rSheet.setBottom(rSheet.bottom()+hline);

  rSheet.setBottom(rSheet.bottom()+hline);

  p->setFont(font);
  p->drawText(rSheet.left(), rSheet.bottom(), calibrateComment->label());
  rSheet.setBottom(rSheet.bottom()+hline);
  p->setFont(old_font);

  p->drawText(QRectF(rSheet.left()+loffset, rSheet.bottom(), rSheet.width()-voffset,             0), Qt::TextWordWrap, calibrateComment->toString(), &rect);
  p->drawText(rSheet.left()+loffset, rSheet.bottom(), rSheet.width()-voffset, rect.height(), Qt::TextWordWrap, calibrateComment->toString());
  rSheet.setBottom(rSheet.bottom()+rect.height());

  p->setFont(old_font);

  r->setTop(rSheet.bottom());
}

QString CYMeasure::metroSummary(bool editing)
{
  QString txt = (editing) ? tr("Editing") : tr("Viewing");
  txt.prepend("<b>");
  txt.append("</b>");

  if (notCal())
    txt.append("<br>"+QString("<font color=\"red\">%1</font>").arg(tr("No calibration")));
  else if (designerCal())
    txt.append("<br>"+QString("<font color=\"magenta\">%1</font>").arg(tr("Designer values")));
  else
    txt.append("<br>"+tr("Last calibration" )+": "+ (*calibrateLast).QDateTime::toString(Qt::ISODate));

  if ((*calibrateLastCtrl) > (*calibrateLast))
  {
    if (calibrateOk->val())
    {
      txt.append("<br>"+tr("Verification in accordance")+": "+ (*calibrateLastCtrl).QDateTime::toString(Qt::ISODate));
    }
    else
    {
      txt.append("<br>"+QString("<font color=\"red\">%1</font>").arg(tr("Verification not in accordance"))+": "+ (*calibrateLastCtrl).QDateTime::toString(Qt::ISODate));
    }
  }

  if (!calibrateNext->toString().startsWith("2000-01-01"))
  {
    txt.append("<br>"+tr("Next verification")+": "+ (*calibrateNext).date().toString(Qt::ISODate));
  }
  return txt;
}

/*! @return \a true s'il s'agit du calibrage constructeur
    \fn CYMeasure::designerCal()
 */
bool CYMeasure::designerCal()
{
  if (!calibrateLast)
    return true;
  return (calibrateLast->toString().startsWith(mInitCalLast)) ? true : false;
}


/*! @return \a true si le capteur n'a jamais été calibré ou étalonné
    \fn CYMeasure::notCal()
 */
bool CYMeasure::notCal()
{
  if (!calibrateLast)
    return true;
  return (calibrateLast->toString().startsWith("2000-01-01")) ? true : false;
}


/*! Initialise le paramètrage de la mesure.
    @param enable     pointe sur le flag d'activation de la voie.
    @param top        pointe sur le point haute de la plage de mesure.
    @param top        pointe sur le point bas de la plage de mesure.
    @param cal        Type de calibrage paramétrable.
    @param ct         Active/désactive l'édition de la surveillance
    @param def_enable activation par défaut.
    \fn CYMeasure::initSetting(CYDB *db, flg *enable, f32 *top, f32 *low, s16 *max_adc, s16 *min_adc, bool cal, bool ct, bool def_enable)
 */
void CYMeasure::initSetting(CYDB *db, flg *enable, f32 *top, f32 *low, bool cal, bool ct, bool def_enable)
{
  mSetting = new CYMeasureSetting(db, this, cal, ct);
  mSetting->init(enable, top, low, def_enable);
}

/*! Initialise le paramètrage de la mesure.
    @param enable     pointe sur le flag d'activation de la voie.
    @param top        pointe sur le point haute de la plage de mesure.
    @param top        pointe sur le point bas de la plage de mesure.
    @param max_adc    max en points au-dessus duquel la mesure est grisée.
    @param min_adc    min en points en-dessus duquel la mesure est grisée (rupture)
    @param cal        Type de calibrage paramétrable.
    @param ct         Active/désactive l'édition de la surveillance
    @param def_enable activation par défaut.
    \fn CYMeasure::initSetting(CYDB *db, flg *enable, f32 *top, f32 *low, s16 *max_adc, s16 *min_adc, bool cal, bool ct, bool def_enable)
 */
void CYMeasure::initSetting(CYDB *db, flg *enable, f32 *top, f32 *low, s16 *max_adc, s16 *min_adc, bool cal, bool ct, bool def_enable)
{
  initSetting(db, enable, top, low, cal, ct, def_enable);

  mMaxAdc16 = new CYS16( mDB, QString("MAX_ADC_%1").arg(objectName()), max_adc);
  mMaxAdc16->setLabel(tr("Maximum number of ADC points"));
  mMaxAdc16->setAutoMinMaxFormat( false );
  mMaxAdc16->setVolatile(true);

  mMinAdc16 = new CYS16( mDB, QString("MIN_ADC_%1").arg(objectName()), min_adc);
  mMinAdc16->setLabel(tr("Minimum number of ADC points"));
  mMinAdc16->setAutoMinMaxFormat( false );
  mMinAdc16->setVolatile(true);
}

/*! Initialise le paramètrage de la mesure.
    @param enable     pointe sur le flag d'activation de la voie.
    @param top        pointe sur le point haute de la plage de mesure.
    @param top        pointe sur le point bas de la plage de mesure.
    @param max_adc    max en points au-dessus duquel la mesure est grisée.
    @param min_adc    min en points en-dessus duquel la mesure est grisée (rupture)
    @param cal        Type de calibrage paramétrable.
    @param ct         Active/désactive l'édition de la surveillance
    @param def_enable activation par défaut.
    \fn CYMeasure::initSetting(CYDB *db, flg *enable, f32 *top, f32 *low, s32 *max_adc, s32 *min_adc, bool cal, bool ct, bool def_enable)
 */
void CYMeasure::initSetting(CYDB *db, flg *enable, f32 *top, f32 *low, s32 *max_adc, s32 *min_adc, bool cal, bool ct, bool def_enable)
{
  initSetting(db, enable, top, low, cal, ct, def_enable);

  mMaxAdc32 = new CYS32( mDB, QString("MAX_ADC_%1").arg(objectName()), max_adc);
  mMaxAdc32->setLabel(tr("Maximum number of ADC points"));
  mMaxAdc32->setAutoMinMaxFormat( false );
  mMaxAdc32->setVolatile(true);

  mMinAdc32 = new CYS32( mDB, QString("MIN_ADC_%1").arg(objectName()), min_adc);
  mMinAdc32->setLabel(tr("Minimum number of ADC points"));
  mMinAdc32->setAutoMinMaxFormat( false );
  mMinAdc32->setVolatile(true);
}

bool CYMeasure::isEnabled()
{
  if (mSetting)
    return mSetting->isEnabled();
  return true;
}

void CYMeasure::updateFormat()
{
  if (mSetting)
    mSetting->updateFormat();
}

void CYMeasure::updateSensor()
{
  if (mSetting)
    mSetting->updateSensor();
}

/*! @return \a true si l'édition du format est active
    \fn CYMeasure::editFormat()
 */
bool CYMeasure::editFormat()
{
  if (mSetting)
   mSetting->editFormat();
  return false;
}


/*! @return \a true si l'édition de la surveillance est active
    \fn CYMeasure::editSupervision()
 */
bool CYMeasure::editSupervision()
{
  if (mSetting)
   mSetting->editSupervision();
  return false;
}

void CYMeasure::sendCal()
{
  calDB->writeData();
  calDB->save();
}
