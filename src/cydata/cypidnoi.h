/***************************************************************************
                          cypidnoi.h  -  description
                             -------------------
    début                  : lun avr 28 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYPIDNOI_H
#define CYPIDNOI_H

// CYLIBS
#include "cyflag.h"

/** CYPIDNoI représente le flag de consigne en rampe d'un PID.
  * S'il vaut 1 alors consigne en rampe.
  * @short Flag de consigne en rampe de PID.
  * @author Gérald LE CLEACH
  */

class CYPIDNoI : public CYFlag
{
  Q_OBJECT
public:
  /** Création d'un flag de consigne en rampe de PID.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param def   Valeur par défaut. */
  CYPIDNoI(CYDB *db, const QString &name, flg *val, const flg def=0);
  
  ~CYPIDNoI();
};

#endif
