/***************************************************************************
                          cypidi.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cypidi.h"

CYPIDI::CYPIDI(CYDB *db, const QString &name, f32 *val, const f32 def, const f32 min, const f32 max, int format)
  : CYF32(db, name, val, def, min, max, format)
{
  mLabel = tr("Integral coefficient");
  addNote(tr("If 0, the integral control won't be in operation."));
  addNote(tr("This control is a mixt P.I.D."));
}

CYPIDI::~CYPIDI()
{
}
