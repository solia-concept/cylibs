/***************************************************************************
                          cymv.cpp  -  description
                             -------------------
    begin                : jeu déc 30 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cymv.h"

// CYLIBS
#include "cycore.h"
#include "cytsec.h"
#include "cyflag.h"
#include "cys16.h"

CYMV::CYMV(CYDB *db, const QString &name, int idNet, t_mv *values, f32 taa_def, f32 tar_def, f32 tpa_def, f32 tpr_def, flg dca_def, flg dcr_def, QString group, QString forwardPhys, QString backwardPhys, QString forwardDesc, QString backwardDesc, QString settings)
  : CYDatasGroup(db, name, idNet, group, settings)
{
  mForwardPhys  = forwardPhys;
  mBackwardPhys = backwardPhys;
  mForwardDesc  = forwardDesc;
  mBackwardDesc = backwardDesc;

  mTAA = new CYTSec(db, QString("%1_TAA").arg(name), &values->taa, taa_def, 0.2, 20.0);
  mTAR = new CYTSec(db, QString("%1_TAR").arg(name), &values->tar, tar_def, 0.2, 20.0);
  mTPA = new CYTSec(db, QString("%1_TPA").arg(name), &values->tpa, tpa_def, 0.1, 10.0);
  mTPR = new CYTSec(db, QString("%1_TPR").arg(name), &values->tpr, tpr_def, 0.1, 10.0);

  mDCA = new CYFlag(db, QString("%1_DCA").arg(name), &values->dca, dca_def);
  mDCR = new CYFlag(db, QString("%1_DCR").arg(name), &values->dcr, dcr_def);

  init();
}

CYMV::CYMV(CYDB *db, const QString &name, t_mv *values, Attr1 *attr)
  : CYDatasGroup(db, name, attr->idNet, attr->group, attr->settings)
{
  mForwardPhys  = attr->forwardPhys;
  mBackwardPhys = attr->backwardPhys;
  mForwardDesc  = (!attr->forwardDesc.isEmpty()) ? attr->forwardDesc : tr("Forward");
  mBackwardDesc = (!attr->backwardDesc.isEmpty()) ? attr->backwardDesc : tr("Backward");

  mTAA = new CYTSec(db, QString("%1_TAA").arg(name), &values->taa, attr->taa_def, 0.2, 20.0);
  mTAR = new CYTSec(db, QString("%1_TAR").arg(name), &values->tar, attr->tar_def, 0.2, 20.0);
  mTPA = new CYTSec(db, QString("%1_TPA").arg(name), &values->tpa, attr->tpa_def, 0.1, 10.0);
  mTPR = new CYTSec(db, QString("%1_TPR").arg(name), &values->tpr, attr->tpr_def, 0.1, 10.0);

  mDCA = new CYFlag(db, QString("%1_DCA").arg(name), &values->dca, attr->dca_def);
  mDCR = new CYFlag(db, QString("%1_DCR").arg(name), &values->dcr, attr->dcr_def);

  init();
}

CYMV::CYMV(CYDB *db, const QString &name, t_mv *values, Attr2 *attr)
  : CYDatasGroup(db, name, attr->idNet, attr->group, attr->settings)
{
  mForwardPhys  = attr->forwardPhys;
  mBackwardPhys = attr->backwardPhys;
  mForwardDesc  = (!attr->forwardDesc.isEmpty()) ? attr->forwardDesc : tr("Forward");
  mBackwardDesc = (!attr->backwardDesc.isEmpty()) ? attr->backwardDesc : tr("Backward");

  mTAA = new CYTSec(db, QString("%1_TAA").arg(name), &values->taa, attr->taa_def, attr->taa_min, attr->taa_max);
  mTAR = new CYTSec(db, QString("%1_TAR").arg(name), &values->tar, attr->tar_def, attr->tar_min, attr->tar_max);
  mTPA = new CYTSec(db, QString("%1_TPA").arg(name), &values->tpa, attr->tpa_def, attr->tpa_min, attr->tpa_max);
  mTPR = new CYTSec(db, QString("%1_TPR").arg(name), &values->tpr, attr->tpr_def, attr->tpr_min, attr->tpr_max);

  mDCA = new CYFlag(db, QString("%1_DCA").arg(name), &values->dca, attr->dca_def);
  mDCR = new CYFlag(db, QString("%1_DCR").arg(name), &values->dcr, attr->dcr_def);

  init();
}

CYMV::~CYMV()
{
}

void CYMV::initStates(CYDB *db, flg *init_ok, flg *forward, s16 *grafcet)
{
  mInitOk = new CYFlag(db, QString("%1_INIT_OK").arg(objectName()), init_ok);
  mForward = new CYFlag(db, QString("%1_FORWARD").arg(objectName()), forward);
  mGrafcet = new CYS16(db, QString("%1_GRAFCET").arg(objectName()), grafcet);

  initStates();
}

void CYMV::initStates(CYDB *db, AttrStates1 *attr)
{
  mInitOk = new CYFlag(db, QString("%1_INIT_OK").arg(objectName()), attr->init_ok);
  mForward = new CYFlag(db, QString("%1_FORWARD").arg(objectName()), attr->forward);
  mGrafcet = new CYS16(db, QString("%1_GRAFCET").arg(objectName()), attr->grafcet);

  initStates();
}

void CYMV::init()
{
  if (CYMV *mv = core->mv(objectName(), false))
    CYFATALTEXT(QString("STR1: %1 STR2: %2 => NOM DE MOUVEMENT '%3' EXISTE DEJA DANS LA BASE DES MOUVEMENTS").arg(mv->db()->objectName()).arg(mDB->objectName()).arg(objectName()))
  core->addMV(objectName(), this);

  mTPR->setLabel(tr("Sensor loss timer %1").arg(mBackwardPhys));
  mTPR->setHelp(tr("Maximum delay before the falling edge of the backward sensor.")
           +" "+tr("If this time is reached, a failure is signalled."));
  mTPR->setGroup(mGroup+":"+mForwardDesc);

  mTAA->setLabel(tr("Sensor appearence timer %1").arg(mForwardPhys));
  mTAA->setHelp(tr("Maximum delay before the rising edge of the forward sensor.")
           +" "+tr("If this time is reached, a failure is signalled."));
  mTAA->setGroup(mGroup+":"+mForwardDesc);

  mTPA->setLabel(tr("Sensor loss timer %1").arg(mForwardPhys));
  mTPA->setHelp(tr("Maximum delay before the falling edge of the forward sensor.")
           +" "+tr("If this time is reached, a failure is signalled."));
  mTPA->setGroup(mGroup+":"+mBackwardDesc);

  mTAR->setLabel(tr("Sensor appearence timer %1").arg(mBackwardPhys));
  mTAR->setHelp(tr("Maximum delay before the rising edge of the backward sensor.")
           +" "+tr("If this time is reached, a failure is signalled."));
  mTAR->setGroup(mGroup+":"+mBackwardDesc);

  mDCA->setLabel(tr("Sensor failure signalisation %1").arg(mForwardPhys));
    mDCA->setChoiceTrue(tr("Fault"));
  mDCA->setHelpTrue(tr("A faillure generates a fault."));
  mDCA->setChoiceFalse(tr("Alert"));
  mDCA->setHelpFalse(tr("A faillure generates an alert and then the timers simulate the sensors."));
  mDCA->setGroup(mGroup);

  mDCR->setLabel(tr("Sensor failure signalisation %1").arg(mBackwardPhys));
  mDCR->setChoiceTrue(tr("Fault"));
  mDCR->setHelpTrue(tr("A faillure generates a fault."));
  mDCR->setChoiceFalse(tr("Alert"));
  mDCR->setHelpFalse(tr("A faillure generates an alert and then the timers simulate the sensors."));
  mDCR->setGroup(mGroup);
}

void CYMV::initStates()
{
  mInitOk->setLabel(tr("Initialized movement"));
  mInitOk->setGroup(mGroup);

  mForward->setLabel(tr("Positioning"));
  mForward->setGroup(mGroup);

  mGrafcet->setLabel(tr("Grafcet current state"));
  mGrafcet->setGroup(mGroup);
}

CYEvent *CYMV::initFaultForwardRising( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", forwardPhys());
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+forwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initFaultForwardFalling( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", forwardPhys());
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+backwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initFaultForwardStatic( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", forwardPhys());
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+forwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initFaultBackwardRising( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", backwardPhys());
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+backwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initFaultBackwardFalling( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", backwardPhys());
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+forwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initFaultBackwardStatic( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", backwardPhys());
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+backwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initFaultCoherence( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", forwardPhys());
  desc = desc.replace("%2", backwardPhys());
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":\n");
  return event;
}

CYEvent *CYMV::initAlertForwardRising( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", forwardPhys());
  CYEvent *event = initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+forwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initAlertForwardFalling( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", forwardPhys());
  CYEvent *event = initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+backwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initAlertForwardStatic( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", forwardPhys());
  CYEvent *event = initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+forwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initAlertBackwardRising( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", backwardPhys());
  CYEvent *event = initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+backwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initAlertBackwardFalling( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", backwardPhys());
  CYEvent *event = initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+forwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initAlertBackwardStatic( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", backwardPhys());
  CYEvent *event = initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":"+group()+":"+backwardDesc()+":\n");
  return event;
}

CYEvent *CYMV::initAlertCoherence( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  desc = desc.replace("%1", forwardPhys());
  desc = desc.replace("%2", backwardPhys());
  CYEvent *event = initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(group()+":\n");
  return event;
}
