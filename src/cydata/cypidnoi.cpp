/***************************************************************************
                          cypidnoi.cpp  -  description
                             -------------------
    début                  : lun avr 28 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cypidnoi.h"

CYPIDNoI::CYPIDNoI(CYDB *db, const QString &name, flg *val, const flg def)
  : CYFlag(db, name, val, def)
{
  mLabel   = tr("Integral frozen in ramp");
  setHelp("",
          tr("The integral value will frozen in ramp."),
          tr("This may be useful to avoid overshoot at the ramp end."
               "The integral value will be modified normally by the PID control."));
}

CYPIDNoI::~CYPIDNoI()
{
}
