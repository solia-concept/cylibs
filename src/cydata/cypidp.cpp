/***************************************************************************
                          cypidp.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cypidp.h"

CYPIDP::CYPIDP(CYDB *db, const QString &name, f32 *val, const f32 def, const f32 min, const f32 max)
  : CYF32(db, name, val, def, min, max)
{
  mLabel = tr("Proportional coefficient");
  addNote(tr("If 0, the proportional control won't be active (It can work in integral mode)"));
  addNote(tr("This control is a mixt P.I.D."));
}

CYPIDP::~CYPIDP()
{
}
