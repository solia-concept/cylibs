//
// C++ Implementation: cymeasuresetting
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
// CYLIBS
#include "cymeasuresetting.h"
#include "cystring.h"
#include "cyflag.h"
#include "cyf32.h"
#include "cycore.h"
#include "cyana.h"
#include "cys16.h"

CYMeasureSetting::CYMeasureSetting(CYDB *db, CYMeasure *mes, bool cal, bool ct)
  : CYDatasGroup(db, QString("%1").arg(mes->objectName()), -1, tr("Measurement setting")),
    mMes(mes)
{
  mEnable=0;
  db->setUnderGroup(mes->label());

  if (cal)
  {
    QString oldDataName = QString("CAL_%1").arg(mMes->objectName());
    QString newDataName = QString("SIGNAL_TYPE_%1").arg(mMes->objectName());
    mDB->changeDataName(oldDataName, newDataName);
    mCal = new CYS16( mDB, newDataName, new s16 );
    mCal->setLabel(tr("Signal type"));

    flg *val_caled = new flg;
    (*val_caled) = false;

    QString from = group();
    QString helpEnd;
    flg *val = new flg;
    (*val) = false;
    QString help = tr("Indicates that the type or scale of the sensor have been changed. So, it's recommended to make a new calibrate of sensor.");
    mSensorChanged = mMes->CYData::initAlert(core->eventsGeneratorSPV, QString("%1_CHANGED").arg(objectName()), val, helpEnd, SI_1, REC|SCR|BST, tr("%1: Sensor changed").arg(mMes->phys()), from, 0, help);
    mSensorChanged->setReportUp(tr("Alert"));
    mSensorChanged->setSuffix("!");
    core->eventsGeneratorSPV->addToEventsList(mSensorChanged);
    mSensorChanged->setLocal(true);
  }
  else
    mCal = 0;

  setEditSupervision(ct);
  db->setUnderGroup(mes->label());
  connect(this, SIGNAL(formatUpdated()), mMes, SIGNAL(formatUpdated()));
  init();
}

CYMeasureSetting::~CYMeasureSetting()
{
}


/*!
    \fn CYMeasureSetting::init()
 */
void CYMeasureSetting::init()
{
  mEditFormat      = true;
  mEditCalType     = false;
  mEditSupervision = false;
}


/*! Initialise le paramétrage de la mesure paramétrable.
    @param enable     pointe sur le flag d'activation de la voie.
    @param top        pointe sur le point haute de la plage de mesure.
    @param top        pointe sur le point bas de la plage de mesure.
    @param def_enable activation par défaut
    \fn CYMeasureSetting::init(flg *enable, f32 *top, f32 *low)
 */
void CYMeasureSetting::init(flg *enable, f32 *top, f32 *low, bool def_enable)
{
  mDB->setUnderGroup( mMes->label() );

  mUnit = new CYString( mDB, QString("UNIT_%1").arg(mMes->objectName()), new QString( mMes->unit() ), tr("Unit"));

  mLabel = new CYString( mDB, QString("LABEL_%1").arg(mMes->objectName()), new QString( mMes->label() ), tr("Label"));

  mEnable = new CYFlag( mDB, QString("ENABLE_%1").arg(mMes->objectName()), enable, def_enable);
  mEnable->setLabel( tr("Enable") );

  mDB->setUnderGroup( mMes->label() + ":" + tr("Scaling") );

  mTDef = new CYF32( mDB, QString("TOP_DEF_%1").arg(mMes->objectName()), top, mMes->topDef(), mMes->numFormat());
 QString txt = mTDef->objectName();
  mTDef->setFormat(mMes->format());
  mTDef->setLabel(tr("High value"));
  mTDef->setAutoMinMaxFormat( false );
  mTDef->initMinMax();
  setSameFormatData( mTDef );

  mLDef = new CYF32( mDB, QString("LOW_DEF_%1").arg(mMes->objectName()), low, mMes->lowDef(), mMes->numFormat());
  mLDef->setFormat(mMes->format());
  mLDef->setLabel(tr("Low value"));
  mLDef->setAutoMinMaxFormat( false );
  mLDef->initMinMax();
  setSameFormatData( mLDef );

  if (mMes->top())
    setSameFormatData( mMes->top() );

  if (mMes->top())
    setSameFormatData( mMes->low() );

  mDB->setUnderGroup( 0 );

  connect( this, SIGNAL( formatUpdated() ), core, SIGNAL( DBChanged() ) );
  connect( mDB, SIGNAL( loaded() ), this, SLOT( updateSensor() ) );
  connect( mDB, SIGNAL( loaded() ), this, SLOT( updateFormat() ) );
  connect( mDB, SIGNAL( saved() ), this, SLOT( updateSensor() ) );
  connect( mDB, SIGNAL( saved() ), this, SLOT( updateFormat() ) );
}


/*! Mise à jour du format de la mesure paramétrable.
    \fn CYMeasureSetting::updateFormat()
 */
void CYMeasureSetting::updateFormat()
{
  if ( !mUnit )
  {
    CYFATALTEXT(QString("Le paramétrage de la mesure %1 n'a pas été initialisé (initSetting)").arg(objectName()) );
  }

  if ( ( mMes->unit() == mUnit->val() ) && ( mMes->label() == mLabel->val() ) )
    return;

  mMes->setUnit( mUnit->val() );
  mMes->setLabel( mLabel->val() );

  QListIterator<CYF32 *> it(mSameFormatDatas);
  while (it.hasNext())
  {
    CYF32 *data = it.next();
    data->setUnit( mUnit->val() );
    data->setLabel( mLabel->val() );
  }
  emit formatUpdated();
}


/*! Mise à jour du type de capteur.
    \fn CYMeasureSetting::updateSensor()
 */
void CYMeasureSetting::updateSensor()
{
  if ( !mCal )
    return;

//   switch (mCal->val())
//   {
//     case CYAna::CALU_487  : mMaxAdc->setVal(4052);
//                             mMinAdc->setVal(199);
//                             break;
//     default               : mMaxAdc->setVal(4096);
//                             mMinAdc->setVal(-1);
//   }
  if ( ( mMes->typeCal() == mCal->val() ) && ( mMes->top()->def() == mTDef->val() )&& ( mMes->low()->def() == mLDef->val() ) )
    return;

  mMes->setTypeCal( mCal->val() );
  mMes->top()->setDef(mTDef->val());
  mMes->low()->setDef(mLDef->val());

  mMes->initCaliDef();

  mSensorChanged->setVal(true);
}

/*! Active/désactive l'édition du format
    \fn CYMeasureSetting::setEditFormat( bool val )
 */
void CYMeasureSetting::setEditFormat( bool val )
{
  mEditFormat = val;
}

/*! Active/désactive l'édition de la surveillance
    \fn CYMeasureSetting::setEditSupervision( bool val )
 */
void CYMeasureSetting::setEditSupervision( bool val )
{
  mEditSupervision = val;
}


/*! @return \a true si l'édition du format est active
    \fn CYMeasureSetting::editFormat()
 */
bool CYMeasureSetting::editFormat()
{
  return mEditFormat;
}


/*! @return \a true si l'édition de la surveillance est active
    \fn CYMeasureSetting::editSupervision()
 */
bool CYMeasureSetting::editSupervision()
{
  return mEditSupervision;
}


/*! Saisie le nom d'une la donnée ayant le même format
    \fn CYMeasureSetting::setSameFormatData(QByteArray dataName)
 */
void CYMeasureSetting::setSameFormatData(QByteArray dataName)
{
  setSameFormatData( (CYF32 *)core->findData( dataName ) );
}


/*! Saisie une donnée donnée ayant le même format
    \fn CYMeasureSetting::setSameFormatData( CYF32 *data )
 */
void CYMeasureSetting::setSameFormatData( CYF32 *data )
{
  if (!data)
    CYFATAL
  mSameFormatDatas.append( data );
  connect( this, SIGNAL( formatUpdated()  ), data, SIGNAL( formatUpdated()  ) );
}


/*! @return \a true si la voie paramétrable est active
    \fn CYMeasureSetting::isEnabled()
 */
bool CYMeasureSetting::isEnabled()
{
  if (mEnable)
    return mEnable->val();
  return false;
}

void CYMeasureSetting::addTypeCal(int type, QString label, bool def)
{
  if (!mCal)
    CYFATAL

  mCal->setChoiceLabel(type, label);
  if (def)
    mCal->setDef(type);
}

void CYMeasureSetting::setTypeCal(int type)
{
  mCal->setVal(type);
  if (!mMes->inherits("CYExtMeasure") && mMes->typeCal()!=type)
    mMes->setTypeCal(type);
}

int CYMeasureSetting::typeCal()
{
  if (!mMes->inherits("CYExtMeasure"))
    return mMes->typeCal();
  return mCal->val();
}

void CYMeasureSetting::acquit()
{
  mSensorChanged->setVal(false);
}
