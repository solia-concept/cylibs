//
// C++ Implementation: cydblibs
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cydblibs.h"

// QWT
#include "qwt_plot_curve.h"
// CYLIBS
#include "cycore.h"
#include "cystring.h"
#include "cyflag.h"
#include "cyu32.h"
#include "cys8.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cycolor.h"
#include "cydatetime.h"
#include "cynethost.h"
#include "cynetlink.h"
#include "cysimplecrypt.h"
#include "cyapplication.h"
#include "cyaboutdata.h"

CYDBLibs::CYDBLibs(QObject *parent, const QString &name)
 : CYDB(((core) ? core->localHost() : parent), name, -1, 0, true)
{
  mFileName = QString(core->appSettingDir()+"%1.cydata").arg(name);

  if (core)
  {
    CYNetLink *link = core->localLink();
    if (link)
      link->addDB(this);
  }

  initProject();
  initTest();
  initDevel();
  initBackup();
  initEvents();
  initUsers();
  initAcquisition();
  initMetrology();

  load();
}


CYDBLibs::~CYDBLibs()
{
}

/*! Initialise les données relatives au projet
    \fn CYDBLibs::initProjet()
 */
void CYDBLibs::initProject()
{
  setUnderGroup(tr("Project"));

/*  mString = new CYString(this, "CY_PRJ_PATH");
  mString->setLabel(tr("Path"));*/

  mFlag = new CYFlag(this, "CY_PRJ_SETPOINTS_PROTECTION", new flg);
  mFlag->setLabel(tr("Setpoints protection"));
  mFlag->setHelp(0,
                 tr("Avoid to erase project setpoints. If a project, which has ever been used by the machine, is modified in the project editor then its saving is done in a different project directory.")+" "+tr("The name of this directory is automatically created with the date and hour of the modification."),
                 0);

  setUnderGroup("");
}


/*! Initialise les données relatives à l'essai
    \fn CYDBLibs::initTest()
 */
void CYDBLibs::initTest()
{
  setUnderGroup(tr("Test"));

//   mString = new CYString(this, "CY_TEST_DIR");
//   mString->setLabel(tr("Directory"));
//
//   mString = new CYString(this, "CY_TEST_NAME", new QString(tr("test")));
//   mString->setLabel(tr("Name"));
//
//   mString = new CYString(this, "CY_TEST_DIR_NAME", new QString(tr("test")));
//   mString->setLabel(tr("Directory Name"));
//
//   mFlag = new CYFlag(this, "CY_TEST_DIR_NUM", new flg);
//   mFlag->setLabel(tr("Add test number"));
//
//   mFlag = new CYFlag(this, "CY_TEST_DIR_NUM", new flg);
//   mFlag->setLabel(tr("Add test number"));
//
//   mDU32 = new CYU32(this, "CY_TEST_NUM", new u32);
//   mDU32->setLabel(tr("Test number"));

  setUnderGroup("");
}


/*! Flags utile au dévelopement de la librairie. Chacun d'eux correspond à un développement spécifique. Les versions de librariries laissées au client doivent avoir tous ses flags à false. A la fin de chaque développement la donnée correspondante doit être supprimée.
    \fn CYDBLibs::initDevel()
 */
void CYDBLibs::initDevel()
{
}


/*! Initialise les données relatives à l'archivage des données.
    \fn CYDBLibs::initBackup()
 */
void CYDBLibs::initBackup()
{
  setUnderGroup(tr("Backup"));

  mFlag = new CYFlag(this, "CY_BACKUP_ENABLE", new flg, false);
  mFlag->setLabel(tr("Enable backup"));
  mFlag->setHelp(tr("Double safeguard in another partition all the files of parameters and useful results in the event of loss of the partition of datas /home."));

  mString = new CYString(this, "CY_BACKUP_DIR", new QString("/mnt/backup"));
  mString->setLabel(tr("Backup path"));
  mString->setHelp(tr("Partition backup path."));

  setUnderGroup(0);
}


/*! Initialise les données relatives aux évènements.
    \fn CYDBLibs::initEvents()
 */
void CYDBLibs::initEvents()
{
  setUnderGroup( tr( "Events" ) );

  mFlag = new CYFlag( this, "CY_EVENT_SPV_START", new flg, false);
  mFlag->setLabel( tr( "Supervisor starting" ) );

  mFlag = new CYFlag( this, "CY_EVENT_SPV_STOP", new flg, false);
  mFlag->setLabel( tr( "Supervisor stoping" ) );

  mFlag = new CYFlag( this, "CY_EVENT_METRO_CAL", new flg, false);
  mFlag->setLabel( tr( "New calibration" ) );

  mString = new CYString( this, "CY_EVENT_METRO_LAST");
  mString->setLabel( tr( "Last channel calibrated" ) );

  mFlag = new CYFlag( this, "CY_EVENT_SEND_PROJECT", new flg, false);
  mFlag->setLabel( tr( "Send project" ) );

  mString = new CYString( this, "CY_EVENT_PROJECT_SENT");
  mString->setLabel( tr( "Project sent to the numerical regulator" ) );

  setUnderGroup( tr( "Events" ) +":"+ tr( "Viewer" ));

  mFlag = new CYFlag( this, "CY_EVENT_VIEWER_NAME", new flg, false);
  mFlag->setLabel( tr( "Name" ) );

  mFlag = new CYFlag( this, "CY_EVENT_VIEWER_TIME", new flg, true);
  mFlag->setLabel( tr( "Date and hour" ) );

  mFlag = new CYFlag( this, "CY_EVENT_VIEWER_SINCE", new flg, false);
  mFlag->setLabel( tr( "Since" ) );

  mFlag = new CYFlag( this, "CY_EVENT_VIEWER_TYPE", new flg, true);
  mFlag->setLabel( tr( "Type" ) );

  mFlag = new CYFlag( this, "CY_EVENT_VIEWER_FROM", new flg, true);
  mFlag->setLabel( tr( "From" ) );

  mFlag = new CYFlag( this, "CY_EVENT_VIEWER_DESC", new flg, true);
  mFlag->setLabel( tr( "Description" ) );

  mString = new CYString( this, "CY_EVENT_VIEWER_HELP");
  mString->setLabel( tr( "Help of the selected event" ) );
	mString->setHelp( tr( "The help of an event is a real diagnostic aid to understand its cause." ) );
	mString->addNote(tr("One or more data (measurements, setpoints, parameters...) allow to visualize directly values which can influence the event triggering. These values, in bold, are refreshed when the help is displayed."));
	mString->addNote(tr("One or more hyperlinks allow to open the Cylix manual directly at a precise place, as for example the part relating to the setting which can influence the event triggering."));

  setUnderGroup( tr( "Events" ) +":"+ tr( "Mail" ));

  mFlag = new CYFlag( this, "CY_EVENTS_MAIL_ENABLE", new flg, true);
  mFlag->setLabel( tr( "Enable the send of fault messages by email" ) );

  mString = new CYString( this, "CY_EVENTS_MAIL_SMTP_SERVOR");
  mString->setLabel (tr( "SMTP servor name" ) );

  mDU16 = new CYU16( this, "CY_EVENTS_MAIL_SMTP_PORT", new u16, 25, 0);
  mDU16->setLabel( tr( "SMTP port" ) );

  mString = new CYString( this, "CY_EVENTS_MAIL_SMTP_USER");
  mString->setLabel (tr( "SMTP user name" ) );

  mString = new CYString( this, "CY_EVENTS_MAIL_SMTP_PASSWORD");
  mString->setLabel (tr( "SMTP password" ) );
  mString->setSimpleCrypt(new CYSimpleCrypt(Q_UINT64_C(0x0c2ad4a4acb9f023)));

  mString = new CYString( this, "CY_EVENTS_MAIL_FROM");
  mString->setLabel( tr( "Sender" ) );

  mString = new CYString( this, "CY_EVENTS_MAIL_TO");
  mString->setLabel( tr( "Receiver (s)" ) );
  mString->addNote( tr( "If there is more than one address, separate them with commas." ) );

  mString = new CYString( this, "CY_EVENTS_MAIL_SUBJECT");
  mString->setLabel( tr( "Subject" ) );
  mString->setDef(cyapp->aboutData()->programName());

  mString = new CYString( this, "CY_EVENTS_MAIL_BODY");
  mString->setLabel( tr( "Body" ) );

  setUnderGroup(0);
}


/*! Initialise les données relative au manuel
    \fn CYDBLibs::initHelp()
 */
void CYDBLibs::initHelp()
{
}


/*! Initialise les données relative à la gestion des utilisateurs
    \fn CYDBLibs::initUsers()
 */
void CYDBLibs::initUsers()
{
  setUnderGroup( tr( "Users" ));

  mFlag = new CYFlag( this, "CY_USER_SIMULATION", new flg, false);
  mFlag->setLabel(tr("Simulation access"));
  mFlag->setVal(false);

  mString = new CYString( this, "CY_USER_TITLE");
  mString->setLabel( tr( "Current user" ) );
  mString->setHelp( tr( "This information is also shown in the Cylix window title bar." ) );

  CYDateTime *dt = new CYDateTime(this, "CY_USER_DT_CHANGE");
  dt->setLabel(tr("Date and time of access change"));
  dt->setVolatile(true);

  setUnderGroup(0);
}

/*! Initialise les données relative à l'acquisition
    \fn CYDBLibs::initAcquisition()
 */
void CYDBLibs::initAcquisition()
{
  setUnderGroup( tr( "Acquisition" ));

  setUnderGroup(0);
}

/*! Initialise les données relative à la métrologie
    \fn CYDBLibs::initAcquisition()
 */
void CYDBLibs::initMetrology()
{
  if (!core->isBenchType("_METRO"))
    return;

  setUnderGroup( tr( "Metrology" ));

  mFlag = new CYFlag(this, "CY_METRO_DISABLE_RAZ", new flg, false);
  mFlag->setLabel(tr("Inhibits reset points"));
  mFlag->setVolatile(true);

  mFlag = new CYFlag(this, "CY_METRO_DISABLE_ADD", new flg, false);
  mFlag->setLabel(tr("Inhibits addition point"));
  mFlag->setVolatile(true);

  mFlag = new CYFlag(this, "CY_METRO_DISABLE_DEL", new flg, false);
  mFlag->setLabel(tr("Inhibits suppression point"));
  mFlag->setVolatile(true);

  mFlag = new CYFlag(this, "CY_METRO_DISABLE_CAL", new flg, false);
  mFlag->setLabel(tr("Inhibits calibration"));
  mFlag->setVolatile(true);

  mFlag = new CYFlag(this, "CY_METRO_DISABLE_VER", new flg, false);
  mFlag->setLabel(tr("Inhibits verification"));
  mFlag->setVolatile(true);

  mFlag = new CYFlag(this, "CY_METRO_DISABLE_DES", new flg, false);
  mFlag->setLabel(tr("Inhibits designer values"));
  mFlag->setVolatile(true);

  mFlag = new CYFlag(this, "CY_METRO_VIEW_WHOLE", new flg, true);
  mFlag->setLabel(tr("Full display"));
  mFlag->setHelp(0, tr("Full display with all values in table and all curves."), tr("Simplified display of table and curves without some values."));

  mColor = new CYColor(this, "CY_METRO_DEV_CURVE_COLOR", new QColor(Qt::blue));
  mColor->setLabel(tr("Curve color of deviation FS"));

  mDS8 = new CYS8(this, "CY_METRO_DEV_CURVE_STYLE", new s8);
  mDS8->setDef(QwtPlotCurve::Dots);
  mDS8->setLabel(tr("Curve style of deviation FS"));
  mDS8->setChoiceLabel(QwtPlotCurve::NoCurve, tr("No Curve"));
  mDS8->setChoiceLabel(QwtPlotCurve::Lines  , tr("Lines"));
  mDS8->setChoiceLabel(QwtPlotCurve::Sticks , tr("Sticks"));
  mDS8->setChoiceLabel(QwtPlotCurve::Steps  , tr("Steps"));
  mDS8->setChoiceLabel(QwtPlotCurve::Dots   , tr("Dots"));

  mColor = new CYColor(this, "CY_METRO_LIN_CURVE_COLOR", new QColor(Qt::magenta));
  mColor->setLabel(tr("Curve color of linearity error"));

  mDS8 = new CYS8(this, "CY_METRO_LIN_CURVE_STYLE", new s8);
  mDS8->setDef(QwtPlotCurve::Sticks);
  mDS8->setLabel(tr("Curve style of linearity error"));
  mDS8->setChoiceLabel(QwtPlotCurve::NoCurve, tr("No Curve"));
  mDS8->setChoiceLabel(QwtPlotCurve::Lines  , tr("Lines"));
  mDS8->setChoiceLabel(QwtPlotCurve::Sticks , tr("Sticks"));
  mDS8->setChoiceLabel(QwtPlotCurve::Steps  , tr("Steps"));
  mDS8->setChoiceLabel(QwtPlotCurve::Dots   , tr("Dots"));

  mString = new CYString( this, "CY_METRO_DIR", new QString(tr("%1/calibrating").arg(core->baseDirectory())));
  mString->setLabel( tr( "Directory of PDF recording" ) );
  mString->setHelp( tr( "Directory containing the report file of verification or calibration. These are automatically classified into sensor subdirectories." ) );



  setUnderGroup(0);
}

