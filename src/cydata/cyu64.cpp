/***************************************************************************
                          cyu64.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyu64.h"

CYU64::CYU64(CYDB *db, const QString &name, int format, Mode mode)
  : CYNum<u64>(db, name, format, mode, VU64)
{
}

CYU64::CYU64(CYDB *db, const QString &name, u64 *val, int format, Mode mode)
  : CYNum<u64>(db, name, val, format, mode, VU64)
{
}

CYU64::CYU64(CYDB *db, const QString &name, u64 *val, const u64 def, int format, Mode mode)
  : CYNum<u64>(db, name, val, def, format, mode, VU64)
{
}

CYU64::CYU64(CYDB *db, const QString &name, u64 *val, const u64 def, const u64 min, const u64 max, int format, Mode mode)
  : CYNum<u64>(db, name, val, def, min, max, format, mode, VU64)
{
}

CYU64::CYU64(CYDB *db, const QString &name, CYNum<u64>::Attr1<u64> *attr)
  : CYNum<u64>(db, name, attr, VU64)
{
}

CYU64::CYU64(CYDB *db, const QString &name, CYNum<u64>::Attr2<u64> *attr)
  : CYNum<u64>(db, name, attr, VU64)
{
}

CYU64::CYU64(CYDB *db, const QString &name, CYNum<u64>::Attr3<u64> *attr)
  : CYNum<u64>(db, name, attr, VU64)
{
}

CYU64::CYU64(CYDB *db, const QString &name, CYNum<u64>::Attr4<u64> *attr)
  : CYNum<u64>(db, name, attr, VU64)
{
}

CYU64::~CYU64()
{
}


int CYU64::nbDigit(Cy::NumBase base)
{
  return toString(max(), false, true, base).length();
}


QString CYU64::toString(bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  return toString(val(), withUnit, ctrlValid, base);
}


/*! @return la valeur \a val en chaîne de caractères suivant le format et l'unité.
    \fn CYU64::toString(double val, bool withUnit, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData)
 */
QString CYU64::toString(double val, bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  Cy::NumBase b;
  if ( base!=Cy::BaseData )
    b = base;
  else
    b = mNumBase;

  QString suffix;
  if (withUnit)
    suffix.append(" "+unit());

  if (ctrlValid && !isValid())
    return QString("%1%2").arg(stringDef()).arg(suffix);
  else
  {
    if (mFormat->isExponential())
      return QString("%1e%2%3").arg(mantissa(val), 0, 'f', nbDec()).arg(exponent(val)).arg(suffix);
    else if ( b!=Cy::Decimal )
    {
      QString num = QString("%1").arg((u64)val, 0, b).toUpper();
      QString zero;
      QString maxTxt = QString("%1").arg((u64)max(), 0, b).toUpper();
      zero.fill('0', maxTxt.length()-num.length());
      QString txt = QString("%1%2%3").arg(zero).arg(num).arg(suffix);
      return txt;
    }
    else
      return QString("%1%2").arg(val, 0, 'f', 0).arg(suffix);
  }
}
