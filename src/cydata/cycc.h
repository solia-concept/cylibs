//
// C++ Interface: cycc
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYCC_H
#define CYCC_H

// CYLIBS
#include "cydatasgroup.h"

class CYU64;
class CYCommand;
class CYString;

/** @short Compteur Cycles de maintenance.
    @author Gérald LE CLEACH
*/
class CYCC : public CYDatasGroup
{
  Q_OBJECT

public:
  /** Structure de saisie des attributs d'un compteur de maintenance (u32). */
  struct Attr1
  {
    /** Nom. */
    const QString &name;
    /** Index réseau correspondant à l'index des valeurs dans différentes structures réseau. */
    int idNet;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Groupe du compteur. */
    QString group;
    /** Etiquette du compteur. */
    QString label;
  };

  /** Création d'un compteur de cycles/commutations de maintenance (u64).
    * @param db     Base de données parent.
    * @param name   Nom du compteur.
    * @param val    Valeur de la donnée (total et partielle).
    * @param attr   Attributs du compteur de cycles. */
  CYCC(CYDB *db, const QString &name, t_cu64 *cnt, Attr1 *attr);

  /** Création d'un compteur de cycles/commutations de maintenance (u32).
    * @param db     Base de données parent.
    * @param name   Nom du compteur.
    * @param val    Valeur de la donnée (total et partielle).
    * @param attr   Attributs du compteur de cycles. */
  CYCC(CYDB *db, const QString &name, t_cu32 *cnt, Attr1 *attr);

  /** Création d'un compteur de cycles/commutations de maintenance (u64).
    * @param db     Base de données parent.
    * @param name   Nom du compteur.
    * @param val    Valeur de la donnée (total et partielle).
    * @param idNet  Index réseau correspondant à l'index des valeurs dans différentes structures réseau.
    * @param mode   Mode d'utilisation.
    * @param format Numéro du format d'affichage dans l'application.
    * @param group  Groupe du compteur.
    * @param label  Etiquette du compteur.
    */
  CYCC(CYDB *db, const QString &name, t_cu64 *cnt, int idNet, Cy::Mode mode, int format, QString group, QString label);

  /** Création d'un compteur de cycles/commutations de maintenance (u32).
    * @param db     Base de données parent.
    * @param name   Nom du compteur.
    * @param val    Valeur de la donnée (total et partielle).
    * @param idNet  Index réseau correspondant à l'index des valeurs dans différentes structures réseau.
    * @param mode   Mode d'utilisation.
    * @param format Numéro du format d'affichage dans l'application.
    * @param group  Groupe du compteur.
    * @param label  Etiquette du compteur.
    */
  CYCC(CYDB *db, const QString &name, t_cu32 *cnt, int idNet, Cy::Mode mode, int format, QString group, QString label);

  ~CYCC();

  void initResetData(CYDB *db, flg *val);
  CYCommand * resetData();

  CYU64 *thresholdData() { return mThresholdData; }
  CYEvent *ctrlEvent;
  void setCtrlEvent(CYEvent *ev);

  CYEvent *alert;

public slots:
    void ctrlPartial();

protected:
    virtual void init();

  /** Donnée du compteur total. */
  CYData *mTotalData;
  /** Donnée du compteur partiel. */
  CYData *mPartialData;
  /** Donnée seuil d'alerte du compteur partiel. */
  CYU64 *mThresholdData;
  /** Commande de remise à 0 du compteur partielle. */
  CYCommand *mResetData;
  /** Donnée pouvant contenir un note de maintenance.. */
  CYString *mNoteData;
};

#endif
