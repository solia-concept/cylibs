/***************************************************************************
                          cys16.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cys16.h"

CYS16::CYS16(CYDB *db, const QString &name, int format, Mode mode)
  : CYNum<s16>(db, name, format, mode, VS16)
{
}

CYS16::CYS16(CYDB *db, const QString &name, s16 *val, int format, Mode mode)
  : CYNum<s16>(db, name, val, format, mode, VS16)
{
}

CYS16::CYS16(CYDB *db, const QString &name, s16 *val, const s16 def, int format, Mode mode)
  : CYNum<s16>(db, name, val, def, format, mode, VS16)
{
}

CYS16::CYS16(CYDB *db, const QString &name, s16 *val, const s16 def, const s16 min, const s16 max, int format, Mode mode)
  : CYNum<s16>(db, name, val, def, min, max, format, mode, VS16)
{
}

CYS16::CYS16(CYDB *db, const QString &name, CYNum<s16>::Attr1<s16> *attr)
  : CYNum<s16>(db, name, attr, VS16)
{
}

CYS16::CYS16(CYDB *db, const QString &name, CYNum<s16>::Attr2<s16> *attr)
  : CYNum<s16>(db, name, attr, VS16)
{
}

CYS16::CYS16(CYDB *db, const QString &name, CYNum<s16>::Attr3<s16> *attr)
  : CYNum<s16>(db, name, attr, VS16)
{
}

CYS16::CYS16(CYDB *db, const QString &name, CYNum<s16>::Attr4<s16> *attr)
  : CYNum<s16>(db, name, attr, VS16)
{
}

CYS16::~CYS16()
{
}
