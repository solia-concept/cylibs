//
// C++ Implementation: cycolor
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2006
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cycolor.h"

CYColor::CYColor(CYDB *db, const QString &name, QColor *val, QString label, Cy::Mode mode)
  : CYData(db, name, 0, mode, Color)
{
  if (val)
    mVal = val;
  else
    mVal = new QColor;

  mDef = *mVal;

  setNbDec(0);
  mLabel = label;

  mOldVal = CYColor::val();
}

CYColor::CYColor(CYDB *db, const QString &name, const char *color, QString label, Cy::Mode mode)
  : CYData(db, name, 0, Sys, Color)
{
  mVal = new QColor(color);
  *mVal  = val();
  mLabel = label;
  mMode  = mode;
  setNbDec(0);

  mDef = *mVal;

  mOldVal = CYColor::val();
}

CYColor::CYColor(CYDB *db, const QString &name, Attr *attr)
  : CYData(db, name, 0, Sys, Color)
{
  mVal = new QColor(attr->color);
  *mVal  = val();
  mLabel = attr->label;
  mMode  = attr->mode;
  setNbDec(0);

  mDef = *mVal;

  mOldVal = CYColor::val();
}

CYColor::~CYColor()
{
}

QColor CYColor::val()
{
  return *mVal;
}

void CYColor::setVal(QColor val)
{
  *mVal = val;
  emit valueChanged();
}

QString CYColor::toString(bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  Q_UNUSED(withUnit)
  Q_UNUSED(ctrlValid)
  Q_UNUSED(base)
  return val().name();
}

QString CYColor::toString(QColor val, bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  Q_UNUSED(withUnit)
  Q_UNUSED(ctrlValid)
  Q_UNUSED(base)
  return val.name();
}

QString CYColor::inputHelp(bool whole, bool withGroup)
{
  Q_UNUSED(whole)
  QString tmp;
  if ((group()!="") && withGroup)
    tmp.append(QString("<b>%1: </b>").arg(group()));
  else
    tmp.append(QString("<b></b>"));
  if (label()!=objectName())
    tmp.append(label());
  else
    tmp.append("<br>"+label());
  if (phys()!="")
    tmp.append(QString(" [%1]").arg(phys()));
  if (elec()!="")
    tmp.append(QString(" [%1]").arg(elec()));
  if (addr()!="")
    tmp.append(QString(" [%1]").arg(addr()));
  if (!tmp.isEmpty())
    tmp.append("<hr>");
  else
    tmp.append("<br>");
  if (help()!="")
    tmp.append(help());
  return tmp;
}

bool CYColor::detectValueChanged()
{
  return ( (*mVal==mOldVal) ? false : true );
}

QString CYColor::exportData()
{
  QString values = QString("%1\t%2\t \t ").arg(toString()).arg(toString(def()));
  QString line = QString("%1\t%2\t%3\t%4\t%5\t%6\t%7")
                .arg(hostsName()).arg(linksName()).arg(group())
                .arg(objectName()).arg(label()).arg(values).arg(help());

  if (line.count("\t")>9)
    CYMESSAGETEXT(line);

  line.replace("\n", " ");
  line.append("\n");

  return line;
}
