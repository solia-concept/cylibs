/***************************************************************************
                          cyflag.cpp  -  description
                             -------------------
    début                  : mer mai 7 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyflag.h"
#include "cytypes.h"

CYFlag::CYFlag(CYDB *db, const QString &name, flg *val, const flg def, Mode mode)
  : CYNum<flg>(db, name, val, def, 0, 1, 0, mode, VFL)
{
  init();
}

CYFlag::CYFlag(CYDB *db, const QString &name, Attr1 *attr, const flg def, Mode mode)
  : CYNum<flg>(db, name, attr->val, def, 0, mode, VFL)
{
  mLabel = attr->label;
  init();
}

CYFlag::~CYFlag()
{
}

void CYFlag::init()
{
  mIsFlag    = true;
  mIsNum     = true;
  mHelp      = "";
  mHelptrue  = "";
  mHelpfalse = "";

	setChoiceLabel(true, mChoicetrue);
	setChoiceLabel(false, mChoicefalse);
}

QString CYFlag::displayHelp(bool whole, bool oneLine, bool withGroup, bool withValue)
{
  Q_UNUSED(whole)
  QString tmp;
  QString yes ="";
  QString no  ="";

  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(group()));

  tmp.append(label());
  if (phys()!="")
    tmp.append(QString(" [%1]").arg(phys()));
  if (elec()!="")
    tmp.append(QString(" [%1]").arg(elec()));
  if (addr()!="")
    tmp.append(QString(" [%1]").arg(addr()));
	if (withValue)
		tmp.append(QString(" = <b>%1</b>").arg(choiceLabel()));

  if ( !oneLine )
  {
    if (!tmp.isEmpty())
      tmp.append("<hr>");
    else
      tmp.append("<br>");
    tmp.append(help()+"<br>");
    if (mHelptrue!="")
      yes = "<tr><td>"+mChoicetrue+": </td><td>"+mHelptrue+"</td></tr>";
    if (mHelpfalse!="")
      no  = "<tr><td>"+mChoicefalse+": </td><td>"+mHelpfalse+"</td></tr>";
    if ((yes!="") || (no!=""))
      tmp.append(tr("If it is:")+"<table>"+yes+no+"</table>");
    if (!note().isEmpty()) tmp.append("<br>"+note());
  }
  return tmp;
}

QString CYFlag::inputHelp(bool whole, bool withGroup)
{
  QString tmp;
  QString yes ="";
  QString no  ="";

  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(group()));
  tmp.append(label());
  if (!tmp.isEmpty())
    tmp.append("<hr>");
  else
    tmp.append("<br>");
  tmp.append(help()+"<br>");
  if (mHelptrue!="")
    yes = "<tr><td>"+mChoicetrue+": </td><td>"+mHelptrue+"</td></tr>";
  if (mHelpfalse!="")
    no  = "<tr><td>"+mChoicefalse+": </td><td>"+mHelpfalse+"</td></tr>";
  if ((yes!="") || (no!=""))
    tmp.append(tr("If you select:")+"<table>"+yes+no+"</table>");
  if (whole && helpDesignerValue())
  {
    if (def())
      tmp.append("<ul><li>"+tr("Designer value:")+" "+mChoicetrue+".</li></ul>");
    else
      tmp.append("<ul><li>"+tr("Designer value:")+" "+mChoicefalse+".</li></ul>");
  }
  if (!note().isEmpty()) tmp.append("<br>"+note());
  return tmp;
}

QString CYFlag::selectionHelp()
{
  // + AIDE OUI + AIDE NON
  if ((mHelptrue!="") && (mHelpfalse!=""))
    return QString(tr("If you select:")
                   + QString("<table>"
                               "<tr><td>"+mChoicetrue +":</td> <td>"+mHelptrue +"</td></tr>"
                               "<tr><td>"+mChoicefalse+":</td> <td>"+mHelpfalse+"</td></tr>"
                             "</table>"));

  // + AIDE OUI - AIDE NON
  if ((mHelptrue!="") && (mHelpfalse==""))
    return QString(tr("If you select:")
                   + QString("<table>"
                               "<tr><td>"+mChoicetrue +":</td> <td>"+mHelptrue +"</td></tr>"
                             "</table>"));

  // - AIDE OUI + AIDE NON
  if ((mHelptrue=="") && (mHelpfalse!=""))
    return QString(tr("If you select:")
                   + QString("<table>"
                               "<tr><td>"+mChoicefalse+":</td> <td>"+mHelpfalse+"</td></tr>"
                             "</table>"));

  // - AIDE OUI - AIDE NON
  return "";
}

void CYFlag::setHelpTrue(QString msg)
{
  mHelptrue = msg;
}

void CYFlag::setHelpFalse(QString msg)
{
  mHelpfalse = msg;
}

void CYFlag::setHelp(QString help, QString yes, QString no)
{
  mHelp    = help;
  mHelptrue = yes;
  mHelpfalse  = no;
}

QString CYFlag::physicalType()
{
  return tr("Binary");
}

bool CYFlag::detectValueChanged()
{
  bool ret = true;
  if (*mVal==mOldVal)
    ret = false;
  else if (!(*mVal))
    mRising = false;
  else
    mRising = true;
  mOldVal = *mVal;
  setValueChanged(ret);
  return ret;
}

void CYFlag::import(flg val)
{
  setVal(val);
}
