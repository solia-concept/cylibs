#ifndef CYDATACHOICE_H
#define CYDATACHOICE_H

#include <QObject>
#include <QString>
#include <QPixmap>

/** Définition d'un choix disponible relatif à une valeur possible. */
class CYDataChoice : public QObject
{
  Q_OBJECT
public:
  explicit CYDataChoice(QObject *parent = 0);

  CYDataChoice &operator=( const CYDataChoice & );

  int value;
  /** Libellé */
  QString label;
  /** Aide */
  QString help;
  /** Illustration */
  QPixmap pixmap;
};

#endif // CYDATACHOICE_H
