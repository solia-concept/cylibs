/***************************************************************************
                          cyf64.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYF64_H
#define CYF64_H

// CYLIBS
#include "cynum.h"

/** CYF64 est une donnée flottante de 64 bits.
  * @short Donnée flottante de 64 bits.
  * @author Gérald LE CLEACH
  */

class CYF64 : public CYNum<f64>
{
public:
  /** Création d'une donnée flottante de 64 bits avec sa valeur.
    * @param db     Base de données parent. 
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYF64(CYDB *db, const QString &name, f64 *val, int format=0, Mode mode=Sys);

  /** Création d'une donnée flottante de 64 bits avec sa valeur ainsi que sa valeur par défaut.
    * @param db     Base de données parent. 
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYF64(CYDB *db, const QString &name, f64 *val, const f64 def, int format=0, Mode mode=Sys);

  /** Création d'une donnée flottante de 64 bits avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent. 
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYF64(CYDB *db, const QString &name, f64 *val, const f64 def, const f64 min, const f64 max, int format=0, Mode mode=Sys);

  /** Création d'une donnée flottante de 64 bits avec les attributs Attr1.
    * @param db    Base de données parent. 
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYF64(CYDB *db, const QString &name, CYNum<f64>::Attr1<f64> *attr);

  /** Création d'une donnée flottante de 64 bits avec les attributs Attr2.
    * @param db    Base de données parent. 
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYF64(CYDB *db, const QString &name, CYNum<f64>::Attr2<f64> *attr);

  /** Création d'une donnée flottante de 64 bits avec les attributs Attr3.
    * @param db    Base de données parent. 
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYF64(CYDB *db, const QString &name, CYNum<f64>::Attr3<f64> *attr);

  /** Création d'une donnée flottante de 64 bits avec les attributs Attr4.
    * @param db    Base de données parent. 
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYF64(CYDB *db, const QString &name, CYNum<f64>::Attr4<f64> *attr);

  ~CYF64();
};

#endif
