/***************************************************************************
                          cydig.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDIG_H
#define CYDIG_H

// CYLIBS
#include "cybool.h"

class CYWord;

/** CYDig est la classe de base entrées/sorties TORS.
  * @short E/S TOR.
  * @author Gérald LE CLEACH
  */

class CYDig : public CYBool
{
public:
  /** Structure de saisie des attributs d'une E/S TOR pointant sur un bit. */
  struct AttrBit
  {
    /** Nom. */
    QString name;
    /** Mot contenant la valeur de la donnée. */
    CYWord *word;
    /** Bit de la donnée dans le mot. */
    short bit;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Message d'aide. */
    QString help;
  };

  /** Structure de saisie des attributs d'une E/S TOR pointant sur un flag. */
  struct AttrFlag1
  {
    /** Nom. */
    QString name;
    /** Flag donnant la valeur de la donnée. */
    flg *flag;
    /** Repère physique. */
    QString phys;
    /** Message d'aide. */
    QString help;
  };

  /** Structure de saisie des attributs d'une E/S TOR pointant sur un flag. */
  struct AttrFlag2
  {
    /** Nom. */
    QString name;
    /** Flag donnant la valeur de la donnée. */
    flg *flag;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Message d'aide. */
    QString help;
  };

  /** Création d'une E/S TOR pointant sur un bit.
    * @param db    Base de données parent. 
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée TOR.
    * @param c     Numéro de la carte TOR. */
  CYDig(CYDB *db, const QString &name, AttrBit *attr, int c=-1);

  /** Création d'une E/S TOR pointant sur un flag.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée TOR.
    * @param c     Numéro de la carte TOR. */
  CYDig(CYDB *db, const QString &name, AttrFlag1 *attr, int c=-1);

  /** Création d'une E/S TOR pointant sur un flag.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée TOR
    * @param c     Numéro de la carte TOR. */
  CYDig(CYDB *db, const QString &name, AttrFlag2 *attr, int c=-1);

  /** Création d'une sortie TOR pointant sur un bit pour le mode forçage.
    * @param db    Base de données parent. 
    * @param name  Nom de la sortie TOR en mode forçage.
    * @param word  Mot contenant la valeur de la donnée.
    * @param bit   Bit de la donnée dans le mot.
    * @param c     Numéro de la carte TOR. */
  CYDig(CYDB *db, const QString &name, CYWord *word, short bit, int c=-1);

  /** Création d'une sortie TOR pointant sur un flag pour le mode forçage.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie TOR en mode forçage.
    * @param flag   Flag donnant la valeur de la donnée.
    * @param c     Numéro de la carte TOR. */
  CYDig(CYDB *db, const QString &name, flg *flag, int c=-1);

  ~CYDig();

protected: // Protected attributes
  /** Carte E/S TOR (Vaut -1 si ASi). */
  int mCard;
};

#endif
