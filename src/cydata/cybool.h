/***************************************************************************
                          cybool.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYBOOL_H
#define CYBOOL_H

// CYLIBS
#include "cytype.h"

class CYWord;

/** CYBool est une donnée booléenne pouvant pointé soit sur un bit d'un CYWord soit sur flag flg.
  * @short Donnée booléenne.
  * @author Gérald LE CLEACH
  */

class CYBool : public CYType<bool>
{
  Q_OBJECT

public:
  /** Structure de saisie des attributs d'une donnée booléenne pointant sur un bit. */
  struct AttrBit
  {
    /** Nom. */
    const QString &name;
    /** Mot contenant la valeur de la donnée. */
    CYWord *word;
    /** Index de la donnée dans le mot. */
    short bit;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Label. */
    QString label;
    /** Message d'aide. */
    QString help;
  };

  /** Structure de saisie des attributs d'une donnée booléenne pointant sur un flag. */
  struct AttrFlag
  {
    /** Nom. */
    const QString &name;
    /** Flag donnant la valeur de la donnée. */
    flg *flag;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Label. */
    QString label;
    /** Message d'aide. */
    QString help;
  };

  /** Création d'une donnée booléenne pointant sur un bit.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYBool(CYDB *db, const QString &name, AttrBit *attr);

  /** Création d'une donnée booléenne pointant sur un flag.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYBool(CYDB *db, const QString &name, AttrFlag *attr);

  /** Création d'une donnée booléenne pointant sur un bit.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param word   Mot contenant la valeur de la donnée.
    * @param bit    Index de la donnée dans le mot.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYBool(CYDB *db, const QString &name, CYWord *word, short bit, int format=0, Cy::Mode mode=Sys);

  /** Création d'une donnée booléenne pointant sur un flag.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param flag   Flag donnant la valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYBool(CYDB *db, const QString &name, flg *flag, int format=0, Cy::Mode mode=Sys);

  ~CYBool();

  /** Lecture de la donnée. */
  virtual bool readData();
  /** Ecriture de la donnée. */
  virtual bool writeData();

  /** @return la valeur. */
  virtual bool val();

  /** @return le type physique. */
  virtual QString physicalType();

  /** @return l'aide à la visualisation.
		* @param whole			Aide entière ex: pour les données numériques avec les bornes, la précision si il y en a une et la valeur par défaut.
		* @param oneLine		Aide uniquement composée de la première ligne.
		* @param withGroup	Aide avec le groupe de la donnée.
		* @param withValue	Aide avec la valeur courante. */
	virtual QString displayHelp(bool whole=true, bool oneLine=false, bool withGroup=true, bool withValue=false);
  /** @return l'aide à la saisie.
		* @param whole			Aide entière ex: pour les données numériques avec les bornes, la précision si il y en a une.
		* @param withGroup	Aide avec le groupe de la donnée. */
  virtual QString inputHelp(bool whole=true, bool withGroup=true);

   /** @return la valeur \a val en chaîne de caractères. */
  virtual QString toString(bool withUnit=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

public slots: // Public slots
  /** Saisie la valeur \a val. */
  virtual void setVal(const bool val);

protected: // Protected attributes
  /** Pointeur sur le mot contenant la donnée. */
  CYWord *mWord;
  /** Bit de donnée. */
  short mBit;
};

#endif
