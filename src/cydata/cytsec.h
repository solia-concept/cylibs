/***************************************************************************
                          cytsec.h  -  description
                             -------------------
    début                  : lun mai 12 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYTSEC_H
#define CYTSEC_H

// CYLIBS
#include "cyf32.h"

class CYTime;

/** CYTSec représente une donnée temporelle flottante et ayant pour unité la seconde.
  * Cette donnée pointe sur un u32.
  * @short Donnée temporelle flottante en seconde.
  * @author Gérald LE CLEACH
  */

class CYTSec : public CYF32
{
  Q_OBJECT
public:
  /** Création d'une donnée temporelle flottante en seconde avec sa valeur ainsi
    * que les valeurs par défaut, maxi et mini.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée (en u32).
    * @param mode  Mode d'utilisation.
    * @param base  Base de temps en ms (ex 1000.0 = 1 sec). */
   CYTSec(CYDB *db, const QString &name, u32 *val, Mode m=Sys, double base=BASE_MSEC);

  /** Création d'une donnée temporelle flottante en seconde avec sa valeur ainsi
    * que les valeurs par défaut, maxi et mini.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée (en u32).
    * @param def   Valeur par défaut (en f32).
    * @param min   Valeur mini (en f32).
    * @param max   Valeur maxi (en f32).
    * @param mode  Mode d'utilisation.
    * @param base  Base de temps en ms (ex 1000.0 = 1 sec). */
   CYTSec(CYDB *db, const QString &name, u32 *val, const f32 def, const f32 min, const f32 max, Mode m=Sys, double base=BASE_MSEC);

  ~CYTSec();

  /** Saisie une valeur flottante en seconde
    * ainsi que la valeur de la donnée entière correspondante. */
  void setVal(const f32 val);
  /**
   * @brief Import contrôlé d'une valeur.
   * @param val Valeur à charger
   * En cas de dépassement d'une limite de la donnée, la valeur à charger
   * est alors forcée à la valeur de la limite dépassée.
   */
  virtual void import(f32 val);
  /** @return la valeur arrondie. */
  f32 val();
  /** @return la valeur sans arrondie. */
  f32 real();
  /** @return la valeur en millisecondes. */
  u32 ms();
  /** @return la valeur \a val en millisecondes. */
  u32 ms(const double val);

  /** Charge la valeur constructeur. */
  virtual void designer();

private: // Private attributes
  /** Valeur entière en milli-seconde. */ 
  u32 *mPtr;
  /** Base de temps. */
  double mBase;
};

#endif
