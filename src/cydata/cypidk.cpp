/***************************************************************************
                          cypidk.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cypidk.h"

CYPIDK::CYPIDK(CYDB *db, const QString &name, f32 *val, const f32 def, const f32 min, const f32 max)
  : CYF32(db, name, val, def, min, max)
{
  mLabel = tr("Integral/derivative ratio");
  addNote(tr("According to Ziegler and Nichols the best ratio is 4.0."));
  addNote(tr("If 0, the derivative control won't be in operation."));
  addNote(tr("This control is a mixt P.I.D."));
}

CYPIDK::~CYPIDK()
{
}
