/***************************************************************************
                          cycttime.cpp  -  description
                             -------------------
    begin                : mer nov 3 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cycttime.h"

// CYLIBS
#include "cycore.h"
#include "cytime.h"
#include "cytsec.h"
#include "cyflag.h"

CYCTTime::CYCTTime(CYDB *db, const QString &name, int idNet, t_ct *values, QString inh_def, QString stab_def, f32 ale_def, flg fl_arret_ale_def, flg fl_arret_def_def, QString label, int format, QString settings)
  : CYCT(db, name, idNet, values, fl_arret_ale_def, fl_arret_def_def, label, format, settings)
{
  mInhibitionTime = new CYTime(db, QString("%1_INH").arg(name), &values->inh, inh_def, "0m0s", "240m0s");
  mStabilisationTime = new CYTime(db, QString("%1_STAB").arg(name), &values->stab, stab_def, "0m0s", "240m0s");
  mAlerteTime = new CYTSec(db, QString("%1_ALE").arg(name), &values->ale, ale_def, 0.0, 600.0);
  init();
}

CYCTTime::CYCTTime(CYDB *db, const QString &name, t_ct *values, Attr1 *attr)
  : CYCT(db, name, attr->idNet, values, attr->fl_arret_ale_def, attr->fl_arret_def_def, attr->label, attr->format, attr->settings)
{
  mInhibitionTime = new CYTime(db, QString("%1_INH").arg(name), &values->inh, attr->inh_def, "0m0s", "240m0s");
  mStabilisationTime = new CYTime(db, QString("%1_STAB").arg(name), &values->stab, attr->stab_def, "0m0s", "240m0s");
  mAlerteTime = new CYTSec(db, QString("%1_ALE").arg(name), &values->ale, attr->ale_def, 0.0, 600.0);
  init();
}

CYCTTime::CYCTTime(CYDB *db, const QString &name, t_ct *values, Attr2 *attr)
  : CYCT(db, name, attr->idNet, values, attr->fl_arret_ale_def, attr->fl_arret_def_def, attr->label, attr->format, attr->settings)
{
  mInhibitionTime = new CYTime(db, QString("%1_INH").arg(name), &values->inh, attr->inh_def, attr->inh_min, attr->inh_max);
  mStabilisationTime = new CYTime(db, QString("%1_STAB").arg(name), &values->stab, attr->stab_def, attr->stab_min, attr->stab_max);
  mAlerteTime = new CYTSec(db, QString("%1_ALE").arg(name), &values->ale, attr->ale_def, attr->ale_min, attr->ale_max);
  init();
}

CYCTTime::~CYCTTime()
{
}

void CYCTTime::init()
{
  mInhibitionTime->setLabel(tr("Minimum measure control delay (T0)"));
  mInhibitionTime->addNote(tr("The minimum control delay is T0"));
  mInhibitionTime->addNote(tr("The maximum control delay is T0+T1"));
  mInhibitionTime->setGroup(mGroup);

  mStabilisationTime->setLabel(tr("Supplementary measure control delay (T1)"));
  mStabilisationTime->setHelp(  tr("The control start when the measure in inside the tolerances, or when this optional inhibition is overreached"));
  mStabilisationTime->addNote(tr("The minimum control delay is T0"));
  mStabilisationTime->addNote(tr("The maximum control delay is T0+T1"));
  mStabilisationTime->setGroup(mGroup);

  mAlerteStop->setLabel(tr("Stop test if maximum alert ?"));
  mAlerteStop->setHelp(0, tr("The bench will be stopped if the max time alert is reached."));
  mAlerteStop->setGroup(mGroup);

  mAlerteTime->setLabel(tr("Maximum time out of alert tolerances"));
  mAlerteTime->setHelp(tr("Maximum outside alert tolerances time before process fault."));
  mAlerteTime->setGroup(mGroup);

  mFaultStop->setLabel(tr("Stop test if out of fault tolerances ?"));
  mFaultStop->setHelp(0, tr("The bench will be stopped if fault tolerance is reached."));
  mFaultStop->setGroup(mGroup);
}


CYEvent *CYCTTime::initFaultAlertMax(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  return CYCT::initFaultAlertMax(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
}


CYEvent *CYCTTime::initFaultAlertMin(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  return CYCT::initFaultAlertMin(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
}
