/***************************************************************************
                          cypidcalculd.cpp  -  description
                             -------------------
    début                  : lun mai 12 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cypidcalculd.h"

CYPIDCalculD::CYPIDCalculD(CYDB *db, const QString &name, flg *val, const flg def)
  : CYFlag(db, name, val, def)
{
  mLabel   = QString(tr("Measure derivative"));
  setHelp("",
          tr("The derivative calculation will be done on the measure."),
          tr("This may be useful to avoid overshoot at the ramp end."
               "The derivative calculation will be done on the gap (setpoint - measure)."));
}

CYPIDCalculD::~CYPIDCalculD()
{
}
