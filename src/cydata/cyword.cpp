/***************************************************************************
                          cyword.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyword.h"

CYWord::CYWord(CYDB *db, const QString &name, u16 *val, u16 def, int format, Mode mode)
  : CYType<u16>(db, name, format, mode, Word)
{
  mGroup = tr("Word");
  mVal  = val;
  *mVal = def;
  mDef  = def;
  mOldVal = *mVal;
}

CYWord::~CYWord()
{
}

bool CYWord::bit(const unsigned short bit)
{
  if (bit>15)
    CYFATALDATA(objectName());

  return (((*mVal) & MASK(bit)) ? true : false);
}

void CYWord::setBit(const unsigned short bit, const bool val)
{
  if (val)
    (*mVal) |= MASK(bit);
  else
    (*mVal) &= ~MASK(bit);
}

QString CYWord::toString(bool binary, bool ctrlValid, Cy::NumBase base)
{
  Q_UNUSED(base)
  if (ctrlValid && !isValid())
    return stringDef();
  else
  {
    if (binary)
    {
      QString string;
      for (int bit=0; bit<16; bit++)
        string.append(QString("%1").arg(((*mVal) & MASK(bit)) ? 1 : 0));
      return string;
    }
    else
      return QString("%1").arg(val());
  }
}
