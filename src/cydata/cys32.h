/***************************************************************************
                          cys32.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYS32_H
#define CYS32_H

// CYLIBS
#include "cynum.h"

/** CYS32 est une donnée entière de 32 bits.
  * @short Donnée entière de 32 bits.
  * @author Gérald LE CLEACH
  */

class CYS32 : public CYNum<s32>
{
public:
  /** Création d'une donnée entière de 32 bits.
    * @param db     Base de données parent.   
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS32(CYDB *db, const QString &name, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 32 bits avec sa valeur.
    * @param db     Base de données parent.   
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS32(CYDB *db, const QString &name, s32 *val, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 32 bits avec sa valeur ainsi que sa valeur par défaut.
    * @param db     Base de données parent.   
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS32(CYDB *db, const QString &name, s32 *val, const s32 def, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 32 bits avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.   
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS32(CYDB *db, const QString &name, s32 *val, const s32 def, const s32 min, const s32 max, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 32 bits avec les attributs Attr1.
    * @param db    Base de données parent.   
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS32(CYDB *db, const QString &name, CYNum<s32>::Attr1<s32> *attr);

  /** Création d'une donnée entière de 32 bits avec les attributs Attr2.
    * @param db    Base de données parent.   
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS32(CYDB *db, const QString &name, CYNum<s32>::Attr2<s32> *attr);

  /** Création d'une donnée entière de 32 bits avec les attributs Attr3.
    * @param db    Base de données parent.   
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS32(CYDB *db, const QString &name, CYNum<s32>::Attr3<s32> *attr);

  /** Création d'une donnée entière de 32 bits avec les attributs Attr4.
    * @param db    Base de données parent.   
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS32(CYDB *db, const QString &name, CYNum<s32>::Attr4<s32> *attr);

  ~CYS32();
};

#endif
