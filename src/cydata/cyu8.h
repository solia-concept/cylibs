/***************************************************************************
                          cyu8.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYU8_H
#define CYU8_H

// CYLIBS
#include "cynum.h"

/** CYU8 est une donnée entière non-signée de 8 bits.
  * @short Donnée entière de 8 bits.
  * @author Gérald LE CLEACH
  */

class CYU8 : public CYNum<u8>
{
public:
  /** Création d'une donnée entière non-signée de 8 bits.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU8(CYDB *db, const QString &name, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière non-signée de 8 bits avec valeur de rattachement.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU8(CYDB *db, const QString &name, u8 *val, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 8 bits avec sa valeur ainsi que sa valeur par défaut.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU8(CYDB *db, const QString &name, u8 *val, const u8 def, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 8 bits avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYU8(CYDB *db, const QString &name, u8 *val, const u8 def, const u8 min, const u8 max, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière non-signée de 8 bits avec les attributs Attr1.
    * @param db     Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU8(CYDB *db, const QString &name, CYNum<u8>::Attr1<u8> *attr);

  /** Création d'une donnée entière non-signée de 8 bits avec les attributs Attr2.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU8(CYDB *db, const QString &name, CYNum<u8>::Attr2<u8> *attr);

  /** Création d'une donnée entière non-signée de 8 bits avec les attributs Attr3.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU8(CYDB *db, const QString &name, CYNum<u8>::Attr3<u8> *attr);

  /** Création d'une donnée entière non-signée de 8 bits avec les attributs Attr4.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYU8(CYDB *db, const QString &name, CYNum<u8>::Attr4<u8> *attr);

  ~CYU8();

  /** @return le nombre de chiffres total. */
  virtual int nbDigit(Cy::NumBase base=Cy::BaseData);

   /** @return la valeur courrante en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(bool withUnit=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

  virtual QString toString(double val, bool withUnit, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);
};

#endif
