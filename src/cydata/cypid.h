/***************************************************************************
                          cypid.h  -  description
                             -------------------
    begin                : jeu avr 22 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYPID_H
#define CYPID_H

// QT
#include <qobject.h>

// CYLIBS
#include "cytypes.h"
#include "cydb.h"

class CYF32;
class CYS16;
class CYFlag;

/** @short PID.
  * @author LE CLÉACH Gérald
  */

class CYPID : public QObject
{
  Q_OBJECT
public:
  /** Création d'un PID avec coefficient proportionnel.
     * @param db      Base de données parent.
     * @param name    Nom du PID (préfixe du nom des données).
     * @param values  Pointe sur les valeurs.
     * @param bp      Flag d'activation de bande proportionnelle:
                      - false => Coefficient proporionnel.
                      - true  => Bande proporionnelle.
     * @param group   Nom du groupe des données relatives.
     * @param type    Type de PID possible (@see cypid_type)
     * @param oldName Ancien nom de PID nécessaire en cas de changement de nom. Permet de récupérer données relatives dans anciens fichiers de sauvegarde.
     */
  CYPID(CYDB *db, const QString &name, t_pid *values, bool bp=false, QString group="PID", cypid_type type=PID_TYPE_FAULT, const QString &oldName="");

  ~CYPID();

  /** Initialise le coefficient proportionnel ou la bande proportionnelle.
    * @param def    Valeur par défaut.
    * @param min    Limite inférieure.
    * @param max    Limite supérieure.
    * @param format Numéro du format d'affichage dans l'application
                    (Utile uniquement pour la bande proportionnelle). */
  void init_P(f32 def=1.0, f32 min=0.0, f32 max=99.0, int format=0);
  /** Initialise le coefficient intégral.
    * @param def    Valeur par défaut.
    * @param min    Limite inférieure.
    * @param max    Limite supérieure.
    * @param format Numéro du format d'affichage dans l'application. */
  void init_I(f32 def=0.1, f32 min=0.0, f32 max=9000.0, int format=0);
  /** Initialise le facteur intégral/dérivée.
    * @param def    Valeur par défaut.
    * @param min    Limite inférieure.
    * @param max    Limite supérieure.
    * @param format Numéro du format d'affichage dans l'application. */
  void init_K(f32 def=0.0, f32 min=0.0, f32 max=900.0, int format=0);
  /** Initialise la puissance minimale.
    * @param def    Valeur par défaut.
    * @param min    Limite inférieure.
    * @param max    Limite supérieure.
    * @param format Numéro du format d'affichage dans l'application. */
  void init_P_MIN(f32 def, f32 min, f32 max, int format=0);
  /** Initialise la puissance maximale.
    * @param def    Valeur par défaut.
    * @param min    Limite inférieure.
    * @param max    Limite supérieure.
    * @param format Numéro du format d'affichage dans l'application. */
  void init_P_MAX(f32 def, f32 min, f32 max, int format=0);
  /** Initialise le choix du calcul de la dérivée.
    * @param type   Type de dérive. */
  void init_TYPE_D(cypid_type_deriv type=D_ECART);
  /** Initialise le flag de calcul de la dérivée.
    * @param def    Valeur par défaut. */
  void init_CALCUL_D(flg def=0);
  /** Initialise le flag de consigne en rampe.
    * @param def    Valeur par défaut. */
  void init_CONS_RAMP(flg def=0);
  /** Initialise le flag d'inhibition de l'integrale en rampe.
    * @param def    Valeur par défaut. */
  void init_NO_I_RAMP(flg def=0);

  QString group() { return mGroup; }

private: // Private methods
  /** Initialisation */
  void init(cypid_type type);

public: // Public attributes
  /** Type de PID. */
  CYS16 *TYPE;
  /** Coefficient proportionnel ou bande proportionnelle. */
  CYF32 *P;
  /** Coefficient intégral. */
  CYF32 *I;
  /** Facteur intégral/dérivée. */
  CYF32 *K;
  /** Puissance minimale. */
  CYF32 *P_MIN;
  /** Puissance maximale. */
  CYF32 *P_MAX;
  /** Type de dérive. */
  CYS16 *TYPE_D;
  /** Flag de calcul de la dérivée. */
  CYFlag *CALCUL_D;
  /** Flag de consigne en rampe. */
  CYFlag *CONS_RAMP;
  /** Flag d'inhibition de l'integrale en rampe. */
  CYFlag *NO_I_RAMP;
  /** Flag mode Bande proportionnelle (sinon coeff. proportionnel). */
  CYFlag *BP;


protected: // Protected attributes
  /** Base de données. */
  CYDB *mDB;
  /** Valeurs du pid. */
  t_pid *mValues;
  /** Flag d'activation de bande proportionnelle.
      - false => Coefficient proporionnel.
      - true  => Bande proporionnelle. */
  bool mBP;
  /** Nom du groupe des données relatives. */
  QString mGroup;
  /** Ancien nom utilisé comme préfix des anciens noms de données. */
  QString mOldName;
};

#endif
