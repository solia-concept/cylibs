/***************************************************************************
                          cys8.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYS8_H
#define CYS8_H

// CYLIBS
#include "cynum.h"

/** CYS8 est une donnée entière de 8 bits.
  * @short Donnée entière de 8 bits.
  * @author Gérald LE CLEACH
  */

class CYS8 : public CYNum<s8>
{
public:
  /** Création d'une donnée entière de 8 bits.
    * @param db     Base de données parent. 
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS8(CYDB *db, const QString &name, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 8 bits avec sa valeur.
    * @param db     Base de données parent. 
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS8(CYDB *db, const QString &name, s8 *val, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 8 bits avec sa valeur ainsi que sa valeur par défaut.
    * @param db     Base de données parent. 
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS8(CYDB *db, const QString &name, s8 *val, const s8 def, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 8 bits avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent. 
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS8(CYDB *db, const QString &name, s8 *val, const s8 def, const s8 min, const s8 max, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 8 bits avec les attributs Attr1.
    * @param db    Base de données parent. 
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS8(CYDB *db, const QString &name, CYNum<s8>::Attr1<s8> *attr);

  /** Création d'une donnée entière de 8 bits avec les attributs Attr2.
    * @param db    Base de données parent. 
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS8(CYDB *db, const QString &name, CYNum<s8>::Attr2<s8> *attr);

  /** Création d'une donnée entière de 8 bits avec les attributs Attr3.
    * @param db    Base de données parent. 
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS8(CYDB *db, const QString &name, CYNum<s8>::Attr3<s8> *attr);

  /** Création d'une donnée entière de 8 bits avec les attributs Attr4.
    * @param db    Base de données parent. 
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS8(CYDB *db, const QString &name, CYNum<s8>::Attr4<s8> *attr);
  
  ~CYS8();
};

#endif
