/***************************************************************************
                          cyg7cnt.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyg7cnt.h"

CYG7Cnt::CYG7Cnt(CYDB *db, const QString &name, Attr *attr)
  : CYS32(db, name, attr->format, attr->mode)
{
  mVal   = attr->val;
  mHelp  = attr->help;
  mG7    = attr->grafcet;
  mNo    = attr->no;
  mLabel = tr("Counter %1 of grafcet %2").arg(mNo).arg(mG7);
  mGroup = tr("Grafcet counter");
}

CYG7Cnt::~CYG7Cnt()
{
}
