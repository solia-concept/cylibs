/***************************************************************************
                          cytim.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYTIM_H
#define CYTIM_H

// CYLIBS
#include "cytime.h"

class CYString;

/** CYTim est un timer de maintenance. Sa valeur ne peut être remise à 0.
  * Cependant CYTim est associée à une donnée qui, elle, a sa valeur qui peut
  * être remise à 0. Cette valeur représente la valeur temporaire du timer.
  * @short Compteur de maintenance.
  * @author Gérald LE CLEACH
  */

class CYTim : public CYTime
{
  Q_OBJECT
public:
  /** Attr est la structure de saisie des attributs d'un timer de maintenance. */
  struct Attr
  {
    /** Nom. */
    const QString &name;
    /** Timer. */
    t_cu32 *tim;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Etiquette. */
    QString label;
  };

  /** Création d'un timer.
    * @param db    Base de données parent.
    * @param name  Nom du timer.
    * @param attr  Attributs du timer. */
  CYTim(CYDB *db, const QString &name, Attr *attr);

  /** Création d'un timer.
    * @param db    Base de données parent.
    * @param name  Nom du timer.
    * @param tim   Timer.
    * @param label Etiquette.
    * @param mode  Mode d'utilisation. */
  CYTim(CYDB *db, const QString &name, t_cu32 *tim, QString label, Cy::Mode mode=Sys);
  ~CYTim();

  virtual void setResetData(CYData *data);
protected:
    virtual void init();
};

#endif
