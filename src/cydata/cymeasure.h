/***************************************************************************
                          cymeasure.h  -  description
                             -------------------
    début                  : mar sep 9 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYMEASURE_H
#define CYMEASURE_H

// QT
#include <QDateTime>
#include <QList>
// CYLIBS
#include "cyf32.h"

class CYS8;
class CYS16;
class CYF32;
class CYU32;
class CYTSec;
class CYDateTime;
class CYCommand;
class CYMeasureSetting;

/** Cette donnée représente une valeur physique mesurée. La validité de
  * celle-ci est donnée par un flag de contrôle.
  * @short Mesure physique.
  * @author Gérald LE CLEACH
  */

class CYMeasure : public CYF32
{
  Q_OBJECT
  Q_ENUMS(Uncertain)

public:

  /** Attr est la structure de saisie des attributs d'une mesure. */
  struct Attr
  {
    /** Nom. */
    const QString &name;
    /** Valeur de la donnée. */
    f32 *val;
    /** Flag de validité. */
    flg  *flag;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Etiquette. */
    QString label;
  };

  /** Attr est la structure de saisie des attributs d'une mesure. */
  struct Attr2
  {
    /** Nom. */
    const QString &name;
    /** Valeur de la donnée. */
    f32 *val;
    /** Flag de validité. */
    flg  *flag;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Etiquette. */
    QString label;
  };

  /** AttrMetro est la structure de saisie des attributs d'une mesure pouvant être étalonnée. */
  struct AttrMetro
  {
    /** Nom. */
    const QString &name;
    /** Valeur de la donnée. */
    f32 *val;
    /** Flag de validité. */
    flg  *flag;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Point bas par défaut. */
    f32 ldef;
    /** Point haut par défaut. */
    f32 tdef;
    /** Type d'incertitude. */
    Uncertain mUncertainType;
    /** Valeur d'incertitude. */
    double mUncertainVal;
    /** Etiquette. */
    QString label;
  };

  /** Création d'une Mesure physique.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param flag   Flag de validité.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYMeasure(CYDB *db, const QString &name, f32 *val, flg *flg, int format=0, Mode mode=Sys);

  /** Création d'une Mesure physique.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYMeasure(CYDB *db, const QString &name, Attr *attr);

  /** Création d'une Mesure physique avec affichage repères physique et électrique.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYMeasure(CYDB *db, const QString &name, Attr2 *attr);

  /** Création d'une mesure pouvant être étalonnée en métrologie.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYMeasure(CYDB *db, const QString &name, AttrMetro *attr);

  ~CYMeasure();

  /** Saisie l'étiquette de la donnée. */
  virtual void setLabel(const QString l);
  /** Saisie le groupe d'appartenance de la donnée. */
  virtual void setGroup(const QString g);

  /** @return l'état de la donnée du mode forçage. */
  f32 forcing();

  void initForcing(CYFlag *ctrl);
  void addForcing(CYF32 *output, CYF32 *input, QString desc);
  /* @return la donnée de forçage à l'index \a i. */
  CYF32 *outputForcing(int i) { return mOutputForcing[i]; }
  /* @return la donnée d'initialisation de forçage à l'index \a i. */
  CYF32 *inputForcing(int i) { return mInputForcing[i]; }
  /* @return la description de forçage à l'index \a i. */
  QString *descForcing(int i) { return mDescForcing[i]; }
  /* @return le nombre de forçages. */
  int nbForcing() { return mOutputForcing.count(); }
  /** @return la donnée de contrôle de forçage.  */
  CYFlag *ctrlForcing() { return mCtrlForcing; }

  /*! Initialise le défaut sur min.
      @param desc Description complétée de " (repère physique-repère électrique):\nlibellé"
      @param help Aide par défaut vide.
  */
  virtual CYEvent *initFault( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor defect"), QString from=tr("Measure"), QString refs=0, QString help=0);
  /*! Initialise l'alerte sur max.
      @param desc Description complétée de " (repère physique-repère électrique):\nlibellé"
      @param help Aide par défaut vide.
  */
  virtual CYEvent *initAlert( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor defect"), QString from=tr("Measure"), QString refs=0, QString help=0);

  /** Initialisation des valeurs par défaut du calibrage. */
  virtual void initCaliDef() {}
  /** Initialisation du calibrage. */
  virtual void initCali();
  /** Initialise les textes des données de calibrage. */
  virtual void initCalText();
  /** @return la donnée du seuil haut. */
  CYF32 *top() { return mTop; }
  /** @return la donnée du seuil bas. */
  CYF32 *low() { return mLow; }

  /** @return la donnée du seuil haut de l'Étendue de Mesure. */
  CYF32 *EMtop() { return mEMTop; }
  /** @return la donnée du seuil bas de l'Étendue de Mesure. */
  CYF32 *EMlow() { return mEMLow; }

  /** Saisie les attributs du calibrage.
    * @param db   Base de données des données de calibrage.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  virtual void setCali(CYDB *db, QString date="2000-01-01");
  /** Saisie les attributs du calibrage.
    * @param direct Donnée numérique directe (non calobrée)
    * @param vcal Valeurs de calibrage.
    * @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, CYMeasure *direct, t_calnum32 *vcal, f32 low, f32 top, f32 lowMin, f32 topMax, QString date="2000-01-01");

  /** Saisie l'évènement qui s'active lorsque le calibrage est changé. */
  virtual void setCalibrateChangedEvent(CYEvent *ev);
  /** @return l'évènement qui s'active lorsque le calibrage est changé. */
  virtual CYEvent *calibrateChangedEvent() { return calibrateChanged; }
  /** Saisie l'évènement qui s'active lorsque le calibrage est dépassé. */
  virtual void setCalibrateOutdatedEvent(CYEvent *ev);
  /** @return l'évènement qui s'active lorsque le calibrage est dépassé. */
  virtual CYEvent *calibrateOutdatedEvent() { return calibrateOutdated; }

  /** Saisie les attributs d'étalonnage de métrolologie.
    * @param db       Base de données de métrologie de cette mesure.
    * @param max_pts  Nombre max de points d'étalonnage.
    * @param nb_pts   Nombre de points d'étalonnage en cours.
    * @param points   Pointe sur un tableau de points d'étalonnage.
    */
  virtual void setCalMetro(CYDB *db, int max_pts, s16 *nb_pts, t_cal_metro points[]);

  /** Saisir \a true pour que cette mesure soit étalonnable en métrologie. Sinon elle sera juste vérifiable. */
  void setMetroCalEnable(bool val) { mMetroCalEnable=val; }
  /** @return \a true si cette mesure est étalonnable en métrologie. Sinon elle est juste vérifiable. */
  bool metroCalEnable() { return mMetroCalEnable; }

  /** Saisie les attributs de commande de métrolologie.
    * @param index    Index de métrologie transmis par \a imes au régulateur pour
                      le moyennage d'un nouveau point d'étalonnage.
    * @param imes     Donnée donnant l'index de métrologie au régulateur pour
                      le moyennage d'un nouveau point d'étalonnage.
    * @param tmoy     Temps de moyennage d'un nouveau point d'étalonnage.
    * @param cmoy     Commande de demande de moyennage d'un nouveau point d'étalonnage.
    * @param prec     Donnée donnant la précision du nouveau point d'étalonnage.
    */
  virtual void setFoncMetro(s16 index, CYS16 *imes, CYTSec *tmoy, CYCommand *cmoy, CYFlag *moy_ok, CYF32 *prec);

  /** Initialise les données d'étalonnage de métrolologie. */
  virtual void initCalMetro();
  /** Ajoute une donnée supplémentaire générique en fiche capteur .
    * Celle-ci peut être un entier, un réel, un flag ou une chaîne de caractères.
    * Dans le cas d'un entier de choix prédéfinis renseignés par setChoiceLabel la représentation graphique
    * se fait sous forme ComboBox. */
  virtual void addSensorSheetExtra(int id, CYData *data) { sensorSheetExtra.insert(id, data); }

  /** Force les données d'étalonnage de métrolologie à leurs valeurs constructeur. */
  virtual void setDesignerMetro();
  /** @return \a true si les données d'étalonnage de métrolologie ont été forcées aux valeurs constructeur. */
  virtual bool forcedDesignerMetro();

  /** Saisie les donnnées des valeurs moyennées de métrolologie.
    * @param tmoy Temps de moyennage restant
    * @param mes  Mesure moyennée en unité capteur
    * @param dir  Valeur moyennée directe (non-étalonnée)
  */
  virtual void setMoyMetro(CYTSec *tmoy, CYMeasure *mes, CYMeasure *dir);

  /** Démarrage du moyennage pour un nouveau point d'étalonnage. */
  void metroStartMoy(f32 tmoy);
  /** @return \a true si le moyennage du nouveau point d'étalonnage est terminé. */
  bool metroMoyOk();

  /** @return le nombre max de points d'étalonnage. */
  int metroMaxPoints() { return mMetroMaxPts; }

  /** Saisie le statut d'édition des points d'étalonnage. */
  void setMetroEditing(bool val);
  /** @return \a true si une édition en cours des points d'étalonnage. */
  bool metroEditing() { return mMetroEditing; }

  /** @return le type de voie en chaîne de caractères. */
  virtual QString channelTypeDesc();

  /** @return l'incertitude de la valeur \a val.
      @param unit Valeur retournée en unité capteur
    */
  virtual float uncertainty(float val, bool unit=true);
  /** @return l'incertitude min en unité capteur de la valeur \a val. */
  virtual float uncertaintyMin(float val);
  /** @return l'incertitude max en unité capteur de la valeur \a val. */
  virtual float uncertaintyMax(float val);
  /** Calcul le gain et l'offset de linéarité par rapport à l'étendue de mesure . */
  virtual void calcul_gain_offset(float &gain, float &offset);

  /** Impression de la fiche capteur. */
  virtual void printSensorSheet(QPainter *p, QRectF *r);
  /** Impression information étalonnage. */
  virtual void printInfoCal(QPainter *p, QRectF *r);
  /** @return une chaîne de caractères formatée résumant les actions de métrologie du capteur. */
  virtual QString metroSummary(bool editing);
  virtual bool designerCal();
  virtual bool notCal();

  /** Envoie le calibrage au RN. */
  void sendCal();

  virtual void initSetting(CYDB *db, flg *enable, f32 *top, f32 *low, bool cal, bool ct, bool def_enable=false);
  virtual void initSetting(CYDB *db, flg *enable, f32 *top, f32 *low, s16 *max_adc, s16 *min_adc, bool cal, bool ct, bool def_enable=false);
  virtual void initSetting(CYDB *db, flg *enable, f32 *top, f32 *low, s32 *max_adc, s32 *min_adc, bool cal, bool ct, bool def_enable=false);
  /** Saisie le type de calibrage d'une mesure paramétrable. */
  virtual void setTypeCal(int type) { Q_UNUSED(type); }
  /** @return le type de calibrage d'une mesure paramétrable. */
  virtual int typeCal() { return 0; }
  bool editFormat();
  bool editSupervision();

  /** @return \a true si la mesure est active. Une mesure peut-être désactivée si celle-ci est paramétrable.
   * @see initSetting. */
  virtual bool isEnabled();
  /** Calibrage auto du capteur. */
  CYFlag *sensorCalAuto;
  /** Constructeur capteur. */
  CYString *sensorDesigner;
  /** Type de capteur. */
  CYString *sensorPN;
  /** Numéro de série du capteur. */
  CYString *sensorSN;
  /** Type d'incertitude. */
  CYS8 *sensorUT;
  /** Incertitude maxi de l'étendue de mesure. */
  CYF32 *sensorUEM;
  /** Incertitude en unité capteur. */
  CYF32 *sensorUUC;
  /** Incertitude en %. */
  CYF32 *sensorUPC;
  /** Environnement pour indiquer par exemple le fluide. */
  CYString *sensorEnv;
  /** Périodicité d'étalonnage. */
  CYU32 *sensorPeriod;
  /** Date du dernier calibrage. */
  CYDateTime *calibrateLast;
  /** Date de la dernier vérification. */
  CYDateTime *calibrateLastCtrl;
  /** Date du prochain calibrage/vérification. */
  CYDateTime *calibrateNext;
  /** Flag d'activation de surveillance date de calibrage. */
  CYFlag *calibrateDateCtrl;
  /** Note de calibrage. */
  CYString *calibrateNoting;
  /** Calibrage changé. */
  CYEvent *calibrateChanged;
  /** Calibrage dépassé. */
  CYEvent *calibrateOutdated;
  /** Opérateur d'étalonnage. */
  CYString *calibrateOperator;
  /** Référence étalon. */
  CYString *calibrateRefEquip;
  /** Procédure d'étalonnage. */
  CYString *calibrateProcedure;
  /** Commentaire d'étalonnage. */
  CYString *calibrateComment;
  /** Flag de validité du calibrage. */
  CYFlag *calibrateOk;
  /** Dictionnaire de données supplémentaires génériques en fiche capteur. */
  QHash<int, CYData*> sensorSheetExtra;

  /** Temps de moyennage restant */
  CYTSec *metroMoyTime;
  /** Valeur moyennée en unité capteur */
  CYMeasure *metroMoyMES;
  /** Valeur moyennée direct sortie capteur */
  CYMeasure *metroMoyDIR;
  /** Donnée de temps de moyennage d'un nouveau point d'étalonnage. */
  CYTSec *metroTMoy;
  /** Fichier du dernier PV de métrologie. */
  CYString *metroPV;

  /** Base données de calibrage. */
  CYDB *calDB;
  /** Base données de calibrage pour métrologie. */
  CYDB *metroDB;

  /** Nombre de points d'étalonnage réalisés. */
  CYS16 *metroNb;

  /** Max adc 16bits. */
  CYS16 * mMaxAdc16;
  /** Min adc 16bits. */
  CYS16 * mMinAdc16;

  /** Max adc 32bits. */
  CYS32 * mMaxAdc32;
  /** Min adc 32bits. */
  CYS32 * mMinAdc32;

  /** @return le seuil bas par défaut. */
  f32 lowDef() { return mLdef; }
  /** @return le seuil haut par défaut. */
  f32 topDef() { return mTdef; }

public slots: // Public slots
  /** Saisie de la valeur en mode forçage. */
  void setForcing(f32 val);
  /** Saisie le type de contôle du mode forçage.
    * @param val  Si = 0 alors le mode forçage contrôle directement
    *             la sortie, sinon force la consigne. */
  void setCtrlForcing(int val);

  void initForcingValue();

  virtual void setCalibrateChanged();
  virtual void setCalibrateControlled();
  virtual void setCalibrateNext();
  virtual void ctrlCalibrateDate();

  virtual void updateFormat();
  virtual void updateSensor();

signals:
  /** Emis à chaque changement du type de contôle du mode forçage.
    * @param val  Si = 0 alors le mode forçage contrôle directement
    *             la sortie, sinon force la consigne. */
  void ctrlForcing(int val);
  /** Emis lorsque le calibrage a changé. **/
  void calibratingChanged();

private: // Private methods
  /** Initialise par défaut. */
  void init();
  /** Initilaise la sortie en mode forçage. */
  void initForcing();

protected: // Protected attributes
  /* Données de forçage. */
  QHash<int, CYF32*> mOutputForcing;
  /* Données d'initialisation de forçage. */
  QHash<int, CYF32*> mInputForcing;
  /* Descriptions de forçage. */
  QHash<int, QString*> mDescForcing;

  /** Donnée de contrôle de forçage.  */
  CYFlag *mCtrlForcing;

  /** Donnée du seuil haut. */
  CYF32 *mTop;
  /** Donnée du seuil bas. */
  CYF32 *mLow;

  /** Seuil bas par défaut. */
  f32 mLdef;
  /** Seuil haut par défaut. */
  f32 mTdef;

  /** Donnée Seuil bas étendue de mesure par défaut. */
  CYF32 *mEMLow;
  /** Donnée Seuil haut étendue de mesure par défaut. */
  CYF32 *mEMTop;

  /** Seuil bas étendue de mesure par défaut. */
  f32 mEMLdef;
  /** Seuil haut étendue de mesure par défaut. */
  f32 mEMTdef;

  /** Vaut \a true si cette mesure est disponible en métrologie. */
  bool mMetroEnable;
  /** Vaut \a true si cette mesure est étalonnable en métrologie. Sinon elle est juste vérifiable. */
  bool mMetroCalEnable;
  /** Nombre max de points d'étalonnage */
  int mMetroMaxPts;

  /** Index de métrologie transmis au régulateur par \a mMetroIMoy au régulateur pour le moyennage d'un nouveau point d'étalonnage. */
  s16 mMetroIndex;
  /** Donnée donnant l'index de métrologie au régulateur pour le moyennage d'un nouveau point d'étalonnage. */
  CYS16  *mMetroIMes;
  /** Commande de demande de moyennage d'un nouveau point d'étalonnage. */
  CYCommand *mMetroCMoy;
  /** Moyennage du nouveau point d'étalonnage terminé. */
  CYFlag *mMetroMoyOk;
  /** Donnée donnant la précision du nouveau point d'étalonnage. */
  CYF32 *mMetroMoyPrec;
  /** Statut d'édition des points d'étalonnage. */
  bool mMetroEditing;
  /** Statut de forçage des données d'étalonnage de métrolologie en valeurs constructeur. */
  bool mMetroDesigner;

  /** Type d'incertitude par défaut. */
  Uncertain mUncertainTypeDef;
  /** Valeur d'incertitude par défaut. */
  double mUncertainValDef;

  /** Liste des données de métrologie */
  QList<CYData*> metroList;

  /** Date d'initialisation du dernier calibrage (ex 2013-01-31). */
  QString mInitCalLast;
  /** Mémorisation temporaire de la date du dernier calibrage. */
  QDateTime mMemCalLast;
  /** Mémorisation temporaire de la date de la dernier vérification. */
  QDateTime mMemCalLastCtrl;
};

#endif
