/***************************************************************************
                          cytim.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cytim.h"

// CYLIBS
#include "cystring.h"

CYTim::CYTim(CYDB *db, const QString &name, Attr *attr)
  : CYTime(db, name, &(attr->tim->val), Cy::HOUR_MIN_SEC, BASE_SEC, attr->mode)
{
  mLabel = attr->label;

  mPartialData = new CYTime(db, QString("TMP_%1").arg(name), &(attr->tim->tmp), Cy::HOUR_MIN_SEC, BASE_SEC, mMode);
  mPartialData->setLabel(mLabel);

  mNoteData = new CYString(db, QString("NOTE_%1").arg(name));
  mNoteData->setLabel(mLabel);

  init();
}

CYTim::CYTim(CYDB *db, const QString &name, t_cu32 *tim, QString label, Cy::Mode mode)
  : CYTime(db, name, &(tim->val), Cy::HOUR_MIN_SEC, BASE_SEC, mode)
{
  mLabel = label;

  mPartialData = new CYTime(db, QString("TMP_%1").arg(name), &(tim->tmp), Cy::HOUR_MIN_SEC, BASE_SEC, mMode);
  mPartialData->setLabel(mLabel);

  mNoteData = new CYString(db, QString("NOTE_%1").arg(name));
  mNoteData->setLabel(mLabel);

  init();
}

CYTim::~CYTim()
{
}

/*! Saisie la donnée de RAZ.
    \fn CYTim::setResetData(CYData *data)
 */
void CYTim::setResetData(CYData *data)
{
  mResetData = data;
}


/*!
    \fn CYTim::init()
 */
void CYTim::init()
{
  mGroup = tr("Maintenance")+':'+tr("Timer");
  mHelp  = tr("Timer used for maintenance");
  
  mPartialData->setGroup(mGroup+':'+tr("Partial"));
  mPartialData->setHelp(mHelp);
  
  mNoteData->setGroup(mGroup+':'+tr("Note"));
  mNoteData->setHelp(mHelp);
  
  mGroup += ':'+tr("Total");
}
