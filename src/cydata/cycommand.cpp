/***************************************************************************
                          cycommand.cpp  -  description
                             -------------------
    début                  : jeu jun 19 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cycommand.h"
#include "cyevent.h"

CYCommand::CYCommand(CYDB *db, const QString &name, flg *val, Mode mode)
  : CYFlag(db, name, val, 0, mode)
{
  mWithEvent = false;
  init();
}

CYCommand::CYCommand(CYDB *db, const QString &name, Attr1 *attr)
  : CYFlag(db, name, attr->val, 0, attr->mode)
{
  mLabel = attr->label;
  setHelp(attr->help);
  mWithEvent = false;
  init();
}

CYCommand::CYCommand(CYDB *db, const QString &name, flg *val, Attr2 *attr)
  : CYFlag(db, name, val, 0, attr->mode)
{
  mLabel = attr->label;
  setHelp(attr->help);
  mWithEvent = false;
  init();
}

CYCommand::CYCommand(CYDB *db, const QString &name, flg *val, Attr3 *attr)
  : CYFlag(db, name, val, 0, attr->mode)
{
  mLabel = attr->label;
  setHelp(attr->help);
  mWithEvent = attr->withEvent;
  init();
}

CYCommand::~CYCommand()
{
}

void CYCommand::init()
{
  mIsFlag    = true;
  mIsNum     = false;
  mMin = false;
  mMax = true;
  connect( mDB, SIGNAL( datasWrited() ), this, SLOT( postWrited() ) );
  
  if ( (mMode!=Monostable) && (mMode!=Bistable) )
    CYFATALDATA(objectName())
  *mVal = false;
}

bool CYCommand::writeData()
{
  if (CYFlag::writeData())
  {
    postWrited();
    return true;
  }
  return false;
}


bool CYCommand::state()
{
  return ((bool)val());
}

void CYCommand::setOn()
{
  setVal(true);
  writeData();
}

void CYCommand::setOff()
{
  if ( mMode == Monostable )
    return;     
  setVal(false);
  writeData();
}

QString CYCommand::physicalType()
{
  return tr("Command");
}

/*! Appelé après l'écriture réseau
    \fn CYCommand::postWrited()
 */
void CYCommand::postWrited()
{
  if ( mMode != Monostable)
    return;
  setVal(false);
}


/*! \a return \a true si la commande génère un évènement.
    \fn CYCommand::withEvent()
 */
bool CYCommand::withEvent()
{
  return mWithEvent;
}


/*! Saisie l'évènement généré par la commande.
    \fn CYCommand::setEvent( CYEvent *event )
 */
void CYCommand::setEvent( CYEvent *event )
{
  mEvent = event;
}


/*! @return l'évènement généré par la commande.
    \fn CYCommand::event()
 */
CYEvent *CYCommand::event()
{
  return mEvent;
}
