/***************************************************************************
                          cyct.cpp  -  description
                             -------------------
    begin                : mar avr 13 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyct.h"

// CYLIBS
#include "cycore.h"
#include "cyflag.h"
#include "cyf32.h"
#include "cyeventsmanager.h"
#include "cyeventslist.h"

CYCT::CYCT(CYDB *db, const QString &name, int idNet, t_ct *values, flg fl_arret_ale_def, flg fl_arret_def_def, QString group, int format, QString settings)
  : CYDatasGroup(db, name, idNet, group, settings)
{
  mAlerteStop = new CYFlag(db, QString("%1_FL_ARRET_ALE").arg(name), &values->fl_arret_ale, fl_arret_ale_def);
  mFaultStop = new CYFlag(db, QString("%1_FL_ARRET_DEF").arg(name), &values->fl_arret_def, fl_arret_def_def);
  mFormat = format;
  init();
}

CYCT::~CYCT()
{
}

void CYCT::init()
{
  if (CYCT *ct = core->ct(objectName(), false))
    CYFATALTEXT(QString("STR1: %1 STR2: %2 => NOM DE MOUVEMENT '%3' EXISTE DEJA DANS LA BASE DES CONTROLES").arg(ct->db()->objectName()).arg(mDB->objectName()).arg(objectName()))
  core->addCT(objectName(), this);
}

void CYCT::initReading(CYDB *db, f32 *alert, f32 *fault)
{
  mAlertReading = new CYF32(db, QString("%1_ALERT_READING").arg(objectName()), alert, mFormat);
  mFaultReading = new CYF32(db, QString("%1_FAULT_READING").arg(objectName()), fault, mFormat);
  
  initReading();
}

void CYCT::initReading(CYDB *db, AttrReading1 *attr)
{
  mAlertReading = new CYF32(db, QString("%1_ALERT_READING").arg(objectName()), attr->alert, mFormat);
  mFaultReading = new CYF32(db, QString("%1_FAULT_READING").arg(objectName()), attr->fault, mFormat);
  
  initReading();
}

void CYCT::initReading()
{
  mAlertReading->setLabel(tr("Alert value"));
  mAlertReading->setGroup(mGroup);

  mFaultReading->setLabel(tr("Fault value"));
  mFaultReading->setGroup(mGroup);
}


CYEvent *CYCT::initFaultAlertMax(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(faultReading()->group(false)+":\n");
  return event;
}


CYEvent *CYCT::initFaultAlertMin(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setPrefix(faultReading()->group(false)+":\n");
  return event;
}


CYEvent *CYCT::initFaultMax(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  Q_UNUSED(refs)
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, faultReading()->objectName(), help);
  event->setPrefix(faultReading()->group(false)+":\n");
  return event;
}


CYEvent *CYCT::initFaultMin(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  Q_UNUSED(refs)
  CYEvent *event = initFault(generator, name, val, helpEnd, enable, proc, desc, from, faultReading()->objectName(), help);
  event->setPrefix(faultReading()->group(false)+":\n");
  return event;
}


CYEvent *CYCT::initAlertMax(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  Q_UNUSED(refs)
  CYEvent *event = initAlert(generator, name, val, helpEnd, enable, proc, desc, from, alertReading()->objectName(), help);
  event->setPrefix(alertReading()->group(false)+":\n");
  return event;
}


CYEvent *CYCT::initAlertMin(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  Q_UNUSED(refs)
  CYEvent *event = initAlert(generator, name, val, helpEnd, enable, proc, desc, from, alertReading()->objectName(), help);
  event->setPrefix(alertReading()->group(false)+":\n");
  return event;
}

void CYCT::setFormatData(QString name)
{
  mAlertReading->setFormatData(name);
  mFaultReading->setFormatData(name);
}

void CYCT::setFormatData(CYData *data)
{
  mAlertReading->setFormatData(data);
  mFaultReading->setFormatData(data);
}
