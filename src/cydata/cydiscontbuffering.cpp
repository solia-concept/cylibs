#include "cydiscontbuffering.h"

// CYLIBS
#include "cyu32.h"

CYDiscontBuffering::CYDiscontBuffering(QObject *parent, CYU32 *Index, CYU32 *number)
  : QObject(parent)
{
  mIndex = Index;
  mIndex->db()->addDiscontBuffering(this);
  mNumber = number;
  mFilled = false;
}

void CYDiscontBuffering::addData(CYData *data)
{
  if (!mData.count())
    mIndex->setMax(data->bufferSize()); // récupération taille buffer
  mData.append(data);
}

void CYDiscontBuffering::ctrlFilled()
{
  if (!mData.count())
    return;

  uint cpt = 0;
  uint num = 0;
  QListIterator<CYData *> it(mData);
  while (it.hasNext())
  {
    CYData *data = it.next();
    if ( data->discontBufferState==CYData::Filled )
    {
      if ((cpt>0) && (data->discontBufferNumber()!=num))
      {
        CYWARNINGTEXT(QString("num %1 != %2").arg(data->discontBufferNumber()).arg(num));
        return;
      }
      cpt++;
      num=data->discontBufferNumber();
    }
  }
  if (cpt && !mFilled)
  {
    if (cpt==(uint)mData.count())
    {
      mFilled=true;
      emit filled(num);
    }
    else
      CYWARNINGTEXT(QString("cpt %1 != %2").arg(cpt).arg(mData.count()));
  }
}

uint CYDiscontBuffering::sampleIndex(int str_fifo, int i)
{
  return mIndex->sample(str_fifo, i);
}

uint CYDiscontBuffering::sampleNumber(int str_fifo, int i)
{
  return mNumber->sample(str_fifo, i);
}

void CYDiscontBuffering::initDiscontBuffer()
{
  if (!mData.count())
    return;

  QListIterator<CYData *> it(mData);
  while (it.hasNext())
  {
    CYData *data = it.next();
    data->releaseDiscontBuffer();
  }
  mFilled = false;
}

void CYDiscontBuffering::releaseDiscontBuffer()
{
  if (!mData.count())
    return;

  QListIterator<CYData *> it(mData);
  while (it.hasNext())
  {
    CYData *data = it.next();
    data->releaseDiscontBuffer();
  }
  mFilled = false;
}

