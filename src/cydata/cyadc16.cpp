/***************************************************************************
                          cyadc16.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyadc16.h"

// CYLIBS
#include "cybool.h"

CYAdc16::CYAdc16(CYDB *db, const QString &name, CYAna *ana, CYAna::Attr16 *attr, int c)
  : CYS16(db, name, 0),
    mCard(c),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mAna   = ana;
  mVal   = attr->adc;
  mLabel = attr->label;
  mElec  = attr->elec;
  mPhys  = attr->phys;
  setNbDec(0);
  
  mEnableColor  = Qt::cyan;
  mDisableColor = Qt::red;
}

CYAdc16::CYAdc16(CYDB *db, const QString &name, CYAna *ana, CYAna::Attr16ADC *attr, int c)
  : CYS16(db, name, 0),
    mCard(c),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mAna   = ana;
  mVal   = attr->adc;
  mLabel = attr->label;
  mElec  = attr->elec;
  mPhys  = attr->phys;
  mLDef  = attr->adc_min;
  mTDef  = attr->adc_max;

  setNbDec(0);

  mEnableColor  = Qt::cyan;
  mDisableColor = Qt::red;
}

CYAdc16::CYAdc16(CYDB *db, const QString &name, CYAna *ana, s16 *val, const char *elec, const char *phys, Cy::Mode mode, QString label, int c)
  : CYS16(db, name, 0, mode),
    mCard(c),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mAna   = ana;
  mVal   = val;
  mLabel = label;
  mElec  = elec;
  mPhys  = phys;
  setNbDec(0);

  mEnableColor  = Qt::cyan;
  mDisableColor = Qt::red;
}

CYAdc16::CYAdc16(CYDB *db, const QString &name, CYAna *ana, CYAna::Attr16Metro *attr, int c)
  : CYS16(db, name, 0),
    mCard(c),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mAna   = ana;
  mVal   = attr->adc;
  mLabel = attr->label;
  mElec  = attr->elec;
  mPhys  = attr->phys;
  mLDef  = attr->adc_min;
  mTDef  = attr->adc_max;

  setNbDec(0);

  mEnableColor  = Qt::cyan;
  mDisableColor = Qt::red;
}

CYAdc16::~CYAdc16()
{
}

void CYAdc16::setPtrCal(t_calana16 *vcal)
{
  mTop->setPtrVal(&(vcal->adcmax));
  mLow->setPtrVal(&(vcal->adcmin));
}

void CYAdc16::changeLevel(f32 tmax, f32 tmin, f32 lmax, f32 lmin)
{
  mTmax = tmax;
  mTmin = tmin;
  mLmax = lmax;
  mLmin = lmin;
}

void CYAdc16::setCtrCal()
{
  float ecf;

  //========== Limites de saisie ===============
  ecf = (mTop->def() - mLow->def());
  mTop->setMax(mTop->def() + (s16) (ecf * (mTmax-100.0)/100.0 + 0.5)); //102 % si mTmax = 102.0 % //MAX ADC haut = valeur ADC haute constru + 2% écart
  mTop->setMin(mTop->def() + (s16) (ecf * (mTmin-100.0)/100.0 + 0.5)); // 70 % si mTmin =  70.0 % //MIN ADC haut = valeur ADC haute constru -30% écart
  mLow->setMax(mLow->def() + (s16) (ecf * mLmax/100.0 + 0.5));         // 30 % si mLmax =  30.0 % //MAX ADC bas  = valeur ADC basse constru +30% écart
  mLow->setMin(mLow->def() + (s16) (ecf * mLmin/100.0 + 0.5));         // -2 % si mLmin =  -2.0 % //MIN ADC bas  = valeur ADC basse constru - 2% écart

  mTop->forceAutoMinMax(false);
  mLow->forceAutoMinMax(false);
}


