//
// C++ Interface: cygenericmeasure
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2015
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMEASURESETTING_H
#define CYMEASURESETTING_H

// CYLIBS
#include "cydatasgroup.h"
#include "cymeasure.h"

class CYString;
class CYS16;
class CYF32;
class CYAna;
class CYFlag;

/**
  @short Paramétrage de mesure
  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMeasureSetting : public CYDatasGroup
{
  Q_OBJECT
  friend class CYMeasure;

public:
  /** Création d'une Mesure physique.
    * @param db     Base de données parent.
    * @param name   Mesure paramétrée.
    * @param cal    Type de calibrage paramétrable.
    * @param ct     Active/désactive l'édition de la surveillance
    */
  CYMeasureSetting(CYDB *db, CYMeasure *mes, bool cal, bool ct);

  ~CYMeasureSetting();

  void init(flg *enable, f32 *top, f32 *low, bool def_enable=false);
  bool editFormat();
  bool editSupervision();
  bool editCalType();

  virtual void setSameFormatData(QByteArray dataName);
  virtual void setSameFormatData( CYF32 *data );
  bool isEnabled();

  /** @return la donnée d'activation de la voie générique. */
  CYFlag * enableData() { return mEnable; }

  /** Ajoute un type de calibrage paramétrable.
    * @param def place de de type comme valeur par défaut. */
  virtual void addTypeCal(int type, QString label, bool def=false);
  /** Saisie le type de calibrage à paramétrer. */
  virtual void setTypeCal(int type);
  /** @return le type de calibrage paramétré. */
  virtual int typeCal();

  virtual void acquit();

public slots:
  virtual void updateFormat();
  virtual void updateSensor();

  virtual void setEditFormat( bool val );
  virtual void setEditSupervision( bool val );

  signals: // Signals
  void formatUpdated();

protected:
  virtual void init();

protected:
  CYMeasure *mMes;
  CYString * mUnit;
  CYString * mLabel;

  /** Donnée d'activation de la voie générique. */
  CYFlag * mEnable;
  /** Donnée du point haut par défaut. */
  CYF32 * mTDef;
  /** Donnée du point bas par défaut. */
  CYF32 * mLDef;
  /** Donnée du type de calibrage. */
  CYS16 * mCal;

  /** Alerte de changement de capteur. */
  CYEvent *mSensorChanged;

  bool mEditFormat;
  bool mEditSupervision;
  bool mEditCalType;
  QList<CYF32 *> mSameFormatDatas;
};

#endif
