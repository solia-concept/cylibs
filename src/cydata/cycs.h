//
// C++ Interface: cycs
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYCS_H
#define CYCS_H

// CYLIBS
#include "cydatasgroup.h"

class CYU32;
class CYCommand;
class CYString;

/** @short Compteur Secondes de maintenance.
    @author Gérald LE CLEACH
*/
class CYCS : public CYDatasGroup
{
  Q_OBJECT

public:
  /** Structure de saisie des attributs d'un compteur de maintenance (u32). */
  struct Attr1
  {
    /** Nom. */
    const QString &name;
    /** Index réseau correspondant à l'index des valeurs dans différentes structures réseau. */
    int idNet;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Groupe du compteur. */
    QString group;
    /** Etiquette du compteur. */
    QString label;
  };

  /** Création d'un compteur horaire de maintenance (u32).
    * @param db     Base de données parent.
    * @param name   Nom du compteur.
    * @param val    Valeur de la donnée (total et partielle).
    * @param attr   Attributs du compteur de cycles. */
  CYCS(CYDB *db, const QString &name, t_cu32 *cnt, Attr1 *attr);

  ~CYCS();

  void initResetData(CYDB *db, flg *val);
  CYCommand * resetData();

  CYU32 *thresholdData() { return mThresholdData; }
  CYEvent *ctrlEvent;
  void setCtrlEvent(CYEvent *ev);

public slots:
    void ctrlPartial();

protected:
    virtual void init();

  /** Donnée du compteur total. */
  CYU32 *mTotalData;
  /** Donnée du compteur partiel. */
  CYU32 *mPartialData;
  /** Donnée seuil d'alerte du compteur partiel. */
  CYU32 *mThresholdData;
  /** Commande de remise à 0 du compteur partielle. */
  CYCommand *mResetData;
  /** Donnée pouvant contenir un note de maintenance.. */
  CYString *mNoteData;
};

#endif
