/***************************************************************************
                          cywordcommand.cpp  -  description
                             -------------------
    début                  : jeu jun 19 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cywordcommand.h"

CYWordCommand::CYWordCommand(CYDB *db, const QString &name, u16 *val, Mode mode)
  : CYWord(db, name, val, 0, mode)
{
  if ((mode!=Monostable) && (mode!=Bistable))
    CYFATALDATA(name)

  *mVal = 0;
}

CYWordCommand::~CYWordCommand()
{
}

bool CYWordCommand::state(const unsigned short no)
{
  return ((bool)bit(no));
}

void CYWordCommand::setOn()
{
  writeData();
  if (mMode==Monostable)
    setVal(0);
}

void CYWordCommand::setOn(const unsigned short val)
{
  setVal(val);
  writeData();
  if (mMode==Monostable)
    setVal(0);
}

void CYWordCommand::setCmdOn(const unsigned short no)
{
  setBit(no, true);
  writeData();
  if (mMode==Monostable)
    setBit(no, false);
}

void CYWordCommand::setCmdOff(const unsigned short no)
{
  if (mMode==Monostable)
    return;
  setBit(no, false);
  writeData();
}
