/***************************************************************************
                          cyu8.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyu8.h"

CYU8::CYU8(CYDB *db, const QString &name, int format, Mode mode)
  : CYNum<u8>(db, name, format, mode, VU8)
{
}

CYU8::CYU8(CYDB *db, const QString &name, u8 *val, int format, Mode mode)
  : CYNum<u8>(db, name, val, format, mode, VU8)
{
}

CYU8::CYU8(CYDB *db, const QString &name, u8 *val, const u8 def, int format, Mode mode)
  : CYNum<u8>(db, name, val, def, format, mode, VU8)
{
}

CYU8::CYU8(CYDB *db, const QString &name, u8 *val, const u8 def, const u8 min, const u8 max, int format, Mode mode)
  : CYNum<u8>(db, name, val, def, min, max, format, mode, VU8)
{
}

CYU8::CYU8(CYDB *db, const QString &name, CYNum<u8>::Attr1<u8> *attr)
  : CYNum<u8>(db, name, attr, VU8)
{
}

CYU8::CYU8(CYDB *db, const QString &name, CYNum<u8>::Attr2<u8> *attr)
  : CYNum<u8>(db, name, attr, VU8)
{
}

CYU8::CYU8(CYDB *db, const QString &name, CYNum<u8>::Attr3<u8> *attr)
  : CYNum<u8>(db, name, attr, VU8)
{
}

CYU8::CYU8(CYDB *db, const QString &name, CYNum<u8>::Attr4<u8> *attr)
  : CYNum<u8>(db, name, attr, VU8)
{
}

CYU8::~CYU8()
{
}


int CYU8::nbDigit(Cy::NumBase base)
{
  return toString(max(), false, true, base).length();
}


QString CYU8::toString(bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  return toString(val(), withUnit, ctrlValid, base);
}


/*! @return la valeur \a val en chaîne de caractères suivant le format et l'unité.
    \fn CYU8::toString(double val, bool withUnit, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData)
 */
QString CYU8::toString(double val, bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  Cy::NumBase b;
  if ( base!=Cy::BaseData )
    b = base;
  else
    b = mNumBase;

  QString suffix;
  if (withUnit)
    suffix.append(" "+unit());

  if (ctrlValid && !isValid())
    return QString("%1%2").arg(stringDef()).arg(suffix);
  else
  {
    QString num;
    if (mFormat->isExponential())
      return QString("%1e%2%3").arg(mantissa(val), 0, 'f', nbDec()).arg(exponent(val)).arg(suffix);
    else if ( b!=Cy::Decimal )
    {
      QString num = QString("%1").arg((u8)val, 0, b).toUpper();
      QString zero;
      QString maxTxt = QString("%1").arg((u8)max(), 0, b).toUpper();
      zero.fill('0', maxTxt.length()-num.length());
      QString txt = QString("%1%2%3").arg(zero).arg(num).arg(suffix);
      return txt;
    }
    else
      return QString("%1%2").arg(val, 0, 'f', 0).arg(suffix);
  }
}
