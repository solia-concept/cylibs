/***************************************************************************
                          cypid.cpp  -  description
                             -------------------
    begin                : jeu avr 22 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cypid.h"


// CYLIBS
#include "cyf32.h"
#include "cys16.h"
#include "cyflag.h"

CYPID::CYPID(CYDB *db, const QString &name, t_pid *values, bool bp, QString group, cypid_type type, const QString &oldName)
  : QObject(db),
    mDB(db),
    mValues(values),
    mBP(bp),
    mGroup(group)
{
  setObjectName(name);
  mOldName = oldName;

  init(type);
}

CYPID::~CYPID()
{
}

void CYPID::init(cypid_type type)
{
  P = 0;
  I = 0;
  K = 0;
  P_MIN = 0;
  P_MAX = 0;
  CALCUL_D  = 0;
  CONS_RAMP = 0;
  NO_I_RAMP = 0;

  if (type!=PID_TYPE_FAULT)
  {
    QString suffix = "_TYPE";
    QString name= objectName()+suffix;
    if (!mOldName.isEmpty())
      mDB->changeDataName(mOldName+suffix, name);
    TYPE = new CYS16(mDB, name, &mValues->type, type, PID_TYPE_FAULT, type);
    TYPE->setMax(PID_TYPE_I);
    TYPE->setDef(type);
    TYPE->designer();
    TYPE->setLabel(tr("Type"));
    TYPE->setChoiceLabel(PID_TYPE_P     , tr("Proportional"));
    TYPE->setChoiceLabel(PID_TYPE_PI    , tr("Proportional Integral"));
    TYPE->setChoiceLabel(PID_TYPE_PD    , tr("Proportional Derivative"));
    TYPE->setChoiceLabel(PID_TYPE_PID   , tr("Proportional Integral Derivative"));
    TYPE->setChoiceLabel(PID_TYPE_I     , tr("Integral"));
    TYPE->setGroup(mGroup);
  }
}

void CYPID::init_P(f32 def, f32 min, f32 max, int format)
{
  QString suffix = "_P";
  QString name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  if (mBP)
  {
    P = new CYF32(mDB, name, &mValues->P, def, min, max, format);
    P->setLabel(tr("Proportional band coefficient"));
    P->setLimitedDecrease(-50.0);
  }
  else
  {
    P = new CYF32(mDB, name, &mValues->P, def, min, max);
    P->setLabel(tr("Proportional coefficient"));
    P->setLimitedIncrease(50.0);
  }
  P->addNote(tr("If 0, the proportional control won't be active (It can work in integral mode)"));
  P->addNote(tr("This control is a mixt P.I.D."));
  P->setGroup(mGroup);

  suffix = "_BP";
  name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  BP = new CYFlag(mDB, name, &mValues->fl_bp, mBP);
  BP->setLabel(tr("Proportional band"));
}

void CYPID::init_I(f32 def, f32 min, f32 max, int format)
{
  QString suffix = "_I";
  QString name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  I = new CYF32(mDB, name, &mValues->I, def, min, max, format);
  I->setLabel(tr("Integral coefficient"));
  I->addNote(tr("If 0, the integral control won't be in operation."));
  I->addNote(tr("This control is a mixt P.I.D."));
  I->setGroup(mGroup);
}

void CYPID::init_K(f32 def, f32 min, f32 max, int format)
{
  QString suffix = "_K";
  QString name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  K = new CYF32(mDB, name, &mValues->I_D, def, min, max, format);
  K->setLabel(tr("Integral/derivative ratio"));
  K->addNote(tr("According to Ziegler and Nichols the best ratio is 4.0."));
  K->addNote(tr("If 0, the derivative control won't be in operation."));
  K->addNote(tr("This control is a mixt P.I.D."));
  K->setGroup(mGroup);
}

void CYPID::init_P_MIN(f32 def, f32 min, f32 max, int format)
{
  QString suffix = "_P_MIN";
  QString name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  P_MIN = new CYF32(mDB, name, &mValues->p_min, def, min, max, format);
  P_MIN->setLabel(tr("Negative output limit"));
  P_MIN->setHelp(tr("Output limit."));
  P_MIN->setGroup(mGroup);
}

void CYPID::init_P_MAX(f32 def, f32 min, f32 max, int format)
{
  QString suffix = "_P_MAX";
  QString name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  P_MAX = new CYF32(mDB, name, &mValues->p_max, def, min, max, format);
  P_MAX->setLabel(tr("Positive output limit"));
  P_MAX->setHelp(tr("Output limit."));
  P_MAX->setGroup(mGroup);
}

void CYPID::init_TYPE_D(cypid_type_deriv type)
{
  QString suffix = "_TYPE_D";
  QString name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  TYPE_D = new CYS16(mDB, name, &mValues->type_deriv, type);
  TYPE_D->setLabel(tr("Derivative type"));
  TYPE_D->setChoiceLabel(D_ECART   , tr("On the gap"));
  TYPE_D->setChoiceLabel(D_MESURE  , tr("On the measure."));
  TYPE_D->setChoiceLabel(D_CONSIGNE, tr("On the setpoint."));
  TYPE_D->setHelp(QString(tr("If you select:")
          + QString("<table>"
                      "<tr><td>"+TYPE_D->choiceLabel(D_ECART)+":</td> <td>"+
                          tr("This may be useful to avoid overshoot at the ramp end."
                               "The derivative calculation will be done on the gap (setpoint - measure).")+
                      "</td></tr>"
                      "<tr><td>"+TYPE_D->choiceLabel(D_MESURE)+":</td> <td>"+
                          tr("The derivative calculation will be done on the measure.")+
                      "</td></tr>"
                      "<tr><td>"+TYPE_D->choiceLabel(D_CONSIGNE)+":</td> <td>"+
                          tr("The derivative calculation will be done on the setpoint.")+
                      "</td></tr>"
                    "</table>")));
  TYPE_D->setGroup(mGroup);
}

void CYPID::init_CALCUL_D(flg def)
{
  QString suffix = "_CALCUL_D";
  QString name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  CALCUL_D = new CYFlag(mDB, name, &mValues->fl_derivmes, def);
  CALCUL_D->setLabel(tr("Measure derivative"));
  CALCUL_D->setHelp("",
                   tr("The derivative calculation will be done on the measure. "
                      "The derivative effect is based on the error variation : setpoint – measurement. "
                      "This may be useful to avoid overshoot at the ramp end."),
                   tr("The derivative effect is based on the setpoint variation."));

  CALCUL_D->setGroup(mGroup);
}

void CYPID::init_CONS_RAMP(flg def)
{
  QString suffix = "_CONS_RAMP";
  QString name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  CONS_RAMP = new CYFlag(mDB, name, &mValues->fl_cons_rmp, def);
  CONS_RAMP->setLabel(tr("Ramp on PID setpoint"));
  CONS_RAMP->setHelp("",
                     tr("The PID setpoint will be a ramp form one."),
                     tr("It will be the final value (No ramp)."));
  CONS_RAMP->setGroup(mGroup);
}

void CYPID::init_NO_I_RAMP(flg def)
{
  QString suffix = "_NO_I_RAMP";
  QString name= objectName()+suffix;
  if (!mOldName.isEmpty())
    mDB->changeDataName(mOldName+suffix, name);
  NO_I_RAMP = new CYFlag(mDB, name, &mValues->fl_blq_irmp, def);
  NO_I_RAMP->setLabel(tr("Integral frozen in ramp"));
  NO_I_RAMP->setHelp("",
                     tr("The integral value will frozen in ramp."),
                     tr("This may be useful to avoid overshoot at the ramp end."
                          "The integral value will be modified normally by the PID control."));
  NO_I_RAMP->setGroup(mGroup);
}
