/***************************************************************************
                          cyana.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYANA_H
#define CYANA_H

// CYLIBS
#include "cymeasure.h"

class CYWord;
class CYAdc16;
class CYAdc32;
class CYString;
class CYDateTime;
class CYEvent;

/** CYAna est une entrée/sortie ANA en unité capteur lisible pour
  * l'utilisateur final. Une donnée en points, CYAdc, y est associée.
  * Ces 2 données se rattachent chacune d'elle à 2 données, une pour les
  * paramètres du seuil haut et une autre pour les paramètres du seuil bas.
  * @short E/S ANA.
  * @author Gérald LE CLEACH
  */

class CYAna : public CYMeasure
{
  Q_OBJECT
public:
  /*=================================================
          Contrôle des seuils électriques
                  Boucle 4/20mA
   ________________________________________________
  |    Condition      |   mA    |  Volt   | ADC    |
  |___________________|_________|_________|________|
  | pleine échelle    | 20.5339 | 10.00   | 4095   |
  |        20.00      | 20.00   |  9.74   | 3988.5 |
  |        4          |  4.00   |  1.948  |  797.7 |
  |  Rupture capteur  |  1.00   |  0.487  |  199.4 |
  |   Sans signal     |  0.00   |  0.00   |    0   |
  |___________________|_________|_________|________|
  =================================================*/

  /** Différents types de calibrage disponibles. */
  enum CAL
  {
    /** Aucun calibrage. */
    NO_CAL = 0,
    /** 12bits 4/20 mA sur 487 Ohms. */
    CALU_487,
    /** 12bits 0/20 mA. */
    CALU_0_20MA,
    /** 12bits 4/20 mA. */
    CALU_4_20MA,
    /** 12bits 0/5 Volt. */
    CALU_5V,
    /** 12bits ±5 Volt. */
    CALB_5V,
    /** 12bits 0/10 Volt. */
    CALU_10V,
    /** 12bits ±10 Volt. */
    CALB_10V,
    /** 12bits ±10 mV. */
    CALB_10MV,
    /** 12bits ±50 mV. */
    CALB_50MV,
    /** 12bits CJC carte DB8225  (10mV/°K) en 0-10 V. */
    CALU_CJC,
    /** 12bits CJC carte DB8225  (10mV/°K) en ±0-10 V. */
    CALB_CJC,
    /** Calibrage automate 0..10000. */
    CAL_API,
  };

  /** Attr est la structure de saisie des attributs d'une donnée ANA acquisition 16bits max (avec bit de parité). */
  struct Attr16
  {
    /** Nom. */
    QString name;
    /** Valeur de la donnée. */
    f32 *val;
    /** Valeur de la donnée ADC. */
    s16 *adc;
    /** Flag de validité. */
    flg  *flag;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Point bas par défaut. */
    f32 ldef;
    /** Point haut par défaut. */
    f32 tdef;
    /** Etiquette. */
    QString label;
  };

  /** Attr est la structure de saisie des attributs avec plage ADC d'une donnée ANA acquisition 16bits max (avec bit de parité). */
  struct Attr16ADC
  {
    /** Nom. */
    QString name;
    /** Valeur de la donnée. */
    f32 *val;
    /** Valeur de la donnée ADC. */
    s16 *adc;
    /** Flag de validité. */
    flg  *flag;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** valeur ADC basse par défaut. */
    s16 adc_min;
    /** Valeur ADC haute par défaut. */
    s16 adc_max;
    /** Point bas par défaut. */
    f32 aff_min;
    /** Point haut par défaut. */
    f32 aff_max;
    /** Point bas étendue de mesure par défaut. */
    f32 EM_min;
    /** Point haut étendue de mesure par défaut. */
    f32 EM_max;
    /** Etiquette. */
    QString label;
  };

  /** Attr est la structure de saisie des attributs d'une donnée ANA 32bits max (avec bit de parité). */
  struct Attr32
  {
    /** Nom. */
    QString name;
    /** Valeur de la donnée. */
    f32 *val;
    /** Valeur de la donnée ADC. */
    s32 *adc;
    /** Flag de validité. */
    flg  *flag;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Point bas par défaut. */
    f32 ldef;
    /** Point haut par défaut. */
    f32 tdef;
    /** Etiquette. */
    QString label;
  };

  /** Attr est la structure de saisie des attributs avec plage ADC d'une donnée ANA 32bits max (avec bit de parité). */
  struct Attr32ADC
  {
    /** Nom. */
    QString name;
    /** Valeur de la donnée. */
    f32 *val;
    /** Valeur de la donnée ADC. */
    s32 *adc;
    /** Flag de validité. */
    flg  *flag;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** valeur ADC basse par défaut. */
    s32 adc_min;
    /** Valeur ADC haute par défaut. */
    s32 adc_max;
    /** Point bas par défaut. */
    f32 aff_min;
    /** Point haut par défaut. */
    f32 aff_max;
    /** Point bas étendue de mesure par défaut. */
    f32 EM_min;
    /** Point haut étendue de mesure par défaut. */
    f32 EM_max;
    /** Etiquette. */
    QString label;
  };

  /** Attr est la structure de saisie des attributs d'une donnée ANA 16bits max (avec bit de parité) pouvant être étalonnée. */
  struct Attr16Metro
  {
    /** Nom. */
    QString name;
    /** Valeur de la donnée. */
    f32 *val;
    /** Valeur de la donnée ADC. */
    s16 *adc;
    /** Flag de validité. */
    flg  *flag;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** valeur ADC basse par défaut. */
    s16 adc_min;
    /** Valeur ADC haute par défaut. */
    s16 adc_max;
    /** Point bas par défaut. */
    f32 aff_min;
    /** Point haut par défaut. */
    f32 aff_max;
    /** Point bas étendue de mesure par défaut. */
    f32 EM_min;
    /** Point haut étendue de mesure par défaut. */
    f32 EM_max;
    /** Type d'incertitude. */
    Cy::Uncertain mUncertainType;
    /** Valeur d'incertitude. */
    double mUncertainVal;
    /** Etiquette. */
    QString label;
  };

  /** Attr est la structure de saisie des attributs d'une donnée ANA 32bits max (avec bit de parité) pouvant être étalonnée. */
  struct Attr32Metro
  {
    /** Nom. */
    QString name;
    /** Valeur de la donnée. */
    f32 *val;
    /** Valeur de la donnée ADC. */
    s32 *adc;
    /** Flag de validité. */
    flg  *flag;
    /** Repère électrique. */
    QString elec;
    /** Repère physique. */
    QString phys;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** valeur ADC basse par défaut. */
    s32 adc_min;
    /** Valeur ADC haute par défaut. */
    s32 adc_max;
    /** Point bas par défaut. */
    f32 aff_min;
    /** Point haut par défaut. */
    f32 aff_max;
    /** Point bas étendue de mesure par défaut. */
    f32 EM_min;
    /** Point haut étendue de mesure par défaut. */
    f32 EM_max;
    /** Type d'incertitude. */
    Cy::Uncertain mUncertainType;
    /** Valeur d'incertitude. */
    double mUncertainVal;
    /** Etiquette. */
    QString label;
  };

  /** Forcing est la structure de saisie des attributs d'une donnée ANA en mode forçage. */
  struct Forcing
  {
    /** Nom. */
    QString name;
    /** Valeur mini. */
    f32 min;
    /** Valeur maxi. */
    f32 max;
  };

  /** Cali161 est la structure de saisie simple des attributs du calibrage d'une donnée ANA 16bits max (bit de parité inclu). */
  struct Cali161
  {
    /** Nom de la donnée ANA à calibrer. */
    QString name;
    /** Valeurs de calibrage. */
    t_calana16 *vcal;
    /** Type de calibrage. */
    CAL cal;
  };

  /** Cali162 est la structure de saisie des attributs du calibrage d'une donnée ANA 16bits max (bit de parité inclu)
    * avec les pourcentages de seuil. */
  struct Cali162
  {
    /** Nom de la donnée ANA à calibrer. */
    QString name;
    /** Valeurs de calibrage. */
    t_calana16 *vcal;
    /** Type de calibrage. */
    CAL cal;
    /** Pourcentage seuil haut maximum. */
    f32 tmax;
    /** Pourcentage seuil haut minimum. */
    f32 tmin;
    /** Pourcentage seuil bas maximum. */
    f32 lmax;
    /** Pourcentage seuil bas minimum. */
    f32 lmin;
    /** Pourcentage seuil haut maximum de la donnée ADC. */
    f32 tmaxadc;
    /** Pourcentage seuil haut minimum de la donnée ADC. */
    f32 tminadc;
    /** Pourcentage seuil bas maximum de la donnée ADC. */
    f32 lmaxadc;
    /** Pourcentage seuil bas minimum de la donnée ADC. */
    f32 lminadc;
  };

  /** Cali321 est la structure de saisie simple des attributs du calibrage d'une donnée ANA 32bits max (bit de parité inclu). */
  struct Cali321
  {
    /** Nom de la donnée ANA à calibrer. */
    QString name;
    /** Valeurs de calibrage. */
    t_calana32 *vcal;
    /** Type de calibrage. */
    CAL cal;
  };

  /** Cali322 est la structure de saisie des attributs du calibrage d'une donnée ANA 32bits max (bit de parité inclu)
    * avec les pourcentages de seuil. */
  struct Cali322
  {
    /** Nom de la donnée ANA à calibrer. */
    QString name;
    /** Valeurs de calibrage. */
    t_calana32 *vcal;
    /** Type de calibrage. */
    CAL cal;
    /** Pourcentage seuil haut maximum. */
    f32 tmax;
    /** Pourcentage seuil haut minimum. */
    f32 tmin;
    /** Pourcentage seuil bas maximum. */
    f32 lmax;
    /** Pourcentage seuil bas minimum. */
    f32 lmin;
    /** Pourcentage seuil haut maximum de la donnée ADC. */
    f32 tmaxadc;
    /** Pourcentage seuil haut minimum de la donnée ADC. */
    f32 tminadc;
    /** Pourcentage seuil bas maximum de la donnée ADC. */
    f32 lmaxadc;
    /** Pourcentage seuil bas minimum de la donnée ADC. */
    f32 lminadc;
  };

  /** Création d'une donnée ANA 16bits.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param card  Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAna(CYDB *db, const QString &name, Attr16 *attr, int card=-1, bool input=true);

  /** Création d'une donnée ANA 32bits.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param card  Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAna(CYDB *db, const QString &name, Attr32 *attr, int card=-1, bool input=true);


  /** Création d'une donnée ANA 16bits.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param card  Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAna(CYDB *db, const QString &name, Attr16ADC *attr, int card=-1, bool input=true);

  /** Création d'une donnée ANA 32bits.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param card  Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAna(CYDB *db, const QString &name, Attr32ADC *attr, int card=-1, bool input=true);


  /** Création d'une donnée ANA 16bits max (bit de parité inclu).
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param adc    Valeur de la donnée ADC.
    * @param flg     Flag de validité.
    * @param elec   Repère électrique.
    * @param phys   Repère physique.
    * @param mode   Mode d'affichage.
    * @param format Numéro du format d'affichage dans l'application.
    * @param ldef   Point bas par défaut.
    * @param tdef   Point haut par défaut.
    * @param label  Etiquette.
    * @param card   Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAna(CYDB *db, const QString &name, f32 *val, s16 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int card=-1, bool input=true);

  /** Création d'une donnée ANA 32bits max (bit de parité inclu).
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param adc    Valeur de la donnée ADC.
    * @param flg     Flag de validité.
    * @param elec   Repère électrique.
    * @param phys   Repère physique.
    * @param mode   Mode d'affichage.
    * @param format Numéro du format d'affichage dans l'application.
    * @param ldef   Point bas par défaut.
    * @param tdef   Point haut par défaut.
    * @param label  Etiquette.
    * @param card   Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAna(CYDB *db, const QString &name, f32 *val, s32 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int card=-1, bool input=true);

  /** Création d'une donnée ANA 16bits max (bit de parité inclu) pouvant être étalonnée en métrologie.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param card  Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAna(CYDB *db, const QString &name, Attr16Metro *attr, int card=-1, bool input=true);

  /** Création d'une donnée ANA 32bits max (bit de parité inclu) pouvant être étalonnée en métrologie.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param card  Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAna(CYDB *db, const QString &name, Attr32Metro *attr, int card=-1, bool input=true);

  ~CYAna();

  /** Saisie le pointeur sur les valeurs de calibrage. */
  void setPtrCal(t_calana16 *vcal);
  /** Saisie le pointeur sur les valeurs de calibrage. */
  void setPtrCal(t_calana32 *vcal);

  /** Saisie les attributs du calibrage.
    * @param vcal Valeurs de calibrage.
    * @param type Type de calibrage.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, t_calana16 *vcal, QString date="2000-01-01");

  /** Saisie les attributs du calibrage.
    * @param vcal Valeurs de calibrage.
    * @param type Type de calibrage.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, t_calana32 *vcal, QString date="2000-01-01");

  /** Saisie les attributs du calibrage.
    * @param vcal Valeurs de calibrage.
    * @param type Type de calibrage.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, t_calana16 *vcal, CAL cal, QString date="2000-01-01");
  /** Saisie les attributs du calibrage.
    * @param vcal Valeurs de calibrage.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, t_calana16 *vcal, s16 low, s16 top, QString date="2000-01-01");
  /** Saisie les attributs du calibrage.
    * @param vcal Valeurs de calibrage.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, t_calana32 *vcal, s32 low, s32 top, QString date="2000-01-01");
  /** Saisie les attributs du calibrage.
    * @param vcal Valeurs de calibrage.
    * @param type Type de calibrage.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, t_calana32 *vcal, CAL cal, QString date="2000-01-01");
  /** Saisie les attributs du calibrage.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, Cali161 *cali, QString date="2000-01-01");
  /** Saisie les attributs du calibrage avec forçage des valeurs ADC.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, Cali161 *cali, s16 low, s16 top, QString date="2000-01-01");
  /** Saisie les attributs du calibrage avec les pourcentages de seuil.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, Cali162 *cali, QString date="2000-01-01");
  /** Saisie les attributs du calibrage.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, Cali321 *cali, QString date="2000-01-01");
  /** Saisie les attributs du calibrage avec forçage des valeurs ADC.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, Cali321 *cali, s32 low, s32 top, QString date="2000-01-01");
  /** Saisie les attributs du calibrage avec les pourcentages de seuil.
      @param date Date d'initialisation à remplir lors de la mise au point. La valeur par défaut "2000-01-01" signifie que le capteur n'est pas étalonné.  */
  void setCali(CYDB *db, Cali322 *cali, QString date="2000-01-01");
  /** Change les pourcentages de seuil.
    * @param tmax  Pourcentage seuil haut maximum.
    * @param tmin  Pourcentage seuil haut minimum.
    * @param lmax  Pourcentage seuil bas maximum.
    * @param lmin  Pourcentage seuil bas minimum. */
  void changeLevel(f32 tmax=100.0, f32 tmin=70.0, f32 lmax=30.0, f32 lmin=0.0);


  /** Initialisation du calibrage. */
  virtual void initCali();
  /** Initialisation du calibrage. */
  void initCali(s32 low, s32 top);

  /** Fixe les limites de saisie des valeurs de calibrage. */
  void setCtrCal();

  /** @return le type de calibrage. */
  CAL cal() { return mCal; }

  /** @return le numéro de la carte. */
  int card() { return mCard; }

  /** Saisie les attributs d'étalonnage de métrolologie.
    * @param db       Base de données de métrologie de cette mesure.
    * @param max_pts  Nombre max de points d'étalonnage.
    * @param nb_pts   Nombre de points d'étalonnage réalisé.
    * @param points   Pointe sur un tableau de points d'étalonnage.
    */
  virtual void setCalMetro(CYDB *db, int max_pts, s16 *nb_pts, t_cal_metro points[]);

  /** @return le type de voie en chaîne de caractères. */
  virtual QString channelTypeDesc();

  /** Initialise les textes des données en lecture. */
  virtual void initReadText();
  /** Initialise les textes des données de calibrage. */
  virtual void initCalText();

  /** Saisie le groupe d'appartenance de la donnée. */
  virtual void setGroup(const QString g);
  /** Saisie l'étiquette de la donnée. */
  virtual void setLabel(const QString l);
  /** Saisie le message d'aide. */
  virtual void setHelp(const QString h);

  /*! Initialise le défaut sur min.
      @param desc Description complétée de " (repère physique-repère électrique):\nlibellé"
      @param help Aide par défaut avec %1 et %2 remplacé par le point bas et haut de calibrage relatif.
  */
  virtual CYEvent *initFault( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor defect"), QString from=tr("Ana. Sensor"), QString refs=0, QString help=tr("Defect of the sensor or the conditioner (Ex: signal lower than 4mA or higher than 20mA).  This may indicate a failure or a measure out of scale. Make sure of the quality of connections and the good performance of the acquisition board."));
  /*! Initialise l'alerte sur max.
      @param desc Description complétée de " (repère physique-repère électrique):\nlibellé"
      @param help Aide par défaut avec %1 et %2 remplacé par le point bas et haut de calibrage relatif.
  */
  virtual CYEvent *initAlert( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor defect"), QString from=tr("Ana. Sensor"), QString refs=0, QString help=tr("Defect of the sensor or the conditioner (Ex: signal lower than 4mA or higher than 20mA).  This may indicate a failure or a measure out of scale. Make sure of the quality of connections and the good performance of the acquisition board."));

  /** Calcul le gain et l'offset de linéarité par rapport à l'étendue de mesure . */
  virtual void calcul_gain_offset(float &gain, float &offset);

  /** Ajoute un type de calibrage d'une mesure paramétrable.
    * @param def place de de type comme valeur par défaut. */
  virtual void addTypeCal(CAL type, QString label, bool def=false);
  /** Saisie le type de calibrage d'une mesure paramétrable. */
  virtual void setTypeCal(int type);
  /** @return le type de calibrage d'une mesure paramétrable. */
  virtual int typeCal();

protected: // Protected attributes
  /** Numéro de carte. */
  int mCard;
  /** \a true si c'est une entrée et \a false si c'est une sortie. */
  bool mIsInput;
  /** Type de calibrage. */
  CAL mCal;
  /** Pourcentage seuil bas minimum. */
  f32 mLmin;
  /** Pourcentage seuil bas maximum. */
  f32 mLmax;
  /** Pourcentage seuil haut minimum. */
  f32 mTmin;
  /** Pourcentage seuil haut maximum. */
  f32 mTmax;
};

#endif
