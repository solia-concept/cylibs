/***************************************************************************
                          cyf64.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyf64.h"

CYF64::CYF64(CYDB *db, const QString &name, f64 *val, int format, Mode mode)
  : CYNum<f64>(db, name, val, format, mode, VF64)
{
}

CYF64::CYF64(CYDB *db, const QString &name, f64 *val, const f64 def, int format, Mode mode)
  : CYNum<f64>(db, name, val, def, format, mode, VF64)
{
}

CYF64::CYF64(CYDB *db, const QString &name, f64 *val, const f64 def, const f64 min, const f64 max, int format, Mode mode)
  : CYNum<f64>(db, name, val, def, min, max, format, mode, VF64)
{
}

CYF64::CYF64(CYDB *db, const QString &name, CYNum<f64>::Attr1<f64> *attr)
  : CYNum<f64>(db, name, attr, VF64)
{
}

CYF64::CYF64(CYDB *db, const QString &name, CYNum<f64>::Attr2<f64> *attr)
  : CYNum<f64>(db, name, attr, VF64)
{
}

CYF64::CYF64(CYDB *db, const QString &name, CYNum<f64>::Attr3<f64> *attr)
  : CYNum<f64>(db, name, attr, VF64)
{
}

CYF64::CYF64(CYDB *db, const QString &name, CYNum<f64>::Attr4<f64> *attr)
  : CYNum<f64>(db, name, attr, VF64)
{
}

CYF64::~CYF64()
{
}
