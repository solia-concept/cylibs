/***************************************************************************
                          cypidmax.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYPIDMAX_H
#define CYPIDMAX_H

// CYLIBS
#include "cyf32.h"

/** CYPIDMax représente la puissance maximale d'un PID.
  * @short Puissance maximale de PID.
  * @author Gérald LE CLEACH
  */

class CYPIDMax : public CYF32
{
  Q_OBJECT
public:
  /** Création d'une puissance maximale de PID.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Limite inférieure.
    * @param max    Limite supérieure.
    * @param format Numéro du format d'affichage dans l'application. */
  CYPIDMax(CYDB *db, const QString &name, f32 *val, const f32 def, const f32 min, const f32 max, int format=0);

  ~CYPIDMax();
};

#endif
