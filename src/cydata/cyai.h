/***************************************************************************
                          cyai.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYAI_H
#define CYAI_H

// CYLIBS
#include "cyana.h"

/** CYAI est une entrées ANA.
  * @short Entrée ANA.
  * @author Gérald LE CLEACH
  */

class CYAI : public CYAna
{
  Q_OBJECT
public:
  /** Création d'une entrée ANA 16bits.
    * @param db    Base de données parent.
    * @param name  Nom de l'entrée ANA.
    * @param attr  Attributs de l'entrée ANA.
    * @param c     Numéro de la carte ANA. */
  CYAI(CYDB *db, const QString &name, Attr16 *attr, int c=-1);

  /** Création d'une entrée ANA 32bits.
    * @param db    Base de données parent.
    * @param name  Nom de l'entrée ANA.
    * @param attr  Attributs de l'entrée ANA.
    * @param c     Numéro de la carte ANA. */
  CYAI(CYDB *db, const QString &name, Attr32 *attr, int c=-1);


  /** Création d'une entrée ANA 16bits.
    * @param db    Base de données parent.
    * @param name  Nom de l'entrée ANA.
    * @param attr  Attributs de l'entrée ANA.
    * @param c     Numéro de la carte ANA. */
  CYAI(CYDB *db, const QString &name, Attr16ADC *attr, int c=-1);

  /** Création d'une entrée ANA 32bits.
    * @param db    Base de données parent.
    * @param name  Nom de l'entrée ANA.
    * @param attr  Attributs de l'entrée ANA.
    * @param c     Numéro de la carte ANA. */
  CYAI(CYDB *db, const QString &name, Attr32ADC *attr, int c=-1);


  /** Création d'une entrée ANA jusqu'à 15bits (16 - bit de parité).
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param adc    Valeur de la donnée ADC.
    * @param flg     Flag de validité.
    * @param elec   Repère électrique.
    * @param phys   Repère physique.
    * @param mode   Mode d'affichage.
    * @param format Numéro du format d'affichage dans l'application.
    * @param ldef   Point bas par défaut.
    * @param tdef   Point haut par défaut.
    * @param label  Etiquette.
    * @param card   Numéro de la carte. */
  CYAI(CYDB *db, const QString &name, f32 *val, s16 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int card=-1);

  /** Création d'une entrée ANA jusqu'à 31bits (32 - bit de parité).
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param adc    Valeur de la donnée ADC.
    * @param flg     Flag de validité.
    * @param elec   Repère électrique.
    * @param phys   Repère physique.
    * @param mode   Mode d'affichage.
    * @param format Numéro du format d'affichage dans l'application.
    * @param ldef   Point bas par défaut.
    * @param tdef   Point haut par défaut.
    * @param label  Etiquette.
    * @param card   Numéro de la carte. */
  CYAI(CYDB *db, const QString &name, f32 *val, s32 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int card=-1);

  /** Création d'une entrée ANA 16 bits pouvant être étalonnée en métrologie.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param card  Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAI(CYDB *db, const QString &name, Attr16Metro *attr, int c=-1);

  /** Création d'une entrée ANA 32 bits pouvant être étalonnée en métrologie.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée.
    * @param card  Numéro de la carte.
    * @param input \a true si c'est une entrée et \a false si c'est une sortie. */
  CYAI(CYDB *db, const QString &name, Attr32Metro *attr, int c=-1);

  ~CYAI();
};

#endif
