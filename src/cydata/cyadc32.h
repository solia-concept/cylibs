/***************************************************************************
                          cyadc32.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYADC32_H
#define CYADC32_H

// CYLIBS
#include "cyana.h"
#include "cys32.h"

/** CYAdc32 est une entrée/sortie ANA en points 32bits (bit de parité inclu).
  * Elle est associée à sa donnée affichable en unité capteur: CYAna.
  * @short E/S ADC.
  * @author Gérald LE CLEACH
  */

class CYAdc32 : public CYS32
{
  friend class CYAna;

public:
  /** Création d'une donnée ADC.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param ana   Donnée ANA en unité capteur.
    * @param attr  Attributs de la donnée.
    * @param c     Numéro de la carte. */
  CYAdc32(CYDB *db, const QString &name, CYAna *ana, CYAna::Attr32 *attr, int c=-1);

  /** Création d'une donnée ADC.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param ana   Donnée ANA en unité capteur.
    * @param attr  Attributs de la donnée.
    * @param c     Numéro de la carte. */
  CYAdc32(CYDB *db, const QString &name, CYAna *ana, CYAna::Attr32ADC *attr, int c=-1);

  /** Création d'une donnée ADC.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param ana   Donnée ANA en unité capteur.
    * @param val   Valeur de la donnée.
    * @param elec  Repère électrique.
    * @param phys  Repère physique.
    * @param mode  Mode d'affichage.
    * @param label Etiquette.
    * @param c     Numéro de la carte. */
  CYAdc32(CYDB *db, const QString &name, CYAna *ana, s32 *val, const char *elec, const char *phys, Cy::Mode mode, QString label, int c=-1);

  /** Création d'une donnée ADC pouvant être étalonnée en métrologie.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param ana   Donnée ANA en unité capteur.
    * @param attr  Attributs de la donnée.
    * @param c     Numéro de la carte. */
  CYAdc32(CYDB *db, const QString &name, CYAna *ana, CYAna::Attr32Metro *attr, int c=-1);


  ~CYAdc32();

  /** Saisie le pointeur sur les valeurs de calibrage. */
  void setPtrCal(t_calana32 *vcal);
  /** Change les pourcentages de seuil.
    * @param tmax  Pourcentage seuil haut maximum.
    * @param tmin  Pourcentage seuil haut minimum.
    * @param lmax  Pourcentage seuil bas maximum.
    * @param lmin  Pourcentage seuil bas minimum. */
  void changeLevel(f32 tmax=102.0, f32 tmin=70.0, f32 lmax=30.0, f32 lmin=-2.0);
  /** Fixe les limites de saisie des valeurs de calibrage. */
  void setCtrCal();

  /** @return le numéro de la carte. */
  int card() { return mCard; }

  /** @return la donnée ANA. */
  CYAna *ana() { return mAna; }
  /** @return la donnée du seuil haut. */
  CYS32 *top() { return mTop; }
  /** @return la donnée du seuil bas. */
  CYS32 *low() { return mLow; }

protected: // Protected attributes
  /** Donnée ADC affichée. */
  CYAna *mAna;
  /** Donnée ADC du seuil haut. */
  CYS32 *mTop;
  /** Donnée ADC du seuil bas. */
  CYS32 *mLow;

  /** Seuil ADC bas par défaut (constructeur). */
  f32 mLDef;
  /** Seuil ADC haut par défaut (constructeur). */
  f32 mTDef;
  /** Numéro de carte. */
  int mCard;
  /** Pourcentage ADC seuil bas minimum. */
  f32 mLmin;
  /** Pourcentage ADC seuil bas maximum. */
  f32 mLmax;
  /** Pourcentage ADC seuil haut minimum. */
  f32 mTmin;
  /** Pourcentage ADC seuil haut maximum. */
  f32 mTmax;
};

#endif
