/***************************************************************************
                          cybool.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cybool.h"

// CYLIBS
#include "cyword.h"

CYBool::CYBool(CYDB *db, const QString &name, AttrBit *attr)
  : CYType<bool>(db, name, 0, attr->mode, Bool)
{
  mDef   = false;
  mLabel = attr->label;
  mHelp  = attr->help;
  mWord  = attr->word;
  mBit   = attr->bit;

  setVal(false);
  mOldVal = val();
}

CYBool::CYBool(CYDB *db, const QString &name, AttrFlag *attr)
  : CYType<bool>(db, name, 0, attr->mode, Bool)
{
  mDef   = false;
  mLabel = attr->label;
  mHelp  = attr->help;
  mWord  = 0;
  mBit   = 0;
  mVal   = (bool *)attr->flag;

  setVal(false);
  mOldVal = val();
}

CYBool::CYBool(CYDB *db, const QString &name, CYWord *word, short bit, int format, Cy::Mode mode)
  : CYType<bool>(db, name, format, mode, Bool)
{
  mDef   = false;
  mWord  = word;
  mBit   = bit;

  setVal(false);
  mOldVal = val();
}

CYBool::CYBool(CYDB *db, const QString &name, flg *flag, int format, Cy::Mode mode)
  : CYType<bool>(db, name, format, mode, Bool)
{
  mDef   = false;
  mWord  = 0;
  mBit   = 0;
  mVal   = (bool *)flag;

  setVal(false);
  mOldVal = val();
}

CYBool::~CYBool()
{
}

bool CYBool::val()
{
  if (mWord)
    return mWord->bit(mBit);
  else
    return CYType<bool>::val();
}

void CYBool::setVal(const bool val)
{
  if (mWord)
    mWord->setBit(mBit, val);
  else
    CYType<bool>::setVal(val);
}

bool CYBool::readData()
{
  if (mWord)
    return mWord->readData();
  else
    return CYType<bool>::readData();
}

bool CYBool::writeData()
{
  if (mWord)
    return mWord->writeData();
  else
    return CYType<bool>::writeData();
}

QString CYBool::displayHelp(bool whole, bool oneLine, bool withGroup, bool withValue)
{
  //TODO
  Q_UNUSED(whole)

  QString tmp="";
  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(group()));
  tmp.append(label());
  if (phys()!="")
    tmp.append(QString(" [%1]").arg(phys()));
  if (elec()!="")
    tmp.append(QString(" [%1]").arg(elec()));
  if (addr()!="")
    tmp.append(QString(" [%1]").arg(addr()));
	if (withValue)
		tmp.append(QString(" = <b>%1</b>").arg(choiceLabel()));

  if ( !oneLine )
  {
    if (!tmp.isEmpty())
      tmp.append("<hr>");
    else
      tmp.append("<br>");
    if (help()!="")
      tmp.append(help());
    if ( !note().isEmpty() ) tmp.append("<br>"+note());
  }
  return tmp;
}

QString CYBool::inputHelp(bool whole, bool withGroup)
{
  QString tmp;

  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(group()));
  tmp.append(label());
  if (phys()!="")
    tmp.append(QString(" [%1]").arg(phys()));
  if (elec()!="")
    tmp.append(QString(" [%1]").arg(elec()));
  if (addr()!="")
    tmp.append(QString(" [%1]").arg(addr()));
  if (!tmp.isEmpty())
    tmp.append("<hr>");
  else
    tmp.append("<br>");
  if (help()!="")
    tmp.append(help());
  if (whole && helpDesignerValue())
    tmp.append("<ul><li>"+tr("Designer value: %1.").arg(def())+"</li></ul>");
  if ( !note().isEmpty() ) tmp.append("<br>"+note());
  return tmp;
}

QString CYBool::physicalType()
{
  return tr("Binary");
}

QString CYBool::toString(bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  Q_UNUSED(withUnit)
  Q_UNUSED(base)
  if (ctrlValid && !isValid())
    return stringDef();
  else
    return (val() ? tr("true") : tr("false"));
}
