#ifndef CYDISCONTBUFFERING_H
#define CYDISCONTBUFFERING_H

// QT
#include <QObject>
#include <QList>

class CYU32;
class CYData;

/*! \brief Classe de bufferisation discontinue
 * La bufferisation discontinue permet de mémoriser certains points uniquement.
 * Permet par exemple de tracer en oscilloscope que des points utiles à
 * l'analyse en s'affranchissant par exemple des temps d'attente et de transition.
 * Chaque échantillon des données ainsi bufferisées est mémorisé suivant l'échantillon
 * relatif d'une donnée index provenant de la même structure réseau.
 * Lorsque toutes les bufferisations des données détectent un index de bufferisation
 * discontinue plus petit que le précédent, alors la bufferisation discontinue qui était
 * en cours est considérée comme terminée et prête pour traitement comme par exemple,
 * une capture automatique export CSV à partir d'un oscilloscope.
 * Une donnée de numéro de bufferisation provenant de la même structure réseau
 * permet de d'indicer de manière synchrone les bufferisations successives pour
 * composer ainsi le nom du fichier CSV.
 */
class CYDiscontBuffering : public QObject
{
  Q_OBJECT
public:
  /*!
   * \brief CYDiscontBuffering
   * \param parent
   * \param Index  donnée d'index temporel
   * \param number      donnée du numéro de bufferisation
   */
  explicit CYDiscontBuffering(QObject *parent, CYU32 *Index, CYU32 *number);

  /** @return la donnée d'index des buffers des données.*/
  const CYU32 *index() { return mIndex; }
  /** @return la donnée du numéro de bufferisation.*/
  const CYU32 *number() { return mNumber; }

  /** @return la valeur d'échantillon de l'index de bufferisation discontinue
   * \param str_fifo  numéro de structure dans la fifo de lecture réseau
   * \param i         index \a i d'échantillon */
  uint sampleIndex(int str_fifo, int i);

  /** @return la valeur d'échantillon du numéro de bufferisation discontinue
   * \param str_fifo  numéro de structure dans la fifo de lecture réseau
   * \param i         index \a i d'échantillon */
  uint sampleNumber(int str_fifo, int i);

  /** Ajoute une donnée à cette bufferisation. */
  void addData(CYData *data);

  /*!
   * @return \a true si les buffers de toutes les données sont
   * remplis à un instant donné et en attente de traîtement.
   * Dans ce cas les buffers sont bloqués en écriture et la bufferisation
   * ne se fait plus que dans les buffers tampons des données. Ceux-ci
   * sont récopiés dans les buffers des données une fois ceux-ci exploités
   * puis libérés par @see releaseDiscontBuffer.
   */
  bool isFilled() { return mFilled; }

signals:
  /*! Emis à chaque fin de bufferisation discontinue de toutes les données traitées.
   * \param num numéro relatif de bufferisation. */
  void filled(unsigned int num);

public slots:
  /*!
   * \brief ctrlFilled contrôle que les buffers de toutes les données sont
   * remplis à un instant donné et en attente de traîtement.
   */
  void ctrlFilled();
  /** Initialise la bufferisation discontinue de chaque donée. */
  void initDiscontBuffer();
  /** Libère pour chaque donnée son buffer courant et y copie le buffer discontinu tampon.
   *  Permet de passer à la lecture de la nouvelle bufferisation qui peut
   *  avoir déjà commencée en écriture dans le buffer tampon pendant le
   *  traitement de la buferisation précécente. */
  void releaseDiscontBuffer();

private:
  CYU32 *mIndex;
  CYU32 *mNumber;
  QList<CYData*> mData;
  bool mFilled;
};

#endif // CYDISCONTBUFFERING_H
