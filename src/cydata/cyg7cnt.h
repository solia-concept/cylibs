/***************************************************************************
                          cyg7cnt.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYG7CNT_H
#define CYG7CNT_H

// CYLIBS
#include "cys32.h"

/** CYG7Cnt est un compteur grafcet.
  * @short Compteur grafect.
  * @author Gérald LE CLEACH
  */

class CYG7Cnt : public CYS32
{
  Q_OBJECT
public:
  /** Attr est la structure de saisie des attributs d'un compteur grafcet. */
  struct Attr
  {
    /** Nom. */
    const QString &name;
    /** Valeur de la donnée. */
    s32 *val;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Numéro de grafcet. */
    int grafcet;
    /** Numéro du compteur. */
    int no;
    /** Messsage d'aide. */
    QString help;
  };

  /** Création d'un compteur de grafcet.
    * @param db    Base de données parent.
    * @param name  Nom du compteur.
    * @param attr  Attributs du compteur. */
  CYG7Cnt(CYDB *db, const QString &name, Attr *attr);
  
  ~CYG7Cnt();

  /** @return le numéro de grafcet. */
  int grafcet() { return mG7; }
  /** @return le numéro du compteur. */
  int no() { return mNo; }

private: // Private attributes
  /** Numéro de grafcet. */
  int mG7;
  /** Numéro du compteur. */
  int mNo;
};

#endif
