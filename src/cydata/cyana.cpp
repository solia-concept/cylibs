/***************************************************************************
                          cyana.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyana.h"

// QT
#include <qtimer.h>
// CYLIBS
#include "cyflag.h"
#include "cytsec.h"
#include "cyadc16.h"
#include "cyadc32.h"
#include "cystring.h"
#include "cydatetime.h"
#include "cyevent.h"
#include "cycore.h"
#include "cymeasuresetting.h"

CYAna::CYAna(CYDB *db, const QString &name, Attr16 *attr, int card, bool input)
  : CYMeasure(db, name, attr->val, attr->flag, attr->format, attr->mode),
    mCard(card),
    mIsInput(input),
    mCal(NO_CAL),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mMetroEnable = false;
  mElec  = attr->elec;
  mPhys  = attr->phys;
  mLdef  = attr->ldef;
  mTdef  = attr->tdef;
  mEMLdef = mLdef;
  mEMTdef = mTdef;

  mEnableColor  = Qt::green;
  mDisableColor = Qt::red;

  mAdc16 = new CYAdc16(db, QString("ADC_%1").arg(name), this, attr, card);
  mAdc32 = nullptr;

  setLabel(attr->label);
}

CYAna::CYAna(CYDB *db, const QString &name, Attr32 *attr, int card, bool input)
  : CYMeasure(db, name, attr->val, attr->flag, attr->format, attr->mode),
    mCard(card),
    mIsInput(input),
    mCal(NO_CAL),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mMetroEnable = false;
  mElec  = attr->elec;
  mPhys  = attr->phys;
  mLdef  = attr->ldef;
  mTdef  = attr->tdef;
  mEMLdef = mLdef;
  mEMTdef = mTdef;

  mEnableColor  = Qt::green;
  mDisableColor = Qt::red;

  mAdc32 = new CYAdc32(db, QString("ADC_%1").arg(name), this, attr, card);
  mAdc16 = nullptr;

  setLabel(attr->label);
}

CYAna::CYAna(CYDB *db, const QString &name, Attr16ADC *attr, int card, bool input)
  : CYMeasure(db, name, attr->val, attr->flag, attr->format, attr->mode),
    mCard(card),
    mIsInput(input),
    mCal(NO_CAL),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mMetroEnable = false;
  mElec  = attr->elec;
  mPhys  = attr->phys;
  mLdef  = attr->aff_min;
  mTdef  = attr->aff_max;
  mEMLdef = mLdef;
  mEMTdef = mTdef;

  mEnableColor  = Qt::green;
  mDisableColor = Qt::red;

  mAdc16 = new CYAdc16(db, QString("ADC_%1").arg(name), this, attr, card);
  mAdc32 = nullptr;

  setLabel(attr->label);
}

CYAna::CYAna(CYDB *db, const QString &name, Attr32ADC *attr, int card, bool input)
  : CYMeasure(db, name, attr->val, attr->flag, attr->format, attr->mode),
    mCard(card),
    mIsInput(input),
    mCal(NO_CAL),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mMetroEnable = false;
  mElec  = attr->elec;
  mPhys  = attr->phys;
  mLdef  = attr->aff_min;
  mTdef  = attr->aff_max;
  mEMLdef = mLdef;
  mEMTdef = mTdef;

  mEnableColor  = Qt::green;
  mDisableColor = Qt::red;

  mAdc32 = new CYAdc32(db, QString("ADC_%1").arg(name), this, attr, card);
  mAdc16 = nullptr;

  setLabel(attr->label);
}

CYAna::CYAna(CYDB *db, const QString &name, f32 *val, s16 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int card, bool input)
  : CYMeasure(db, name, val, flg, format, mode),
    mCard(card),
    mIsInput(input),
    mCal(NO_CAL),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mMetroEnable = false;
  mElec  = elec;
  mPhys  = phys;
  mLdef  = ldef;
  mTdef  = tdef;
  mEMLdef = mLdef;
  mEMTdef = mTdef;

  mEnableColor  = Qt::green;
  mDisableColor = Qt::red;

  mAdc16 = new CYAdc16(db, QString("ADC_%1").arg(name), this, adc, elec, phys, Cy::Sys, label, card);
  mAdc32 = nullptr;

  setLabel(label);
}

CYAna::CYAna(CYDB *db, const QString &name, f32 *val, s32 *adc, flg *flg, const char *elec, const char *phys, Cy::Mode mode, int format, f32 ldef, f32 tdef, QString label, int card, bool input)
  : CYMeasure(db, name, val, flg, format, mode),
    mCard(card),
    mIsInput(input),
    mCal(NO_CAL),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mMetroEnable = false;
  mElec  = elec;
  mPhys  = phys;
  mLdef  = ldef;
  mTdef  = tdef;
  mEMLdef = mLdef;
  mEMTdef = mTdef;

  mEnableColor  = Qt::green;
  mDisableColor = Qt::red;

  mAdc32 = new CYAdc32(db, QString("ADC_%1").arg(name), this, adc, elec, phys, Cy::Sys, label, card);
  mAdc16 = nullptr;

  setLabel(label);
}

CYAna::CYAna(CYDB *db, const QString &name, Attr16Metro *attr, int card, bool input)
  : CYMeasure(db, name, attr->val, attr->flag, attr->format, attr->mode),
    mCard(card),
    mIsInput(input),
    mCal(NO_CAL),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mMetroEnable = false;
  mElec  = attr->elec;
  mPhys  = attr->phys;
  mLdef  = attr->aff_min;
  mTdef  = attr->aff_max;
  mEMLdef = attr->EM_min;
  mEMTdef = attr->EM_max;
  mUncertainTypeDef=attr->mUncertainType;
  mUncertainValDef=attr->mUncertainVal;

  mEnableColor  = Qt::green;
  mDisableColor = Qt::red;

  mAdc16 = new CYAdc16(db, QString("ADC_%1").arg(name), this, attr, card);
  mAdc32 = nullptr;

  setLabel(attr->label);
}

CYAna::CYAna(CYDB *db, const QString &name, Attr32Metro *attr, int card, bool input)
  : CYMeasure(db, name, attr->val, attr->flag, attr->format, attr->mode),
    mCard(card),
    mIsInput(input),
    mCal(NO_CAL),
    mLmin(-2.0),
    mLmax(30.0),
    mTmin(70.0),
    mTmax(102.0)
{
  mMetroEnable = false;
  mElec  = attr->elec;
  mPhys  = attr->phys;
  mLdef  = attr->aff_min;
  mTdef  = attr->aff_max;
  mEMLdef = attr->EM_min;
  mEMTdef = attr->EM_max;
  mUncertainTypeDef=attr->mUncertainType;
  mUncertainValDef=attr->mUncertainVal;

  mEnableColor  = Qt::green;
  mDisableColor = Qt::red;

  mAdc32 = new CYAdc32(db, QString("ADC_%1").arg(name), this, attr, card);
  mAdc16 = nullptr;

  setLabel(attr->label);
}

CYAna::~CYAna()
{
}

//Calibrage sans type -> valeurs issues dbRN1
void CYAna::setCali(CYDB *db, t_calana16 *vcal, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mCal = NO_CAL;
  mInitCalLast = date;

  //valeurs affmin/affmax
  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  //valeurs adc
  mAdc16->mLow = new CYS16(db, QString("LOW_%1").arg(adc16()->objectName()), &(vcal->adcmin), adc16()->numFormat(), Cy::Sys);
  mAdc16->mTop = new CYS16(db, QString("TOP_%1").arg(adc16()->objectName()), &(vcal->adcmax), adc16()->numFormat(), Cy::Sys);
  mAdc16->mLow->setFormat(adc16()->numFormat());
  mAdc16->mTop->setFormat(adc16()->numFormat());
  mAdc16->mLow->setDef(mAdc16->mLDef);
  mAdc16->mTop->setDef(mAdc16->mTDef);

  initCali();
  db->setUnderGroup(0);
}

//Calibrage sans type -> valeurs issues dbRN1
void CYAna::setCali(CYDB *db, t_calana32 *vcal, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mCal = NO_CAL;
  mInitCalLast = date;

  //valeurs affmin/affmax
  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  //valeurs adc
  mAdc32->mLow = new CYS32(db, QString("LOW_%1").arg(adc32()->objectName()), &(vcal->adcmin), adc32()->numFormat(), Cy::Sys);
  mAdc32->mTop = new CYS32(db, QString("TOP_%1").arg(adc32()->objectName()), &(vcal->adcmax), adc32()->numFormat(), Cy::Sys);
  mAdc32->mLow->setFormat(adc32()->numFormat());
  mAdc32->mTop->setFormat(adc32()->numFormat());
  mAdc32->mLow->setDef(mAdc32->mLDef);
  mAdc32->mTop->setDef(mAdc32->mTDef);

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, t_calana16 *vcal, CAL type, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mCal   = type;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc16->mLow = new CYS16(db, QString("LOW_%1").arg(adc16()->objectName()), &(vcal->adcmin), adc16()->numFormat());
  mAdc16->mTop = new CYS16(db, QString("TOP_%1").arg(adc16()->objectName()), &(vcal->adcmax), adc16()->numFormat());

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, t_calana32 *vcal, CAL type, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mCal   = type;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc32->mLow = new CYS32(db, QString("LOW_%1").arg(adc32()->objectName()), &(vcal->adcmin), adc32()->numFormat(), Cy::Sys);
  mAdc32->mTop = new CYS32(db, QString("TOP_%1").arg(adc32()->objectName()), &(vcal->adcmax), adc32()->numFormat(), Cy::Sys);

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, t_calana16 *vcal, s16 low, s16 top,  QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc16->mLow = new CYS16(db, QString("LOW_%1").arg(adc16()->objectName()), &(vcal->adcmin), adc16()->numFormat());
  mAdc16->mLow->setDef(low);

  mAdc16->mTop = new CYS16(db, QString("TOP_%1").arg(adc16()->objectName()), &(vcal->adcmax), adc16()->numFormat());
  mAdc16->mTop->setDef(top);

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, t_calana32 *vcal, s32 low, s32 top, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc32->mLow = new CYS32(db, QString("LOW_%1").arg(adc32()->objectName()), &(vcal->adcmin), adc32()->numFormat(), Cy::Sys);
  mAdc32->mLow->setDef(low);

  mAdc32->mTop = new CYS32(db, QString("TOP_%1").arg(adc32()->objectName()), &(vcal->adcmax), adc32()->numFormat(), Cy::Sys);
  mAdc32->mTop->setDef(top);

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, Cali161 *cali, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mCal = cali->cal;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(cali->vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(cali->vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc16->mLow = new CYS16(db, QString("LOW_%1").arg(adc16()->objectName()), &(cali->vcal->adcmin), adc16()->numFormat());
  mAdc16->mTop = new CYS16(db, QString("TOP_%1").arg(adc16()->objectName()), &(cali->vcal->adcmax), adc16()->numFormat());

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, Cali321 *cali, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mCal = cali->cal;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(cali->vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(cali->vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc32->mLow = new CYS32(db, QString("LOW_%1").arg(adc32()->objectName()), &(cali->vcal->adcmin), adc32()->numFormat(), Cy::Sys);
  mAdc32->mTop = new CYS32(db, QString("TOP_%1").arg(adc32()->objectName()), &(cali->vcal->adcmax), adc32()->numFormat(), Cy::Sys);

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, Cali161 *cali, s16 low, s16 top, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(cali->vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(cali->vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc16->mLow = new CYS16(db, QString("LOW_%1").arg(adc16()->objectName()), &(cali->vcal->adcmin), adc16()->numFormat());
  mAdc16->mLow->setDef(low);

  mAdc16->mTop = new CYS16(db, QString("TOP_%1").arg(adc16()->objectName()), &(cali->vcal->adcmax), adc16()->numFormat());
  mAdc16->mTop->setDef(top);

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, Cali321 *cali, s32 low, s32 top, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(cali->vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(cali->vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc32->mLow = new CYS32(db, QString("LOW_%1").arg(adc32()->objectName()), &(cali->vcal->adcmin), adc32()->numFormat(), Cy::Sys);
  mAdc32->mLow->setDef(low);

  mAdc32->mTop = new CYS32(db, QString("TOP_%1").arg(adc32()->objectName()), &(cali->vcal->adcmax), adc32()->numFormat(), Cy::Sys);
  mAdc32->mTop->setDef(top);

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, Cali162 *cali, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mCal = cali->cal;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(cali->vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(cali->vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc16->mLow = new CYS16(db, QString("LOW_%1").arg(adc16()->objectName()), &(cali->vcal->adcmin), adc16()->numFormat());
  mAdc16->mTop = new CYS16(db, QString("TOP_%1").arg(adc16()->objectName()), &(cali->vcal->adcmax), adc16()->numFormat());

  changeLevel(cali->tmax, cali->tmin, cali->lmax, cali->lmin);
  mAdc16->changeLevel(cali->tmaxadc, cali->tminadc, cali->lmaxadc, cali->lminadc);

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setCali(CYDB *db, Cali322 *cali, QString date)
{
  db->setUnderGroup(group()+":"+label());
  calDB = db;
  mCal = cali->cal;
  mInitCalLast = date;

  mLow = new CYF32(db, QString("LOW_%1").arg(objectName()), &(cali->vcal->affmin), mNumFormat);
  mTop = new CYF32(db, QString("TOP_%1").arg(objectName()), &(cali->vcal->affmax), mNumFormat);
  mLow->setFormat(format());
  mTop->setFormat(format());
  mLow->setDef(mLdef);
  mTop->setDef(mTdef);

  mAdc32->mLow = new CYS32(db, QString("LOW_%1").arg(adc32()->objectName()), &(cali->vcal->adcmin), adc32()->numFormat(), Cy::Sys);
  mAdc32->mTop = new CYS32(db, QString("TOP_%1").arg(adc32()->objectName()), &(cali->vcal->adcmax), adc32()->numFormat(), Cy::Sys);

  changeLevel(cali->tmax, cali->tmin, cali->lmax, cali->lmin);
  mAdc32->changeLevel(cali->tmaxadc, cali->tminadc, cali->lmaxadc, cali->lminadc);

  initCali();
  db->setUnderGroup(0);
}

void CYAna::setPtrCal(t_calana16 *vcal)
{
  mTop->setPtrVal(&(vcal->affmax));
  mLow->setPtrVal(&(vcal->affmin));
}

void CYAna::setPtrCal(t_calana32 *vcal)
{
  mTop->setPtrVal(&(vcal->affmax));
  mLow->setPtrVal(&(vcal->affmin));
}

void CYAna::changeLevel(f32 tmax, f32 tmin, f32 lmax, f32 lmin)
{
  mTmax = tmax;
  mTmin = tmin;
  mLmax = lmax;
  mLmin = lmin;
}

void CYAna::initCali()
{
  setCtrCal();

  if (mSetting)
  {
    mSetting->setSameFormatData(mTop);
    mSetting->setSameFormatData(mLow);
  }

  CYMeasure::initCali();
}

void CYAna::initCali(s32 low, s32 top)
{
  if (mAdc16)
  {
    mAdc16->low()->setDef(low);
    mAdc16->top()->setDef(top);
  }
  else if (mAdc32)
  {
    mAdc32->low()->setDef(low);
    mAdc32->top()->setDef(top);
  }
  else
    CYWARNINGTEXT(QString("CYAna::initCali(s32 low, s32 top): %1").arg(objectName()));

  setCtrCal();
  initCalText();
}

void CYAna::setCtrCal()
{
  float ecf;

  //========== Limites de saisie ===============
  ecf = (mTop->def() - mLow->def());
  mTop->setMax(mTop->def() + (f32) (ecf * (mTmax-100.0)/100.0)); //100 % si mTmax = 100.0 % //MAX haut = valeur haute constru + 2% écart
  mTop->setMin(mTop->def() + (f32) (ecf * (mTmin-100.0)/100.0)); // 70 % si mTmin =  70.0 % //MIN haut = valeur haute constru -30% écart
  mLow->setMax(mLow->def() + (f32) (ecf * mLmax/100.0));         // 30 % si mLmax =  30.0 % //MAX bas  = valeur basse constru +30% écart
  mLow->setMin(mLow->def() + (f32) (ecf * mLmin/100.0));         //  0 % si mLmin =   0.0 % //MIN bas  = valeur basse constru - 2% écart

  //========== Points constructeur ==============
  mMax = mEMTdef;
  mMin = mEMLdef;
  mDef = ecf/2;

  mTop->forceAutoMinMax(false);
  mLow->forceAutoMinMax(false);
  if (mAdc16)
    mAdc16->setCtrCal();
  if (mAdc32)
    mAdc32->setCtrCal();
}

void CYAna::setCalMetro(CYDB *db, int max_pts, s16 *nb_pts, t_cal_metro points[])
{
  mMetroEnable = true;

  metroDB = db;
  core->addMetro(this);
  metroDB->setWriteWithApply(false);
  mMetroMaxPts = max_pts;

  f32 affmin,affmax,adcmin,adcmax;
  affmin=affmax=adcmin=adcmax=0;
  if (mTop->def() > mLow->def())
  {
    affmin = EMlow()->val();
    affmax = EMtop()->val();
  }
  else
  {
    affmin = EMtop()->val();
    affmax = EMlow()->val();
  }
  if (mAdc16)
  {
    adcmin = (f32)mAdc16->low()->val();
    adcmax = (f32)mAdc16->top()->val();
  }
  else if (mAdc32)
  {
    adcmin = (f32)mAdc32->low()->val();
    adcmax = (f32)mAdc32->top()->val();
  }
  else
    CYWARNINGTEXT(QString("CYAna::setCalMetro(): %1").arg(objectName()));

  QString group = tr("Metrology")+":"+label();

  metroNb = new CYS16(metroDB, QString("METRO_NB_%1").arg(objectName()), nb_pts, 2, 2, max_pts);
  metroNb->setLabel(tr("Number of points used for calibration"));
  metroList.append(metroNb);

  metroDB->setUnderGroup(group+":"+tr("Bench sensor in ADC"));
  CYF32 *bdir = new CYF32(metroDB, QString("METRO_BDIR_%1").arg(objectName()), new f32);
  bdir->setFormat(adc16() ? adc16()->format() : adc32()->format());
  bdir->setNbDec(1);
  bdir->setLabel(tr("New point"));
  bdir->setVolatile(true);
  metroList.append(bdir);

  metroDB->setUnderGroup(group+":"+tr("Bench sensor"));
  CYF32 *bench = new CYF32(metroDB, QString("METRO_BENCH_%1").arg(objectName()), new f32, adcmax, adcmin, adcmax);
  bench->setFormat(format());
  bench->setPrecision(0.0);
  bench->setNbDec(format()->nbDec()+1);
  bench->setLabel(tr("New point"));
  bench->setVolatile(true);
  metroList.append(bench);

  metroDB->setUnderGroup(group+":"+tr("Reference sensor"));
  CYF32 *ref = new CYF32(metroDB, QString("METRO_REF_%1").arg(objectName()), new f32, EMlow()->val(), EMlow()->val(), EMtop()->val(), numFormat());
  ref->setFormat(format());
  ref->setPrecision(0.0);
  ref->setNbDec(format()->nbDec()+1);
  ref->setLabel(tr("New point"));
  ref->setVolatile(true);
  metroList.append(ref);

  for (int i=1; i<=mMetroMaxPts; i++)
  {
    CYF32 *bdir;
    CYF32 *bench;
    CYF32 *ref;
    t_cal_metro *point = &points[i-1];
    if (i==1)
    {
      bdir  = new CYF32(metroDB, QString("METRO_BDIR%1_%2").arg(i).arg(objectName()) , &point->val_capteur, adcmin, adcmin, adcmax, adc16() ? adc16()->numFormat() : adc32()->numFormat());
      bench = new CYF32(metroDB, QString("METRO_BENCH%1_%2").arg(i).arg(objectName()), new f32            , low()->val(), affmin, affmax, numFormat());
      ref   = new CYF32(metroDB, QString("METRO_REF%1_%2").arg(i).arg(objectName())  , &point->val_etalon , low()->val(), affmin, affmax, numFormat());
    }
    else
    {
      bdir  = new CYF32(metroDB, QString("METRO_BDIR%1_%2").arg(i).arg(objectName()) , &point->val_capteur, adcmax, adcmin, adcmax, adc16() ? adc16()->numFormat() : adc32()->numFormat());
      bench = new CYF32(metroDB, QString("METRO_BENCH%1_%2").arg(i).arg(objectName()), new f32            , top()->val(), affmin, affmax, numFormat());
      ref   = new CYF32(metroDB, QString("METRO_REF%1_%2").arg(i).arg(objectName())  , &point->val_etalon , top()->val(), affmin, affmax, numFormat());
    }
    bdir->setGroup(group+":"+tr("ADC bench values"));
    bdir->setLabel(tr("Point %1").arg(i));
    bdir->setFormat(adc16() ? adc16()->format() : adc32()->format());
    bdir->setNbDec(1);
    metroList.append(bdir);

    bench->setGroup(group+":"+tr("Bench values"));
    bench->setLabel(tr("Point %1").arg(i));
    bench->setFormat(format());
    metroList.append(bench);

    ref->setGroup(group+":"+tr("Reference values"));
    ref->setLabel(tr("Point %1").arg(i));
    ref->setFormat(format());
    metroList.append(ref);

    CYTSec *tmoy = new CYTSec(metroDB, QString("METRO_TMOY%1_%2").arg(i).arg(objectName()), new u32);
    tmoy->setLabel(tr("Point %1").arg(i));
    tmoy->setGroup(group+":"+tr("Averaging times"));
    metroList.append(tmoy);
  }

  initCalText();
  initCalMetro();
}

QString CYAna::channelTypeDesc()
{
  return tr("Analogic");
}

void CYAna::initReadText()
{
  QString label = mIsInput?tr("ADC value"):tr("DAC value");
  if (mAdc16)
  {
    mAdc16->setLabel(label);
    mAdc16->setHelp(mHelp);
  }
  else if (mAdc32)
  {
    mAdc32->setLabel(label);
    mAdc32->setHelp(mHelp);
  }
  else
    CYWARNINGTEXT(QString("CYAna::initReadText(): %1").arg(objectName()));
}

void CYAna::initCalText()
{
  CYMeasure::initCalText();

  if (mAdc16)
  {
    if (mAdc16->top())
    {
      mAdc16->mTop->setLabel(tr("High value (not calibrated)")+" "+elec());
      mAdc16->mTop->setHelp(mHelp);
      mAdc16->mTop->addNote(tr("Maximum value of device usage range."));
    }
    if (mAdc16->low())
    {
      mAdc16->mLow->setLabel(tr("Low value (not calibrated)")+" "+elec());
      mAdc16->mLow->setHelp(mHelp);
      mAdc16->mLow->addNote(tr("Minimum value of device usage range."));
    }
  }
  else if (mAdc32)
  {
    if (mAdc32->top())
    {
      mAdc32->mTop->setLabel(tr("High value (not calibrated)")+" "+elec());
      mAdc32->mTop->setHelp(mHelp);
      mAdc32->mTop->addNote(tr("Maximum value ADC of device usage range."));
    }
    if (mAdc32->low())
    {
      mAdc32->mLow->setLabel(tr("Low value (not calibrated)")+" "+elec());
      mAdc32->mLow->setHelp(mHelp);
      mAdc32->mLow->addNote(tr("Minimum value ADC of device usage range."));
    }
  }
  else
    CYWARNINGTEXT(QString("CYAna::initCalText(): %1").arg(objectName()));

}

void CYAna::setGroup(const QString g)
{
  CYMeasure::setGroup(g);
  if (mAdc16)
    mAdc16->setGroup(g+":"+mLabel);
  if (mAdc32)
    mAdc32->setGroup(g+":"+mLabel);
}

void CYAna::setLabel(const QString l)
{
  CYMeasure::setLabel(l);
  initReadText();
}

void CYAna::setHelp(const QString h)
{
  CYMeasure::setHelp(h);
  initReadText();
}

CYEvent *CYAna::initFault(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  if (addr()!="")
    desc = desc+QString(" (%1-%2-%3):\n%4 !").arg(phys()).arg(elec()).arg(addr()).arg(label());
  else
    desc = desc+QString(" (%1-%2):\n%3 !").arg(phys()).arg(elec()).arg(label());
  CYEvent *event = CYData::initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  return event;
}


CYEvent *CYAna::initAlert(CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  if (addr()!="")
    desc = desc+QString(" (%1-%2-%3):\n%4 !").arg(phys()).arg(elec()).arg(addr()).arg(label());
  else
    desc = desc+QString(" (%1-%2):\n%3 !").arg(phys()).arg(elec()).arg(label());
  CYEvent *event = CYData::initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  return event;
}

void CYAna::calcul_gain_offset(float &gain, float &offset)
{
  int off;
  float dif_aff, dif_adc;

  dif_aff=mTop->val()-mLow->val();
  dif_adc=0.0;
  if (mAdc16)
  {
    dif_adc=(float)(mAdc16->top()->val() - mAdc16->low()->val());
  }
  else if (mAdc32)
  {
    dif_adc=(float)(mAdc32->top()->val() - mAdc32->low()->val());
  }
  else
    CYWARNINGTEXT(QString("CYAna::calcul_gain_offset(float &gain, float &offset): %1").arg(objectName()));

  if ((fabs(dif_aff)<1e-30) || (fabs(dif_adc)<1e-30))
  {
    gain=1.0;
    offset=0.0;
    CYWARNINGTEXT(tr("Gain offset of %1").arg(objectName()));
    return;
  }
  gain=dif_aff/dif_adc;
  off=(int)((mLow->val()*dif_adc)/dif_aff);
  if (mAdc16)
  {
    offset=mAdc16->low()->val()-off;
  }
  else if (mAdc32)
  {
    offset=mAdc32->low()->val()-off;
  }
  else
    CYWARNINGTEXT(QString("CYAna::calcul_gain_offset(float &gain, float &offset): %1").arg(objectName()));
}

void CYAna::addTypeCal(CAL type, QString label, bool def)
{
  if (!mSetting)
    CYFATALTEXT(QString("%1 addTypeCal: créer d'abord le paramétrage de mesure par initSetting").arg(objectName()));

  mSetting->addTypeCal(type, label, def);
}

void CYAna::setTypeCal(int type)
{
  mCal = (CYAna::CAL)type;
}

int CYAna::typeCal()
{
  return mCal;
}
