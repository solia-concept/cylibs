//
// C++ Implementation: cyextmeasure
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
// CYLIBS
#include "cyextmeasure.h"
#include "cystring.h"
#include "cyflag.h"
#include "cyu8.h"
#include "cys16.h"
#include "cycore.h"
#include "cyana.h"
#include "cymeasuresetting.h"

CYExtMeasure::CYExtMeasure(CYDB *db, const QString &name, f32 *val, flg *flg, int format, Mode mode)
 : CYMeasure(db, name, val, flg, format, mode)
{
  init();
}


CYExtMeasure::CYExtMeasure(CYDB *db, const QString &name, Attr *attr)
 : CYMeasure(db, name, attr)
{
  init();
}


CYExtMeasure::~CYExtMeasure()
{
}


/*!
    \fn CYExtMeasure::init()
 */
void CYExtMeasure::init()
{
  mEditScale      = true;
  mEditFormat      = true;
  mEditSupervision = true;
}


/*! Initialise le paramètrage de la mesure externe.
 *  @param def_enable activation par défaut.
    \fn CYExtMeasure::initSetting(CYDB *db, QString ana, flg *enable, f32 *top, f32 *low, f32 *ana_top, f32 *ana_low, f32 *level, f32 *ita, f32 *itd, s16 *cal, bool def_enable, QString underGroup)
 */
void CYExtMeasure::initSetting(CYDB *db, QString ana, flg *enable, f32 *top, f32 *low, f32 *ana_top, f32 *ana_low, f32 *level, f32 *ita, f32 *itd, s16 *cal, bool def_enable, QString underGroup)
{
  mSettingDB = db;
  if (underGroup.isEmpty())
    underGroup = label();
  mSettingDB->setUnderGroup( underGroup );

  mAna = (CYAna *)core->findData( ana );
  if (!mAna)
    CYFATALTEXT(ana)

  mUnit = new CYString( mSettingDB, QString("UNIT_%1").arg(objectName()), new QString( unit() ), tr("Unit"));
  mUnit->setPhys(mAna->phys());
  mUnit->setElec(mAna->elec());

  mLabel = new CYString( mSettingDB, QString("LABEL_%1").arg(objectName()), new QString( label() ), tr("Label"));
  mLabel->setPhys(mAna->phys());
  mLabel->setElec(mAna->elec());

  mEnable = new CYFlag( mSettingDB, QString("ENABLE_%1").arg(objectName()), enable, def_enable);
  mEnable->setLabel( tr("Enable") );
  mEnable->setPhys(mAna->phys());
  mEnable->setElec(mAna->elec());

  mSettingDB->setUnderGroup( underGroup + ":" + tr("Scaling") );

  mTop = new CYF32( mSettingDB, QString("TOP_%1").arg(objectName()), top, *top, numFormat());
  mTop->setPhys(mAna->phys());
  mTop->setElec(mAna->elec());
  mTop->setFormat(format());
  mTop->setLabel(tr("High value"));
  mTop->setAutoMinMaxFormat( false );
  mTop->initMinMax();
  setSameFormatData( mTop );

  mLow = new CYF32( mSettingDB, QString("LOW_%1").arg(objectName()), low, *low, numFormat());
  mLow->setPhys(mAna->phys());
  mLow->setElec(mAna->elec());
  mLow->setFormat(format());
  mLow->setLabel(tr("Low value"));
  mLow->setAutoMinMaxFormat( false );
  mLow->initMinMax();
  setSameFormatData( mLow );

  mSettingDB->setUnderGroup( underGroup + ":" + tr("Scaling") + ":" + mAna->label() );

  mAnaTop = 0;
  mAnaLow = 0;

  if (ana_top)
  {
    mAnaTop = new CYF32( mSettingDB, QString("ANA_TOP_%1").arg(objectName()), ana_top, mAna->max(), mAna->min(), mAna->max(), mAna->numFormat());
    mAnaTop->setPhys(mAna->phys());
    mAnaTop->setElec(mAna->elec());
    mAnaTop->setLabel(tr("High value"));
  }
  if (ana_low)
  {
    mAnaLow = new CYF32( mSettingDB, QString("ANA_LOW_%1").arg(objectName()), ana_low, mAna->min(), mAna->min(), mAna->max(), mAna->numFormat());
    mAnaTop->setPhys(mAna->phys());
    mAnaTop->setElec(mAna->elec());
    mAnaLow->setLabel(tr("Low value"));
  }

  mSettingDB->setUnderGroup( underGroup + ":" + tr("Supervision") );

  if (level)
  {
    mLevel = new CYF32( mSettingDB, QString("LEVEL_%1").arg(objectName()), level, numFormat() );
    mLevel->setLabel(tr ("Level") );
    mLevel->setPhys(mAna->phys());
    mLevel->setElec(mAna->elec());
    setSameFormatData( mLevel );
  }
  else
    mLevel = 0;

  if (ita)
  {
    mITA = new CYF32( mSettingDB, QString("IT_ALE_%1").arg(objectName()), ita, numFormat() );
    mITA->setLabel(tr ("Alert tolerance interval") );
    mITA->setPhys(mAna->phys());
    mITA->setElec(mAna->elec());
    setSameFormatData( mITA );
  }
  else
    mITA = 0;


  if (itd)
  {
    mITD = new CYF32( mSettingDB, QString("IT_DEF_%1").arg(objectName()), itd, numFormat() );
    mITD->setLabel(tr ("Fault tolerance interval") );
    mITD->setPhys(mAna->phys());
    mITD->setElec(mAna->elec());
    setSameFormatData( mITD );
  }
  else
    mITD = 0;

  mSettingDB->setUnderGroup( underGroup );

  if (cal)
  {
    QString oldDataName = QString("CAL_%1").arg(objectName());
    QString newDataName = QString("SIGNAL_TYPE_%1").arg(objectName());
    mSettingDB->changeDataName(oldDataName, newDataName);
    mCal = new CYS16( mSettingDB, newDataName, cal );
    mCal->setLabel(tr("Calibration type"));
    mCal->setPhys(mAna->phys());
    mCal->setElec(mAna->elec());
  }
  else
    mCal = 0;

  mSettingDB->setUnderGroup( 0 );

  connect( this, SIGNAL( formatUpdated() ), core, SIGNAL( DBChanged() ) );
  connect( mSettingDB, SIGNAL( loaded() ), this, SLOT( updateFormat() ) );
  connect( mSettingDB, SIGNAL( saved() ), this, SLOT( updateFormat() ) );
}

/*! Initialise le paramètrage de la mesure externe sans donnée analogique associée.
 *  @param def_enable activation par défaut.
    \fn CYExtMeasure::initSetting(CYDB *db, flg *enable, f32 *level, f32 *ita, f32 *itd, s16 *cal, bool def_enable, QString underGroup)
 */
void CYExtMeasure::initSetting(CYDB *db, flg *enable, f32 *level, f32 *ita, f32 *itd, s16 *cal, bool def_enable, QString underGroup)
{
  mSettingDB = db;
  if (underGroup.isEmpty())
    underGroup = label();
  mSettingDB->setUnderGroup( underGroup );

  mUnit = new CYString( mSettingDB, QString("UNIT_%1").arg(objectName()), new QString( unit() ), tr("Unit"));

  mLabel = new CYString( mSettingDB, QString("LABEL_%1").arg(objectName()), new QString( label() ), tr("Label"));

  mEnable = new CYFlag( mSettingDB, QString("ENABLE_%1").arg(objectName()), enable, def_enable);
  mEnable->setLabel( tr("Enable") );

  mSettingDB->setUnderGroup( label() + ":" + tr("Supervision") );

  if (level)
  {
    mLevel = new CYF32( mSettingDB, QString("LEVEL_%1").arg(objectName()), level, numFormat() );
    mLevel->setLabel(tr ("Level") );
    setSameFormatData( mLevel );
  }
  else
    mLevel = 0;

  if (ita)
  {
    mITA = new CYF32( mSettingDB, QString("IT_ALE_%1").arg(objectName()), ita, numFormat() );
    mITA->setLabel(tr ("Alert tolerance interval") );
    setSameFormatData( mITA );
  }
  else
    mITA = 0;


  if (itd)
  {
    mITD = new CYF32( mSettingDB, QString("IT_DEF_%1").arg(objectName()), itd, numFormat() );
    mITD->setLabel(tr ("Fault tolerance interval") );
    setSameFormatData( mITD );
  }
  else
    mITD = 0;

  if (cal)
  {
    QString oldDataName = QString("CAL_%1").arg(objectName());
    QString newDataName = QString("SIGNAL_TYPE_%1").arg(objectName());
    mSettingDB->changeDataName(oldDataName, newDataName);
    mCal = new CYS16( mSettingDB, newDataName, cal );
    mCal->setLabel(tr("Calibration type"));
  }
  else
    mCal = 0;

  mSettingDB->setUnderGroup( 0 );

  connect( this, SIGNAL( formatUpdated() ), core, SIGNAL( DBChanged() ) );
  connect( mSettingDB, SIGNAL( loaded() ), this, SLOT( updateFormat() ) );
  connect( mSettingDB, SIGNAL( saved() ), this, SLOT( updateFormat() ) );
}


void CYExtMeasure::setDef(f32 top, f32 low)
{
  mTop->setDef(top);
  mLow->setDef(low);
  designer();
}


/*! Mise à jour du format de la mesure externe.
    \fn CYExtMeasure::updateFormat()
 */
void CYExtMeasure::updateFormat()
{
  if ( !mUnit )
  {
    CYFATALTEXT(QString("Le paramétrage de la mesure externe %1 n'a pas été initialisé (initSetting)").arg(objectName()) );
  }

  if (core)
    core->formats[numFormat()]->setUnit(mUnit->val());

  if ( ( unit() == mUnit->val() ) && ( label() == mLabel->val() ) )
    return;

  mFormat->setUnit(mUnit->val());

  setUnit( mUnit->val() );
  setLabel( mLabel->val() );

  QListIterator<CYF32 *> it(mSameFormatDatas);
  while (it.hasNext())
  {
    CYF32 *data = it.next();
    data->setUnit( mUnit->val() );
    data->setLabel( mLabel->val() );
  }
  emit formatUpdated();
}

/*! Active/désactive l'édition de l'échelle
    \fn CYExtMeasure::setEditScale( bool val )
 */
void CYExtMeasure::setEditScale( bool val )
{
  mEditScale = val;
}

/*! Active/désactive l'édition du format
    \fn CYExtMeasure::setEditFormat( bool val )
 */
void CYExtMeasure::setEditFormat( bool val )
{
  mEditFormat = val;
}


/*! Active/désactive l'édition de la surveillance
    \fn CYExtMeasure::setEditSupervision( bool val )
 */
void CYExtMeasure::setEditSupervision( bool val )
{
  mEditSupervision = val;
}


/*! @return \a true si l'édition de l'échelle est active
    \fn CYExtMeasure::editScale()
 */
bool CYExtMeasure::editScale()
{
  return mEditScale;
}

/*! @return \a true si l'édition du format est active
    \fn CYExtMeasure::editFormat()
 */
bool CYExtMeasure::editFormat()
{
  return mEditFormat;
}


/*! @return \a true si l'édition de la surveillance est active
    \fn CYExtMeasure::editSupervision()
 */
bool CYExtMeasure::editSupervision()
{
  return mEditSupervision;
}


/*! Saisie le nom d'une la donnée ayant le même format
    \fn CYExtMeasure::setSameFormatData(QByteArray dataName)
 */
void CYExtMeasure::setSameFormatData(QByteArray dataName)
{
  setSameFormatData( (CYF32 *)core->findData( dataName ) );
}


/*! Saisie une donnée donnée ayant le même format
    \fn CYExtMeasure::setSameFormatData( CYF32 *data )
 */
void CYExtMeasure::setSameFormatData( CYF32 *data )
{
  mSameFormatDatas.append( data );
  connect( this, SIGNAL( formatUpdated()  ), data, SIGNAL( formatUpdated()  ) );
}


/*! @return \a true si la voie externe est active
    \fn CYExtMeasure::isEnabled()
 */
bool CYExtMeasure::isEnabled()
{
  return mEnable->val();
}

void CYExtMeasure::addTypeCal(CYAna::CAL type, QString label, bool def)
{
  if (!mCal)
    CYFATALTEXT(QString("%1 addTypeCal: activer d'abord le paramétrage de type de calibrage par initSetting").arg(objectName()));
  mCal->setChoiceLabel(type, label);
  if (def)
    mCal->setDef(type);
}

void CYExtMeasure::setTypeCal(int type)
{
  if (!mCal)
    CYFATALTEXT(QString("%1 setTypeCal: activer d'abord le paramétrage de type de calibrage par initSetting").arg(objectName()));
  mCal->setVal(type);
}

int CYExtMeasure::typeCal()
{
  if (mCal)
    return mCal->val();
  return 0;
}

