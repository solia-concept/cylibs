/***************************************************************************
                          cys64.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cys64.h"

CYS64::CYS64(CYDB *db, const QString &name, int format, Mode mode)
  : CYNum<s64>(db, name, format, mode, VS64)
{
}

CYS64::CYS64(CYDB *db, const QString &name, s64 *val, int format, Mode mode)
  : CYNum<s64>(db, name, val, format, mode, VS64)
{
}

CYS64::CYS64(CYDB *db, const QString &name, s64 *val, const s64 def, int format, Mode mode)
  : CYNum<s64>(db, name, val, def, format, mode, VS64)
{
}

CYS64::CYS64(CYDB *db, const QString &name, s64 *val, const s64 def, const s64 min, const s64 max, int format, Mode mode)
  : CYNum<s64>(db, name, val, def, min, max, format, mode, VS64)
{
}

CYS64::CYS64(CYDB *db, const QString &name, CYNum<s64>::Attr1<s64> *attr)
  : CYNum<s64>(db, name, attr, VS64)
{
}

CYS64::CYS64(CYDB *db, const QString &name, CYNum<s64>::Attr2<s64> *attr)
  : CYNum<s64>(db, name, attr, VS64)
{
}

CYS64::CYS64(CYDB *db, const QString &name, CYNum<s64>::Attr3<s64> *attr)
  : CYNum<s64>(db, name, attr, VS64)
{
}

CYS64::CYS64(CYDB *db, const QString &name, CYNum<s64>::Attr4<s64> *attr)
  : CYNum<s64>(db, name, attr, VS64)
{
}

CYS64::~CYS64()
{
}
