/***************************************************************************
                          cycnt.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYCNT_H
#define CYCNT_H

// CYLIBS
#include "cyu32.h"

class CYString;

/** CYCnt est un compteur de maintenance d'entiers non-signés de 32 bits.
  * Sa valeur ne peut être remise à 0. Cependant CYCnt est associée à une
  * donnée qui, elle, a sa valeur qui peut être remise à 0. Cette valeur
  * représente la valeur temporaire du compteur.
  * @short Compteur de maintenance (u32).
  * @author Gérald LE CLEACH
  */

class CYCnt : public CYU32
{
  Q_OBJECT
public:
  /** Attr est la structure de saisie des attributs d'un compteur de maintenance (u32). */
  struct Attr
  {
    /** Nom. */
    const QString &name;
    /** Compteur. */
    t_cu32 *cnt;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Numéro du format d'affichage dans l'application. */
    int format;
    /** Description. */
    QString desc;
  };

  /** Création d'un compteur de maintenance (u32).
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYCnt(CYDB *db, const QString &name, t_cu32 *val, int format=0, Mode mode=Sys);

  /** Création d'un timer.
    * @param db     Base de données parent.
    * @param name   Nom du timer.
    * @param cnt    Compteur.
    * @param label  Etiquette.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYCnt(CYDB *db, const QString &name, t_cu32 *cnt, QString label, int format=0, Cy::Mode mode=Sys);

  /** Création d'un compteur de maintenance (u32).
    * @param db     Base de données parent.
    * @param name  Nom du compteur.
    * @param attr  Attributs du compteur. */
  CYCnt(CYDB *db, const QString &name, Attr *attr);

  ~CYCnt();

  /** Valeur temporaire du compteur. */
  CYU32 *tmp;
  /** Note du timer. */
  CYString *note;
};

#endif
