/***************************************************************************
                          cymv.h  -  description
                             -------------------
    begin                : jeu déc 30 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYMV_H
#define CYMV_H


// QT
#include <qobject.h>
// CYLIBS
#include "cydatasgroup.h"

class CYTSec;
class CYFlag;
class CYS16;

/** @short Groupe de données relatives à un mouvement.
  * @author LE CLÉACH Gérald
  */

class CYMV : public CYDatasGroup
{
  Q_OBJECT
public:
   /** Structure de saisie des attributs de paramètrage du mouvement. */
  struct Attr1
  {
    /** Nom du mouvement (préfixe du nom des données). */
					const QString &name;
    /** Index réseau correspondant à l'index des valeurs dans la structure réseau. */
    int idNet;
    /** Valeur constructeur pour la Tempo Apparition Aller en secondes. */
    f32 taa_def;
    /** Valeur constructeur pour la Tempo Apparition Retour en secondes. */
    f32 tar_def;
    /** Valeur constructeur pour la Tempo Tempo Perte Aller en secondes. */
    f32 tpa_def;
    /** Valeur constructeur pour la Tempo Perte Retour en secondes. */
    f32 tpr_def;
    /** Valeur constructeur pour la génération de Défaut Capteur Aller (sinon alerte). */
    flg  dca_def;
    /** Valeur constructeur pour la génération de Défaut Capteur Retour (sinon alerte). */
    flg  dcr_def;
    /** Groupe du mouvement. */
    QString group;
    /** Nom du capteur aller. */
    QString forwardPhys;
    /** Nom du capteur retour. */
    QString backwardPhys;
    /** Description du mouvement aller. */
    QString forwardDesc;
    /** Description du mouvement retour. */
    QString backwardDesc;
    /** Accès au paramètrage. */
    QString settings;
  };

   /** Structure de saisie des attributs de paramètrage du mouvement. */
  struct Attr2
  {
    /** Nom du mouvement (préfixe du nom des données). */
    const QString &name;
    /** Index réseau correspondant à l'index des valeurs dans la structure réseau. */
    int idNet;
    /** Valeur constructeur pour la Tempo Apparition Aller en secondes. */
    f32 taa_def;
    /** Valeur minimum pour la Tempo Apparition Aller en secondes. */
    f32 taa_min;
    /** Valeur maximum pour la Tempo Apparition Aller en secondes. */
    f32 taa_max;
    /** Valeur constructeur pour la Tempo Apparition Retour en secondes. */
    f32 tar_def;
    /** Valeur minimum pour la Tempo Apparition Retour en secondes. */
    f32 tar_min;
    /** Valeur maximum pour la Tempo Apparition Retour en secondes. */
    f32 tar_max;
    /** Valeur constructeur pour la Tempo Tempo Perte Aller en secondes. */
    f32 tpa_def;
    /** Valeur minimum pour la Tempo Tempo Perte Aller en secondes. */
    f32 tpa_min;
    /** Valeur maximum pour la Tempo Tempo Perte Aller en secondes. */
    f32 tpa_max;
    /** Valeur constructeur pour la Tempo Perte Retour en secondes. */
    f32 tpr_def;
    /** Valeur minimum pour la Tempo Perte Retour en secondes. */
    f32 tpr_min;
    /** Valeur maximum pour la Tempo Perte Retour en secondes. */
    f32 tpr_max;
    /** Valeur constructeur pour la génération de Défaut Capteur Aller (sinon alerte). */
    flg  dca_def;
    /** Valeur constructeur pour la génération de Défaut Capteur Retour (sinon alerte). */
    flg  dcr_def;
    /** Groupe du mouvement. */
    QString group;
    /** Nom du capteur aller. */
    QString forwardPhys;
    /** Nom du capteur retour. */
    QString backwardPhys;
    /** Description du mouvement aller. */
    QString forwardDesc;
    /** Description du mouvement retour. */
    QString backwardDesc;
    /** Accès au paramètrage. */
    QString settings;
  };

  /** Construction des données de paramètrage du mouvement.
    * @param db       Base de données parent.
    * @param name     Nom du mouvement (préfixe du nom des données).
    * @param idNet    Index réseau correspondant à l'index des valeurs dans la structure réseau.
    * @param values   Pointe sur les valeurs.
    * @param taa_def  Valeur constructeur pour la Tempo Apparition Aller en secondes.
    * @param tar_def  Valeur constructeur pour la Tempo Apparition Retour en secondes.
    * @param tpa_def  Valeur constructeur pour la Tempo Perte Aller en secondes.
    * @param tpr_def  Valeur constructeur pour la Tempo Perte Retour en secondes.
    * @param dca_def  Valeur constructeur pour la génération de Défaut Capteur Aller (sinon alerte).
    * @param dca_def  Valeur constructeur pour la génération de Défaut Capteur Retour (sinon alerte).
    * @param group    Groupe du mouvement.
    * @param forwardPhys  Nom du capteur aller.
    * @param backwardPhys Nom du capteur retour.
    * @param forwardDesc  Description du mouvement aller.
    * @param backwardDesc Description du mouvement retour.
    * @param settings     Accès au paramètrage. */
  CYMV(CYDB *db, const QString &name, int idNet, t_mv *values, f32 taa_def, f32 tar_def, f32 tpa_def, f32 tpr_def, flg dca_def, flg dcr_def, QString group, QString forwardPhys, QString backwardPhys, QString forwardDesc = tr("Forward"), QString backwardDesc = tr("Backward"), QString settings=0);

  /** Construction des des données de paramètrage du mouvement.
    * @param db     Base de données parent.
    * @param name   Nom du mouvement (préfixe du nom des données).
    * @param values Pointe sur les valeurs.
    * @param attr   Attributs de paramètrage. */
  CYMV(CYDB *db, const QString &name, t_mv *values, Attr1 *attr);

  /** Construction des des données de paramètrage du mouvement.
    * @param db     Base de données parent.
    * @param name   Nom du mouvement (préfixe du nom des données).
    * @param values Pointe sur les valeurs.
    * @param attr   Attributs de paramètrage. */
  CYMV(CYDB *db, const QString &name, t_mv *values, Attr2 *attr);

  ~CYMV();

   /** Structure de saisie des attributs des états du mouvement. */
  struct AttrStates1
  {
    /** Pointe sur le flag d'initialisation ok. */
    flg  *init_ok;
    /** Pointe sur la flag de positionnement aller (Sinon retour). */
    flg  *forward;
    /** Pointe sur l'étape courante du grafcet mouvement. */
    s16 *grafcet;
  };

  /** Initialisation des données d'état du mouvement.
    * @param db       Base de données parent.
    * @param init_ok  Pointe sur le flag d'initialisation ok.
    * @param forward  Pointe sur la flag de positionnement aller (Sinon retour).
    * @param grafcet  Pointe sur l'étape courante du grafcet mouvement. */
  void initStates(CYDB *db, flg *init_ok, flg *forward, s16 *grafcet);

  /** Initialisation des données d'état du mouvement.
    * @param db    Base de données parent.
    * @param attr  Attributs d'états. */
  void initStates(CYDB *db, AttrStates1 *attr);

  /** @return la Tempo Apparition Aller. */
  CYTSec *TAA() { return mTAA; }
  /** @return la Tempo Apparition Retour. */
  CYTSec *TAR() { return mTAR; }
  /** @return la Tempo Perte Aller. */
  CYTSec *TPA() { return mTPA; }
  /** @return la Tempo Perte Retour. */
  CYTSec *TPR() { return mTPR; }

  /** @return le flag de génération de Défaut Capteur Aller (sinon alerte). */
  CYFlag *DCA() { return mDCA; }
  /** @return le flag de génération de Défaut Capteur Retour (sinon alerte). */
  CYFlag *DCR() { return mDCR; }

  /** @return le flag d'initialisation ok.. */
  CYFlag *initOk() { return mInitOk; }
  /** @return le flag de positionnement aller (Sinon retour). */
  CYFlag *forward() { return mForward; }
  /** @return l'étape courante du grafcet mouvement. */
  CYS16 *grafcet() { return mGrafcet; }
  
  /** @return la nom du capteur aller. */
  virtual QString forwardPhys() { return mForwardPhys; }
  /** Saisie la nom du capteur aller. */
  virtual void setForwardPhys(QString txt)  { mForwardPhys = txt; }
  /** @return la nom du capteur retour. */
  virtual QString backwardPhys()  { return mBackwardPhys; }
  /** Saisie la nom du capteur retour. */
  virtual void setBackwardPhys(QString txt) { mBackwardPhys = txt; }

  /** @return la description du mouvement aller. */
  virtual QString forwardDesc() { return mForwardDesc; }
  /** Saisie la description du mouvement aller. */
  virtual void setForwardDesc(QString txt)  { mForwardDesc = txt; }
  /** @return la description du mouvement retour. */
  virtual QString backwardDesc()  { return mBackwardDesc; }
  /** Saisie la description du mouvement retour. */
  virtual void setBackwardDesc(QString txt) { mBackwardDesc = txt; }

  // DEFAUTS
  /** Initialisation défaut sur front montant capteur aller */
  CYEvent *initFaultForwardRising( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("No sensor presence %1")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The maximum waiting time of rising edge of the sensor of end of forward stroke has been exceeded by the movement forward.")+" "+tr("This may indicate a sensor failure or movement."));
  /** Initialisation défaut sur front descandant capteur aller */
  CYEvent *initFaultForwardFalling( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("No sensor loss %1"    )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The maximum waiting time of falling edge of the sensor of end of forward stroke has been exceeded by the movement backward.")+" "+tr("This may indicate a sensor failure or movement."));
  /** Initialisation défaut statique capteur aller */
  CYEvent *initFaultForwardStatic( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor loss %1"       )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The forward sensor was lost in forward static.")+" "+tr("This may indicate an adjustment defect of the sensor."));

  /** Initialisation défaut sur front montant capteur retour */
  CYEvent *initFaultBackwardRising( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("No sensor presence %1")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The maximum waiting time of rising edge of the sensor of end of backward stroke has been exceeded by the movement backward.")+" "+tr("This may indicate a sensor failure or movement."));
  /** Initialisation défaut sur front descandant capteur retour */
  CYEvent *initFaultBackwardFalling( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("No sensor loss %1"    )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The maximum waiting time of falling edge of the sensor of end of backward stroke has been exceeded by the movement forward.")+" "+tr("This may indicate a sensor failure or movement."));
  /** Initialisation défaut statique capteur retour */
  CYEvent *initFaultBackwardStatic( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor loss %1"       )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The backward sensor was lost in backward static.")+" "+tr("This may indicate an adjustment defect of the sensor."));

  /** Initialisation défaut cohérence capteurs aller/retour */
  CYEvent *initFaultCoherence( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor coherence %1/%2" )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The sensors forward and backward were actived at the same time.")+" "+tr("This may indicate an adjustment defect of the sensors."));

  // ALERTES
  /** Initialisation alerte sur front montant capteur aller */
  CYEvent *initAlertForwardRising( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("No sensor presence %1")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The maximum waiting time of rising edge of the sensor of end of forward stroke has been exceeded by the movement forward.")+" "+tr("This may indicate a sensor failure or movement."));
  /** Initialisation alerte sur front descandant capteur aller */
  CYEvent *initAlertForwardFalling( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("No sensor loss %1"    )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The maximum waiting time of falling edge of the sensor of end of forward stroke has been exceeded by the movement backward.")+" "+tr("This may indicate a sensor failure or movement."));
  /** Initialisation alerte statique capteur aller */
  CYEvent *initAlertForwardStatic( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor loss %1"       )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The forward sensor was lost in forward static.")+" "+tr("This may indicate an adjustment defect of the sensor."));

  /** Initialisation alerte sur front montant capteur retour */
  CYEvent *initAlertBackwardRising( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("No sensor presence %1")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The maximum waiting time of rising edge of the sensor of end of backward stroke has been exceeded by the movement backward.")+" "+tr("This may indicate a sensor failure or movement."));
  /** Initialisation alerte sur front descandant capteur retour */
  CYEvent *initAlertBackwardFalling( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("No sensor loss %1"    )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The maximum waiting time of falling edge of the sensor of end of backward stroke has been exceeded by the movement forward.")+" "+tr("This may indicate a sensor failure or movement."));
  /** Initialisation alerte statique capteur retour */
  CYEvent *initAlertBackwardStatic( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor loss %1"       )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The backward sensor was lost in backward static.")+" "+tr("This may indicate an adjustment defect of the sensor."));

  /** Initialisation alerte cohérence capteurs aller/retour */
  CYEvent *initAlertCoherence( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr("Sensor coherence %1/%2" )+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("The sensors forward and backward were actived at the same time.")+" "+tr("This may indicate an adjustment defect of the sensors."));

private: // Private methods
  /** Initialisation des données de paramètrage du mouvement. */
  void init();
  /** Initialisation des données d'état du mouvement. */
  void initStates();

protected: // Protected attributes
  /** Tempo Apparition Aller. */
  CYTSec *mTAA;
  /** Tempo Apparition Retour. */
  CYTSec *mTAR;
  /** Tempo Perte Aller. */
  CYTSec *mTPA;
  /** Tempo Perte Retour. */
  CYTSec *mTPR;

  /** Flag de génération de Défaut Capteur Aller (sinon alerte). */
  CYFlag *mDCA;
  /** Flag de génération de Défaut Capteur Retour (sinon alerte). */
  CYFlag *mDCR;

  /** Flag d'initialisation ok.. */
  CYFlag *mInitOk;
  /** Flag de positionnement aller (Sinon retour). */
  CYFlag *mForward;

  /** Etape courante du grafcet mouvement. */
  CYS16 *mGrafcet;

  /** Nom du capteur aller. */
  QString mForwardPhys;
  /** Nom du capteur retour. */
  QString mBackwardPhys;
  
  /** Description du mouvement aller. */
  QString mForwardDesc;
  /** Description du mouvement retour. */
  QString mBackwardDesc;
};

#endif
