/***************************************************************************
                          cydata.h  -  description
                             -------------------
    début                  : Wed Mar 12 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDATA_H
#define CYDATA_H

// QT
#include <QObject>
#include <QStringList>
#include <QHash>
#include <QMap>
#include <QPixmap>
#include <QList>
#include <QElapsedTimer>
// CYLIBS
#include "cy.h"
#include "cytypes.h"
#include "cydatachoice.h"

class CYDB;
class CYFlag;
class CYS32;
class CYAdc16;
class CYAdc32;
class CYMeasure;
class CYFormat;
class CYProfibusItem;
class CYString;
class CYEvent;
class CYEventsGenerator;
class QSettings;
class CYMeasureSetting;
class CYDiscontBuffering;

/** CYData est la classe de base d'une donnée CY.
  * @short Classe de base d'une donnée CY.
  * @author Gérald LE CLEACH
  */

class CYData : public QObject, public Cy
{
  Q_OBJECT

public:
  /** Une donnée peut être sectionnée en mantisse et exposant pour une valeur exponentielle
  * ou en heures, minutes, secondes et millisecondes pour une tempo.
  * Par soucis de lisibilité du code nous avons une
  * énumération "Section" qui sert au traîtement de différentes
  * méthodes de la classe. Celles-ci ont un paramètre de type Section
  * qui indique quelle partie de la donnée exponentielle ou temporelle est à traîter. */
  enum Section
  {
    /** Mantisse. */
    Mantissa,
    /** Exposant. */
    Exponent,

    /** Heures. */
    Hours,
    /** Minutes. */
    Minutes,
    /** Secondes. */
    Seconds,
    /** Millisecondes. */
    Milliseconds,
  };

  enum BufferState
  {
    /** En remplissage */
    Filling,
    /** Fin remplissage et prêt pour capture.  */
    Filled,
  };

  /** Type de simulation du signal afin de générer par exemple une courbe. L*/
  enum SimulType
  {
    // Signal non simulé. Permet de figer une valeur en simulation.
    None,
    // Signal d'un sinus
    Sinus,
  };

  /** Création de la base d'une donnée.
    * @param db  Base de données parent.
    * @param name    Nom de la donnée.
    * @param format  Numéro du format d'affichage dans l'application.
    * @param mode    Mode d'utilisation.
    * @param type    Type de valeur. */
  CYData(CYDB *db, const QString &name, int format=0, Mode mode=Sys, Cy::ValueType type=Undef);

  ~CYData();

  CYMeasureSetting *mSetting;

  /** Initialisation générale appélée dans le constructeur
   * et pouvant être enrichie dans les classes filles pour
   * une initialisation commune à tous leurs constructeur. */
  virtual void init();
  /** Lecture de la donnée. */
  virtual bool readData();
  /** Ecriture de la donnée. */
  virtual bool writeData();
  /** Ecriture de la donnée de forçage. */
  virtual bool writeForcing() { return 0; }
  /** Initialise la valeur de la donnée forçage. */
  virtual void initForcingValue() {}
  /** Validité de la donnée. */
  bool isOk(bool testFlag=true);
  /** @return la donnée du mode forçage. */
  virtual CYData *dataForcing() { return mDataForcing; }
  /** @return la donnée ADC 16bits. */
  CYAdc16 *adc16() { return mAdc16; }
  /** @return la donnée ADC 32bits. */
  CYAdc32 *adc32() { return mAdc32; }
  /** @return le flag de validité. */
  virtual CYFlag *flag() { return mFl; }
  /** @return le flag de validité. */
  virtual void setFlag(CYFlag *flag);
  /** Saisie la chaîne de caractères d'affichage de défaut.
    Saisir 0 pour annuler cette gestion. */
  virtual void setStringDef(QString txt);
  /** @return la chaîne de caractères d'affichage de défaut. */
  virtual QString stringDef();

  /** @return true si la valeur est numérique. */
  bool isNum() { return mIsNum; }
  /** @return true si la valeur est entier. */
  bool isInt() { return mIsInt; }
  /** @return true si la valeur est signée. */
  bool isSigned() { return mIsSigned; }
  /** @return true si la valeur fait partie d'un compteur. */
  bool isCnt() { return mIsCnt; }
  /** @return true si la valeur fait partie d'un timer. */
  bool isTim() { return mIsTim; }
  /** @return true s'il s'agit d'une valeur temporaire d'un compteur/timer. */
  bool isTmp() { return mIsTmp; }
  /** @return true si c'est une donnée flag. */
  bool isFlag() { return mIsFlag; }

  /** @return le format d'affichage. */
  CYFormat *format() { return mFormat; }
  /** @return le numéro du format d'affichage dans l'application. */
  int numFormat() { return mNumFormat; }
  /** Calcule le nombre de chiffres dans le cas d'une donnée numérique. */
  virtual void calculNbDigits();

  /** @return l'unité */
  QString unit();
  /** @return la correspondance physique. */
  QString physical();
  /** @return le type physique avec l'unité. */
  virtual QString physicalType();
  /** @return le nombre de chiffres total. */
  virtual int nbDigit(Cy::NumBase base=Cy::BaseData);
  /** @return le nombre de chiffres après la virgule. */
  virtual int nbDec();
  /** @return la précision. */
  double precision();
  /** Saisir \a true s'il s'agit d'un format exponentiel. */
  void setExponential(bool val);
  /** @return \a true s'il s'agit d'un format exponentiel. */
  bool isExponential();

  /** @return le mode d'utilisation. */
  Mode mode() { return mMode; }
  /** @return le type de valeur. */
  ValueType type() { return mType; }

  /** @return le nom de la donnée en dans un tableau d'octet QByteArray suivant l'encodage UTF-8. */
  QByteArray dataName();
  /** @return le nom des hôtes de la donnée. */
  virtual QString hostsName();
  /** @return le nom des connection réseau. */
  virtual QString linksName();
  /** @return la base de données de rattachement. */
  CYDB *db() { return mDB; }

  /** @return le groupe d'appartenance de la donnée. */
  virtual QString group(bool withDBGroup=true);
  /** @return la section du groupe d'appartenance suivant \a choice. */
  virtual QString groupSection(Cy::GroupSection choice);

  /** @return l'étiquette de la donnée.
    * @param whole étiquette complète avec le préfixe et le suffixe */
  virtual QString label(bool whole=true);

  /** @return le titre de la donnée. */
  virtual QString title();
  /** @return le message d'aide. */
  virtual QString help();
  /**
   * @brief Génération d'une balise de donnée d'aide.
   * Cette balise de donnée sert dans CYData::help() d'une donnée à afficher pour information la valeur courante d'une autre donnée.
   * @param name Nom de la donnée à traiter
   * @return la balise de recherche d'une donnée d'aide.
   */
  static QString markupHelpData(QString name);
  /**
    * @brief Transforme le(s) balise(s) de donnée d'aide en texte à afficher.
    * @param txt Texte contenant d'éventuelles balises de données.
    */
  void replaceMarkupHelpData(QString &txt);
  /** @return le message d'aide compatible HTML. */
  virtual QString helpHTML();

  /** @return la ou les notes à rajouter dans l'aide.
   *  @param format Format du texte à choisir en fonction de la compatibilté de l'objet à afficher ce format.
   *  @param header Affiche en en-tête "Note:" ou "Notes:". */
  virtual QString note(Qt::TextFormat format=Qt::RichText, bool header=true);
  /** @return la ou les notes à rajouter dans le manuel en ligne. */
  virtual QString cydocNote();
  /** @return la liste de notes à rajouter dans l'aide. */
  QStringList notes() { return mNotes; }
  /** Saisie la liste de notes à rajouter dans l'aide. */
  virtual void setNotes(QStringList list) { mNotes = list; }

  /** @return l'aide à la visualisation.
   * @param whole  Aide entière ex: pour les données numériques avec les bornes, la précision si il y en a une et la valeur par défaut.
   * @param oneLine    Aide uniquement composée de la première ligne.
   * @param withGroup  Aide avec le groupe de la donnée.
   * @param withValue  Aide avec la valeur courante. */
  virtual QString displayHelp(bool whole=true, bool oneLine=false, bool withGroup=true, bool withValue=false);
  /** @return l'aide à la saisie.
   * @param whole  Aide entière ex: pour les données numériques avec les bornes, la précision si il y en a une.
   * @param withGroup  Aide avec le groupe de la donnée. */
  virtual QString inputHelp(bool whole=true, bool withGroup=true);
  /** @return les informations constructeur. */
  virtual QString infoCY(QColor color=Qt::blue);
  /** @return le repère électrique. */
  virtual QString elec() { return mElec; }
  /** @return le repère physique. */
  virtual QString phys() { return mPhys; }
  /** @return l'adresse. */
  virtual QString addr() { return mAddr; }

  /** @return la chaîne de caractères de clé \a key du dictionnaire de chaînes de caractères supplémentaires. */
  virtual QString string(QString key)
  {
    QString *txt = stringsDict.value(key);
    if (txt)
  return *txt;
    return 0;
  }
  /** Saisie la chaîne de caractères \a txt de clé \a key dans le dictionnaire de chaînes de caractères supplémentaires. */
  virtual void addString(QString key, QString txt) { stringsDict.insert(key, new QString(txt)); }

  /** @return le choix texte d'activation d'une donnée flag. */
  virtual QString choicetrue() { return mChoicetrue; }
  /** @return le choix texte de désactivation d'une donnée flag. */
  virtual QString choicefalse() { return mChoicefalse; }
  /** @return la valeur courante en chaîne de caractères suivant le format et l'unité. */
  virtual QString toString(bool withUnit=true, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData)
  {
    Q_UNUSED(withUnit);
    Q_UNUSED(ctrlValid);
    Q_UNUSED(base);
    return 0;
  }
  /** Force le flag indiquant qui'il s'agit de valeurs mini, maxi générées ou non automatiquement. */
  inline void forceAutoMinMax(bool val) { mAutoMinMax = val; }
  /** @return \a true s'il s'agit de valeurs mini, maxi générées automatiquement.
  * Utile que pour les données numériques.  */
  bool autoMinMax() { return mAutoMinMax; }
  /** Saisir \a false pour que les valeurs mini, maxi générées automatiquement ne reprennent pas les valeurs du format */
  void setAutoMinMaxFormat(bool val) { mAutoMinMaxFormat = val; }
  /** @return \a true s'il les valeurs mini, maxi générées automatiquement reprennent les valeurs du format */
  bool autoMinMaxFormat() { return mAutoMinMaxFormat; }
  /** Initialise les min max lorsqu'ils ne sont pas saisis. */
  virtual void initMinMax() {}

  /** Charge la valeur constructeur. */
  virtual void designer() {}
  /** Charge la valeur constructeur de la donnée ayant pour nom \a dataName. */
  static void designer(QString dataName);
  /** Charge la valeur constructeur de la donnée \a data. */
  static void designer(CYData *data);
  /** Place cette donnée pour vérification de valeur constructeur à la fin du démarrage de l'application. */
  virtual void setDataCheckDef();
  /** Vérifie la valeur par défaut du constructeur par rarpport aux limites. */
  virtual void checkDef() {}

  /** @return la valeur temporaire lorsqu'il s'agit d'une donnée numérique. */
  virtual double tmp() { return mTmp; }
  /** Saisie une valeur temporaire lorsqu'il s'agit d'une donnée numérique. */
  virtual void setTmp(const double val) { mTmp = val; }

  /** @return la valeur castée en un int de la donnée si elle est numérique. */
  virtual int valToInt();
  /** @return la valeur castée en un double de la donnée si elle est numérique. */
  virtual double valToDouble();
  /** @return la valeur maxi castée en un double de la donnée si elle est numérique. */
  virtual double maxToDouble();
  /** @return la valeur mini castée en un double de la donnée si elle est numérique. */
  virtual double minToDouble();
  /** @return la valeur par défaut castée en un double de la donnée si elle est numérique. */
  virtual double defToDouble();

  /** Saisie la couleur d'affichage utilisée lorsque la valeur est valide.  */
  void setEnableColor (const QColor &c);
  /** @return la couleur d'affichage utilisée lorsque la valeur est valide.  */
  QColor enableColor () const { return mEnableColor; }

  /** Saisie la couleur d'affichage utilisée lorsque la valeur n'est pas valide.  */
  void setDisableColor (const QColor &c);
  /** @return la couleur d'affichage utilisée lorsque la valeur n'est pas valide.  */
  QColor disableColor () const { return mDisableColor; }

  /** Saisie la couleur de saisie.  */
  void setInputColor (const QColor &c) { mInputColor = c; }
  /** @return la couleur de saisie.  */
  QColor inputColor () const { return mInputColor; }

  /** @return \a  \a true s'il s'agit d'une donnée en lecture rapide. */
  bool isReadFast();
  /** @return le buffer d'acquisition. */
  QVector<double> buffer() { return mBuffer; }
  /**
   * @return la valeur d'initialisation de buffer. @see CYCore::setInitBufferVal
   */
  double initBufferVal();

  /**
   * Saisie une valeur dans le buffer.
   * @param id  Index dans le buffer de la valeur à renseigner.
   * @param val Nouvelle valeur.
   */
  void setBufferVal(const int &id, const double &val);
  /** Authorise l'utilisation du buffer si \a val vaut \a true. */
  void useBuffer(bool val);
  /** Ajoute dans le buffer les \nb valeurs nouvellement lues (nombre de structures lues). */
  virtual void buffering(int nb) { Q_UNUSED(nb); }
  /** @return l'index de la donnée courante dans le buffer. */
  int idBuffer() { return mIdBuffer; }
  /** @return la taille du buffer. */
  uint bufferSize() { return mBufferSize; }
  /** @return le pointeur de la valeur courante du buffer.
  * @param offset Offset para rapport à la valeur courante. */
  double *bufferCurrent();
  /** @return l'état de remplissage du buffer. */
  bool bufferFull() { return mBufferFull; }
  /** @return la taille de l'échantillon d'acquisition (nombre de valeurs à la fois). */
  short sampleSize();

  /*! Configure la bufferisation d'un tableau de valeurs.
   * @param nbDataName  Nom de la donnée nombre de valeurs à bufferiser.
   *        Sa valeur max est utilisée pour indiquer la taille du tableau. */
  void setTableBuffer(QString nbDataName);
  /*! Configure la bufferisation d'un tableau de valeurs.
   * @param nb  Donnée nombre de valeurs à bufferiser.
   *    Sa valeur max est utilisée pour indiquer la taille du tableau. */
  void setTableBuffer(CYS32 *nb);
  /** @return la taille du tableau de valeurs à bufferiser.
   * Vaut -1 dans le cas d'une bufferisation autre. */
  int tableBufferSize();
  /** @return le nombre de valeurs à bufferiser du tableau de valeurs. */
  int tableBufferNb();
  /** @return le nombre de valeurs bufferisées du tableau de valeurs. */
  int tableBufferCpt();

  /*! Saisie la bufferisation discontinue traitant cette donnée.
   * @see CYDiscontBuffering */
  void setDiscontBuffering(CYDiscontBuffering *discontBuffering);
  /** @return le buffer tampon de bufferisation discontinue. */
  QVector<double> discontBuffer() { return mDiscontBuffer; }
  /** @return l'index de lecture de la donnée courante dans le buffer. */
  int discontBufferId() { return mDiscontBufferId; }
  /** @return la valeur d'échantillon de l'index de bufferisation discontinue
   * \param str_fifo  numéro de structure dans la fifo de lecture réseau
   * \param i     index \a i d'échantillon */
  unsigned int discontSampleId(int str_fifo, int i);
  /** @return la valeur d'échantillon du numéro de bufferisation discontinue
   * \param str_fifo  numéro de structure dans la fifo de lecture réseau
   * \param i     index \a i d'échantillon */
  unsigned int discontBufferNumber(int str_fifo, int i);
  /*!
   * \brief Fin d'une bufferisation discontinue.
   * Le buffer discontinu tampon est alors nettoyé et le buffer courant est
   * bloqué en écriture jusqu'à libération par @see releaseDiscontBuffer.
   */
  void endDiscontBuffer();
  /** @return le numéro de la bufferisation discontinue en cours. */
  uint discontBufferNumber() { return mDiscontBufferNum; }

  /** @return la période d'acquisition en ms. */
  float acquisitionTime();
  /** Vérifie s'il faut buffériser la valeur nouvellement lue sur la communication asynchrone
   *  et si oui combien de fois pour compenser éventuellement le timer. Celui-ci peut ne pas
   *  être garanti sous Windows.
   *  @return le nombre de fois à bufferiser la valeur courante sur la communication asynchrone. */
  bool nbBufferingAsyncCom();

  /** @return le buffer d'acquisition par rafales. */
  QVector<double> burstsBuffer() { return mBurstsBuffer; }
  /** Saisie le flag d'activation du buffer ultra rapide par son nom. */
  void setBurstsBuffer(QByteArray flagName);
  /** Saisie le flag d'activation du buffer ultra rapide. */
  void setBurstsBuffer(CYFlag *flag);
  /** @return le flag d'activation du buffer d'acquisition par rafales. */
  CYFlag *burstsBufferFlag() { return mBurstsBufferFlag; }
  /** @return le pointeur de la valeur courante du buffer par rafales.
  * @param offset Offset para rapport à la valeur courante. */
  double *burstsBufferCurrent();

  /** @return \a true si le buffer d'acquisition par rafales est activé par l'échantillon d'index \a i dans la structure \a str. */
  bool burstsBufferEnable(int str=0, int i=0);
  /** @return l'index de la donnée courante dans le buffer d'acquisition par rafales. */
  int idBurstsBuffer() { return mIdBurstsBuffer; }
  /** @return la taille du buffer d'acquisition par rafales. */
  int burstsBufferSize() { return mBurstsBufferSize; }
  /** @return le rapport période d'acquisition rapide / période d'acquisition par rafales. */
  int burstsRatio() { return mBurstsRatio;}
  /** @return l'état de remplissage du buffer d'acquisition  par rafales. */
  bool burstsBufferFull() { return mBurstsBufferFull; }

  /** Initialise la donnée comme étant une donnée profibus. */
  virtual void initProfibus() {}
  /** Initialise la donnée comme étant une donnée profibus. */
  virtual void initProfibus(void *ptr);
  /** @return le numéro du régulateur profibus. */
  int profibusRN();
  /** @return l'adresse profibus. */
  int profibusAdr();
  /** @return le numéro d'octet profibus. */
  int profibusByte();
  /** @return le numéro d'octet profibus en chaine de caractères avec un 0 devant s'il est inférieur à 10. */
  QString profibusByteString();
  /** @return le numéro de bit profibus. */
  int profibusBit();

  virtual QColor currentColor();

  virtual bool hasChoice();
  /** Efface les choix possibles */
  virtual void clearChoice();
  virtual void setChoice(int value, QString label=0, QString help=0);
  virtual void setChoice(int value, QPixmap pixmap, QString label=0, QString help=0);
  virtual void setChoiceLabel(int value, QString label);
  virtual QString choiceLabel();
  virtual QString choiceLabel(int value);
  virtual void setChoiceHelp(int value, QString help);
  virtual QString choiceHelp(int value);
  virtual void setChoicePixmap(int value, QPixmap pixmap);
  virtual QPixmap choicePixmap(int value);
  bool simulation();
  virtual void setHelpDesignerValue(bool val);
  virtual bool helpDesignerValue();
  virtual bool isFromFormat(CYFormat *fmt);
  virtual Cy::NumBase numBase();

  virtual void setEventsGenerator(CYEventsGenerator *generator);
  /*! Initialise un évènement. */
  virtual CYEvent *initEvent( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help);
  /*! Initialise un défaut. */
  virtual CYEvent *initFault( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help);
  /*! Initialise une alerte. */
  virtual CYEvent *initAlert( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help);

  /*! Désactive la gestion de texte de la valeur spéciale.
   Permet de visualiser une donnée non valide avec la couleur \a disableColor() sans "-" cachant la valeur.
   @see CYLineEdit::putSpecialValueText(). */
  void disableSpecialValueText(bool val) { mNotSpecialValueText = val; }
  /*! @return \a true si la gestion de texte de la valeur spéciale est désactivée.
   @see CYLineEdit::putSpecialValueText(). */
  bool notSpecialValueText() { return mNotSpecialValueText; }

  /** Saisie le format et le libellé.
  * @param format Format d'affichage source.
  * @param label  Text por libellé de la donnée. */
  virtual void setFormatLabel(CYFormat *format, QString label);

  /** @return une ligne donnée pour le fichier d'export des données avec les
  * champs suivants espacés d'une tabulation:
  * Hôte(s), connexion(s), groupe, nom, description, valeur, def, min, max, aide.
  * @see CYDB::exportDatas() */
  virtual QString exportData() { return 0; }

  /** @return true si le trigger est déclenché. */
  virtual bool triggerState() { return mTriggerState; }
  /** @return l'index buffer de la valeur de déclenchement du trigger. */
  virtual int triggerIdBuffer();
  /** @return l'index buffer de la valeur de déclenchement du trigger décalé du post-trigger. */
  virtual int postTriggerIdBuffer();
  /** @return le pointeur de la valeur du buffer de déclenchement du trigger.
    * @param offset Offset para rapport à la valeur courante. */
  double *bufferTrigger();
  /** @return l'état de remplissage du buffer trigger. */
  bool triggerBufferFull() { return mTriggerBufferFull; }
  /** @return le compteur de remplissage du buffer trigger après déclenchement. */
  int triggerBufferCpt() { return mTriggerBufferCpt; }

  virtual bool isEnabled();

  /** @return \a true si la gestion de FIFO est active. */
  bool enableFIFO() { return false; }

  /** @return \a une nouvelle valeur de simulation en buffer à l'index \a id. */
  virtual double bufferSimulation(int id);

  /** @return la donnée numérique direct du matériel (non calibrée). */
  CYMeasure *direct() { return mDirect; }

  /**
   * @return L'augmentation max en % par rapport à la valeur courante. La valeur par défaut à 0 inhibe ce contrôle.
   * @see setLimitedIncrease();
   */
  double limitedIncrease() { return mLimitedIncrease; }
  /**
   * @return La diminution max en % par rapport à la valeur courante. La valeur par défaut à 0 inhibe ce contrôle.
   * @see setLimitedDecrease();
   */
  double limitedDecrease() { return mLimitedDecrease; }

signals: // Signals
  /** Emis à chaque changement de valeur. */
  void valueChanged();
  /** Emis à chaque changement de valeur. Si \a rising vaut \a true alors la valeur est en hausse
  * et à l'inverse lorsque \a rising vaut \a false la valeur est en baisse. */
  void valueRising(bool rising);
  /** Emis à chaque changement de couleur d'affichage utilisée lorsque la valeur est valide.  */
  void enableColorChanged(const QColor &c);
  /** Emis à chaque changement de couleur d'affichage utilisée lorsque la valeur n'est pas valide.  */
  void disableColorChanged(const QColor &c);
  /** Emis à chaque changement de valeur de forçage à partir du synoptic.
  * (Uniquement pour des données binaires) */
  void synopticForcingChanged(int);
  /** Emis à chaque changement de valeur de forçage à partir du synoptic.
  * (Uniquement pour des données numériques) */
  void synopticForcingChanged(double);
  /** Emis à chaque changement de valeur de forçage à partir de la table de sorties.
  * (Uniquement pour des données binaires) */
  void tableForcingChanged(int);
  /** Emis à chaque changement de valeur de forçage à partir de la table de sorties.
  * (Uniquement pour des données numériques) */
  void tableForcingChanged(double);

  void formatUpdated();
  void labelUpdated();

  /** Emis au déclenchement du trigger. */
  void trigger();
  /** Emis pour signaler l'annulation du trigger actif. */
  void triggerCancel();

public slots: // Public slots
  /** Saisie le format d'affichage
   * @param format index du format dans l'application. */
  virtual void setFormat(int format);
  /** Saisie le format d'affichage.
   * @param format format d'affichage source. */
  virtual void setFormat(CYFormat *format);
  /** Saisie la donnée de mise à jour du format d'affichage.
   * A chaque changement de format de la donnée ayant pour nom \a name le format est automatiquement reporté sur la donnée. */
  virtual void setFormatData(QString name);
  /** Saisie la donnée de mise à jour du format d'affichage.
   * A chaque changement de format de la donnée \a data le format est automatiquement reporté sur la donnée. */
  virtual void setFormatData(CYData *data);
  /** Mise à jour du format d'affichage par rapport à la donnée de mise à jour du format d'affichage. */
  virtual void updateFormatData();
  /** Copie dans la donnée le format et le libellé de la donnée \a src.
   * @param src Donnée source contenant le libellé et la source. */
  virtual void copyFormatLabel(CYData *src);
  /** Saisie l'unité. */
  virtual void setUnit(const QString u);
  /** Saisie le type physique. */
  virtual void setPhysical(const QString p);
  /** Saisie le nombre de chiffres total. */
  virtual void setNbDigit(int nb);
  /** Saisie le nombre de chiffres après la virgule. */
  virtual void setNbDec(int nb);
  /** Saisie la précision si la valeur est numérique.
   * @param precision  : 0.2, 1.0, 5.0, 10.0, 1, 5, 10 etc... */
  virtual void setPrecision(const double precision);

  /** Saisie le mode d'utilisation. */
  virtual void setMode(Mode m) { mMode = m; }

  /** Saisie le groupe d'appartenance de la donnée. */
  virtual void setGroup(const QString g);
  /** Saisie l'étiquette de la donnée. */
  virtual void setLabel(const QString l);
  /** Saisie le message d'aide. */
  virtual void setHelp(const QString h);
  /** Saisie une note à rajouter dans l'aide. */
  virtual void addNote(const QString n) { mNotes.append(n); }
  /** Saisie les informations constructeur. */
  virtual void setInfoCY(const QString i) { mInfoCY = i; }
  /** Saisie le repère électrique. */
  virtual void setElec(const char *txt) { mElec = txt; }
  /** Saisie le repère électrique. */
  virtual void setElec(QString txt) { mElec = txt; }
  /** Saisie le repère physique. */
  virtual void setPhys(const char *txt) { mPhys = txt; }
  /** Saisie le repère physique. */
  virtual void setPhys(QString txt) { mPhys = txt; }
  /** Saisie l'addresse. */
  virtual void setAddr(const char *txt) { mAddr = txt; }
  /** Saisie l'addresse. */
  virtual void setAddr(QString txt) { mAddr = txt; }

  /** Saisie le le choix texte de désactivation d'une donnée flag. */
  virtual void setChoiceTrue(QString txt) { setChoiceLabel(true, txt); mChoicetrue = txt; }
  /** Saisie le le choix texte de désactivation d'une donnée flag. */
  virtual void setChoiceFalse(QString txt) { setChoiceLabel(false, txt); mChoicefalse = txt; }

  /** Charge la valeur sauvegardée de la donnée. */
  virtual void load();
  /** Sauvegarde la valeur de la donnée. */
  virtual void save();
  /** Initialise l'ancienne valeur. */
  virtual void initOldVal();
  /** Charge l'ancienne valeur. */
  virtual void loadOldVal();
  /** Cette donnée est toujours valide. */
  virtual void setAlwaysOk(bool state);
  /** @return \a true si cette donnée est toujours valide. */
  virtual bool alwaysOk() { return mAlwaysOk; }
  /** Saisir \a true pour que cette donnée ne soit pas enregistrée sur le disque dur. */
  virtual void setVolatile(bool val) { mVolatile = val; }
  /** @return \a true si cette donnée n'est pas enregistrée sur le disque dur. */
  virtual bool isVolatile() { return mVolatile; }

  /** @return \a true si la base de donnée est en cours de lecture. */
  bool reading();

  /** @return \a true si la donnée est valide. */
  bool isValid();

  /** Copie la valeur de la donnée ayant pour nom \a srcDataName dans la donnée ayant pour nom \destDataName.
   * @param limit Si \a true copie uniquement si la valeur de la donnée source est dans les limites de la donnée destination. Sinon retourne un message d'erreur et force à la valeur designer.  */
  static void copy(QString srcDataName, QString destDataName, bool limit=true);
  /** Copie la valeur de la donnée \a src dans la donnée \dest.
   * @param limit Si \a true copie uniquement si la valeur de la donnée source est dans les limites de la donnée destination. Sinon retourne un message d'erreur et force à la valeur designer.  */
  static void copy(CYData *src, CYData *dest, bool limit=true);
  /** Copie la valeur de la donnée \a src.
   * @param src   Donnée source.
   * @param limit Si \a true copie uniquement si la valeur de la donnée source est dans les limites de la donnée. Sinon retourne un message d'erreur et force à la valeur designer.  */
  virtual void copyFrom(CYData *src, bool limit=true);

  /** Copie la valeur de la donnée \a src.
   * @param src       Donnée source.
   * @param error     Message d'erreur. @return "" si la copie s'est bien passé.
   * @param designer  Si \a true remplace en cas de hors bornes la nouvelle valeur par la valeur designer sinon la valeur reste la même que celle avant la copie. Cette fonctionnalité n'est valable uniquement pour les valeurs bornées.
   * @param limit     Si \a true copie uniquement si la valeur de la donnée source est dans les limites de la donnée. Sinon retourne un message d'erreur et force à la valeur designer si le paramètre relatif est à \a true.  */
  virtual void copyFrom(CYData *src, QString &error, bool designer=false, bool limit=true);

  /** Détection de changement de valeur. */
  virtual bool detectValueChanged() { return false; }
  /** @return le résultat de la dernière détection de changement de valeur. */
  virtual bool modified() { return mModified; }

  /** @return la donnée de RAZ. @see mResetData. */
  virtual CYData *resetData() { return mResetData; }
  /** Saisie la donnée de RAZ. @see mResetData. */
  virtual void setResetData(CYData *data) { mResetData = data; }

  /** @return la donnée partielle. @see mPartialData. */
  virtual CYData *partialData() { return mPartialData; }
  /** Saisie la donnée partielle. @see mPartialData. */
  virtual void setPartialData(CYData *data) { mPartialData = data; }

  /** @return la donnée du seuil d'alerte du compteur partiel partiel. @see mThresholdData. */
  virtual CYData *thresholdData() { return mThresholdData; }
  /** Saisie la donnée du seuil d'alerte du compteur partiel partiel. @see mThresholdData. */
  virtual void setThresholdData(CYData *data) { mThresholdData = data; }

  /** @return la donnée note. @see mNoteData. */
  virtual CYString *noteData() { return mNoteData; }
  /** Saisie la donnée note. @see mNoteData. */
  virtual void setNoteData(CYString *data) { mNoteData = data; }
  virtual void setNumBase( NumBase val );

  /** Active ou non l'affichage en aide la liste des définitions disponibles relatives aux différentes valeurs possibles.
    @see CYComboBox. */
  virtual void setChoiceDictHelp(bool enable) { mChoiceDictHelp = enable; }
  /** @return l'activation ou non de l'affichage en aide la liste des définitions disponibles relatives aux différentes valeurs possibles.
    @see CYComboBox. */
  virtual bool choiceDictHelp() { return mChoiceDictHelp; }

  /** Active le trigger
   * @param level seuil de déclenchement du trigger
   * @param pos   pré-trigger
   * @param slop  choix de rampe de déclenchement */
  virtual void startTrigger(double level, double post, Cy::Change slope);
  /** Arrête le trigger */
  virtual void stopTrigger();
  /** Saisie la donnée de déclenchement du trigger */
  virtual void setExtTrigger(CYData *data);
  virtual void initTrigger();

  virtual void changeBuffer(int size, float time, int sample);

  /** Activation ou non des FIFO de la CYNum ou de ses enfants. */
  virtual void setEnableFIFO(bool val) { Q_UNUSED(val); }

  /** Initialise la bufferisation discontinue */
  void initDiscontBuffer();
  /** Libère le buffer courant et y copie le buffer discontinu tampon.
   *  Permet de passer à la lecture de la nouvelle bufferisation qui peut
   *  avoir déjà commencée en écriture dans le buffer tampon pendant le
   *  traitement de la buferisation précécente. */
  void releaseDiscontBuffer();

  /** Choisi le type de simulation.
   *  Permet d'activer ou non la simulation d'un signal pour afficher par exemple en oscilloscope un sinus lors de démo ou pour illustrer le manuel.
   *  Par ailleurs, en cas de simultion, la valeur simulée et bufferisée devient également la valeur courante affichée par exemple dans les afficheurs simples. */
  void setSimulType(SimulType type) { mSimulType = type; }
    /** @return le type de simulation. */
  SimulType simulType() { return mSimulType; }
  /** Saisie la valeur maximale de simulation. */
  void setSimulMax(double val);
  /** Saisie la valeur minimale de simulation. */
  void setSimulMin(double val);
  /** Saisie la période de simulation en ms. */
  void setSimulPeriode(double val);
  /** Saisie le déphasage de simulation pour déphaser une courbe par rapport à une autre. */
  void setSimulPhase(double val);
  /** Coefficient d'amplitude de simulation courbe pour modifier amplitude par défaut. */
  void setSimulkA(double val);

  /** Remplissage manuel du buffer */
  inline void setManualBuffer(const bool &val) { mManualBuffer = val; }

  /// Saisie le complément d'information ajouté au début des libellés des données lors de leur modification.
  virtual void setPrefixLabel(const QString &txt) { mPrefixLabel = txt; }
  /// Saisie le complément d'information ajouté à la fin des libellés des données lors de leur modification.
  virtual void setSuffixLabel(const QString &txt) { mSuffixLabel = txt; }

  /**
   * @brief Ajout d'un contrôle d'augmentation de valeur pour éviter une saisie trop butale.
   * @param limit Augmentation max en % par rapport à la valeur courante. La valeur par défaut à 0 inhibe ce contrôle.
   * @see variationTooAbrupt()
   */
  virtual void setLimitedIncrease(const double &limit) { mLimitedIncrease = limit; }
  /**
   * @brief Ajout d'un contrôle de diminution de valeur pour éviter une saisie trop butale.
   * @param limit Diminution max en % par rapport à la valeur courante. La valeur par défaut à 0 inhibe ce contrôle.
   * @see variationTooAbrupt()
   */
  virtual void setLimitedDecrease(const double &limit) { mLimitedDecrease = limit; }

protected: // Protected methods
  /** Initialise le type. */
  virtual void initType();
  /** Saisie le résultat de la détection de changement de valeur. */
  virtual void setValueChanged(bool state);

private: // Private methods
  /** Insert la donnée dans la base globale et dans la base de sa structure réseau. */
  void addToDB();

public: // Public attributes

  /** Liste des définitions disponibles relatives aux différentes valeurs possibles.
    @see CYComboBox. */
  QMap<int, CYDataChoice*> choiceDict;

  /** Dictionnaires de chaînes de caractères supplémentaires. */
  QMap<QString, QString*> stringsDict;

  /** @return l'état de bufferisation discontinue */
  BufferState discontBufferState;

  /**
   * @brief Test si une nouvelle valeur ne génère pas un trop grand changement.
   * @param newVal Nouvelle valeur à tester
   * @return @true si la variation est trop butale.
   */
  virtual bool variationTooAbrupt(double newVal);
  
protected: // Protected attributes
  /** Fichier de configuration. */
  QSettings *mCfg;
  /** Base de données de rattachement. */
  CYDB *mDB;
  /** Donnée utilisée pour le mode forçage. */
  CYData *mDataForcing;
  /** Donnée adc 16bits (bit de parité inclu). */
  CYAdc16 *mAdc16;
  /** Donnée adc 32bits (bit de parité inclu). */
  CYAdc32 *mAdc32;
  /** Donnée numérique non calibrée. */
  CYMeasure *mDirect;
  /** Flag de validité. */
  CYFlag *mFl;

  /** Si \a true cette donnée est toujours valide. */
  bool mAlwaysOk;
  /** Donnée qui n'est jamais enregistrée sur le disque dur. */
  bool mVolatile;

  /** Format d'affichage. */
  CYFormat *mFormat;
  /** Numéro du format d'affichage dans l'application. */
  int mNumFormat;
  /** Format d'affichage d'origine. */
  CYFormat *mSenderFormat;
  /** Mesure de mise à jour du format d'affichage. */
  CYData *mFormatData;

  /** Mode d'utilisation. */
  Mode mMode;
  /** Type de valeur. */
  ValueType mType;
  /** Type de valeur en chaîne de caractères. */
  QString mTypesString;

  /** Vaut \a true si c'est une donnée numérique. */
  bool mIsNum;
  /** Vaut \a true si c'est une donnée entière. */
  bool mIsInt;
  /** Vaut \a true si c'est une donnée signée. */
  bool mIsSigned;
  /** Vaut \a true si c'est une donnée compteur. */
  bool mIsCnt;
  /** Vaut \a true si c'est une donnée timer. */
  bool mIsTim;
  /** Vaut \a true si c'est une donnée temporaire d'un compteur/timer. */
  bool mIsTmp;
  /** Vaut \a true si c'est une donnée flag. */
  bool mIsFlag;

  /** Buffer d'acquisition. */
  QVector<double> mBuffer;
  /** Utilisation du buffer d'acquisition. */
  bool mUseBuffer;
  double mInitBufferVal;
  /** Temps écoulé depuis la dernière bufferisation. */
  QElapsedTimer mTimerBuffer;
  /** Compensation du timer de bufferisation */
  double mTimerBufferCompensation;
  /** Index de la donnée courante dans le buffer. */
  uint mIdBuffer;
  /** Etat de remplissage du buffer. */
  bool mBufferFull;
  /** Taille du buffer. */
  uint mBufferSize;
  /** Taille d'un échantillon. */
  uint mSampleSize;

  /** Donnée donnant le nombre de valeurs prêtes à bufferiser d'un tableau de valeurs. */
  CYS32 *mTableBufferNb;
  /** Nombre de valeurs bufferisées à partir d'un tableau de valeurs. */
  uint mTableBufferCpt;
  /** Etat de bufferisation d'un tableau de valeurs. */
  bool mTableBuffering;

  /** Buffer tampon d'acquisition discontinue. */
  QVector<double> mDiscontBuffer;
  /** Bufferisation discontinue de rattachement. */
  CYDiscontBuffering *mDiscontBuffering;
  /** Index de la donnée courante en lecture dans le buffer. */
  uint mDiscontBufferId;
  /** Numéro de la bufferisation discontinue en cours. */
  uint mDiscontBufferNum;

  /** Buffer d'acquisition par rafales. */
  QVector<double> mBurstsBuffer;
  /** Flag d'activation du buffer d'acquisition par rafales. */
  CYFlag *mBurstsBufferFlag;
  /** Taille du buffer d'acquisition par rafales. */
  uint mBurstsBufferSize;
  /** Rapport période d'acquisition rapide / période d'acquisition par rafales. */
  uint mBurstsRatio;
  /** Index de la donnée courante dans le buffer d'acquisition par rafales. */
  uint mIdBurstsBuffer;
  /** Nombre d'échantillons insérés dans le buffer d'acquisition par rafales depuis le dernier échantillon inséré dans l'autre buffer. */
  uint mCptBurstsBuffer;
  /** Etat actif ou non du buffer d'acquisition par rafales. */
  bool mBurstsBufferState;
  /** Etat de remplissage du buffer d'acquisition par rafales. */
  bool mBurstsBufferFull;

  /** Remplissage manuel du buffer */
  bool mManualBuffer;

  /** Groupe d'appartenance. */
  QString mGroup;
  /** Etiquette. */
  QString mLabel;
  /** Message d'aide. */
  QString mHelp;
  /** Notes à rajouter dans l'aide. */
  QStringList mNotes;
  /** Informations constructeur. */
  QString mInfoCY;
  /** Repère électrique. */
  QString mElec;
  /** Repère physique. */
  QString mPhys;
  /** Addresse. */
  QString mAddr;

  /** Choix texte d'activation d'une donnée flag. */
  QString mChoicetrue;
  /** Choix texte de désactivation d'une donnée flag. */
  QString mChoicefalse;

  /** Couleur d'affichage utilisée lorsque la valeur est valide.  */
  QColor mEnableColor;
  /** Couleur d'affichage utilisée lorsque la valeur n'est pas valide.  */
  QColor mDisableColor;
  /** Couleur de saisie.  */
  QColor mInputColor;

  /** Résultat de la dernière détection de changement de valeur. */
  bool mModified;
  /** Indique lors d'un changement de valeur si la nouvelle valeur est plus grande que l'ancienne. */
  bool mRising;

  /** Indique s'il s'agit de valeurs mini, maxi générées automatiquement.
  * Utile que pour les données numériques.  */
  bool mAutoMinMax;
  /** Bornes les min max calculés par rapport au format.*/
  bool mAutoMinMaxFormat;

  /** Infos profibus. */
  CYProfibusItem *mProfibusInfo;
  /** Index d'une donnée référencée par index. */
  int mIndex;

  /** Valeur temporaire lorsqu'il s'agit d'une donnée numérique. */
  double mTmp;

  /** Donnée utile pour les données de type compteurs de maintenance. Cette donnée est associée à la donnée à la quelle elle appartient pour exécuter une RAZ de la donnée partielle. */
  CYData *mResetData;
  /** Donnée utile pour les données de type compteurs de maintenance. C'est sur cette donnée qu'agit la RAZ. */
  CYData *mPartialData;
  /** Donnée utile pour les données de type compteurs de maintenance. C'est cette donnée qui permet de définir le seuil d'alerte du compteur partiel de maintenance. */
  CYData *mThresholdData;
  /** Donnée pouvant contenir un commentaire saisie par l'utilisateur à propos de la donnée à laquelle elle appartient. */
  CYString *mNoteData;
  bool mHelpDesignerValue;
  bool mHelpDesignerValueFromDB;
  NumBase mNumBase;

  /** Évènements relatifs à cette donnée. */
  QList<CYEvent*> mEvents;

  bool mNotSpecialValueText;
  /** Affiche en aide la liste des définitions disponibles relatives aux différentes valeurs possibles.
    @see CYComboBox. */
  bool mChoiceDictHelp;

  bool mTriggerEnabled;
  double mTriggerLevel;
  double mPostTrigger;
  bool mTriggerStarted;
  double mTriggerPrec;
  Cy::Change mTriggerSlope;
  /** Buffer d'acquisition du trigger. */
  QVector<double> mTriggerBuffer;
  /** Index dans le buffer de la valeur de déclenchement du trigger. */
  uint mTriggerIdBuffer;
  /** Index dans le buffer de la valeur de déclenchement du trigger décalé du post-trigger. */
  uint mPostTriggerIdBuffer;
  /** Compteur de remplissage du buffer trigger après déclenchement. */
  uint mTriggerBufferCpt;
  /** Etat actif ou non du trigger. */
  bool mTriggerState;
  /** Etat de remplissage du buffer d'acquisition du trigger. */
  bool mTriggerBufferFull;
  CYData *mExtTrigger;

  /** Type de simulation. */
  SimulType mSimulType;
  /** Valeur maximale de simulation. */
  double mSimulMax;
  /** Valeur minimale de simulation. */
  double mSimulMin;
  /** Période de simulation en ms . */
  double mSimulPeriode;
  /** Déphasage de simulation pour déphaser une courbe par rapport à une autre. */
  double mSimulPhase;
  /** Coefficient d'amplitude de simulation courbe pour modifier amplitude par défaut. */
  double mSimulkA;

  /// Complément d'information ajouté au début des libellés des données lors de leur modification.
  QString mPrefixLabel;
  /// Complément d'information ajouté à la fin des libellés des données lors de leur modification.
  QString mSuffixLabel;

  /// @see setLimitedIncrease();
  double mLimitedIncrease;
  /// @see setLimitedDecrease();
  double mLimitedDecrease;
};

#endif
