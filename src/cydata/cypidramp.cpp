/***************************************************************************
                          cypidramp.cpp  -  description
                             -------------------
    début                  : lun avr 28 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cypidramp.h"

CYPIDRamp::CYPIDRamp(CYDB *db, const QString &name, flg *val, const flg def)
  : CYFlag(db, name, val, def)
{
  mLabel = tr("Ramp on PID setpoint");
  setHelp("",
          tr("The PID setpoint will be a ramp form one."),
          tr("It will be the final value (No ramp)."));
}

CYPIDRamp::~CYPIDRamp()
{
}
