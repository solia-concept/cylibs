/***************************************************************************
                          cypidmax.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cypidmax.h"

CYPIDMax::CYPIDMax(CYDB *db, const QString &name, f32 *val, const f32 def, const f32 min, const f32 max, int format)
  : CYF32(db, name, val, def, min, max, format)
{
  mLabel = tr("Positive output limit");
  mHelp  = tr("Output limit.");
}

CYPIDMax::~CYPIDMax()
{
}
