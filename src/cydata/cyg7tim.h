/***************************************************************************
                          cyg7tim.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYG7TIM_H
#define CYG7TIM_H

// CYLIBS
#include "cytime.h"

/** CYG7Tim est un timer grafcet.
  * @short Compteur grafect.
  * @author Gérald LE CLEACH
  */

class CYG7Tim : public CYTime
{
  Q_OBJECT
public:
  /** Attr est la structure de saisie des attributs d'un timer grafcet. */
  struct Attr
  {
    /** Nom. */
    const QString &name;
    /** Valeur de la donnée. */
    u32 *val;
    /** Mode d'affichage. */
    Cy::Mode mode;
    /** Format d'affichage temporelle. */
    Cy::TimeFormat format;
    /** Base temporelle. */
    double base;
    /** Numéro de grafcet. */
    int grafcet;
    /** Numéro du timer. */
    int no;
    /** Messsage d'aide. */
    QString help;
  };

  /** Création d'un timer de grafcet.
    * @param db    Base de données parent.
    * @param attr  Attributs du timer.
    * @param name  Nom du timer. */
  CYG7Tim(CYDB *db, const QString &name, Attr *attr);
  
  ~CYG7Tim();

  /** @return le numéro de grafcet. */
  int grafcet() { return mG7; }
  /** @return le numéro du timer. */
  int no() { return mNo; }

private: // Private attributes
  /** Numéro de grafcet. */
  int mG7;
  /** Numéro du timer. */
  int mNo;
};

#endif
