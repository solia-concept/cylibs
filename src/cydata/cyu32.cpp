/***************************************************************************
                          cyu32.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyu32.h"

CYU32::CYU32(CYDB *db, const QString &name, int format, Mode mode)
  : CYNum<u32>(db, name, format, mode, VU32)
{
}

CYU32::CYU32(CYDB *db, const QString &name, u32 *val, int format, Mode mode)
  : CYNum<u32>(db, name, val, format, mode, VU32)
{
}

CYU32::CYU32(CYDB *db, const QString &name, u32 *val, const u32 def, int format, Mode mode)
  : CYNum<u32>(db, name, val, def, format, mode, VU32)
{
}

CYU32::CYU32(CYDB *db, const QString &name, u32 *val, const u32 def, const u32 min, const u32 max, int format, Mode mode)
  : CYNum<u32>(db, name, val, def, min, max, format, mode, VU32)
{
}

CYU32::CYU32(CYDB *db, const QString &name, CYNum<u32>::Attr1<u32> *attr)
  : CYNum<u32>(db, name, attr, VU32)
{
}

CYU32::CYU32(CYDB *db, const QString &name, CYNum<u32>::Attr2<u32> *attr)
  : CYNum<u32>(db, name, attr, VU32)
{
}

CYU32::CYU32(CYDB *db, const QString &name, CYNum<u32>::Attr3<u32> *attr)
  : CYNum<u32>(db, name, attr, VU32)
{
}

CYU32::CYU32(CYDB *db, const QString &name, CYNum<u32>::Attr4<u32> *attr)
  : CYNum<u32>(db, name, attr, VU32)
{
}

CYU32::~CYU32()
{
}


int CYU32::nbDigit(Cy::NumBase base)
{
  return toString(max(), false, true, base).length();
}


QString CYU32::toString(bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  return toString(val(), withUnit, ctrlValid, base);
}


/*! @return la valeur \a val en chaîne de caractères suivant le format et l'unité.
    \fn CYU32::toString(double val, bool withUnit, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData)
 */
QString CYU32::toString(double val, bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  Cy::NumBase b;
  if ( base!=Cy::BaseData )
    b = base;
  else
    b = mNumBase;

  QString suffix;
  if (withUnit)
    suffix.append(" "+unit());

  if (ctrlValid && !isValid())
    return QString("%1%2").arg(stringDef()).arg(suffix);
  else
  {
    if (mFormat->isExponential())
      return QString("%1e%2%3").arg(mantissa(val), 0, 'f', nbDec()).arg(exponent(val)).arg(suffix);
    else if ( b!=Cy::Decimal )
    {
      QString num = QString("%1").arg((u32)val, 0, b).toUpper();
      QString zero;
      QString maxTxt = QString("%1").arg((u32)max(), 0, b).toUpper();
      zero.fill('0', maxTxt.length()-num.length());
      QString txt = QString("%1%2%3").arg(zero).arg(num).arg(suffix);
      return txt;
    }
    else
      return QString("%1%2").arg(val, 0, 'f', 0).arg(suffix);
  }
}
