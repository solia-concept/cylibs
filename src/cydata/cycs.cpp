//
// C++ Implementation: cycs
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cycs.h"

// CYLIBS
#include "cycore.h"
#include "cytime.h"
#include "cycommand.h"
#include "cystring.h"
#include "cydblibs.h"

CYCS::CYCS(CYDB *db, const QString &name, t_cu32 *cnt, Attr1 *attr)
  : CYDatasGroup(db, name, attr->idNet, attr->group)
{
  mLabel = attr->label;

  mTotalData = new CYTime(db, QString("TOTAL_%1").arg(name), &(cnt->val), Cy::HOUR_MIN_SEC, BASE_SEC, attr->mode);
  mTotalData->setLabel(mLabel);

  mPartialData = new CYTime(db, QString("PARTIAL_%1").arg(name), &(cnt->tmp), Cy::HOUR_MIN_SEC, BASE_SEC, attr->mode);
  mPartialData->setLabel(mLabel);

  mThresholdData = new CYTime(db, QString("THRESHOLD_%1").arg(name), new u32, Cy::HOUR, BASE_SEC, attr->mode);
  mThresholdData->setLabel(mLabel);

  mNoteData = new CYString(db, QString("NOTE_%1").arg(name));
  mNoteData->setLabel(mLabel);

  init();
}

CYCS::~CYCS()
{
}

/*!
    \fn CYCnt::init()
 */
void CYCS::init()
{
  if ( mGroup.isEmpty() )
    mGroup = tr("Maintenance")+':'+tr("Time counter");
  else
    mGroup = mGroup+':'+tr("Maintenance")+':'+tr("Time counter");

  mHelp  = tr("Counter used for maintenance");

  mTotalData->setGroup(mGroup+':'+tr("Total"));
  mTotalData->setHelp(mHelp);

  mPartialData->setGroup(mGroup+':'+tr("Partial"));
  mPartialData->setHelp(mHelp);

  mThresholdData->setGroup(mGroup+':'+tr("Alert threshold"));
  mThresholdData->setHelp(tr("An alert is generated if the partial counter reaches this value."));
  mThresholdData->addNote(tr("A null value disable the control."));

  mNoteData->setGroup(mGroup+':'+tr("Note"));
  mNoteData->setHelp(mHelp);

  mTotalData->setPartialData( mPartialData );
  mTotalData->setNoteData( mNoteData );
  mTotalData->setThresholdData( mThresholdData );

  QString from = tr("Maintenance");
  QString help, helpEnd;
  flg *val = new flg;
  (*val) = false;
  help = tr("Indicates that the partial counter reached the alert threshold. This threshold is settable in the maintenance counters window.");
  CYEvent *event = mPartialData->CYData::initAlert(core->eventsGeneratorSPV, QString("FA_THRESHOLD_%1").arg(objectName()), val, helpEnd, SI_1, REC|SCR|BST, QString(mGroup+":"+mLabel), from, 0, help);
  setCtrlEvent(event);
}


/*!
    \fn CYCS::initResetData(CYDB *db, flg *val)
 */
void CYCS::initResetData(CYDB *db, flg *val)
{
  mResetData = new CYCommand(db, QString("RESET_%1").arg(objectName()), val, Cy::Monostable);
  mResetData->setLabel(mLabel);
  mResetData->setGroup(mGroup+':'+tr("Reset"));
  mTotalData->setResetData( mResetData );
}


/*! @return la donnée de RAZ.
    \fn CYCS::resetData()
 */
CYCommand * CYCS::resetData()
{
  return mResetData;
}


void CYCS::setCtrlEvent(CYEvent *ev)
{
  ctrlEvent = ev;
  ctrlEvent->setReportUp(tr("Alert"));
  ctrlEvent->setLocal(true);
  QTimer *timer = new QTimer;
  timer->start(5000);
  connect(timer, SIGNAL(timeout()), SLOT(ctrlPartial()));
}


/*! Contrôle que le compteur partiel ne dépasse pas la valeur de contrôle
    \fn CYCS::ctrlPartial()
 */
void CYCS::ctrlPartial()
{
  if (!core->started() || !db()->linksOn())
    return;

  if (mThresholdData->val()==0)
    return;

  if (!ctrlEvent)
    return;

  if (mPartialData->val()>mThresholdData->val())
    ctrlEvent->setVal(true);
  else
    ctrlEvent->setVal(false);
}

