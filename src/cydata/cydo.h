/***************************************************************************
                          cydo.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDO_H
#define CYDO_H

// CYLIBS
#include "cydig.h"

/** CYDO est une sortie TOR.
  * @short Sortie TOR.
  * @author Gérald LE CLEACH
  */

class CYDO : public CYDig
{
  Q_OBJECT
public:
  /** Création d'une sortie TOR pointant sur un bit.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie TOR.
    * @param attr  Attributs de la sortie TOR.
    * @param c     Numéro de la carte TOR. */
  CYDO(CYDB *db, const QString &name, AttrBit *attr, int c=-1);
  
  /** Création d'une sortie TOR pointant sur un flag.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie TOR.
    * @param attr  Attributs de la sortie TOR.
    * @param c     Numéro de la carte TOR. */
  CYDO(CYDB *db, const QString &name, AttrFlag1 *attr, int c=-1);

  /** Création d'une sortie TOR pointant sur un flag.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie TOR.
    * @param attr  Attributs de la sortie TOR.
    * @param c     Numéro de la carte TOR. */
  CYDO(CYDB *db, const QString &name, AttrFlag2 *attr, int c=-1);

  /** Création d'une sortie TOR pour le mode forçage sur un bit.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie TOR en mode forçage.
    * @param word  Mot contenant la donnée.
    * @param bit   Bit de la donnée dans le mot.
    * @param c     Numéro de la carte TOR. */
  CYDO(CYDB *db, const QString &name, CYWord *word, short bit, int c=-1);
  
  /** Création d'une sortie TOR pour le mode forçage sur un bit.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie TOR en mode forçage.
    * @param flag  Flag donnant la valeur de la donnée.
    * @param c     Numéro de la carte TOR. */
  CYDO(CYDB *db, const QString &name, flg *flag, int c=-1);


  /** Création d'une sortie TOR pointant sur un flag.
    * @param db    Base de données parent.
    * @param name  Nom de la sortie TOR.
    * @param flag  Flag donnant la valeur de la donnée.
    * @param mode  Mode d'affichage.
    * @param elec  Repère électrique.
    * @param phys  Repère physique.
    * @param help  Message d'aide.
    * @param c     Numéro de la carte TOR. */
 CYDO(CYDB *db, const QString &name, flg *flag, Cy::Mode mode, QString elec, QString phys, QString help, int c=-1);

  ~CYDO();

  /** Configure la sortie en mode forçage.
    * @param forcing Donnée en mode forçage. */
  void initForcing(CYBool *forcing);
  /** Ecriture de la donnée de forçage. */
  virtual bool writeForcing();
  /** @retrurn la base de données de forçage. */
  virtual CYDB *dbForcing();

  /** Initialise la valeur de la donnée forçage. */
  virtual void initForcingValue();

  /** @return l'état de la valeur en mode forçage. */
  bool forcing();

public slots: // Public slots
  /** Saisie de la valeur en mode forçage. */
  void setForcing(bool val);
};

#endif
