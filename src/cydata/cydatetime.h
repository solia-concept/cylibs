//
// C++ Interface: cydate
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDATETIME_H
#define CYDATETIME_H

// QT
#include <qdatetime.h>
// CYLIBS
#include "cys64.h"

/**
  @short Donnée d'horodatage enregistrable en s64.
  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYDateTime : public CYS64, public QDateTime
{
Q_OBJECT
public:
  /** Attr est la structure de saisie des attributs d'une donnée alpha-numérique. */
  struct AttrString
  {
    /** Nom. */
    const QString &name;
    /** Pointe sur la chaine de caractères. */
    char *ptrChar;
    /** Nombre de caractères. */
    int nbChar;
    /** Base de temps en ms (ex 1000.0 = 1 sec). */
    double base;
    /** Mode d'utilisation. */
    Cy::Mode mode;
    /** Etiquette. */
    QString label;
  };

  /** Création d'une donnée date.
    * @param db       Base de données parent.
    * @param name     Nom de la donnée.
    * @param val      Valeur de la donnée.
    * @param def      Valeur par défaut en entier à renseigner par la valeur par défaut provenant du RN afin de ne pas surcharger les fichiers de sauvegarde .cydata.
    * @param min      Valeur mini (ex: "2008-10-16T00:00:00", "2008-10-16", "00:00:00")
    * @param max      Valeur maxi (ex: "2100-12-31T23:59:59", "2100-12-31", "23:59:59").
    * @param base     Base de temps en ms (ex 1000.0 = 1 sec).
    * @param label    Etiquette.
    * @param mode     Mode d'utilisation. */
  CYDateTime(CYDB *db, const QString &name, s64 *val, double base=BASE_SEC, s64 def=0, QString min="2000-01-01T00:00:00", QString max="2100-12-31T23:59:59", QString label = 0, Cy::Mode mode = Sys);

  /** Création d'une donnée date.
    * @param db       Base de données parent.
    * @param name     Nom de la donnée.
    * @param val      Valeur de la donnée.
    * @param def      Valeur par défaut (ex: "2008-10-16T10:50:00", "2008-10-16", "10:50:00").
    * @param min      Valeur mini (ex: "2008-10-16T00:00:00", "2008-10-16", "00:00:00").
    * @param max      Valeur maxi (ex: "2100-12-31T23:59:59", "2100-12-31", "23:59:59").
    * @param base     Base de temps en ms (ex 1000.0 = 1 sec).
    * @param label    Etiquette.
    * @param mode     Mode d'utilisation. */
  CYDateTime(CYDB *db, const QString &name, QString *val=new QString(QDateTime::currentDateTime().toString(Qt::ISODate)), QString def="2000-01-01T00:00:00", QString min="2000-01-01T00:00:00", QString max="2100-12-31T23:59:59", double base=BASE_SEC, QString label = 0, Cy::Mode mode = Sys);

  /** Création d'une donnée date.
    * @param db       Base de données parent.
    * @param name     Nom de la donnée.
    * @param ptrChar  Pointe sur la chaine de caractères donnant la date.
    * @param nbChar   Nombre de caractères.
    * @param base     Base de temps en ms (ex 1000.0 = 1 sec).
    * @param label    Etiquette.
    * @param mode     Mode d'utilisation. */
  CYDateTime(CYDB *db, const QString &name, char *ptrChar, int nbChar, double base=BASE_SEC, QString label = 0, Cy::Mode mode = Sys);

  /** Création d'une donnée date.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYDateTime(CYDB *db, const QString &name, AttrString *attr);

  ~CYDateTime();

  /** @return la date correspondant à la chaîne de caractères \a val */
  static QDateTime fromString(QString val);

  /** @return la valeur d'horodatage en s64 correspondant à \a dt suivant la base de temps.
     * @param base     Base de temps en ms (ex 1000.0 = 1 s).*/
  static s64 toSinceEpoch(const QDateTime dt, double base, bool *ok = nullptr);
  /** @return la valeur d'horodatage en s64 correspondant à \a dt suivant la base de temps. */
  virtual s64 toSinceEpoch(const QDateTime dt);
  /** @return la valeur d'horodatage en s64 correspondant à la chaîne de caractères \a txt suivant la base de temps. */
  virtual s64 toSinceEpoch(QString txt);
  /** Saisie la valeur d'horodatage \a val de QDateTime suivant la base de temps. */
  virtual void setSinceEpoch(const s64 val);

  /** @return la valeur en cours en chaîne de caractères (ex : 2024-01-18 09:00:00).
    * @param withUnit Pas utilisée dans cette classe (permet uniquement d'assurrer l'enrichissement de la méthode).
    * @param base     Pas utilisée dans cette classe (permet uniquement d'assurrer l'enrichissement de la méthode).
    */
  QString toString(bool withUnit=false, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);
  /** @return la valeur \a val en chaîne de caractères (ex : 2024-01-18 09:00:00).
    * @param withUnit Pas utilisée dans cette classe (permet uniquement d'assurrer l'enrichissement de la méthode).
    * @param base     Pas utilisée dans cette classe (permet uniquement d'assurrer l'enrichissement de la méthode).
    */
  QString toString(double val, bool withUnit=false, bool ctrlValid=true, Cy::NumBase base=Cy::BaseData);

  /** Saisie une valeur par une chaine de caractères \a val. */
  virtual void setVal(QString val);
  /** Saisie une valeur par un QDateTime \a val. */
  virtual void setVal(QDateTime val);
  /** Saisie une valeur exprimée dans la base de temps de la donnée.*/
  virtual void setVal(s64 val);

  /** Saisie une valeur temporaire. */
  virtual void setTmp(const double val);

  /** @return la base de temps en ms (ex 1000.0 = 1 sec). */
  double base() { return mBase; }

private: // Private attributes
  /** Base de temps. */
  double mBase;
};

#endif
