/***************************************************************************
                          cypidmin.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYPIDMIN_H
#define CYPIDMIN_H

// CYLIBS
#include "cyf32.h"

/** CYPIDMin représente la puissance minimale d'un PID.
  * @short Puissance minimale de PID.
  * @author Gérald LE CLEACH
  */

class CYPIDMin : public CYF32
{
  Q_OBJECT
public:
  /** Création d'une puissance minimale de PID.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Limite inférieure.
    * @param max    Limite supérieure.
    * @param format Numéro du format d'affichage dans l'application. */
  CYPIDMin(CYDB *db, const QString &name, f32 *val, const f32 def, const f32 min, const f32 max, int format);

  ~CYPIDMin();
};

#endif
