/***************************************************************************
                          cyct.h  -  description
                             -------------------
    begin                : mar avr 13 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYCT_H
#define CYCT_H

// QT
#include <qobject.h>
// CYLIBS
#include "cydatasgroup.h"

class CYFlag;
class CYF32;

/** @short Base de contrôle tolérances.
  * @author LE CLÉACH Gérald
  */

class CYCT : public CYDatasGroup
{
  Q_OBJECT
public:
 /** Création d'un groupe de contrôle tolérances.
    * @param db               Base de données parent.
    * @param name             Nom du groupe (préfixe du nom des données).
    * @param idNet            Index réseau correspondant à l'index des valeurs dans la structure réseau.
    * @param values           Pointe sur les valeurs.
    * @param fl_arret_ale_def Valeur constructeur pour le défaut si temps maxi hors tolerance d'alerte atteint.
    * @param fl_arret_def_def Valeur constructeur pour le défaut si hors tolerance de défaut.
    * @param group            Groupe du contrôle (nom du groupe des données relatives).
    * @param format           Numéro du format d'affichage dans l'application de la mesure.
    * @param settings         Accès au paramètrage. */
  CYCT(CYDB *db, const QString &name, int idNet, t_ct *values, flg fl_arret_ale_def, flg fl_arret_def_def, QString group, int format=0, QString settings=0);

  ~CYCT();

  /** @return le flag d'authorisation de défaut si temps maxi hors tolérance d'alerte atteint. */
  CYFlag  *alerteStop() { return mAlerteStop; }
  /** @return le flag d'authorisation de défaut si hors tolérance de défaut. */
  CYFlag  *faultStop() { return mFaultStop; }

   /** Structure de saisie des attributs des états du mouvement. */
  struct AttrReading1
  {
    /** Pointe sur la valeur générant une alerte. */
    f32 *alert;
    /** Pointe sur la valeur générant un défaut. */
    f32 *fault;
    /** Numéro du format d'affichage dans l'application. */
    int format;
  };
  
  /** Initialisation des données de lecture.
    * @param db       Base de données parent.
    * @param alert    Pointe sur la valeur générant une alerte.
    * @param fault    Pointe sur la valeur générant un défaut. */
  void initReading(CYDB *db, f32 *alert, f32 *fault);

  /** Initialisation des données de lecture.
    * @param db       Base de données parent.
    * @param attr     Attributs des données de lecture. */
  void initReading(CYDB *db, AttrReading1 *attr);

  /** @return la donnée de lecture de la valeur générant une alerte. */
  CYF32 *alertReading() { return mAlertReading; }
  /** @return la donnée de lecture de la valeur générant un défaut. */
  CYF32 *faultReading() { return mFaultReading; }

  /** Initialise l'alerte sur max devenue défaut. */
  virtual CYEvent *initFaultAlertMax( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" > MAX (Max. alert)")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was higher than the maximum tolerated value, which produced an alert. Also The maximum time (or number of cycles) of alert has been reached and thus produced a fault."));
  /** Initialise l'alerte sur min devenue défaut. */
  virtual CYEvent *initFaultAlertMin( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" < MIN (Max. alert)")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was lower than the minimum tolerated value, which produced an alert. Also The maximum time (or number of cycles) of alert has been reached and thus produced a fault."));
  /** Initialise le défaut sur max. */
  virtual CYEvent *initFaultMax( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" > MAX: DATA1")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was higher than the maximum allowed value, which produced a fault."));
  /** Initialise le défaut sur min. */
  virtual CYEvent *initFaultMin( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" < MIN: DATA1")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was lower than the minimum allowed value, which produced a fault."));
  /** Initialise l'alerte sur max. */
  virtual CYEvent *initAlertMax( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" > MAX: DATA1"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was higher than the maximum tolerated value, which produced an alert.")+" "+tr("The value at which the alert occured is written in the message.")+" "+tr("The maximum measured value is written in the message of end of alert."));
  /** Initialise l'alerte sur min. */
  virtual CYEvent *initAlertMin( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" < MIN: DATA1"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was lower than the minimum tolerated value, which produced an alert.")+" "+tr("The value at which the alert occured is written in the message.")+" "+tr("The minimum measured value is written in the message of end of alert."));

public slots: // Public slots
  /** Saisie la donnée de mise à jour du format d'affichage.
    * A chaque changement de format de la donnée ayant pour nom \a name le format est automatiquement reporté sur 
    * les données générant une alerte et un défaut. */
  virtual void setFormatData(QString name);
  /** Saisie la donnée de mise à jour du format d'affichage.
    * A chaque changement de format de la donnée \a data le format est automatiquement reporté sur 
    * les données générant une alerte et un défaut. */
  virtual void setFormatData(CYData *data);

private: // Private methods
  /** Initialisation des données de paramètrage du mouvement. */
  void init();
  /** Initialisation des données de lecture. */
  void initReading();

protected: // Protected attributes
  /** Flag d'authorisation de défaut si temps maxi hors tolérance d'alerte atteint. */
  CYFlag  *mAlerteStop;
  /** Flag d'authorisation de défaut si hors tolérance de défaut. */
  CYFlag  *mFaultStop;

  /** Donnée de lecture de la valeur générant une alerte. */
  CYF32 *mAlertReading;
  /** Donnée de lecture de la valeur générant un défaut. */
  CYF32 *mFaultReading;

  /** Numéro du format d'affichage dans l'application de la mesure. */
  int mFormat;
};

#endif
