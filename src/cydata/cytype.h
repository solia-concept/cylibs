/***************************************************************************
                          cytype.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYTYPE_H
#define CYTYPE_H

// CYLIBS
#include "cytypes.h"
#include "cydata.h"

/** CYType est une classe générique capable de traîter les différents
  * types de valeurs possibles d'une donnée CY.
  * @short Donnée générique CY.
  * @author Gérald LE CLEACH
  */

template<class T>
class CYType : public CYData
{
public:
 /** Création d'une donnée.
   * @param db      Base de données parent.
   * @param name    Nom de la donnée.
   * @param format  Numéro du format d'affichage dans l'application.
   * @param mode    Mode d'utilisation.
   * @param type    Type de valeur. */
  CYType(CYDB *db, const QString &name, int format=0, Cy::Mode mode=Sys, Cy::ValueType type=Undef)
    : CYData(db, name, format, mode, type)
  {
    mVal = 0;
  }
  
  ~CYType()
  {
    if (mVal)
      delete mVal;
  }

  /** @return le pointeur sur la valeur. */
  T *ptrVal() { return mVal; }
  /** Saisie le pointeur sur la valeur. */
  void setPtrVal(T *val) { mVal = val; }

  /** @return la valeur. */
  virtual T val()
  {
    if (mVal==0)
      return 0;
    else
      return *mVal;
  }

  /** @return la valeur maxi si elle est numérique. */
  virtual T max() { return 0; }
  /** @return la valeur mini si elle est numérique. */
  virtual T min() { return 0; }
  /** @return la valeur par défaut du constructeur. */
  virtual T def() { return mDef; }

  /** Saisie une valeur. */
  virtual void setVal(T val) { *mVal = val; emit valueChanged(); }
  /** Saisie la valeur maxi si elle est numérique. */
  virtual void setMax(T max) { Q_UNUSED(max); }
  /** Saisie la valeur mini si elle est numérique. */
  virtual void setMin(T min) { Q_UNUSED(min); }
  /** Saisie la valeur par défaut du constructeur. */
  virtual void setDef(T def) { mDef = def; }
  /** Charge la valeur constructeur. */
  virtual void designer() { setVal(mDef); }
  /** Saisie l'ancienne valeur. */
  virtual void setOldVal(T val) { mOldVal = val; }
  /** @return l'ancienne valeur. */
  virtual T oldVal() { return mOldVal; }

  /** Initialise la donnée comme étant un donnée profibus. */
  virtual void initProfibus();

  /** Détection de changement de valeur. */
  virtual bool detectValueChanged()
  {
    bool ret = true;
    if ( mType == Cy::String )
    {
      if (*mVal==mOldVal)
        ret = false;
    }
    else if (mVal!=nullptr)
    {
      if (*mVal==mOldVal)
        ret = false;
      else if (*mVal>mOldVal)
        mRising = true;
      else
        mRising = false;
      mOldVal = *mVal;
      setValueChanged(ret);
    }
    return ret;
  }

  /** @return une ligne donnée pour le fichier d'export des données avec les 
    * champs suivants espacés d'une tabulation:
    * Hôte(s), connexion(s), groupe, nom, description, valeur, def, min, max, aide.
    * @see CYDB::exportDatas() */
  virtual QString exportData();

protected: // Protected attributes
  /** Valeur. */
  T *mVal;
  /** Valeur par défaut du constructeur. */
  T mDef;
  /** Ancienne valeur. */
  T mOldVal;
};

template<class T>
inline void CYType<T>::initProfibus()
{
  CYData::initProfibus((void *)mVal);
}

template<class T>
inline QString CYType<T>::exportData()
{
  QString values = QString("%1\t%2\t\t").arg(CYType<T>::toString()).arg(CYType<T>::def());
  QString link = CYType<T>::linksName().isEmpty() ? " " : CYType<T>::linksName();
  QString group = CYType<T>::group().isEmpty() ? " " : CYType<T>::group();

  QString line = QString("%1\t%2\t%3\t%4\t%5\t%6\t%7")
                .arg(CYType<T>::hostsName()).arg(link).arg(group)
                .arg(CYType<T>::objectName()).arg(CYType<T>::label()).arg(values).arg(CYType<T>::help());

  if (line.count("\t")>9)
    CYMESSAGETEXT(line);

  line.replace("\n", " ");
  line.append("\n");

  return line;
}


#endif
