//
// C++ Implementation: cypen
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

// QT
#include <QPainter>
// CYLIBS
#include "cypen.h"
#include "cyu8.h"
#include "cyu32.h"
#include "cycolor.h"

CYPen::CYPen(CYDB *db, const QString &name, QString label, QPen pen, Cy::Mode mode)
 : CYDatasGroup(db, name, -1, label)
{
  mStyle = new CYU8(mDB, QString("%1_STYLE").arg(name), new u8, (u8)pen.style());
  mStyle->setLabel(tr("Style"));

  QString path = QString("%1/cylibs/pics/").arg(qApp->applicationDirPath()).toUtf8();

  mStyle->setChoice(Qt::NoPen         , QPixmap(path+"nopen.png"         ), tr("No line at all")                          );
  mStyle->setChoice(Qt::SolidLine     , QPixmap(path+"solidline.png"     ), tr("A simple line")                           );
  mStyle->setChoice(Qt::DashLine      , QPixmap(path+"dashline.png"      ), tr("Dashes separated by a few pixels")        );
  mStyle->setChoice(Qt::DotLine       , QPixmap(path+"dotline.png"       ), tr("Dots separated by a few pixels")          );
  mStyle->setChoice(Qt::DashDotLine   , QPixmap(path+"dashdotline.png"   ), tr("Alternate dots and dashes")               );
  mStyle->setChoice(Qt::DashDotDotLine, QPixmap(path+"dashdotdotline.png"), tr("One dash, two dots, one dash, two dots")  );

  mWidth = new CYU32( db, QString("%1_WIDTH").arg(name), new u32, pen.width(), 0, 10, 0, mode );
  mWidth->setLabel( tr("Width") );

  mColor = new CYColor( db, QString("%1_COLOR").arg(name), new QColor(pen.color()), tr("Color"), mode );
}


CYPen::~CYPen()
{
}


/*! Dessine une image ayant pour taille \a size illustrant le crayon dans
    \fn CYPen::picture(QSize size)
 */
QPicture CYPen::picture(QSize size)
{
  QPicture pic;
  QPainter painter;
  painter.begin(&pic);
  QPen pen;
  pen.setColor( mColor->val() );
  pen.setWidth( mWidth->val() );
  pen.setStyle( (Qt::PenStyle)mStyle->val() );
  painter.setPen( pen );
  QRect rect = QRect(0, 0, size.width(), size.height());
  painter.drawLine(rect.left(), rect.center().y(), rect.right(), rect.center().y());
  painter.restore();
  painter.end();
  return pic;
}


/*! Saisie de la couleur.
    \fn CYPen::setColor( QColor color )
 */
void CYPen::setColor( QColor color )
{
    mColor->setVal( color );
}


/*! @return le crayon.
    \fn CYPen::pen()
 */
QPen CYPen::pen()
{
    return QPen( mColor->val(), mWidth->val(), (Qt::PenStyle)mStyle->val() );
}

/*! Force un nouveau crayon.
    \fn CYPen::setPen( QPen pen )
 */
void CYPen::setPen( QPen pen )
{
  mColor->setVal( pen.color() );
  mWidth->setVal( pen.width() );
  mStyle->setVal( pen.style() );
}
