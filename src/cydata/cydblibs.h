//
// C++ Interface: cydblibs
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDBLIBS_H
#define CYDBLIBS_H

// CYLIBS
#include <cydb.h>

class CYString;
class CYFlag;
class CYU32;
class CYU8;
class CYS8;
class CYU16;
class CYColor;

/**
@short Base de données propre à CYLIBS.

@author Gérald LE CLEACH
*/
class CYDBLibs : public CYDB
{
Q_OBJECT

public:
    CYDBLibs(QObject *parent = 0, const QString &name = 0);

    ~CYDBLibs();

protected:
    virtual void initProject();
    virtual void initTest();
    virtual void initDevel();
    virtual void initBackup();
    virtual void initEvents();
    virtual void initHelp();
    virtual void initUsers();
    virtual void initAcquisition();
    virtual void initMetrology();

protected:
    CYString * mString;
    CYFlag   * mFlag;
    CYU32    * mDU32;
    CYU8     * mDU8;
    CYS8     * mDS8;
    CYU16    * mDU16;
    CYColor  * mColor;
};

#endif
