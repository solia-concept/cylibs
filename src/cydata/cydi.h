/***************************************************************************
                          cydi.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYDI_H
#define CYDI_H

// CYLIBS
#include "cydig.h"

/** CYDI est une entrée TOR.
  * @short Entrée TOR.
  * @author Gérald LE CLEACH
  */

class CYDI : public CYDig
{
  Q_OBJECT
public:
  /** Création d'une entrée TOR pointant sur un bit.
    * @param db    Base de données parent.
    * @param name  Nom de l'entrée TOR.
    * @param attr  Attributs de l'entrée TOR.
    * @param c     Numéro de la carte TOR. */
  CYDI(CYDB *db, const QString &name, AttrBit *attr, int c=-1);

  /** Création d'une entrée TOR pointant sur un flag.
    * @param db    Base de données parent.
    * @param name  Nom de l'entrée TOR.
    * @param attr  Attributs de l'entrée TOR.
    * @param c     Numéro de la carte TOR. */
  CYDI(CYDB *db, const QString &name, AttrFlag1 *attr, int c=-1);

  /** Création d'une entrée TOR pointant sur un flag.
    * @param db    Base de données parent.
    * @param name  Nom de l'entrée TOR.
    * @param attr  Attributs de l'entrée TOR.
    * @param c     Numéro de la carte TOR. */
  CYDI(CYDB *db, const QString &name, AttrFlag2 *attr, int c=-1);

  /** Création d'une entrée TOR pointant sur un flag.
    * @param db    Base de données parent.
    * @param name  Nom de l'entrée TOR.
    * @param flag  Flag donnant la valeur de la donnée.
    * @param mode  Mode d'affichage.
    * @param elec  Repère électrique.
    * @param phys  Repère physique.
    * @param help  Message d'aide.
    * @param c     Numéro de la carte TOR. */
 CYDI(CYDB *db, const QString &name, flg *flag, Cy::Mode mode, QString elec, QString phys, QString help, int c=-1);

  ~CYDI();
};

#endif
