/***************************************************************************
                          cyflag.h  -  description
                             -------------------
    début                  : mer mai 7 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYFLAG_H
#define CYFLAG_H

// CYLIBS
#include "cynum.h"

/** @short Flag.
  * @author Gérald LE CLEACH
  */

class CYFlag : public CYNum<flg>
{
  Q_OBJECT

public:
  /** Attr1 est la structure de saisie des attributs d'un flag. */
  struct Attr1
  {
    /** Nom. */
    const QString &name;
    /** Pointe sur la valeur physique. */
    flg *val;
    /** Etiquette. */
    QString label;
  };

  /** Création d'un flag avec une valeur par défaut.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param def   Valeur par défaut.
    * @param mode Mode d'utilisation. */
  CYFlag(CYDB *db, const QString &name, flg *val, const flg def=0, Mode mode=Cy::Sys);

  /** Création d'un flag avec une valeur par défaut.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr Attributs de la donnée.
    * @param def   Valeur par défaut.
    * @param mode Mode d'utilisation. */
  CYFlag(CYDB *db, const QString &name, Attr1 *attr, const flg def=0, Mode mode=Cy::Sys);

  ~CYFlag();

  /** @return le type physique. */
  virtual QString physicalType();

  /** @return l'aide à la visualisation.
    * @param whole    Inutile ici.
    * @param oneLine  Aide uniquement composée de la première ligne.
		* @param withGroup	Aide avec le groupe de la donnée.
		* @param withValue	Aide avec la valeur courante. */
	virtual QString displayHelp(bool whole=true, bool oneLine=false, bool withGroup=true, bool withValue=false);
  /** @return l'aide à la saisie.
		* @param whole			Inutile ici.
		* @param withGroup	Aide avec le groupe de la donnée. */
  virtual QString inputHelp(bool whole=true, bool withGroup=true);

  /** @return le message d'aide formaté sur l'activation ou désactivation du flag. */
  virtual QString selectionHelp();

  /** Saisie l'aide de saisie.
    * @param help Message d'aide classique.
    * @param yes  Message d'aide d'activation du flag.
    * @param no   Message d'aide de désactivation du flag. */
  void setHelp(QString help="", QString yes="", QString no="");
  /** Saisie le message d'aide d'activation du flag. */
  void setHelpTrue(QString msg);
  /** Saisie le message d'aide de désactivation du flag. */
  void setHelpFalse(QString msg);

  /** Détection de changement de valeur.*/
  virtual bool detectValueChanged();

  /**
   * @brief Import contrôlé d'une valeur.
   * @param val Valeur à charger
   * Surchage de CYNum<T>::import pour ne pas forcer la valeur -1 d'un '#define TRUE -1' provenant du RN en min,
   * soit 0 qui correspondant alors à '#define FALSE 0'.
   */
  virtual void import(flg val);

private: // Private methods
  /** Initialise le flag. */
  void init();

protected: // Protected attributes
  /** Message d'aide d'activation du flag. */
  QString mHelptrue;
  /** Message d'aide de désactivation du flag. */
  QString mHelpfalse;
};

#endif
