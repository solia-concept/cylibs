//
// C++ Implementation: cydatasgroup
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cydatasgroup.h"

// CYLIBS
#include "cycore.h"

CYDatasGroup::CYDatasGroup(CYDB *db, const QString &name, int idNet, QString group, QString settings)
: QObject(db),
  mDB(db),
  mIdNet(idNet),
  mGroup(group),
  mSettings(settings)
{
  setObjectName(name);
}


CYDatasGroup::~CYDatasGroup()
{
}

/*! Renseigne le générateurs d'évènements relatifs à ce groupe de données
    \fn CYDatasGroup::setEventsGenerator(CYEventsGenerator *generator)
 */
void CYDatasGroup::setEventsGenerator(CYEventsGenerator *generator)
{
  QListIterator<CYEvent *> it(mEvents);
  while (it.hasNext())
  {
    CYEvent *event = it.next();
    event->setGenerator(generator);
  }
}

CYEvent *CYDatasGroup::initEvent( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = core->initEvent(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  mEvents.append(event);
  return event;
}


CYEvent *CYDatasGroup::initAlert( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = core->initAlert(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  mEvents.append(event);
  return event;
}


CYEvent *CYDatasGroup::initFault( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = core->initFault(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  mEvents.append(event);
  return event;
}
