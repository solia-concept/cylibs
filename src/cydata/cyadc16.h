/***************************************************************************
                          cyadc16.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYADC16_H
#define CYADC16_H

// CYLIBS
#include "cyana.h"
#include "cys16.h"

/** CYAdc16 est une entrée/sortie ANA en points 16bits (bit de parité inclu).
  * Elle est associée à sa donnée affichable en unité capteur: CYAna.
  * @short E/S ADC.
  * @author Gérald LE CLEACH
  */

class CYAdc16 : public CYS16
{  
  friend class CYAna;
public:
  /** Création d'une donnée ADC.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param ana   Donnée ANA en unité capteur.
    * @param attr  Attributs de la donnée.
    * @param c     Numéro de la carte. */
  CYAdc16(CYDB *db, const QString &name, CYAna *ana, CYAna::Attr16 *attr, int c=-1);

  /** Création d'une donnée ADC.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param ana   Donnée ANA en unité capteur.
    * @param attr  Attributs de la donnée.
    * @param c     Numéro de la carte. */
  CYAdc16(CYDB *db, const QString &name, CYAna *ana, CYAna::Attr16ADC *attr, int c=-1);

  /** Création d'une donnée ADC.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param ana   Donnée ANA en unité capteur.
    * @param val   Valeur de la donnée.
    * @param elec  Repère électrique.
    * @param phys  Repère physique.
    * @param mode  Mode d'affichage.
    * @param label Etiquette.
    * @param c     Numéro de la carte. */
  CYAdc16(CYDB *db, const QString &name, CYAna *ana, s16 *val, const char *elec, const char *phys, Cy::Mode mode, QString label, int c=-1);

  /** Création d'une donnée ADC pouvant être étalonnée en métrologie.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param ana   Donnée ANA en unité capteur.
    * @param attr  Attributs de la donnée.
    * @param c     Numéro de la carte. */
  CYAdc16(CYDB *db, const QString &name, CYAna *ana, CYAna::Attr16Metro *attr, int c=-1);

  ~CYAdc16();

  /** Saisie le pointeur sur les valeurs de calibrage. */
  void setPtrCal(t_calana16 *vcal);
  /** Change les pourcentages de seuil.
    * @param tmax  Pourcentage seuil haut maximum.
    * @param tmin  Pourcentage seuil haut minimum.
    * @param lmax  Pourcentage seuil bas maximum.
    * @param lmin  Pourcentage seuil bas minimum. */
  void changeLevel(f32 tmax=102.0, f32 tmin=70.0, f32 lmax=30.0, f32 lmin=-2.0);
  /** Fixe les limites de saisie des valeurs de calibrage. */
  void setCtrCal();

  /** @return le numéro de la carte. */
  int card() { return mCard; }

  /** @return la donnée ANA. */
  CYAna *ana() { return mAna; }
  /** @return la donnée du seuil haut. */
  CYS16 *top() { return mTop; }
  /** @return la donnée du seuil bas. */
  CYS16 *low() { return mLow; }

protected: // Protected attributes
  /** Donnée ADC affichée. */
  CYAna *mAna;
  /** Donnée du seuil haut ADC. */
  CYS16 *mTop;
  /** Donnée du seuil bas ADC. */
  CYS16 *mLow;

  /** Seuil ADC bas par défaut (constructeur). */
  f32 mLDef;
  /** Seuil ADC haut par défaut (constructeur). */
  f32 mTDef;

  /** Numéro de carte. */
  int mCard;
  /** Pourcentage seuil bas ADC minimum. */
  f32 mLmin;
  /** Pourcentage seuil bas ADC maximum. */
  f32 mLmax;
  /** Pourcentage seuil haut ADC minimum. */
  f32 mTmin;
  /** Pourcentage seuil haut ADC maximum. */
  f32 mTmax;
};

#endif
