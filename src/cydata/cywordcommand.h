/***************************************************************************
                          cywordcommand.h  -  description
                             -------------------
    début                  : jeu jun 19 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYWORDCOMMAND_H
#define CYWORDCOMMAND_H

// CYLIBS
#include <cyword.h>

/** CYWordCommand est un ensemble de 16 commandes bistables/monostables.
  * @short Ensemble de 16 commandes bistables/monostables.
  * @author Gérald LE CLEACH
  */

class CYWordCommand : public CYWord
{
public:
  /** Création d'un mot de 16 bits.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Pointe sur la valeur physique.
    * @param mode  Mode d'utilisation. */
  CYWordCommand(CYDB *db, const QString &name, u16 *val, Mode mode=Monostable);

  ~CYWordCommand();

  /** @return l'état de la commande \a no (0..15). */
  bool state(const unsigned short no);

public slots: // Public slots
  /** Met à l'état actif les commandes selon la valeur du mot. */
  void setOn();
  /** Met à l'état actif les commandes selon la valeur du mot qui vaut \a val. */
  void setOn(const unsigned short val);
  /** Met à l'état actif la commande \a no (0..15). */
  void setCmdOn(const unsigned short no);
  /** Met à l'état désactif la commande \a no (0..15). */
  void setCmdOff(const unsigned short no);
};

#endif
