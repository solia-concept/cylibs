/***************************************************************************
                          cydi.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// CYLIBS
#include "cydi.h"
#include "cydb.h"

CYDI::CYDI(CYDB *db, const QString &name, AttrBit *attr, int c)
  : CYDig(db, name, attr, c)
{
  mLabel = attr->elec;
  mElec  = attr->elec;
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Digital input"));
  if (mMode == Cy::Free)
  {
    mPhys  = tr("Unplugged");
    mHelp  = "-----";
  }
  else
  {
    mPhys  = attr->phys;
    mHelp  = attr->help;
  }
}

CYDI::CYDI(CYDB *db, const QString &name, AttrFlag1 *attr, int c)
  : CYDig(db, name, attr, c)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Digital input"));
  mLabel = attr->phys;
  mPhys  = attr->phys;
  mHelp  = attr->help;
}

CYDI::CYDI(CYDB *db, const QString &name, AttrFlag2 *attr, int c)
  : CYDig(db, name, attr, c)
{
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Digital input"));
  mLabel = attr->elec;
  mElec  = attr->elec;
  if (mMode == Cy::Free)
  {
    mPhys  = tr("Unplugged");
    mHelp  = "-----";
  }
  else
  {
    mPhys  = attr->phys;
    mHelp  = attr->help;
  }
}

CYDI::CYDI(CYDB *db, const QString &name, flg *flag, Cy::Mode mode, QString elec, QString phys, QString help, int c)
  : CYDig(db, name, flag, c)
{
  mMode=mode;
  if (!mDB->underGroup().isEmpty())
    setGroup(mDB->underGroup());
  else
    setGroup(tr("Digital input"));
  mLabel = elec;
  mElec  = elec;
  if (mMode == Cy::Free)
  {
    mPhys  = tr("Unplugged");
    mHelp  = "-----";
  }
  else
  {
    mPhys  = phys;
    mHelp  = help;
  }
}

CYDI::~CYDI()
{
}
