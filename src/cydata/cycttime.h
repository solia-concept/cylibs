/***************************************************************************
                          cycttime.h  -  description
                             -------------------
    begin                : mer nov 3 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYCTTIME_H
#define CYCTTIME_H

// QT
#include <QObject>
// CYLIBS
#include "cyct.h"

class CYTime;
class CYTSec;

/** @short Contrôle tolérances en temps.
  * @author LE CLÉACH Gérald
  */

class CYCTTime : public CYCT
{
  Q_OBJECT

public: 
   /** Attr est la structure de saisie des attributs d'un groupe de contrôle tolérances en temps. */
  struct Attr1
  {
    /** Nom du groupe (préfixe du nom des données). */
    const QString &name;
    /** Index réseau correspondant à l'index des valeurs dans la structure réseau. */
    int idNet;
    /** Valeur constructeur pour le temps d'inhibition en chaine de caractères. */
    QString inh_def;
    /** Valeur constructeur pour le temps maximum de stabilisation en chaine de caractères. */
    QString stab_def;
    /** Valeur constructeur pour le temps maximum d'alerte avant défaut. */
    f32 ale_def;
    /** Valeur constructeur pour le défaut si temps maxi hors tolerance d'alerte atteint. */
    flg  fl_arret_ale_def;
    /** Valeur constructeur pour le défaut si hors tolerance de défaut. */
    flg  fl_arret_def_def;
    /** Etiquette du contrôle (nom du groupe des données relatives). */
    QString label;
    /** Numéro du format d'affichage dans l'application de la mesure. */
    int format;
    /** Accès au paramètrage. */
    QString settings;
  };

   /** Attr est la structure de saisie des attributs d'un groupe de contrôle tolérances en temps. */
  struct Attr2
  {
    /** Nom du groupe (préfixe du nom des données). */
    const QString &name;
    /** Index réseau correspondant à l'index des valeurs dans la structure réseau. */
    int idNet;
    /** Valeur constructeur pour le temps d'inhibition en chaine de caractères. */
    QString inh_def;
    /** Valeur minimum pour le temps d'inhibition en chaine de caractères. */
    QString inh_min;
    /** Valeur maximum pour le temps d'inhibition en chaine de caractères. */
    QString inh_max;
    /** Valeur constructeur pour le temps maximum de stabilisation en chaine de caractères. */
    QString stab_def;
    /** Valeur minimum pour le temps maximum de stabilisation en chaine de caractères. */
    QString stab_min;
    /** Valeur maximum pour le temps maximum de stabilisation en chaine de caractères. */
    QString stab_max;
    /** Valeur constructeur pour le temps maximum d'alerte avant défaut. */
    f32 ale_def;
    /** Valeur minimum pour le temps maximum d'alerte avant défaut. */
    f32 ale_min;
    /** Valeur maximum pour le temps maximum d'alerte avant défaut. */
    f32 ale_max;
    /** Valeur constructeur pour le défaut si temps maxi hors tolerance d'alerte atteint. */
    flg  fl_arret_ale_def;
    /** Valeur constructeur pour le défaut si hors tolerance de défaut. */
    flg  fl_arret_def_def;
    /** Etiquette du contrôle (nom du groupe des données relatives). */
    QString label;
    /** Numéro du format d'affichage dans l'application de la mesure. */
    int format;
    /** Accès au paramètrage. */
    QString settings;
  };

 /** Création d'un groupe de contrôle tolérances en temps.
    * @param db               Base de données parent.
    * @param name             Nom du groupe (préfixe du nom des données).
    * @param idNet            Index réseau correspondant à l'index des valeurs dans la structure réseau.
    * @param values           Pointe sur les valeurs.
    * @param inh_def          Valeur constructeur pour le temps d'inhibition en chaine de caractères.
    * @param stab_def         Valeur constructeur pour le temps maximum de stabilisation en chaine de caractères.
    * @param ale_def          Valeur constructeur pour le temps maximum d'alerte avant défaut.
    * @param fl_arret_ale_def Valeur constructeur pour le défaut si temps maxi hors tolerance d'alerte atteint.
    * @param fl_arret_def_def Valeur constructeur pour le défaut si hors tolerance de défaut.
    * @param label            Etiquette du contrôle  (nom du groupe des données relatives).
    * @param format           Numéro du format d'affichage dans l'application de la mesure.
    * @param settings         Accès au paramètrage. */
  CYCTTime(CYDB *db, const QString &name, int idNet, t_ct *values, QString inh_def, QString stab_def, f32 ale_def, flg fl_arret_ale_def, flg fl_arret_def_def, QString label, int format=0, QString settings=0);

 /** Création d'un groupe de contrôle tolérances en temps.
    * @param db     Base de données parent.
    * @param name   Nom du groupe (préfixe du nom des données).
    * @param values Pointe sur les valeurs.
    * @param attr   Attributs du groupe. */
  CYCTTime(CYDB *db, const QString &name, t_ct *values, Attr1 *attr);

 /** Création d'un groupe de contrôle tolérances en temps.
    * @param db     Base de données parent.
    * @param name   Nom du groupe (préfixe du nom des données).
    * @param values Pointe sur les valeurs.
    * @param attr   Attributs du groupe. */
  CYCTTime(CYDB *db, const QString &name, t_ct *values, Attr2 *attr);

  ~CYCTTime();

  /** @return le temps d'inhibition avant contrôle. */
  CYTime *inhibitionTime() { return mInhibitionTime; }
  /** @return le temps maximum de stabilisation avant contrôle. */
  CYTime *stabilisationTime() { return mStabilisationTime; }
  /** @return le temps maximum hors tolérance d'alerte avant défaut. */
  CYTSec *alerteTime() { return mAlerteTime; }

  /** Initialise l'alerte sur max devenue défaut. */
  virtual CYEvent *initFaultAlertMax( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" > MAX (Max. alert)")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was higher than the maximum tolerated value, which produced an alert. Also the maximum time of alert has been reached and thus produced a fault."));
  /** Initialise l'alerte sur min devenue défaut. */
  virtual CYEvent *initFaultAlertMin( CYEventsGenerator *generator, const QString &name, flg *val=0, QString helpEnd=0, bool enable=SI_1, int proc=REC|SCR|BST, QString desc=tr(" < MIN (Max. alert)")+(" !"), QString from=tr("Process"), QString refs=0, QString help=tr("Measurement was lower than the minimum tolerated value, which produced an alert. Also the maximum time of alert has been reached and thus produced a fault."));

private: // Private methods
  /** Initialisation */
  void init();

protected: // Protected attributes
  /** Temps d'inhibition avant contrôle. */
  CYTime *mInhibitionTime;
  /** Temps maximum de stabilisation avant contrôle. */
  CYTime *mStabilisationTime;
  /** Temps maximum hors tolérance d'alerte avant défaut. */
  CYTSec *mAlerteTime;
};

#endif
