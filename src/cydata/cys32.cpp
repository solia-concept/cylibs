/***************************************************************************
                          cys32.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cys32.h"

CYS32::CYS32(CYDB *db, const QString &name, int format, Mode mode)
  : CYNum<s32>(db, name, format, mode, VS32)
{
}

CYS32::CYS32(CYDB *db, const QString &name, s32 *val, int format, Mode mode)
  : CYNum<s32>(db, name, val, format, mode, VS32)
{
}

CYS32::CYS32(CYDB *db, const QString &name, s32 *val, const s32 def, int format, Mode mode)
  : CYNum<s32>(db, name, val, def, format, mode, VS32)
{
}

CYS32::CYS32(CYDB *db, const QString &name, s32 *val, const s32 def, const s32 min, const s32 max, int format, Mode mode)
  : CYNum<s32>(db, name, val, def, min, max, format, mode, VS32)
{
}

CYS32::CYS32(CYDB *db, const QString &name, CYNum<s32>::Attr1<s32> *attr)
  : CYNum<s32>(db, name, attr, VS32)
{
}

CYS32::CYS32(CYDB *db, const QString &name, CYNum<s32>::Attr2<s32> *attr)
  : CYNum<s32>(db, name, attr, VS32)
{
}

CYS32::CYS32(CYDB *db, const QString &name, CYNum<s32>::Attr3<s32> *attr)
  : CYNum<s32>(db, name, attr, VS32)
{
}

CYS32::CYS32(CYDB *db, const QString &name, CYNum<s32>::Attr4<s32> *attr)
  : CYNum<s32>(db, name, attr, VS32)
{
}

CYS32::~CYS32()
{
}
