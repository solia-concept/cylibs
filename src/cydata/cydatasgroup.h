//
// C++ Interface: cydatasgroup
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2005
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYDATASGROUP_H
#define CYDATASGROUP_H

// QT
#include <QObject>
#include <QList>
// CYLIBS
#include "cytypes.h"
#include "cydb.h"
#include "cyevent.h"

/** @short Groupe de données.
    Un groupe de données comportent comme son nom l'indique plusieurs données. Celles-ci
    sont reliées entre-elles par un index réseau correspondant à l'index des valeurs dans
    différentes structures réseau. Ceux-ci permet ainsi de regrouper des données dont leur
    valeur peuvent être en lecture ou en écriture par rapport au(x) régulateur(s).
    @author Gérald LE CLEACH
*/
class CYDatasGroup : public QObject
{
Q_OBJECT
public:
  /** Construction du groupe de données.
    * @param db       Base de données parent.
    * @param name     Nom du groupe (préfixe du nom des données).
    * @param idNet    Index réseau correspondant à l'index des valeurs dans différentes structures réseau.
    * @param group    Nom du groupe (arborescence).
    * @param settings Accès au paramètrage. */
  CYDatasGroup(CYDB *db, const QString &name, int idNet, QString group, QString settings=0);

    ~CYDatasGroup();

  /** @return la base de données de rattachement. */
  CYDB *db() { return mDB; }
  /** @return l'index réseau correspondant à l'index des valeurs dans la structure réseau. */
  int idNet() { return mIdNet; }
  /** @return l'étiquette du groupe. */
  virtual QString label() { return mLabel; }
  /** @return le nom du groupe (arborescence). */
  virtual QString group() { return mGroup; }
  /** @return l'accès au paramètrage. */
  virtual QString settings() { return mSettings; }

  virtual void setEventsGenerator(CYEventsGenerator *generator);
  /*! Initialise un évènement. */
  virtual CYEvent *initEvent( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help);
  /*! Initialise un défaut. */
  virtual CYEvent *initFault( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help);
  /*! Initialise une alerte. */
  virtual CYEvent *initAlert( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help);

public slots:
  virtual void ctrlPartial() {}

protected: // Protected attributes
  /** Base de données de rattachement. */
  CYDB *mDB;
  /** Index réseau correspondant à l'index des valeurs dans la structure réseau. */
  int mIdNet;
  /** Etiquette par défaut des données du groupe. */
  QString mLabel;
  /** Nom du groupe (arborescence). */
  QString mGroup;
  /** Aide du groupe. */
  QString mHelp;
  /** Accès au paramètrage. */
  QString mSettings;

  /** Évènements relatifs à ce groupe de données. */
  QList<CYEvent*> mEvents;
};

#endif
