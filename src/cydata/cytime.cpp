/***************************************************************************
                          cytime.cpp  -  description
                             -------------------
    début                  : mer mai 7 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cytime.h"

#include <sys/time.h>

CYTime::CYTime(CYDB *db, const QString &name, u32 *val, Cy::TimeFormat format, double base, Mode mode)
  : CYU32(db, name, val, 0, mode),
    mTimeFormat(format),
    mBase(base)
{
  setDef(0);
  setMin(0);
  setMax(UINT_MAX);
  mAutoMinMax = true;
  *mVal = mDef;
  init();
}

CYTime::CYTime(CYDB *db, const QString &name, u32 *val, QString def, QString min, QString max, double base, Mode mode)
  : CYU32(db, name, val, 0, mode),
    mBase(base)
{
  mAutoMinMax = false;

  mTimeFormatTmp = TIME_NO_FORMAT;
  mDef = stringTimeToVal(def);
  *mVal = mDef;

  if (mTimeFormatTmp==TIME_NO_FORMAT)
    CYFATALDATA(name)
  mTimeFormat = mTimeFormatTmp;

  mTimeFormatTmp = TIME_NO_FORMAT;
  mMin = stringTimeToVal(min);

  if (mTimeFormatTmp!=mTimeFormat)
    CYFATALDATA(name)

  mTimeFormatTmp = TIME_NO_FORMAT;
  mMax = stringTimeToVal(max);

  if (mTimeFormatTmp!=mTimeFormat)
    CYFATALDATA(name)

  init();
}

CYTime::CYTime(CYDB *db, const QString &name, u32 *val, const u32 def, const u32 min, const u32 max, Cy::TimeFormat format, double base, Mode mode)
  : CYU32(db, name, val, def, min, max, 0, mode),
    mTimeFormat(format),
    mBase(base)
{
  *mVal = mDef;
  init();
}

CYTime::CYTime(CYDB *db, const QString &name, u32 *val, double base, Mode mode)
  : CYU32(db, name, val, 0, mode),
    mBase(base)
{
}

CYTime::~CYTime()
{
}

void CYTime::init()
{
  mType = Time;

  mHours        = 0;
  mMinutes      = 0;
  mSeconds      = 0;
  mMilliseconds = 0;

  QString fmt = QString("%1_FMT").arg(objectName());
  mFormat = new CYFormat(this, fmt, "", tr("Time"), mMin, mMax, 0, 0.0);


  switch (mTimeFormat)
  {
    case HOUR             : mFormat = new CYFormat(this, fmt, tr("hour"), tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Hours;
                            break;

    case MIN              : mFormat = new CYFormat(this, fmt, tr("min"), tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Minutes;
                            break;

    case SEC              : mFormat = new CYFormat(this, fmt, tr("sec"), tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Seconds;
                            break;

    case MSEC             : mFormat = new CYFormat(this, fmt, tr("msec"), tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Milliseconds;
                            if (mBase>BASE_SEC)
                              CYFATALDATA(objectName());
                            break;

    case HOUR_MIN         : mFormat = new CYFormat(this, fmt, 0, tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Hours;
                            break;

    case MIN_SEC          : mFormat = new CYFormat(this, fmt, 0, tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Minutes;
                            break;

    case SEC_MSEC         : mFormat = new CYFormat(this, fmt, 0, tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Seconds;
                            if (mBase>BASE_SEC)
                              CYFATALDATA(objectName());
                            break;

    case HOUR_MIN_SEC     : mFormat = new CYFormat(this, fmt, 0, tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Hours;
                            break;

    case HOUR_MIN_SEC_MSEC: mFormat = new CYFormat(this, fmt, 0, tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Hours;
                            if (mBase>BASE_SEC)
                              CYFATALDATA(objectName());
                            break;

    case MIN_SEC_MSEC     : mFormat = new CYFormat(this, fmt, 0, tr("Time"), mMin, mMax, 0, 0.0);
                            mSection = CYTime::Minutes;
                            if (mBase>BASE_SEC)
                              CYFATALDATA(objectName());
                            break;

    default               : CYMESSAGETEXT(QString("%1 => format ?").arg(objectName()));
  }

  mTmp = (double)(*mVal);
}

u32 CYTime::val()
{
  double i_part, mantisse;
  u32 ret;

  if (mVal==0)
  {
    CYWARNINGTEXT(QString("u32 CYTime::val()"));
    return 0;
  }

  if (precision()==0.0)
  {
    ret = output(*mVal);
    return ret;
  }

  mantisse = modf((double) output(*mVal)/output((u32)precision()), &i_part);
  if(mantisse>0.5)
    i_part+=1.0;
  if(mantisse<-0.5)
    i_part-=1.0;
  ret = ((u32)(i_part*precision()));
  return ret;
}

double CYTime::tmp()
{
  return (double)output(mTmp);
}

u32 CYTime::real()
{
  return output(*mVal);
}

u32 CYTime::min()
{
 return output(CYU32::min());
}

u32 CYTime::max()
{
 return output(CYU32::max());
}

u32 CYTime::def()
{
 return output(CYU32::def());
}

void CYTime::setVal(const u32 val)
{
  mTmp = input(val);
  CYU32::setVal(mTmp);
}

void CYTime::setms(const u32 val)
{
    setVal(val*BASE_MSEC/mBase);
}
void CYTime::sets(const u32 val)
{
    setVal(val*BASE_SEC/mBase);
}
void CYTime::setmin(const u32 val)
{
    setVal(val*BASE_MIN/mBase);
}
void CYTime::sethr(const u32 val)
{
    setVal(val*BASE_HOUR/mBase);
}

void CYTime::import(const u32 val)
{
  mTmp = input(val);
  CYU32::import(mTmp);
}

void CYTime::setDef(const u32 val)
{
  mDef=input(val);

  if (def()>max() || def()<min())
    setDataCheckDef();
}


void CYTime::checkDef()
{
  if (def()>max())
    CYWARNINGTEXT(tr("Designer value of %1:%2 is higher than maximum value (%3>%4) !").arg(db()->objectName()).arg(objectName()).arg(def()).arg(max()));
  if (def()<min())
    CYWARNINGTEXT(tr("Designer value of %1:%2 is smaller than minimum value (%3<%4) !").arg(db()->objectName()).arg(objectName()).arg(def()).arg(min()));
}


void CYTime::setMin(const u32 val)
{
  mMin = input(val);
}

void CYTime::setMax(const u32 val)
{
  mMax = input(val);
}

void CYTime::setTmp(const double val)
{
  mTmp = (double)input(val);
  sections();
}

void CYTime::designer()
{
  *mVal = mDef;
  mTmp = (double)(*mVal);
}

QString CYTime::stringDef()
{
  QString txt;
  txt = toString(max(), true, false);
  txt.replace('0', '-');
  txt.replace('1', '-');
  txt.replace('2', '-');
  txt.replace('3', '-');
  txt.replace('4', '-');
  txt.replace('5', '-');
  txt.replace('6', '-');
  txt.replace('7', '-');
  txt.replace('8', '-');
  txt.replace('9', '-');
  return txt;
}

QString CYTime::toString(bool whole, bool ctrlValid, Cy::NumBase base)
{
  Q_UNUSED(base)
  return toString((double)real(), whole, ctrlValid);
}

QString CYTime::toString(double val, bool whole, bool ctrlValid, Cy::NumBase base)
{
  Q_UNUSED(base)
  if (ctrlValid && !isValid())
    return stringDef();

  char buf1[10], buf2[10], buf3[10], buf4[10];

  val = (double)input(val);
  switch (mTimeFormat)
  {
    case HOUR             : return QString("%1 %2").arg(hours(val)).arg(unit());
    case MIN              : return QString("%1 %2").arg(minutes(val)).arg(unit());
    case SEC              : return QString("%1 %2").arg(seconds(val)).arg(unit());
    case MSEC             : return QString("%1 %2").arg(milliseconds(val)).arg(unit());

    case HOUR_MIN         : if (hours(val) || whole)
                            {
                              sprintf(buf1,"%d", hours(val));
                              sprintf(buf2,"%02d", minutes(val));
                              return QString(tr("%1h%2m")).arg(buf1).arg(buf2);
                            }
                            else
                            {
                              sprintf(buf1,"%d", minutes(val));
                              return QString(tr("%2 min")).arg(buf1);
                            }

    case MIN_SEC          : if (minutes(val) || whole)
                            {
                              sprintf(buf1,"%d", minutes(val));
                              sprintf(buf2,"%02d", seconds(val));
                              return QString(tr("%1m%2s")).arg(buf1).arg(buf2);
                            }
                            else
                            {
                              sprintf(buf1,"%d", seconds(val));
                              return QString(tr("%1 sec")).arg(buf1);
                            }

    case SEC_MSEC         : if (seconds(val) || whole)
                            {
                              sprintf(buf1,"%d", seconds(val));
                              sprintf(buf2,"%03d", milliseconds(val));
                              return QString(tr("%1s%2ms")).arg(buf1).arg(buf2);
                            }
                            else
                            {
                              sprintf(buf1,"%d", milliseconds(val));
                              return QString(tr("%3 msec")).arg(buf1);
                            }

    case HOUR_MIN_SEC     : if (hours(val) || whole)
                            {
                              sprintf(buf1,"%d", hours(val));
                              sprintf(buf2,"%02d", minutes(val));
                              sprintf(buf3,"%02d", seconds(val));
                              return QString(tr("%1h%2m%3s")).arg(buf1).arg(buf2).arg(buf3);
                            }
                            else if (minutes(val))
                            {
                              sprintf(buf1,"%d", minutes(val));
                              sprintf(buf2,"%02d", seconds(val));
                              return QString(tr("%1m%2s")).arg(buf1).arg(buf2);
                            }
                            else
                            {
                              sprintf(buf1,"%d", seconds(val));
                              return QString(tr("%1 sec")).arg(buf1);
                            }

    case HOUR_MIN_SEC_MSEC: if (hours(val) || whole)
                            {
                              sprintf(buf1,"%d", hours(val));
                              sprintf(buf2,"%02d", minutes(val));
                              sprintf(buf3,"%02d", seconds(val));
                              sprintf(buf4,"%03d", milliseconds(val));
                              return QString(tr("%1h%2m%3s%4")).arg(buf1).arg(buf2).arg(buf3).arg(buf4);
                            }
                            else if (minutes(val))
                            {
                              sprintf(buf1,"%d", minutes(val));
                              sprintf(buf2,"%02d", seconds(val));
                              sprintf(buf3,"%03d", milliseconds(val));
                              return QString(tr("%1m%2s%3")).arg(buf1).arg(buf2).arg(buf3);
                            }
                            else if (seconds(val))
                            {
                              sprintf(buf1,"%d", seconds(val));
                              sprintf(buf2,"%03d", milliseconds(val));
                              return QString(tr("%1s%2")).arg(buf1).arg(buf2);
                            }
                            else
                            {
                              sprintf(buf1,"%d", milliseconds(val));
                              return QString(tr("%1 msec")).arg(buf1);
                            }


    case MIN_SEC_MSEC     : if (minutes(val) || whole)
                            {
                              sprintf(buf1,"%d", minutes(val));
                              sprintf(buf2,"%02d", seconds(val));
                              sprintf(buf3,"%03d", milliseconds(val));
                              return QString(tr("%1m%2s%3")).arg(buf1).arg(buf2).arg(buf3);
                            }
                            else if (seconds(val))
                            {
                              sprintf(buf1,"%d", seconds(val));
                              sprintf(buf2,"%03d", milliseconds(val));
                              return QString(tr("%1s%2")).arg(buf1).arg(buf2);
                            }
                            else
                            {
                              sprintf(buf1,"%d", milliseconds(val));
                              return QString(tr("%1 msec")).arg(buf1);
                            }

    default               : CYMESSAGETEXT(QString("%1 => format ?").arg(objectName()));
  }
  return 0;
}

QString CYTime::prefix()
{
  char buf1[10], buf2[10], buf3[10];

  switch(mTimeFormat)
  {
    case HOUR             :
    case MIN              :
    case SEC              :
    case MSEC             : return "";

    case HOUR_MIN         : if (mSection == CYTime::Hours)
                              return "";
                            if (mSection == CYTime::Minutes)
                            {
                              sprintf(buf1,"%d", hours());
                              return QString(tr("%1h")).arg(buf1);
                            }
                            CYWARNINGTEXT(QString("void CYTime::prefix(): %1 (HOUR_MIN)").arg(objectName()));
                            return "";

    case MIN_SEC          : if (mSection == CYTime::Minutes)
                              return "";
                            if (mSection == CYTime::Seconds)
                            {
                              sprintf(buf1,"%d", minutes());
                              return QString(tr("%1m")).arg(buf1);
                            }
                            CYWARNINGTEXT(QString("void CYTime::prefix(): %1 (MIN_SEC) => %2").arg(objectName()).arg(mSection));
                            return "";

    case SEC_MSEC         : if (mSection == CYTime::Seconds)
                              return "";
                            if (mSection == CYTime::Milliseconds)
                            {
                              sprintf(buf1,"%d", seconds());
                              return QString(tr("%1s")).arg(buf1);
                            }
                            CYWARNINGTEXT(QString("void CYTime::prefix(): %1 (SEC_MSEC)").arg(objectName()));
                            return "";

    case HOUR_MIN_SEC     : if (mSection == CYTime::Hours)
                              return "";
                            if (mSection == CYTime::Minutes)
                            {
                              sprintf(buf1,"%d", hours());
                              return QString(tr("%1h")).arg(buf1);
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              sprintf(buf1,"%d", hours());
                              sprintf(buf2,"%02d", minutes());
                              return QString(tr("%1h%2m")).arg(buf1).arg(buf2);
                            }
                            CYWARNINGTEXT(QString("void CYTime::prefix(): %1 (HOUR_MIN_SEC)").arg(objectName()));
                            return "";

    case HOUR_MIN_SEC_MSEC: if (mSection == CYTime::Hours)
                              return "";
                            if (mSection == CYTime::Minutes)
                            {
                              sprintf(buf1,"%d", hours());
                              return QString(tr("%1h")).arg(buf1);
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              sprintf(buf1,"%d", hours());
                              sprintf(buf2,"%02d", minutes());
                              return QString(tr("%1h%2m")).arg(buf1).arg(buf2);
                            }
                            if (mSection == CYTime::Milliseconds)
                            {
                              sprintf(buf1,"%d", hours());
                              sprintf(buf2,"%02d", minutes());
                              sprintf(buf3,"%02d", seconds());
                              return QString(tr("%1h%2m%3s")).arg(buf1).arg(buf2).arg(buf3);
                            }
                            CYWARNINGTEXT(QString("void CYTime::prefix(): %1 (HOUR_MIN_SEC_MSEC)").arg(objectName()));
                            return "";

    case MIN_SEC_MSEC     : if (mSection == CYTime::Minutes)
                              return "";
                            if (mSection == CYTime::Seconds)
                            {
                              sprintf(buf1,"%d", minutes());
                              return QString(tr("%1m")).arg(buf1);
                            }
                            if (mSection == CYTime::Milliseconds)
                            {
                              sprintf(buf1,"%d", minutes());
                              sprintf(buf2,"%02d", seconds());
                              return QString(tr("%1m%2s")).arg(buf1).arg(buf2);
                            }
                            CYWARNINGTEXT(QString("void CYTime::prefix(): %1 (MIN_SEC_MSEC) => %2").arg(objectName()).arg(mSection));
                            return "";

    default               : CYMESSAGETEXT(QString("%1 => format ?").arg(objectName()));
  }
  return 0;
}

QString CYTime::suffix()
{
  char buf1[10], buf2[10], buf3[10];

	QString suffix_ms = QString(tr("ms"));
    if (mBase<10.0){ //ms
        suffix_ms = QString(tr("ms"));
    }
    else if (mBase<100.0){ //10ms
        suffix_ms = QString(tr("0ms"));
    }
    else if (mBase<1000.0){ //100ms
        suffix_ms = QString(tr("00ms"));
    }

	switch(mTimeFormat)
  {
    case HOUR             : return QString(tr(" hour"));
    case MIN              : return QString(tr(" min"));
    case SEC              : return QString(tr(" sec"));
    case MSEC             : return QString(tr(" msec"));

    case HOUR_MIN         : if (mSection == CYTime::Hours)
                            {
                              sprintf(buf1,"%02d", minutes());
                              return QString(tr("h%1m")).arg(buf1);
                            }
                            if (mSection == CYTime::Minutes)
                            {
                              return QString(tr("m"));
                            }
                            CYWARNINGTEXT(QString("void CYTime::suffix(): %1 (HOUR_MIN)").arg(objectName()));
                            return "";

    case MIN_SEC          : if (mSection == CYTime::Minutes)
                            {
                              sprintf(buf1,"%02d", seconds());
                              return QString(tr("m%1s")).arg(buf1);
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              return QString(tr("s"));
                            }
                            CYWARNINGTEXT(QString("void CYTime::suffix(): %1 (MIN_SEC)").arg(objectName()));
                            return "";

    case SEC_MSEC         : if (mSection == CYTime::Seconds)
                            {
                              sprintf(buf1,"%03d", milliseconds());
                              return QString(tr("s%1ms")).arg(buf1);
                            }
                            if (mSection == CYTime::Milliseconds)
                            {
                                return suffix_ms;
                            }
                            CYWARNINGTEXT(QString("void CYTime::suffix(): %1 (SEC_MSEC)").arg(objectName()));
                            return "";

    case HOUR_MIN_SEC     : if (mSection == CYTime::Hours)
                            {
                              sprintf(buf1,"%02d", minutes());
                              sprintf(buf2,"%02d", seconds());
                              return QString(tr("h%1m%2s")).arg(buf1).arg(buf2);
                            }
                            if (mSection == CYTime::Minutes)
                            {
                              sprintf(buf1,"%02d", seconds());
                              return QString(tr("m%1s")).arg(buf1);
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              return QString(tr("s"));
                            }
                            CYWARNINGTEXT(QString("void CYTime::suffix(): %1 (HOUR_MIN_SEC)").arg(objectName()));
                            return "";

    case HOUR_MIN_SEC_MSEC : if (mSection == CYTime::Hours)
                            {
                              sprintf(buf1,"%02d", minutes());
                              sprintf(buf2,"%02d", seconds());
                              sprintf(buf3,"%03d", milliseconds());
                              return QString(tr("h%1m%2s%3ms")).arg(buf1).arg(buf2).arg(buf3);
                            }
                            if (mSection == CYTime::Minutes)
                            {
                              sprintf(buf1,"%02d", seconds());
                              sprintf(buf2,"%03d", milliseconds());
                              return QString(tr("m%1s%2ms")).arg(buf1).arg(buf2);
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              sprintf(buf1,"%03d", milliseconds());
                              return QString(tr("s%1ms")).arg(buf1);
                            }
                            if (mSection == CYTime::Milliseconds)
                            {
															return suffix_ms;
														}
                            CYWARNINGTEXT(QString("void CYTime::suffix(): %1 (HOUR_MIN_SEC_MSEC)").arg(objectName()));
                            return "";

    case MIN_SEC_MSEC     : if (mSection == CYTime::Minutes)
                            {
                              sprintf(buf1,"%02d", seconds());
                              sprintf(buf2,"%03d", milliseconds());
                              return QString(tr("m%1s%2ms")).arg(buf1).arg(buf2);
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              sprintf(buf1,"%03d", milliseconds());
                              return QString(tr("s%1ms")).arg(buf1);
                            }
                            if (mSection == CYTime::Milliseconds)
                            {
															return suffix_ms;
														}
														CYWARNINGTEXT(QString("void CYTime::suffix(): %1 (MIN_SEC_MSEC)").arg(objectName()));
                            return "";

    default               : CYWARNINGTEXT(QString("void CYTime::suffix(): %1=> format ?").arg(objectName()));
  }
  return 0;
}

void CYTime::setSection(CYData::Section section)
{
  mSection = section;
}

CYData::Section CYTime::section()
{
  return mSection;
}

bool CYTime::isSectionMaxi()
{
  switch(mTimeFormat)
  {
    case HOUR             :
    case MIN              :
    case SEC              :
    case MSEC             : return true;

    case HOUR_MIN         : if (mSection == CYTime::Hours)
                              return true;
                            return false;

    case MIN_SEC          : if (mSection == CYTime::Minutes)
                              return true;
                            return false;

    case SEC_MSEC         : if (mSection == CYTime::Seconds)
                              return true;
                            return false;

    case HOUR_MIN_SEC     : if (mSection == CYTime::Hours)
                              return true;
                            return false;

    case HOUR_MIN_SEC_MSEC: if (mSection == CYTime::Hours)
                              return true;
                            return false;

    case MIN_SEC_MSEC     : if (mSection == CYTime::Minutes)
                              return true;
                            return false;

    default               : CYWARNINGTEXT(QString("bool CYTime::sectionMaxi(): %1=> format ?").arg(objectName()));
  }
  return true;
}

bool CYTime::sectionMaxi(const double val)
{
  setValSection((int)val);
  switch(mTimeFormat)
  {
    case HOUR             :
    case MIN              :
    case SEC              :
    case MSEC             : return false;

    case HOUR_MIN         : mSection = CYTime::Hours;
                            return true;

    case MIN_SEC          : mSection = CYTime::Minutes;
                            return true;

    case SEC_MSEC         : mSection = CYTime::Seconds;
                            return true;

    case HOUR_MIN_SEC     : mSection = CYTime::Hours;
                            return true;

    case HOUR_MIN_SEC_MSEC: mSection = CYTime::Hours;
                            return true;

    case MIN_SEC_MSEC     : mSection = CYTime::Minutes;
                            return true;

    default               : CYWARNINGTEXT(QString("bool CYTime::sectionMaxi(): %1=> format ?").arg(objectName()));
  }
  return false;
}

bool CYTime::sectionMini(const double val)
{
  setValSection((int)val);
  switch(mTimeFormat)
  {
    case HOUR             :
    case MIN              :
    case SEC              :
    case MSEC             : return false;

    case HOUR_MIN         : mSection = CYTime::Minutes;
                            return true;

    case MIN_SEC          : mSection = CYTime::Seconds;
                            return true;

    case SEC_MSEC         : mSection = CYTime::Milliseconds;
                            return true;

    case HOUR_MIN_SEC     : mSection = CYTime::Seconds;
                            return true;

    case HOUR_MIN_SEC_MSEC: mSection = CYTime::Milliseconds;
                            return true;

    case MIN_SEC_MSEC     : mSection = CYTime::Milliseconds;
                            return true;

    default               : CYWARNINGTEXT(QString("void CYTime::sectionMini(): %1=> format ?").arg(objectName()));
  }
  return false;
}

bool CYTime::sectionUp(const double val)
{
  setValSection((int)val);
  switch(mTimeFormat)
  {
    case HOUR             :
    case MIN              :
    case SEC              :
    case MSEC             : return false;

    case HOUR_MIN_SEC     : if (mSection == CYTime::Hours)
                              return false;
                            if (mSection == CYTime::Seconds)
                            {
                              mSection = CYTime::Minutes;
                              return true;
                            }
                            if (mSection == CYTime::Minutes)
                            {
                              mSection = CYTime::Hours;
                              return true;
                            }
                            CYWARNINGTEXT(QString("bool CYTime::sectionUp(): %1 (HOUR_MIN_SEC)").arg(objectName()));
                            return false;

    case HOUR_MIN_SEC_MSEC : if (mSection == CYTime::Hours)
                              return false;
                            if (mSection == CYTime::Milliseconds)
                            {
                              mSection = CYTime::Seconds;
                              return true;
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              mSection = CYTime::Minutes;
                              return true;
                            }
                            if (mSection == CYTime::Minutes)
                            {
                              mSection = CYTime::Hours;
                              return true;
                            }
                            CYWARNINGTEXT(QString("bool CYTime::sectionUp(): %1 (HOUR_MIN_SEC_MSEC)").arg(objectName()));
                            return false;

    case MIN_SEC_MSEC     : if (mSection == CYTime::Minutes)
                              return false;
                            if (mSection == CYTime::Milliseconds)
                            {
                              mSection = CYTime::Seconds;
                              return true;
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              mSection = CYTime::Minutes;
                              return true;
                            }
                            CYWARNINGTEXT(QString("bool CYTime::sectionUp(): %1 (MIN_SEC_MSEC)").arg(objectName()));
                            return false;

    case HOUR_MIN         : if (mSection == CYTime::Hours)
                              return false;
                            if (mSection == CYTime::Minutes)
                            {
                              mSection = CYTime::Hours;
                              return true;
                            }
                            CYWARNINGTEXT(QString("bool CYTime::sectionUp(): %1 (HOUR_MIN)").arg(objectName()));
                            return false;

    case MIN_SEC          : if (mSection == CYTime::Minutes)
                              return false;
                            if (mSection == CYTime::Seconds)
                            {
                              mSection = CYTime::Minutes;
                              return true;
                            }
                            CYWARNINGTEXT(QString("bool CYTime::sectionUp(): %1 (MIN_SEC)").arg(objectName()));
                            return false;

    case SEC_MSEC         : if (mSection == CYTime::Seconds)
                              return false;
                            if (mSection == CYTime::Milliseconds)
                            {
                              mSection = CYTime::Seconds;
                              return true;
                            }
                            CYWARNINGTEXT(QString("bool CYTime::sectionUp(): %1 (SEC_MSEC)").arg(objectName()));
                            return false;

    default               : CYWARNINGTEXT(QString("bool CYTime::sectionUp(): %1=> format ?").arg(objectName()));
  }

  return false;
}

bool CYTime::sectionDown(const double val)
{
  setValSection((int)val);
  switch(mTimeFormat)
  {
    case HOUR             :
    case MIN              :
    case SEC              :
    case MSEC             : return false;

    case HOUR_MIN         : if (mSection == CYTime::Hours)
                            {
                              mSection = CYTime::Minutes;
                              return true;
                            }
                            if (mSection == CYTime::Minutes)
                              return false;
                            CYWARNINGTEXT(QString("bool CYTime::sectionDown(): %1 (HOUR_MIN)").arg(objectName()));
                            return false;

    case MIN_SEC          : if (mSection == CYTime::Minutes)
                            {
                              mSection = CYTime::Seconds;
                              return true;
                            }
                            if (mSection == CYTime::Seconds)
                              return false;
                            CYWARNINGTEXT(QString("bool CYTime::sectionDown(): %1 (MIN_SEC)").arg(objectName()));
                            return false;

    case SEC_MSEC         : if (mSection == CYTime::Seconds)
                            {
                              mSection = CYTime::Milliseconds;
                              return true;
                            }
                            if (mSection == CYTime::Milliseconds)
                              return false;
                            CYWARNINGTEXT(QString("bool CYTime::sectionDown(): %1 (SEC_MSEC)").arg(objectName()));
                            return false;

    case HOUR_MIN_SEC     : if (mSection == CYTime::Hours)
                            {
                              mSection = CYTime::Minutes;
                              return true;
                            }
                            if (mSection == CYTime::Minutes)
                            {
                              mSection = CYTime::Seconds;
                              return true;
                            }
                            if (mSection == CYTime::Seconds)
                              return false;
                            CYWARNINGTEXT(QString("bool CYTime::sectionDown(): %1 (HOUR_MIN_SEC)").arg(objectName()));
                            return false;

    case HOUR_MIN_SEC_MSEC: if (mSection == CYTime::Hours)
                            {
                              mSection = CYTime::Minutes;
                              return true;
                            }
                            if (mSection == CYTime::Minutes)
                            {
                              mSection = CYTime::Seconds;
                              return true;
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              mSection = CYTime::Milliseconds;
                              return true;
                            }
                            if (mSection == CYTime::Milliseconds)
                              return false;
                            CYWARNINGTEXT(QString("bool CYTime::sectionDown(): %1 (HOUR_MIN_SEC_MSEC)").arg(objectName()));
                            return false;

    case MIN_SEC_MSEC     : if (mSection == CYTime::Minutes)
                            {
                              mSection = CYTime::Seconds;
                              return true;
                            }
                            if (mSection == CYTime::Seconds)
                            {
                              mSection = CYTime::Milliseconds;
                              return true;
                            }
                            if (mSection == CYTime::Milliseconds)
                              return false;
                            CYWARNINGTEXT(QString("bool CYTime::sectionDown(): %1 (MIN_SEC_MSEC)").arg(objectName()));
                            return false;

    default               : CYWARNINGTEXT(QString("bool CYTime::sectionDown(): %1=> format ?").arg(objectName()));
  }

  return false;
}

bool CYTime::section(QString left)
{
  CYData::Section sect;

  bool digit;
  left.toInt(&digit);

  switch(mTimeFormat)
  {
    case HOUR             :
    case MIN              :
    case SEC              :
    case MSEC             : return false;

    case HOUR_MIN         : if (left.isEmpty() || digit)
                            {
                              sect = CYTime::Hours;
                              break;
                            }
                            if (!left.contains(tr("m")))
                            {
                              sect = CYTime::Minutes;
                              break;
                            }
                            return false;

    case MIN_SEC          : if (left.isEmpty() || digit)
                            {
                              sect = CYTime::Minutes;
                              break;
                            }
                            if (!left.contains(tr("s")))
                            {
                              sect = CYTime::Seconds;
                              break;
                            }
                            return false;

    case SEC_MSEC         : if (left.isEmpty() || digit)
                            {
                              sect = CYTime::Seconds;
                              break;
                            }
                            if (!left.contains(tr("ms")))
                            {
                              sect = CYTime::Milliseconds;
                              break;
                            }
                            return false;

    case HOUR_MIN_SEC     : if (left.isEmpty() || digit)
                            {
                              sect = CYTime::Hours;
                              break;
                            }
                            if (!left.contains(tr("m")))
                            {
                              sect = CYTime::Minutes;
                              break;
                            }
                            if (!left.contains(tr("s")))
                            {
                              sect = CYTime::Seconds;
                              break;
                            }
                            return false;

    case HOUR_MIN_SEC_MSEC: if (left.isEmpty() || digit)
                            {
                              sect = CYTime::Hours;
                              break;
                            }
                            if (left.contains(tr("ms")) || left.contains(tr("s")))
                            {
                              sect = CYTime::Milliseconds;
                              break;
                            }
                            if (!left.contains(tr("m")))
                            {
                              sect = CYTime::Minutes;
                              break;
                            }
                            sect = CYTime::Seconds;
                            break;

    case MIN_SEC_MSEC     : if (left.isEmpty() || digit)
                            {
                              sect = CYTime::Minutes;
                              break;
                            }
                            if (left.contains(tr("ms")) || left.contains(tr("s")))
                            {
                              sect = CYTime::Milliseconds;
                              break;
                            }
                            sect = CYTime::Seconds;
                            break;

    default               : return false;
  }

  if (mSection != sect)
  {
    mSection = sect;
    return true;
  }
  return false;
}


void CYTime::setValSection(const int value)
{
  switch (mTimeFormat)
  {
    case HOUR             : setHours(value);
                            break;

    case MIN              : setMinutes(value);
                            break;

    case SEC              : setSeconds(value);
                            break;

    case MSEC             : setMilliseconds(value);
                            break;

    case HOUR_MIN         : if (mSection == Hours)
                              setHours(value);
                            else if (mSection == Minutes)
                              setMinutes(value);
                            else
                              CYMESSAGETEXT(QString("%1=> HOUR_MIN").arg(objectName()));
                            break;

    case MIN_SEC          : if (mSection == Minutes)
                              setMinutes(value);
                            else if (mSection == Seconds)
                              setSeconds(value);
                            else
                              CYMESSAGETEXT(QString("%1=> MIN_SEC").arg(objectName()));
                            break;

    case SEC_MSEC         : if (mSection == Seconds)
                              setSeconds(value);
                            else if (mSection == Milliseconds)
                              setMilliseconds(value);
                            else
                              CYMESSAGETEXT(QString("%1=> SEC_MSEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC     : if (mSection == Hours)
                              setHours(value);
                            else if (mSection == Minutes)
                              setMinutes(value);
                            else if (mSection == Seconds)
                              setSeconds(value);
                            else
                              CYMESSAGETEXT(QString("%1=> HOUR_MIN_SEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC_MSEC: if (mSection == Hours)
                              setHours(value);
                            else if (mSection == Minutes)
                              setMinutes(value);
                            else if (mSection == Seconds)
                              setSeconds(value);
                            else if (mSection == Milliseconds)
                              setMilliseconds(value);
                            else
                              CYMESSAGETEXT(QString("%1=> HOUR_MIN_SEC_MSEC").arg(objectName()));
                            break;

    case MIN_SEC_MSEC     : if (mSection == Hours)
                              setHours(value);
                            else if (mSection == Minutes)
                              setMinutes(value);
                            else if (mSection == Seconds)
                              setSeconds(value);
                            else if (mSection == Milliseconds)
                              setMilliseconds(value);
                            else
                              CYMESSAGETEXT(QString("%1=> MIN_SEC_MSEC").arg(objectName()));
                            break;

    default               : CYMESSAGETEXT(QString("%1=> format ?").arg(objectName()));
  }
}

int CYTime::valSection()
{
  switch (mTimeFormat)
  {
    case HOUR             : return hours();
    case MIN              : return minutes();
    case SEC              : return seconds();
    case MSEC             : return milliseconds();

    case HOUR_MIN         : if (mSection == Hours)
                              return hours();
                            else if (mSection == Minutes)
                              return minutes();
                            else
                              CYWARNINGTEXT(QString("int CYTime::valSection(): %1=> HOUR_MIN").arg(objectName()));
                            break;

    case MIN_SEC          : if (mSection == Minutes)
                            {
                              int res = minutes();
                              return res;
                            }
                            else if (mSection == Seconds)
                            {
                              int res = seconds();
                              return res;
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::valSection(): %1=> MIN_SEC").arg(objectName()));
                            break;

    case SEC_MSEC         : if (mSection == Seconds)
                              return seconds();
                            else if (mSection == Milliseconds)
                              return milliseconds();
                            else
                              CYWARNINGTEXT(QString("int CYTime::valSection(): %1=> SEC_MSEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC     : if (mSection == Hours)
                              return hours();
                            else if (mSection == Minutes)
                              return minutes();
                            else if (mSection == Seconds)
                              return seconds();
                            else
                              CYWARNINGTEXT(QString("int CYTime::valSection(): %1=> HOUR_MIN_SEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC_MSEC: if (mSection == Hours)
                              return hours();
                            else if (mSection == Minutes)
                              return minutes();
                            else if (mSection == Seconds)
                              return seconds();
                            else if (mSection == Milliseconds)
                              return milliseconds();
                            else
                              CYWARNINGTEXT(QString("int CYTime::valSection(): %1=> HOUR_MIN_SEC_MSEC").arg(objectName()));
                            break;

    case MIN_SEC_MSEC     : if (mSection == Minutes)
                              return minutes();
                            else if (mSection == Seconds)
                              return seconds();
                            else if (mSection == Milliseconds)
                              return milliseconds();
                            else
                              CYWARNINGTEXT(QString("int CYTime::valSection(): %1=> MIN_SEC_MSEC").arg(objectName()));
                            break;

    default               : CYWARNINGTEXT(QString("CYTime::void CYTime::valSection(): %1=> format ?").arg(objectName()));
  }
  return 0;
}

int CYTime::defSection()
{
  switch (mTimeFormat)
  {
    case HOUR             : return hours(mDef);
    case MIN              : return minutes(mDef);
    case SEC              : return seconds(mDef);
    case MSEC             : return milliseconds(mDef);

    case HOUR_MIN         : if (mSection == Hours)
                              return hours(mDef);
                            else if (mSection == Minutes)
                              return minutes(mDef);
                            else
                              CYWARNINGTEXT(QString("int CYTime::defSection(): %1=> HOUR_MIN").arg(objectName()));
                            break;

    case MIN_SEC          : if (mSection == Minutes)
                              return minutes(mDef);
                            else if (mSection == Seconds)
                              return seconds(mDef);
                            else
                              CYWARNINGTEXT(QString("int CYTime::defSection(): %1=> MIN_SEC").arg(objectName()));
                            break;

    case SEC_MSEC         : if (mSection == Seconds)
                              return seconds(mDef);
                            else if (mSection == Milliseconds)
                              return milliseconds(mDef);
                            else
                              CYWARNINGTEXT(QString("int CYTime::defSection(): %1=> SEC_MSEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC     : if (mSection == Hours)
                              return hours(mDef);
                            else if (mSection == Minutes)
                              return minutes(mDef);
                            else if (mSection == Seconds)
                              return milliseconds(mDef);
                            else
                              CYWARNINGTEXT(QString("int CYTime::defSection(): %1=> HOUR_MIN_SEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC_MSEC: if (mSection == Hours)
                              return hours(mDef);
                            else if (mSection == Minutes)
                              return minutes(mDef);
                            else if (mSection == Seconds)
                              return milliseconds(mDef);
                            else
                              CYWARNINGTEXT(QString("int CYTime::defSection(): %1=> HOUR_MIN_SEC_MSEC").arg(objectName()));
                            break;

    default               : CYWARNINGTEXT(QString("CYTime::void CYTime::defSection(): %1=> format ?").arg(objectName()));
  }
  return 0;
}

int CYTime::minSection()
{
  switch (mTimeFormat)
  {
    case HOUR             : return hours(mMin);
    case MIN              : return minutes(mMin);
    case SEC              : return seconds(mMin);
    case MSEC             : return milliseconds(mMin);

    case HOUR_MIN         : if (mSection == Hours)
                              return hours(mMin);
                            else if (mSection == Minutes)
                            {
                              if (hours() > hours(mMin))
                                return 0;
                              else
                              return minutes(mMin);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::minSection(): %1=> HOUR_MIN").arg(objectName()));

                            break;

    case MIN_SEC          : if (mSection == Minutes)
                              return minutes(mMin);
                            else if (mSection == Seconds)
                            {
                              if (minutes() > minutes(mMin))
                                return 0;
                              else
                                return seconds(mMin);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::minSection(): %1=> MIN_SEC").arg(objectName()));
                            break;

    case SEC_MSEC         : if (mSection == Seconds)
                              return seconds(mMin);
                            else if (mSection == Milliseconds)
                            {
                              if (seconds() > seconds(mMin))
                                return 0;
                              else
                                return milliseconds(mMin);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::minSection(): %1=> SEC_MSEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC     : if (mSection == Hours)
                              return hours(mMin);
                            else if (mSection == Minutes)
                            {
                              if (hours() > hours(mMin))
                                return 0;
                              else
                              return minutes(mMin);
                            }
                            else if (mSection == Seconds)
                            {
                              if (hours() > hours(mMin))
                                return 0;
                              else if (minutes() > minutes(mMin))
                                return 0;
                              else
                                return seconds(mMin);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::minSection(): %1=> HOUR_MIN_SEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC_MSEC: if (mSection == Hours)
                              return hours(mMin);
                            else if (mSection == Minutes)
                            {
                              if (hours() > hours(mMin))
                                return 0;
                              else
                              return minutes(mMin);
                            }
                            else if (mSection == Seconds)
                            {
                              if (hours() > hours(mMin))
                                return 0;
                              else if (minutes() > minutes(mMin))
                                return 0;
                              else
                                return seconds(mMin);
                            }
                            else if (mSection == Milliseconds)
                            {
                              if (hours() > hours(mMin))
                                return 0;
                              else if (minutes() > minutes(mMin))
                                return 0;
                              else if (seconds() > seconds(mMin))
                                return 0;
                              else
                                return milliseconds(mMin);
                            }

                            else
                              CYWARNINGTEXT(QString("int CYTime::minSection(): %1=> HOUR_MIN_SEC_MSEC").arg(objectName()));
                            break;

    case MIN_SEC_MSEC     : if (mSection == Minutes)
                              return minutes(mMin);
                            else if (mSection == Seconds)
                            {
                              if (minutes() > minutes(mMin))
                                return 0;
                              else
                                return seconds(mMin);
                            }
                            else if (mSection == Milliseconds)
                            {
                              if (minutes() > minutes(mMin))
                                return 0;
                              else if (seconds() > seconds(mMin))
                                return 0;
                              else
                                return milliseconds(mMin);
                            }

                            else
                              CYWARNINGTEXT(QString("int CYTime::minSection(): %1=> MIN_SEC_MSEC").arg(objectName()));
                            break;

    default               : CYWARNINGTEXT(QString("CYTime::void CYTime::minSection(): %1=> format ?").arg(objectName()));
  }
  return 0;
}

int CYTime::maxSection()
{
	double kmsec = 1.0;
    if (mBase<10.0) //ms
		kmsec = 1.0;
    else if (mBase<100.0) //10ms
		kmsec = 10.0;
    else if (mBase<1000.0) //100ms
		kmsec = 100.0;

  switch (mTimeFormat)
  {
    case HOUR             : return hours(mMax);
    case MIN              : return minutes(mMax);
    case SEC              : return seconds(mMax);
    case MSEC             : return milliseconds(mMax);

    case HOUR_MIN         : if (mSection == Hours)
                              return hours(mMax);
                            else if (mSection == Minutes)
                            {
                              if (hours() < hours(mMax))
                                return 59;
                              else
                                return minutes(mMax);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> HOUR_MIN").arg(objectName()));
                            break;

    case MIN_SEC          : if (mSection == Minutes)
                              return minutes(mMax);
                            else if (mSection == Seconds)
                            {
                              if (minutes() < minutes(mMax))
                                return 59;
                              else
                                return seconds(mMax);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> MIN_SEC").arg(objectName()));
                            break;

    case SEC_MSEC         : if (mSection == Seconds)
                              return seconds(mMax);
                            else if (mSection == Milliseconds)
                            {
                              if (seconds() < seconds(mMax))
																return 999/kmsec;
                              else
                                return milliseconds(mMax);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> SEC_MSEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC     : if (mSection == Hours)
                              return hours(mMax);
                            else if (mSection == Minutes)
                            {
                              if (hours() < hours(mMax))
                                return 59;
                              else
                                return minutes(mMax);
                            }
                            else if (mSection == Seconds)
                            {
                              if (hours() < hours(mMax))
                                return 59;
                              else if (minutes() < minutes(mMax))
                                return 59;
                              else
                                return seconds(mMax);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> HOUR_MIN_SEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC_MSEC: if (mSection == Hours)
                              return hours(mMax);
                            else if (mSection == Minutes)
                            {
                              if (hours() < hours(mMax))
                                return 59;
                              else
                                return minutes(mMax);
                            }
                            else if (mSection == Seconds)
                            {
                              if (hours() < hours(mMax))
                                return 59;
                              else if (minutes() < minutes(mMax))
                                return 59;
                              else
                                return seconds(mMax);
                            }
                            else if (mSection == Milliseconds)
                            {
                              if (hours() < hours(mMax))
																return 999/kmsec;
                              else if (minutes() < minutes(mMax))
																return 999/kmsec;
                              else if (seconds() < seconds(mMax))
																return 999/kmsec;
                              else
                                return milliseconds(mMax);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> HOUR_MIN_SEC_MSEC").arg(objectName()));
                            break;

    case MIN_SEC_MSEC     : if (mSection == Minutes)
                              return minutes(mMax);
                            else if (mSection == Seconds)
                            {
                              if (hours() < hours(mMax))
                                return 59;
                              else if (minutes() < minutes(mMax))
                                return 59;
                              else
                                return seconds(mMax);
                            }
                            else if (mSection == Milliseconds)
                            {
                              if (hours() < hours(mMax))
																return 999/kmsec;
                              else if (minutes() < minutes(mMax))
																return 999/kmsec;
                              else if (seconds() < seconds(mMax))
																return 999/kmsec;
                              else
                                return milliseconds(mMax);
                            }
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> MIN_SEC_MSEC").arg(objectName()));
                            break;

    default               : CYWARNINGTEXT(QString("CYTime::void CYTime::maxSection(): %1=> format ?").arg(objectName()));
  }
  return 0;
}

double CYTime::precisionSection()
{
  switch (mTimeFormat)
  {
    case HOUR             : return precision();
    case MIN              : return precision();
    case SEC              : return precision();
    case MSEC             : return precision();

    case HOUR_MIN         : if (mSection == Hours)
                              return 1;
                            else if (mSection == Minutes)
                              return 1;
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> HOUR_MIN").arg(objectName()));
                            break;

    case MIN_SEC          : if (mSection == Minutes)
                              return 1;
                            else if (mSection == Seconds)
                              return 1;
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> MIN_SEC").arg(objectName()));
                            break;

    case SEC_MSEC         : if (mSection == Seconds)
                              return 1;
                            else if (mSection == Milliseconds)
                              return precision();
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> SEC_MSEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC     : if (mSection == Hours)
                              return 1;
                            else if (mSection == Minutes)
                              return 1;
                            else if (mSection == Seconds)
                              return 1;
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> HOUR_MIN_SEC").arg(objectName()));
                            break;

    case HOUR_MIN_SEC_MSEC: if (mSection == Hours)
                              return 1;
                            else if (mSection == Minutes)
                              return 1;
                            else if (mSection == Seconds)
                              return 1;
                            else if (mSection == Milliseconds)
                              return precision();
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> HOUR_MIN_SEC_MSEC").arg(objectName()));
                            break;

    case MIN_SEC_MSEC     : if (mSection == Minutes)
                              return 1;
                            else if (mSection == Seconds)
                              return 1;
                            else if (mSection == Milliseconds)
                              return precision();
                            else
                              CYWARNINGTEXT(QString("int CYTime::maxSection(): %1=> MIN_SEC_MSEC").arg(objectName()));
                            break;

    default               : CYWARNINGTEXT(QString("CYTime::void CYTime::maxSection(): %1=> format ?").arg(objectName()));
  }
  return 0;
}

void CYTime::sections()
{
  mHours        = hours();
  mMinutes      = minutes();
  mSeconds      = seconds();
  mMilliseconds = milliseconds();
}

void CYTime::setHours(const int val)
{
  if (mBase<1000.0)
    mTmp = (double)input(output(mTmp) + 3600000.0*(val-hours()));
  else
    mTmp = (double)input((3600.0*mBase*val + 60.0*mBase*minutes() + mBase*seconds() + milliseconds())/mBase);
  sections();
}

void CYTime::setMinutes(const int val)
{
  if (mBase<1000.0)
    mTmp = (double)input(output(mTmp) + 60000.0*(val-minutes()));
  else
    mTmp = (double)input((3600.0*mBase*hours() + 60.0*mBase*val + mBase*seconds() + milliseconds())/mBase);
  sections();
}

void CYTime::setSeconds(const int val)
{
  if (mBase<1000.0)
    mTmp = (double)input(output(mTmp) + 1000.0*(val-seconds()));
  else
    mTmp = (double)input((3600.0*mBase*hours() + 60.0*mBase*minutes() + mBase*val + milliseconds())/mBase);
  sections();
}

void CYTime::setMilliseconds(const int val)
{
  if (mBase<1000.0)
	{
		double kmsec = 1.0;
        if (mBase<10.0) //ms
			kmsec = 1.0;
        else if (mBase<100.0) //10ms
			kmsec = 10.0;
        else if (mBase<1000.0) //100ms
			kmsec = 100.0;
		mTmp = (double)input(output(mTmp) + (val*kmsec-milliseconds()));
	}
	else
    mTmp = (double)input((3600.0*mBase*hours() + 60.0*mBase*minutes() + mBase*seconds()+ val)/mBase);
  sections();
}

QString CYTime::displayHelp(bool whole, bool oneLine, bool withGroup, bool withValue)
{
  QString minTxt = toString((double)(min()), true, false);
  QString maxTxt = toString((double)(max()), true, false);

  sections();

  QString tmp;
  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(group()));
  if (phys()!="")
    tmp.append(QString(" [%1]").arg(phys()));
  if (elec()!="")
    tmp.append(QString(" [%1]").arg(elec()));
  if (addr()!="")
    tmp.append(QString(" [%1]").arg(addr()));
  tmp.append(label());

	if (withValue)
		tmp.append(QString(" = <b>%1</b>").arg(toString()));

  if ( !oneLine )
  {
    if (!tmp.isEmpty())
      tmp.append("<hr>");
    else
      tmp.append("<br>");
    if (help()!="")
      tmp.append(help());
    if (whole)
    {
      QString tmp2;
      if (!mAutoMinMax)
        tmp2.append("<li>"+tr("Value from %1 to %2.").arg(minTxt).arg(maxTxt)+"</li>");
      if (precision())
        tmp2.append("<li>"+tr("Precision is %1 %2.").arg(precision()).arg(unit())+"</li>");
      if (!tmp2.isEmpty())
        tmp.append("<ul>"+tmp2+"</ul>");
    }
    if (!note().isEmpty()) tmp.append(note());
  }
  return tmp;
}

QString CYTime::inputHelp(bool whole, bool withGroup)
{
  QString minTxt = toString((double)(min()), true, false);
  QString maxTxt = toString((double)(max()), true, false);
  QString defTxt = toString((double)(def()), true, false);

  sections();

  QString tmp = "";
  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(group()));
  if (label()!=objectName())
    tmp.append(label());
  else
    tmp.append("<br>"+label());
  if (!tmp.isEmpty())
    tmp.append("<hr>");
  else
    tmp.append("<br>");
  if (help()!="")
    tmp.append(help());
  if (whole)
  {
    QString tmp2;
    if (!mAutoMinMax)
      tmp2.append("<li>"+tr("Value from %1 to %2.").arg(minTxt).arg(maxTxt)+"</li>");
    if (precision())
      tmp2.append("<li>"+tr("Precision is %1 %2.").arg(precision()).arg(unit())+"</li>");
    if (helpDesignerValue())
      tmp2.append("<li>"+tr("Designer value: %1.").arg(defTxt)+"</li>");
    if (!tmp2.isEmpty())
      tmp.append("<ul>"+tmp2+"</ul>");
  }
  if (!note().isEmpty()) tmp.append(note());
  return tmp;
}

bool CYTime::isValid()
{
  if ((mTmp>=(double)(mMin)) && (mTmp<=(double)(mMax)))
    return true;
  return false;
}

u32 CYTime::stringTimeToVal(QString &string)
{
  QStringList list;
  QRegExp rx;
  int pos;

  rx = QRegExp( "(\\D+)" );
  pos = 0;
  while ( pos >= 0 )
  {
    pos = rx.indexIn( string, pos );
    if ( pos > -1 )
    {
        list += rx.cap( 1 );
        pos  += rx.matchedLength();
    }
  }
  QStringList stringUnit = list;

  list.clear();
  rx = QRegExp( "(\\d+)" );
  pos = 0;
  while ( pos >= 0 )
  {
    pos = rx.indexIn( string, pos );
    if ( pos > -1 )
    {
        list += rx.cap( 1 );
        pos  += rx.matchedLength();
    }
  }
  QStringList stringDig = list;

  QList<QString*> values;
  for ( QStringList::Iterator it = stringDig.begin(); it != stringDig.end(); ++it )
    values.append(new QString(*it));

  double sec = 0.0;
  if (mBase<1.0)
    mBase = 1.0;

  mHours        = 0;
  mMinutes      = 0;
  mSeconds      = 0;
  mMilliseconds = 0;

  QString format = stringUnit.join("_");
  format.replace(" ", "");

  QListIterator<QString *> it2(values);
  if ((format == "hour") || (format == "h"))
  {
    mTimeFormatTmp = HOUR;
    mHours = values[0]->toInt();
    sec = (double)(3600.0*mHours);
  }
  else if ((format == "min") || (format == "m"))
  {
    mTimeFormatTmp = MIN;
    mMinutes = values[0]->toInt();
    sec = (double)(60.0*mMinutes);
  }
  else if ((format == "sec") || (format == "s"))
  {
    mTimeFormatTmp = SEC;
    mSeconds = values[0]->toInt();
    sec = (double)(mSeconds);
  }
  else if ((format == "msec") || (format == "ms") )
  {
    mTimeFormatTmp = MSEC;
    mMilliseconds = values[0]->toInt();
    sec = (double)(0.0);
  }
  else if ((format == "hour_min") || (format == "h_m"))
  {
    mTimeFormatTmp = HOUR_MIN;
    mHours = values[0]->toInt();
    mMinutes = values[1]->toInt();
    sec = (double)(60.0*(60.0*mHours + mMinutes));
  }
  else if ((format == "hour_min_sec") || (format == "h_m_s"))
  {
    mTimeFormatTmp = HOUR_MIN_SEC;
    mHours = values[0]->toInt();
    mMinutes = values[1]->toInt();
    mSeconds = values[2]->toInt();
    sec = (double)(3600.0*mHours + 60.0*mMinutes + mSeconds);
  }
  else if ((format == "min_sec") || (format == "m_s"))
  {
    mTimeFormatTmp = MIN_SEC;
    mMinutes = values[0]->toInt();
    mSeconds = values[1]->toInt();
    sec = (double)(60.0*mMinutes + mSeconds);
  }
  else if ((format == "sec_msec") || (format == "s_ms"))
  {
    mTimeFormatTmp = SEC_MSEC;
    mSeconds = values[0]->toInt();
    mMilliseconds = values[1]->toInt();
    sec = (double)(mSeconds);
  }
  else if ((format == "hour_min_sec_msec") || (format == "h_m_s_ms"))
  {
    mTimeFormatTmp = HOUR_MIN_SEC_MSEC;
    mHours = values[0]->toInt();
    mMinutes = values[1]->toInt();
    mSeconds = values[2]->toInt();
    mMilliseconds = values[3]->toInt();
    sec = (double)(3600.0*mHours + 60.0*mMinutes + mSeconds);
  }
  else if ((format == "min_sec_msec") || (format == "m_s_ms"))
  {
    mTimeFormatTmp = MIN_SEC_MSEC;
    mMinutes = values[0]->toInt();
    mSeconds = values[1]->toInt();
    mMilliseconds = values[2]->toInt();
    sec = (double)(60.0*mMinutes + mSeconds);
  }
  else
    CYFATALTEXT(format);

  double msec;
//   if ( (mBase<BASE_SEC) && (mBase>=BASE_MSEC))
//     msec = (double)(sec*1000.0 + mMilliseconds);
//   else if ( mBase<BASE_SEC )
//     msec = (double)(sec*BASE_SEC + mMilliseconds);
//   else if (mBase < BASE_MIN)
//     msec = (double)(sec + mMilliseconds);
//   else if (mBase < BASE_HOUR)
//     msec = (double)(sec/60.0 + mMilliseconds);
//   else
//     msec = (double)(sec/3600.0 + mMilliseconds);

  msec = (double)(sec*1000.0 + mMilliseconds);

/*  double tmp = input(msec);*/
  double tmp = msec/mBase;
  if (tmp>UINT_MAX)
    CYFATALDATA(objectName());
  return (u32)tmp;
}

int CYTime::nbSections()
{
  switch(mTimeFormatTmp)
  {
    case HOUR             :
    case MIN              :
    case SEC              :
    case MSEC             : return 1;
    case HOUR_MIN         :
    case MIN_SEC          :
    case SEC_MSEC         : return 2;
    case MIN_SEC_MSEC     :
    case HOUR_MIN_SEC     : return 3;
    case HOUR_MIN_SEC_MSEC: return 4;
    default               : CYFATALDATA(objectName());
  }
  return 0;
}

int CYTime::hours()
{
  return hours(mTmp);
}

int CYTime::minutes()
{
  return minutes(mTmp);
}

int CYTime::seconds()
{
  return seconds(mTmp);
}

int CYTime::milliseconds()
{
  return milliseconds(mTmp);
}

int CYTime::hours(const double val)
{
  int res;
  switch(mTimeFormat)
  {
    case HOUR             :
    case HOUR_MIN         :
    case HOUR_MIN_SEC     :
    case HOUR_MIN_SEC_MSEC: res = (int)(ms(val)/3600000.0);
                            return res;
    default               : return 0;
  }
}

int CYTime::minutes(const double val)
{
  int res;
  switch(mTimeFormat)
  {
    case MIN              :
    case MIN_SEC          :
    case MIN_SEC_MSEC     : res = (int)(ms(val)/60000.0);
                            return res;
    case HOUR_MIN         :
    case HOUR_MIN_SEC     :
    case HOUR_MIN_SEC_MSEC: res = (int)((ms(val)/60000.0) - 60.0*hours(val));
                            return res;
    default               : return 0;
  }
}

int CYTime::seconds(const double val)
{
  int res;
  switch(mTimeFormat)
  {
    case SEC              :
    case SEC_MSEC         : res = (int)(ms(val)/1000.0);
                            return res;
    case MIN_SEC          :
    case MIN_SEC_MSEC     : res = (int)((ms(val)/1000.0) - 60.0*minutes(val));
                            return res;
    case HOUR_MIN         :
    case HOUR_MIN_SEC     :
    case HOUR_MIN_SEC_MSEC: res = (int)((ms(val)/1000.0) - 3600.0*hours(val) - 60.0*minutes(val));
                            return res;
    default               : return 0;
  }
}

int CYTime::milliseconds(const double val)
{
  int res;
  double MS;
  double dif;
  switch(mTimeFormat)
  {
    case MSEC             : res = (int)(ms(val));
                            return res;
    case SEC              :
    case SEC_MSEC         : res = (int)(ms(val) - 1000.0*seconds(val));
                            return res;
    case MIN_SEC          :
    case MIN_SEC_MSEC     : res = (int)(ms(val) - 1000.0*(60.0*minutes(val) + seconds(val)));
                            return res;
    case HOUR_MIN         :
    case HOUR_MIN_SEC     :
//     case HOUR_MIN_SEC_MSEC: return (int)(ms(val) - 1000.0*(60.0*(60.0*hours(val) + minutes(val)) + seconds(val)));
    case HOUR_MIN_SEC_MSEC: MS = ms(val);
                            dif = 1000.0*(60.0*(60.0*hours(val) + minutes(val)) + seconds(val));
                            res = (int)(MS - dif);
                            return res;
    default               : return 0;
  }
}

double CYTime::h()
{
  return m()/60.0;
}

double CYTime::m()
{
  return s()/60.0;
}

double CYTime::s()
{
  return ms()/1000.0;
}

double CYTime::ms()
{
  return ms(mTmp);
}

double CYTime::ms(const double val)
{
  return val*mBase;
}

u32 CYTime::input(const u32 val)
{
  double ms;
  if (mBase >= BASE_HOUR)
    ms = BASE_HOUR;
  else if (mBase >= BASE_MIN)
    ms = BASE_MIN;
  else if (mBase >= BASE_SEC)
    ms = BASE_SEC;
  else
    ms = 1;

  double res = ((val*ms)+(mBase/2))/mBase;
//   double res = ((val*mBase)+(mBase/2))/mBase;
  if (res>UINT_MAX)
    return UINT_MAX;
  return (u32)(res);
}

u32 CYTime::output(const u32 val)
{
  float ms;
  if (mBase >= BASE_HOUR)
    ms = BASE_HOUR;
  else if (mBase >= BASE_MIN)
    ms = BASE_MIN;
  else if (mBase >= BASE_SEC)
    ms = BASE_SEC;
  else
    ms = 1;

  double res = (val*mBase)/(ms);
  if (res>UINT_MAX)
    return UINT_MAX;
  return (u32)(res);
}
