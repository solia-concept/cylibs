/***************************************************************************
                          cys8.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cys8.h"

CYS8::CYS8(CYDB *db, const QString &name, int format, Mode mode)
  : CYNum<s8>(db, name, format, mode, VS8)
{
}

CYS8::CYS8(CYDB *db, const QString &name, s8 *val, int format, Mode mode)
  : CYNum<s8>(db, name, val, format, mode, VS8)
{
}

CYS8::CYS8(CYDB *db, const QString &name, s8 *val, const s8 def, int format, Mode mode)
  : CYNum<s8>(db, name, val, def, format, mode, VS8)
{
}

CYS8::CYS8(CYDB *db, const QString &name, s8 *val, const s8 def, const s8 min, const s8 max, int format, Mode mode)
  : CYNum<s8>(db, name, val, def, min, max, format, mode, VS8)
{
}

CYS8::CYS8(CYDB *db, const QString &name, CYNum<s8>::Attr1<s8> *attr)
  : CYNum<s8>(db, name, attr, VS8)
{
}

CYS8::CYS8(CYDB *db, const QString &name, CYNum<s8>::Attr2<s8> *attr)
  : CYNum<s8>(db, name, attr, VS8)
{
}

CYS8::CYS8(CYDB *db, const QString &name, CYNum<s8>::Attr3<s8> *attr)
  : CYNum<s8>(db, name, attr, VS8)
{
}

CYS8::CYS8(CYDB *db, const QString &name, CYNum<s8>::Attr4<s8> *attr)
  : CYNum<s8>(db, name, attr, VS8)
{
}

CYS8::~CYS8()
{
}
