/***************************************************************************
                          cys16.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYS16_H
#define CYS16_H

// CYLIBS
#include "cynum.h"

/** CYS16 est une donnée entière de 16 bits.
  * @short Donnée entière de 16 bits.
  * @author Gérald LE CLEACH
  */

class CYS16 : public CYNum<s16>
{
public:
  /** Création d'une donnée entière de 16 bits.
    * @param db     Base de données parent.  
    * @param name   Nom de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS16(CYDB *db, const QString &name, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 16 bits avec sa valeur.
    * @param db     Base de données parent.  
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS16(CYDB *db, const QString &name, s16 *val, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 16 bits avec sa valeur ainsi que sa valeur par défaut.
    * @param db    Base de données parent.  
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS16(CYDB *db, const QString &name, s16 *val, const s16 def, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 16 bits avec sa valeur ainsi que les valeurs par défaut, maxi et mini.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Valeur mini.
    * @param max    Valeur maxi.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYS16(CYDB *db, const QString &name, s16 *val, const s16 def, const s16 min, const s16 max, int format=0, Mode mode=Sys);

  /** Création d'une donnée entière de 16 bits avec les attributs Attr1.
    * @param db    Base de données parent.  
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS16(CYDB *db, const QString &name, CYNum<s16>::Attr1<s16> *attr);
  
  /** Création d'une donnée entière de 16 bits avec les attributs Attr2.
    * @param db    Base de données parent.  
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS16(CYDB *db, const QString &name, CYNum<s16>::Attr2<s16> *attr);

  /** Création d'une donnée entière de 16 bits avec les attributs Attr3.
    * @param db    Base de données parent.  
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS16(CYDB *db, const QString &name, CYNum<s16>::Attr3<s16> *attr);

  /** Création d'une donnée entière de 16 bits avec les attributs Attr4.
    * @param db    Base de données parent.  
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYS16(CYDB *db, const QString &name, CYNum<s16>::Attr4<s16> *attr);
  
  ~CYS16();
};

#endif
