/***************************************************************************
                          cystring.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cystring.h"
#include <stdio.h>
#include <cysimplecrypt.h>

CYString::CYString(CYDB *db, const QString &name, QString *val, QString label, Cy::Mode mode)
  : CYType<QString>(db, name, 0, mode, String)
{
  mNbChar = -1;
  mPtrChar = 0;
  mCryptographicHash =0 ;
  mSimpleCrypt = 0;

  if (val)
    mVal = val;
  else
    mVal = new QString("");

  mDef = *mVal;

  setNbDec(0);
  mLabel = label;

  mEnableColor = Qt::black;


  mOldVal = CYString::val();
}

CYString::CYString(CYDB *db, const QString &name, char *ptrChar, int nbChar, QString label, Cy::Mode mode)
  : CYType<QString>(db, name, 0, Sys, String)
{
  mNbChar = nbChar;
  if (mNbChar<0)
    CYFATALDATA(name)
  mPtrChar = ptrChar;
  mCryptographicHash =0 ;
  mSimpleCrypt = 0;

  mVal = new QString("");
  *mVal  = val();
  mLabel = label;
  mMode  = mode;
  setNbDec(0);

  mDef = *mVal;

  mEnableColor = Qt::black;


  mOldVal = CYString::val();
}

CYString::CYString(CYDB *db, const QString &name, Attr *attr)
  : CYType<QString>(db, name, 0, Sys, String)
{
  mNbChar = attr->nbChar;
  if (mNbChar<0)
    CYFATALDATA(name)
  mPtrChar = attr->ptrChar;
  mCryptographicHash =0 ;
  mSimpleCrypt = 0;

  mVal = new QString("");
  *mVal  = val();
  mLabel = attr->label;
  mMode  = attr->mode;
  setNbDec(0);

  mDef = *mVal;

  mEnableColor = Qt::black;

  mOldVal = CYString::val();
}

CYString::~CYString()
{
}

QString CYString::val()
{
  if (mPtrChar && (mNbChar>0))
  {
    char buf[mNbChar];
    sprintf(buf,"%s", mPtrChar);
    *mVal = buf;
    mVal->replace("", "é");
    mVal->replace("", "è");
    mVal->replace("", "ê");
    mVal->replace("", "î");
    mVal->replace("", "ç");
    mVal->replace("", "à");
    mVal->replace("", "â");
    mVal->replace("", "ô");
    mVal->replace("", "û");
  }
  else if (mNbChar>0)
  {
    mVal->truncate(mNbChar);
  }
  return *mVal;
}

void CYString::setVal(QString val)
{
  if (mNbChar>0)
    val.truncate(mNbChar);

  if (mCryptographicHash)
  {
    mCryptographicHash->addData(val.toUtf8());
    *mVal = mCryptographicHash->result();
  }
  else if (mSimpleCrypt)
    *mVal = mSimpleCrypt->encryptToString(val);
  else
    *mVal = val;

  if (mPtrChar && (mNbChar>0) && !mVal->toUtf8().isNull())
    sprintf(mPtrChar,"%s", mVal->toUtf8().data());
  emit valueChanged();
}

void CYString::setCryptographicHash(QCryptographicHash *hash)
{
  mCryptographicHash = hash;
}

void CYString::setSimpleCrypt(CYSimpleCrypt *simpleCrypt)
{
  mSimpleCrypt = simpleCrypt;
}

QString CYString::simpleDecrypt(QString txt)
{
  if (mSimpleCrypt)
    return mSimpleCrypt->decryptToString(txt);
  else
    return txt;
}

QString CYString::simpleDecrypt()
{
  return simpleDecrypt(val());
}

QString CYString::toString(bool withUnit, bool ctrlValid, Cy::NumBase base)
{
  Q_UNUSED(withUnit)
  Q_UNUSED(ctrlValid)
  Q_UNUSED(base)
  return val();
}

QString CYString::inputHelp(bool whole, bool withGroup)
{
  QString tmp = "";
  if (withGroup)
    tmp.append(QString("<b>%1: </b>").arg(group()));
  if (label()!=objectName())
    tmp.append(label());
  else
    tmp.append("<br>"+label());
  if (phys()!="")
    tmp.append(QString(" [%1]").arg(phys()));
  if (elec()!="")
    tmp.append(QString(" [%1]").arg(elec()));
  if (addr()!="")
    tmp.append(QString(" [%1]").arg(addr()));
  if (!tmp.isEmpty())
    tmp.append("<hr>");
  else
    tmp.append("<br>");
  if (help()!="")
    tmp.append(help());
  if (whole && !def().isEmpty() && helpDesignerValue())
    tmp.append("<ul><li>"+tr("Designer value: %1.").arg(def())+"</li></ul>");
  if ( !note().isEmpty() ) tmp.append("<br>"+note());
  if (nbChar()>0)
  {
    tmp.append("<br>"+tr("The maximum number of characters is %1.").arg(nbChar()));
    tmp.append("<br>"+tr("If the string is too long it will be truncated!"));
  }
  return tmp;
}


bool CYString::findSection(const QString &section, const QString &sep)
{
  QStringList list;
  const QString str = val();
  list = str.split(sep, Qt::SkipEmptyParts);
  if (list.indexOf(section)==-1)
    return false;
  return true;
}


void CYString::prepend(QString txt)
{
  setVal(txt+val());
}


void CYString::append(QString txt)
{
  setVal(val()+txt);
}
