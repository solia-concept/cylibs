/***************************************************************************
                          cyf32.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyf32.h"

CYF32::CYF32(CYDB *db, const QString &name, int format, Mode mode)
  : CYNum<f32>(db, name, format, mode, VF32)
{
}

CYF32::CYF32(CYDB *db, const QString &name, f32 *val, int format, Mode mode)
  : CYNum<f32>(db, name, val, format, mode, VF32)
{
}

CYF32::CYF32(CYDB *db, const QString &name, f32 *val, const f32 def, int format, Mode mode)
  : CYNum<f32>(db, name, val, def, format, mode, VF32)
{
}

CYF32::CYF32(CYDB *db, const QString &name, f32 *val, const f32 def, const f32 min, const f32 max, int format, Mode mode)
  : CYNum<f32>(db, name, val, def, min, max, format, mode, VF32)
{
}

CYF32::CYF32(CYDB *db, const QString &name, CYNum<f32>::Attr1<f32> *attr)
  : CYNum<f32>(db, name, attr, VF32)
{
}

CYF32::CYF32(CYDB *db, const QString &name, CYNum<f32>::Attr2<f32> *attr)
  : CYNum<f32>(db, name, attr, VF32)
{
}

CYF32::CYF32(CYDB *db, const QString &name, CYNum<f32>::Attr3<f32> *attr)
  : CYNum<f32>(db, name, attr, VF32)
{
}

CYF32::CYF32(CYDB *db, const QString &name, CYNum<f32>::Attr4<f32> *attr)
  : CYNum<f32>(db, name, attr, VF32)
{
}

CYF32::~CYF32()
{
}
