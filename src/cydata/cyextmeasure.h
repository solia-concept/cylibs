//
// C++ Interface: cyextmeasure
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYEXTMEASURE_H
#define CYEXTMEASURE_H

// CYLIBS
#include "cymeasure.h"
#include "cyana.h"

class CYString;
class CYU8;
class CYF64;
class CYAna;
class CYFlag;

/**
@short Mesure externe

  @author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYExtMeasure : public CYMeasure
{
  Q_OBJECT

public:
  /** Création d'une Mesure physique.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param flag   Flag de validité.
    * @param format Numéro du format d'affichage dans l'application.
    * @param mode   Mode d'utilisation. */
  CYExtMeasure(CYDB *db, const QString &name, f32 *val, flg *flg, int format=0, Mode mode=Sys);

  /** Création d'une Mesure physique.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param attr  Attributs de la donnée. */
  CYExtMeasure(CYDB *db, const QString &name, CYMeasure::Attr *attr);

  ~CYExtMeasure();

  void initSetting(CYDB *db, QString ana, flg *enable, f32 *top, f32 *low, f32 *ana_top, f32 *ana_low, f32 *level=0, f32 *ita=0, f32 *itd=0, s16 *cal=0, bool def_enable=false, QString underGroup="");

  void initSetting(CYDB *db, flg *enable, f32 *level, f32 *ita, f32 *itd, s16 *cal, bool def_enable=false, QString underGroup="");
  /** Saisie les valeurs par défauts des points haut et bas*/
  void setDef(f32 top, f32 low);

  bool editScale();
  bool editFormat();
  bool editSupervision();
  virtual void setSameFormatData(QByteArray dataName);
  virtual void setSameFormatData( CYF32 *data );
  bool isEnabled();

  /** @return la donnée d'activation de la voie externe. */
  CYFlag * enableData() { return mEnable; }

  /** Ajoute un type de calibrage d'une mesure paramétrable.
      * @param def place de de type comme valeur par défaut. */
  virtual void addTypeCal(CYAna::CAL type, QString label, bool def=false);
  /** Saisie le type de calibrage d'une mesure paramétrable. */
  virtual void setTypeCal(int type);
  /** @return le type de calibrage d'une mesure paramétrable. */
  virtual int typeCal();

public slots:
  virtual void updateFormat();
  virtual void setEditScale( bool val );
  virtual void setEditFormat( bool val );
  virtual void setEditSupervision( bool val );

protected:
  virtual void init();

public:
  /** Base données de paramètrage. */
  CYDB * mSettingDB;
  CYString * mUnit;
  CYString * mLabel;

  /** Donnée d'activation de la voie externe. */
  CYFlag * mEnable;
  /** Donnée du seuil haut. */
  CYF32 * mTop;
  /** Donnée du seuil bas. */
  CYF32 * mLow;
  /** Donnée analogique rattachée. */
  CYAna * mAna;
  /** Donnée du seuil haut ANA. */
  CYF32 * mAnaTop;
  /** Donnée du seuil bas ANA. */
  CYF32 * mAnaLow;
  /** Donnée niveau de surveillance. */
  CYF32 * mLevel;
  /** Donnée intervale d'alerte de surveillance. */
  CYF32 * mITA;
  /** Donnée intervale de défaut de surveillance. */
  CYF32 * mITD;
  /** Donnée du type de calibrage. */
  CYS16 * mCal;

  bool mEditScale;
  bool mEditFormat;
  bool mEditSupervision;
  QList<CYF32*> mSameFormatDatas;
};

#endif
