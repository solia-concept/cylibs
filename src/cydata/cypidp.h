/***************************************************************************
                          cypidp.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYPIDP_H
#define CYPIDP_H

// CYLIBS
#include "cyf32.h"

/** CYPIDP représente le coefficient proportionnel d'un PID.
  * @short Coefficient proportionnel de PID.
  * @author Gérald LE CLEACH
  */

class CYPIDP : public CYF32
{
  Q_OBJECT
public:
  /** Création d'un coefficient proportionnel de PID.
    * @param db    Base de données parent.
    * @param name  Nom de la donnée.
    * @param val   Valeur de la donnée.
    * @param def   Valeur par défaut.
    * @param min   Limite inférieure.
    * @param max   Limite supérieure. */
  CYPIDP(CYDB *db, const QString &name, f32 *val, const f32 def=1.0, const f32 min=0.0, const f32 max=99.0);
  ~CYPIDP();
};

#endif
