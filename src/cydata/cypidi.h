/***************************************************************************
                          cypidi.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYPIDI_H
#define CYPIDI_H

// CYLIBS
#include "cyf32.h"

/** CYPIDI représente le coefficient intégral d'un PID.
  * @short Coefficient intégral de PID.
  * @author Gérald LE CLEACH
  */

class CYPIDI : public CYF32
{
  Q_OBJECT
public:
  /** Création d'un coefficient intégral de PID.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut.
    * @param min    Limite inférieure.
    * @param max    Limite supérieure.
    * @param format Numéro du format d'affichage dans l'application. */
  CYPIDI(CYDB *db, const QString &name, f32 *val, const f32 def=0.1, const f32 min=0.0, const f32 max=9000.0, int format=0);

  ~CYPIDI();
};

#endif
