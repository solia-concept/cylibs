/***************************************************************************
                          cyeventsreport.h  -  description
                             -------------------
    début                  : jeu aoû 21 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYFILECSV_H
#define CYFILECSV_H

// QT
#include <qobject.h>
#include <qcolor.h>
#include <qstringlist.h>
#include <qdir.h>

class CYDB;
class CYS32;
class CYF64;

/** @short Enregistrement en fichiers CSV suivant la taille ou le nombre de lignes max.
  * @author Gérald LE CLEACH
  */

class CYFileCSV : public QObject
{
  Q_OBJECT

public: 
  /** Création d'un enregistrement en fichiers CSV.
   *  @param parent 		Parent
   *  @param name 			Nom
   *  @param label 			Libellé d'enregistrement.
   *  @param path 			Chemin du répertoire contenant les fichiers d'enregistrement.
   *  @param fileNum  	Activation d'un numéro dans le nom de fichier à la place de la date et l'heure de création.
   *  @param maxSize    Taille max de fichier (0 supprime la limite).
   *  @param maxLines 	Nombre max de lignes (0 supprime la limite).
   */
  CYFileCSV(QObject *parent=0, const QString &name=0, QString label=0, QString path=0, bool fileNum=false, int maxSize=102400, int maxLines=1000, bool useNewFile=false);
  ~CYFileCSV() {}


  /** Ajout d'une ligne. */
  void addLine(QString line);
  /** Ajout des lignes. */
  void addLines(QStringList newlines);
  /** Fixe les lignes d'en-tête. */
  void setHeaderLines(QStringList lines) { mHeaderLines=lines; }

  /** @return le fichier de journalisation. 
      @param newFile Force la création d'un nouveau fichier de journalisation*/
  QFile *findFile(bool newFile=false, int newlines=0, int newSize=0);
  /** @return le nouveau fichier de journalisation. */
  QFile *newFile(QString filename);

  /** @return le nom du fichier de journalisation. */
  QString fileName() { return mFileName; }
  /** Saisie le nom du fichier de journalisation. */
  void setFileName(const QString &filename) { mFileName = filename;}

  /** @return le chemin du répertoire d'archivage. */
  QString path();
  /** Saisie le chemin du répertoire d'archivage. */
  void setPath(const QString &path);

  /** @return la date et heure de chargement du projet en texte. */
  QString loadTimeToString();

//  /** @return la date et heure de chargement du projet . */
//  QDateTime loadTime() { return mLoadTime; }
//  /** Saisie la date et heure de chargement du projet . */
//  void setLoadTime(QDateTime time) { mLoadTime = time; }
//  /** @return la date et heure de chargement du projet en texte. */
//  QString loadTimeToString() { return mLoadTime.toString(Qt::ISODate).replace(QChar('T'), " / " ); }
//  /** Saisie la date et heure de chargement du projet en texte. */
//  void loadTimeFromString(const QString &time) { mLoadTime = QDateTime::fromString(time, Qt::ISODate); }
//
//  /** @return la date et heure de fermeture du projet . */
//  QDateTime closeTime() { return mCloseTime; }
//  /** Saisie la date et heure de fermeture du projet . */
//  void setCloseTime(QDateTime time) { mCloseTime = time; }
//  /** @return la date et heure de fermeture du projet en texte. */
//  QString closeTimeToString() { return mCloseTime.toString(Qt::ISODate).replace(QChar('T'), " / " ); }
//  /** Saisie la date et heure de fermeture du projet en texte. */
//  void closeTimeFromString(const QString &time) { mCloseTime = QDateTime::fromString(time, Qt::ISODate); }

  /** Met à jour le répertoire d'archivage. */
  void updateDir();
  /** Charge le journal d'évènements. */
  void load(QString filename);
  /** Charge le journal d'évènements. */
  void load();

public: // Public attributes
  /** Liste des lignes du fichier. */
  QStringList lines;
  /** Nombre de lignes par fichier. */
  CYS32 *maxLinesFile;
  /** Taille maximum d'un fichier. */
  CYF64 *maxFileSize;

protected: // Protected attributes
  /** Base de données. */
  CYDB *mDB;
  /** Répertoire d'archivage. */
  QDir *mDir;
  /** Libellé d'enregistrement. */
  QString mLabel;
  /** Chemin du répertoire d'archivage. */
  QString mPath;  
  /** Nom du fichier courrant. */
  QString mFileName;
  /** Prefix du nom du fichier. */
  QString mPrefix;
  /** Numéro du fichier courrant. */
  uint mFileNum;
  /** Indique s'il faut utiliser un nouveau fichier. */
  bool mUseNewFile;
//  /** Date et heure de chargement du projet . */
//  QDateTime mLoadTime;
//  /** Date et heure de fermeture du projet . */
//  QDateTime mCloseTime;
  /** Activation d'un numéro dans le nom de fichier. */
  bool mFileNumEnable;

  /** Nombre max de lignes. */
  int mMaxLines;
  /** Taille max de fichier. */
  int mMaxSize;

  QStringList mHeaderLines;
};

#endif
