//
// C++ Interface: cysmtp
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYSMTP_H
#define CYSMTP_H

// QT
#include <QSsl>
#include <QtNetwork/qsslsocket.h>
#include <QAbstractSocket>
#include <QTextStream>

/**
@short Simple Mail Transfer Protocol (Protocole simple de transfert de courrier)

@author Gérald LE CLEACH
*/
class CYSMTP : public QObject
{
Q_OBJECT
public:
    CYSMTP( const QString &from, const QString &to, const QString &subject, const QString &body );
    ~CYSMTP();

    void sendMail( const QString &from, const QString &to,const QString &subject, const QString &body);

signals:
    void status( const QString & );

private slots:
    void stateChanged(QAbstractSocket::SocketState socketState);
    void errorReceived(QAbstractSocket::SocketError socketError);
    void connected();
    void readyRead();
    void disconnected();

private:
    int timeout;
    QString message;
    QTextStream * t;
    QSslSocket *socket;
    QString from;
    QString rcpt;
    QString response;
    QString user;
    QString pass;
    QString host;
    int port;
    enum states{Tls, HandShake ,Auth,User,Pass,Rcpt,Mail,Data,Init,Body,Quit,Close};
    int state;
};

#endif
