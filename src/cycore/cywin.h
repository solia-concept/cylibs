/***************************************************************************
                          cywin.h  -  description
                             -------------------
    début                  : mar fév 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYWIN_H
#define CYWIN_H

// QT
#include <QMainWindow>
#include <QEvent>
#include <QHash>
#include <qaction.h>
#include <QMenuBar>
#include <QSettings>
#include <QDomElement>
// CYLIBS
#include <cyaction.h>
#include <cyactioncollection.h>

class CYUserAction;
class CYAction;
class CYStatusBar;
class CYStatusBarLabel;

/** CYWin constitue la fenêtre de base du superviseur CYLIX. Celle-ci possède
  * des menus, des barres d'outils et d'état et une vue centrale. Les différentes
  * fenêtres de l'application hérite de celle-ci.
  * @short Fenêtre de base de CYLIX.
  * @author Gérald LE CLEACH
  */

class CYWin : public QMainWindow
{
  Q_OBJECT

public:
  enum { MAX_RECENTFILES = 9 }; // Must not exceed 9

  enum ReverseStateChange { StateNoReverse, StateReverse };
  struct StateChange
  {
    QStringList actionsToEnable;
    QStringList actionsToDisable;
  };

  /** Construction de la fenêtre et installation des menus et des widgets
    * de la vue centrale.
    * @param parent   Widget parent.
    * @param name     Nom de la fenêtre.
    * @param index    Index de la fenêtre.
    * @param label    Libellé de la fenêtre.
    * @param f        Spécifie le flag widget. Par défaut il est à WType_TopLevel et WDestructiveClose.
    *                 TopLevel indique que la fenêtre est un widget de niveau supérieur, n'a aucun effet
    *                 par rapport à un éventuel parent.
    *                 DestructiveClose indique que la fenêtre est automatiquement détruite lorsque la
    *                 fenêtre est fermée.
    *                 Mettre 0 pour annuler ce pararmètre par défaut. */
  CYWin(QWidget *parent=0, const QString &name=0, int index=0, const QString &label=0, Qt::WindowFlags f=Qt::Widget,bool unique=true);
  ~CYWin();

  /** @return l'index de la fenêtre qui est utilisé pour donnée un ordre différents que celui alphabétique. */
  int index() { return mIndex; }
  /** @return le libellé de la fenêtre. */
  const QString label() { return mLabel; }

  /**
   * Restore the session specified by @p number.
   *
   * Returns @p false if this
   * fails, otherwise returns @p true and shows the window.
   * You should call canBeRestored() first.
   * If @p show is true (default), this widget will be shown automatically.
   */
  bool restore( int number, bool show = true );

  /**
   * <b>Session Management</b>
   *
   * Essaye de restaurer le widget de premier niveau tel que
   * défini par le nombre (1..x).
   *
   * Si la session ne contient pas un nombre si élevé, la
   * configuration n'est pas modifiée et retourne faux.
   **/
  static bool canBeRestored( int number );

  void savePropertiesInternal( QSettings *, int );
  bool readPropertiesInternal( QSettings *, int );

  virtual void saveProperties(QSettings *);
  virtual void readProperties(QSettings *);

  virtual void createGUI(const QString &xmlfile=0);

  /** Initialise le menu d'aide. */
  virtual void initHelpMenu();

  QHash<QString, QString*> & actionDisable();
  void setActionNoEdit( QString actions );
  bool actionEdit( QString action );
  void setActionDesigner( QString actions );
  bool actionDesigner( QString action );
  /** @return l'action ayant pour nom \a name de l'utilisateur courant. */
  virtual CYUserAction * userAction(QString name);
  /** @return l'action ayant pour nom \a name. */
  CYAction* action( const char* name );
  virtual CYActionCollection *actionCollection();
  /**
   * @return An iterator over the list of all toolbars for this window.
   */
//  QPtrListIterator<QToolBar> toolBarIterator();

  QMenu *helpMenu;

  bool enableEventsPanel() { return mEnableEventsPanel; }

  virtual bool okToClear();

  /** Crée le menu de langues dynamiquement selon les traductions existantes
   * à partir du contenu de mLangPath. */
  virtual void createLanguageMenu(QMenu *menu) { Q_UNUSED(menu) }

  Qt::ToolButtonStyle toolButtonStyle();

public slots: // Public slots
  /** change the status message to text */
  void slotStatusMsg(const QString& text);
//  virtual void show();
  virtual void setWindowTitle(const QString &caption, bool modified);
  virtual void setWindowTitle(const QString &caption);
  virtual void setPlainCaption(	const QString &caption);

  virtual void setEnableEventsPanel(bool val);

  virtual void whatsThis();
  virtual void aboutQt();

  virtual void registerRecentURL(const QUrl& url);
  virtual void updateRecentFilesMenu();
  virtual void fileOpenRecent( int index );

  void addStateActionEnabled(const QString& state, const QString& action);

  void addStateActionDisabled(const QString& state, const QString& action);

  StateChange getActionsToChangeForState(const QString& state);

  void setToolButtonStyle(Qt::ToolButtonStyle style);

protected:
  virtual void closeEvent(QCloseEvent *event);
  virtual void customEvent(QEvent *e);
  virtual void timerEvent(QTimerEvent *);
  virtual bool queryClose();
	virtual void setActionDisable( QString actionName, QString flagName);
	virtual void createGUIMenu(QMenu *menu_parent, QDomElement &el, QString path);
	virtual CYAction *createGUIAction(QMenu *menu_parent, QDomElement &el, QString path);
	virtual void createGUIMenu(QToolBar *toolBar, QDomElement &el);
	virtual CYAction *createGUIAction(QToolBar *toolBar, QDomElement &el);

  CYStatusBar *statusbar;
  bool dontSaveSession;
//  int timerId;

  /**
   * Actions can collectively be assigned a "State". To accomplish this
   * the respective actions are tagged as \<enable\> or \<disable\> in
   * a \<State\> \</State\> group of the GUIfile. During program execution the
   * programmer can call stateChanged() to set actions to a defined state.
   *
   * @param newstate Name of a State in the GUIfile.
   * @param reverse If the flag reverse is set to StateReverse, the State is reversed.
   * (actions to be enabled will be disabled and action to be disabled will be enabled)
   * Default is reverse=false.
   */
   virtual void stateChanged(const QString &newstate, ReverseStateChange reverse = StateNoReverse);

protected slots:
  void connectHost();
  virtual void showStatusBar();
  void editToolbars();
  /** Appelé lors l'appel de l'à propos. */
  virtual void showAboutApplication();

protected:
  QHash<QString, QMenu*>  mMenuBar;
  QHash<QString, QToolBar*>  mToolBar;

  CYAction   *mStatusBarTog; // TODO à supprimer ?
  CYStatusBarLabel *mStatusLabel;
  QHash<QString, QString*>  mActionDisable;
  QStringList mActionsNoEdit;
  QStringList mActionsDesigner;
  bool mFisrtShow;
  bool mEnableEventsPanel;
  bool mUnique;
  CYActionCollection *mActionCollection;

  int mIndex;
  QString mLabel;

  QMenu *mFileMenu;
  QMenu *mEditMenu;
  QMenu *mViewMenu;
  QMenu *mSettingMenu;

  QStringList mRecentFiles;

  Qt::ToolButtonStyle mToolButtonStyle;

  // Actions to enable/disable on a state change
  QMap<QString,StateChange> m_actionsStateMap;
};

#endif
