/***************************************************************************
                          cy.h  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CY_H
#define CY_H

// ANSI
#include <stdlib.h>
// QT
#include <QtGlobal>
#include <QMessageBox>
#include <QString>
// CY
#include "cytypes.h"
#include "scmdb_t.h"

#if defined(Q_OS_WIN)
  #include <winsock2.h>
  #include <sys/timeb.h>
  #include <process.h>
#elif defined(Q_OS_LINUX)
  #include <sys/socket.h>
  #include <sys/resource.h>
  #include <netinet/in.h>
  #include <netinet/ip.h>
  #include <netinet/tcp.h>
  #include <arpa/inet.h>
  #include <netdb.h>
#endif

/** CY regroupe plusieurs groupes de définitions propres à CY.
  * @author Gérald LE CLEACH
  */


/** Marquage de texte en français.. */
#define TRFR QString

// BASE TEMPORELLES
/** Heures. */
#define BASE_HOUR   3600000.0
/** Minutes. */
#define BASE_MIN      60000.0
/** Secondes. */
#define BASE_SEC       1000.0
/** Millisecondes. */
#define BASE_MSEC         1.0

// EVENEMENTS CYLIBS
/** Suppression d'une cellule d'analyse CYAnalyseCell. */
#define RemoveAnalyseCell   2000

//   TRAITEMENT ET AFFICHAGE DES EVENEMENTS
#define BST    M00  // =>  Message bistable (Acquittements dans journal)
#define SCR    M01  // =>  Affichage à l'écran (Implicite pour process)
#define REC    M02  // =>  Enregistrement dans le journal d'évènements
#define RAZ    M03  // =>  Remise à zéro dès prise en compte par evnt

#define SI_0   false    // actif si à 0
#define SI_1   true     // actif si à 1


/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro.
  * Cette macro ne doit être que temporaire. Si elle doit rester dans les sources il vaut mieux utiliser CYMESSAGE. */
void CYDebug(QString file, int line);
#define CYDEBUG CYDebug(__FILE__, __LINE__);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi qu'un text explicatif.
  * Cette macro ne doit être que temporaire. Si elle doit rester dans les sources il vaut mieux utiliser CYMESSAGETEXT. */
void CYDebugText(QString file, int line, QString text);
#define CYDEBUGTEXT(text) CYDebugText(__FILE__, __LINE__, text);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro. */
void CYDebugBox(QString file, int line);
#define CYDEBUGBOX void CYDebugBox(__FILE__, __LINE__);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi qu'un text explicatif. */
void CYDebugBoxText(QString file, int line, QString text);
#define CYDEBUGBOXTEXT(text) CYDebugBoxText(__FILE__, __LINE__, text);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet qui a généré cette erreur. */
void CYDebugBoxObject(QString file, int line, QString object);
#define CYDEBUGBOXOBJECT(object) CYDebugBoxObject(__FILE__, __LINE__, object);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur. */
void CYDebugBoxData(QString file, int line, QString data);
#define CYDEBUGBOXDATA(data) CYDebugBoxData(__FILE__, __LINE__, data);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur. */
void CYDebugBoxObjectData(QString file, int line, QString object, QString data);
#define CYDEBUGBOXOBJECTDATA(object, data) CYDebugBoxObjectData(__FILE__, __LINE__, object, data);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro. */
void CYMessage(QString file, int line);
#define CYMESSAGE CYMessage(__FILE__, __LINE__);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
	* ainsi qu'un text explicatif. */
void CYMessageText(QString file, int line, QString text);
#define CYMESSAGETEXT(text) CYMessageText(__FILE__, __LINE__, text);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro.
  * Cette macro ne doit pas être temporaire. */
void CYWarning(QString file, int line);
#define CYWARNING CYWarning(__FILE__, __LINE__);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi qu'un text explicatif.
  * Cette macro ne doit pas être temporaire. */
void CYWarningText(QString file, int line, QString text);
#define CYWARNINGTEXT(text) CYWarningText(__FILE__, __LINE__, text);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet qui a généré cette erreur.
  * Cette macro ne doit pas être temporaire. */
void CYWarningObject(QString file, int line, QString object);
#define CYWARNINGOBJECT(object) CYWarningObject(__FILE__, __LINE__, object);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur.
  * Cette macro ne doit pas être temporaire. */
void CYWarningData(QString file, int line, QString data);
#define CYWARNINGDATA(data) CYWarningData(__FILE__, __LINE__, data);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi qu'une liste de causes qui a généré cette erreur.
  * Cette macro ne doit pas être temporaire. */
void CYWarningList(QString file, int line, QStringList list, QString fileName);
#define CYWARNINGLIST(list, fileName) CYWarningList(__FILE__, __LINE__, list, fileName);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur.
  * Cette macro ne doit pas être temporaire. */
void CYWarningObjectData(QString file, int line, QString object, QString data);
#define CYWARNINGOBJECTDATA(object, data) CYWarningObjectData(__FILE__, __LINE__, QString object, QString data);


/** Lance un QMessageBox::critical contenant le nom du fichier et le numéro de ligne où est placée cette macro. */
void CYFatal(QString file, int line);
#define CYFATAL CYFatal(__FILE__, __LINE__);

/** Affiche en console un message contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi qu'un text explicatif.
  * Cette macro ne doit être que temporaire. Si elle doit rester dans les sources il vaut mieux utiliser CYFATALOBJECT. */
void CYFatalText(QString file, int line, QString text);
#define CYFATALTEXT(text) CYFatalText(__FILE__, __LINE__, text);

/** Lance un QMessageBox::critical contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet qui a généré cette erreur. */
void CYFatalObject(QString file, int line, QString object);
#define CYFATALOBJECT(object) CYFatalObject(__FILE__, __LINE__, object);

/** Lance un QMessageBox::critical contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet qui a généré cette erreur. */
void CYFatalData(QString file, int line, QString data);
#define CYFATALDATA(data) CYFatalData(__FILE__, __LINE__, data);

/** Lance un QMessageBox::critical contenant le nom du fichier et le numéro de ligne où est placée cette macro,
  * ainsi que le nom de l'objet et de la donnée qui a généré cette erreur. */
void CYFatalObjectData(QString file, int line, QString object, QString data);
#define CYFATALOBJECTDATA(object, data) CYFatalObjectData(__FILE__, __LINE__, object, data);

class Cy
{
public:
  /** Types de valeur d'une donnée CY. */
  enum ValueType
  {
    /** Non défini. */
    Undef = 0,
    /** Entier signé de 8 bits. */
    VS8,
    /** Entier signé de 16 bits. */
    VS16,
    /** Entier signé de 32 bits. */
    VS32,
    /** Entier signé de 64 bits. */
    VS64,
    /** Entier entier non-signé de 8 bits. */
    VU8,
    /** Entier non-signé de 16 bits. */
    VU16,
    /** Entier non-signé de 32 bits. */
    VU32,
    /** Entier non-signé de 64 bits. */
    VU64,
    /** Flottant de 32 bits. */
    VF32,
    /** Flottant de 64 bits. */
    VF64,
    /** Chaîne de caractères. */
    String,
    /** Temps représenté par un u32. */
    Time,
    /** Temps représenté par un f32 en secondes. */
    Sec,
    /** Mot de 16 bits. */
    Word,
    /** Booléen. */
    Bool,
    /** Couleur. */
    Color,
    /** Entier signé standard utilisé en flag (borné 0/1) */
    VFL,
  };

  /** Types de conteneur possibles. */
  enum TemplateType
  {
    /** Boîte de dialogue. */
    Dialog,
    /** Widget. */
    Widget,
    /** Frame. */
    Frame,
    /** Groupe de bouttons. */
    ButtonGroup,
  };

  /** Modes d'utilisation d'une donnée CY. */
  enum Mode
  {
    /** Non visible pour l'utilisateur final. */
    Sys = 0,
    /** Visible pour l'utilisateur final. */
    User,
    /** Visible uniquement pour le constructeur. */
    Designer,
    /** Pour les E/S non câblées. */
    Free,
    /** Pour les commandes et les groupes de commandes.
      * @see CYCommand, CYWCommand. */
    Bistable,
    /** Pour les commandes et les groupes de commandes.
      * @see CYCommand, CYWCommand. */
    Monostable,
    /** Les commandes peuvent avoir un évènement associé.
      * @see CYCommand. */
    Event,
  };

  /** Format d'affichage temporel. */
  enum TimeFormat
  {
    /** Temps sans format défini. */
    TIME_NO_FORMAT,
    /** Temps en heures (ex : 5 hour). */
    HOUR,
    /** Temps en minutes (ex : 6 min). */
    MIN,
    /** Temps en secondes (ex : 4 sec). */
    SEC,
    /** Temps en millisecondes (ex : 81 msec). */
    MSEC,
    /** Temps en heures, minutes (ex : 5h06m). */
    HOUR_MIN,
    /** Temps en heures, minutes, secondes (ex : 5h06m04s). */
    HOUR_MIN_SEC,
    /** Temps en minutes et secondes (ex : 6m04s). */
    MIN_SEC,
    /** Temps en secondes et millisecondes sans 0 devant (ex : 4s081). */
    SEC_MSEC,
    /** Temps en heures, minutes, secondes et millisecondes (ex : 5h06m04s081). */
    HOUR_MIN_SEC_MSEC,
    /** Temps en minutes secondes et millisecondes (ex : 6m04s081). */
    MIN_SEC_MSEC,
  };

  /** Base numérique en cas de donnée entière positive */
  enum NumBase
  {
    /** Base numérique définie par la donnée.  */
    BaseData = 0,
    /** A base 2.  */
    Binary = 2,
    /** A base 10.  */
    Decimal = 10,
    /** A base 16.  */
    Hexadecimal = 16,
    /** A base 8.  */
    Octal = 8,
  };

  /** Section de groupe de données. */
  enum GroupSection
  {
    /** Pas de demande relative au groupe. */
    No,
    /** Demande relative au dernier sous-groupe.. */
    lastGroup,
    /** Demande relative aux sous-groupes. */
    underGroup,
    /** Demande relative au groupe entier. */
    wholeGroup
  };

  /** Énumération des types d'incertitude possibles pour les mesures @see CYMeasure */
  enum Uncertain
  {
     /** Incertitude maxi de l'étendue de mesure en %. */
    UEM = 0,
    /** Incertitude en unité capteur. */
    UUC = 1,
    /** Incertitude en % par point. */
    UPC = 2,
  };

  /** Énumération des types de détection de changement d'état ou de valeur. */
  enum Change{ None=0, Rising=1, Falling=2, Changing=3 };
};

/** @return le chemin complet du fichier.
@param file 	Chemin du fichier pouvant être complet ou relatif.
		Dans le premier cas celui-ci commence par un '/'.
		S'il ne commence ni par un '/' ni par '.' il est considéré
		comme un fichier système et est alors recherché comme tel.
*/
QString findResource(QString file, QString type="data");

/** @return le chemin saisi unicode ou ASCII en interprétant dans ce cas les possibles encodages %.*/
QUrl UrlFromUserInput(const QString& input);

/** Copie un répertoire. */
bool copyPath(QString sourceDir, QString destinationDir, bool overWriteDirectory);

/** Copie les fichiers d'un répertoire dans au autre. */
bool copyDirectoryFiles(const QString &fromDir, const QString &toDir, bool coverFileIfExist);

/** Suppression d'un répertoire avec son contenu */
bool removeDir(QString dirPath);

/**
 * @brief Compare 2 versions
 * @param version1  Version 1
 * @param version2  Version 2
 * @return
 *  - \a 1 si \a version1 > \a version2
 *  - \a 0 si \a version1 = \a version2
 *  - \a -1 si \a version1 < \a version2
 */
int compareVersion(QString version1, QString version2);

//======================================
// GESTION DE TUBES INTER-PROCESSUS
//======================================

int open_tube(dcltb_t *ptb);
int close_tube(dcltb_t *ptb);
int read_tube(dcltb_t *ptube, void *data, size_t size);
int write_tube(dcltb_t *ptube, void *data, size_t size);

#endif
