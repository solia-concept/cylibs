/***************************************************************************
                          cymaths.cpp  -  description
                             -------------------
    début                  : mar aoû 12 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cymaths.h"
#include "cy.h"

/** @return \a true si \a k est un nombre premier. */
bool cyIsPrime(int k)
{
  int i;

  for (i=2;i<k;i++)
    if (!(k%i))
      return false;
  return true;
}

/** @return le plus petit nombre premier >= n .*/
int cyMinPrime(int n)
{
  if (n<2)
    return -1;

  int k;

  for (k=n;k<INT_MAX;k++)
    if (cyIsPrime(k))
      return k;
  return -1;
}

double cyExpFromString(QString txt, double &mantissa, int &exp)
{
  int e;

  e = txt.indexOf("e");
  if ((e==-1))
    return txt.toDouble();

  QString tmp;
  tmp = txt.left(e);
  mantissa = tmp.toDouble();
  tmp = txt.remove(tmp+"e");
  exp = tmp.toInt();

  tmp = QString("%1e%2").arg(mantissa).arg(exp);

  double val = tmp.toDouble();

  return val;
}

QString cyExpToString(double val, double &mantissa, int &exp, int nbDec)
{
  QString txt="";

  if (nbDec>=0)
    txt = QString("%1").arg(val, 0, 'e', nbDec);
  else
    txt = QString("%1").arg(val, 0, 'e');

  int e = txt.indexOf("e");
  if (e==-1)
    return txt;

  QString tmp="";
  tmp = txt.left(e);
  mantissa = tmp.toDouble();
  tmp = txt.remove(tmp+"e");
  exp = tmp.toInt();

  return txt;
}

double cyMantissa(double val)
{
  double mantissa=0.0;
  int exp;
  cyExpToString(val, mantissa, exp);
  return mantissa;
}

/** @return l'exposant de \a val. */
int cyExponent(double val)
{
  double mantissa;
  int exp=0;
  cyExpToString(val, mantissa, exp);
  return exp;
}

// double cyRound(double val, double precision)
// {
//   double i_part, mantisse;
// 
//   if (precision==0.0)
//     return val;
// 
//   mantisse = modf((double) val/precision, &i_part);
//   if(mantisse>0.5)
//     i_part+=1.0;
//   if(mantisse<-0.5)
//     i_part-=1.0;
//   return (i_part*precision);
// }
// 
// double cyRoundSup(double val, double precision)
// {
//   double i_part, mantisse;
// 
//   if (precision==0.0)
//     return val;
// 
//   mantisse = modf((double) val/precision, &i_part);
//   if(mantisse>0.5)
//     i_part+=1.0;
//   return (i_part*precision);
// }
// 
// double cyRoundInf(double val, double precision)
// {
//   double i_part, mantisse;
// 
//   if (precision==0.0)
//     return val;
// 
//   mantisse = modf((double) val/precision, &i_part);
//   if(mantisse<-0.5)
//     i_part-=1.0;
//   return (i_part*precision);
// }
// 
