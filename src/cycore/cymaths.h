/***************************************************************************
                          cymaths.h  -  description
                             -------------------
    début                  : mar aoû 12 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYMATHS_H
#define CYMATHS_H

// ANSI
#include <limits.h>
#include <math.h>
// QT
#include <qapplication.h>

/** @return \a true si \a k est un nombre premier. */
bool cyIsPrime(int k);

/** @return le plus petit nombre premier >= n .*/
int cyMinPrime(int n);

/** @return la mantisse de \a val. */
double cyMantissa(double val);

/** @return l'exposant de \a val. */
int cyExponent(double val);

/** @return la valeur exponentielle à partir de la chaîne de caractères \a txt. */
double cyExpFromString(QString txt, double &mantissa, int &exp);

/** @return la chaîne de caractères de la valeur exponentielles \a val. */
QString cyExpToString(double val, double &mantissa, int &exp, int nbDec=-1);

/** @return la valeur arrondie de \a val suivant la précision \a precision. */
template<class T>
T cyRound(T val, double precision);

/** @return la valeur arrondie supérieure de \a val suivant la précision \a precision. */
template<class T>
T cyRoundSup(T val, double precision);

/** @return la valeur arrondie inférieure de \a val suivant la précision \a precision. */
template<class T>
T cyRoundInf(T val, double precision);

template<class T>
inline T cyRound(T val, double precision)
{
  double i_part, mantisse;

  if (precision==0.0)
    return val;

  mantisse = modf((double) val/precision, &i_part);
  if(mantisse>0.5)
    i_part+=1.0;
  if(mantisse<-0.5)
    i_part-=1.0;
  return ((T)(i_part*precision));
}

template<class T>
inline T cyRoundSup(T val, double precision)
{
  double i_part, mantisse;

  if (precision==0.0)
    return val;

  mantisse = modf((double) val/precision, &i_part);
  if(mantisse>0.5)
    i_part+=1.0;
  return ((T)(i_part*precision));
}

template<class T>
inline T cyRoundInf(T val, double precision)
{
  double i_part, mantisse;

  if (precision==0.0)
    return val;

  mantisse = modf((double) val/precision, &i_part);
  if(mantisse<-0.5)
    i_part-=1.0;
  return ((T)(i_part*precision));
}

/**
 * @brief Variation de valeur suivant un certain pourcentage.
 * @param val   Valeur courante
 * @param coef  Coefficient en % d'augmentation si > 0 ou de diminution si < 0
 * @return la nouvelle valeur.
 */
template<class T>
inline T cyVariation(T val, double limit)
{
  if (val!=0.0)
      return ((T)(val*(1+limit/100.0)));
  else
      return((T)0.5);
}

#endif
