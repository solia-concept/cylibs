/***************************************************************************
                          cyprofibusitem.h  -  description
                             -------------------
    begin                : mer nov 17 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYPROFIBUSITEM_H
#define CYPROFIBUSITEM_H

// QT
#include <qobject.h>
// CYLIBS
#include "cytypes.h"


/** @short Elément profibus.
  * @author LE CLÉACH Gérald
  */

class CYProfibusItem : public QObject
{
  Q_OBJECT
public:
  CYProfibusItem(QObject *parent, e_pbtype type, e_pbtype_adr adr_type, int rn, int adress, void *ptr, int byte, int bit, int index);
  ~CYProfibusItem();

  /** @return le type d'élément. */
  e_pbtype type() { return mType; }
  /** @return le type d'adressage. */
  e_pbtype_adr adrType() { return mAdrType; }
  /** @return le numéro du régulateur. */
  int RN() { return mRN; }
  /** @return l'adresse profibus. */
  int adress() { return mAdress; }
  /** @return le pointeur. */
  void *ptr() { return mPtr; }
  /** @return le  numéro d'octet. */
  int byte() { return mByte; }
  /** @return le  numéro de bit. */
  int bit() { return mBit; }
  /** @return L'index par rapport au numéro d'appareil et type d'élément. */
  int index() { return mIndex; }

private: // Private attributes
  /** Type d'élément. */
  e_pbtype mType;
  /** Type d'adressage. */
  e_pbtype_adr mAdrType;
  /** Numéro du régulateur. */
  int mRN;
  /** Adresse. */
  int mAdress;
  /** Ponteur. */
  void *mPtr;
  /** Numéro d'octet. */
  int mByte;
  /** Numéro de bit. */
  int mBit;
  /** Index par rapport au numéro d'appareil et type d'élément. */
  int mIndex;
};

#endif
