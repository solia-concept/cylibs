/***************************************************************************
                          cyfilecsv.cpp  -  description
                             -------------------
    début                  : jeu aoû 21 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyfilecsv.h"

// QT
#include <qfile.h>
#include <QTextStream>
// CYLIBS
#include "cycore.h"
#include "cydb.h"
#include "cydblibs.h"
#include "cyeventreport.h"
#include "cystring.h"
#include "cys32.h"
#include "cyf64.h"

CYFileCSV::CYFileCSV(QObject *parent, const QString &name, QString label, QString path, bool fileNum, int maxSize, int maxLines, bool useNewFile)
  : QObject(parent),
    mLabel(label),
    mPath(path)
{
	setObjectName(name);
	mFileNumEnable 	= fileNum;

  mDB = core->cydblibs();

  mDB->setUnderGroup(mLabel);

  maxLinesFile = new CYS32(mDB, QString("%1_MAX_LINES_FILE").arg(name), new s32, maxLines, 1, 100000);
  maxLinesFile->setLabel(tr("Maximum of lines per file"));

  maxFileSize = new CYF64(mDB, QString("%1_MAX_FILE_SIZE").arg(name), new f64, maxSize, 1.0, 2000000000.0);
  maxFileSize->setLabel(tr("Maximum file size"));

  mDB->load();

  mFileNum = 1;
	mFileName = "";
  mUseNewFile = useNewFile;
  if (!maxLinesFile->val() && !maxFileSize->val())
  	CYFATALTEXT(QString("Il faut au moins activer une limite soit en nombre de lignes soit en taille !"));

  mDir = new QDir(path, "*.csv");
  setPath(path);
}

//CYFileCSV::~CYFileCSV()
//{
//}

QString CYFileCSV::path()
{
  return mPath;
}

void CYFileCSV::setPath(const QString &path)
{
  mPath = path;
  mDir->setPath(path);
  updateDir();
}

void CYFileCSV::updateDir()
{
  if (!mDir->exists())
    mDir->mkdir(mPath);
}

QFile *CYFileCSV::newFile(QString filename)
{
  mUseNewFile = false;
  mFileName = filename;
  QFile *file = new QFile(mFileName);
  core->mkdir(QFileInfo(mFileName).absolutePath());
  if (file->open(QIODevice::ReadWrite | QIODevice::Append))
  {
    QTextStream stream(file);
    stream << QString("%1:%2\n").arg(core->customerName()).arg(core->designerRef());
    for ( QStringList::Iterator it = mHeaderLines.begin(); it != mHeaderLines.end(); ++it )
    {
    	lines.append(*it);
      stream << *it;
    }
    file->close();
    core->backupFile(mFileName);
    return file;
  }
  else
    return 0;
}

QFile *CYFileCSV::findFile(bool nf,int newlines, int newSize)
{
	QString txt;

	if (!mFileNumEnable)
	{
		QFileInfo fi(mFileName);
		if (nf || !fi.exists())
		{
      mFileName = QString("%1/%2%3.csv").arg(mDir->path()).arg(mPrefix).arg(QDateTime::currentDateTime().toString("yyMMdd_hhmmss"));
      mFileName.replace("//","/");
		  // Creation d'un nouveau fichier
		  return (newFile(mFileName));
		}
  	if (maxLinesFile->val()>0)
  	{
    	long nbLines = 0;
    	QFile file(mFileName);
      if (file.exists() && file.open(QIODevice::ReadOnly))
      {
        QTextStream stream(&file);
        while (!stream.atEnd())
        {
          txt = stream.readLine();
      	  nbLines++;
      	  if ((nbLines+newlines)>maxLinesFile->val())
      	  {
      	  	nf=true;
      	  	break;
      	  }
      	}
      }
      else
      	nf=true;

      if (txt.startsWith("FIN") || txt.startsWith("END")) // fin de fichier forcé par un des mots clés relatifs
      	nf=true;
  	}
  	else if ((fi.size()+newSize) > maxFileSize->val())
  	{
  		nf=true;
  	}

  	if (nf)
  	{
			mFileName = QString("%1/%2%3.csv").arg(mDir->path()).arg(mPrefix).arg(QDateTime::currentDateTime().toString("yyMMdd_hhmmss"));
		  // Creation d'un nouveau fichier
		  return (newFile(mFileName));
  	}
  	else
      return (new QFile(mFileName));
	}
	else
	{
		bool max=false;
		int fileNum = mFileNum;
    mFileName = QString("%1/%2%3.csv").arg(mDir->path()).arg(mPrefix).arg(fileNum);

    QFileInfo fi(mFileName);

    while (mDir->exists(mFileName))
	  {
	    QFile file(mFileName);
	  	if (maxLinesFile->val()>0)
	  	{
	    	long nbLines = 0;
	    	QFile file(mFileName);
	      QTextStream stream(&file);
				while (!stream.atEnd())
	      {
	        stream.readLine();
	    	  nbLines++;
	    	  if ((nbLines+newlines)>maxLinesFile->val())
	    	  {
	    	  	max=true;
	    	  	break;
	    	  }
	    	}
	  	}
	  	else if ((fi.size()+newSize) > maxFileSize->val())
	  		max=true;

	    if (!nf && fi.exists() && !max)
	    {
	      QFileInfo fi2(QString("%1/%2%3.csv").arg(mDir->path()).arg(mPrefix).arg(fileNum+1));
	      if (!fi2.exists())
	      {
	        mFileNum = fileNum;
	        return (new QFile(mFileName));
	      }
	    }
	    fileNum++;
	    mFileName = QString("%1/%2%3.csv").arg(mDir->path()).arg(mPrefix).arg(fileNum);
	  }
	  mFileNum = fileNum;
	  // Creation d'un nouveau fichier
	  return (newFile(mFileName));
	}
}

void CYFileCSV::addLine(QString line)
{
  if (line.isEmpty())
    return;

//  lines.append(line);
//  lines.append("\n");

  QFile *file = findFile( mUseNewFile, 1 );

  if (file && (file->open(QIODevice::ReadWrite | QIODevice::Append)))
  {
    QTextStream stream(file);
    stream << line;
    stream << "\n";
    file->close();
    core->backupFile(mFileName);
    return;
  }
  else
    CYWARNING;
}


void CYFileCSV::addLines(QStringList newLines)
{
  if (newLines.isEmpty())
    return;

  QFile *file = findFile( mUseNewFile, newLines.count() );
  if (file && (file->open(QIODevice::ReadWrite | QIODevice::Append)))
  {
    QTextStream stream(file);
    for ( QStringList::Iterator it = newLines.begin(); it != newLines.end(); ++it )
    {
    	lines.append(*it);
      stream << *it;
    }
    file->close();
    core->backupFile(mFileName);
    return;
  }
  else
    CYWARNING;
}

void CYFileCSV::load(QString filename)
{
  mFileName = filename;
  load();
}

void CYFileCSV::load()
{
  QFile *file = new QFile(mFileName);
  if (!file->open(QIODevice::ReadOnly))
  {
    CYMESSAGETEXT(mFileName);
    return;
  }
  QTextStream stream(file);
  QString line;
  lines.clear();
  while (!stream.atEnd())
  {
    line = stream.readLine();
    lines.append(line);
  }
}

QString CYFileCSV::loadTimeToString()
{
  QFileInfo fi = QFileInfo(mFileName);
  return fi.created().toString(Qt::ISODate).replace(QChar('T'), " / " );
}
