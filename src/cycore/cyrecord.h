/***************************************************************************
                          cyrecord.h  -  description
                             -------------------
    début                  : mer avr 30 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYRECORD_H
#define CYRECORD_H

// QT
#include <qobject.h>

/** @short Enregistrement binaire.
  * @author Gérald LE CLEACH
  */

class CYRecord : public QObject
{
public: 
  CYRecord(void *ptr, int size, const QString &filename, QObject *parent=0, const QString &name=0, int nb=1);
  ~CYRecord();

  /** @return \a true si le fichier a déjà été chargé en mémoire. */
  bool isLoaded();

public slots: // Public slots
  /** Charge l'enregistrement dans la zone mémoire concernée. */
  void load();
  /** Enregistre une zone mémoire dans son fichier de sauvegarde. */
  void save();

private: // Private methods
  /** @return le checksum du fichier binaire. */
  unsigned int fileCheck();

  /** Création d'un fichier. */
  void createFile(int copy);
  /** Lecture du fichier. */
  char readFile(int copy);

private: // Private attributes
  /** Pointeur sur la zone mémoire à enregistrer. */
  void *mPtr;

  /** Nom du fichier. */
  QString mFileName;
  /** Nom du fichier . */
  QString mFileNameBack;
  /** Taille du fichier. */
  int mFileSize;
  /** Vaut \a true si le fichier a déjà été chargé en mémoire. */
  bool mLoaded;
};

#endif
