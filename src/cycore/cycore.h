/***************************************************************************
 cycore.h  -  description
 -------------------
 début                  : mer mar 19 2003
 copyright              : (fr) 2003 par Gérald LE CLEACH
 email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

 Ce fichier fait partie des bibliothèques
 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

class CYCore;

#ifndef CYCORE_H
#define CYCORE_H

// ANSI
#include <stdlib.h>
// QT
#include <QWidget>
#include <QTimer>
#include <QHash>
#include <QList>
#include <QIcon>
#include <QUrl>
#include <QMenu>
#include <QDir>
#include <QProcess>
// CYLIBS
#include "cytypes.h"
#include "cydb.h"
#include "cyprintableinfo.h"
#include "cyeventsgeneratorspv.h"
#include "cymeasure.h"

class CYUser;
class CYUserGroup;
class CYFormat;
class CYData;
class CYNetModule;
class CYNetHost;
class CYNetLink;
class CYNetConnect;
class CYEventsManager;
class CYDisplayStyle;
class CYProject;
class CYAcquisition;
class CYAcquisitionWin;
class CYAcquisitionFast;
class CYAcquisitionSlow;
class CYMetroWin;
class CYSplashScreen;
class CYProfibusItem;
class CYMV;
class CYCT;
class CYProjectEditor;
class CYDBLibs;
class CYWin;
class CYMainWin;
class CYAcquisitionBackupFlag;
class CYMeasure;
class CYString;
class CYEvent;
class CYEventsGenerator;
class CYEventsPanel;
class CYFileCSV;
class QSettings;
class CYReadOnlyFilter;

/** CYCore gère toutes les intéractions avec le ou les hôtes connecté(s).
 * Un hôte peut être un banc d'essai, un régulateur du banc ou un système externe.
 * Il faut utiliser engage() pour établir une communication et disengage() pour
 * terminer la communication avec un hôte. Si on ne sait pas si un certain hôte
 * est déjà connecté il suffit d'utiliser engageHost(). S'il n'y a pas encore
 * de communication ou que le champ hostname est vide, une boîte de dialogue
 * sera présentée pour entrer les détails des connexions.
 * @short Noyau de l'application.
 * @author Gérald LE CLEACH
 */

class CYCore : public QObject
{
  Q_OBJECT

public:
  // Public methods
  /** Construit le noyau de l'application.
   * @param m Module de communication.
   */
  CYCore(QObject *parent = 0, const QString &name = 0);
  ~CYCore();

  /** return la version de Cylibs en chaîne de caractères divisée en 2 ou 3 parties sépérarées par un point:
      1er le numéro d'évolution majeur,
      2eme le numéro de l'évolution mineur,
      3eme partie le numéro dans le cas de patch ou correctif de développement. */
  QString version();

  /** Initialise le noyau de l'application.
   * C'est dans cette fonction que sont créés les différents
   * hôtes, bases de données et connexions réseau. */
  virtual void init();
  /** Initialise les niveaux d'accès. */
  virtual void initAccess();
  /** Initialise les formats d'affichage des données. */
  virtual void initFormat() {}
  /** Initialise le gestionnaire d'événements */
  virtual void initEvents();

  /** @return le type du banc d'essai. */
  virtual QByteArray benchType() const;
  /** Saisie le type du banc d'essai. */
  virtual void setBenchType(const QByteArray &name);
  /** @return \a true si une option parmie les\a options listées et séparées par un '_'
   * est définie dans le type du banc d'essai. */
  virtual bool isBenchType(QString option);

  /** @return l'hôte local, autrement dit le superviseur. */
  CYNetHost * localHost();

  /** @return \a true si la connexion réseau avec le(s) RN est autorisé. */
  bool networkEnable() { return  mNetworkEnable; }

  /** @return le nom du client. */
  QString customerName() { return mCustomerName; }
  /** Saisie le nom du client. */
  void setCustomerName(QString name) { mCustomerName = name; }
  /** @return le logo du client. */
  virtual QPixmap customerLogo() { return QPixmap(); }

  /** @return la réference constructeur. */
  QString designerRef() { return mDesignerRef; }
  /** Saisie la réference Cylix. */
  void setDesignerRef(QString ref) { mDesignerRef = ref; }

  /** @return le logo constructeur utilisé par exemple en pied page des impressions courbe. */
  virtual QPixmap designerLogo();

  /** @return la réference client du banc. */
  virtual QString customerRef() { return mCustomerRef; }
  /** Saisie la réference client du banc. */
  virtual void setCustomerRef(QString ref)
  {
    mCustomerRef = ref;
    emit newCustomerRef (ref);
  }

  /** @return le message d'en-tête à imprimer. */
  virtual QString printingHeader() { return mPrintingHeader; }
  /** Saisie le message d'en-tête à imprimer. */
  virtual void setPrintingHeader(QString msg) { mPrintingHeader = msg; }

  /** @return le message qui contient le type d'essai en cours. */
  virtual QString testType() { return mTestType; }
  /** Saisie le message qui contient le type d'essai en cours. */
  virtual void setTestType(QString msg) { mTestType = msg; }

  /** @return le nombre de projet(s) d'essai simultané(s). */
  virtual int projectCount() { return 1; }

  /** Fonction à enrichir dans Cylix en cas de projets d'essai simultannés
   *  pour proposer par un choix de numéro de projet, de poste...
   * @return l'index du projet d'essai sélectionné. */
  virtual int selectProject() { return 0; }

  /** @return le projet d'essai en cours.
      * @param index  Index de projet (0 si un seul project actif à la fois). */
  virtual CYProject * project(int index=0);
  /** Saisie le projet d'essai en cours.
   * param tmp     Chargement temporaire (inutile de garder une trace de ce chargement).
   * @param index  Index de projet (0 si un seul project actif à la fois).*/
  virtual void setProject(CYProject *project, bool tmp = false, int index =0) = 0;
  /** @return le fichier du projet en cours.
    * @param index  Index de projet (0 si un seul project actif à la fois).*/
  QString projectFile(int index=0) { return mProjectFile[index]; }
  /** Charge le projet ayant pour nom de fichier \a filename
   * @param filename nom du fichier projet.
   * @param tmp      temporaire (pas de nouveau répertoire).
   * @param index    Index de projet (0 si un seul project actif à la fois). */
  virtual CYProject *loadProject(const QString &filename, bool tmp = false, int index=0) = 0;

  /** Lecture des valeurs actuelles et passées d'une structure.
   * La valeur retournée est l'indice du dernier échantillon lu. Un échantillon
   * représente les valeurs de la structure à un temps donné.
   * @param str    Index de la structure dans le tableau des devices.
   * @param index  Indice du premier échantillon à lire.
   * @param com    Numéro de com. */
  int readData(int str, u32 index = 0, int com = 0);
  /** Ecriture d'une structure de données.
   * @param str    Index de la structure dans le tableau des devices.
   * @param index  Indice du premier échantillon à écrire. */
  bool writeData(int str, u32 index = 0);

  /** Ajoute une donnée à la base de données locale et globale. */
  void addToDB(CYData *data);
  /** @return la donnée ayant pour nom \a name, ou 0 si la recheche échoue.
   @param fatal  Mettre à \a false pour ne pas quitter l'application.
   @param notify Mettre à \a false pour ne pas informer l'utilisateur que la donnée n'existe pas. */
  CYData *findData(const QString &name, bool fatal = true, bool notify = true);
  /** @return la donnée de la base de donnée \a localDB ayant pour nom \a name, ou 0 si la recheche échoue.
   @param fatal Mettre à \a false pour ne pas quitter l'application.
   @param notify Mettre à \a false pour ne pas informer l'utilisateur que la donnée n'existe pas. */
  CYData * findData(CYDB *localDB, const QString &name, bool fatal = true, bool notify = true);

  /** Ajoute un nouvel hôte à la base de données globale.
   * @param host  Hôte. */
  void addHost(CYNetHost *host = 0);
  /** @return l'hôte ayant pour index \a id. */
  CYNetHost * host(int id);
  /** Ajoute une nouvelle connexion.
   * @param link  Connexion. */
  void addLink(CYNetLink *link = 0);
  /** @return la connexion locale. */
  CYNetLink * localLink();

  /** Etablie une communication avec un hôte donné.
   * @param hostName  Nom de l'hôte. */
  bool engageHost(const QString &hostName);
  /** Commande d'établisssement d'une communication avec l'hôte donné.
   * @param hostName    Nom de l'hôte.
   * @param connection  Connexion sur laquelle il faut établir la communication.
   *                    Si 0 établit la communication sur toutes
   *                    les connexions disponibles de l'hôte. */
  bool engage(const QString &hostName = 0);

  bool disengage(const CYNetHost *host);
  /** Termine la communication avec un hôte donné.
   * @param hostname  Nom de l'hôte. */
  bool disengage(const QString &hostname);
  /** Perte d'un hôte donné.
   * @param host  Hôte perdu. */
  void hostLost(const CYNetHost *host);
  /** Cette fonction retransmet des messages texte aux widgetx principaux qui
   * affiche ceux-ci dans une boîte pop-up. Ceci doit servir aux objets pour
   * leur permettre d'être supprimés avant que la boîte pop-up soit fermée. */
  void notify(const QString &msg) const;
  /** Rattachement du gestionaire de données à un widget principal. */
  void setBroadcaster(QWidget *bc)
  {
    broadcaster.insert (bc->objectName(), bc);
  }
  /** Getionnaire d'évènement du CYCore. */
  virtual bool event(QEvent *e);

  /** Récupère les paramètres de l'hôte de communication:
   * @param hostname  Nom de l'hôte.
   * @param shell     Shell.
   * @param command   Commande personnalisée.
   * @param port      Numéro de port lors d'une communication avec un démon. */
  bool getHostInfo(const QString &host, QString &shell, QString &command, int &port);
  /** Remplit la base de données. */
  virtual void fill() {}

  /** Charge la configuration de l'application spécifique à l'utilisateur OS en cours. */
  virtual void readOSUserProperties(QSettings *cfg);
  /** Enregistre la configuration de l'application spécifique à l'utilisateur OS en cours. */
  virtual void saveOSUserProperties(QSettings *cfg);

  /** Charge la configuration de l'application commune à tous les utilisateurs de l'OS. */
  virtual void readOSProperties(QSettings *cfg);
  /** Enregistre la configuration de l'application commune à tous les utilisateurs de l'OS. */
  virtual void saveOSProperties(QSettings *cfg);

  /** @return la liste des utilisateurs par titre. */
  QStringList userList();
  /** @return la liste des groupes d'utilisateurs par titre. */
  QStringList userGroupList();

  /** @return la liste d'utilisateurs. */
  QHash<int, CYUser*> &users();
  /** @return la liste de groupe d'utilisateurs. */
  QHash<int, CYUserGroup*> &userGroups();

  /** @return \a true si un projet est chargé. */
  bool projectLoaded()
  {
    return mProjectLoaded;
  }
  /** Saisie \a true pour indiquer qu'un projet est chargé. */
  void setProjectLoaded(bool val)
  {
    mProjectLoaded = val;
  }

  /** @return \a true si le répertoire d'essai est par défaut à créer à chaque lancement d'essai. */
  virtual bool newTestDir() { return mNewTestDir; }
  /** Sasir \a true pour que le répertoire d'essai soit par défaut à créer à chaque lancement d'essai. */
  virtual void setNewTestDir(bool newTestDir) { mNewTestDir = newTestDir; }

  /** @return \a true si le nom du répertoire d'essai est créé automatiquement ou \a false pour une saisie du nom par l'utilisateur. */
  virtual bool testDirAuto() { return mTestDirAuto; }
  /** Saisir \a true pour que le répertoire d'essai soit créé automatiquement, ou \a false pour une saisie du nom par l'utilisateur. */
  virtual void setTestDirAuto(bool testDirAuto) { mTestDirAuto = testDirAuto; }

  /** Saisie et emet le message à afficher dans l'écran de démarrage soft. */
  virtual void setSplashMessage(const QString &msg) { emit splashMessage (msg); }

  virtual void setSplashScreen(CYSplashScreen *splash);

  /** @return l'éditeur de projet. */
  virtual CYProjectEditor *projectEditor() { return mProjectEditor; }
  /** Saisie l'éditeur de projet. */
  virtual void setProjectEditor(CYProjectEditor * editor) { mProjectEditor = editor; }
  /** @return  \a true si les consignes du projet sont protégées. */
  virtual bool projectSetpointsProtected();
  /**  @return la base des données CYLIBS.*/
  CYDBLibs *cydblibs() { return mCYDBLibs; }
  virtual void initDB();
  bool initOk();
  virtual QString baseDirectory();
  bool notifyDatasLoading();
  virtual void initBaseDirectory(QString path = QDir::homePath());
  QHash<QString, CYWin*> &windows();
  virtual void addWindow(CYWin *win);
  virtual void loadUsersData();
  virtual CYMainWin * mainWin();
  virtual void setMainWin(CYMainWin * win);

  void addGroup(QString txt, QWidget *wdg);
  int maxCurvesByScope();
  /** Saisie le nombre max de courbes possibles par oscilloscope. */
  void setMaxCurvesByScope(int val);
  /** @return le nom du fichier d'archive du postMortem généré par le \a flag. Si cette fonction n'est pas réimplémentée dans l'affaire alors le nom du fichier est généré uniquement à partir du préfix associée à ce flag. */
  virtual QString postMortemBackupFileName(CYAcquisitionBackupFlag *flag);

  /** @return l'état de simulation des évènements.
   * En simulation une demande d'acquittement sur 2 est prise en compte et arrête la simulation des évènements. L'autre permet de la relancer. */
  bool simulEvents() { return mSimulEvents; }

  /** @return l'évènement ayant pour nom \a name, ou 0 si la recheche échoue.
   @param fatal  Mettre à \a false pour ne pas quitter l'application.
   @param notify Mettre à \a false pour ne pas informer l'utilisateur que l'évènement n'existe pas. */
  CYEvent *findEvent(const QString &name, bool fatal = true, bool notify = true);
  /** @return le générateur d'évènements ayant pour nom \a name. */
  virtual CYEventsGenerator *eventsGenerator(QString name);
  virtual void setEventsGenerator(CYEvent *event, CYEventsGenerator *generator);
  /** Initialise un évènement. */
  virtual CYEvent *initEvent(CYEventsGenerator *generator, const QString &name, flg *val,
                             QString helpEnd, bool enable, int proc, QString desc, QString from,
                             QString refs, QString help);
  /** Initialise un défaut. */
  virtual CYEvent *initFault(CYEventsGenerator *generator, const QString &name, flg *val,
                             QString helpEnd, bool enable, int proc, QString desc, QString from,
                             QString refs, QString help);
  /** Initialise une alerte. */
  virtual CYEvent *initAlert(CYEventsGenerator *generator, const QString &name, flg *val,
                             QString helpEnd, bool enable, int proc, QString desc, QString from,
                             QString refs, QString help);

  /** @return le pointeur de l'évènement de création de projet.*/
  virtual flg *createProjectEvent(int projectId=0);
  /** @return le pointeur de l'évènement de chargement de projet.*/
  virtual flg *loadProjectEvent(int projectId=0);
  /** @return le pointeur de l'évènement de fermeture de projet.*/
  virtual flg *closeProjectEvent(int projectId=0);
  /** @return le pointeur de l'évènement de sauvegarde de projet.*/
  virtual flg *saveProjectEvent(int projectId=0);

  /** Permet une personnalisation du menu d'aide des fenêtre principale de l'application. */
  virtual void customHelpMenu(QMenu *helpMenu);
  /** Ajoute une donnée dont la valeur constructeur est à vérifier à la fin du démarrage de l'application. */
  virtual void addDataCheckDef(CYData *data);

  /** Saisie l'offset sur la taille des polices par défaut de l'application.  */
  virtual void setOffsetFontSize(int offset) { mOffsetFontSize = offset; }
  /** @return l'offset sur la taille des polices par défaut de l'application.  */
  virtual int offsetFontSize() {  return mOffsetFontSize; }

  /** Saisie le format de date heure par défaut de l'application.  */
  virtual void setDateTimeFormat(QString format) { mDateTimeFormat = format; }
  /** @return le format de date heure par défaut de l'application.  */
  virtual QString dateTimeFormat() {  return mDateTimeFormat; }
  /**
   * @brief docUrl Génération d'URL adapté à l'OS, la langue et l'utilisateur courants.
   * L'utilisateur a accès à la documentation uniquement si l'action d'ouverture du fichier est autorisé par l'administration utilisateurs.
   * @param document    Nom du document.
   * @param bookmark    Signet éventuel dans le PDF.
   * @param extFile     Extension du fichier d'aide.
   * @param protect     Si \a true l'accès est protégé au document par l'action \a actionName.
   * @param actionName  Nom de l'action configurable par l'administration utilisateurs pour permettre son ouverture. En cas de champ vide, le nom de l'action prend automatiquement le nom du document.
   * @return le chemin d'un document avec l'éventuel signet interne en fonction de la langue et de l'utilisateur en cours.
   */
  virtual QUrl docUrl(const QString &document, const QString &bookmark="", QString extFile="pdf", bool protect=true, QString actionName="");
  /**
   * @brief htmlUrlBookmark Création temporaire d'un alias en HTML pour accéder à un URL avec signet.
   * Sous Windows, QUrl::setFragment() ne fonctionnnant pas, bien une alternative est ici faite
   * par la création d'un fichier temporaire HTML contenant une redirection vers la cible interprétée
   * correctement par le navigateur Web de l'OS.
   * Sous Linux, bien que QUrl::setFragment() fonctionne, l'ouverture se faisant avec l'application de l'OS
   * associée au type de document, le signet peut ne pas être prise en compte si l'application utilisée
   * n'est pas capable de gèrer l'ouverture avec référence. De fait, pour éviter ce problème, l'ouverture
   * sous Linux se fait aussi via ce fichier temporaire afin de laisser le navigateur Web de gérer correctement
   * l'ouverture du fichier.
   * @param url       Url à compéteter par le signet.
   * @param bookmark  Signet à ajouter à \a url.
   * @return l'URL du fichier temporaire HTML permettant d'ouvrir le document avec le signet.
   */
  virtual QUrl htmlUrlBookmark(const QUrl &url, const QString &bookmark);
  /**
   * @brief Remplace dans le text \a html les balises HTML par des balises DocBook.
   * @param html  Texte HTML à convertir
   * @param dtd   Compatible ou non DTD (document type definition) par le remplacement ou non de caractères spéciaux par leurs équivalents en entité décimale XML.
   * @return un code compatible DocBook à partir d'un code html.
   */
  static QString html2DocBook(QString html, bool dtd=true);
  /**
   * @brief addDocBookMark Ajoute un marque-page du manuel.
   * @param id    Identifiant dans la documentation.
   * @param label Texte à afficher.
   */
  void addDocBookMark(QString id, QString text);
  /**
   * @brief htmlDocBookMark Code HTML d'un signet d'une documentation.
   * @param id   Identifiant dans la documentation.
   * @param text Texte à afficher. En cas de champ vide, récupère le texte dans le dictionnaire de signets.
   * @return le signet sous forme HTML, soit "<a href='#id'>text</a>"
   */
  virtual QString htmlDocBookMark(QString id, QString text="");
  /**
   * @brief htmlDocBookMarkUrl Mise à jour des signet(s) d'un code HTML suivant l'accès d'une documentation.
   * Si l'accès au document est autorisé à l'utilisateur courant, le(s) signet(s) contenu(s) dans le code HTML se voie(nt) ajouter dans l'URL le chemin d'accès à la documenation relative et suivant la langue en cours. Si l'accès n'est pas autorisé, aucun chemin n'est donné et un message d'échec d'ouverture d'URL s'affichera alors lors d'un clic sur l'hyperlien.
   * @param html Texte HTML contenant un ou plusieurs signets 'href' du document.
   * @return le code HTML contenant le(s) signet(s) avec le chemin de la documentation accessible par l'utilisateur courant.
   */
  virtual QString htmlDocBookMarkUrl(QString html, const QString &document="cydoc");
  /**
   * @brief xmlDocBookMark Code XML d'un signet d'une documentation.
   * @param id   Identifiant dans la documentation.
   * @param text Texte à afficher. En cas de champ vide, récupère le texte dans le dictionnaire de signets.
   * @return le signet sous forme XML, soit "<link linkend="id">text</link>"
   */
  virtual QString xmlDocBookMark(QString id, QString text="");
  /**
   * @brief Convertit les hyperliens contenus dans \a xml en lien HTML afin de les utiliser à partir de Cylix.
   * @param xml       Code XML à convertir.
   * @param fileName  Nom et chemin du fichier (Ex pdf) contenant la cible.
   */
  static QString htmlLinkFromXml(QString &xml, QString fileName);

  virtual void addEventsPanel(CYWin *win);
  virtual CYEventsPanel *eventsPanel();


  /** Saisir \a true pour que les consignes du projet soient protégées
   * @param dataName Donnée donnant le nom du répertoire intercalaire. Si celui-ci est nul, le répertoire intercalaire est créé à partir de la date et l'heure courante. */
  virtual void setProjectSetpointsProtected(bool val, QByteArray dataName = 0);
  /** @return le répertoire de prtotection des consignes projet */
  virtual QString projectSetpointsProtectedDir();

  /** Vérifie le code d'accès de l'utilisateur courant.
   * @param parent Sort sur ce widget plutôt que la fenêtre principale de Cylix.
   * @param note Ajoute un texte explicatif.
   * @return 0 en cas d'échec, 1 si ok, 2 si mot de passe constructeur et 3 si mot de passe simulation, */
  int  checkPassword(QWidget *parent = 0, QString note=0);

  QIcon loadIconSet(const QString & name, int size = 0);
  QPixmap loadIcon(const QString & name, int size = 0);

  /** @return la langue actuellement chargée. */
  void setCurrentLanguage(const QString &lang) { mCurrLang=lang; }
  /** @return la langue actuellement chargée. */
  QString currentLanguage() { return mCurrLang; }
  /** Charge une langue par le raccourci de langue donnée (ex. de, en) */
  void loadLanguage(const QString &rLanguage);

  /** @return le filtre d'événements en lecture seule. */
  CYReadOnlyFilter* readOnlyFilter() const { return mReadOnlyFilter; }

  /** @return \a true si l'accès à l'environnement de bureau peut être protégé. */
  bool desktopEnvironmentProtectable() { return mDesktopEnvironmentProtectable; }


  /**
   * @brief Saisie la valeur d'initialisation des buffers.
   * @param val Nouvelle valeur d'initialisation des buffers, par défaut à NAN (Not A Number).
   *
   * Cette fonction permet de modifier la valeur par défaut d'initialisation des valeurs des buffers
   * des données. Chaque nouvelle donnée bufferisée prend en compte cette valeur à sa création.
   *
   * La valeur NAN permet par exemple de ne pas afficher en courbes des points invalides.
   * Une fois les valeurs du buffer initialisées à NAN, il faut alors attendre que le buffer se remplisse complètement
   * pour que les échelles automatiques de ses courbes fonctionnnent. Pour que celles-ci fonctionnent
   * dès le début de la bufferisation il faut initialiser le buffer à un valeur différente de NAN
   * comme par exemple 0.
   *
   * Pour initialiser les buffers de nouvelles données par une valeur particulière, il faut appeler cette
   * fonction avant la création de celles-ci. Une fois ces données crées, il faut rappeler la même fonction
   * mais cette fois sans valeur pour remettre la valeur par défaut, soit NAN, et ce afin d'initialiser
   * sans valeur particulière les buffers des autres données par la suite. *
   */
  void setInitBufferVal(double val=NAN);
  /**
   * @return la valeur d'initialisation courante des buffers. @see setInitBufferVal
   */
  double initBufferVal();

signals: // Signals
  /** Signale que l'état des hôtes vient de changer. */
  void hostChanged();
  /** Signale que la comunication avec un hôte donné vient d'échouer. */
  void hostConnectionLost(const QString &hostName);
  /** Redimensionne la taille des bases de données rattachées. */
  void resizeAllDB();
  /** Signale qu'une base de donnée vient de changer. */
  void DBChanged();
  /** Emis à chaque changement de projet.
    * @param index  Index de projet (0 si un seul project actif à la fois).*/
  void projectChanged(int index);
  /** Emis à chaque changement d'essai.
    * @param index  Index de projet (0 si un seul project actif à la fois).*/
  void testChanged(int index);
  /** Active ou désactive les actions suivant l'utilisateur courant et l'état de fonctionnement du process. */
  void enableActions();
  /** Envoie la nouvelle référence client. */
  void newCustomerRef(QString ref);
  /** Demande le chargement du fichier du projet.
      @param index  Index de projet (0 si un seul project actif à la fois). */
  void loadCurrentProject(int index);
  /** Emet le message à afficher dans l'écran de démarrage soft. */
  void splashMessage(const QString &);
  /** Emis au démarrage du module. */
  void moduleStarting();
  /** Emis à chaque changement d'accès. */
  bool accessChanged();
  /** Emis à chaque sauvegarde des paramètres machines dans un fichier texte lisible par le client. */
  void savingBenchSettings();
  void simulationOn();
  void simulationOff();
  /** Donne l'ordre de rafraîchir les valeurs de la barre de messagerie. */
  void refreshingMessagingValues();
  void testEventsMail();
  /** Emis suite à un refus de créer un projet lors d'un premier démarrage de CYLIX. */
  void quitPanel();

public slots:
  /** Redimensionne la taille de la base de données. */
  void resizeDB();

  /** Démarre l'essai du projet courant. */
  virtual void start() {}
  /** Arrête l'essai. */
  virtual void stop() {}

  /** Connexion à un hôte. */
  bool connectHost();

  /** Autorise la connexion réseau avec le(s) RN. */
  void networkConnect();
  /** Désactive la connexion réseau avec le(s) RN. */
  void networkDisconnect();

  void updateGUI();

  /** Construit un nouvel accès utilisateur. Un utilisateur ainsi qu'un groupe
   * d'utilisateurs sont créés à partir du titre \a title.  */
  void newUserAccess(QString title, QString title_group = 0);

  /** Création d'un nouvel utilisateur. */
  CYUser * createUser(QString title);
  /** Supprime un nouvel utilisateur. */
  bool removeUser(CYUser *u);

  /** Création d'un nouveau groupe d'utilisateurs. */
  CYUserGroup * createUserGroup(QString title);
  /** Supprime un nouveau groupe d'utilisateurs. */
  bool removeUserGroup(CYUserGroup *g);

  /** Historise l'accès en cours. */
  virtual void historyAccess();
  /** Change l'accès. */
  virtual bool changeAccess(CYUser *access);
  /** Change l'accès utilisateur.
   * @param title  Titre de l'accès. */
  bool changeUserAccess(QString title);
  /** Change le mot de passe de l'utilisateur courant. */
  void changeUserPassword();
  /** @return l'utilisateur courant. */
  CYUser *user();
  /** @return l'utilisateur ayant pour titre d'accès \a title. */
  CYUser *user(QString title);
  /** @return le groupe utilisateurs ayant pour titre d'accès \a title. */
  CYUserGroup *userGroup(QString title);
  /** @return \a true si l'administration des utilisateurs est pyramidale. */
  bool pyramidalUserAdmin() { return mPyramidalUserAdmin; }

  /** @return l'utilisateur administrateur. */
  CYUser *administrator() { return mAdministrator; }
  /** Saisie l'utilisateur administrateur. */
  void setAdministrator(CYUser *user) { mAdministrator = user; }

  /** @return l'accès protégé. */
  CYUser *protectedAccess() { return mProtectedAccess; }
  /** Saisie l'accès protégé. */
  void setProtectedAccess(CYUser *access) { mProtectedAccess = access; }
  /** @return \a true si l'accès protégé est en cours. */
  bool isProtectedAccess();
  /** Active/désactive l'accès protégé. */
  void enableProtectedAccess();

  /** @return l'état de fonctionnement du process. */
  bool isRunning() { return mRunning; }
  /** Démarre le module de communication réseau. */
  void startModule();
  /** Saisie l'état de fonctionnement du process. */
  virtual void setRunning(bool state);

  /** Reçoit un signal d'attente opérateur.
   * @param id Identificateur de l'attente.  */
  virtual void waitOperator(int id);

  /** Création de la zone d'affichage des infos supplémentaires */
  virtual void printInfo(QPainter *p, QRectF *r);
  /** Saisie la liste d'impressions de valeur de donnée. */
  virtual void setPrintableInfoList(QList<CYPrintableInfo*> list) { mPrintableInfoList = list; }
  /** @return la liste d'impressions de valeur de donnée. */
  virtual QList<CYPrintableInfo*> printableInfoList() { return mPrintableInfoList; }
  /** @return l'état du process. */
  virtual QString processState();

  /** Création d'un PV d'essai. */
  virtual void createTestReport() {}
  /** Création d'un PV d'essai.
   * @param simul simule le PV d'essai si géré dans Cylix */
  virtual void createTestReport(bool simul) { Q_UNUSED(simul); }

  /** @return \a true si le mode forçage est verrouillé. */
  bool lockForcing();
  /** Verrouille ou non selon \a val le mode forçage. */
  virtual void setLockForcing(bool val);

  /** @return \a true si le mode d'accès constructeur est actif. */
  bool designer();
  /** @return \a true si \a password est le mot de passe constructeur.
   * Dans ce cas le mode d'accès constructeur devient alors actif. */
  bool isDesignerPassword(QString password);

  /** @return \a true si le mode d'accès de simulation est actif. */
  bool
  simulation();
  /** @return \a true si \a password est le mot de passe de simulation.
   * Dans ce cas le mode d'accès en simulation devient alors actif. */
  bool isSimulationPassword(QString password);

  /** Appelé lors l'appel de l'à propos. */
  virtual void showAboutApplication();

  /** @return le chemin du répertoire où sont sauvegardées les données générales de l'application. */
  QString appDataDir() { return mAppDataDir; }
  /** Saisie le chemin et créer le répertoire où sont sauvegardées les données générales de l'application. */
  virtual void setAppDataDir(QString path);

  /** @return le chemin du répertoire où sont sauvegardées les réglages généraux de l'application. */
  QString appSettingDir() { return mAppSettingDir; }
  /** Saisie le chemin et créer le répertoire où sont sauvegardées les réglages généraux de l'application. */
  virtual void
  setAppSettingDir(QString path);

  /** @return le chemin du répertoire où sont sont enregistrés les fichiers temporaires de l'application.
    * Ce répertoire est supprimé à chaque changement utilisateur. */
  QString appTmpDir() { return mAppTmpDir; }
  /** Saisie le chemin et créer le répertoire où sont sont enregistrés les fichiers temporaires de l'application.
   *  Si le répertoire existe déjà, alors il est supprimé puis recréé. */
  virtual void setAppTmpDir(QString path);

  /** @return le chemin du répertoire d'essai courrant.
    * @param index  Index de projet (0 si un seul project actif à la fois).*/
  QString testDirPath(int index);
  /** @return \a true si le répertoire d'essai courrant existe.
    * @param index  Index de projet (0 si un seul project actif à la fois).*/
  bool testDirOk(int index);
  /** Ouvre la fenêtre d'acquisitions. */
  void acquisitionWin();
  /** Création de la fenêtre de métrologie
   * @param name Nom de la fenêtre utilisé dans l'administration des utilisateurs. */
  void createMetroWin(QString name = tr ("Metrology window"), int index=0, QString label=0);
  /** Ouvre la fenêtre de métrologie. */
  void showMetroWin();
  /** Ouvre la fenêtre de métrologie. */
  CYMetroWin *metroWin();
  /** Ajoute une mesure en métrologie. */
  void addMetro(CYMeasure *mes);

  /** Initialise les acquisitions. */
  virtual void initAcquisitions() {}
  /** Archivage du post-mortem. */
  virtual void postMortemBackup() {}

  /** Réception de la durée en secondes du programme principal à chaque changement de celle-ci. */
  virtual void setProgramTimeChanged(uint time);
  /** @return la durée en secondes du programme principal. */
  virtual uint programTime() { return 0; }

  /** Initialise la table profibus globale. */
  virtual void initProfibus() {}
  /** Ajoute une table profibus avec \a nb appareils. */
  void addProfibusTable(t_pb_desc **table, int nb);
  /** @return l'élément profibus ayant pour pointeur \a ptr. */
  CYProfibusItem *profibusItem(void *ptr);

  /** Supprime l'écran de démarrage soft. */
  void deleteSplash();

  /** @return la configuration de l'application spécifique à l'utilisateur OS en cours. */
  QSettings *OSUserConfig();
  /** @return la configuration de l'application commune à tous les utilisateurs de l'OS. */
  QSettings *OSConfig();

  /** @return \a true s'il manque le projet courant. */
  bool projectLack() { return mProjectLack; }
  /** Saisir \a true pour indiquer qu'il manque le projet courant. */
  void setProjectLack(bool val) { mProjectLack = val; }
  /** Autorise ou non le rafraîchissement des objets d'analyse graphique:
   * oscilloscope, hystogramme, multimètre... */
  void enableAnalyseRefresh(bool val);
  /** Autorise ou non le rafraîchissement des objets d'analyse graphique:
   * oscilloscope, hystogramme, multimètre...
   * Si le rafraîchissement était autorisé alors il ne l'est plus ou inversement. */
  void enableAnalyseRefresh();
  /** @return \a true si le rafraîchissement des objets d'analyse graphique:
   * oscilloscope, hystogramme, multimètre..., est autorisé. */
  bool analyseRefreshEnabled();
  /** Arrête toutes les acquisitions. */
  void stopAcquisitions();

  /** Ajoute un mouvement dans le dictionnaire de mouvements.
   * @param key Clé dans le dictionnaire. */
  virtual void addMV(const QString & key, CYMV *mv);
  /** @return le mouvement ayant pour clé \a key dans le dictionnaire de mouvements. */
  virtual CYMV *mv(const QString & key, bool useFatal = true);
  /** @return le groupe du mouvement ayant pour clé \a key dans le dictionnaire de mouvements. */
  virtual QString groupMV(const QString & key);

  /** Ajoute un contrôle tolérances dans le dictionnaire de contrôles tolérances.
   * @param key Clé dans le dictionnaire. */
  virtual void addCT(const QString & key, CYCT *ct);
  /** @return le contrôle tolérances ayant pour clé \a key dans le dictionnaire de contrôles tolérances. */
  virtual CYCT *ct(const QString & key, bool useFatal = true);
  /** @return le contrôle tolérances ayant pour index réseau \a idNet dans le dictionnaire de contrôles tolérances. */
  virtual CYCT *ct(int idNet, bool useFatal = true);
  /** @return le groupe du contrôle tolérances ayant pour clé \a key dans le dictionnaire de contrôles tolérances. */
  virtual QString groupCT(const QString & key);

  /** @return la connexion réseau qui gère la com \a com. */
  virtual CYNetLink *link(int com);

  /** Envoie au(x) régulateur(s) le projet.
    * @param index  Index de projet (0 si un seul project actif à la fois).*/
  virtual void sendProject(int index);
  /**
   * @brief Envoie au(x) régulateur(s) le(s) projet(s).
   */
  virtual void sendProject();
  virtual void quit();

  /** Active l'évènement de création de projet.*/
  virtual void setCreateProjectEvent(int projectId=0);
  /** Active l'évènement de chargement de projet.*/
  virtual void setLoadProjectEvent(int projectId=0);
  /** Active l'évènement de fermeture de projet.*/
  virtual void setCloseProjectEvent(int projectId=0);
  /** Active l'évènement de sauvegarde de projet.*/
  virtual void setSaveProjectEvent(int projectId=0);

  /** Sauvegarde les paramètres machines dans un fichier texte lisible par le client.*/
  virtual void saveBenchSettings();

  virtual void  mkdir(const QString & dirName, bool acceptAbsPath = true);
  /** Copie d'un répertoire.
   * @brief copyDirectory
   * @param sourcePath Réperptoire à copier
   * @param destinationPath Réperptoire destination
   * @return \a false en cas d'erreur après un messsage d'alerte.
   */
  virtual bool copyDirectory(const QString &sourcePath, const QString &destinationPath);
  /** Suppresion d'un répertoire.
   * @param path Chemin du réperptoire
   * @return \a false en cas d'erreur après un messsage d'alerte.
   */
  virtual bool removeDirectory(const QString &path);
  virtual void acknowledge();

  virtual void backupFile(QString file);
  virtual void backupDir(QString dir);
  virtual void startRefreshDB();
  /**
   * @brief Sauvegarde état machine.
   * Cette sauvegarde se fait dans un répertoire horodaté en reprenant les chemins complets des fichiers courants contenants les données y compris celles venant du RN.
   * Ces sauvegardes peuvent être générées par l'utilisateur lorsqu'il souhaite transmettre un état particulier de la machine au développeur.
   * Afin d'illustrer le manuel utilisateur, ces sauvegardes permettent également au développeur d'initialiser des vues sur son PC avec des valeurs réelles provenant du PC superviseur.
   */
  virtual void machineStatusBackup();
  virtual void cylixStop();
  virtual void setNotifyDatasLoading(bool val);

  /*! Ouvre le manuel PDF par l'application par défaut de l'OS.
      \fn CYCore::invokeHelp()
  */
  virtual void invokeHelp();
  /*! Ouvre le manuel PDF sur un de ses liens par l'application par défaut de l'OS.
   *  Ouvre le manuel opérateur Si l'utilisateur n'a pas accès au manuel de Cylix.
      \fn CYCore::invokeHelp(const QString &document, const QString &destination)
      @param document    Nom du document.
      @param bookmark    Signet dans le PDF
  */
  virtual void invokeHelp(const QString &document, const QString &bookmark="");

  /** Vérifie les valeurs constructeur des données de la liste des données pouvant avoir une incohérence entre la valeur constructeur et les bornes. */
  virtual void chekDefDatas();
  /** Ouvre une fenêtre Exporte dans un fichier toutes les données de l'application par ligne avec en colonne leur groupe, nom, libellé, valeur et aide.
   * @see CYDB::exportDatas() */
  virtual void exportDatas();
  /** Fonction d'export des données de l'application dans un fichier csv.
   * Chaque ligne représente une donnée avec les champs suivants espacés d'une tabulation:
   * Hôte(s), connexion(s), groupe, nom, description, valeur, def, min, max, aide.
   * @see CYData::exportData() */
  virtual void exportDatas(QString fileName);
  /** Création d'entités dans le fichier "/tmp/cylix_entity.dtd". Celle-ci sont des macros semblables à des #define.
   * Ces entités contiennent du texte généré par Cylix dans la langue en cours qui peuvent ensuite être utilisé pour réaliser le manuel en ligne.
   * @see CYDB::createEntitiesDocBook() */
  virtual void createEntitiesDocBook();
  /** Ajoute une entité XML pour le fichier "/tmp/cylix_entity.dtd" utilisable dans le manuel en ligne.
   *  Pour chaque données une entité est créée automatiquement avec comme identifiant son nom et comme valeur l'équivalent en XML de l'aide en bulle de saisie.
   *  Il est ainsi possible de récupérer le texte d'aide des sources de Cylix pour l'intégrer dans le manuel en ligne.
   *  Les données relatives à une option de compilation ne sont pas créées lorsque l'option est désactivée alors que leurs entités peuvent se trouver dans le code XML du manuel.
   *  Bien que celui-ci puisse être conditionnée aux options de compilation, des erreurs "Entity not defined" empêchent alors la création du manuel.
   *  Pour palier ceci, il suffit de créer une ENTITY vide dans le code compilé.
   * @brief addDocBookEntity Ajoute une entité du manuel.
   * @param id    Identifiant dans la documentation.
   * @param label Texte à afficher.
   * @see CYDB::createEntitiesDocBook()
   */
  virtual void addDocBookEntity(QString id, QString text="");

  /** Fonction appelé une fois que l'application est complètement démarré. */
  virtual void setStarted();
  /** @return \a true une fois que l'application est complètement démarré. */
  bool started() {return mStarted; }

  /** @return l'historique des paramètres projet.*/
  CYFileCSV *historyProject() { return mHistoryProject; }

  /** Génère le manuel en ligne DocBook. */
  virtual void createDocBook();

  /** Charge la configuration des raccourcis clavier de l'environnement de bureau.
    @param protect charge la configuration protégé Cylix, sinon restore la configuration d'origine de l'environnement. */
  virtual void loadDesktopShortcuts(bool protect);

  /** Ouverture de SoliaGraph.
   * @brief soliaGraph
   */
  void openSoliaGraph();

protected slots:
  /** Appel l'aide en ligne de connexion à un hôte. */
  void helpConnectHost();
  virtual void screenSaverDetection();
  virtual void screenSaverAction();
  virtual void readPanelPID();
  virtual void readDesktopPID();

protected:
  virtual void backup(QString source, bool isDir);
  /** Génère le(s) manuels en ligne (complet, opérateur...) en format PDF via le script par défaut cydoc2pdf.
   *  Cette fonction peut être enrichie pour générer par exemple différents manuels
   *  dans différents format par l'apppel d'autres scripts propre à l'affaire.
   *  Il est possible de générer différentes variantes de manuel avec le même code XML via des conditions définies par des --stringparam dans cydoc2pdf.
   * @brief createDocBook
   * @param option      Option du script de génération cydoc2pdf
   * @param language    Langue dans laquelle généré la doc.
   */
  virtual void createDocBook(QString option, QString language);
  /** Génère un manuel en ligne en apppelant le script par défaut cydoc2pdf.
   *  Cette fonction peut être enrichie pour générer par exemple différents manuels
   *  dans différents format par l'apppel d'autres scripts propre à l'affaire.
   *  Il est possible de générer différentes variantes de manuel avec le même code XML via des conditions définies par des --stringparam dans cydoc2pdf.
   * @brief createDocBook
   * @param option      Option du script de génération cydoc2pdf
   * @param language    Langue dans laquelle généré la doc.
   * @param prefixFile  Préfix du nom du fichier principal xml et du fichier résulat pdf (ex cydoc, cydoc_oper...).
   * @param conditions  Activation de code XML suivant des conditions.
   */
  virtual void createDocBook(QString option, QString language, QString prefixFile, QStringList conditions=QStringList(""));

public:
  // Public attributes
  /** Module de communication réseau. */
  CYNetModule *module;
  /** Dictionnaire des différents formats d'affichage. */
  QHash<int, CYFormat*> formats;
  /** Base de données globale. */
  QHash<QString, CYData*> db;
  /** Base profibus globale. */
  QHash<int, CYProfibusItem*> profibus;
  /** Dictionnaire des différentes bases de données. */
  QHash<QString, CYDB*> dbList;
  /** Dictionnaire des différents hôtes. */
  QHash<QString, CYNetHost*> hostList;
  /** Dictionnaire des différentes connections réseau. */
  QHash<QString, CYNetLink*> linkList;
  /** Gestionnaire d'évènements du système. */
  CYEventsManager *events;
  /** Générateur d'évènements superviseur */
  CYEventsGeneratorSPV *eventsGeneratorSPV;
  /** Listes d'acquisitions gérées par l'application. */
  QHash<QString, CYAcquisition*> acquisitions;
  /** Style des afficheurs. */
  CYDisplayStyle *style;
  /** Indique qu'il n'y a pas de projet courant. */
  bool mNoCurrentProject;
  /** Evènement de rafraîchissement des objets d'analyse graphique:
   * oscilloscope, hystogramme, multimètre..., est autorisé. */
  flg analyseRefreshEvent;

  /** Liste des mesures en métrologie */
  QList<CYMeasure*> metroList;

  /** Permet de désactiver temporairement les messages de debug. */
  bool disableCYDEBUG;
  /** Permet de désactiver temporairement des messages en console. */
  bool disableCYMESSAGE;
  /** Permet de désactiver temporairement les messages d'alertes. */
  bool disableCYWARNING;
  /** Permet de désactiver temporairement les messages de défaut critique. */
  bool disableCYFATAL;

protected:
  // Protected attributes
  /** Core initialisé. */
  bool mInitOk;
  /** Nom du banc d'essai. */
  QString mBenchType;
  /** Projets d'essai en cours. */
  QHash<int, CYProject *> mProject;
  /** Fichiers des projets en cours. */
  QHash<int, QString> mProjectFile;
  /** Timer de mise à jour rapide de données. */
  QTimer *fastTimer;
  /** Timer de mise à jour lente de données. */
  QTimer *slowTimer;
  /** Utilisateur en cours. */
  CYUser *mUser;
  /** Liste d'utilisateurs. */
  QHash<int, CYUser*> mUsers;
  /** Liste de groupe d'utilisateurs. */
  QHash<int, CYUserGroup*> mUserGroups;
  /** Utlisateur administrateur. */
  CYUser *mAdministrator;
  /** Accès protégé. */
  CYUser *mProtectedAccess;
  /** Base données initialisées. */
  bool mInitDBOk;
  /** Etat de fonctionnement du process. */
  bool mRunning;
  /** Nom du client. */
  QString mCustomerName;
  /** Réference constructeur. */
  QString mDesignerRef;
  /** Réference client du banc. */
  QString mCustomerRef;
  /** Message d'en-tête à imprimer. */
  QString mPrintingHeader;
  /** Message qui contient le type d'essai en cours. */
  QString mTestType;
  /** Liste d'impressions de valeur de donnée. */
  QList<CYPrintableInfo*> mPrintableInfoList;
  /** Chemin du répertoire où sont installés les fichiers kde de l'application. */
  QString mKdeDir;
  /** Chemin du répertoire où sont sauvegardées les données générales de l'application. */
  QString mAppDataDir;
  /** Chemin du répertoire où sont sauvegardées les réglages généraux de l'application. */
  QString mAppSettingDir;
  /** Chemin du répertoire où sont enregistrés les fichiers temporaires de l'application. */
  QString mAppTmpDir;

  /** Fenêtre d'acquisitions. */
  CYAcquisitionWin *mAcquisitionWin;
  /** Fenêtre de métrologie. */
  CYMetroWin *mMetroWin;

  /** Vaut \a true si un projet est chargé. */
  bool mProjectLoaded;
  /** Indique qu'il manque le projet courant. */
  bool mProjectLack;

  /** Vaut \a true si le répertoire d'essai est par défaut à créer à chaque lancement d'essai. */
  bool mNewTestDir;
  /** Vaut \a true si le répertoire d'essai est créé automatiquement, sinon une demande de choix de répertoire est faite à l'utilisateur. */
  bool mTestDirAuto;

  /** Objet de configuration de l'application. */
  QSettings *mConfig;

  /** Flag autorisant ou non le rafraîchissement des objets d'analyse graphique:
   * oscilloscope, hystogramme, multimètre... */
  bool mEnableAnalyseRefresh;
  /** Ecran de démarrage soft. */
  CYSplashScreen *mSplash;
  /** Dictionnaire de mouvements. */
  QHash<QString, CYMV*> mDictMV;
  /** Dictionnaire de contrôles tolérances. */
  QHash<QString, CYCT*> mDictCT;

  QHash<int, flg*>mCreateProjectEvent;
  QHash<int, flg*>mLoadProjectEvent;
  QHash<int, flg*>mCloseProjectEvent;
  QHash<int, flg*>mSaveProjectEvent;
  /** Editeur de projet. */
  CYProjectEditor * mProjectEditor;

  /** Base des données CYLIBS.*/
  CYDBLibs *mCYDBLibs;
  QString mBaseDirectory;
  CYMainWin * mMainWin;
  bool mNotifyDatasLoading;
  QHash<QString, CYWin*> mWindows;
  bool mScreenSaverDetected;
  int mMaxCurvesByScope;

  bool mSimulEvents;
  bool mSimulMode;

  bool mPyramidalUserAdmin;

  /** Liste de données dont la valeur constructeur est à vérifier à la fin du démarrage de l'application. */
  QList<CYData*> mDataCheckDef;

  /** Bandeau de messagerie affiché dans toutes les fenêtres principales de Cylix @see CYWin. */
  CYEventsPanel *mEventsPanel;

  /** Répertoire où sont intallés les fichiers système KDE3 à ajuster en fonction du système */
  QString mKde3Dir;

  /** Historique des accès.*/
  CYFileCSV *mHistoryAccess;
  /** Enregistrement des temps d'essai.*/
  CYFileCSV *mTestTimes;
  /** Historique des paramètres projet.*/
  CYFileCSV *mHistoryProject;

  /** Contient la langue actuellement chargée. */
  QString mCurrLang;

  /** Vaut \a true si l'accès à l'environnement de bureau peut être protégé. */
  bool mDesktopEnvironmentProtectable;

  /** Gestionnaite d'événements*/
  CYEventsManager *mEventsManager;

  virtual void initSoliaGraph();

private:
  // Private attributes
  /** Boîte de connexion à un hôte. */
  CYNetConnect *ncd;
  /** Liste des widgets rattachés à la base de données. */
  QHash<QString, QWidget*> broadcaster;
  /** Verrou du mode forçage. */
  bool mLockForcing;
  /** Offset sur la taille des police par défaut de l'application.  */
  int mOffsetFontSize;
  /** Vaut \a true lorsque l'application est complètement démarré. */
  bool mStarted;
  /** Format de date heure par défaut de l'application. */
  QString mDateTimeFormat;
  /** Donnée donnant le nom du répertoire de protection des paramètres */
  CYString *mProtectProtectDirData;

  /** Filtre d'événements en lecture seule.*/
  CYReadOnlyFilter *mReadOnlyFilter;

  QProcess *mProcessPanelPID;
  QProcess *mProcessDesktopPID;

  QSize mAboutApplicationSize;

  /** Dictionnaire de signets du manuel. */
  QHash<QString, QString> mDocBookMark;
  /** Dictionnaire d'entités du manuel. */
  QHash<QString, QString> mDocBookEntity;

  /** Autorise ou non la connexion réseau avec le(s) RN. */
  bool mNetworkEnable;

  double mInitBufferVal;

  int mUserId;
};
/** Instance globale accessible par tout dans l'application. */
extern CYCore *core;

#endif
