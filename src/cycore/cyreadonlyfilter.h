#ifndef CYREADONLYFILTER_H
#define CYREADONLYFILTER_H

#include <QObject>
#include <QEvent>

/** Filtre d'événements en mode lecture seule permettant de
 * désactiver la gestion des évènements de saisie d'objets
 * graphiques.
 * @short Filtre d'événements en mode lecture seule.
 * @author Gérald LE CLEACH
*/
class CYReadOnlyFilter : public QObject
{
  Q_OBJECT
public:
  explicit CYReadOnlyFilter(QObject *parent = 0);

protected:
  bool eventFilter(QObject *obj, QEvent *event);
};

#endif // CYREADONLYFILTER_H
