/***************************************************************************
                          cywin.cpp  -  description
                             -------------------
    début                  : mar fév 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cywin.h"

// QT
#include <QLayout>
#include <QEvent>
#include <QTimerEvent>
#include <QEvent>
#include <QWhatsThis>
#include <QToolButton>
// CYLIBS
#include "cy.h"
#include "cycore.h"
#include "cyuser.h"
#include "cyusergroup.h"
#include "cyuseractioncollection.h"
#include "cyeventspanel.h"
#include "cynumdisplay.h"
#include "cyapplication.h"
#include "cystatusbar.h"
#include "cymessagebox.h"
#include "cyaboutdata.h"
#include "cyactionmenu.h"

CYWin::CYWin(QWidget *parent, const QString &name, int index, const QString &label, Qt::WindowFlags f, bool unique)
  : QMainWindow(parent, f),
    mIndex(index),
    mLabel(label)
{
  setObjectName(name);
  setEnableEventsPanel(false);
  dontSaveSession = false;
  mFisrtShow=true;
  mUnique=unique;
  //  timerId = -1;
  mActionCollection = 0;
  mToolButtonStyle = Qt::ToolButtonTextBesideIcon;

  mFileMenu = 0;
  mEditMenu = 0 ;
  mSettingMenu = 0;

  helpMenu=0;
  mStatusBarTog = 0;
  //  mStatusBarTog = KStdAction::showStatusbar( this, SLOT(showStatusBar()) , actionCollection());
  //  KStdAction::configureToolbars(this, SLOT(editToolbars()), actionCollection());
  //  mStatusBarTog->setChecked(false);

  //  (void) new CYAction(tr("C&onnect host..."), "cyconnect_established", 0, this, SLOT(connectHost())   , actionCollection(), "connect_host");

  // TODO KDE3->QT3
  //  setHelpMenuEnabled(false);

  if ( mUnique ) // mUnique=false pour CYScopeAnalyser
  {
    core->addWindow( this );
  }
}

CYWin::~CYWin()
{
}

void CYWin::connectHost()
{
  core->engageHost("");
}

void CYWin::showStatusBar()
{
}

//void CYWin::show()
//{
////  if (mFisrtShow && core && !core->designer() && !core->simulation())
////  {
////    // Premier affichage en taille maxi pour utlisateur lambda
////    setWindowState(Qt::WindowMaximized);
////  }
////  else
////  {
////    setWindowState(Qt::WindowActive);
////  }
//  QMainWindow::show();
////  mFisrtShow = false;
//}

void CYWin::editToolbars()
{
  // TODO KDE3->QT3
  //  QString xmlFile = actionCollection()->xmlFile().section( '/', -1 );
  //  KEditToolbar dlg(actionCollection(), xmlFile, false);
  //  actionCollection()->setXMLFile( xmlFile );

  //  dlg.exec();
  //  core->user()->updateGUI( metaObject()->className() );
}

void CYWin::customEvent(QEvent *ev)
{
  Q_UNUSED(ev)
  // TODO QT5
  //  if (ev->type() == QEvent::User)
  //  {
  //    /* Due à la communication asynchrone entre cylix et ses correspondants,
  //     * nous devons parfois montrer des boîtes de message qui sont activées
  //     * par des objets qui sont déjà morts. */
  //    CYMessageBox::error(this, *((QString *)ev->data()));
  //    delete (QString *)ev->data();
  //  }
}

void CYWin::timerEvent(QTimerEvent *)
{
  //  if (statusbar->isVisibleTo(this))
  //  {
  //    /* Demande quelques infos sur l'état de la mémoire. L'info
  //     * demandée sera reçue par answerReceived(). */
  //    SensorMgr->sendRequest("localhost", "pscount"           , (SensorClient*) this, 0);
  //    SensorMgr->sendRequest("localhost", "mem/physical/free" , (SensorClient*) this, 1);
  //    SensorMgr->sendRequest("localhost", "mem/physical/used" , (SensorClient*) this, 2);
  //    SensorMgr->sendRequest("localhost", "mem/swap/free"     , (SensorClient*) this, 3);
  //  }
}

void CYWin::closeEvent(QCloseEvent *event)
{
  if (!queryClose())
    event->ignore();
}

bool CYWin::queryClose()
{
  if (core && !dontSaveSession)
  {
    saveProperties(core->OSUserConfig());
    core->OSUserConfig()->sync();
  }

  return (true);
}


bool CYWin::canBeRestored( int number )
{
  if ( !cyapp->isRestored() )
    return false;
  QSettings *config = cyapp->OSUserConfig();
  if ( !config )
    return false;
  config->beginGroup( QString::fromLatin1("Number") );
  int n = config->value( QString::fromLatin1("NumberOfWindows") , 1 ).toInt();
  return number >= 1 && number <= n;
}

bool CYWin::restore( int number, bool show )
{
  if ( !canBeRestored( number ) )
    return false;
  QSettings *config = cyapp->OSUserConfig();
  if ( readPropertiesInternal( config, number ) )
  {
    if ( show )
      CYWin::show();
    return false;
  }
  return false;
}

void CYWin::savePropertiesInternal( QSettings *config, int number )
{
  // TODO KDE3->QT3
  //    bool oldASWS = d->autoSaveWindowSize;
  //    d->autoSaveWindowSize = true; // make saveMainWindowSettings save the window size

  QString s;
  s.setNum(number);
  s.prepend(QString::fromLatin1("WindowProperties"));
  config->beginGroup(s);

  // store objectName, className, Width and Height  for later restoring
  // (Only useful for session management)
  config->setValue(QString::fromLatin1("ObjectName"), objectName());
  config->setValue(QString::fromLatin1("ClassName"), metaObject()->className());

  //    saveMainWindowSettings(config); // Menubar, statusbar and Toolbar settings.

  s.setNum(number);
  config->beginGroup(s);
  saveProperties(config);

  //    d->autoSaveWindowSize = oldASWS;
}

bool CYWin::readPropertiesInternal( QSettings *config, int number )
{
  //    if ( number == 1 )
  //        readGlobalProperties( config );

  // in order they are in toolbar list
  QString s;
  s.setNum(number);
  s.prepend(QString::fromLatin1("WindowProperties"));

  config->beginGroup(s);

  //    // restore the object name (window role)
  //    if ( config->hasKey(QString::fromLatin1("ObjectName" )) )
  setObjectName( config->value(QString::fromLatin1("ObjectName")).toString()); // latin1 is right here

  //    applyMainWindowSettings(config); // Menubar, statusbar and toolbar settings.

  s.setNum(number);
  config->beginGroup(s);
  readProperties(config);
  return true;
}

void CYWin::readProperties(QSettings *cfg)
{
  // TODO KDE3->QT3 voir si pas de problème dû au changement de langue
  while (!cfg->group().isEmpty()) cfg->endGroup();
  cfg->beginGroup(objectName());

  //  if (cfg->readBoolEntry("isDesigner", 0) == true)
  //  {
  //     showMinimized();


  for ( int i = 0; i < MAX_RECENTFILES; ++i )
  {
    QString filename = cfg->value( "File" + QString::number( i + 1 ) ).toString();
    if ( !filename.isEmpty() )
      mRecentFiles.push_back( filename );
  }
  if ( mRecentFiles.count() )
    updateRecentFilesMenu();

  if (mStatusBarTog && (!cfg->value("StatusBarHidden", 1).toInt()))
  {
    mStatusBarTog->setChecked(true);
    showStatusBar();
  }

//  setMinimumSize(sizeHint());

  /* Nous pouvons ignorer 'isMaximized' parce que nous ne pouvons pas mettre
      la fenêtre en taille maxi, nous sauvegardons alors à la place les coordonnées */
//  if (cfg->value("isMinimized").toBool() == true)
//    showMinimized();
//  else
  {
    int wx = cfg->value("PosX", 100).toInt();
    int wy = cfg->value("PosY", 100).toInt();
    int ww = cfg->value("SizeX", 600).toInt();
    int wh = cfg->value("SizeY", 375).toInt();
    setGeometry(wx, wy, ww, wh);
  }
}


void CYWin::saveProperties(QSettings *cfg)
{
  while (!cfg->group().isEmpty()) cfg->endGroup();
  cfg->beginGroup(objectName());

  cfg->setValue("PosX", geometry().x());
  cfg->setValue("PosY", geometry().y());
  cfg->setValue("SizeX", width());
  cfg->setValue("SizeY", height());
  cfg->setValue("isMinimized", isMinimized());
  cfg->setValue("isDesigner", core->designer() || core->simulation() );

  for ( int i = 0; i < int(mRecentFiles.count()); ++i )
    cfg->setValue( "File" + QString::number( i + 1 ), mRecentFiles[i] );

  if (mStatusBarTog)
    cfg->setValue("StatusBarHidden", !mStatusBarTog->isChecked());
}

void CYWin::slotStatusMsg(const QString& text)
{
  Q_UNUSED(text)
  //QT5  ///////////////////////////////////////////////////////////////////
  //QT5  // change status message permanently
  //QT5  QString status = text.simplified();
  //QT5  if (!status.isEmpty())
  //QT5  {
  //QT5    int actualWidth = mStatusLabel->fontMetrics().width(status);
  //QT5    int realLetterCount = status.length();
  //QT5    int newLetterCount = (mStatusLabel->width() * realLetterCount) / actualWidth;
  //QT5    if (newLetterCount > 3)
  //QT5      newLetterCount -= 4;
  //QT5    status = status.left(newLetterCount);
  //QT5  }
  //QT5  mStatusLabel->setText(status);
}

void CYWin::showAboutApplication()
{
  core->showAboutApplication();
}

void CYWin::createGUIMenu(QMenu *menu_parent, QDomElement &el, QString path)
{
  if (!menu_parent)
  {
    CYWARNING;
    return;
  }
  QString name=el.attribute( "name" );
  CYActionMenu *actionMenu=(CYActionMenu *)actionCollection()->action(name);
  QString title;
  if (!actionMenu)
    return;

  if (actionMenu->isVisible())
    title = actionMenu->text();
	path = title+"/";

  QMenu *menu = actionMenu->popupMenu();

	if (name=="languagesMenu")
  {
    createLanguageMenu(menu);
    if (menu->isEmpty())
      return;
  }
  else
  {
    QDomNodeList list = el.childNodes();
    for (int i=0; i<list.count(); ++i)
    {
      QDomElement item = list.item(i).toElement();
      if (item.tagName()=="text")
        title = item.text();
      if (item.tagName()=="SubMenu")
				createGUIMenu(menu, item, path);
      if (item.tagName()=="Action")
				createGUIAction(menu, item, path);
      if (item.tagName()=="Separator")
        menu->addSeparator();
    }
  }
  menu->setTitle(title);
  menu_parent->addMenu( menu );
}

CYAction *CYWin::createGUIAction(QMenu *menu_parent, QDomElement &el, QString path)
{
  if (!menu_parent)
  {
    CYWARNING;
    return 0;
  }

  QString name=el.attribute( "name" );
	CYAction *action = actionCollection()->addActionTo( name, menu_parent);
	if (action)
		action->setMenuPath(path);
	return action;
}


void CYWin::createGUIMenu(QToolBar *toolBar, QDomElement &el)
{
  if (!toolBar)
  {
    CYWARNING;
    return;
  }
  QString name=el.attribute( "name" );
  CYActionMenu *actionMenu=(CYActionMenu *)actionCollection()->action(name);
  QString title;
  if (!actionMenu)
    return;

  if (actionMenu->isVisible())
      title = actionMenu->text();

  QMenu *menu = actionMenu->popupMenu();

  QDomNodeList list = el.childNodes();
  for (int i=0; i<list.count(); ++i)
  {
    QDomElement item = list.item(i).toElement();
    if (item.tagName()=="text")
      title = item.text();
    if (item.tagName()=="SubMenu")
			createGUIMenu(menu, item, "");
    if (item.tagName()=="Action")
			createGUIAction(menu, item, "");
    if (item.tagName()=="Separator")
      menu->addSeparator();
  }
  menu->setTitle(title);

  QToolButton* toolButton = new QToolButton();
  toolButton->setMenu(menu);
  toolButton->setPopupMode(QToolButton::InstantPopup);
  toolBar->addWidget(toolButton);
  toolButton->setDefaultAction(actionCollection()->action(name));
  toolButton->setToolButtonStyle(mToolButtonStyle);
}

Qt::ToolButtonStyle CYWin::toolButtonStyle()
{
  return mToolButtonStyle;
}

void CYWin::setToolButtonStyle(Qt::ToolButtonStyle style)
{
  mToolButtonStyle = style;
  createGUI();
}

CYAction *CYWin::createGUIAction(QToolBar *toolBar, QDomElement &el)
{
  if (!toolBar)
  {
    CYWARNING;
    return 0;
  }

  QString name=el.attribute( "name" );
	CYAction *action = actionCollection()->addActionTo( name, toolBar);
	return action;
}

/** Remplace KMainWindow::createGUI( const QString &xmlfile, bool _conserveMemory )
 *  sans gestion du fichier XML qui servait surtout à la configuration des menus KDE
 *  par l'utilisateur. */
void CYWin::createGUI(const QString &xmlfile)
{
  // disabling the updates prevents unnecessary redraws
  setUpdatesEnabled( false );

  // make sure to have an empty GUI
  QMenuBar* mb = menuBar();

  //  (void)toolBarIterator(); // make sure toolbarList is most-up-to-date
  //  toolbarList.setAutoDelete( true );
  //  toolbarList.clear();
  //  toolbarList.setAutoDelete( false );

  if (xmlfile.isEmpty())
  {
    setUpdatesEnabled( true );
    updateGeometry();
    return;
  }

  QFile file(QString(":guirc/%1").arg(xmlfile));
  if ( !file.open( QIODevice::ReadOnly ) )
  {
    CYMessageBox::sorry( this, tr( "Can't open the file %1" ).arg( xmlfile ) );
    setUpdatesEnabled( true );
    updateGeometry();
    return;
  }

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  QString errorMsg;
  int errorLine;
  int errorColumn;
  if ( !doc.setContent( &file, &errorMsg, &errorLine, &errorColumn ) )
  {
    CYMessageBox::sorry( this, tr( "The file %1 does not contain valid XML\n"
                                   "%2: line:%3 colomn:%4" ).arg( xmlfile ).arg( errorMsg ).arg( errorLine ).arg( errorColumn ) );
    return;
  }
  // Vérifie le type propre du document.
  if ( doc.doctype().name() != "kpartgui" )
  {
    CYMessageBox::sorry( this, QString( tr( "The file %1 does not contain a valid"
                                            "definition, which must have a document type " ).arg( xmlfile )
                                        + "'kpartgui'" ) );
    return;
  }
  // Vérifie la taille propre.
  QDomElement el = doc.documentElement();

	QString path = (metaObject()->className() == "CYMainWin") ? "" : windowTitle()+"/";

	/* Charge la barre de menu. */
	QDomNodeList menuBarDom = el.elementsByTagName( "MenuBar" );
	for ( int i = 0; i < menuBarDom.count(); ++i )
	{
    QDomElement el = menuBarDom.item( i ).toElement();

    QDomNodeList menus = el.elementsByTagName( "Menu" );
    for ( i = 0; i < menus.count(); ++i )
    {
      QDomElement el = menus.item( i ).toElement();
      QString name=el.attribute( "name" );
      QMenu *menu = mMenuBar.value(name);
      if (!menu)
      {
        menu = new QMenu( this );
        mb->addMenu( menu );
        mMenuBar.insert(name, menu);
      }
      else
        menu->clear();

      CYActionMenu *actionMenu=(CYActionMenu *)actionCollection()->action(name);
      if (actionMenu)
        menu->setTitle(actionMenu->text());
			path = menu->title()+"/";

      QDomNodeList list = el.childNodes();
      for (int i=0; i<list.count(); ++i)
      {
        QDomElement item = list.item(i).toElement();
				if (item.tagName()=="SubMenu")
					createGUIMenu(menu, item, path);
				if (item.tagName()=="Action")
					createGUIAction(menu, item, path);
        if (item.tagName()=="Separator")
          menu->addSeparator();
      }
    }
  }

  /* Charge les barres d'outils. */
  QDomNodeList toolBarDom = el.elementsByTagName( "ToolBar" );
  for ( int i = 0; i < toolBarDom.count(); ++i )
  {
    QDomElement el = toolBarDom.item( i ).toElement();

    QString name=el.attribute( "name" );
    QToolBar * toolBar = mToolBar.value(name);
    if (!toolBar)
    {
      toolBar = new QToolBar( this );
      toolBar->setWindowTitle( name );
      addToolBar(toolBar);
      mToolBar.insert(name, toolBar);
    }
    else
      toolBar->clear();
		path = toolBar->windowTitle()+"/";

    QDomNodeList list = el.childNodes();
    for (int i=0; i<list.count(); ++i)
    {
      QDomElement item = list.item(i).toElement();
			if (item.tagName()=="SubMenu")
				createGUIMenu(toolBar, item);
			if (item.tagName()=="Action")
				createGUIAction(toolBar, item);
      if (item.tagName()=="Separator")
        toolBar->addSeparator();
    }
  }

  setUpdatesEnabled( true );
  updateGeometry();
  mActionCollection->setXMLFile(xmlfile);
}

void CYWin::initHelpMenu()
{
  if (!helpMenu)
  {
    helpMenu = new QMenu( this );
    helpMenu->setTitle(tr("&Help"));
    menuBar()->addMenu( helpMenu );
  }

  helpMenu->clear();
  core->customHelpMenu(helpMenu);
  helpMenu->addSeparator();
  helpMenu->addAction( QIcon(":/pics/cylix_icon.png"), tr("&About %1").arg(qApp->applicationName()), this, SLOT(showAboutApplication()), Qt::Key_F10 );
  helpMenu->addAction( tr("About &Qt"), this, SLOT(aboutQt()) );
}

void CYWin::whatsThis()
{
  QWhatsThis::enterWhatsThisMode();
}

void CYWin::aboutQt()
{
  QMessageBox::aboutQt( this, tr("Qt Application") );
}


/*! Saisie un nom de flags d'inhibition de l'action ayant pour nom \a actionName de la fenêtre
    @param flagName Commence par un "!" pour inverser le sens d'inhibition
    \fn CYWin::setActionDisable( QString actionName, QString flagName )
 */
void CYWin::setActionDisable( QString actionName, QString flagName )
{
  mActionDisable.insert( actionName, new QString(flagName) );
}


/*! @return la liste d'actions avec les noms de leur flags d'inhibition respectifs.
    \fn CYWin::actionDisable()
 */
QHash<QString, QString*> & CYWin::actionDisable()
{
  return mActionDisable;
}


/*! Saisie une action comme non éditable
    \fn CYWin::setActionNoEdit( QString action )
 */
void CYWin::setActionNoEdit( QString action )
{
  mActionsNoEdit.append( action );
}


/*! @return \a true si l'action est éditable
    \fn CYWin::actionEdit( QString action )
 */
bool CYWin::actionEdit( QString action )
{
  for ( QStringList::Iterator it = mActionsNoEdit.begin(); it != mActionsNoEdit.end(); ++it )
  {
    if ( (*it)==action )
      return false;
  }
  return true;
}


/*! Saisie une action utilisable que en accès constructeur
    \fn CYWin::setActionNoEdit( QString action )
 */
void CYWin::setActionDesigner( QString action )
{
  mActionsDesigner.append( action );
  setActionNoEdit( action );
}

void CYWin::setWindowTitle(const QString &caption, bool modified)
{
  setPlainCaption( cyapp->makeStdCaption(caption, false, modified) );
}

void CYWin::setWindowTitle(const QString &caption)
{
  setPlainCaption( cyapp->makeStdCaption(caption, false) );
}

void CYWin::setPlainCaption(const QString &caption)
{
  QString txt = QString("%1 - %2 (%3_lib%4)").arg(cyapp->aboutData()->programName()).arg(caption).arg(cyapp->aboutData()->version()).arg(core->version());
  QMainWindow::setWindowTitle(txt);
  //#if defined Q_WS_X11
  //    NETWinInfo info( qt_xdisplay(), winId(), qt_xrootwin(), 0 );
  //    info.setName( caption.utf8().data() );
  //#endif
}

/*! @return \a true si l'action est de type constructeur
    \fn CYWin::actionEdit( QString action )
 */
bool CYWin::actionDesigner( QString action )
{
  for ( QStringList::Iterator it = mActionsDesigner.begin(); it != mActionsDesigner.end(); ++it )
  {
    if ( (*it)==action )
      return true;
  }
  return false;
}

CYUserAction * CYWin::userAction(QString name)
{
  CYUserActionCollection *collection = core->user()->group()->actionCollections().value(this->metaObject()->className());
  return collection->actions().value(name);
}


/*! Active ou non suivant \a val l'aintégration du bandeau de messagerie dans la fenêtre.
    \fn CYWin::setEnableEventsPanel(bool val)
 */
void CYWin::setEnableEventsPanel(bool val)
{
  mEnableEventsPanel=val;
  core->addEventsPanel(this);
}


CYAction *CYWin::action( const char* name )
{
  CYAction *a = actionCollection()->action( name );
  return a;
}

CYActionCollection *CYWin::actionCollection()
{
  if (!mActionCollection)
    mActionCollection = new CYActionCollection( this, QString("%1_COLLECTION").arg(objectName()).toUtf8() );
  return mActionCollection;
}


//QPtrListIterator<QToolBar> CYWin::toolBarIterator()
//{
//    toolbarList.clear();
//    QPtrList<QToolBar> lst;
//    for ( int i = (int)QMainWindow::DockUnmanaged; i <= (int)DockMinimized; ++i ) {
//        lst = toolBars( (ToolBarDock)i );
//        for ( QToolBar *tb = lst.first(); tb; tb = lst.next() ) {
//            toolbarList.append( tb );
//        }
//    }
//    return QPtrListIterator<QToolBar>( toolbarList );
//}

void CYWin::registerRecentURL(const QUrl& url)
{
  QString fileName = url.url();
  Q_UNUSED(fileName)
  // TODO QT5
//  if ( mRecentFiles.indexOf( fileName ) != mRecentFiles.end() )
//    return;

//  mRecentFiles.push_back( fileName );
//  if ( mRecentFiles.count() > MAX_RECENTFILES )
//    mRecentFiles.pop_front();

//  updateRecentFilesMenu();
}

void CYWin::updateRecentFilesMenu()
{
  // TODO QT5

  //  if (!mFileMenu)
  //    return;

  //  for ( int i = 0; i < MAX_RECENTFILES; ++i )
  //  {
  //    if ( mFileMenu->actions( i ) )
  //      mFileMenu->removeItem( i );
  //    if ( i < int(mRecentFiles.count()) )
  //      mFileMenu->insertItem( QString( "&%1 %2" ).
  //                             arg( i + 1 ).arg( mRecentFiles[i] ),
  //                             this, SLOT( fileOpenRecent(int) ),
  //                             0, i );
  //  }
}

bool CYWin::okToClear()
{
  //  if ( mChanged )
  //  {
  //    QString msg;
  //    if ( m_filename.isEmpty() )
  //      msg = "Unnamed chart ";
  //    else
  //      msg = QString( "Chart '%1'\n" ).arg( m_filename );
  //    msg += "has been changed.";

  //    int x = QMessageBox::information( this, "Chart -- Unsaved Changes",
  //                                      msg, "&Save", "Cancel", "&Abandon",
  //                                      0, 1 );
  //    switch( x )
  //    {
  //      case 0: // Save
  //        fileSave();
  //        break;
  //      case 1: // Cancel
  //      default:
  //        return false;
  //      case 2: // Abandon
  //        break;
  //    }
  //  }

  return true;
}

void CYWin::fileOpenRecent( int index )
{
  Q_UNUSED(index)
  // TODO QT5
  //    if ( !okToClear() )
  //        return;

  //    load( mRecentFiles[index] );
}

void CYWin::addStateActionEnabled(const QString& state,
                                  const QString& action)
{
  StateChange stateChange = getActionsToChangeForState(state);

  stateChange.actionsToEnable.append( action );

  if (!m_actionsStateMap.contains(state))
    m_actionsStateMap.insert(state, stateChange);
}


void CYWin::addStateActionDisabled(const QString& state,
                                   const QString& action)
{
  StateChange stateChange = getActionsToChangeForState(state);

  stateChange.actionsToDisable.append( action );

  m_actionsStateMap.insert(state, stateChange);
}


CYWin::StateChange CYWin::getActionsToChangeForState(const QString& state)
{
  return m_actionsStateMap[state];
}


void CYWin::stateChanged(const QString &newstate, CYWin::ReverseStateChange reverse)
{
  Q_UNUSED(newstate)
  Q_UNUSED(reverse)
//  StateChange stateChange = getActionsToChangeForState(newstate);

//  bool settrue = (reverse == StateNoReverse);
//  bool setfalse = !settrue;

//  // Enable actions which need to be enabled...
//  //
//  for ( QStringList::Iterator it = stateChange.actionsToEnable.begin();
//        it != stateChange.actionsToEnable.end(); ++it ) {

//    CYAction *action = actionCollection()->action((*it).toUtf8());
//    if (action) action->setEnabled(settrue);
//  }

//  // and disable actions which need to be disabled...
//  //
//  for ( QStringList::Iterator it = stateChange.actionsToDisable.begin();
//        it != stateChange.actionsToDisable.end(); ++it ) {

//    CYAction *action = actionCollection()->action((*it).toUtf8());
//    if (action) action->setEnabled(setfalse);
//  }

}

