//
// C++ Implementation: cymainwin
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
// QT
#include <QStringList>
#include <QDir>
// CYLIBS
#include "cymainwin.h"
#include "cycore.h"
#include "cyproject.h"
#include "cyuser.h"
#include "cychangeaccess.h"
#include "cyuseradminedit.h"
#include "cyuseradminedit2.h"
#include "cyapplication.h"
#include "cyaboutdata.h"
#include "cyactionmenu.h"
#include "cyglobal.h"
#include "cymessagebox.h"

CYMainWin::CYMainWin(QWidget *parent, const QString &name, int index, const QString &label)
  : CYWin(parent, name, index, label, Qt::Window)
{
  mLanguageMenu = 0;
  mAutoClose = false;
  new CYActionMenu( tr("Languages"), 0, actionCollection(), "languagesMenu");

  core->setMainWin( this );
}


CYMainWin::~CYMainWin()
{
}


/*! Autorise la fermeture automatique de la fenêtre sans demande de confirmation de l'utilisateur.
    \fn CYMainWin::setAutoClose( bool val )
 */
void CYMainWin::setAutoClose( bool val )
{
  mAutoClose = val;
}


/*! Passe en accès protégé.
    \fn CYMainWin::protectedAccess()
 */
void CYMainWin::protectedAccess()
{
  core->enableProtectedAccess();
  setWindowTitle(title());
}


/*! @return le titre de la fenêtre.
    \fn CYMainWin::title()
 */
QString CYMainWin::title()
{
  QString testDirPath = (core->project()) ? core->project()->testDirPath() : "";

  if (core->user())
    return tr("Test: %1 (Access: %2)").arg(testDirPath).arg(core->user()->title());
  else
    return tr("Test: %1").arg(testDirPath);
}

/*! Change d'accès
    \fn CYMainWin::changeAccess()
 */
void CYMainWin::changeAccess()
{
  CYChangeAccess* dlg = new CYChangeAccess(this, "CYChangeAccess");
  Q_CHECK_PTR(dlg);
  dlg->exec();
  setWindowTitle(title());
}


void CYMainWin::changePassword()
{
  core->changeUserPassword();
  setWindowTitle(title());
}


/*! Ouvre la boîte d'administration utilisateur
    \fn CYMainWin::userAdmin()
 */
void CYMainWin::userAdmin()
{
  if (core->pyramidalUserAdmin())
  {
    CYUserAdminEdit2 *dlg = new CYUserAdminEdit2( this, "userAdminEdit" );
    dlg->exec();
  }
  else
  {
    CYUserAdminEdit *dlg = new CYUserAdminEdit( this, "userAdminEdit" );
    dlg->exec();
  }
}

void CYMainWin::readProperties(QSettings *cfg)
{
  CYWin::readProperties(cfg);
}

void CYMainWin::saveProperties(QSettings *cfg)
{
  core->historyAccess();
  CYWin::saveProperties(cfg);
  core->saveOSUserProperties(cfg);
}

void CYMainWin::createLanguageMenu(QMenu *menu)
{
  menu->clear();
  if (mLanguageMenu)
    disconnect(mLanguageMenu, SIGNAL(aboutToShow()), this, SLOT(refreshLanguageMenu()));
  mLanguageMenu = menu;

  mLanguageActions.clear();

  QActionGroup* langGroup = new QActionGroup(menu);
  langGroup->setExclusive(true);

  connect(langGroup, SIGNAL (triggered(QAction *)), this, SLOT (slotLanguageChanged(QAction *)));

  // format de la langue system
  QString defaultLocale = QLocale::system().name(); // e.g. "de_DE"
  defaultLocale.truncate(defaultLocale.lastIndexOf('_')); // e.g. "de"

  QString langPath = cyapp->installLanguagePath();
  QDir dir(langPath);
  QString qmFiles = QString("%1_*.qm").arg(cyapp->aboutData()->programName());
  QStringList fileNames = dir.entryList(QStringList(qmFiles.toUtf8()));

  if (fileNames.isEmpty())
    return;

  for (int i = 0; i < fileNames.size()+1; ++i)
  {
    // récupère la localisation extraite de filename
    QString locale;
    if (i<fileNames.size())
    {
      locale = fileNames[i]; // "TranslationExample_de.qm"
      locale.truncate(locale.lastIndexOf('.')); // "TranslationExample_de"
      locale.replace(cyapp->applicationName(),""); // cylix-1008527_BPC_fr => _fr
      locale.remove(0, locale.indexOf('_') + 1); // "de"
    }
    else
      locale = "en"; // language natif
    QString icon_name=locale;
    if (icon_name.endsWith("_CN"))
      icon_name = "cn";
    QString ressource = QString(":/icons/languages/%1.png").arg(icon_name);
    QIcon icon = QIcon(ressource);
    QString lang = QLocale::languageToString(QLocale(locale).language());
    QAction *action = new QAction(icon, lang, this);
    action->setCheckable(true);
    action->setData(locale);
    mLanguageActions.insert(locale, action);
    menu->addAction(action);
    langGroup->addAction(action);

    // définit les traducteurs par défaut et la langue active
    if (defaultLocale == locale)
    {
      action->setChecked(true);
    }
  }
  connect(mLanguageMenu, SIGNAL(aboutToShow()), this, SLOT(refreshLanguageMenu()));
}

// Appelé à chaque fois, quand une entrée du menu de langue de menu est appelé
void CYMainWin::slotLanguageChanged(QAction* action)
{
  if(0 != action)
  {
    // charger le langage dépendant du contenu de l'action
    core->loadLanguage(action->data().toString());
    setWindowIcon(action->icon());
    core->updateGUI();
    CYMessageBox::information(this, tr("The new language will be taken into account the next time the application will be started."));
  }
}

void CYMainWin::refreshLanguageMenu()
{
  QHashIterator<QString, QAction *> i(mLanguageActions);
  while (i.hasNext())
  {
    i.next();
    bool ok = (i.key()==core->currentLanguage()) ? true : false;
    i.value()->setChecked(ok);
  }
}


void CYMainWin::changeEvent(QEvent* event)
{
  if(0 != event)
  {
    switch(event->type())
    {
    // cet événement est envoyé si un traducteur est chargé
    case QEvent::LanguageChange:
      //    ui.retranslateUi(this);
      break;

      // cet événement est envoyé, si la langue du système change
    case QEvent::LocaleChange:
    {
      QString locale = QLocale::system().name();
      locale.truncate(locale.lastIndexOf('_'));
      core->loadLanguage(locale);
      break;
    }
    default:
      // pas de filtre supplémentaire sur les autres événements
      break;
    }
  }
  CYWin::changeEvent(event);
}
