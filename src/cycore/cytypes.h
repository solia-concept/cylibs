/***************************************************************************
                          commun_lib.h  -  description
                             -------------------
    debut                  : ven fev 7 2003
    copyright              : (fr) 2003 par Gerald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                            !!!!! ATTENTION !!!!!
                    Ce fichier est commun aux bibliotheques
                 utiles au developpement d'un superviseur CYLIX
                et celles utiles au developpement d'un regulateur

 ***************************************************************************/
#ifndef CYTYPES_H
#define CYTYPES_H

#define TYPES_VERSION 5.43

//#define FLG_S8 // A décommmenter pour les anciennes applications livrées avec des flags en s8
// Depuis la version 5.37 les flags utilisent, comme en POSIX C, un int à la place des s8 afin d'optimiser les temps de traitements.
// Ce typedef permet de faciliter la compatibilité avec les anciens RN DOS pour lesquels une optimisation en mémoire était faite à l'inverse avec un s8.

// masque pour accès bit
#define MASK(i)   (M00 << (i))
#define M00       0x0001
#define M01       0x0002
#define M02       0x0004
#define M03       0x0008
#define M04       0x0010
#define M05       0x0020
#define M06       0x0040
#define M07       0x0080
#define M08       0x0100
#define M09       0x0200
#define M10       0x0400
#define M11       0x0800
#define M12       0x1000
#define M13       0x2000
#define M14       0x4000
#define M15       0x8000


//=========================================================================
// alignement des structures DOS
#define ALI8_32(i)   (((i)%4) ? ((i)+(4-((i)%4))) : (i))  // alignement des structures  char 8 bits
#define ALI16_32(i)  (((i)%2) ? ((i)+(2-((i)%2))) : (i))  // alignement des structures  mot 16 bits

#ifndef __KERNEL__
  #ifdef OS_DOS
    typedef signed char s8;
    typedef unsigned char u8;

    typedef signed int s16;
    typedef unsigned int u16;

    typedef signed long s32;
    typedef unsigned long u32;

    typedef double f64;
  #else  // OS_DOS
    typedef char s8;
    typedef unsigned char u8;

    typedef signed short s16;
    typedef unsigned short u16;

    typedef signed int s32;
    typedef unsigned int u32;

    #if defined(__MINGW64__)
    typedef signed long long s64;
    typedef long long u64;
    #else
    typedef signed long s64;
    typedef unsigned long u64;
    #endif// __MINGW64__

    typedef double f64;

  #endif // OS_DOS
#endif // __KERNEL__

typedef float f32;

#ifdef FLG_S8
  typedef s8 flg;
#else
  typedef int flg;
#endif

/** Typedef d'une structure d'un mot de 16 bits. */

typedef struct
{
  s16 T00 :1;
  s16 T01 :1;
  s16 T02 :1;
  s16 T03 :1;
  s16 T04 :1;
  s16 T05 :1;
  s16 T06 :1;
  s16 T07 :1;
  s16 T08 :1;
  s16 T09 :1;
  s16 T10 :1;
  s16 T11 :1;
  s16 T12 :1;
  s16 T13 :1;
  s16 T14 :1;
  s16 T15 :1;
} t_field;

/** Typedef d'une structure d'un mot en hexadecimal. */
typedef struct
{
  /** octet de poids faible. */
  u8 lsb;
  /** octet de poids fort. */
  u8 msb;
} t_byte;

/** Typedef d'un mot. */
typedef union
{
  /** mot en binaire. */
  t_field B;
  /** mot en hexadecimal. */
  t_byte byte;
  /** mot en decimal. */
  u16 w;
} t_word;

typedef union
{
  t_byte byte;
  u8  vu8[2];
  s8  vs8[2];
  u16 vu16;
  s16 vs16;
}w16;

typedef union
{
  u8  vu8[4];
  s8  vs8[4];
  u16 vu16[2];
  s16 vs16[2];
  s32 vs32;
  u32 vu32;
}w32;

/** Typedef d'un compteur u32. */
typedef struct
{
  /** compteur fixe sans remise a zero. */
  u32 val;
  /** compteur avec remise a zero. */
  u32 tmp;
} t_cu32;

#ifndef OS_DOS
/** Typedef d'un compteur u64. */
typedef struct
{
  /** compteur fixe sans remise à zero. */
  u64 val;
  /** compteur avec remise à zero. */
  u64 tmp;
} t_cu64;
#endif

/** Typedef de sauvegarde des points haut et bas d'un calibrage ADC 16bits (bit de parité inclu)/unité capteur (ex. 0-10Bar). */
typedef struct
{
  /** valeurs mini en unite capteur. */
  f32 affmin;
  /** valeurs maxi en unite capteur. */
  f32 affmax;
  /** valeurs mini en points. */
  s16 adcmin;
  /** valeurs maxi en points. */
  s16 adcmax;
} t_calana16;

/** Typedef de sauvegarde des points haut et bas d'un calibrage ADC 32bits (bit de parité inclu)/unité capteur (ex. 0-10Bar). */
typedef struct
{
  /** valeurs mini en unite capteur. */
  f32 affmin;
  /** valeurs maxi en unite capteur. */
  f32 affmax;
  /** valeurs mini en points. */
  s32 adcmin;
  /** valeurs maxi en points. */
  s32 adcmax;
} t_calana32;

/** Typedef de sauvegarde des points haut et bas d'un calibrage signal numérique/unité capteur (ex.0-10Bar/0-10Bar). */
typedef struct
{
  /** valeurs mini en unite capteur. */
  f32 affmin;
  /** valeurs maxi en unite capteur. */
  f32 affmax;
  /** valeurs mini capteur (signal électrique). */
  f32 captmin;
  /** valeurs maxi capteur (signal électrique). */
  f32 captmax;
} t_calnum32;

/** Typedef de sauvegarde des points de calibration metrologie pour capteur non lineaires. */
typedef struct
{
  /** valeur ADC ou numerique */
  f32 val_capteur;
  /** valeur associee (en unite capteur) */
  f32 val_etalon;
} t_cal_metro;


/** Typedef de saisie controle tolerances en temps ou en cycles. */
typedef struct
{
  /** temps / cycles inhibition avant controle. */
  u32 inh;
  /** temps / cycles maximum de stabilisation avant controle. */
  u32 stab;
  /** temps / cycles maximum hors tolerance d'alerte avant defaut. */
  u32 ale;
  /** Defaut si temps maxi hors tolerance d'alerte atteint. */
  flg  fl_arret_ale;
  /** Defaut si hors tolerance de defaut. */
  flg  fl_arret_def;
#ifdef FLG_S8
  flg  ali8_32[2];
#endif
} t_ct;

/** Typedef de saisie d'un mouvement. */
typedef struct
{
  /** Tempo Apparition Aller. */
  u32 taa;
  /** Tempo Apparition Retour. */
  u32 tar;
  /** Tempo Perte Aller. */
  u32 tpa;
  /** Tempo Perte Retour. */
  u32 tpr;

  /** Controle alerte/defaut par tempo Avec Capteur Aller. */
  flg  aca;
  /** Controle alerte/defaut par tempo Avec Capteur Retour. */
  flg  acr;
  /** Defaut Capteur Aller (sinon alerte). */
  flg  dca;
  /** Defaut Capteur Retour (sinon alerte). */
  flg  dcr;
} t_mv;


/** Type de PID possible */
enum cypid_type
   {
    /** Controle type PID par valeur de facteur integral/derivee et fl_derivmes (compatibilite ancienne PID)*/
    PID_TYPE_FAULT = 0,
    /** Proportionnel uniquement */
    PID_TYPE_P,
    /** Proportionnel Integral */
    PID_TYPE_PI,
    /** Proportionnel Derivee */
    PID_TYPE_PD,
    /** Proportionnel, Derivee et Integral */
    PID_TYPE_PID,
    /** Integral */
    PID_TYPE_I,
    };

/** Type du choix de derive possible pour calcul PID. Non utilise si type de PID vaut PID_TYPE_FAULT */
enum cypid_type_deriv
   {
    /** derive / ecart mesure - consigne */
    D_ECART = 0,
    /** derive / mesure */
    D_MESURE,
    /** derive / consigne */
    D_CONSIGNE,
    };

/** Typedef d'un PID. */
typedef struct
{
  /** coefficient proportionnel ou Bande proportionnelle. */
  f32 P;
  /** coefficient integral. */
  f32 I;
  /** facteur integral/derivee. */
  f32 I_D;
  /** puissance maxi. */
  f32 p_max;
  /** puissance mini. */
  f32 p_min;
  /** controle du PID. 0=P 1=PI 2=PID 3=I  */
  s16 type;
  /** controle du PID. 0=D/ecart 1=D/mesure 2=D/consigne.  */
  s16 type_deriv;
  /** si 1 alors derive sur mesure sinon sur ecart (compatibilite ancienne PID). */
  flg fl_derivmes;
  /** si 1 alors consigne en rampe. */
  flg fl_cons_rmp;
  /** si 1 alors bloque l'integrale en rampe. */
  flg fl_blq_irmp;
  /** si 1 alors en mode Bande proportionnelle (sinon coeff. proportionnel). */
  flg fl_bp;

} t_pid;


//=============================================================
// gestion profibus

enum e_pbtype   {E_TOR=0,    // entree tor  NE PAS CHANGER L'ORDRE (PAIR/IMPAIR)
                 S_TOR,      // sortie tor
                 E_OCT,      // entree 8 bits
                 S_OCT,      // sortie 8 bits
                 E_MOT,      // entree 16 bits
                 S_MOT,      // sortie 16 bits
                 E_F32,      // entree float
                 S_F32,      // sortie float
                 NB_PBTYPE_ES};

enum e_pbtype_adr {ADR_ASI=0,    // type adressage ASi
                   ADR_PBUS,     // type adressage Profibus
                   NB_PBTYPE_ADR};


typedef struct  // correspondance zone memoire profibus base de donnee
{
  void *ptr;    // pointeur flag (process)
  s16  num_oct,num_bit;
  void *adr_pb; // adresse profibus
  u8   mask;
}t_pb_bd;

typedef struct  // descripteur base de donnee profibus
{
  s16       num_reg;                 // numero regulateur (necessaire a la construction du repere electrique)
  s16       adresse;                 // adresse equipement
  s16       type_adr;                // type adresse asi ou profibus
  s16       I_addr;                  // adress (octet) table d'entree profibus (voir config SyCon)
  s16       I_len;                   // lenght (octet) table d'entree profibus (voir config SyCon)
  s16       O_addr;                  // adress (octet) table de sortie profibus (voir config SyCon)
  s16       O_len;                   // lenght (octet) table de sortie profibus (voir config SyCon)
  t_pb_bd   *pbbd[NB_PBTYPE_ES];     // table de pointeur sur table des donnees/nb element
  s16       nb_element[NB_PBTYPE_ES];// nombre d'element
  char      *pbl;                    // pointeur sur zone memoire PC lecture
  char      *pbe;                    // pointeur sur zone memoire PC ecriture
}t_pb_desc;


//================================================================
// TRANSFERT DE FICHIERS PAR R422

#define R422FC_WAIT     0  // attente du regulateur
#define R422FC_REBOOT   1  // demande de reboot du regulateur
#define R422FC_WRITE    2  // demande d'ecriture fichier (spv=>rn)
#define R422FC_READ     3  // demande de lecture fichier (rn=>spv)
#define R422FC_END      4  // sortie sans reboot de r422f.exe
#define R422FC_TRAME_OK 5  // trame transferee sur spv
#define NB_R422F_CMD    6

#define R422F_PATH_L      80
#define R422F_SIZE_BLOCK  512

#define R422FI_WAIT       0  // RIEN
#define R422FI_TRAME_OK   1  // trame transferee sur rn
#define R422FI_DEL_FILE   2  // impossible d'effacer le fichier
#define R422FI_MAKE_FILE  3  // impossible de creer le fichier
#define R422FI_NUM_BLOCK  4  // erreur numero block
#define R422FI_SIZE_FILE  5  // erreur taille fichier en fin de transfert
#define R422FI_NODIR      6  // repertoire innexistant
#define R422FI_NOFILE     7  // fichier innexistant
#define R422FI_OPENFILE   8  // impossible d'ouvrir le fichier
#define NB_R422F_INFO     9

struct s_r422f_cmd    // commandes transferts superviseur regulateur (spv->rn)
{
  s8   cmd;                           // commandes
  s8   path_name[R422F_PATH_L];       // chemin RN
  s8   file_name[13];                 // nom fichier en cours d'ecriture RN "12345678.123"+"\0"
  s8   ali8_32[2];
  s32  file_size;                     // taille du fichier en octets
};

struct s_r422f_info   // info transferts superviseur regulateur (rn->spv)
{
  s8   version[40];                   // version du r422f.exe
  s8   info;                          // info
  s8   file_name[13];                 // nom fichier en cours de lecture RN "12345678.123"+"\0"
  s8   ali8_32[2];
  s16  num_block;                     // numero du block transfere
  s16  ali16_32[1];
  s32  file_size;                     // taille du fichier en octets
};

struct s_r422f_write // Donnees en ecriture superviseur -> regulateur (spv->rn)
{
  s16 num_block;                // numero du block transfere
  s16 size_block;               // taille du block transfere en octets
  s8  data[R422F_SIZE_BLOCK];   // donnees ()
};

struct s_r422f_read // Donnees en lecture superviseur -> regulateur (rn->spv)
{
  s16 num_block;                // numero du block transfere
  s16 size_block;               // taille du block transfere en octets
  s8  data[R422F_SIZE_BLOCK];   // donnees ()
};

// Definition des structures passes sur le reseau
#define ST_R422F_CMD     0
#define ST_R422F_INFO    1
#define ST_R422F_READ    2
#define ST_R422F_WRITE   3
#define NB_ST_R422F      4

#ifdef RESEAU
  struct s_r422f_cmd     r422f_cmd;
  struct s_r422f_info    r422f_info;
  struct s_r422f_read    r422f_read;
  struct s_r422f_write   r422f_write;
#else
  extern struct s_r422f_cmd     r422f_cmd;
  extern struct s_r422f_info    r422f_info;
  extern struct s_r422f_read    r422f_read;
  extern struct s_r422f_write   r422f_write;
#endif

#endif // CYTYPES_H

//=========================================================================
// FIN fichier
