/***************************************************************************
                          cyprintableinfo.h  -  description
                             -------------------
    begin                : mer jan 21 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYPRINTABLEINFO_H
#define CYPRINTABLEINFO_H

// QT
#include <qobject.h>

class CYData;

/** @short Impression de la valeur d'une donnée.
  * @author LE CLÉACH Gérald
  */

class CYPrintableInfo : public QObject
{
  Q_OBJECT
public: 
  CYPrintableInfo(QObject *parent, const QString &name, QString dataName, QString text=0);
  ~CYPrintableInfo();

  /** @return la chaîne de carcatères imprimable. */
  QString toString();
  /** Saisie du texte associé à la valeur. */
  void setText(QString text) { mText = text; }

protected: // Protected attributes
  /** Donnée. */
  CYData *mData;
  /** Text à associé à la valeur. */
  QString mText;
};

#endif
