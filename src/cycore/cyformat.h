/***************************************************************************
                          cyformat.h  -  description
                             -------------------
    début                  : mar jui 29 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYFORMAT_H
#define CYFORMAT_H

#include <qobject.h>
// CYLIBS
#include "cymaths.h"

/** @short Format d'affichage d'une donnée.
  * @author Gérald LE CLEACH
  */

class CYFormat : public QObject
{

public:
  /** Construit un format d'affichage.
    * @param parent         Parent.
    * @param name           Nom.
    * @param unit           Unité.
    * @param phys           Correspondance physique.
    * @param min            Valeur mini par défaut.
    * @param max            Valeur maxi par défaut.
    * @param dec            Nombre de chiffres après la virgule.
    * @param prec           Arrondi.
    * @param slowSize       Taille du buffer d'acquisition lente.
    * @param slowTime       Période d'acquisition lente en ms (pas du buffer).
    * @param fastSize       Taille du buffer d'acquisition rapide.
    * @param fastTime       Période d'acquisition rapide en ms (pas du buffer).
    * @param fastSample     Taille de l'échantillon d'acquisition rapide. 
    * @param burstsSize  Taille du buffer d'acquisition par rafales.
    * @param burstsRatio Rapport période d'acquisition rapide / période d'acquisition par rafales. */
  CYFormat(QObject *parent, const QString &name, QString unit, QString phys,
           double min, double max, int dec=0, double prec=0,
           uint slowSize=0, uint slowTime=0,
           uint fastSize=0, float fastTime=0, short fastSample=0,
           uint burstsSize=0, short burstsRatio=1);

  /** Construit un format d'affichage exponentiel.
    * @param parent         Parent.
    * @param name           Nom.
    * @param unit           Unité.
    * @param phys           Correspondance physique.
    * @param min            Valeur mini par défaut ("2.58e-24").
    * @param max            Valeur maxi par défaut ("6.85e30").
    * @param dec            Nombre de chiffres après la virgule.
    * @param prec           Arrondi.
    * @param slowSize       Taille du buffer d'acquisition lente.
    * @param slowTime       Période d'acquisition lente en ms (pas du buffer).
    * @param fastSize       Taille du buffer d'acquisition rapide.
    * @param fastTime       Période d'acquisition rapide en ms (pas du buffer).
    * @param fastSample     Taille de l'échantillon d'acquisition rapide. 
    * @param burstsSize  Taille du buffer d'acquisition par rafales.
    * @param burstsRatio Rapport période d'acquisition rapide / période d'acquisition par rafales. */
  CYFormat(QObject *parent, const QString &name, QString unit, QString phys,
           QString min, QString max, int dec=0, double prec=0,
           uint slowSize=0, uint slowTime=0,
           uint fastSize=0, float fastTime=0, short fastSample=0,
           uint burstsSize=0, short burstsRatio=1);

  ~CYFormat();
  
  /** Saisie l'unité. */
  void setUnit(const QString u) { mUnit = u; }
  /** @return l'unité */
  QString unit() { return mUnit; }

  /** Saisie le type physique. */
  void setPhysical(const QString p) { mPhysical = p; }
  /** @return la correspondance physique. */
  QString physical() { return mPhysical; }
  /** @return le type physique avec l'unité. */
  QString physicalType();  

  /** Saisie la précision d'affichage si la valeur est numérique.
    * @param precision  : 0.2, 1.0, 5.0, 10.0, 1, 5, 10 etc... */
  void setPrecision(const double precision) { mPrecision = precision; }
  /** @return la précision. */
  double precision() { return mPrecision; }

  /** Saisie le nombre de chiffres total. */
  void setNbDigit(int nb);
  /** @return le nombre de chiffres total. */
  int nbDigit() { return mNbDigit; }

  /** Saisie le nombre de chiffres après la virgule. */
  void setNbDec(int nb);
  /** @return le nombre de chiffres après la virgule. */
  int nbDec() { return mNbDec; }

  /** Saisie la valeur mini par défaut. */
  void setMin(const double val) { mMin = val; }
  /** @return la valeur mini par défaut. */
  double min() { return cyRoundSup(mMin, precision()); }

  /** Saisie la valeur maxi par défaut. */
  void setMax(const double val) { mMax = val; }
  /** @return la valeur maxi par défaut. */
  double max() { return cyRoundInf(mMax, precision()); }

  /** Saisie la taille du buffer de l'acquisition lente ou rapide en fonction de \a slow. */
  void setBufferSize(uint size, bool slow=true)
  {
    if (slow) mSlowBufferSize = size;
    else      mFastBufferSize = size;
  }
  /** @return la taille du buffer de l'acquisition lente ou rapide en fonction de \a slow. */
  uint bufferSize(bool slow=true) { return (slow) ? mSlowBufferSize : mFastBufferSize; }

  /** @return la taille du buffer de l'acquisition par rafales. */
  uint burstsBufferSize() { return mBurstsBufferSize; }
  /** @return le rapport période d'acquisition rapide / période d'acquisition par rafales. */
  uint burstsRatio() { return mBurstsRatio; }

  /** Saisie la période d'acquisition lente ou rapide en fonction de \a slow en ms (pas du buffer). */
  void setAcquisitionTime(float time, bool slow=true)
  {
    if (slow) mSlowAcquisitionTime = time;
    else      mFastAcquisitionTime = time;
  }
  /** @return la période d'acquisition lente ou rapide en fonction de \a slow en ms (pas du buffer). */
  float acquisitionTime(bool slow=true) { return (slow) ? mSlowAcquisitionTime : mFastAcquisitionTime; }

  /** Saisie la taille de l'échantillon d'acquisition rapide (nombre de valeurs à la fois). */
  void setSampleSize(short size)
  {
    mFastSampleSize = size;
  }
  /** @return la taille de l'échantillon d'acquisition lente ou rapide en fonction de \a slow (nombre de valeurs à la fois). */
  short sampleSize(bool slow=true) { return (slow) ? 1 : mFastSampleSize; }

  /** Saisie la chaîne de caractères d'affichage de défaut. */
  void setStringDef(QString txt) { mTxtDef = txt; }
  /** @return la chaîne de caractères d'affichage de défaut. */
  QString stringDef() { return mTxtDef; }

  /** Saisir \a true s'il s'agit d'un format exponentiel. */
  void setExponential(bool val);
  /** @return \a true s'il s'agit d'un format exponentiel. */
  bool isExponential();

  /** Copie les paramètres du format \a src. */
  void copy(CYFormat *src);

protected: // Protected methods
  /** Construit une chaine de caractères d'affichage de la donnée en cas de défaut. */
  virtual void initTxtDef();

private: // Private attributes
  /** Unité */
  QString mUnit;
  /** Correspondance physique. */
  QString mPhysical;
  /** Valeur mini par défaut. */
  double mMin;
  /** Valeur maxi par défaut. */
  double mMax;
  /** Nombre de chiffres total. */
  int mNbDigit;
  /** Nombre de chiffres après la virgule. */
  int mNbDec;
  /** Précision. */
  double mPrecision;
  /** Chaîne de caractères d'affichage de défaut. */
  QString mTxtDef;

  /** Taille du buffer d'acquisition lente. */
  float mSlowBufferSize;
  /** Période d'acquisition lente en ms (pas du buffer). */
  float mSlowAcquisitionTime;

  /** Taille du buffer d'acquisition rapide. */
  uint mFastBufferSize;
  /** Période d'acquisition rapide en ms (pas du buffer). */
  float mFastAcquisitionTime;
  /** Taille de l'échantillon d'acquisition rapide (nombre de valeurs à la fois). */
  short mFastSampleSize;

  /** Taille du buffer d'acquisition par rafales. */
  uint mBurstsBufferSize;
  /** Rapport période d'acquisition rapide / période d'acquisition par rafales. */
  uint mBurstsRatio;

  /** Vaut \a true si c'est un format exponentiel. */
  bool mExponential;

  
};

#endif
