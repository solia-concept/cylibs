/***************************************************************************
                          cysplashscreen.h  -  description
                             -------------------
    begin                : lun aoû 2 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYSPLASHSCREEN_H
#define CYSPLASHSCREEN_H

// QT
#include <qobject.h>
#include <qwidget.h>
#include <qlabel.h>

/** @short Ecran de démarrage/d'arrêt du soft.
  * @author LE CLÉACH Gérald
  */

class CYSplashScreen : public QObject
{
   Q_OBJECT
public:
  /** @param start Vaut \a true s'il s'agit de l'écran de démarrage et non d'arrêt. */
  CYSplashScreen(bool start=true);
  ~CYSplashScreen();

public slots: // Public slots
  /** Affiche un message texte. */
  void showMessage(const QString &message);

protected: // Protected attributes
  /** Widget. */
  QWidget *mSplash;
  /** Etiquette de messagerie. */
  QLabel *mMessage;
};

#endif
