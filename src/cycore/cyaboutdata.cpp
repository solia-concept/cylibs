/***************************************************************************
                      cyaboutdata.cpp  -  description
                             -------------------
    début                  : mar jan 27 2015
    copyright              : (fr) 2015 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// QT
#include <QFile>
#include <QTextStream>
#include <QStringList>
// CYLIBS
#include "cyaboutdata.h"

QString
CYAboutPerson::name() const
{
   return QString::fromUtf8(mName);
}

QString
CYAboutPerson::task() const
{
   if (mTask && *mTask)
      return QObject::QObject::tr(mTask);
   else
      return QString();
}

QString
CYAboutPerson::emailAddress() const
{
   return QString::fromUtf8(mEmailAddress);
}


QString
CYAboutPerson::webAddress() const
{
   return QString::fromUtf8(mWebAddress);
}


CYAboutTranslator::CYAboutTranslator(const QString & name,
                const QString & emailAddress)
{
    mName=name;
    mEmail=emailAddress;
}

QString CYAboutTranslator::name() const
{
    return mName;
}

QString CYAboutTranslator::emailAddress() const
{
    return mEmail;
}

class CYAboutDataPrivate
{
public:
    CYAboutDataPrivate()
        : translatorName("_: NAME OF TRANSLATORS\nYour names")
        , translatorEmail("_: EMAIL OF TRANSLATORS\nYour emails")
        , productName(0)
        , programLogo(0)
        , customAuthorTextEnabled(false)
        , mTranslatedProgramName( 0 )
        {}
    ~CYAboutDataPrivate()
        {
             delete programLogo;
             delete[] mTranslatedProgramName;
        }
    const char *translatorName;
    const char *translatorEmail;
    const char *productName;
    QImage* programLogo;
    QString customAuthorPlainText, customAuthorRichText;
    bool customAuthorTextEnabled;
    const char *mTranslatedProgramName;
};



CYAboutData::CYAboutData( const char *appName,
                        const char *programName,
                        const char *version,
                        const char *shortDescription,
      int licenseType,
      const char *copyrightStatement,
      const char *text,
      const char *homePageAddress,
      const char *bugsEmailAddress
      ) :
  mProgramName( programName ),
  mShortDescription( shortDescription ),
  mLicenseKey( licenseType ),
  mCopyrightStatement( copyrightStatement ),
  mOtherText( text ),
  mHomepageAddress( homePageAddress ),
  mBugEmailAddress( bugsEmailAddress ),
  mLicenseText (0)
{
  mVersion = version;

   d = new CYAboutDataPrivate;

   if( appName ) {
     const char *p = strrchr(appName, '/');
     if( p )
   mAppName = p+1;
     else
   mAppName = appName;
   } else
     mAppName = 0;
}

CYAboutData::~CYAboutData()
{
    if (mLicenseKey == License_File)
        delete [] mLicenseText;
    delete d;
}

void
CYAboutData::addAuthor( const char *name, const char *task,
        const char *emailAddress, const char *webAddress )
{
  mAuthorList.append(CYAboutPerson(name,task,emailAddress,webAddress));
}

void
CYAboutData::addCredit( const char *name, const char *task,
        const char *emailAddress, const char *webAddress )
{
  mCreditList.append(CYAboutPerson(name,task,emailAddress,webAddress));
}

void
CYAboutData::setTranslator( const char *name, const char *emailAddress)
{
  d->translatorName=name;
  d->translatorEmail=emailAddress;
}

void
CYAboutData::setLicenseText( const char *licenseText )
{
  mLicenseText = licenseText;
  mLicenseKey = License_Custom;
}

void
CYAboutData::setLicenseTextFile( const QString &file )
{
  mLicenseText = qstrdup(QFile::encodeName(file));
  mLicenseKey = License_File;
}

void
CYAboutData::setAppName( const char *appName )
{
  mAppName = appName;
}

void
CYAboutData::setProgramName( const char* programName )
{
  mProgramName = programName;
  translateInternalProgramName();
}

void
CYAboutData::setVersion( const char* version )
{
  mVersion = version;
}

void
CYAboutData::setShortDescription( const char *shortDescription )
{
  mShortDescription = shortDescription;
}

void
CYAboutData::setLicense( LicenseKey licenseKey)
{
  mLicenseKey = licenseKey;
}

void
CYAboutData::setCopyrightStatement( const char *copyrightStatement )
{
  mCopyrightStatement = copyrightStatement;
}

void
CYAboutData::setOtherText( const char *otherText )
{
  mOtherText = otherText;
}

void
CYAboutData::setHomepage( const char *homepage )
{
  mHomepageAddress = homepage;
}

void
CYAboutData::setBugAddress( const char *bugAddress )
{
  mBugEmailAddress = bugAddress;
}

void
CYAboutData::setProductName( const char *productName )
{
  d->productName = productName;
}

const char *
CYAboutData::appName() const
{
   return mAppName;
}

const char *
CYAboutData::productName() const
{
   if (d->productName)
      return d->productName;
   else
      return appName();
}

QString
CYAboutData::programName() const
{
   if (mProgramName && *mProgramName)
      return QObject::tr(mProgramName);
   else
      return QString();
}

const char*
CYAboutData::internalProgramName() const
{
   if (d->mTranslatedProgramName)
      return d->mTranslatedProgramName;
   else
      return mProgramName;
}

// KCrash should call as few things as possible and should avoid e.g. malloc()
// because it may deadlock. Since QObject::tr() needs it, when KLocale is available
// the tr() call will be done here in advance.
void
CYAboutData::translateInternalProgramName() const
{
  delete[] d->mTranslatedProgramName;
  d->mTranslatedProgramName = 0;
  // TODO KDE3->QT3
//  if( KGlobal::locale() )
      d->mTranslatedProgramName = qstrdup( programName().toUtf8());
}

QImage
CYAboutData::programLogo() const
{
    return d->programLogo ? (*d->programLogo) : QImage();
}

void
CYAboutData::setProgramLogo(const QImage& image)
{
    if (!d->programLogo)
       d->programLogo = new QImage( image );
    else
       *d->programLogo = image;
}

QString
CYAboutData::version() const
{
   return QString::fromLatin1(mVersion);
}

QString
CYAboutData::shortDescription() const
{
   if (mShortDescription && *mShortDescription)
      return QObject::tr(mShortDescription);
   else
      return QString();
}

QString
CYAboutData::homepage() const
{
   return QString::fromLatin1(mHomepageAddress);
}

QString
CYAboutData::bugAddress() const
{
   return QString::fromLatin1(mBugEmailAddress);
}

const QList<CYAboutPerson>
CYAboutData::authors() const
{
   return mAuthorList;
}

const QList<CYAboutPerson>
CYAboutData::credits() const
{
   return mCreditList;
}

const QList<CYAboutTranslator>
CYAboutData::translators() const
{
    QList<CYAboutTranslator> personList;

    if(d->translatorName == 0)
        return personList;

    QStringList nameList;
    QStringList emailList;

    QString names = QObject::tr(d->translatorName);
    if(names != QString::fromUtf8(d->translatorName))
    {
        nameList = names.split(',', Qt::SkipEmptyParts);
    }


    if(d->translatorEmail)
    {
        QString emails = QObject::tr(d->translatorEmail);

        if(emails != QString::fromUtf8(d->translatorEmail))
        {
            emailList = emails.split(',',Qt::KeepEmptyParts);
        }
    }


    QStringList::Iterator nit;
    QStringList::Iterator eit=emailList.begin();

    for(nit = nameList.begin(); nit != nameList.end(); ++nit)
    {
        QString email;
        if(eit != emailList.end())
        {
            email=*eit;
            ++eit;
        }

        QString name=*nit;

        personList.append(CYAboutTranslator(name.trimmed(), email.trimmed()));
    }

    return personList;
}

QString
CYAboutData::aboutTranslationTeam()
{
    return QObject::tr("replace this with information about your translation team",
            "<p>KDE is translated into many languages thanks to the work "
            "of the translation teams all over the world.</p>"
            "<p>For more information on KDE internationalization "
            "visit <a href=\"http://l10n.kde.org\">http://l10n.kde.org</a></p>"
            );
}

QString
CYAboutData::otherText() const
{
   if (mOtherText && *mOtherText)
      return QObject::tr(mOtherText);
   else
      return QString();
}


QString
CYAboutData::license() const
{
  QString result;
  if (!copyrightStatement().isEmpty())
    result = copyrightStatement() + "\n\n";

  QString l;
  QString f;
  switch ( mLicenseKey )
  {
    case License_File:
       f = QFile::decodeName(mLicenseText);
       break;
    case License_GPL_V2:
       l = "GPL v2";
       // TODO KDE3->QT5
//       f = QStandardPaths:locate(QStandardPaths::AppDataLocation, "LICENSES/GPL_V2");
       break;
    case License_LGPL_V2:
       l = "LGPL v2";
       // TODO KDE3->QT5
//       f = QStandardPaths:locate(QStandardPaths::AppDataLocation, "LICENSES/LGPL_V2");
       break;
    case License_BSD:
       l = "BSD License";
       // TODO KDE3->QT5
//       f = QStandardPaths:locate(QStandardPaths::AppDataLocation, "LICENSES/BSD");
       break;
    case License_Artistic:
       l = "Artistic License";
       // TODO KDE3->QT5
//       f = QStandardPaths:locate(QStandardPaths::AppDataLocation, "LICENSES/ARTISTIC");
       break;
    case License_QPL_V1_0:
       l = "QPL v1.0";
       // TODO KDE3->QT5
//       f = QStandardPaths:locate(QStandardPaths::AppDataLocation, "LICENSES/QPL_V1.0");
       break;
    case License_Custom:
       if (mLicenseText && *mLicenseText)
          return( QObject::tr(mLicenseText) );
       // fall through
    default:
       result += QObject::tr("No licensing terms for this program have been specified.\n"
                   "Please check the documentation or the source for any\n"
                   "licensing terms.\n");
       return result;
      }

  if (!l.isEmpty())
     result += QObject::tr("This program is distributed under the terms of the %1.").arg( l );

  if (!f.isEmpty())
  {
     QFile file(f);
     if (file.open(QIODevice::ReadOnly))
     {
        result += '\n';
        result += '\n';
        QTextStream str(&file);
        result += str.readAll();
     }
  }

  return result;
}

QString
CYAboutData::copyrightStatement() const
{
  if (mCopyrightStatement && *mCopyrightStatement)
     return QObject::tr(mCopyrightStatement);
  else
     return QString();
}

QString
CYAboutData::customAuthorPlainText() const
{
  return d->customAuthorPlainText;
}

QString
CYAboutData::customAuthorRichText() const
{
  return d->customAuthorRichText;
}

bool
CYAboutData::customAuthorTextEnabled() const
{
  return d->customAuthorTextEnabled;
}

void
CYAboutData::setCustomAuthorText(const QString &plainText, const QString &richText)
{
  d->customAuthorPlainText = plainText;
  d->customAuthorRichText = richText;

  d->customAuthorTextEnabled = true;
}

void
CYAboutData::unsetCustomAuthorText()
{
  d->customAuthorPlainText = QString();
  d->customAuthorRichText = QString();

  d->customAuthorTextEnabled = false;
}
