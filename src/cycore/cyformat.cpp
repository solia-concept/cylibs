/***************************************************************************
                          cyformat.cpp  -  description
                             -------------------
    début                  : mar jui 29 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyformat.h"
#include "cy.h"

CYFormat::CYFormat(QObject *parent, const QString &name, QString unit, QString phys,
                   double min, double max, int dec, double prec,
                   uint slowSize, uint slowTime,
                   uint fastSize, float fastTime, short fastSample,
                   uint burstsSize, short burstsRatio)
  : QObject(parent),
    mUnit(unit),
    mPhysical(phys),
    mMin(min),
    mMax(max),
    mNbDigit(0),
    mNbDec(dec),
    mPrecision(prec),
    mSlowBufferSize(slowSize),
    mSlowAcquisitionTime(slowTime),
    mFastBufferSize(fastSize),
    mFastAcquisitionTime(fastTime),
    mFastSampleSize(fastSample),
    mBurstsBufferSize(burstsSize),
    mBurstsRatio(burstsRatio)
{
  setObjectName(name);
  mExponential=false;

  initTxtDef();
}

CYFormat::CYFormat(QObject *parent, const QString &name, QString unit, QString phys,
                   QString min, QString max, int dec, double prec,
                   uint slowSize, uint slowTime,
                   uint fastSize, float fastTime, short fastSample,
                   uint burstsSize, short burstsRatio)
  : QObject(parent),
    mUnit(unit),
    mPhysical(phys),
    mMin(min.toDouble()),
    mMax(max.toDouble()),
    mNbDigit(dec+1),
    mNbDec(dec),
    mPrecision(prec),
    mSlowBufferSize(slowSize),
    mSlowAcquisitionTime(slowTime),
    mFastBufferSize(fastSize),
    mFastAcquisitionTime(fastTime),
    mFastSampleSize(fastSample),
    mBurstsBufferSize(burstsSize),
    mBurstsRatio(burstsRatio)
{
  setObjectName(name);
  mExponential=true;

  initTxtDef();
}

CYFormat::~CYFormat()
{
}

void CYFormat::copy(CYFormat *src)
{
  mUnit                = src->unit();
  mPhysical            = src->physical();
  mMin                 = src->min();
  mMax                 = src->max();
  mNbDec               = src->nbDec();
  mNbDigit             = src->nbDigit();
  mPrecision           = src->precision();
  mSlowBufferSize      = src->bufferSize(true);
  mSlowAcquisitionTime = src->acquisitionTime(true);
  mFastBufferSize      = src->bufferSize(false);
  mFastAcquisitionTime = src->acquisitionTime(false);
  mFastSampleSize      = src->sampleSize(false);
  mBurstsBufferSize    = src->burstsBufferSize();
  mBurstsRatio         = src->burstsRatio();
  mExponential         = src->isExponential();

  initTxtDef();
}

void CYFormat::setNbDigit(int nb)
{
  mNbDec +=  nb - mNbDigit;
  if (mNbDec<0)
    mNbDec = 0;
  mNbDigit = nb;
  initTxtDef();
}

void CYFormat::setNbDec(int nb)
{
  mNbDigit += nb - mNbDec;
  mNbDec = nb;
  initTxtDef();
}

QString CYFormat::physicalType()
{
  if (mUnit.isEmpty())
    return mPhysical;
  else
    return QString("%1 (%2)").arg(mPhysical).arg(mUnit);
}

void CYFormat::initTxtDef()
{
  if ((mNbDigit-mNbDec)<0)
    return;

  mTxtDef.fill('-', mNbDigit);

  if (mNbDec)
    mTxtDef.insert((mNbDigit-mNbDec), ".");

  int tmp = mNbDigit-mNbDec;
  while (tmp > 3)
  {
    mTxtDef.insert((tmp-3), " ");
    tmp -= 3;
  }
}

void CYFormat::setExponential(bool val)
{
  mExponential = val;
}

bool CYFormat::isExponential()
{
  return mExponential;
}
