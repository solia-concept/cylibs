/***************************************************************************
                          cyprofibusitem.cpp  -  description
                             -------------------
    begin                : mer nov 17 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyprofibusitem.h"

// CYLIBS
#include "cy.h"

CYProfibusItem::CYProfibusItem(QObject *parent, e_pbtype type, e_pbtype_adr adr_type, int rn, int adress, void *ptr, int byte, int bit, int index)
: QObject(parent),
  mType(type),
  mAdrType(adr_type),
  mRN(rn),
  mAdress(adress),
  mPtr(ptr),
  mByte(byte),
  mBit(bit),
  mIndex(index)
{
  QString name = QString("%1/%2/%3.%4_%5").arg(rn).arg(adress).arg(byte).arg(bit).arg(index);

  switch( mAdrType )
  {
    case ADR_ASI: name.prepend("ASI_"); break; // type adressage ASi
    case ADR_PBUS: name.prepend("PBUS_"); break; // type adressage Profibus
    default: CYFATAL;
  }

  switch( mType )
  {
    case E_TOR: name.prepend("E_TOR_"); break; // entree tor
    case S_TOR: name.prepend("S_TOR_"); break; // sortie tor
    case E_OCT: name.prepend("E_OCT_"); break; // entree 8 bits
    case S_OCT: name.prepend("S_OCT_"); break; // sortie 8 bits
    case E_MOT: name.prepend("E_MOT_"); break; // entree 16 bits
    case S_MOT: name.prepend("S_MOT_"); break; // sortie 16 bits
    case E_F32: name.prepend("E_F32_"); break; // entree float
    case S_F32: name.prepend("S_F32_"); break; // sortie float
    default: CYFATAL;
  }

  setObjectName(name);
}

CYProfibusItem::~CYProfibusItem()
{
}
