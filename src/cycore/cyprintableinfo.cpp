/***************************************************************************
                          cyprintableinfo.cpp  -  description
                             -------------------
    begin                : mer jan 21 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyprintableinfo.h"

// CYLIBS
#include "cycore.h"
#include "cybool.h"
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"
#include "cystring.h"

CYPrintableInfo::CYPrintableInfo(QObject *parent, const QString &name, QString dataName, QString text)
  : QObject(parent),
    mText(text)
{
  setObjectName(name);

  if (core)
  {
    mData = core->findData(dataName);
    if (mData && (text==0))
      mText = mData->label()+"\n";
  }
}

CYPrintableInfo::~CYPrintableInfo()
{
}

QString CYPrintableInfo::toString()
{
  if (!mData)
    return 0;

  switch (mData->type())
  {
    case Cy::Bool   :
                    {
                      CYBool *data = (CYBool *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                      break;
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString((double)data->val()));
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    case Cy::String :
                    {
                      CYString *data = (CYString *)mData;
                      if (data->isOk())
                        return QString("%1%2").arg(mText).arg(data->toString());
                      return QString("%1%2").arg(mText).arg(data->stringDef());
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      break;
  }
  return 0;
}
