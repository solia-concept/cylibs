#include "cyapplication.h"

// QT
#include <QtGlobal>
#include <QDir>
#include <QSettings>
#include <QTimer>
#include <QByteArray>
// CYLIBS
#include "cyaboutdata.h"
#include "cy.h"
#include "cycore.h"

#ifdef Q_WS_X11
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/SM/SMlib.h>
//#include <fixx11h.h>
#endif

CYApplication* CYApplication::CYApp = 0L;

CYApplication::CYApplication( int& argc, char** argv, CYAboutData *aboutData, bool GUIenabled ) :
  QApplication( argc, argv, GUIenabled ),
  mAboutData( aboutData )
{
  bAlreadyExists = false;

  mOSUserConfig = nullptr;
  mOSConfig     = nullptr;
  mSharedMemory = nullptr;

  // initialisation des chemins d'installation en développement
  mInstall         = QString("%1").arg(CY_STR_INSTALL     );
  mInstallDoc      = QString("%1").arg(CY_STR_INSTALL_DOC );
  mInstallBin      = QString("%1").arg(CY_STR_INSTALL_BIN );
  mInstallLanguage = QString("%1").arg(CY_STR_INSTALL_LANG);
  mInstallLinux    = QString("%1").arg(CY_STR_INSTALL_LINUX);
  mInstallWindows  = QString("%1").arg(CY_STR_INSTALL_WINDOWS);

#ifndef QT_DEBUG
  mSharedMemory = new QSharedMemory(aboutData->appName(), this);

  // when  can create it only if it doesn't exist
  if (mSharedMemory->create(5000))
  {
    mSharedMemory->lock();
    *(char*)mSharedMemory->data() = '\0';
    mSharedMemory->unlock();

    // start checking for messages of other instances.
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(checkForMessage()));
    timer->start(200);
  }
  // it exits, so we can attach it?!
  else if (mSharedMemory->attach())
  {
    bAlreadyExists = true;
  }
  else
  {
    CYFATALTEXT(mSharedMemory->errorString())
  }
#endif

  init(GUIenabled);
}


CYApplication::~CYApplication()
{
  if (mSharedMemory)
    mSharedMemory->detach();
}

QSettings* CYApplication::OSUserConfig()
{
  if (mOSUserConfig)
    return mOSUserConfig;

  mOSUserConfig = new QSettings(QSettings::UserScope, "Cylix",  aboutData()->appName());
  return mOSUserConfig;
}

QSettings* CYApplication::OSConfig()
{
  if (mOSConfig)
    return mOSConfig;

  mOSConfig = new QSettings(core->appDataDir()+"config.ini", QSettings::IniFormat);
  return mOSConfig;
}

CYAboutData *CYApplication::aboutData() const
{
  return mAboutData;
}

void CYApplication::setAboutData(CYAboutData *about)
{
  mAboutData=about;
}

void CYApplication::init(bool GUIenabled)
{
  mGUIenabled = GUIenabled;
  //  if ((getuid() != geteuid()) ||
  //      (getgid() != getegid()))
  //  {
  //     fprintf(stderr, "The CYLIBS libraries are not designed to run with suid privileges.\n");
  //     ::exit(127);
  //  }

  // TODO KDE3->QT3 : valider le passage à true
  //  QApplication::setDesktopSettingsAware( true );

  CYApp = this;
#ifdef Q_WS_X11
  //  // register a communication window for desktop changes (Matthias)
  //  if (GUIenabled && kde_have_kipc )
  //  {
  //    smw = new QWidget(0,0);
  //    long data = 1;
  //    XChangeProperty(qt_xdisplay(), smw->winId(),
  //                    atom_DesktopWindow, atom_DesktopWindow,
  //                    32, PropModeReplace, (unsigned char *)&data, 1);
  //  }
  //  d->oldIceIOErrorHandler = IceSetIOErrorHandler( kde_ice_ioerrorhandler );
#elif defined(Q_WS_WIN)
  init_windows(GUIenabled);
#else
  // FIXME(E): Implement for Qt Embedded
#endif
}

/**
 * Actions propres à MS Windows-related au démmarrage d'une CYApplication.
 *
 * - Utilisation de la traduction de Qt. Le fichier "qt_<language>.qm"
 *   devra être stocké au même endroit que les fichiers .po d'un même langage.
 *
 * @internal
*/
void CYApplication::init_windows(bool /*GUIenabled*/)
{
  //	QString qt_transl_file = ::locate( "locale", KGlobal::locale()->language()
  //		+ "/LC_MESSAGES/qt_" + KGlobal::locale()->language() + ".qm" );
  //	QTranslator *qt_transl = new QTranslator();
  //	if (qt_transl->load( qt_transl_file, ""))
  //		cyapp->installTranslator( qt_transl );
  //	else
  //		delete qt_transl;
}

QString CYApplication::caption() const
{
  // We have some about data ?
  if ( aboutData() )
    return aboutData()->programName();
  else
    // Last resort : application name
    return objectName();
}

QString CYApplication::makeStdCaption( const QString &userCaption,
                                       bool withAppName, bool modified ) const
{
  QString s = userCaption.isEmpty() ? caption() : userCaption;

  // If the document is modified, add '[modified]'.
  if (modified)
    s += QString::fromUtf8(" [") + tr("modified") + QString::fromUtf8("]");

  if ( !userCaption.isEmpty() ) {
    // Add the application name if:
    // User asked for it, it's not a duplication  and the app name (caption()) is not empty
    if ( withAppName && !caption().isNull() && !userCaption.endsWith(caption())  )
      s += QString::fromUtf8(" - ") + caption();
  }

  return s;
}

void CYApplication::checkForMessage()
{
  QStringList arguments;

  mSharedMemory->lock();
  char *from = (char*)mSharedMemory->data();

  while(*from != '\0'){
    int sizeToRead = int(*from);
    ++from;

    QByteArray byteArray = QByteArray(from, sizeToRead);
    byteArray[sizeToRead] = '\0';
    from += sizeToRead;

    arguments << QString::fromUtf8(byteArray.constData());
  }

  *(char*)mSharedMemory->data() = '\0';
  mSharedMemory->unlock();

#ifndef QT_DEBUG
  if(arguments.size()) emit messageAvailable( arguments );
#endif
}

// public functions.

bool CYApplication::sendMessage(const QString &message)
{
  //we cannot send mess if we are master process!
  if (isMasterApp()){
    return false;
  }

  QByteArray byteArray;
  byteArray.append(char(message.size()));
  byteArray.append(message.toUtf8());
  byteArray.append('\0');

  mSharedMemory->lock();
  char *to = (char*)mSharedMemory->data();
  while(*to != '\0'){
    int sizeToRead = int(*to);
    to += sizeToRead + 1;
  }

  const char *from = byteArray.data();
  memcpy(to, from, qMin(mSharedMemory->size(), byteArray.size()));
  mSharedMemory->unlock();

  return true;
}

void CYApplication::switchTranslator(QLocale locale)
{
  if (!locale.name().startsWith("en"))
  {
    QString defaultLocale = locale.name(); // e.g. "de_DE"
    defaultLocale.truncate(defaultLocale.lastIndexOf('_')); // e.g. "de"

    switchTranslator(mTranslatorQt, QString("qt_%1.qm").arg(locale.name()), QString("%1/translations").arg(DEF_QT_INSTALL_PREFIX));
    switchTranslator(mTranslatorCy, QString("cylibs_%1.qm").arg(locale.name()), mInstallLanguage);
    switchTranslator(mTranslator, QString("%1_%2.qm").arg(aboutData()->programName()).arg(locale.name()), mInstallLanguage);
  }
}

void CYApplication::switchTranslator(QTranslator& translator, const QString& filename, const QString &directory)
{
  // supprime l'ancienne traduction
  removeTranslator(&translator);

  // charge la nouvelle traduction
  if(translator.load(filename, directory))
  {
    if (!installTranslator(&translator))
    {
      CYWARNINGTEXT(QString("filename:%1, directory=%2").arg(filename).arg(directory));
    }
  }
}

