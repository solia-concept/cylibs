#include "cyreadonlyfilter.h"
#include "cycore.h"

CYReadOnlyFilter::CYReadOnlyFilter(QObject *parent) : QObject(parent)
{

}

bool CYReadOnlyFilter::eventFilter(QObject *obj, QEvent *event)
{
  if (
      (event->type() == QEvent::ActivationChange    ) ||
      (event->type() == QEvent::ContextMenu         ) ||
      (event->type() == QEvent::Drop                ) ||
      (event->type() == QEvent::KeyPress            ) ||
      (event->type() == QEvent::KeyRelease          ) ||
      (event->type() == QEvent::MouseButtonDblClick ) ||
      (event->type() == QEvent::MouseButtonPress    ) ||
      (event->type() == QEvent::MouseButtonRelease  ) ||
      (event->type() == QEvent::Shortcut            ) ||
      (event->type() == QEvent::ShortcutOverride    ) ||
      (event->type() == QEvent::GrabKeyboard        ) ||
      (event->type() == QEvent::GrabMouse           ) ||
      (event->type() == QEvent::OkRequest           ) ||
      (event->type() == QEvent::Wheel               )  )
  {
    return true;
  }
  else
  {
    // standard event processing
    return QObject::eventFilter(obj, event);
  }
}
