/*
 * scmdb_t.h
 *
 *  Created on: 13 déc. 2012
 *      Author: clc
 */

#ifndef SCMDB_T_H_
#define SCMDB_T_H_

#ifndef SUCCESS
#define SUCCESS 0
#endif

#ifndef FAILURE
#define FAILURE -1
#endif

#if defined(Q_OS_WIN)
  #include <winsock2.h>
#endif

#include <stdint.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <dirent.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <signal.h>


namespace std {
namespace tr1 {
using ::int8_t;
using ::int16_t;
using ::int32_t;
using ::int64_t;
using ::int_fast8_t;
using ::int_fast16_t;
using ::int_fast32_t;
using ::int_fast64_t;
using ::int_least8_t;
using ::int_least16_t;
using ::int_least32_t;
using ::int_least64_t;
using ::intmax_t;
// using ::intintptr_t;
using ::uint8_t;
using ::uint16_t;
using ::uint32_t;
using ::uint64_t;
using ::uint_fast8_t;
using ::uint_fast16_t;
using ::uint_fast32_t;
using ::uint_fast64_t;
using ::uint_least8_t;
using ::uint_least16_t;
using ::uint_least32_t;
using ::uint_least64_t;
using ::uintmax_t;
// using ::uintintptr_t;
}
}

#define B00 0x0001
#define OFLAG_TBSPV    (O_NONBLOCK|O_RDWR)

#if defined(Q_OS_WIN)
  #define OFLAG_REGITPRC (O_RDWR)
  #define MSG_WAITALL 0x8
  #define OFLAG_SDRCSOCET 0 //MSG_WAITALL
  #define LIEN_TUBEWIN   "\\\\.\\pipe"
  #define NM_TBINITMDB "\\\\.\\pipe\\sctb_initmdb"
  #define NM_TBACQMDB  "\\\\.\\pipe\\sctb_acqmdb"
#elif defined(Q_OS_LINUX)
  #define OFLAG_REGITPRC (O_NONBLOCK|O_RDWR)
  #define OFLAG_SDRCSOCET (MSG_WAITALL | MSG_NOSIGNAL)
  #define LIEN_ITPRC  "/tmp/MDB_CLT"
  #define CH_PIDPRC   "/tmp/MDB_CLT/pid_prg.bin"
  #define NM_TBINITMDB "/tmp/MDB_CLT/sctb_initmdb"
  #define NM_TBACQMDB  "/tmp/MDB_CLT/sctb_acqmdb"
#endif


#define OFLAG_INITMDB   O_NONBLOCK | O_RDWR
#define SUFIXE_TBDCL "dcl"
#define SUFIXE_TBDON "don"
#define SUFIXE_VRPDCL "lvr"
#define SUFIXE_VRPDON "nvr"
#define LG_NOM_REGISTRE 64
#define PORT_COMVRP 50000
enum
{
  READ_DATA = 0,
  WRITE_DATA,
};

#define TMTRPS_RES_NS     1000  //resolution d atte en ns
#define TMTRPS_US         1000  //timeout reponse en us
#define TMTRPS_NBLEC      (int)(TMTRPS_US*1e3/TMTRPS_RES_NS)



#define NB_MAXSTRUCT 256
enum
{
  TBREGSRV_SRV=0,
  TBREGSRV_REG
};
//---Enum-----------------------------------------------------------------------------------------

typedef enum
{
  RTU = 0x01,
  TCP,
  SKT,
  PASRL,
  ADS
} TypeCom;

typedef enum
{
  TPREG_NORMAL = 0x00, //registre normal pas d'enregistrement
  TPREG_ENR,           //registre a enregistrer
  TPREG_COMPT,         //registre type compteur totalisateur a ecraser/enregistrer
  TPREG_ERRNO,         //registre type errno a surveiller
  TPREG_ERRNO_INV,     //ernno inverser
  TPREG_COURBE,        //affichage courbe
} TypeReg;

typedef enum
{
  MDB_DATA_BIT = 0x00, //type bit
  MDB_DATA_REG,        //type reg
  MDB_DATA_INC,        //data inconnue -> necessite une requete pr la lecture ecriture
  MDB_DATA_ST,
  MDB_DATA_ADS
} TypeData;

typedef enum
{
  MDB_ACCES_RO  = 0x00,
  MDB_ACCES_RW  = 0x01,
  MDB_ACCES_EEP = 0x02,
} TypeAcces;

enum
{
  TBInit_ras = 0x01,
  TBInit_erreur,
  TBInit_Init,
  TBInit_Stop_communication,
  TBInit_Stop_prg,
  TBInit_ETATCOM,
  TBInit_STARTSEQ,
  TBInit_STOPSEQ,
  TBInit_RAZCMPT,
};


/* Liste des défauts com.
 *
 * DEFAUT_COM_PRG
 *  - Défaut interne au programme de communication (Allocation mémoire, tube...).
 *
 * DEFAUT_COM_TIMEOUT
 *  - Pas de réponse du régulateur ou du PLC
 *
 * DEFAUT_COM_ST
 *  - Défaut de structure (adresse, handle, taille...)
 *
 * DEFAUT_COM_TRAME
 *  - Défaut trame
 * 	  -> MDB:   Erreur trame modbus
 * 	  -> RN:    Non Implémenté
 * 	  -> PASRL: Non Implémenté
 * 	  -> ADS:   Non Implémenté
 *
 * DEFAUT_COM_INT
 * 	- Défaut interne.
 * 	  -> MDB:   Erreur interne initialisation serveur modbus
 * 	  -> RN:    Erreur interne initialisation ou serveur socket
 * 	  -> PASRL: Non Implémenté
 * 	  -> ADS:   Erreur ports ads
 *
 * DEFAUT_COM_EXT
 *  - Défaut de communication externe.
 * 	  -> MDB:   Non Implémenté
 * 	  -> RN:    Non Implémenté
 * 	  -> PASRL: Erreur communication RS422
 * 	  -> ADS:   Non Implémenté
 *
 * DEFAUT_COM_PASRL
 * 	- Défaut venant de la passerelle.
 * 	  -> MDB:   Non Implémenté
 * 	  -> RN:    Non Implémenté
 * 	  -> PASRL: Erreur initialisation de la passerelle
 * 	  -> ADS:   Non Implémenté
 *
 * DEFAUT_COM_INC
 *  - Défaut inconnue.
 */

enum
{
  DEFAUT_COM_NODEFAUT = 0,
  DEFAUT_COM_PRG,
  DEFAUT_COM_TIMEOUT,
  DEFAUT_COM_ST,
  DEFAUT_COM_TRAME,
  DEFAUT_COM_INT,
  DEFAUT_COM_EXT,
  DEFAUT_COM_PASRL,
  DEFAUT_COM_INC,
  DEFAUT_COM_NB
};


enum
{
	MDBERR_ENCOURS=-1, //en cours
	MDBERR_Succes,
	MDBERR_EXCEPTION_ILLEGAL_FUNCTION,
	MDBERR_EXCEPTION_ILLEGAL_DATA_ADDRESS,
	MDBERR_EXCEPTION_ILLEGAL_DATA_VALUE,
	MDBERR_EXCEPTION_SLAVE_OR_SERVER_FAILURE,
	MDBERR_EXCEPTION_ACKNOWLEDGE,
	MDBERR_EXCEPTION_SLAVE_OR_SERVER_BUSY,
	MDBERR_EXCEPTION_NEGATIVE_ACKNOWLEDGE,
	MDBERR_EXCEPTION_MEMORY_PARITY,
	MDBERR_EXCEPTION_NOT_DEFINED,
	MDBERR_EXCEPTION_GATEWAY_PATH,
	MDBERR_EXCEPTION_GATEWAY_TARGET,
	MDBERR_EXCEPTION_MAX,
	MDBERR_ERRCOM_BADCRC,
	MDBERR_ERRCOM_BADDATA,
	MDBERR_ERRCOM_BADEXC,
	MDBERR_ERRCOM_UNKEXC,
	MDBERR_ERRCOM_MDATA,
	MDBERR_ERRCOM_BADSLAVE,
	MDBERR_TIMEOUT,   //timeout socket
	MDBERR_INCONNUS,
	MDBERR_Alloc_err,
	MDBERR_Initmdb_err,
	MDBERR_Connectmdb_err,
	MDBERR_Tube_err,
	MDBERR_Timeout_prg, //pas de reponse du rn (pgr serveur)
	MDBERR_Init_PASRL,
	MDBERR_TUBE_VIDE,
	MDBERR_RS422_TIMEOUT,
	MDBERR_RS422_RW,
	MDBERR_RS422_NAME,
	MDBERR_RS422_CHK,
	MDBERR_RS422_SIZE,
	MDBERR_RS422_PARITY,
	MDBERR_RS422_SFI,
	MDBERR_RS422_REMOTE,
	MDBERR_RS422_NOIDENT,
};


enum
{
  TBACQ_Succes = 0,
  TBACQ_cpy_data_err,
  TBACQ_init_struct_err
};

enum
{
  TBREQ_Ras = -2,
  TBREQ_En_cours,
};
//---Tube-----------------------------------------------------------------------------------------
typedef struct
{
  int addr;      //Addresse du slave                             -> A renseigner
  int baudrate;  //vitesse                                       -> A renseigner
  char dev[16];  //devise /dev/ttyUSB0                           -> A renseigner
  char parity;   //bit de Parite : 'N'                           -> A renseigner
  int data_bit;  //8                                             -> A renseigner
  int stop_bit;  //1                                             -> A renseigner
} tb_cfgrtu;

typedef struct
{
  char ip[16];   //Address IP                                    -> A renseigner
  int port;      //Port : Default Norme : 502                    -> A renseigner
} tb_cfgtcp;

typedef struct
{
  uint8_t   b[8];                          //Address IP                                    -> A renseigner
  uint16_t  port;                          //Port : Default Norme : 502                    -> A renseigner
  char      ie_rnr[LG_NOM_REGISTRE];       //Handle IE RNR                                 -> A renseigner
  char      il_rnr[LG_NOM_REGISTRE];       //Handle IL RNR                                 -> A renseigner
}tb_cfgads;


typedef struct
{
  int32_t  id;                    //id du tube associé                    -> A renseigner
  uint16_t addr;                  //address du registre ou bit            -> A renseigner
  int8_t   nom[LG_NOM_REGISTRE];  //Nom du registre                       -> A renseigner
  uint32_t lgsto;                 //longeur de la structure en octet      -> A renseigner
  TypeData tpdt;                  //type data bit ou registre             -> A renseigner
  int16_t  attr_com;              //r422                                  -> A renseigner
  int16_t  seq;                   //r422                                  -> A renseigner
  int8_t   index_auto;            //r422                                  -> A renseigner
  uint32_t flags;                 //r422                                  -> A renseigner
  int16_t  nb_read;               //r422                                  -> A renseigner
  int16_t  nb_write;              //r422                                  -> A renseigner
  int16_t  index_regdef;          //index registre defaut                 -> A renseigner
  int16_t  acces;
} tb_dclreg;

typedef struct
{
  int32_t id;     //id du boitier                                -> A renseigner
  TypeCom tcm;    //type de com                                  -> A renseigner
  uint16_t nbst;  //nombre de structure                          -> Automatique  a l'init
} tb_clt;

typedef struct
{
  int32_t  id;       //id du boitier                             -> A renseigner
  TypeCom  tcm;      //type de com                               -> A renseigner
  uint16_t nbst;     //nombre de structure
  uint16_t max_adr;  //max adresse pr alloc memoire
} tb_srv;

typedef struct
{
	char      nm[64];
	int       oflag;
	int       num;
#if defined(Q_OS_WIN)
	HANDLE    hIn;
	DWORD     err;
#endif
} dcltb_t;

typedef struct
{
int16_t rw;     //read : 0 write 1
int16_t prt;    //priorite;         -> A renseigner (si 0 lecture auto off)
int16_t index;
} ordrw_t;


typedef struct
{
int8_t  rep;
int8_t  rw;
int16_t index;
int16_t id;
int16_t defaut[9];
} entrp_t;

typedef struct
{
int32_t rw;                 //read : 0 write 1
int32_t addr;               //address du reg modbus
int32_t lg;                 //longeur voulu en mot ou bit selon type
int32_t tpdt;               //type bit ou reg
int32_t index;
} reqrc_t;

//---Config modbus
typedef struct
{
int            addr;         //Addresse du slave                                                                      -> A renseigner
int            baudrate;     //vitesse                                                                                -> A renseigner
char           *dev;         //devise /dev/ttyUSB0                                                                    -> A renseigner
char            parity;      //bit de Parite : 'N'                                                                    -> A renseigner
int            data_bit;     //8                                                                                      -> A renseigner
int            stop_bit;     //1                                                                                      -> A renseigner
int            md_serie;     //MODBUS_RTU_RS232, MODBUS_RTU_RS485                                                     -> A renseigner
int            md_rts;       //MODBUS_RTU_RTS_NONE, MODBUS_RTU_RTS_DOWN, MODBUS_RTU_RTS_UP                            -> A renseigner
int            md_err_recov; //MODBUS_ERROR_RECOVERY_NONE MODBUS_ERROR_RECOVERY_LINK | MODBUS_ERROR_RECOVERY_PROTOCOL -> A renseigner
struct timeval to_rps;       //Structure Timeout reponse                                                              -> A renseigner
struct timeval to_octet;     //Structure Timeout reponse                                                              -> A renseigner
} cfgmdbrtu_t;

typedef struct
{
char *ip;          //Address IP                                  -> A renseigner
int  port;         //Port : Default Norme : 502                  -> A renseigner
int  socket;
} cfgmdbip_t;

typedef struct
{
  uint8_t    b[8];
  uint16_t   port;
  char      ie_rnr[LG_NOM_REGISTRE];       //Handle IE RNR                                 -> A renseigner
  char      il_rnr[LG_NOM_REGISTRE];       //Handle IL RNR                                 -> A renseigner
}cfgmdbads_t;


typedef struct
{
int32_t    id;                     //numero registre                              -> A renseigner
int8_t     nom[LG_NOM_REGISTRE];   //Nom du registre                              -> A renseigner
void       *prtst;                 //pointeur sur la donnee                       -> A renseigner
uint32_t   lgsto;                  //longeur de la structure en octet             -> A renseigner
uint16_t   addr;                   //address du registre                          -> 0 : automatique  a l'init sinon a renseigner
TypeData   tpdt;                   //type data bit ou registre                    -> A renseigner
TypeAcces  acces;                  //Acces read/write ou eep                      -> A renseigner
uint16_t   modulo;                 //modulo utiliser calcul adresse               -> A renseigner
int16_t    seq;                    //r422                                         -> A renseigner
int16_t    attr_com;               //r422                                         -> A renseigner
int8_t     index_auto;             //r422                                         -> A renseigner
uint32_t   flags;                  //r422                                         -> A renseigner
int16_t    nb_read;                //r422                                         -> A renseigner
int16_t    nb_write;               //r422                                         -> A renseigner
int16_t    index_regdef;           //index registre envoie erreur. -1 des.        -> A renseigner
dcltb_t    p_dcltb;                //pointeur sur le tube declaration associé     -> Automatique a l'init
dcltb_t    p_dontb;                //pointeur sur le tube donne associé           -> Automatique a l'init
dcltb_t    p_spvtb;                //pointeur sur le tube spv associé             -> Automatique a l'init
uint16_t   lgst;                   //longeur de la structure en mot               -> Automatique  a l'init
uint8_t    fl_rw;                  //Flag lecture/ecriture reussi                 -> Interne prg
ordrw_t    ordre;                  //ordre envoye
reqrc_t    req;                    //requete recu en cas de tpdt inconnu          -> Interne prg
int16_t    prt;                    //priorite
int16_t    prt_tmp;                //priorite temporaire
void       *ptrBuf;
void       *ptr_buf_tmp;
entrp_t    entete_tb;              //entete de reponse tube
} regmdb_t;

typedef struct
{
TypeCom   tcm;        //type de com                                  -> A renseigner
void      *cfg;       //config modbus                                -> A renseigner
regmdb_t  *rg;        //config registre                              -> A renseigner
uint32_t  lgst;       //taille de la table structure en octet        -> A renseigner
uint16_t  nbst;       //nombre de structure                          -> Automatique  a l'init
uint16_t  max_adr;
uint32_t  max_lgdata;
} cfgmdb_t;

#define TAILLE_MAX_REF_APP 32
typedef struct
{
int32_t   id;            //id automatique a l init numero de structure
cfgmdb_t  *mdb;          //gestion du modbus.                           -> A renseigner
int       init;          //true ou false si init realiser               -> Interne prg
int       fl_err;        //true si erreur dernier lecture               -> Interne prg
char      adr_com[32];   //temporaire type com
int       adr_com_rtu;
int       num_com;       //temporaire num com
int32_t   nb_nnrep;      //nombre non reponse
} clientmdb_t;

#endif /* SCMDB_T_H_ */
