//
// C++ Implementation: cyabout
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// ANSI
#include <stdlib.h>
// QT
#include <QFileDialog>
// CYLIBS
#include "cyabout.h"
#include "ui_cyabout.h"
#include "cycore.h"
#include "cypushbutton.h"
#include "cynethost.h"
#include "cyuser.h"
#include "cynetr422f.h"

CYAbout::CYAbout(QWidget *parent, const QString &name)
  : CYWidget(parent,name), ui(new Ui::CYAbout)
{
  ui->setupUi(this);

  connect( ui->updateButton , SIGNAL( clicked() ), SLOT( updateHost() ) );
}


CYAbout::~CYAbout()
{
  delete ui;
}

void CYAbout::setHost(int index)
{
  mIndex = index;
  setGenericMark(mIndex);
  mHost = core->host(mIndex);

  // Cache le groupe info temps réel si pas RN Linux
  if (mHost->type!=CYNetHost::RN_LINUX)
    ui->realTimeGroup->hide();

  QString flagName = mHost->enableR422F();
  if (flagName.isEmpty()) // mise à jour non gérée
  {
    ui->updateButton->hide();
    return;
  }

  if (!core->designer() && !core->user()->administrator())
  {
    ui->updateButton->setEnabled(false);
  }
  else if (flagName.startsWith("!"))
  {
    flagName = flagName.remove("!");
    ui->updateButton->setFlagName(flagName.toUtf8());
    ui->updateButton->setInverseFlag(true);
  }
  else
  {
    ui->updateButton->setFlagName(flagName.toUtf8());
  }
}

void CYAbout::updateHost()
{
//  if (core->module->isModbus())
//  {
//    QString startWith = getenv( "HOME" );
//    QString filter = (core->designer()) ? "*.tar.gz *.ini *.tar" : "*.tar.gz *.tar";

//    QString fileName = QFileDialog::getOpenFileName( this, tr("Select archive to send to the regulator."), startWith, filter);

//    if (fileName.isEmpty())
//      return;

//    QString program = "scp";
//    QStringList arguments;
//    arguments << fileName << "root@rn:/root/rn/RN_Linux";
//    QProcess *proc = new QProcess(this);
//    proc->start(program, arguments);
//    CYMESSAGETEXT(QString("%1 %2").arg(proc->program()).arg(proc->arguments().join(" ")));
//  }
//  else
  {
    QString startWith = getenv( "HOME" );
    QString filter = (core->designer()) ? "*.tar.gz *.ini *.tar" : "*.tar.gz *.tar";

    QString fileName = QFileDialog::getOpenFileName( this, tr("Select archive to send to the regulator."), startWith, filter);

    if (fileName.isEmpty())
      return;

    CYNetR422F *dlg = new CYNetR422F( mHost, fileName, this);
    if (dlg->exec())
    {
    }
  }
}
