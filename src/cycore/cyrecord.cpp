/***************************************************************************
                          cyrecord.cpp  -  description
                             -------------------
    début                  : mer avr 30 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cyrecord.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
// CYLIBS
#include "cytypes.h"
#include "cy.h"

#define R_1ST_FILE  M00
#define R_2ND_FILE  M01
#define R_TWO_FILE  M02

#define R_INIT      M04
#define R_TEST      M05

CYRecord::CYRecord(void *ptr, int size, const QString &filename, QObject *parent, const QString &name, int nb)
  : QObject(parent),
    mPtr(ptr),
    mFileName(filename)
{
  setObjectName(name);

  mLoaded = false;
  mFileNameBack = mFileName;
  mFileNameBack.replace(mFileNameBack.length()-1, 1, '~');
  mFileSize = size * nb;
}

CYRecord::~CYRecord()
{
}

void CYRecord::load()
{
  if (access(mFileName.toUtf8(), F_OK))
  {
    // si premier fichier existe pas
    if (access(mFileNameBack.toUtf8(), F_OK))  //si second fichier n'existe pas
      createFile(R_TWO_FILE);
    else
    {  // si second fichier existe et 1er fichier n'existe pas
      if(!readFile(R_2ND_FILE))
        createFile(R_TWO_FILE);
      else
        createFile(R_1ST_FILE);
    }
  }
  else
  {
    // si 1er fichier existe
    if(!readFile(R_1ST_FILE))
    {
      // si erreur sur lecture 1er fichier
      if (access(mFileNameBack.toUtf8(), F_OK))  //si second fichier n'existe pas
        createFile(R_TWO_FILE);
      else
      {
        // si second fichier existe et 1er fichier n'existe pas
        if(!readFile(R_2ND_FILE))
          createFile(R_TWO_FILE);
        else
          createFile(R_1ST_FILE);
      }
    }
    else
    {  // si 1er fichier ok test du second fichier
      if (access(mFileNameBack.toUtf8(), F_OK))  //si second fichier n'existe pas
        createFile(R_2ND_FILE);
      else
      {  // si second fichier existe test validiter des données
        if(!readFile(R_2ND_FILE | R_TEST))
          createFile(R_2ND_FILE); // si données corrompues second fichier
      }
    }
  }
  mLoaded = true;
}

void CYRecord::save()
{
  int fd;
  unsigned int check;

  if ((fd = open(mFileName.toUtf8(), O_WRONLY, S_IRUSR | S_IWUSR)) != -1)
  {
    check=fileCheck();
    ssize_t size;
    size = write(fd, &check, sizeof(check));
    if (size<0)
      CYWARNING
    size = write(fd, mPtr, mFileSize);
    if (size<0)
      CYWARNING
    close(fd);
  }
  else
    createFile(R_1ST_FILE);

  if ((fd=open(mFileNameBack.toUtf8(), O_WRONLY, S_IRUSR | S_IWUSR)) != -1)
  {
    check=fileCheck();
    ssize_t size;
    size = write(fd, &check, sizeof(check));
    if (size<0)
      CYWARNING
    size = write(fd, mPtr, mFileSize);
    if (size<0)
      CYWARNING
    close(fd);
  }
  else
    createFile(R_2ND_FILE);
}

unsigned int CYRecord::fileCheck()
{
  unsigned int  nbc  = mFileSize;
  unsigned int *iptr = (unsigned int *)mPtr, ch, i;

  nbc /= 2;
  for (ch=0, i=0; i<nbc; i++)
    ch += *iptr++;
  return (~ch);
}

void CYRecord::createFile(int copy)
{
  // attention on écrit le checksum en premier
  // pour faciliter la vérif. ensuite
  int  fd;
  unsigned int check;

  if (copy & (R_1ST_FILE | R_TWO_FILE))
  {
    if ((fd = open(mFileName.toUtf8(), O_CREAT, S_IRUSR | S_IWUSR)) != -1)
    {
      check = fileCheck();
      ssize_t size;
      size = write(fd, &check, sizeof(check));
      if (size<0)
        CYWARNING
      size =write(fd, mPtr, mFileSize);
      if (size<0)
        CYWARNING
      close(fd);
    }
  }
  if(copy & (R_2ND_FILE | R_TWO_FILE))
  {
    if ((fd = open(mFileNameBack.toUtf8(), O_CREAT, S_IRUSR | S_IWUSR)) != -1)
    {
      check=fileCheck();
      ssize_t size;
      size = write(fd, &check, sizeof(check));
      if (size<0)
        CYWARNING
      size =write(fd, mPtr, mFileSize);
      if (size<0)
        CYWARNING
      close(fd);
    }
  }
}

char CYRecord::readFile(int copy)
{
  int fd;
  unsigned int check;
  bool fl_unlink = true;
  QString pname;

  if (copy & R_2ND_FILE)
    pname = mFileNameBack;
  else
    pname = mFileName;

  if ((fd = open(pname.toUtf8(), O_RDWR, S_IRUSR | S_IWUSR)) != -1 )
  {
    struct stat desc;
    fstat(fd, &desc);
    if (desc.st_size == (int)(mFileSize+sizeof(check)))
    {
      ssize_t size;
      size = read(fd, &check, sizeof(check));
      if (size<0)
        CYWARNING
      size = read(fd, mPtr, mFileSize);
      if (size<0)
        CYWARNING

      fl_unlink = false;
    }
    close(fd);
  }
  if (!fl_unlink)
    unlink(pname.toUtf8());

  return(!fl_unlink);
}

bool CYRecord::isLoaded()
{
  return mLoaded;
}
