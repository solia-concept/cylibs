/***************************************************************************
                          cycore.cpp  -  description
                             -------------------
    début                  : mer mar 19 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#include "cycore.h"

#define DEBUG

// QT
#include <QComboBox>
#include <QPushButton>
#include <QPainter>
#include <QLayout>
#include <QDir>
#include <QProgressDialog>
#include <QFileDialog>
#include <QPixmap>
#include <QTextStream>
#include <QEvent>
#include <QFrame>
#include <QString>
#include <QMenu>
#include <QList>
#include <QProgressDialog>
#include <QTextCodec>
#include <QResource>
#include <QDesktopServices>
// CYLIBS
#include "cyglobal.h"
#include "cyaboutdata.h"
#include "cyuser.h"
#include "cyusergroup.h"
#include "cyuseradminedit.h"
#include "cydb.h"
#include "cydata.h"
#include "cyformat.h"
#include "cynetlink.h"
#include "cynethost.h"
#include "cynetmodule.h"
#include "cynetconnect.h"
#include "cydisplaystyle.h"
#include "cymaths.h"
#include "cyproject.h"
#include "cyeventsmanager.h"
#include "cyacquisition.h"
#include "cyacquisitionwin.h"
#include "cysplashscreen.h"
#include "cyprofibusitem.h"
#include "cymv.h"
#include "cyct.h"
#include "cydblibs.h"
#include "cyflag.h"
#include "cystring.h"
#include "cymainwin.h"
#include "cydatasbrowser.h"
#include "cydatastable.h"
#include "cyacquisitiondataview.h"
#include "cyacquisitionbackupflag.h"
#include "cydataslistview.h"
#include "cyeventslist.h"
#include "cyabout.h"
#include "cydatastablecol.h"
#include "cydatastablerow.h"
#include "cydatastabledisplaycell.h"
#include "cymetrowin.h"
#include "cyfilecsv.h"
#include "cydatetime.h"
#include "cyapplication.h"
#include "cymessagebox.h"
#include "cyaboutapplication.h"
#include "cyreadonlyfilter.h"

CYCore *core;

CYCore::CYCore(QObject *parent, const QString &name)
  : QObject(parent)
{
  setObjectName(name);
  mInitOk          = false;
  mUser            = 0;
  mAdministrator   = 0;
  mProtectedAccess = 0;
  mProtectProtectDirData = 0;
  mPyramidalUserAdmin=true;
  mInitDBOk  = false;
  mBenchType = tr("Bench").toUtf8();
//  mProject   = 0;
  mRunning   = false;
  events     = 0;
  mLockForcing  = true;
  mProjectLoaded = false;
  mProjectLack   = true;
  mNewTestDir  = true;
  mTestDirAuto = true;
  mStarted = false;
  mSimulEvents = true;
  mSimulMode = false;
  mNotifyDatasLoading = true;
  mScreenSaverDetected = false;
  mDesktopEnvironmentProtectable = false;

  mMainWin = 0;
  mAcquisitionWin = 0;
  mMetroWin = 0;

  mEnableAnalyseRefresh = true;
  analyseRefreshEvent   = true;
  mMaxCurvesByScope = 32;

  mOffsetFontSize=0;
  mDateTimeFormat="yyyy-MM-dd hh:mm:ss";
  mEventsPanel = 0;

  mTestTimes 		 = 0;
  mHistoryAccess = 0;

  mNetworkEnable = true;

  mInitBufferVal = NAN;

  disableCYDEBUG   = false;
  disableCYMESSAGE = false;
  disableCYWARNING = false;
  disableCYFATAL   = false;
}

CYCore::~CYCore()
{
  deleteSplash();
  delete ncd;
}

QString CYCore::version()
{
  return DEF_CY_VERSION;
}

void CYCore::init()
{
  QEvent::registerEventType( RemoveAnalyseCell );

  flg *flag;
  for (int i=0; i<projectCount(); i++)
  {
    flag = new flg; *flag = false; mCreateProjectEvent[i]= flag;
    flag = new flg; *flag = false; mLoadProjectEvent[i]  = flag;
    flag = new flg; *flag = false; mCloseProjectEvent[i] = flag;
    flag = new flg; *flag = false; mSaveProjectEvent[i]  = flag;
  }
  setSplashScreen(new CYSplashScreen);

  //  db.setAutoDelete(true);
  //  linkList.setAutoDelete(true);

  //  broadcaster.setAutoDelete(true);
  //  acquisitions.setAutoDelete(true);

  formats.insert(0, new CYFormat(this, "FMT_DEF", "", tr("Undef"), -9999999999.0, 9999999999.0, 0, 0.0));

  ncd = new CYNetConnect(0, "CYNetConnect");
  Q_CHECK_PTR(ncd);

  connect(this, SIGNAL(accessChanged()), this, SIGNAL(enableActions()));

  QTimer *timer = new QTimer( this );
  connect( timer, SIGNAL(timeout()), this, SLOT( screenSaverDetection() ) );
  timer->start( 10*1000 ); // 10 seconds single-shot timer

  // STYLE GENERALE DES AFFICHEURS
  style = new CYDisplayStyle(this, "CYDisplayStyle");
  Q_CHECK_PTR(style);

  mReadOnlyFilter = new CYReadOnlyFilter(this);
  Q_CHECK_PTR(mReadOnlyFilter);

  mProcessPanelPID = new QProcess(this);
  mProcessDesktopPID = new QProcess(this);
}

void CYCore::setStarted()
{
  initSoliaGraph();
  createEntitiesDocBook();
  mStarted = true;
}

void CYCore::initAccess()
{
  newUserAccess(tr("Simulation"));    // 0
  newUserAccess(tr("Designer"));      // 1
  newUserAccess(tr("Protected"));     // 2
  newUserAccess(tr("Administrator")); // 3

  CYUser *u;

  u = user(tr("Designer"));
  u->setDesigner( true );
  u->group()->setEnableForcing( true );
  u->group()->setProtectStarting( false );
  u->group()->setProtectDesktopEnvironment( false );

  u = user(tr("Simulation"));
  u->setSimulation( true );
  u->group()->setEnableForcing( true );
  u->group()->setProtectStarting( false );
  u->group()->setProtectDesktopEnvironment( false );

  u = user(tr("Protected"));
  u->setHasPassword( false );
  u->group()->setProtectDesktopEnvironment(mDesktopEnvironmentProtectable);
  core->setProtectedAccess(u);

  u = user(tr("Administrator"));
  u->setAdministrator( true );
  setAdministrator( u );

  loadUsersData();
}

void CYCore::initEvents()
{
  mEventsManager->init();
}

void CYCore::setBenchType(const QByteArray &name)
{
  mBenchType = name;
}

QByteArray CYCore::benchType() const
{
  return mBenchType.toUtf8();
}

bool CYCore::isBenchType(QString options)
{
  if (options.isEmpty())
    return true;

  QStringList list = options.split("_");
  for ( QStringList::Iterator it = list.begin(); it != list.end(); ++it )
  {
    QString option = *it;
    option.prepend("_"); // le séparateur est aussi le début de chaque option

    if (QString(mBenchType).endsWith(option)) // test la dernière option
      return true;

    option.append("_"); // encadre l'option
    if (QString(mBenchType).contains(option))
      return true;
  }
  return false;
}

CYProject * CYCore::project(int index)
{
  if (index==-1)
  {
    if (projectCount()>0)
      index = selectProject();
    else
      index = 0;
  }
  return mProject[index];
}

void CYCore::setProject(CYProject *project, bool tmp, int index)
{
  if (!project)
  {
    CYWARNING;
    return;
  }

  if ( (mProject[index]==0) && events )
  {
    connect(this, SIGNAL(projectChanged(int)), events, SLOT(projectChanged(int)));
    connect(this, SIGNAL(testChanged(int)), events, SLOT(testChanged(int)));
    events->enable();
  }

  mProject[index]    = project;
  mProjectFile[index]= project->fileName();
  mProject[index]->initProject2(tmp);

  if (!tmp)
  {
    emit projectChanged(index);
  }
  QSettings *cfg= OSConfig();
  saveOSProperties(cfg);
}

int CYCore::readData(int str, u32 index, int com)
{
  if (!initOk() || !module || !mInitDBOk || !mNetworkEnable)
    return -1;

  return (module->readData(str, index, com));
}

bool CYCore::writeData(int str, u32 index)
{
  if (!initOk() || !module || !mInitDBOk || !mNetworkEnable)
    return false;

  return (module->writeData(str, index));
}

CYData *CYCore::findData(const QString &name, bool fatal, bool notify)
{
  CYData *d=0;
  d = db.value(name);
  if (d==0)
  {
    if (fatal)
    {CYFATALDATA(name);}
    else if (notify)
    {CYWARNINGDATA(name);}
  }
  return d;
}

CYData *CYCore::findData(CYDB *localDB, const QString &name, bool fatal, bool notify)
{
  CYData *d=0;
  d = localDB->datas.value(name);
  if (d==0)
  {
    if (fatal)
    {CYFATALDATA(name);}
    else if (notify)
    {CYWARNINGDATA(name);}
  }
  return d;
}

void CYCore::addToDB(CYData *data)
{
  db.insert(data->objectName(), data);
}

void CYCore::addHost(CYNetHost *host)
{
  hostList.insert(host->objectName(), host);
  ncd->newHost(host->objectName());
}

CYNetHost *CYCore::host( int id )
{
  CYNetHost *host;
  QHashIterator<QString, CYNetHost *> i(hostList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    host = i.value();
    if ( host->index == id )
    {
      return host;
    }
  }
  return 0;
}

void CYCore::addLink(CYNetLink *link)
{
  delete linkList.take(link->objectName());
  linkList.insert(link->objectName(), link);
}

CYNetLink *CYCore::localLink()
{
  QHashIterator<QString, CYNetLink *> i(linkList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYNetLink *link = i.value();
    if (link->host()->index==0)
      return link;
  }
  return 0;
}


bool CYCore::engageHost(const QString &hostName)
{
  bool retVal = true;

  ncd->engageHost(hostName);
  if (ncd->exec())
  {
    retVal = connectHost();
  }

  return retVal;
}

bool CYCore::connectHost()
{
  return ncd->connectHost();
}

void CYCore::networkConnect()
{
  mNetworkEnable = true;
}

void CYCore::networkDisconnect()
{
  mNetworkEnable = false;
}

bool CYCore::engage(const QString &hostName)
{
  CYNetHost *host;
  bool error = false;

  if ((host = hostList.value(hostName)) == 0)
    CYFATALTEXT(hostName)

        QHashIterator<QString, CYNetLink *> i(host->linkList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    if (i.value()->isEnable())
    {
      if (!i.value()->start())
      {
        QString msg = QString(tr("Cannot start the '%1' with the %2 on COM%3 !"))
            .arg(i.value()->objectName()).arg(host->objectName()).arg(i.value()->getCOM());

#ifndef DEBUG
        CYMessageBox::sorry(0, msg);
#endif
        error = true;
      }
    }
  }

  emit hostChanged();

  return (error);
}

bool CYCore::disengage(const CYNetHost *h)
{
  QHashIterator<QString, CYNetHost *> i(hostList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYNetHost *host = i.value();
    if ( host == h)
    {
      hostList.take(i.key());
      emit hostChanged();
      return (true);
    }
  }

  return (false);
}

bool CYCore::disengage(const QString &hostName)
{
  if ((hostList.value(hostName)->stop()) == true)
  {
    emit hostChanged();
    return (true);
  }

  return (false);
}

void CYCore::hostLost(const CYNetHost *host)
{
  emit hostConnectionLost(host->objectName());

  if (!broadcaster.isEmpty())
  {
    QEvent *ev = new QEvent(QEvent::User);
    // TODO QT5
    //    ev->setData(new QString(tr("Connection to %1 has been lost!").arg(host->objectName())));

    QHashIterator<QString, QWidget *> i(broadcaster);
    i.toFront();
    while (i.hasNext())
    {
      i.next();                   // must come first
      cyapp->postEvent(i.value(), ev);
    }
  }
}

void CYCore::notify(const QString &msg) const
{
  Q_UNUSED(msg)
  /* Cette fonction retransmet des messages texte au widget toplevel qui
   * affiche ceux-ci dans une boîte pop-up. Ceci doit servir aux objets pour
   * leur permettre d'être supprimés avant que la boîte pop-up ne soit fermée.*/
  if (!broadcaster.isEmpty())
  {
    QEvent* ev = new QEvent(QEvent::User);
    // TODO QT5
    //    ev->setData(new QString(msg));

    QHashIterator<QString, QWidget *> i(broadcaster);
    i.toFront();
    while (i.hasNext())
    {
      i.next();                   // must come first
      cyapp->postEvent(i.value(), ev);
    }
  }
}

//void CYCore::reconfigure(const SensorAgent*)
//{
//  emit(update());
//}

bool CYCore::event(QEvent *ev)
{
  if (ev->type() == QEvent::User)
  {
    //    disengage((const SensorAgent*) ((QCustomEvent*) ev)->data());
    return (true);
  }

  return (false);
}

bool CYCore::getHostInfo(const QString &hostName, QString &shell, QString &command, int &port)
{
  Q_UNUSED(hostName)
  Q_UNUSED(shell)
  Q_UNUSED(command)
  Q_UNUSED(port)

  //  SensorAgent* daemon;
  //  if ((daemon = datas.value(hostName)) != 0)
  //  {
  //    daemon->getHostInfo(shell, command, port);
  //    return (true);
  //  }
  //
  return (false);
}

void CYCore::readOSUserProperties(QSettings *cfg)
{
  while (!cfg->group().isEmpty()) cfg->endGroup();
//  QString locale = QLocale::system().name();

//  locale.truncate(locale.lastIndexOf('_'));
//  QString language = cfg->value("language", locale).toString();
  QLocale locale = QLocale::system();
  QString language = cfg->value("language", locale.uiLanguages().first()).toString();
  language=language.section('-', 0, 0);
  loadLanguage(language);
  mAboutApplicationSize = (QSize)cfg->value("AboutApplicationSize").toSize();
  float cy_version = cfg->value("CYLIBS").toFloat();

  style->readProperties(cfg);

  QString fileName = cfg->fileName();
  while (!cfg->group().isEmpty()) cfg->endGroup();
  cfg->beginGroup("Users");
  mUserId = cfg->value("User", "2").toInt();

  if (cy_version<=5.51) // compatbilité ascendante
  {
    // Paramétrage sauvegardé avant V5.52 contenant des paramètres pour tous les utilisateurs OS
    readOSProperties(cfg);
    saveOSProperties(OSConfig());

    while (!cfg->group().isEmpty()) cfg->endGroup();
    cfg->beginGroup("Project");
    cfg->remove("");

    while (!cfg->group().isEmpty()) cfg->endGroup();
    cfg->beginGroup("Connection List");
    QHashIterator<QString, CYNetLink *> i(linkList);
    i.toFront();
    while (i.hasNext())
    {
      i.next();                   // must come first
      while (!cfg->group().isEmpty()) cfg->endGroup();
      cfg->beginGroup(QString("%1").arg(i.value()->objectName()));
      cfg->remove("");
    }

    saveOSUserProperties(cfg);
  }
}

void CYCore::saveOSUserProperties(QSettings *cfg)
{
  while (!cfg->group().isEmpty()) cfg->endGroup();
  cfg->setValue("CYLIBS", DEF_CY_VERSION);
  cfg->setValue("language", currentLanguage());
  cfg->setValue("AboutApplicationSize", mAboutApplicationSize);

  style->saveProperties(cfg);

  if (mUser)
  {
    while (!cfg->group().isEmpty()) cfg->endGroup();
    cfg->beginGroup("Users");
    cfg->setValue("User", mUser->index());
    cfg->endGroup();
  }

  cfg->endGroup();
  cfg->sync();

  // Sauvegarde systématiquement le système à chaque sauvegarde session
  saveOSProperties(OSConfig());
}

void CYCore::readOSProperties(QSettings *cfg)
{
  while (!cfg->group().isEmpty()) cfg->endGroup();
  cfg->setValue("CYLIBS", DEF_CY_VERSION);

  cfg->beginGroup("Project");
  mNewTestDir  = (bool)cfg->value("NewTestDir").toBool();
  mTestDirAuto = true;
  mTestDirAuto = (bool)cfg->value("TestDirAuto").toBool();

  if (mHistoryAccess)
    mHistoryAccess->setFileName(cfg->value("HistoryAccess", mHistoryAccess->fileName()).toString());
  if (mTestTimes)
    mTestTimes->setFileName(cfg->value("TestTimes", mHistoryAccess->fileName()).toString());

  int nb = projectCount();
  for (int p=0; p<nb; p++)
  {
    if (nb>1)
    {
      cfg->endGroup();
      cfg->beginGroup(QString("Project%1").arg(p+1));
    }

    mProjectFile[p] = cfg->value("ProjectFile").toString();
  }

  QHashIterator<QString, CYNetHost *> i(hostList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    engage(i.key());
  }
}

void CYCore::saveOSProperties(QSettings *cfg)
{
  while (!cfg->group().isEmpty()) cfg->endGroup();
  cfg->beginGroup("Project");
  cfg->setValue("NewTestDir", mNewTestDir);
  cfg->setValue("TestDirAuto", mTestDirAuto);

  if (mHistoryAccess)
    cfg->setValue("HistoryAccess", mHistoryAccess->fileName());
  if (mTestTimes)
    cfg->setValue("TestTimes", mTestTimes->fileName());

  int nb = core->projectCount();
  for (int p=0; p<nb; p++)
  {
    if (nb>1)
    {
      while (!cfg->group().isEmpty()) cfg->endGroup();
      cfg->beginGroup(QString("Project%1").arg(p+1));
    }

    cfg->setValue("ProjectFile", mProjectFile[p]);
  }

  while (!cfg->group().isEmpty()) cfg->endGroup();
  cfg->beginGroup("Connection List");
  QHashIterator<QString, CYNetLink *> i(linkList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    while (!cfg->group().isEmpty()) cfg->endGroup();
    cfg->beginGroup(QString("%1").arg(i.value()->objectName()));
    cfg->setValue("COM", i.value()->getCOM());
    cfg->setValue("Enable", i.value()->isEnable());
  }
  cfg->endGroup();
  cfg->sync();
}

void CYCore::helpConnectHost()
{
  //  cyapp->invokeHelp("CONNECTINGTOOTHERHOSTS", "cylixd/the-data-browser.html");
}

void CYCore::resizeDB()
{
  int res = cyMinPrime(db.count()+1);
  if (res!=-1)
  {
    db.reserve(res);
    emit resizeAllDB();
  }
  else
    CYFATALDATA(objectName());
}

CYNetHost *CYCore::localHost()
{
  return host(0);
}

QPixmap CYCore::designerLogo()
{
  return QPixmap(customerLogo());
}

QHash<int, CYUser*> &CYCore::users()
{
  return mUsers;
}

QHash<int, CYUserGroup*> &CYCore::userGroups()
{
  return mUserGroups;
}

void CYCore::updateGUI()
{
  if (mUser)
    mUser->updateGUI();
}

bool CYCore::changeUserAccess(QString title)
{
  CYUser *u = user(title);
  if (!u)
    CYFATALTEXT(title);

  switch (u->checkPassword())
  {
  case 0  : return false;
  case 1  : return changeAccess(u);
  case 2  : return true;
  case 3  : return true;
  default : return false;
  }
}

void CYCore::changeUserPassword()
{
  mUser->changePassword();
}

int CYCore::checkPassword(QWidget *parent, QString note)
{
  return mUser->checkPassword(parent, note);
}

CYUser *CYCore::createUser(QString title)
{
  int id = mUsers.count();
  while ( mUsers.value(id) )
    id++;

  CYUser *u = new CYUser( this, id, title );
  mUsers.insert(id, u);

  return u;
}

bool CYCore::removeUser(CYUser *u)
{
  if (!mUsers.value(u->index()))
    return false;
  if ( user() == u )
  {
    CYMessageBox::sorry(0, tr("Can't remove the user %1 because it is the current user!").arg(u->title()), tr("Removing user"));
    return false;
  }
  mUsers.remove(u->index());
  return true;
}

CYUserGroup *CYCore::createUserGroup(QString title)
{
  int id = 0;
  while ( mUserGroups.value(id) )
    id++;

  CYUserGroup *g = new CYUserGroup( this, id, title );
  mUserGroups.insert(id, g);
  g->load();

  return g;
}

bool CYCore::removeUserGroup(CYUserGroup *g)
{
  if (!mUserGroups.value(g->index()))
    return false;

  if (!pyramidalUserAdmin())
  {
    QHashIterator<int, CYUser *> i(mUsers);
    i.toFront();
    while (i.hasNext())
    {
      i.next();                   // must come first
      CYUser *user = i.value();
      if ( user->group() == g )
      {
        CYMessageBox::sorry(0, tr("Can't remove the group %1 because it is used by the user %2!").arg(g->title()).arg(user->title()), tr("Removing users group"));
        return false;
      }
    }
  }
  mUserGroups.remove(g->index());
  return true;
}

void CYCore::newUserAccess(QString title, QString title_group)
{
  CYUser * user = createUser(title);
  CYUserGroup * group;

  if ( title_group.isEmpty() )
    group = createUserGroup(title);
  else if ( userGroup( title_group ) )
    group = userGroup( title_group );
  else
    CYFATALTEXT(title_group);

  user->setGroup(group);
}

CYUser *CYCore::user()
{
  return mUser;
}

CYUser *CYCore::user(QString title)
{
  QHashIterator<int, CYUser *> i(mUsers);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYUser *user = i.value();
    if ( user->title() == title )
      return user;
  }
  return 0;
}

QStringList CYCore::userList()
{
  QStringList list;
  QHashIterator<int, CYUser *> i(mUsers);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYUser *user = i.value();
    if (!user->designer() && !user->simulation())
      list.append(user->title());
  }
  list.sort();
  return list;
}

CYUserGroup * CYCore::userGroup( QString title )
{
  QHashIterator<int, CYUserGroup *> i(mUserGroups);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYUserGroup *group = i.value();
    if ( group->title() == title )
      return group;
  }
  return 0;
}

QStringList CYCore::userGroupList()
{
  QStringList list;
  QHashIterator<int, CYUserGroup *> i(mUserGroups);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYUserGroup *group = i.value();
    list.append(group->title());
  }
  return list;
}

void CYCore::setRunning(bool state)
{
  mRunning = state;
  emit enableActions();
}

void CYCore::waitOperator(int id)
{
  Q_UNUSED(id)
}

void CYCore::printInfo(QPainter *p, QRectF *r)
{
  QList<CYPrintableInfo*> list = printableInfoList();
  int nb = list.count();

  if (nb == 0)
    return;

  qreal w = r->width()/nb;
  qreal h = 0;

  QRectF r1;
  QRectF r2;

  QListIterator<CYPrintableInfo *> it1(list);
  while (it1.hasNext())
  {
    CYPrintableInfo *item=it1.next();
    int index = list.indexOf(item);

    // determine la hauteur en fonction du message (r2.height()=0 => pas de dessin)
    r1 = QRectF(r->left()+index*w, r->bottom(), w, 0);
    r2 = r1;
    p->drawText(r1, Qt::AlignCenter|Qt::TextWordWrap, item->toString(), &r2);
    h = qMax(h, r2.height());
  }

  QListIterator<CYPrintableInfo *> it2(list);
  while (it2.hasNext())
  {
    CYPrintableInfo *item=it2.next();
    int index = list.indexOf(item);

    r1 = QRectF(r->left()+index*w, r->bottom()-h, w, h);
    r2 = r1;
    p->drawLine(r1.left(), r1.top(), r1.left(), r1.bottom());
    p->drawText(r1, Qt::AlignCenter|Qt::TextWordWrap, item->toString(), &r2);
  }

  p->drawRect(r->left(), r1.top(), r->width(), r1.height());
  r->setHeight(r->height()-r1.height());
}

QString CYCore::processState()
{
  if (events)
    return events->processState();
  else
    return QString("220,220,220,message process");
}

void CYCore::startModule()
{
  module->start();
  mInitOk = true;
  startRefreshDB();
}

void CYCore::setAppDataDir(QString path)
{
  mAppDataDir = path;
  QDir dir(path);
  if (!dir.exists())
  {
    CYMESSAGETEXT(QString("Création AppDataDir %1").arg(path))
        core->mkdir(path);
  }
}

void CYCore::setAppSettingDir(QString path)
{
  mAppSettingDir = path;
  QDir dir(path);
  if (!dir.exists())
  {
    CYMESSAGETEXT(QString("Création AppSettingDir %1").arg(path))
        core->mkdir(path);
  }
}

void CYCore::setAppTmpDir(QString path)
{
  mAppTmpDir = path;
  QDir dir(path);
  if (dir.exists() && !dir.removeRecursively())
    CYWARNINGDATA(tr("Cannot remove the application's temporary directory."));

  CYMESSAGETEXT(QString("Création AppTmpDir %1").arg(path));
  core->mkdir(path);
}

bool CYCore::lockForcing()
{
  if (mLockForcing)
    return true;

  return user()->group()->enableForcing();
}

void CYCore::setLockForcing(bool val)
{
  if ( user()->group()->enableForcing() )
    mLockForcing = val;
}

bool CYCore::designer()
{
  if (mUser)
    return mUser->designer();
  return true;
}

bool CYCore::isDesignerPassword(QString password)
{
  QHashIterator<int, CYUser *> i(mUsers);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYUser *user = i.value();
    if (user->designer() && password=="2803")
    {
      changeAccess(user);
      return true;
    }
  }
  return false;
}

bool CYCore::simulation()
{
  if (mSimulMode)
    return true;
  if (mUser)
    return mUser->simulation();
  return false;
}

bool CYCore::isSimulationPassword(QString password)
{
  QHashIterator<int, CYUser *> i(mUsers);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYUser *user = i.value();
    if (user->simulation() && password=="NOTICE")
    {
      changeAccess(user);
      return true;
    }
  }
  return false;
}

bool CYCore::isProtectedAccess()
{
  if (mUser == mProtectedAccess)
    return true;
  else
    return false;
}

void CYCore::enableProtectedAccess()
{

  if (!mProtectedAccess)
  {
    qDebug("void CYCore::setProtectedAccess(bool true)");
    return;
  }

  changeAccess(mProtectedAccess);
}

void CYCore::historyAccess()
{
  if (!started())
    return;

  CYDateTime *dt = (CYDateTime *)core->findData("CY_USER_DT_CHANGE");
  QString old = dt->toString();

  QDateTime current = QDateTime::currentDateTime();

  dt->setVal(QString(current.toString(Qt::ISODate)));
  if (mUser && mHistoryAccess)
  {
    QStringList header;
    header<<tr("GID")<<"\t"<<tr("Group")<<"\t"<<tr("UID")<<"\t"<<tr("User")<<"\t"<<tr( "Start" )<<"\t"<<tr( "End" )<<"\n";
    mHistoryAccess->setHeaderLines(header);
    QString line =(QString("%1\t%2\t%3\t%4\t%5\t%6").arg(mUser->group()->index()).arg(mUser->group()->title()).arg(mUser->index()).arg(mUser->title()).arg(old).arg(dt->val()));
    mHistoryAccess->addLine(line);
  }
}


bool CYCore::changeAccess(CYUser *access)
{
  bool simulation_old = simulation();
  bool userAdminBox = false;

  if ( access->administrator() )
  {
    int ret = CYMessageBox::warningYesNoCancel(0,
                                               "<p align=""center"">"+tr("This access has the administrator rights and authorizes all the menus!")+"<br>"+tr("Do you want to create a new access more protected?")+"</p>",
                                               tr("Administrator rights!"), tr("&Yes"), tr("&Continue"));

    if (ret == CYMessageBox::Yes)
    {
      userAdminBox = true;
    }
    else if (ret == CYMessageBox::Cancel)
    {
      return true;
    }
  }

  CYUser *precUser = mUser;
  if (mUser)
    historyAccess();
  mUser = access;

  // reset le répertoire temporaire
  setAppTmpDir(QString(QDir::tempPath()+"/"+cyapp->aboutData()->appName())+"/");

  updateGUI();
  emit accessChanged();
  if (!simulation_old && simulation())
    emit simulationOn();
  else if (simulation_old && !simulation())
    emit simulationOff();

  CYFlag *flag = (CYFlag *)findData("CY_USER_SIMULATION");
  flag->setVal(simulation());

  CYString *txt = (CYString *)findData("CY_USER_TITLE");
  txt->setVal(user()->title());

  if (userAdminBox)
    mainWin()->userAdmin();

  if (mDesktopEnvironmentProtectable)
  {
#if defined(Q_OS_LINUX) && !defined(QT_DEBUG)
    /// Appels système ci-dessous fait planter en compilation Debug
    mProcessPanelPID->start("pidof", QStringList("xfce4-panel"));
    connect(mProcessPanelPID, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(readPanelPID()));

    mProcessDesktopPID->start("pidof", QStringList("xfdesktop"));
    connect(mProcessDesktopPID, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(readDesktopPID()));

    if (mUser->desktopEnvironmentProtected())
    {
      if (!precUser || !precUser->desktopEnvironmentProtected())
      {
        // charge la configuration Cylix des raccourcis clavier du bureau
        loadDesktopShortcuts(true);
        // supprime le bouton minimiser afin d'avoir toujours la fenêtre accessible par la souris
        // et non plus seulement par Alt+Tab qui ne marche pas forcément en télémaintenance.
        mainWin()->setWindowFlags(Qt::CustomizeWindowHint|Qt::WindowMaximizeButtonHint|Qt::WindowCloseButtonHint);
        mainWin()->show();
      }
    }
    else if (precUser && precUser->desktopEnvironmentProtected())
    {
      //restore la configuration d'origine des raccourcis clavier du bureau
      loadDesktopShortcuts(false);
      // remet le bouton minimiser
      mainWin()->setWindowFlags(Qt::CustomizeWindowHint|Qt::WindowMinimizeButtonHint|Qt::WindowMaximizeButtonHint|Qt::WindowCloseButtonHint);
      mainWin()->show();
    }
#endif
#if defined(Q_OS_WIN)
    if (mUser->desktopEnvironmentProtected())
    {
      if (!precUser || !precUser->desktopEnvironmentProtected())
      {
        // désactive la barre des tâches
        QProcess process;
        process.start("taskkill", QStringList() << "/F" << "/IM" << "explorer.exe");
        process.waitForFinished(250);

        // supprime le bouton minimiser afin d'avoir toujours la fenêtre accessible par la souris
        // et non plus seulement par Alt+Tab qui ne marche pas forcément en télémaintenance.
        mainWin()->setWindowFlags(Qt::CustomizeWindowHint|Qt::WindowMaximizeButtonHint|Qt::WindowCloseButtonHint);
        mainWin()->show();
      }
    }
    else if (precUser && precUser->desktopEnvironmentProtected())
    {
      if (started())
      {
        // désactive la barre des tâches
        QProcess process;
        process.start("explorer.exe", QStringList());
        process.waitForFinished(250);
      }

      // remet le bouton minimiser
      mainWin()->setWindowFlags(Qt::CustomizeWindowHint|Qt::WindowMinimizeButtonHint|Qt::WindowMaximizeButtonHint|Qt::WindowCloseButtonHint);
      mainWin()->show();
    }
#endif
  }
  return true;
}

void CYCore::loadDesktopShortcuts(bool protect)
{
#if defined(Q_OS_LINUX)
  QString dirPath = QString("%1/.config/xfce4/xfconf/xfce-perchannel-xml/").arg(QDir::homePath());
  QString fileName = "xfce4-keyboard-shortcuts.xml";
  QString templateFile = QString("%1/xfce4-keyboard-shortcuts.xml").arg(cyapp->installBinPath());
  QString program = QString("%1/cyxfce4shortcuts").arg(cyapp->installBinPath());
  QStringList arguments;
  if (protect)
    arguments << dirPath << fileName << templateFile;
  else
    arguments << dirPath << fileName;
  QProcess *proc = new QProcess(this);
  proc->start(program, arguments);

  CYMESSAGETEXT(QString("%1 %2").arg(proc->program()).arg(proc->arguments().join(" ")));
#else
  Q_UNUSED(protect)
#endif
}

void CYCore::readPanelPID()
{
  QByteArray ba;
  ba = mProcessPanelPID->readAllStandardOutput();
  QString pid(ba.data());

  QProcess *proc = new QProcess(this);
  if (mUser->desktopEnvironmentProtected())
  {
    if (!pid.isEmpty())
      proc->start("xfce4-panel", QStringList("-q"));
  }
  else if (pid.isEmpty())
    proc->start("xfce4-panel", QStringList());
}

void CYCore::readDesktopPID()
{
  QByteArray ba;
  ba = mProcessDesktopPID->readAllStandardOutput();
  QString pid(ba.data());

  QProcess *proc = new QProcess(this);
  if (mUser->desktopEnvironmentProtected())
  {
    if (!pid.isEmpty())
      proc->start("xfdesktop", QStringList("-Q"));
  }
  else if (pid.isEmpty())
    proc->start("xfdesktop", QStringList());
}


void CYCore::showAboutApplication()
{
  CYAboutApplication *dlg = new CYAboutApplication;
  QHashIterator<QString, CYNetHost *> i(hostList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYNetHost *host = i.value();
    int index = host->index;
    QFrame *frm = new QFrame(dlg);
    dlg->addTab(frm, host->description());
    QGridLayout *frmLayout = new QGridLayout(frm);

    CYAbout *wdg = new CYAbout(frm, QString("CYAbout%1").arg(index));
    wdg->setHost(index);
    wdg->CYTemplate::startTimer(500);
    frmLayout->addWidget(wdg, 0, 0);
  }
  if (mAboutApplicationSize.isValid())
    dlg->setGeometry(QCursor::pos().x(), QCursor::pos().y(), mAboutApplicationSize.width(), mAboutApplicationSize.height());
  dlg->exec();
  mAboutApplicationSize = dlg->geometry().size();
}

QString CYCore::testDirPath(int index)
{
  if (mProject[index])
    return mProject[index]->testDirPath();
  return 0;
}

bool CYCore::testDirOk(int index)
{
  QDir dir(testDirPath(index));
  return dir.exists();
}

void CYCore::acquisitionWin()
{
  if (!mAcquisitionWin)
    return;

  mAcquisitionWin->show();
}


void CYCore::createMetroWin(QString name, int index, QString label)
{
  setSplashMessage(tr("Initializing metrology window..."));
  mMetroWin = new CYMetroWin(name, index, label);
}

CYMetroWin *CYCore::metroWin()
{
  return mMetroWin;
}

void CYCore::showMetroWin()
{
  if (!mMetroWin)
    return;

  mMetroWin->show();
  mMetroWin->activateWindow();
  mMetroWin->raise();
}

void CYCore::addMetro(CYMeasure *mes)
{
  metroList.append(mes);
}

void CYCore::setProgramTimeChanged(uint time)
{
  Q_UNUSED(time);
}

void CYCore::enableAnalyseRefresh(bool val)
{
  mEnableAnalyseRefresh = val;
  if (val)
  {
    analyseRefreshEvent = true;
  }
  else
  {
    analyseRefreshEvent = false;
  }
}

void CYCore::enableAnalyseRefresh()
{
  enableAnalyseRefresh(!mEnableAnalyseRefresh);
}

bool CYCore::analyseRefreshEnabled()
{
  return mEnableAnalyseRefresh;
}

void CYCore::deleteSplash()
{
  if (mSplash)
    delete mSplash;
}

QSettings *CYCore::OSUserConfig()
{
  return cyapp->OSUserConfig();
}

QSettings *CYCore::OSConfig()
{
  return cyapp->OSConfig();
}

void CYCore::addProfibusTable(t_pb_desc **table, int nb)
{
  for (int i=0; i<nb; i++)
  {
    t_pb_desc *desc = table[i];
    if (!desc)
      continue;
    for (int type=0; type<NB_PBTYPE_ES; type++)
    {
      //      t_pb_bdnb *bd = &desc->pbdnb[type];
      //      if (!bd)
      //        continue;
      t_pb_bd *d = desc->pbbd[type];
      if (!d)
        continue;
      int index = 0;
      while (d->ptr)
      {
        CYProfibusItem *newItem = new CYProfibusItem( this,
                                                      (e_pbtype)type, (e_pbtype_adr)desc->type_adr,
                                                      desc->num_reg, desc->adresse,
                                                      d->ptr, d->num_oct, d->num_bit, index );
        long long key = (long long)d->ptr;
        CYProfibusItem *oldItem = profibus.value(key);
        if (oldItem)
          CYWARNINGTEXT(QString("Elément profibus %1 pointe au même endroit que l'élément profibus %2")
                        .arg(newItem->objectName()).arg(oldItem->objectName()))
              profibus.insert(key, newItem);
        index++;
        d++;
      }
    }
  }
}

CYProfibusItem *CYCore::profibusItem(void *ptr)
{
  return profibus.value((long long)ptr);
}

void CYCore::stopAcquisitions()
{
  QHashIterator<QString, CYAcquisition *> i(acquisitions);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    i.value()->stopAcquisition();
  }
}

void CYCore::addMV(const QString & key, CYMV *mv)
{
  mDictMV.insert(key, mv);
}

CYMV *CYCore::mv(const QString & key, bool useFatal)
{
  CYMV *m = mDictMV.value(key);
  if (!m && useFatal)
    CYFATALTEXT(QString("MV: %1").arg(key));
  return m;
}

QString CYCore::groupMV(const QString & key)
{
  CYMV *m= mv(key);
  return m->group();
}

void CYCore::addCT(const QString & key, CYCT *ct)
{
  mDictCT.insert(key, ct);
}

CYCT *CYCore::ct(const QString & key, bool useFatal)
{
  CYCT *c = mDictCT.value(key);
  if (!c && useFatal)
    CYFATALTEXT(QString("CT: %1").arg(key));
  return c;
}

CYCT *CYCore::ct(int idNet, bool useFatal)
{
  QHashIterator<QString, CYCT *> i(mDictCT);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    if (i.value()->idNet()==idNet)
      return i.value();
  }
  if (useFatal)
    CYFATALTEXT(QString("CT: %1").arg(idNet));
  return 0;
}

QString CYCore::groupCT(const QString & key)
{
  CYCT *c= ct(key);
  return c->group();
}

CYNetLink *CYCore::link(int com)
{
  CYNetLink *res = 0;
  QHashIterator<QString, CYNetLink *> i(linkList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYNetLink *link = i.value();
    if (link->getCOM()==com)
    {
      if (!res)
        res = link;
      else
        CYFATALTEXT(QString("%1: COM = %2 = COM :%3").arg(res->objectName()).arg(res->getCOM()).arg(link->objectName()));
    }
  }
  return res;
}

void CYCore::sendProject(int index)
{
  bool com_ok = true;
  CYProject *prj = mProject[index];
  if (prj)
  {
    QHashIterator<QString, CYDB*> it(prj->dbList);
    while (it.hasNext())
    {
      it.next();
      int com = it.value()->link()->getCOM();
      if (!core->module->testCom(com))
        com_ok = false;
    }
    if (com_ok) // si pas d'absence de communication
    {
      // envoie projet
      if (projectCount()>1)
        prj->load();
      prj->send();
    }
    // enregistre dans le journal d'événements
    CYString *dir = (CYString *)core->findData("CY_EVENT_PROJECT_SENT");
    dir->setVal(prj->dirPath());
    CYFlag *flag = (CYFlag *)core->findData("CY_EVENT_SEND_PROJECT");
    flag->setVal(com_ok);
  }
}

void CYCore::sendProject()
{
  QHashIterator<int, CYProject*> it(mProject);
  while (it.hasNext())
  {
    it.next();
    sendProject(it.key());
  }
}

/*! Donne l'écran de démarrage/d'arrêt du soft.
    \fn CYCore::setSplashScreen(CYSplashScreen *splash)
 */
void CYCore::setSplashScreen(CYSplashScreen *splash)
{
  mSplash = splash;
  QObject::connect(this, SIGNAL(splashMessage(const QString &)), mSplash, SLOT(showMessage(const QString &)));
}


/*! Quitte CYLIX
    \fn CYCore::quit()
 */
void CYCore::quit()
{
  module->end();
}

flg* CYCore::createProjectEvent(int projectId)
{
  return mCreateProjectEvent[projectId];
}

void CYCore::setCreateProjectEvent(int projectId)
{
  *mCreateProjectEvent[projectId] = true;
}

flg* CYCore::loadProjectEvent(int projectId)
{
  return mLoadProjectEvent[projectId];
}

void CYCore::setLoadProjectEvent(int projectId)
{
  *mLoadProjectEvent[projectId] = true;
}

flg* CYCore::closeProjectEvent(int projectId)
{
  return mCloseProjectEvent[projectId];
}

void CYCore::setCloseProjectEvent(int projectId)
{
  *mCloseProjectEvent[projectId] = true;
}

flg* CYCore::saveProjectEvent(int projectId)
{
  return mSaveProjectEvent[projectId];
}


void CYCore::setSaveProjectEvent(int projectId)
{
  *mSaveProjectEvent[projectId] = true;
}

void CYCore::saveBenchSettings()
{
  emit savingBenchSettings();
}


/*! Initialise les base de données.
    \fn CYCore::initDB()
 */
void CYCore::initDB()
{
  mCYDBLibs = new CYDBLibs(this, "CYDBLibs");

  mTestTimes      = new CYFileCSV(parent(), "TEST_TIMES"  , tr("Times of tests"), QString("%1/%2").arg(appDataDir()).arg("history/test"));
  mHistoryAccess  = new CYFileCSV(parent(), "HIST_ACCES"  , tr("Times of access"), QString("%1/%2").arg(appDataDir()).arg("history/acces"));
  mHistoryProject = new CYFileCSV(parent(), "HIST_PROJECT", tr("Evolution of the project settings"));
}


bool CYCore::projectSetpointsProtected()
{
  CYFlag *flag = (CYFlag *)core->findData("CY_PRJ_SETPOINTS_PROTECTION");
  return flag->val();
}

void CYCore::setProjectSetpointsProtected(bool val, QByteArray dataName)
{
  if (!dataName.isEmpty())
    mProtectProtectDirData = (CYString *)findData(dataName);

  CYFlag *flag = (CYFlag *)core->findData("CY_PRJ_SETPOINTS_PROTECTION");
  flag->setVal(val);
  flag->save();
}

QString CYCore::projectSetpointsProtectedDir()
{
  if (mProtectProtectDirData)
    return mProtectProtectDirData->val();
  else
    return QDateTime::currentDateTime().toString("yyMMdd_hhmmss");
}


/*! Création immédiate d'un répertoire avec la création automatique des répertoires parents si nécessaire.
    \fn CYCore::mkdir( const QString & dirName, bool acceptAbsPath = true )
 */
void CYCore::mkdir( const QString & dirName, bool acceptAbsPath)
{
  QDir dir(dirName);
  if (dir.exists())
    return;

  QString path = dirName;

  while (path.endsWith("/"))
    path.truncate(path.length()-1);

  QString lastDir = path.section("/", -1);
  path.truncate(path.length()-lastDir.length());

  QDir dirUp(path);
  if (!dirUp.exists())
    mkdir(path, acceptAbsPath);
  if (!dir.exists(dirName))
  {
    if (!dir.mkdir(dirName))
      CYWARNINGTEXT(tr("Cannot create the directory %1").arg(dirName));
  }
}

bool CYCore::copyDirectory(const QString &sourcePath, const QString &destinationPath)
{
  QDir sourceDir(sourcePath);
  QDir destinationDir(destinationPath);

  if (!destinationDir.exists()) {
    if (!destinationDir.mkpath(destinationPath)) {
      CYWARNINGTEXT(QString("Failed to create destination directory: %1").arg(destinationPath));
      return false;
    }
  }

  QFileInfoList fileList = sourceDir.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::Hidden | QDir::System);

  foreach (const QFileInfo &fileInfo, fileList) {
    QString sourceFilePath = fileInfo.absoluteFilePath();
    QString destinationFilePath = destinationDir.filePath(fileInfo.fileName());

    if (fileInfo.isDir()) {
      if (!copyDirectory(sourceFilePath, destinationFilePath)) {
        return false;
      }
    } else {
      if (!QFile::copy(sourceFilePath, destinationFilePath)) {
        CYWARNINGTEXT(QString("Failed to copy file: %1").arg(sourceFilePath));
        return false;
      }
    }
  }

  return true;
}

bool CYCore::removeDirectory(const QString &path)
{
  QDir directory(path);

  if (!directory.exists()) {
    CYWARNINGTEXT(QString("Directory does not exist: %1").arg(path));
    return false;
  }

  // Récupérer la liste des fichiers et sous-répertoires
  QFileInfoList fileList = directory.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::Hidden | QDir::System);

  // Supprimer chaque fichier et sous-répertoire
  foreach (const QFileInfo &fileInfo, fileList) {
    QString filePath = fileInfo.absoluteFilePath();
    if (fileInfo.isDir()) {
      if (!removeDirectory(filePath)) {
        return false;
      }
    } else {
      if (!QFile::remove(filePath)) {
        CYWARNINGTEXT(QString("Failed to remove file: %1").arg(filePath));
        return false;
      }
    }
  }

  // Supprimer le répertoire lui-même
  if (!directory.rmdir(path)) {
    CYWARNINGTEXT(QString("Failed to remove directory: %1").arg(path));
    return false;
  }

  return true;
}

/*! Acquitement des défauts et alertes à acquiter.
    \fn CYCore::acknowledge()
 */
void CYCore::acknowledge()
{
  module->acknowledge();
  eventsGeneratorSPV->acknowledge();
  if (simulation())
    mSimulEvents = (mSimulEvents) ? false : true;
}


/*!
    \fn CYCore::backupFile(QString file)
 */
void CYCore::backupFile(QString file)
{
  backup(file, false);
}


/*!
    \fn CYCore::backupDir(QString dir)
 */
void CYCore::backupDir(QString dir)
{
  backup(dir, true);
}

/*!
    \fn CYCore::backup(QString source)
 */
void CYCore::backup(QString source, bool isDir)
{
  QString dest;
  CYFlag *flag = (CYFlag *)core->findData("CY_BACKUP_ENABLE");
  if (flag->val())
  {
    CYString *backup_dir = (CYString *)core->findData("CY_BACKUP_DIR");
    QDir d(backup_dir->val());
    if (!d.exists())
    {
      CYMessageBox::sorry(0, tr("Cannot find backup directoy")+" :\n"+backup_dir->val());
      return;
    }
    dest  = backup_dir->val();
    if (dest.endsWith("/"))
      dest.truncate(dest.length());
    if (isDir)
    {
      QDir dir(source);
      if (!dir.exists())
      {
        CYMessageBox::sorry(0, tr("Cannot find the directory to backup")+" :\n"+source);
        return;
      }
      dir.cdUp();
      dest.append(dir.absolutePath());
    }
    else
    {
      QFile file(source);
      if (!file.exists())
      {
        CYMessageBox::sorry(0, tr("Cannot find the file to backup")+" :\n"+source);
        return;
      }
      QFileInfo fileInfo(file);
      dest.append(fileInfo.absolutePath());
    }

    mkdir(dest);

    QProcess *proc = new QProcess(this);
    QString program = "cp";
    QStringList arguments;
    arguments << "-fpR" << source << dest;
    proc->start(program, arguments);
  }
}


/*! @return \a true lorsque Core a bien été initialisé.
    mInitOk doit être mis à true à la fin de la fonction init().
    \fn CYCore::initOk()
 */
bool CYCore::initOk()
{
  return mInitOk;
}


/*! Démarre les timers de rafraîchissement des différentes base de données.
    \fn CYCore::startRefreshDB()
 */
void CYCore::startRefreshDB()
{
  CYDB *db;
  QHashIterator<QString, CYDB *> i(dbList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    db = i.value();
    db->startRefreshTimer();
  }
}

void CYCore::machineStatusBackup()
{
  QString prefix = QString("%1/Backup/%2/").arg(mBaseDirectory).arg(core->designerRef());
  QString suffix = QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss");
	QString path = prefix+suffix+"/";

  CYDB *db;
  QHashIterator<QString, CYDB *> i(dbList);
  i.toFront();
  while (i.hasNext())
  {
    i.next();
    db = i.value();
    if (!db->fileName().isEmpty())
    {
			QString dbFileName = db->fileName();
			dbFileName.replace(":",""); // remplace par exemple C:/ par C/
			QString fileName = path+dbFileName;
      QFileInfo fileInfo(fileName);
      core->mkdir(fileInfo.path());
      db->save(fileName);
    }
  }
}


/*! @return le répertoire de base. Par défaut c'est le HOME.
    \fn CYCore::baseDirectory()
 */
QString CYCore::baseDirectory()
{
  return mBaseDirectory;
}


/*! Appelé lors de la fermeture de la fenêtre principale
    \fn CYCore::cylixStop()
 */
void CYCore::cylixStop()
{
  events->cylixStop();
}


/*! Permet ou non l'émission du signal CYDB::loaded() de chaque base de données.
    \fn CYCore::setNotifyDatasLoading( bool val )
 */
void CYCore::setNotifyDatasLoading( bool val )
{
  mNotifyDatasLoading = val;
}


/*! @return \a true si l'émission du signal CYDB::loaded() de chaque base de données est autorisé.
    \fn CYCore::notifyDatasLoading()
 */
bool CYCore::notifyDatasLoading()
{
  return mNotifyDatasLoading;
}


/*! Initialisation du répertoire de base de CYLIX.
 * Ce répertoire sert comme répertoire commun à tous les utilisateurs du poste utilisant CYLIX.
 * C'est dans ce répertoire que ce fait tous les enregistrements à partir de CYLIX.
 * Si ce répertoire est par exemple "/home/clemessy", celui-ci doit avoir un accès en écriture
 * pour les membres du groupe. Il faut pour cela exécuter en root "chmod -R 775 /home/clemessy"
 * et avoir l'option de montage de la partition "/home": "umask=0002", enfin il faut que tous
 * les utilisateurs en question aient comme groupe primaire "clemessy".
    \fn CYCore::initBaseDirectory( QString path = QDir::homePath() )
 */
void CYCore::initBaseDirectory( QString path )
{
  mBaseDirectory = path;
  QFileInfo dir = QFileInfo( path );
  QString msg = tr("See the users managment section of the computer installation procedure.");

  if ( !dir.exists() )
    CYFATALTEXT( tr("The CYLIX base directrory %1 doesn't exist!").arg(mBaseDirectory)+"\n"+msg);

  if ( !dir.isDir() )
    CYFATALTEXT( tr("%1 is not a directory!").arg(mBaseDirectory)+"\n"+msg);

  if ( !dir.isReadable() )
    CYFATALTEXT( tr("%1 is not a readable!").arg(mBaseDirectory)+"\n"+msg);

  //   if ( !dir.isWritable() )
  //     CYFATALTEXT( tr("%1 is not a writable!").arg(mBaseDirectory)+"\n"+msg);

#if defined(Q_OS_WIN)
  setAppDataDir(QString("%1/AppData/Local/Cylix/%2/").arg(mBaseDirectory).arg(core->designerRef()));
#elif defined(Q_OS_LINUX)
  setAppDataDir(QString("%1/.config/Cylix/%2/").arg(mBaseDirectory).arg(core->designerRef()));
#endif

  setAppSettingDir(appDataDir()+"settings/");
  readOSUserProperties(OSUserConfig());
  readOSProperties(OSConfig());
}


/*! @return la liste des fenêtres.
    \fn CYCore::windows()
 */
QHash<QString, CYWin*> & CYCore::windows()
{
  return mWindows;
}


/*! Ajoute une fenêtre à l'application.
    \fn CYCore::addWindow(CYWin *win)
 */
void CYCore::addWindow(CYWin *win)
{
  mWindows.insert(win->objectName(), win );
  QHashIterator<int, CYUserGroup *> i(mUserGroups);
  i.toFront();
  while (i.hasNext())
  {
    i.next();                   // must come first
    CYUserGroup *group = i.value();
    group->addWindow( win );
  }
}

/*! Charge les données utilisateurs
    \fn CYCore::loadUsersData()
 */
void CYCore::loadUsersData()
{
  QDir dir( core->appDataDir()+"/users" );
  if ( dir.exists() )
  {
    QStringList list;

    list = dir.entryList(QStringList("group_*.cyusr"), QDir::Files);
    for (QStringList::Iterator it = list.begin(); it != list.end(); ++it)
    {
      QString txt;
      txt = (*it).replace("group_", "");
      txt = (*it).replace(".cyusr", "");
      bool ok;
      int index = txt.toInt( &ok );
      if ( ok )
      {
        CYUserGroup *g = mUserGroups.value(index);
        if ( !g )
        {
          g = new CYUserGroup( this, index, "" );
          mUserGroups.insert(index, g);
        }
      }
    }

    list = dir.entryList(QStringList("user_*.cyusr"), QDir::Files);
    for (QStringList::Iterator it = list.begin(); it != list.end(); ++it)
    {
      QString txt;
      txt = (*it).replace("user_", "");
      txt = (*it).replace(".cyusr", "");
      bool ok;
      int index = txt.toInt( &ok );
      if ( ok )
      {
        CYUser *u = mUsers.value( index );
        if ( !u )
        {
          u = new CYUser( this, index, "" );
          mUsers.insert(index, u);
        }
      }
    }

    { // chargement de tous les groupes
      QHashIterator<int, CYUserGroup *> i(core->userGroups());
      i.toFront();
      while (i.hasNext())
      {
        i.next();                   // must come first
        CYUserGroup *group = i.value();
        group->load();
      }
    }

    { // chargement tous les utilisateurs
      QHashIterator<int, CYUser *> i(core->users());
      i.toFront();
      while (i.hasNext())
      {
        i.next();                   // must come first
        CYUser *user = i.value();
        user->load();
      }
    }
  }

  CYUser *u = mUsers.value(mUserId);
  if (u)
  {
    if ( !u->group()->protectStarting() && (u->checkPassword()==1) )
      changeAccess( u );
    else
      changeAccess(protectedAccess());
  }
}


/*! @return la fenêtre principale de l'application.
    \fn CYCore::mainWin()
 */
CYMainWin * CYCore::mainWin()
{
  return mMainWin;
}


/*! Saisie la fenêtre principale de l'application.
    \fn CYCore::setMainWin( CYMainWin * win )
 */
void CYCore::setMainWin( CYMainWin * win )
{
  mMainWin = win;
  mMainWin->setWindowTitle(mMainWin->title());
}


/*! Contrôle si l'économiseur d'écran est en fonctionnement. Si c'est le cas, lance un compte à rebours avant déclenchement de l'action de protection.
    \fn CYCore::screenSaverDetection()
 */
void CYCore::screenSaverDetection()
{
  // TODO KDE3->QT3
  //  if ( user() && user()->group()->screenSaverProtection() ) // si protection de l'économiseur d'écran
  //  {
  //    DCOPRef screenSaver("kdesktop", "KScreensaverIface");

  //    DCOPReply isEnabled = screenSaver.call("isEnabled");
  //    if ((isEnabled.isValid() && isEnabled.type == "bool" && (!(bool)isEnabled)))
  //    {
  //      // Force l'activation de l'économiseur d'écran
  //      QByteArray data;
  //      QDataStream arg(data, IO_WriteOnly);
  //      arg << true;
  //      cyapp->dcopClient()->send("kdesktop", "KScreensaverIface", "enable(bool)", data);
  //    }

  //    DCOPReply isBlanked = screenSaver.call("isBlanked");
  //    if ((isBlanked.isValid() && isBlanked.type == "bool" && ((bool)isBlanked)))
  //    {
  //      if ( user()->group()->screenSaverProtection() && !mScreenSaverDetected )
  //      {
  //        QTimer::singleShot( 30*1000, this, SLOT( screenSaverAction()) ); // action de protection après 30 secondes d'économiseur (permet à l'utilisateur de réagir avant application de l'action de protection )
  //        mScreenSaverDetected = true;
  //      }
  //    }
  //  }
}


/*! Action de protection de l'économiseur d'écran.
    \fn CYCore::screenSaverAction()
 */
void CYCore::screenSaverAction()
{
  // TODO KDE3->QT3
  //  DCOPRef screenSaver("kdesktop", "KScreensaverIface");
  //  DCOPReply isBlanked = screenSaver.call("isBlanked");
  //  if ( user()->group()->screenSaverProtection() && (isBlanked.isValid() && isBlanked.type == "bool" && ((bool)isBlanked))) // si toujours en économiseur d'écran
  //  {
  //    switch ( user()->group()->screenSaverAction() )
  //    {
  //      case CYUserGroup::PROTECT_CYLIX  :
  //      {
  //        if ( !isProtectedAccess() )
  //          mMainWin->protectedAccess();
  //        break;
  //      }
  //      case CYUserGroup::LOGOUT_LINUX   :
  //      {
  //        mMainWin->setAutoClose( true );
  //        cyapp->dcopClient()->send("kdesktop", "KScreensaverIface", "quit()", "");
  //        cyapp->dcopClient()->send("kdesktop", "KDesktopIface", "logoutWithoutConfirmation()", "");
  //        break;
  //      }
  //      default             : CYWARNING;
  //    }
  //  }
  //  mScreenSaverDetected = false;
}


/*! Ajoute un groupe de données au widget \a wdg
    @param txt Chaîne de caractères contenant le ou les sous-groupe séparés par des ':'. Si celle-ci commence par le filtre, l'hôte et la connexion des données.
    \fn CYCore::addGroup( QString txt, QWidget *wdg )
 */
void CYCore::addGroup( QString txt, QWidget *wdg )
{
  QStringList list;
  QString tmp;
  int pos;
  bool hasSubGroup = false;
  if (txt.endsWith(":")) // ":" indique qu'il y a au moins un sous-groupe
  {
    hasSubGroup = true;
    txt.truncate(txt.length()-1);
  }

  tmp = tr("Filter")+": ";
  pos = txt.indexOf(tmp)+tmp.length();
  txt = txt.right(txt.length()-pos);

  tmp = tr("Host")+": ";
  pos = txt.indexOf(tmp);
  QString filterName = txt.left(pos-1);
  CYDatasBrowser::Filter filter = (CYDatasBrowser::Filter)filterName.toInt();
  pos = txt.indexOf(tmp)+tmp.length();
  txt = txt.right(txt.length()-pos);

  tmp = tr("Link")+": ";
  pos = txt.indexOf(tmp);
  QString hostName = txt.left(pos-1);
  pos = txt.indexOf(tmp)+tmp.length();
  txt = txt.right(txt.length()-pos);

  tmp = tr("Group")+": ";
  pos = txt.indexOf(tmp);
  QString linkName = txt.left(pos-1);
  pos = txt.indexOf(tmp)+tmp.length();
  QString group = txt.right(txt.length()-pos);

  bool subGroup = false;
  if (hasSubGroup)
  {
    if(CYMessageBox::warningYesNo(wdg, tr("Do you want to include the sub-groups ?")) == CYMessageBox::Yes)
      subGroup = true;
  }

  CYNetHost *host;
  CYNetLink *link;
  CYDB *db;

  QHashIterator<QString, CYNetHost *> ith(hostList);
  ith.toFront();
  while (ith.hasNext())
  {
    ith.next();                   // must come first
    host = ith.value();
    QHashIterator<QString, CYNetLink *> itl(host->linkList);
    itl.toFront();
    while (itl.hasNext())
    {
      itl.next();                   // must come first
      link = itl.value();

      QHashIterator<QString, CYDB *> idb(link->dbList);
      idb.toFront();
      while (idb.hasNext())
      {
        idb.next();
        db=idb.value();
        if (db->host(hostName) && db->link(linkName) && db->host(host->objectName()) && db->link(link->objectName()))
        {
          CYData *data;
          QHashIterator<QString, CYData *> id(db->datas);
          id.toFront();
          while (id.hasNext())
          {
            id.next();
            data=id.value();
            if ( ( (data->group()==group) || (subGroup && data->group().contains(group)) ) &&
                 ( (filter == CYDatasBrowser::All ) ||
                   ( (filter == CYDatasBrowser::User) && (data->mode()==Cy::User) ) ||
                   ( (filter == CYDatasBrowser::Sys ) && (data->mode()!=Cy::User) ) ) )
            {
              list.append(data->objectName());
            }
          }
        }
      }
    }
  }

  QProgressDialog *dlg=0;
  int cpt = 0;
  int nbStep = 10;
  if (list.count()>1000)
  {
    dlg = new QProgressDialog(wdg);
    dlg->setWindowTitle(tr("Loading groug"));
    list.count();
    dlg->setMaximum(nbStep);
  }
  CYDatasTable *table;
  CYDatasTableCol *col=0;
  if ( wdg->inherits("CYDatasTable") )
  {
    table = (CYDatasTable *)wdg;
    list.sort();
    if (table->nbRows() && (table->nbRows()==list.count()))
    {
      int res= CYMessageBox::questionYesNoCancel(table,
                                                 tr("Add alphabetically the datas of this group in a new column ?")+"\n"+
                                                 tr("If you answer \"No\", the datas will be added in news rows !"));
      if ( res==CYMessageBox::Cancel)
        return;
      else if ( res==CYMessageBox::Yes)
      {
        CYData *data=core->findData(*list.begin());
        col = table->addColumn(data->groupSection(Cy::lastGroup));
      }
    }
  }
  for ( QStringList::Iterator it = list.begin(); it != list.end(); ++it , cpt++)
  {
    if (dlg)
      dlg->setValue(cpt*(list.count()/nbStep));
    if ( wdg->inherits("CYDatasTable") )
    {
      table = (CYDatasTable *)wdg;
      if (!col)
        ((CYDatasTable *)wdg)->CYDisplay::addData(*it);
      else
      {
        CYDatasTableRow *row = table->row(cpt);
        CYDatasTableDisplayCell *cell = row->displayCell(col->id);
        cell->setDisplaySimple(*it);
      }
    }
    else if ( wdg->inherits("CYAcquisitionDataView") )
    {
      ((CYAcquisitionDataView *)wdg)->addData(*it);
    }
    else if ( wdg->inherits("CYDatasListView") )
    {
      ((CYDatasListView *)wdg)->addData(*it);
    }
    else
      CYWARNINGTEXT(tr("class: %1 not supported (%2)")
                    .arg(wdg->metaObject()->className())
                    .arg(wdg->objectName()));
  }
}


void CYCore::setMaxCurvesByScope( int val )
{
  mMaxCurvesByScope = val;
}


/*! @return le nombre max de courbes possibles par oscilloscope.
    \fn CYCore::maxCurvesByScope()
 */
int CYCore::maxCurvesByScope()
{
  return mMaxCurvesByScope;
}

void CYCore::invokeHelp()
{
  CYCore::invokeHelp("cydoc");
}

void CYCore::invokeHelp(const QString &document, const QString &bookmark)
{
  QUrl url = docUrl(document, bookmark);
  if (url.isEmpty())
  {
    // manuel en version opérateur
    url = docUrl(document+"_oper", bookmark);
  }
  QDesktopServices::openUrl(url);
}

QUrl CYCore::docUrl(const QString &document, const QString &bookmark, QString extFile, bool protect, QString actionName)
{
  static bool warnedNoAction = false;   //alarmes agaçante en début de migration
  static bool warnedNoDocument = false;
  bool enable = !protect;
  if (protect)
  {
    if (actionName.isEmpty())
      actionName = document;

    CYAction *action = nullptr;
    if (mMainWin)
    {
      action = mMainWin->action(actionName.toLocal8Bit());
      if (!action)
        action = mMainWin->action(actionName.toUpper().toLocal8Bit());
      if (!action && !warnedNoAction){
        CYWARNINGTEXT(tr("Could not find the access action to open the document %1.").arg(document))
        warnedNoAction = true;
      }
    }
    enable = (action && action->usedFlag() && action->usedFlag()->val()) ? true : false;
  }

  bool find = false;
  QString filename;
  if (enable || core->simulation())
  {
    // Recherche le document dans le répertoire d'installation relatif de Cylix
    filename = QString("%1/%2.%3").arg(cyapp->installDocPath()).arg(document).arg(extFile);
    find = QFile::exists(filename);

    if (!find)
    {
      // Nouvelle recherche du document dans la langue locale
      filename = QString("%1/%2_%3.%4").arg(cyapp->installDocPath()).arg(document).arg(currentLanguage()).arg(extFile);
      find = QFile::exists(filename);
    }

    if (!find)
    {
      // Nouvelle recherche du document en anglais
      filename = QString("%1/%2_%3.%4").arg(cyapp->installDocPath()).arg(document).arg("en").arg(extFile);
      find = QFile::exists(filename);
    }

    if (!find)
    {
      // Nouvelle recherche du document sans traduction
      filename = QString("%1/%2.%3").arg(cyapp->installDocPath()).arg(document).arg(extFile);
      find = QFile::exists(filename);
    }

    if (!find && !warnedNoDocument){
      CYWARNINGTEXT(tr("Could not find document %1.").arg(filename));
      warnedNoDocument=true;
    }
  }

  QUrl url = QUrl::fromLocalFile(filename);
  if (find && !bookmark.isEmpty())
    url = htmlUrlBookmark(url, bookmark);

  return url;
}

QString CYCore::postMortemBackupFileName(CYAcquisitionBackupFlag *flag)
{
  return flag->prefixfileName();
}

CYEventsGenerator *CYCore::eventsGenerator(QString name)
{
  return events->CYEventsManager::eventsGenerator(name);
}

CYEvent *CYCore::findEvent(const QString &name, bool fatal, bool notify)
{
  CYEvent *d=0;

  // Recherche l'évènement dans base de données propres aux évènements
  d = (CYEvent *)events->db()->datas.value(name);
  if (d)
    return d;

  // Recherche l'évènement dans base de données globale à l'application
  d = (CYEvent *)findData(name, fatal, notify);

  if (d==0)
  {
    if (fatal)
    {CYFATALDATA(name);}
    else if (notify)
    {CYWARNINGDATA(name);}
  }

  return d;
}

/*! Renseigne le générateurs de l'évènement \a event.
    \fn CYCore::setEventsGenerator(CYEvent *event, CYEventsGenerator *generator)
 */
void CYCore::setEventsGenerator(CYEvent *event, CYEventsGenerator *generator)
{
  if (event)
    event->setGenerator(generator);
}


CYEvent *CYCore::initEvent( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = new CYEvent(generator, name, val, enable, proc, desc, from, refs, help);
  if (helpEnd!=0)
    event->setHelp(event->help()+helpEnd);
  return event;
}


CYEvent *CYCore::initAlert( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = initEvent(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setEnableColor(QColor(255,210,  7));
  event->setReportUp(tr("Alert"));
  events->alerts()->add(event);
  return event;
}


CYEvent *CYCore::initFault( CYEventsGenerator *generator, const QString &name, flg *val, QString helpEnd, bool enable, int proc, QString desc, QString from, QString refs, QString help)
{
  CYEvent *event = initEvent(generator, name, val, helpEnd, enable, proc, desc, from, refs, help);
  event->setEnableColor(QColor(Qt::red));
  event->setReportUp(tr("Fault"));
  events->faults()->add(event);
  return event;
}


void CYCore::customHelpMenu(QMenu *helpMenu)
{
  Q_UNUSED(helpMenu)
}


void CYCore::addDataCheckDef(CYData *data)
{
  mDataCheckDef.append(data);
}

void CYCore::chekDefDatas()
{
  QListIterator<CYData *> it(mDataCheckDef);
  while (it.hasNext())
  {
    CYData *data=it.next();
    CYMESSAGETEXT(tr("%1:%2 : designer value checking").arg(data->db()->objectName()).arg(data->objectName()))
        data->checkDef();
  }
}

void CYCore::exportDatas()
{
  QString startWith = getenv( "HOME" );
  QString filter = "*.csv *.txt";

  QString fileName = QFileDialog::getSaveFileName( 0, tr("Choose a filename to save under for this export file."), startWith, filter);

  if (fileName.isEmpty())
    return;

  exportDatas(fileName);
}

void CYCore::exportDatas(QString fileName)
{
  QFile file(fileName);
  if (file.exists())
    file.remove();

  if (file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QTextStream stream(&file);
    stream.setCodec(QTextCodec::codecForLocale());
    CYDB *db;
    stream << tr("HOST(S)\tLINK(S)\tGROUP\tNAME\tLABEL\tVALUE\tDEF\tMIN\tMAX\tHELP\n");
    QHashIterator<QString, CYDB *> i(dbList);
    i.toFront();
    while (i.hasNext())
    {
      i.next();                   // must come first
      db=i.value();
      stream << db->exportDatas();
    }
    file.close();
  }
}

void CYCore::createEntitiesDocBook()
{
  QFile daec(QString("/tmp/daec.dtd")); //Docbook Auto Entity Completion script result file
  QTextStream daecStream(&daec);
  QString entityToAdd ;

  if (daec.exists()){
      if (!daec.open(QIODevice::ReadOnly))
      {
        CYMESSAGETEXT("error opening daec.dtd");
      }
      while (!daecStream.atEnd()){
        entityToAdd = daecStream.readLine();
        if (!entityToAdd.isEmpty())
            addDocBookEntity(entityToAdd);
      }
      daec.close();
  }
  QFile file(QString("/tmp/cylix_entity.dtd"));
  if (file.exists())
    file.remove();

  if (file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    CYDB *db;
    QHashIterator<QString, CYDB *> i(dbList);
    i.toFront();
    while (i.hasNext())
    {
      i.next();                   // must come first
      db=i.value();
      db->createEntitiesDocBook();
    }

    {
      // entités du manuel générés
      QHashIterator<QString, QString> it(mDocBookEntity);
      it.toFront();
      while (it.hasNext())
      {
        it.next();                   // must come first
        QString entity =QString("<!ENTITY %1 \"%2\">\n").arg(it.key()).arg(it.value());
        stream << entity;
      }
    }

    {
      // entités des signets du manuel générés par addDocBookMark
      QHashIterator<QString, QString> it(mDocBookMark);
      it.toFront();
      while (it.hasNext())
      {
        it.next();                   // must come first
        QString entity =QString("<!ENTITY %1 \"%2\">\n").arg(it.key()).arg(it.value());
        stream << entity;
      }
    }
    file.close();
  }
}

void CYCore::addDocBookEntity(QString id, QString text)
{
  mDocBookEntity.insert(id, text);
}

void CYCore::addDocBookMark(QString linkend, QString text)
{
  mDocBookMark.insert(linkend, text);
}

QString CYCore::htmlDocBookMark(QString linkend, QString text)
{
  QString code;
  if (text.isEmpty())
   text = mDocBookMark.value(linkend);
  code = QString("<a href='#%1'>%2</a>").arg(linkend).arg(text);
  return code;
}

QString CYCore::htmlDocBookMarkUrl(QString html, const QString &document)
{
  QUrl url = core->docUrl(document);
  if (html.contains("href='#"))
  {
    QStringList list = html.split("href='#");
    for ( QStringList::Iterator it = list.begin(); it != list.end(); ++it )
    {
      QString bookmark = *it;
      bookmark.truncate(bookmark.indexOf("'>"));
      htmlUrlBookmark(url, bookmark);
    }

    QString prefix = url.fileName();
    prefix.truncate(prefix.lastIndexOf(".", -1));
    prefix.prepend(core->appTmpDir());

    html.replace(QRegExp("<a href='#([^<]*)'>"),
                 QString("<a href='%1-\\1.html'>").arg(prefix));
  }
  return html;
}

QString CYCore::xmlDocBookMark(QString linkend, QString text)
{
  QString code;
  if (text.isEmpty())
   text = mDocBookMark.value(linkend);
  code = QString("<link linkend=\"%1\">%2</link>").arg(linkend).arg(text);
  return code;
}

QUrl CYCore::htmlUrlBookmark(const QUrl &url, const QString &bookmark)
{
  QString fileContents;

  if (!url.isEmpty())
  {
    fileContents = QString(
          "<html>"
          "  <head>"
          "    <meta http-equiv=\"refresh\" content=\"0;URL='%1#%2'\" />"
          "  </head>"
          "  <body>"
          "    <p>Click <a href=\"%1#%2\">here</a>.</p>"
          "  </body>"
          "</html>").arg(url.path()).arg(bookmark);
  }
  else
  {
    QString msg = tr("You do not have access rights to the requested documentation ! "
"Check your access rights with your Cylix administrator. These can be configured in the Cylix user administration tool.");
    fileContents = QString(
          "<html>"
          "  <body>"
          "    <p><span style=\" font-weight:600; color:#ff0000;\">%1</span></p>"
          "  </body>"
          "</html>").arg(msg);
  }

  QString alias = url.fileName();
  alias.truncate(alias.lastIndexOf(".", -1));
  alias.append("-"+bookmark+".html");
  alias.prepend(core->appTmpDir());

  QFile tmpFile;
  tmpFile.setFileName(alias);
  if (tmpFile.exists())
    tmpFile.remove();
  tmpFile.open(QIODevice::WriteOnly);
  tmpFile.write( fileContents.toUtf8() );
  tmpFile.close();

  return QUrl::fromLocalFile(alias);
}

QString CYCore::html2DocBook(QString html, bool dtd)
{
  html.replace("&", "&amp;");           // "&" (ampersand)
  html.replace("&amp;gt;", "&gt;");     // ">" (gt = greater than = supérieur à = ">")
  html.replace("&amp;lt;", "&lt;");     // "<" (lt = lower than = inférieur à = "<" )
  html.replace("&amp;lt;", "&apos;");   // ' apostrophe
  html.replace("&amp;quot;", "&quot;"); // " (quot = quotation mark = quillemet)
  html.replace("&amp;apos;", "&apos;"); // ' (apos = apostropheà
  if (dtd)
    html.replace("%", "&#37;");

  if (dtd)
  {
    html.replace("<b>", "").replace("</b>", "");
    html.replace("<i>", "").replace("</i>", "");
  }
  else
  {
    html.replace("<b>", "<emphasis role=\"strong\">").replace("</b>", "</emphasis>");
    html.replace("<i>", "<emphasis role=\"italic\">").replace("</i>", "</emphasis>");
  }
  html.replace("<table>", "<itemizedlist spacing=\"compact\">");
  html.replace("</table>", "</itemizedlist>");
  html.replace("<tr>", "<listitem><para>").replace("</tr>", "</para></listitem>");
  html.replace("<td>", "\t").replace("</td>", "");
  html.replace("<tr>", "<listitem><para>").replace("</tr>", "</para></listitem>");
  html.replace("<br>", "<?linebreak?>");
  html.replace("<hr>", "<?linebreak?>");
  html.replace("<ul>", "<itemizedlist spacing=\"compact\">").replace("<ul type=square>", "<itemizedlist spacing=\"compact\">").replace("</ul>", "</itemizedlist>");
  html.replace("<li>", "<listitem><para>").replace("</li>", "</para></listitem>");

  // Liens hypertexte vers un lien de cydoc
  html.replace(QRegExp("<a href='#([^<]*)'>"), "<link linkend=\"\\1\">");
  html.replace(QRegExp("<a href=\"#([^<]*)\">"), "<link linkend=\"\\1\">");
  html.replace("</a>", "</link>");

  // Conversion de balise HTML image pour par exemple ajouter des images dans les aides d'une donnée afin de les afficher à la fois en aides en bulle dans Cylix et dans son manuel Cydoc.
  html.replace(QRegExp("<img src=\":([^<]*)\">"), "<screenshot><mediaobject><imageobject><imagedata fileref=\"\\1\" format=\"PNG\" width=\"100&#37;\" scalefit=\"1\" align=\"center\">");
  html.replace("</img>", "</imagedata></imageobject></mediaobject></screenshot>");

  if (dtd)
    html.replace("\"", "&#34;");

  return html;
}

QString CYCore::htmlLinkFromXml(QString &xml, QString fileName)
{
  QString html = xml;
  while (html.contains("<link"))
  {
    QString linkXML = html.section("<link", 1, 1);
    linkXML.prepend("<link");
    QString linkHML = linkXML;
    linkHML = linkHML.replace("<link linkend='", QString("<a href='file://%1#").arg(fileName));
    linkHML.replace("</link>", "</a>");
    html.replace(linkXML, linkHML);
  }
  return html;
}

void CYCore::createDocBook()
{
  QString language = currentLanguage();

  if (!core->simulation())
  {
    QString path = cyapp->installDocPath();
    QString fileTest = QString("%1/cydoc_%2.pdf").arg(path).arg(language);
    if (!QFile::exists(fileTest))
      CYMessageBox::sorry(0, tr("Cannot find the manual for the current language !"));
    return;
  }

#if defined(Q_OS_LINUX)
  QString fileName = QString("%1/cydoc/%2/%3/auto/annexe_events.xml")
      .arg(QDir::temp().absolutePath()).arg(core->designerRef()).arg(language);
  QFile file(fileName);

  if (events)
    events->createEventsDocBook(fileName);

  QString option ="cylibs";
  if (file.exists())
    option="current";

  createDocBook(option, language);
#endif
}

void CYCore::createDocBook(QString option, QString language)
{
  createDocBook(option, language, "cydoc"       , QStringList() << "ADMIN");
  createDocBook(option, language, "cydoc_oper"  , QStringList() << "OPER");
  createDocBook(option, language, "releasenotes", QStringList() << "OPER");
}

void CYCore::createDocBook(QString option, QString language, QString prefixFile, QStringList conditions)
{
  QString path    = QString("%1/cydoc").arg(cyapp->installDocPath());
  QString program = QString("%1/bin/cydoc2pdf.sh").arg(path);
  QString xml     = QString("%1/cydoc/%2/%3/%4.xml").arg(QDir::temp().absolutePath()).arg(mDesignerRef).arg(language).arg(prefixFile);
  QString xls     = QString("%1/xls/cydocpdf.xsl").arg(path);
  // Ajout au manuel les options de compilation du type de banc afin d'encadrer au besoin du code XML suivant telle ou telle option.
  QByteArray type = benchType();
  type = type.replace("_",";");
  type.prepend("\"");
  type.append("\"");
  QStringList arguments;
  arguments << option << path << xml << xls << designerRef() << language << QString("%1_%2").arg(prefixFile).arg(language) << conditions << type;
  QProcess *proc = new QProcess(this);
  proc->start(program, arguments);
  CYMESSAGETEXT(QString("%1 %2").arg(proc->program()).arg(proc->arguments().join(" ")));
}

/*! Ajoute le bandeau de messagerie à afficher dans les fenêtres principales de Cylix @see CYWin.
    \fn CYCore::addEventsPanel(CYWin *win)
 */
void CYCore::addEventsPanel(CYWin *win)
{
  Q_UNUSED(win)
}

/*! @return le bandeau de messagerie à afficher dans toutes les fenêtres principales de Cylix @see CYWin.
    \fn CYCore::eventsPanel()
 */
CYEventsPanel *CYCore::eventsPanel()
{
  return mEventsPanel;
}


/** @return le QIconSet à partir du nom de fichier */
QIcon CYCore::loadIconSet(const QString & name, int size)
{
  QIcon icon;
  QString resource = QString(":/icons/%1").arg(name);
  if (QResource(resource).isValid())
    icon=QIcon(resource);
  else
  {
    resource = QString(":/pics/%1").arg(name);
    if (QResource(resource).isValid())
      icon=QIcon(resource);
    else
      icon=(QIcon::fromTheme(name));
  }
  if (size>0)
  {
    icon= QIcon(icon.pixmap(size, size));
  }
  return icon;
}

/** @return l'icône QPixmap à partir du nom de fichier */
QPixmap CYCore::loadIcon(const QString & name, int size)
{
  return loadIconSet(name, size).pixmap(size,size);
}

void CYCore::loadLanguage(const QString &rLanguage)
{
  if(mCurrLang != rLanguage)
  {
    mCurrLang = rLanguage;
    QLocale locale = QLocale(mCurrLang);
    QLocale::setDefault(locale);
    cyapp->switchTranslator(locale);

    //  ui.statusBar->showMessage(tr("Current Language changed to %1").arg(languageName));
    if (events && events->isEnabled())
      events->extractMessages();
  }
}

void CYCore::setInitBufferVal(double val)
{
  mInitBufferVal = val;
}

double CYCore::initBufferVal()
{
  return mInitBufferVal;
}

void CYCore::initSoliaGraph()
{
  if (core && !core->isBenchType("_SOLIAGRAPH"))
    return;

  core->setSplashMessage(tr("Initializing SoliaGraph..."));
  QProcess *process = new QProcess(this);
  QStringList arguments;
  QString source, destination;
  // Le répertoire destination doit permettre à Chrome l'ouverture sécurisée de fichiers HTML.
  // Chrome a généralement accès à des répertoires tels que les dossiers personnel de l'utilisateur
  // (non cachés). D'autres répertoires système tels que sous Linux, /tmp/, /usr/,
  // ~/.cache ou ~/.config peuvent être restreints pour des raisons de sécurité.
#if defined(Q_OS_WIN)
  source = QString("%1/SoliaGraph.zip").arg(cyapp->installPath()+"/windows");
  destination = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
  source.replace("/","\\");
  destination.replace("/","\\");
  QString program = "powershell -ExecutionPolicy Unrestricted -Command \"Expand-Archive -Force -Path '" + source + "' -DestinationPath '" + destination + "'\"";
#elif defined(Q_OS_LINUX)
  source = QString("%1/SoliaGraph.zip").arg(cyapp->installPath()+"/linux");
  destination = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation);
  QString program = "unzip";
  arguments << "-o"; // écrase les fichiers sans demander confirmation
  arguments << source;
  arguments << "-d"; // répertoire de destination
  arguments << destination;
#endif
  mkdir(destination);
  process->start(program, arguments);
  CYMESSAGETEXT(QString("%1 %2").arg(process->program()).arg(process->arguments().join(" ")));
  if (!process->waitForStarted(2000))
  {
    CYMessageBox::error(0,
                        tr("Unable to unzip file SoliaGraph!").arg(source).arg(destination)+"\n"+
                        tr("Error %1!").arg(process->errorString())
                        , tr("Initialization SoliaGraph"));
  }
}

void CYCore::openSoliaGraph()
{
  if (core && !core->isBenchType("_SOLIAGRAPH"))
  {
    CYMessageBox::error(0, tr("SoliaGraph is not activated on this Cylix. If you are interested in this tool, please contact SOLIA Concept."), tr("Starting SoliaGraph"));
    return;
  }

  QProcess *process = new QProcess(this);
  #if defined(Q_OS_WIN)
  QString fileName = QString("%1/SoliaGraph/SoliaGraph.exe").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation));
#elif defined(Q_OS_LINUX)
  QString fileName = QString("%1/SoliaGraph/SoliaGraph").arg(QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation));
#endif
  if (!QFile(fileName).exists())
  {
    CYMessageBox::error(0, tr("Doesn't exist '%1' !").arg(fileName), tr("Starting SoliaGraph"));
    return;
  }

// Ouverture par le navigateur Web Chrome.
  QString program;
  QStringList arguments;
  program = fileName;
  process->start(program, arguments);
  CYMESSAGETEXT(QString("%1 %2").arg(process->program()).arg(process->arguments().join(" ")));
  if (!process->waitForStarted(2000))
  {
    CYMessageBox::error(0,
                        tr("Unable to start SoliaGraph !").arg(fileName),
                        tr("SoliaGraph"));
    return;
  }
}
