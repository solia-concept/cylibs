//
// C++ Implementation: cysmtp
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "cysmtp.h"

// QT
#include <qtimer.h>
#include <qapplication.h>
#include <qmessagebox.h>
#include <qregexp.h>
#include <QTextStream>
// CYLIBS
#include <cy.h>
#include "cycore.h"
#include "cystring.h"
#include "cyu16.h"

CYSMTP::CYSMTP( const QString &from, const QString &to, const QString &subject, const QString &body )
{
  socket = new QSslSocket( this );
  connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
  connect(socket, SIGNAL(connected()), this, SLOT(connected() ) );
  connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this,SLOT(errorReceived(QAbstractSocket::SocketError)));
  connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(stateChanged(QAbstractSocket::SocketState)));
  connect(socket, SIGNAL(disconnected()), this,SLOT(disconnected()));

  CYString *servor = (CYString *)core->findData( "CY_EVENTS_MAIL_SMTP_SERVOR" );
  CYU16 *port = (CYU16 *)core->findData( "CY_EVENTS_MAIL_SMTP_PORT" );
  CYString *user = (CYString *)core->findData( "CY_EVENTS_MAIL_SMTP_USER" );
  CYString *password = (CYString *)core->findData( "CY_EVENTS_MAIL_SMTP_PASSWORD" );

  this->host = servor->val();
  this->port = port->val();
  this->user = user->val();
  this->pass = password->simpleDecrypt();
  this->timeout = 5000;

  sendMail(from, to, subject, body);
}


CYSMTP::~CYSMTP()
{
    delete t;
    delete socket;
}

void CYSMTP::sendMail(const QString &from, const QString &to, const QString &subject, const QString &body)
{
    message = QString::fromLatin1( "From: " ) + from +"\n";

    rcpt="";
    QStringList list = to.split(",");
    for ( QStringList::Iterator it = list.begin(); it != list.end(); ++it )
    {
      QString dest;
      if (!rcpt.isEmpty())
        rcpt.append(",");
      dest.append("<");
      dest.append(*it);
      dest.append(">");
      message.append(QString("to: "+ dest + "\r\n"));
      rcpt.append(dest);
    }
    message.append( QString::fromLatin1( "Subject: " ) + subject +"\n");
#if defined(Q_OS_WIN)  // v5.18
    message.append("Content-Type: text/plain;\n\n");
#else
    message.append("Content-Type: text/plain; charset=utf-8\n\n");
#endif
    message.append(body+ "\n");
    message.replace( QString::fromLatin1( "\n" ), QString::fromLatin1( "\r\n" ) );
    message.replace( QString::fromLatin1( "\r\n.\r\n" ),
    QString::fromLatin1( "\r\n..\r\n" ) );

    this->from = from;
    state = Init;
    socket->connectToHostEncrypted(host, port); //"smtp.gmail.com" and 465 for gmail TLS
    t = new QTextStream( socket );

    socket->waitForConnected(timeout);
}

void CYSMTP::stateChanged(QAbstractSocket::SocketState socketState)
{
    qDebug() <<"stateChanged " << socketState;
}

void CYSMTP::errorReceived(QTcpSocket::SocketError socketError)
{
    qDebug() << "error " <<socketError;
}

void CYSMTP::disconnected()
{

    qDebug() <<"disconneted";
    qDebug() << "error "  << socket->errorString();
}

void CYSMTP::connected()
{
    emit status( tr( "Connected to %1" ).arg( socket->peerName() ) );
}

void CYSMTP::readyRead()
{
    QString responseLine;
    do {
        responseLine = socket->readLine();
        response += responseLine;
    } while( socket->canReadLine() && responseLine[3] != ' ' );
    responseLine.truncate( 3 );

    if ( state == Init && responseLine == "220" )
    {
        // banner was okay, let's go on
        *t << "EHLO localhost" <<"\r\n";
        t->flush();

        state = HandShake;
    }
    //No need, because I'm using socket->startClienEncryption() which makes the SSL handshake for you
/*
    else if (state == Tls && responseLine == "250")
    {
        // Trying AUTH
        qDebug() << "STarting Tls";
        *t << "STARTTLS" << "\r\n";
        t->flush();
        state = HandShake;
    }*/
    else if (state == HandShake && responseLine == "250")
    {
        socket->startClientEncryption();
        if(!socket->waitForEncrypted(timeout))
        {
            qDebug() << socket->errorString();
            state = Close;
        }


        //Send EHLO once again but now encrypted

        *t << "EHLO localhost" << "\r\n";
        t->flush();
        state = Auth;
    }
    else if (state == Auth && responseLine == "250")
    {
        // Trying AUTH
        qDebug() << "Auth";
        *t << "AUTH LOGIN" << "\r\n";
        t->flush();
        state = User;
    }
    else if (state == User && responseLine == "334")
    {
        //Trying User
        qDebug() << "Username";
        //GMAIL is using XOAUTH2 protocol, which basically means that password and username has to be sent in base64 coding
        //https://developers.google.com/gmail/xoauth2_protocol
        *t << QByteArray().append(user.toUtf8()).toBase64()  << "\r\n";
        t->flush();

        state = Pass;
    }
    else if (state == Pass && responseLine == "334")
    {
        //Trying pass
        qDebug() << "Pass";
        *t << QByteArray().append(pass.toUtf8()).toBase64() << "\r\n";
        t->flush();

        state = Mail;
    }
    else if ( state == Mail && responseLine == "235" )
    {
        // HELO response was okay (well, it has to be)

        //Apperantly for Google it is mandatory to have MAIL FROM and RCPT email formated the following way -> <email@gmail.com>
        qDebug() << "MAIL FROM:<" << from << ">";
        *t << "MAIL FROM:<" << from << ">\r\n";
        t->flush();
        state = Rcpt;
    }
    else if ( state == Rcpt && responseLine == "250" )
    {
      //Apperantly for Google it is mandatory to have MAIL FROM and RCPT email formated the following way -> <email@gmail.com>
      if (rcpt.contains(","))
      {
        QStringList list = rcpt.split(",");
        QString to = list.takeFirst();
        rcpt = list.join("");
        *t << "RCPT TO:" << to << "\r\n"; //r
        t->flush();
        state = Rcpt;
      }
      else
      {
        *t << "RCPT TO:" << rcpt << "\r\n"; //r
        t->flush();
        state = Data;
      }
    }
    else if ( state == Data && responseLine == "250" )
    {

        *t << "DATA\r\n";
        t->flush();
        state = Body;
    }
    else if ( state == Body && responseLine == "354" )
    {

        *t << message << "\r\n.\r\n";
        t->flush();
        state = Quit;
    }
    else if ( state == Quit && responseLine == "250" )
    {
        *t << "QUIT\r\n";
        t->flush();
        // here, we just close.
        state = Close;
        emit status("Message sent");
    }
    else if ( state == Close )
    {
        //deleteLater();
        //return;
    }
    else
    {
        // something broke.
        QMessageBox::warning( 0, tr( "Simple SMTP client" ), tr( "Unexpected reply from SMTP server:\n\n" ) + response );
        state = Close;
        emit status("Failed to send message");
    }
    response = "";
}
