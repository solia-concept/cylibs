#ifndef CYAPPLICATION_H
#define CYAPPLICATION_H

// QT
#include <QApplication>
#include <QSharedMemory>
#include <QStringList>
#include <QTranslator>

#define cyapp CYApplication::cyApplication()

class QSettings;
class CYApplicationPrivate;
class CYAboutData;

/** @short Remplace KApplication de KDE3.*/
class CYApplication : public QApplication
{
  Q_OBJECT

public:
  /**
   * Constructeur avec analyse des arguments de ligne de commande.
   *
   * @param argc nombre d'arguments de ligne de commande
   *
   * @param argv valeur(s) d'argument de ligne de commande
   *
   * @param aboutData donnéees sur cette application (see CYAboutData)
   *
   * @param GUIenabled Set to false to disable all GUI stuff. This implies
   * no styles either.
   */
 CYApplication(int& argc, char** argv, CYAboutData *aboutData, bool GUIenabled=true);

  virtual ~CYApplication();

  /**
   * @return le pointeur l'application courante.
   *
   * Ceci est similaire au pointeur global QApplication qApp.
   */
  static CYApplication* cyApplication() { return CYApp; }

  /**
   * L'application est-elle restauré à partir du gestionnaire de session ?
   *
   * @return If true, this application was restored by the session manager.
   *    Note that this may mean the config object returned by
   * OSUserConfig() contains data saved by a session closedown.
   * @see OSUserConfig()
   */
  bool isRestored() const { return QApplication::isSessionRestored(); }

  /**
   * @return Un pointeur sur l'objet de configuration spécifique à l'application et à la session en cours
   * @see QSettings
   */
  QSettings* OSUserConfig();
  /**
   * @return Un pointeur sur l'objet de configuration spécifique à l'application et commun aux différentes sessions système (OS)
   * @see QSettings
   */
  QSettings* OSConfig();

  /**
   * @return les données sur l'instance, ou 0 si elle n'ont
   * pas été encore fixées
   */
  CYAboutData *aboutData() const;


  void setAboutData(CYAboutData *about);

  /**
   * Returns a text for the window caption.
   *
   * This may be set by
   * "-caption", otherwise it will be equivalent to the name of the
   * executable.
   * @return the text for the window caption
   */
  QString caption() const;

  /**
   * Builds a caption that contains the application name along with the
   * userCaption using a standard layout.
   *
   * To make a compliant caption
   * for your window, simply do: @p setWindowTitle(cyapp->makeStdCaption(yourCaption));
   *
   * @param userCaption The caption string you want to display in the
   * window caption area. Do not include the application name!
   * @param withAppName Indicates that the method shall include or ignore
   * the application name when making the caption string. You are not
   * compliant if you set this to @p false.
   * @param modified If true, a 'modified' sign will be included in the
   * returned string. This is useful when indicating that a file is
   * modified, i.e., it contains data that has not been saved.
   * @return the created caption
   */
  QString makeStdCaption( const QString &userCaption,
                          bool withAppName=true, bool modified=false ) const;

  bool alreadyExists() const
  {
      return bAlreadyExists;
  }

  bool isMasterApp() const
  {
      return !alreadyExists();
  }

  bool sendMessage(const QString &message);

  void switchTranslator(QTranslator& translator, const QString& filename, const QString &directory);

  /*! \return le répertoire d'installation de l'application.*/
  QString installPath() { return mInstall;}
  /*! \return le répertoire d'installation des fichiers de documentation de l'application */
  QString installDocPath() { return mInstallDoc;}
  /*! \return le répertoire d'installation des fichiers binaires exécutables de l'application */
  QString installBinPath() { return mInstallBin; }
  /*! \return le répertoire d'installation des fichiers modèles CYAS de feuilles d'analyse graphique de l'application */
  QString installCyasPath() { return mInstallCyas; }
  /*! \return le répertoire d'installation des fichiers modèles ODS de l'application */
  QString installOdsPath() { return mInstallOds; }

  /*! \return le répertoire d'installation des fichiers de traduction de l'application */
  QString installLanguagePath() { return mInstallLanguage; }
  /** \return le répertoire d'installation de divers fichiers Linux de l'application */
  QString installLinuxPath() { return mInstallLinux; }
  /** \return le répertoire d'installation de divers fichiers Windows de l'application */
  QString installWindowsPath() { return mInstallWindows; }

public slots:
  void checkForMessage();
  void switchTranslator(QLocale locale);

signals:
  void messageAvailable(const QStringList& messages);

protected:
  /// Current application object.
  static CYApplication *CYApp;

  CYAboutData *mAboutData;

  /** Répertoire d'installation de l'application.*/
  QString mInstall;
  /** Répertoire d'installation des fichiers de documentation de l'application */
  QString mInstallDoc;
  /** Répertoire d'installation des fichiers binaires exécutables de l'application */
  QString mInstallBin;
  /** Répertoire d'installation des fichiers modèles CYAS de feuilles d'analyse graphique de l'application */
  QString mInstallCyas;
  /** Répertoire d'installation des fichiers modèles ODS de l'application */
  QString mInstallOds;
  /** Répertoire d'installation des fichiers de traduction de l'application */
  QString mInstallLanguage;
  /** Répertoire d'installation de divers fichiers Linux. */
  QString mInstallLinux;
  /** Répertoire d'installation de divers fichiers Windows. */
  QString mInstallWindows;

  /** Contient les traductions pour cette application */
  QTranslator mTranslator;
  /** Contient les traductions pour Cylibs */
  QTranslator mTranslatorCy;
  /** Contient les traductions pour qt */
  QTranslator mTranslatorQt;

private:
  void init( bool GUIenabled );
  void init_windows(bool GUIenabled);

private:
  bool mGUIenabled;
  QSettings* mOSUserConfig;
  QSettings* mOSConfig;

  bool bAlreadyExists;
  QSharedMemory *mSharedMemory;
};

#endif // CYAPPLICATION_H
