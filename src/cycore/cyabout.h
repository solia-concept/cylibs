//
// C++ Interface: cyabout
//
// Description: 
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYABOUT_H
#define CYABOUT_H

// CYLIBS
#include <cywidget.h>

class CYNetHost;

namespace Ui {
		class CYAbout;
}

/**
@short Widget "À propos".

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYAbout : public CYWidget
{
Q_OBJECT
public:
    CYAbout(QWidget *parent = 0, const QString &name = 0);

    ~CYAbout();

    /** Saisie l'index de l'hôte de cette vue. */
    virtual void setHost(int index);

public slots:
    /** Met à jour l'hôte distant. */
    virtual void updateHost();

protected:
    int mIndex;
    CYNetHost *mHost;

private:
    Ui::CYAbout *ui;
};

#endif
