//
// C++ Interface: cymainwin
//
// Description:
//
//
// Author: Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CYMAINWIN_H
#define CYMAINWIN_H

// QT
#include <QAction>
// CYLIBS
#include "cywin.h"

/**
@short Fenêtre principale de l'application.

	@author Gérald LE CLEACH <gerald.lecleach@solia-concept.fr>
*/
class CYMainWin : public CYWin
{
  Q_OBJECT

public:
    /** Construction de la fenêtre principale et installation des menus.
      * @param parent   Widget parent.
      * @param name     Nom de la fenêtre.
      * @param index    Index de la fenêtre.
      * @param label    Libellé de la fenêtre.*/
    CYMainWin(QWidget *parent, const QString &name, int index, const QString &label);

    ~CYMainWin();

    virtual void readProperties(QSettings *);
    virtual void saveProperties(QSettings *);

    /** Crée le menu de langues dynamiquement selon les traductions existantes
     * à partir du contenu de mLangPath. */
    virtual void createLanguageMenu(QMenu *menu);

    /** Cet événement est appelé, quand une nouvelle traduction est chargé ou
     * la langue du système change. */
    virtual void changeEvent(QEvent*);

    virtual QString title();

public slots:
    virtual void setAutoClose( bool val );
    /** Change le mot de passe de l'accès courant. */
    virtual void changePassword();
    virtual void changeAccess();
    virtual void userAdmin();
    virtual void protectedAccess();
    /**  Appelé par les actions de menu de langue. */
    virtual void slotLanguageChanged(QAction* action);
    virtual void refreshLanguageMenu();

protected:
    bool mAutoClose;

    /** Chemin des fichiers de langue. Ceci est toujours fixé à / langues. */
    QString mLangPath;
    QMenu *mLanguageMenu;
    QHash<QString, QAction*> mLanguageActions;
};

#endif
