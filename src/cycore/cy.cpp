/***************************************************************************
                          cy.cpp  -  description
                             -------------------
    début                  : ven avr 25 2003
    copyright              : (fr) 2003 par Gérald LE CLEACH
    email                  : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// ANSI
#if defined(Q_OS_LINUX)
#include <stdint.h>
#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <dirent.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <math.h>
#include <signal.h>
#endif
// QT
#include <qapplication.h>
#include <QFile>
#include <QDir>
#include <QString>
// CYLIBS
#include "cy.h"
#include "cycore.h"

int debug=0;

void CYDebug(QString file, int line)
{
  if (core && core->disableCYDEBUG)
    return;
  QString msg = QObject::tr("File %1 at line %2").arg(file).arg(line);
  qDebug("%s", msg.toUtf8().constData());\
}

void CYDebugText(QString file, int line, QString text)
{
  if (core && core->disableCYDEBUG)
    return;
  QString msg = QObject::tr("File %1 at line %2: ").arg(file).arg(line)+text;
  qDebug("%s", msg.toUtf8().constData());\
}

void CYDebugBox(QString file, int line)
{
  if (core && core->disableCYDEBUG)
    return;
  QString msg = QObject::tr("File %1 at line %2 !").arg(file).arg(line);
  QMessageBox::information(nullptr, QObject::tr("CYLIX DEBUG"), msg);
  qWarning("%s", msg.toUtf8().constData());\
}

void CYDebugBoxText(QString file, int line, QString text)
{
  if (core && core->disableCYDEBUG)
    return;
  QString msg = QObject::tr("File %1 at line %2 !").arg(file).arg(line)+"\n"+text;
  QMessageBox::information(nullptr, QObject::tr("CYLIX DEBUG"), msg);
  qWarning("%s", msg.toUtf8().constData());
}

void CYDebugBoxObject(QString file, int line, QString object)
{
  if (core && core->disableCYDEBUG)
    return;
  QString msg = QObject::tr("File %1 at line %2 on object %3 !").arg(file).arg(line).arg(object);
  QMessageBox::information(nullptr, QObject::tr("CYLIX OBJECT DEBUG"), msg);
  qWarning("%s", msg.toUtf8().constData());\
}

void CYDebugBoxData(QString file, int line, QString data)
{
  if (core && core->disableCYDEBUG)
    return;
  QString msg = QObject::tr("File %1 at line %2 for data %3 !").arg(file).arg(line).arg(data);
  QMessageBox::information(nullptr, QObject::tr("CYLIX DATA DEBUG"), msg);
  qWarning("%s", msg.toUtf8().constData());
}

void CYDebugBoxObjectData(QString file, int line, QString object, QString data)
{
  if (core && core->disableCYDEBUG)
    return;
  QString msg = QObject::tr("File %1 at line %2 on object %3 for data %4 !").arg(file).arg(line).arg(object).arg(data);
  QMessageBox::information(nullptr, QObject::tr("CYLIX OBJECT DATA DEBUG"), msg);
  qWarning("%s", msg.toUtf8().constData());
}

void CYMessage(QString file, int line)
{
  if (core && core->disableCYMESSAGE)
    return;
  QString msg = QObject::tr("File %1 at line %2").arg(file).arg(line);
  qDebug("%s", msg.toUtf8().constData());\
}

void CYMessageText(QString file, int line, QString text)
{
  if (core && core->disableCYMESSAGE)
    return;
  QString msg = QObject::tr("File %1 at line %2: ").arg(file).arg(line)+text;
  qDebug("%s", msg.toUtf8().constData());\
}

void CYWarning(QString file, int line)
{
  if (core && core->disableCYWARNING)
    return;
  QString msg = QObject::tr("File %1 at line %2 !").arg(file).arg(line);
  QMessageBox::warning(nullptr, QObject::tr("CYLIX ERROR"), msg);
  qWarning("%s", msg.toUtf8().constData());\
}

void CYWarningText(QString file, int line, QString text)
{
  if (core && core->disableCYWARNING)
    return;
  QString msg = QObject::tr("File %1 at line %2 !").arg(file).arg(line)+"\n"+text;
  QMessageBox::warning(nullptr, QObject::tr("CYLIX ERROR"), msg);
  qWarning("%s", msg.toUtf8().constData());
}

void CYWarningObject(QString file, int line, QString object)
{
  if (core && core->disableCYWARNING)
    return;
  QString msg = QObject::tr("File %1 at line %2 on object %3 !").arg(file).arg(line).arg(object);
  QMessageBox::warning(nullptr, QObject::tr("CYLIX OBJECT ERROR"), msg);
  qWarning("%s", msg.toUtf8().constData());\
}

void CYWarningData(QString file, int line, QString data)
{
  if (core && core->disableCYWARNING)
    return;
  QString msg = QObject::tr("File %1 at line %2 for data %3 !").arg(file).arg(line).arg(data);
  QMessageBox::warning(nullptr, QObject::tr("CYLIX DATA ERROR"), msg);
  qWarning("%s", msg.toUtf8().constData());
}

void CYWarningList(QString file, int line, QStringList list, QString fileName)
{
  if (core && core->disableCYWARNING)
    return;
  QMessageBox msgBox(QMessageBox::Warning, QObject::tr("CYLIX OBJECT DATA ERROR"), QObject::tr("%1 at line %2 : error in sheet %3 for one or multiple datas.").arg(file).arg(line).arg(fileName));
  QString msg;
  QStringListIterator it(list);
  while (it.hasNext()) msg.append(QObject::tr("%1 !\n").arg(it.next()));
  msgBox.setDetailedText(msg);\
  qWarning("%s", msg.toUtf8().constData());
  msgBox.exec();
}

void CYWarningObjectData(QString file, int line, QString object, QString data)
{
  if (core && core->disableCYWARNING)
    return;
  QString msg = QObject::tr("File %1 at line %2 on object %3 for data %4 !").arg(file).arg(line).arg(object).arg(data);
  QMessageBox::warning(nullptr, QObject::tr("CYLIX OBJECT DATA ERROR"), msg);
  qWarning("%s", msg.toUtf8().constData());
}

void CYFatal(QString file, int line)
{
  if (core && core->disableCYFATAL)
    return;
  QString msg = QObject::tr("Error in source file %1 at line %2 !").arg(file).arg(line);
  QMessageBox::critical(nullptr, QObject::tr("CYLIX ERROR"), msg);
  qFatal("%s", msg.toUtf8().constData());
}

void CYFatalText(QString file, int line, QString text)
{
  if (core && core->disableCYFATAL)
    return;
  QString msg = QObject::tr("Error in source file %1 at line %2 !").arg(file).arg(line)+"\n"+text;
  QMessageBox::critical(nullptr, QObject::tr("CYLIX ERROR"), msg);
  qFatal("%s", msg.toUtf8().constData());
}

void CYFatalObject(QString file, int line, QString object)
{
  if (core && core->disableCYFATAL)
    return;
  QString msg = QObject::tr("Error in source file %1 at line %2 on object %3 !").arg(file).arg(line).arg(object);
  QMessageBox::critical(nullptr, QObject::tr("CYLIX OBJECT ERROR"), msg);
  qFatal("%s", msg.toUtf8().constData());
}

void CYFatalData(QString file, int line, QString data)
{
  if (core && core->disableCYFATAL)
    return;
  QString msg = QObject::tr("Error in source file %1 at line %2 for data %3 !").arg(file).arg(line).arg(data);
  QMessageBox::critical(nullptr, QObject::tr("CYLIX DATA ERROR"), msg);
  qFatal("%s", msg.toUtf8().constData());
}

void CYFatalObjectData(QString file, int line, QString object, QString data)
{
  if (core && core->disableCYFATAL)
    return;
  QString msg = QObject::tr("Error in source file %1 at line %2 on object %3 for data %4 !").arg(file).arg(line).arg(object).arg(data);
  QMessageBox::critical(nullptr, QObject::tr("CYLIX OBJECT DATA ERROR"), msg);
  qFatal("%s", msg.toUtf8().constData());
}

QString findResource(QString file, QString )
{
  if (QString(file).startsWith("/") ||
      QString(file).startsWith(".") ||
      QString(file).startsWith(":") )
    return file;
  else if (core)
  {
    QString path= qApp->applicationDirPath().toUtf8();
    return QString("%1/%2").arg(path).arg(file);
  }
  else // Qt Designer
    return file;
}

QUrl UrlFromUserInput(const QString& input)
{
   QByteArray latin = input.toLatin1();
   QByteArray utf8 = input.toUtf8();
   if (latin != utf8)
   {
      // URL string containing unicode characters (no percent encoding expected)
      return QUrl::fromUserInput(input);
   }
   else
   {
      // URL string containing ASCII characters only (assume possible %-encoding)
      return QUrl::fromUserInput(QUrl::fromPercentEncoding(input.toLatin1()));
   }
}

bool copyPath(QString sourceDir, QString destinationDir, bool overWriteDirectory)
{
  QDir originDirectory(sourceDir);

  if (! originDirectory.exists())
  {
    return false;
  }

  QDir destinationDirectory(destinationDir);

  if(destinationDirectory.exists() && !overWriteDirectory)
  {
    return false;
  }
  else if(destinationDirectory.exists() && overWriteDirectory)
  {
    destinationDirectory.removeRecursively();
  }

  originDirectory.mkpath(destinationDir);

  foreach (QString directoryName, originDirectory.entryList(QDir::Dirs | \
                                                            QDir::NoDotAndDotDot))
  {
    QString destinationPath = destinationDir + "/" + directoryName;
    originDirectory.mkpath(destinationPath);
    copyPath(sourceDir + "/" + directoryName, destinationPath, overWriteDirectory);
  }

  foreach (QString fileName, originDirectory.entryList(QDir::Files))
  {
    QFile::copy(sourceDir + "/" + fileName, destinationDir + "/" + fileName);
  }

  /*! Possible race-condition mitigation? */
  QDir finalDestination(destinationDir);
  finalDestination.refresh();

  if(finalDestination.exists())
  {
    return true;
  }

  return false;
}

bool copyDirectoryFiles(const QString &fromDir, const QString &toDir, bool coverFileIfExist)
{

  QDir sourceDir(fromDir);
  QDir targetDir(toDir);
  if(!targetDir.exists()){    /* if directory don't exits, build it */
    if(!targetDir.mkdir(targetDir.absolutePath()))
      return false;
  }

  QFileInfoList fileInfoList = sourceDir.entryInfoList();
  foreach(QFileInfo fileInfo, fileInfoList){
    if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
      continue;

    if(fileInfo.isDir()){    /* if it is directory , copy recursively*/
      if(!copyDirectoryFiles(fileInfo.filePath(),
                             targetDir.filePath(fileInfo.fileName()),
                             coverFileIfExist))
        return false;
    }
    else{            /* if coverFileIfExist == true, remove old file first */
      if(coverFileIfExist && targetDir.exists(fileInfo.fileName())){
        targetDir.remove(fileInfo.fileName());
      }

      /// files copy
      if(!QFile::copy(fileInfo.filePath(),
                      targetDir.filePath(fileInfo.fileName()))){
        return false;
      }
    }
  }
  return true;
}

bool removeDir(QString dirPath)
{
  QDir folder(dirPath);
  //On va lister dans ce répertoire tous les éléments différents de "." et ".."
  //(désignant respectivement le répertoire en cours et le répertoire parent)
  folder.setFilter(QDir::NoDotAndDotDot | QDir::AllEntries);
  foreach(QFileInfo fileInfo, folder.entryInfoList())
  {
    //Si l'élément est un répertoire, on applique la méthode courante à ce répertoire, c'est un appel récursif
    if(fileInfo.isDir())
    {
      if(!removeDir(fileInfo.filePath())) //Si une erreur survient, on retourne false
        return false;
    }
    //Si l'élément est un fichier, on le supprime
    else if(fileInfo.isFile())
    {
      if(!QFile::remove(fileInfo.filePath()))
      {
        //Si une erreur survient, on retourne false
        return false;
      }
    }
  }
  return folder.removeRecursively();
}

int compareVersion(QString version1, QString version2)
{
  version1 = version1.section('_', 0, 0);
  QString VER1_MAJ = version1.section('.', 0, 0); // évolutions majeures
  QString VER1_MIN = version1.section('.', 1, 1); // évolutions mineures
  QString VER1_PAT = version1.section('.', 2, 2); // Patch ou correctif de développement

  version2 = version2.section('_', 0, 0);
  QString VER2_MAJ = version2.section('.', 0, 0); // évolutions majeures
  QString VER2_MIN = version2.section('.', 1, 1); // évolutions mineures
  QString VER2_PAT = version2.section('.', 2, 2); // Patch ou correctif de développement

  if (VER1_MAJ.toInt()>VER2_MAJ.toInt())
    return 1;
  if (VER1_MAJ.toInt()<VER2_MAJ.toInt())
    return -1;

  // si évolutions majeures égales
  if (VER1_MIN.toInt()>VER2_MIN.toInt())
    return 1;
  if (VER1_MIN.toInt()<VER2_MIN.toInt())
    return -1;

  // si évolutions majeures et mineures égales
  if (VER1_PAT.toInt()>VER2_PAT.toInt())
    return 1;
  if (VER1_PAT.toInt()<VER2_PAT.toInt())
    return -1;

  // versions identiques
  return 0;
}

int open_tube(dcltb_t *ptb)
{
#if defined(Q_OS_WIN)
  wchar_t* wString=new wchar_t[4096];
  MultiByteToWideChar(CP_ACP, 0, ptb->nm, -1, wString, 4096);

  if (WaitNamedPipe(wString, NMPWAIT_USE_DEFAULT_WAIT) == 0)
  {
    if(debug)
      CYMESSAGETEXT(QString("WaitNamedPipe failed with error : %1").arg(GetLastError()));
    return false;
  }
  if(debug)
    CYMESSAGETEXT(QString("pwrite: the pipe is ready"));


  ptb->hIn = CreateFile(wString,
                        GENERIC_READ | GENERIC_WRITE , //| FILE_SHARE_READ | FILE_SHARE_WRITE
                        0,
                        NULL, OPEN_EXISTING,
                        FILE_ATTRIBUTE_NORMAL, //| FILE_FLAG_OVERLAPPED
                        NULL);


  if (ptb->hIn == INVALID_HANDLE_VALUE)
  {
    if(debug)
      CYMESSAGETEXT(QString("CreateFile failed with error : %1").arg(GetLastError()));
    return false;
  }

  DWORD mode = PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_NOWAIT;

  if ( SetNamedPipeHandleState(ptb->hIn,  &mode,  NULL,	NULL) == 0)
  {
    if(debug)
      CYMESSAGETEXT(QString("SetNamedPipeHandleState failed. error= %1").arg(GetLastError()));
    return FALSE;
  }

#elif defined(Q_OS_LINUX)
  ptb->num = open(ptb->nm, ptb->oflag);
  if (ptb->num == -1)
  {
    if(debug)
      CYMESSAGETEXT(QString("Erreur ouverture : %1").arg(strerror(errno)));
    return false;
  }
#endif

  return true;
}

int close_tube(dcltb_t *ptube)
{

#if defined(Q_OS_WIN)
  DisconnectNamedPipe(ptube->hIn);
  CloseHandle(ptube->hIn);
#elif defined(Q_OS_LINUX)
  if(ptube->num != -1)
  {
    if(close(ptube->num)!=-1)
      return true; //unlink(ptube->nm);
  }
#endif

  return false;
}
int read_tube(dcltb_t *ptube, void *data, size_t size)
{
#if defined(Q_OS_WIN)
  DWORD		dwBytesRead;
  if (!ReadFile(ptube->hIn, data, size, &dwBytesRead, NULL))
  {
    ptube->err = GetLastError();
    CYMESSAGETEXT(QString("Erreur read : %l").arg(ptube->err));
    return -1;
  }

  return dwBytesRead;
#elif defined(Q_OS_LINUX)
  return read(ptube->num, data, size);
#endif

  return -1;
}


int write_tube(dcltb_t *ptube, void *data, size_t size)
{
#if defined(Q_OS_WIN)
  DWORD   dwWritten;

  if (!WriteFile(ptube->hIn, data, size, &dwWritten, NULL)) //ERROR_NO_DATA 232 ERROR_NO_DATA ERROR_BROKEN_PIPE
  {
    ptube->err = GetLastError();
    CYMESSAGETEXT(QString("Erreur write : %1, nb_write : %2").arg(ptube->err).arg(dwWritten));
    return -1;
  }
  if(dwWritten != size)
    CYMESSAGETEXT(QString("Erreur write, nb_write : %2").arg(dwWritten));
  return dwWritten;
#elif defined(Q_OS_LINUX)
  return write(ptube->num, data, size);
#endif

  return -1;
}

