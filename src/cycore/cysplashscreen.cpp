/***************************************************************************
                          cysplashscreen.cpp  -  description
                             -------------------
    begin                : lun aoû 2 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cysplashscreen.h"

// QT
#include <QLayout>
#include <QPixmap>
#include <QApplication>
#include <QLabel>
#include <QScreen>
// CYLIBS
#include "cyapplication.h"
#include "cy.h"

CYSplashScreen::CYSplashScreen(bool start)
: QObject()
{
  mSplash = new QWidget(0, Qt::FramelessWindowHint);
  mSplash->setWindowModality(Qt::WindowModal);

  QVBoxLayout *vbox = new QVBoxLayout(mSplash);
  vbox->setContentsMargins(0,0,0,0);

  QLabel *pixmap = new QLabel(mSplash);
  QPixmap pm;
  pm.load(":pics/cylix-splash.png");
  pixmap->setPixmap(pm);
  vbox->addWidget(pixmap);

  mMessage = new QLabel(mSplash);
//  QPalette palette;
//  palette.setColor(mMessage->foregroundRole(), Qt::white);
//  palette.setColor(mMessage->backgroundRole(), Qt::black);
//  mMessage->setPalette(palette);
  vbox->addWidget(mMessage);

  QScreen *screen = QGuiApplication::screenAt(QPoint(0,0));
  QRect  rect = screen->geometry();
  mSplash->move(rect.x() + (rect.width() - mSplash->sizeHint().width()) / 2,
                rect.y() + (rect.height() - mSplash->sizeHint().height()) / 2);
  mSplash->setFixedSize(mSplash->sizeHint());

  if (start)
    showMessage(tr("Starting CYLIX..."));
  else
    showMessage(tr("Stopping CYLIX..."));

  mSplash->show();
}

CYSplashScreen::~CYSplashScreen()
{
  delete mSplash;
}

void CYSplashScreen::showMessage(const QString &message)
{
  mMessage->setText(message);
  if (cyapp)
    cyapp->processEvents();
  mSplash->show();
}
