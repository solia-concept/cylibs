/***************************************************************************
                          cyacquisitionslow.h  -  description
                             -------------------
    begin                : mer mai 5 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITIONSLOW_H
#define CYACQUISITIONSLOW_H

// QT
#include <qtimer.h>
// CYLIBS
#include "cyacquisition.h"

/** @short Acquisition lente.
  * La synchronisation est gérée par le superviseur seul.
  * @author LE CLÉACH Gérald
  */

class CYAcquisitionSlow : public CYAcquisition
{
  Q_OBJECT
public:
  /** Constructeur d'acquisition lente sans gestion de mise en pause.
    * @param parent Parent.
    * @param name   Nom.
    * @param label  Etiquette.
    * @param enable Flag d'activation.
    * @param link   Connexion réseau. */
  CYAcquisitionSlow(QObject *parent, const QString &name, QString label, CYFlag *enable, CYNetLink *link=0);
  /** Constructeur d'acquisition lente avec gestion de mise en pause.
    * @param parent Parent.
    * @param name   Nom.
    * @param label  Etiquette.
    * @param enable Flag d'activation.
    * @param pause  Flag de mise en pause.
    * @param link   Connexion réseau. */
  CYAcquisitionSlow(QObject *parent, const QString &name, QString label, CYFlag *enable, CYFlag *pause, CYNetLink *link=0);
  ~CYAcquisitionSlow();

public slots: // Public slots
  /** Ajoute dans le buffer les valeurs nouvellement lues. */
  void buffering();
  /** Charge la configuration d'acquisition. */
  virtual bool loadSetup();
  /** Sauvegarde la configuration d'acquisition . */
  virtual bool saveSetup();
  /** Démarre l'acquisition. */
  virtual bool startAcquisition();
  /** Arrête l'acquisition. */
  virtual void stopAcquisition();

protected: // Protected methods
  /** Traîtement de l'acquisition en fonctionnement. */
  virtual void runAcquisition();

private: // Private methods
  /** Initialisation de l'acquisition. */
  void init();
  /** Ajoute une ligne d'acquisition.
    * @param time Temps de l'échantillon en ms. */
  void addLine(double time);

protected: // Protected attributes
  /** Timer d'acquisition. */
  QTimer *mAcqTimer;
protected slots:
    void enableStateChanged(bool rising);
};

#endif
