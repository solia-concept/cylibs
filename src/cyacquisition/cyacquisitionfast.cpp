/***************************************************************************
                          cyacquisitionfast.cpp  -  description
                             -------------------
    begin                : jeu avr 29 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisitionfast.h"

// QT
#include <QLocale>
// CYLIBS
#include "cycore.h"
#include "cyf32.h"
#include "cys32.h"
#include "cyflag.h"
#include "cytime.h"
#include "cystring.h"
#include "cyacquisitiondata.h"
#include "cyacquisitionbackupflag.h"

CYAcquisitionFast::CYAcquisitionFast(QObject *parent, const QString &name, QString label, CYFlag *enable, CYNetLink *link, int sampleSize, float sampleTimer)
: CYAcquisition(parent, name, label, enable, link),
  mSampleSize(sampleSize),
  mSampleTimer(sampleTimer)
{
  init();
}

CYAcquisitionFast::CYAcquisitionFast(QObject *parent, const QString &name, QString label, CYFlag *enable, CYFlag *pause, CYNetLink *link, int sampleSize, float sampleTimer)
: CYAcquisition(parent, name, label, enable, pause, link),
  mSampleSize(sampleSize),
  mSampleTimer(sampleTimer)
{
  init();
}

void CYAcquisitionFast::init()
{
  burstFlag   = 0;
  burstEnable = 0;

  CYAcquisition::init();

  acqTimer = new CYTime(mDB, "AQC_TIMER", new u32, "100ms", "10ms", "10000ms", 1.0);
  acqTimer->setLabel(tr("Acquisition period"));

  // POST-MORTEM
  acqLengthTime = new CYTime(mDB, "AQC_TIME", new u32, "2h0m00s", "0h0m0s", "1000h0m0s", 1.0);
  acqLengthTime->setLabel(tr("Acquisition length time of a file"));
  acqLengthTime->setHelp(tr("This time combined with the acquisition period automatically generates a number of lines per acquisition file. To reduce the time required to import multiple acquisition files into SoliaGraph, it is advisable to limit the number of small files by maximizing this time."));

  // HISTORIQUE
  acqWindowTime = new CYTime(mDB, "AQC_TIME_WINDOW", new u32, "10 sec", "1 sec", "6000 sec", 1.0);
  acqWindowTime->setLabel(tr("Acquisition window time"));

  acqIntervalTime = new CYTime(mDB, "NO_AQC_TIME_WINDOW", new u32, "10h0m0s", "0h0m0s", "100h0m0s", 1.0);
  acqIntervalTime->setLabel(tr("No acquisition window time"));

  acqTimer->setMin((int)mSampleTimer);
  acqTimer->setPrecision((int)mSampleTimer);

  mBurstsRatio = 1;
}

CYAcquisitionFast::~CYAcquisitionFast()
{
}

void CYAcquisitionFast::buffering(int str)
{
  if (!enableFlag)
    return;

  mStr = str;

  if (pauseFlag)
  {
    if (pauseFlag->val() && !mPausing->val())
    {
      // acquisition en pause
      mPauseTime = 0.0;
      mPausing->setVal(true);
    }
    else if (!pauseFlag->val() && mPausing->val())
    {
      // fin de pause
      mPausing->setVal(false);
    }
  }
  else
  {
    // pas de gestion de pause
    mPausing->setVal(false);
  }

  if (enableFlag->val() || (isOn() && core->simulation() && core->started()))
  {
    runAcquisition();
  }
  else if (!enableFlag->val() && mStarted->val())
  {
    stopAcquisition();
  }
}

bool CYAcquisitionFast::startAcquisition()
{
  if (pauseFlag && pauseFlag->val())
      return false;

  mWindow->setVal(false);

  mTotalTime    = (formatSoliaGraph->val()) ? QDateTime::currentMSecsSinceEpoch() : 0.0;
  mPauseTime    = 0.0;
  mWindowTime   = 0.0;
  mCptSamples   = 0;
  mIntervalTime = acqIntervalTime->val();
  mStartTime    = QDateTime::currentDateTime();

  mCurrentBuffer= 2;
  mBuffer1.clear();
  mBuffer2.clear();

  mNbLinesFile = 0;
  mUseNewFile  = true;

//  loadSetup();
  setNameDir(dirName->val());
  if (mPostMortem)
    removeFiles();
  startSaving();
  saveSetup();

  mMaxNbCharTime = QString("%1").arg(pretendTime->val()*1000).length();

  if (postMortem())
    mPostMortemBackupTimer->start((int)postMortemBackupTimer->ms());

  mStarted->setVal(true);

  return true;
}

void CYAcquisitionFast::stopAcquisition()
{
  postMortemBackupContinu();

  mPausing->setVal(false);
  mStarted->setVal(false);
  mRunning  = false;

  mCptSamples = 0;

  if (postMortem())
    mPostMortemBackupTimer->stop();

  // vide les buffers dans le (ou les) fichier(s) d'acquisition
  while (!mBuffer1.isEmpty() || !mBuffer2.isEmpty())
    save();

  stopSaving();
  saveSetup();
}

void CYAcquisitionFast::runAcquisition()
{
  // attendre la fin du travail precedent
  while (mRunning) ;

  mRunning = true;

  if (!mStarted->val())
  {
   if (!startAcquisition())
    {
      mRunning = false;  // acquisition en pause
      return;
    }
  }
  for(int i=0; i<mSampleSize; i++)
  {
    double time = mCptSamples*mSampleTimer;
    bool burst = false;
    if ( burstEnable && burstEnable->val() )
    {
      if ( burstFlag && burstFlag->sample(mStr, i) )
      {
        time = mSampleTimer/mBurstsRatio;
        burst = true;
      }
      else
        mTotalTime  = (formatSoliaGraph->val()) ? QDateTime::currentMSecsSinceEpoch() : 0.0;
    }

    if (((time>=(double)acqTimer->val()) && !burst) || burst)
    {
      mCptSamples = 0;

      if (mPausing->val()) // en pause
      {
        mPauseTime += time;
      }
      else  // acquisition
      {
        if (mPostMortem)
        {
          addLine(mTotalTime, i);
        }
        else if ( burstEnable && burstEnable->val() )
        {
          if (burst)
            addLine(mTotalTime, i);
        }
        else
        {
          if (mIntervalTime<acqIntervalTime->val()) // attente de la fenêtre d'acquisition.
          {
            mIntervalTime += time;
          }
          else if (!mWindow->val()) // début de la fenêtre d'acquisition.
          {
            mWindow->setVal(true);
            mWindowTime = time;
            addLine(mTotalTime, i);
          }
          else if (mWindowTime<=acqWindowTime->CYU32::val())  // dans la fenêtre d'acquisition
          {
            if (mWindowTime==acqWindowTime->CYU32::val()) // fin de la fenêtre d'acquisition.
            {
              mWindow->setVal(false);
              mIntervalTime = time;
            }
            mWindowTime += time;
            addLine(mTotalTime, i);
          }
        }
      }
      mTotalTime += time;
    }
    mCptSamples++;
  }
  mRunning = false;
}

void CYAcquisitionFast::addLine(double time, int sample)
{
  QString *line;
  QString sep;
  if (formatSoliaGraph->val())
  {
    sep = ";";
    line = new QString(QString("%1%2").arg(time, mMaxNbCharTime, 'f', 0).arg("000")); // timestamp ms converti en µs
  }
  else
  {
    sep = separator->val();
    if (burstEnable)
      line = new QString(QString("%1").arg(time, mMaxNbCharTime, 'f', 2));
    else
      line = new QString(QString("%1").arg(time, mMaxNbCharTime, 'f', 0));
  }

  QListIterator<CYAcquisitionData *> it(datas);
  it.toFront();
  while (it.hasNext())
  {
    CYAcquisitionData *data =it.next();
		line->append(QString("%1%2").arg(sep).arg(data->fieldCSV(fileSizeOptimization->val(), true, mStr, sample)));
  }
  line->append("\n");
  CYAcquisition::addLine(line);
}


void CYAcquisitionFast::initBurstDatas(QByteArray flagName, QByteArray enableName, short ratio)
{
  initBurstDatas((CYFlag *)core->findData(flagName), (CYFlag *)core->findData(enableName), ratio);
}

void CYAcquisitionFast::initBurstDatas(CYFlag *flag, CYFlag *enable, short ratio)
{
  if (mBurstsRatio<=0)
    CYFATALTEXT(objectName())

  burstFlag = flag;
  burstEnable = enable;
  mBurstsRatio = ratio;

  acqBurstsTimer = new CYF32(mDB, "AQC_BURSTS_TIMER", new f32, mSampleTimer/mBurstsRatio);
  acqBurstsTimer->setUnit(tr("ms"));
  acqBurstsTimer->setVolatile(true);
  acqBurstsTimer->setVal(mSampleTimer);
  acqBurstsTimer->setLabel(tr("Bursts acquisition period"));
}
