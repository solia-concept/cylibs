/***************************************************************************
                          cyacquisitionbackupflag.cpp  -  description
                             -------------------
    begin                : ven jun 18 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisitionbackupflag.h"

// QT
#include <QPixmap>
#include <QString>
#include <QStyle>
// CYLIBS
#include "cystring.h"
#include "cytime.h"
#include "cys16.h"
#include "cyacquisition.h"

CYAcquisitionBackupFlag::CYAcquisitionBackupFlag(CYDB *db, const QString &name, bool custom, flg *val, const flg def)
: CYFlag(db, name, val, def),
  mCustom(custom)
{
  mAcquisition = 0;

  connect(this, SIGNAL(valueRising(bool)), SLOT(stateChanged(bool)));
}

CYAcquisitionBackupFlag::~CYAcquisitionBackupFlag()
{
}

void CYAcquisitionBackupFlag::setPrefixFileName(QString prefix)
{
  mPrefixFileName->setVal(prefix);
}

QString CYAcquisitionBackupFlag::prefixfileName()
{
  return mPrefixFileName->val();
}

QString CYAcquisitionBackupFlag::header()
{
  return mHeader->val();
}

void CYAcquisitionBackupFlag::setHeader(QString val)
{
  mHeader->setVal(val);
}

void CYAcquisitionBackupFlag::setEnable(int val)
{
  mEnable->setVal(val);
}

bool CYAcquisitionBackupFlag::isEnable()
{
  return (mEnable->val()==No) ? false : true;
}

void CYAcquisitionBackupFlag::emitBackupToDo()
{
  emit backupToDo(this);
}

void CYAcquisitionBackupFlag::stateChanged(bool rising)
{
  if ( ( rising && (mEnable->val()==Rising ))
     ||(!rising && (mEnable->val()==Falling))
     ||(mEnable->val()==Changing)
     )
  {
    if (mDelay->val()==0)
      emitBackupToDo();
    else
    {
      QTimer::singleShot(mDelay->val(), this, SLOT(emitBackupToDo()));
    }
  }
}

QString CYAcquisitionBackupFlag::headerWithDateTime()
{
  QDate date = QDate::currentDate();
  QTime time = QTime::currentTime();
  return QString(header()+":\t"+date.toString(Qt::ISODate)+" "+time.toString(Qt::ISODate));
}


void CYAcquisitionBackupFlag::init(CYAcquisition *acq, QString prefix, QString header, int enable, bool overwrite)
{
  if ( mAcquisition )
    CYFATALTEXT( QString("%1 est déjà utilisé pour l'acquisition %2").arg( objectName() ).arg( mAcquisition->objectName() ) )

  mAcquisition = acq;

  mPrefixFileName = new CYString(mAcquisition->db(), QString("%1_PREFIX_FILENAME").arg(objectName()), new QString(prefix));
  mPrefixFileName->setLabel(tr("Prefix used in the backup file name"));

  mHeader = new CYString(mAcquisition->db(), QString("%1_HEADER").arg(objectName()), new QString(header));
  mHeader->setLabel(tr("Header used in the backup file"));


  mEnable = new CYS16(mAcquisition->db(), QString("%1_ENABLE").arg(objectName()), new s16);
  mEnable->setLabel(tr("Enable backup"));
  QIcon icon_true = qApp->style()->standardIcon(QStyle::SP_DialogOkButton);
  QIcon icon_false = qApp->style()->standardIcon(QStyle::SP_DialogCancelButton);
  if (!mCustom)
  {
    mEnable->setChoice(None  , icon_false.pixmap(22,22), tr("No"));
    mEnable->setChoice(Rising, icon_true.pixmap(22,22) , tr("Yes"));
  }
  else
  {
    mEnable->setChoice(None    , icon_false.pixmap(22,22)             , tr("No")      , tr("No backup"));
    mEnable->setChoice(Rising  , QPixmap(":icons/cyedge_rising.png")  , tr("Rising")  , tr("Backup on the rising edge of event."));
    mEnable->setChoice(Falling , QPixmap(":icons/cyedge_falling.png") , tr("Falling") , tr("Backup on the falling edge of event."));
    mEnable->setChoice(Changing, QPixmap(":icons/cyedge_changing.png"), tr("Changing"), tr("Backup on the rising and falling edges of event."));
  }
  mEnable->setDef(enable);

  mOverwrite = new CYFlag(mAcquisition->db(), QString("%1_OVERWRITE").arg(objectName()), new flg, overwrite);
  mOverwrite->setLabel(tr("Recording"));
  mOverwrite->setChoiceTrue(tr("Overwrite"));
  mOverwrite->setChoiceFalse(tr("Increment"));
  mOverwrite->setHelp(0,
                      tr("If the prefix doesn't change the new archive overwrites the last one."),
                      tr("The number of backup is appended to the name of backup file. Thus, old backup files are note removed."));

  mDelay = new CYTime(mAcquisition->db(), QString("%1_DELAY").arg(objectName()), new u32, "0h0m0s", "0h0m0s", "10h0m0s");
  mDelay->setLabel(tr("Backup delay on event"));
}
