/***************************************************************************
                          cyacquisitionslow.cpp  -  description
                             -------------------
    begin                : mer mai 5 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisitionslow.h"

// QT
#include <QLocale>
// CYLIBS
#include "cycore.h"
#include "cys32.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cyflag.h"
#include "cytime.h"
#include "cystring.h"
#include "cyacquisitiondata.h"
#include "cyacquisitionbackupflag.h"

CYAcquisitionSlow::CYAcquisitionSlow(QObject *parent, const QString &name, QString label, CYFlag *enable, CYNetLink *link)
: CYAcquisition(parent, name, label, enable, link)
{
  init();
}

CYAcquisitionSlow::CYAcquisitionSlow(QObject *parent, const QString &name, QString label, CYFlag *enable, CYFlag *pause, CYNetLink *link)
: CYAcquisition(parent, name, label, enable, pause, link)
{
  init();
}

void CYAcquisitionSlow::init()
{
  CYAcquisition::init();

  acqTimer = new CYTime(mDB, "AQC_TIMER", new u32, "10 sec", "1 sec", "100 sec", 1.0);
  acqTimer->setLabel(tr("Acquisition period"));

  // POST-MORTEM
  acqLengthTime = new CYTime(mDB, "AQC_TIME", new u32, "10 min", "1 min", "6000 min", 1.0);
  acqLengthTime->setLabel(tr("Acquisition length time"));

  // HISTORIQUE
  acqWindowTime = new CYTime(mDB, "AQC_TIME_WINDOW", new u32, "10 min", "1 min", "6000 min");
  acqWindowTime->setLabel(tr("Acquisition window time"));

  acqIntervalTime = new CYTime(mDB, "NO_AQC_TIME_WINDOW", new u32, "10h0m0s", "0h0m0s", "100h0m0s");
  acqIntervalTime->setLabel(tr("No acquisition window time"));

  mAcqTimer = new QTimer(this);
  connect(mAcqTimer, SIGNAL(timeout()), SLOT(buffering()));

  if (enableFlag)
    connect(enableFlag, SIGNAL(valueRising(bool)), SLOT(enableStateChanged(bool)));
}

CYAcquisitionSlow::~CYAcquisitionSlow()
{
}

void CYAcquisitionSlow::buffering()
{
  if (!enableFlag)
    return;

  if (pauseFlag)
  {
    if (pauseFlag->val() && !mPausing->val())
    {
      // acquisition en pause
      mPauseTime = 0.0;
      mPausing->setVal(true);
    }
    else if (!pauseFlag->val() && mPausing->val())
    {
      // fin de pause
      mPausing->setVal(false);
    }
  }
  else
  {
    // pas de gestion de pause
    mPausing->setVal(false);
  }

  if (enableFlag->val() || (isOn() && core->simulation()))
  {
    runAcquisition();
  }
  else if (!enableFlag->val() && mStarted->val())
  {
    stopAcquisition();
  }
}

bool CYAcquisitionSlow::startAcquisition()
{
  if (pauseFlag && pauseFlag->val())
      return false;

  mWindow->setVal(false);

  mTotalTime    = (formatSoliaGraph->val()) ? QDateTime::currentMSecsSinceEpoch() : 0.0;
  mPauseTime    = 0.0;
  mWindowTime   = 0.0;
  mIntervalTime = acqIntervalTime->val();
  mStartTime    = QDateTime::currentDateTime();

  mCurrentBuffer= 2;
  mBuffer1.clear();
  mBuffer2.clear();

  mNbLinesFile = 0;
  mUseNewFile  = true;

//  loadSetup();
  setNameDir(dirName->val());
  if (mPostMortem)
    removeFiles();
  startSaving();
//  saveSetup();

  mMaxNbCharTime = QString("%1").arg(pretendTime->val()*1000).length();

  if (postMortem())
    mPostMortemBackupTimer->start((int)postMortemBackupTimer->ms());

  mStarted->setVal(true);

  return true;
}

void CYAcquisitionSlow::stopAcquisition()
{
  postMortemBackupContinu();

  mPausing->setVal(false);
  mStarted->setVal(false);
  mRunning  = false;

  if (postMortem())
    mPostMortemBackupTimer->stop();

  // vide les buffers dans le (ou les) fichier(s) d'acquisition
  while (!mBuffer1.isEmpty() || !mBuffer2.isEmpty())
    save();

  stopSaving();
  saveSetup();
}

void CYAcquisitionSlow::runAcquisition()
{
  // attendre la fin du travail precedent
  while (mRunning) ;

  mRunning = true;

  if (!mStarted->val())
  {
    if (!startAcquisition())
    {
      mRunning = false;   // acquisition en pause
      return;
    }
  }
  if (mPausing->val()) // en pause
  {
    mPauseTime += acqTimer->val();
  }
  else  // acquisition
  {
    if (mPostMortem)
    {
      addLine(mTotalTime);
    }
    else
    {
      if (mIntervalTime<acqIntervalTime->val()) // attente de la fenêtre d'acquisition.
      {
        mIntervalTime += acqTimer->val();
      }
      else if (!mWindow->val()) // début de la fenêtre d'acquisition.
      {
        mWindow->setVal(true);
        mWindowTime = acqTimer->val();
        addLine(mTotalTime);
      }
      else // dans la fenêtre d'acquisition
      {
        if (mWindowTime==acqWindowTime->CYU32::val()) // fin de la fenêtre d'acquisition.
        {
          mWindow->setVal(false);
          mIntervalTime = acqTimer->val();
        }
        mWindowTime += acqTimer->val();
        addLine(mTotalTime);
      }
    }
  }

  mTotalTime += acqTimer->val();

  mRunning = false;
}

void CYAcquisitionSlow::addLine(double time)
{
  QString *line;
  QString sep;
  if (formatSoliaGraph->val())
  {
    sep = ";";
    line = new QString(QString("%1%2").arg(time, mMaxNbCharTime, 'f', 0).arg("000")); // timestamp ms converti en µs
  }
  else
  {
    sep = separator->val();
    line = new QString(QString("%1").arg(time, mMaxNbCharTime, 'f', 0));
  }

  QListIterator<CYAcquisitionData *> it(datas);
  it.toFront();
  while (it.hasNext())
  {
    CYAcquisitionData *data =it.next();
		line->append(QString("%1%2").arg(sep).arg(data->fieldCSV(fileSizeOptimization->val(), false)));
  }
  line->append("\n");
  CYAcquisition::addLine(line);
}

bool CYAcquisitionSlow::loadSetup()
{
  bool ret = CYAcquisition::loadSetup();
  mAcqTimer->stop();
  mAcqTimer->start(acqTimer->val());
  return ret;
}

bool CYAcquisitionSlow::saveSetup()
{
  bool ret = CYAcquisition::saveSetup();
  mAcqTimer->stop();
  mAcqTimer->start(acqTimer->val());
  return ret;
}


/*! Détection de l'activation / désactivation de l'acquisition
    \fn CYAcquisitionSlow::enableStateChanged(bool rising)
 */
void CYAcquisitionSlow::enableStateChanged(bool rising)
{
  if (rising)
    runAcquisition();
}
