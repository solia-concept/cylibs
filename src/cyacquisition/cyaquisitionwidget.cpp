/***************************************************************************
                          cyaquisitionwidget.cpp  -  description
                             -------------------
    begin                : jeu mai 6 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyaquisitionwidget.h"

// CYLIBS
#include "cyacquisitionview.h"
#include "cyacquisitionsetup.h"

CYAquisitionWidget::CYAquisitionWidget(QWidget *parent, const QString &name)
: QTabWidget(parent)
{
  setObjectName(name);
  mView = new CYAcquisitionView(this, "CYAcquisitionView");
  addTab(mView, tr("View"));

  mSetup = new CYAcquisitionSetup(this, "CYAcquisitionSetup");
  addTab(mSetup, tr("Setup"));
}

CYAquisitionWidget::~CYAquisitionWidget()
{
}
