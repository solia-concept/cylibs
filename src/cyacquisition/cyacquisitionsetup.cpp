/***************************************************************************
                          cyacquisitionsetup.cpp  -  description
                             -------------------
    begin                : ven avr 30 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisitionsetup.h"
#include "ui_cyacquisitionsetup.h"

#include <sys/types.h>
// QT
#include <QPushButton>
#include <QRadioButton>
#include <QTabWidget>
#include <QFileDialog>
#include <QString>
#include <QShowEvent>
#include <QTreeWidgetItem>
#include <QTreeWidgetItemIterator>
// CYLIBS
#include "cycore.h"
#include "cys16.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cytime.h"
#include "cystring.h"
#include "cynuminput.h"
#include "cynetlink.h"
#include "cyframe.h"
#include "cydatasbrowser.h"
#include "cyacquisition.h"
#include "cyacquisitiondata.h"
#include "cyacquisitiondataview.h"
#include "cyacquisitionbackupflag.h"
#include "cytextlabel.h"
#include "cybuttongroup.h"
#include "cyacquisitionfast.h"
#include "cymessagebox.h"
#include "cydataslistview.h"
#include "cydataseditlist.h"
#include "cyacquisitiondataview.h"

CYAcquisitionSetup::CYAcquisitionSetup(QWidget *parent, const QString &name)
  : CYWidget(parent,name), ui(new Ui::CYAcquisitionSetup)
{
  ui->setupUi(this);
  mAcquisition = 0;
  connect(ui->acquisitionDataView, SIGNAL(modifie()), this, SIGNAL(modifie()));
  connect(ui->acquisitionDataViewHeader, SIGNAL(modifie()), this, SIGNAL(modifie()));
  connect(ui->formatSoliaGraph, SIGNAL(state(bool)), ui->separationGroup, SLOT(setDisabled(bool)));
  connect(this, SIGNAL(modifie()), this, SLOT(informations()));

  mTimer->start(200);
}

CYAcquisitionSetup::~CYAcquisitionSetup()
{
  delete ui;
}

void CYAcquisitionSetup::apply()
{
  if (!isVisible() || !mAcquisition)
    return;

  if (!mAcquisition->postMortem() && mAcquisition->started())
  {
    QString msg = tr("Applying this change will restart this acquisition with a new time reference and "
                     "a new file will be created !");
    int res = CYMessageBox::warningYesNo(mThis, msg, tr("Acquisition setup applying"), tr("&Apply"), tr("&Discard"));
    if (res == CYMessageBox::No)
      return;
  }

  CYWidget::update();

  mAcquisition->datas.clear();
  mAcquisition->headerDatas.clear();
  mAcquisition->useNewFile();

  CYDatasListView *list;
  list=ui->acquisitionDataViewHeader->list();
  for (QTreeWidgetItemIterator it(list); *it; ++it)
  {
    QString dataName=(*it)->text(list->dataNameColumn());
    QString label=(*it)->text(list->labelColumn());
    if (!dataName.isEmpty())
      mAcquisition->addHeaderData(core->findData(dataName), label);
  }

  list=ui->acquisitionDataView->list();
  for (QTreeWidgetItemIterator it(list); (*it); ++it)
  {
    QString dataName=(*it)->text(list->dataNameColumn());
    QString label=(*it)->text(list->labelColumn());
    if (!dataName.isEmpty())
      mAcquisition->addData(core->findData(dataName), label);
  }

  if (ui->radioSepButton1->isChecked())
    mAcquisition->setSeparatorType(1);
  else if (ui->radioSepButton2->isChecked())
    mAcquisition->setSeparatorType(2);
  else if (ui->radioSepButton3->isChecked())
    mAcquisition->setSeparatorType(3);
  else if (ui->radioSepButton4->isChecked())
    mAcquisition->setSeparatorType(4);
  else if (ui->radioSepButton5->isChecked())
    mAcquisition->setSeparatorType(5);
  else
    mAcquisition->setSeparatorType(1);

  mAcquisition->stopAcquisition();

  //  emit validate();
  initAcquisition();
}

void CYAcquisitionSetup::setAcquisition(CYAcquisition *acquisition)
{
  mAcquisition = acquisition;
  if (!mAcquisition)
    return;

  CYAcquisitionFast * fast = 0;
  if (mAcquisition->inherits("CYAcquisitionFast"))
  {
    fast = (CYAcquisitionFast *)mAcquisition;
  }

  if ( !mAcquisition->postMortem() )
  {
    ui->tabWidget->removeTab(4);
    if (fast && fast->burstEnable)
    {
      ui->acqTimerInput->setHideFlagName(fast->burstEnable->dataName());
      ui->acqTimerLabel->setHideFlagName(fast->burstEnable->dataName());
      ui->acqBurstsTimerDisplay->setHideFlagName(fast->burstEnable->dataName());
      ui->acqBurstsTimerDisplay->setInverseHideFlag(true);
      ui->acqBurstsTimerLabel->setHideFlagName(fast->burstEnable->dataName());
      ui->acqBurstsTimerLabel->setInverseHideFlag(true);
    }
  }
  else
  {
    // inhibition simulation temps si pas d'archivage en continu du post-mortem
    connect(ui->pmContinusBackup, SIGNAL(state(bool)), ui->acqPretendTime, SLOT(setEnabled(bool)));
    ui->acqPretendTime->setFlagName("PM_CONTNUOUS_BACKUP");

    if (mAcquisition->postMortemFlags()>0)
    {
      ui->table->setRowCount(mAcquisition->postMortemFlags());
      ui->table->setColumnWidth(0, 230);
      ui->table->setColumnWidth(1, 150);
      ui->table->setColumnWidth(2, 130);
      ui->table->setColumnWidth(3, 120);
      ui->table->setColumnWidth(4, 120);

      QString dataName;
      CYAcquisitionBackupFlag *flag;
      for (int i=0; i<mAcquisition->postMortemFlags(); i++)
      {
        ui->table->setRowHeight(i, 30);

        flag = mAcquisition->postMortemFlag(i);
        dataName = flag->objectName();
        CYTextLabel *label = new CYTextLabel(ui->table, QString("LABEL_%1").arg(dataName));
        label->setDataName(dataName.toUtf8());
        label->setShowLabel(true);
        ui->table->setCellWidget(i, 0, label);

        dataName = flag->dataEnable()->objectName();
        CYComboBox *enable = new CYComboBox(ui->table, QString("ENABLE_%1").arg(dataName));
        enable->setDataName(dataName.toUtf8());
        ui->table->setCellWidget(i, 1, enable);

        dataName = flag->dataPrefixfileName()->objectName();
        CYTextInput *prefix = new CYTextInput(ui->table, QString("PREFIX_%1").arg(dataName));
        prefix->setDataName(dataName.toUtf8());
        ui->table->setCellWidget(i, 2, prefix);

        dataName = flag->dataOverwrite()->objectName();
        CYFlagInput *overwrite = new CYFlagInput(ui->table, QString("OVERWRITE_%1").arg(dataName));
        overwrite->setDataName(dataName.toUtf8());
        overwrite->setEnablePixmap(false);
        ui->table->setCellWidget(i, 3, overwrite);

        if (QString(flag->objectName())=="POSTMORTEM_BACKUP_TIMER_FLAG")
        {
          CYNumInput *timeInput = new CYNumInput(ui->table, "BACKUP_TIMER");
          timeInput->setDataName("POSTMORTEM_BACKUP_TIMER");
          ui->table->setCellWidget(i, 4, timeInput);
        }
        else
        {
          dataName = flag->dataDelay()->objectName();
          CYNumInput *timeInput = new CYNumInput(ui->table, QString("DELAY_%1").arg(dataName));
          timeInput->setDataName(dataName.toUtf8());
          ui->table->setCellWidget(i, 4, timeInput);
        }
      }
    }
  }
  linkDatas();

  ui->browser->clear();
  ui->browser->init(mAcquisition->link()->host(), mAcquisition->link(), true);
  setLocaleDB(mAcquisition->db());
  if (mAcquisition->postMortem())
  {
    ui->acqWindowTime->hide();
    ui->acqWindowTimeLabel->hide();
    ui->acqIntervalTime->hide();
    ui->acqIntervalTimeLabel->hide();
    ui->acqMaxLines->hide();
    ui->acqMaxLinesLabel->hide();
    ui->acqNbFiles->hide();
    ui->acqNbFilesLabel->hide();
    ui->pmContinusBackup->show();
    ui->pmMaxLines->show();
    ui->pmMaxLinesLabel->show();
    ui->pmMinTime->show();
    ui->pmMinTimeLabel->show();
  }
  else
  {
    ui->acqWindowTime->show();
    ui->acqWindowTimeLabel->show();
    ui->acqIntervalTime->show();
    ui->acqIntervalTimeLabel->show();
    ui->acqMaxLines->show();
    ui->acqMaxLinesLabel->show();
    ui->acqNbFiles->show();
    ui->acqNbFilesLabel->show();
    ui->pmContinusBackup->hide();
    ui->pmMaxLines->hide();
    ui->pmMaxLinesLabel->hide();
    ui->pmMinTime->hide();
    ui->pmMinTimeLabel->hide();
  }

  if (core && !core->isBenchType("_SOLIAGRAPH"))
  {
    // Compatibilité SoliaGraph grisée (pas cachée), afin de faire connaître cette possibilité si adaptation du Cylix et activation de son option.
    ui->formatSoliaGraph->setEnabled(false);
    ui->formatLabel->setEnabled(false);
    ui->separationGroup->setEnabled(true);
  }
  else
    ui->separationGroup->setDisabled(mAcquisition->formatSoliaGraph->val());
}

void CYAcquisitionSetup::showEvent(QShowEvent *e)
{
  Q_UNUSED(e)
  bool first = isVisible() ? false : true;

  if (core && !mAcquisition)
    if (CYAcquisition *acq = core->acquisitions.value(mAcquisitionName))
      setAcquisition(acq);

  initAcquisition();
  if (first)
    informations();
}

void CYAcquisitionSetup::initAcquisition()
{
  if (mAcquisition)
  {
    ui->acquisitionDataViewHeader->clear();
    QListIterator<CYAcquisitionData *> i(mAcquisition->headerDatas);
    i.toFront();
    while (i.hasNext())
      ui->acquisitionDataViewHeader->addData(i.next()->data()->objectName());
    ui->acquisitionDataViewHeader->refresh();

    ui->acquisitionDataView->clear();
    QListIterator<CYAcquisitionData *> j(mAcquisition->datas);
    j.toFront();
    while (j.hasNext())
      ui->acquisitionDataView->addData(j.next()->data()->objectName());
    ui->acquisitionDataView->refresh();

    ui->otherSeparator->setEnabled(false);
    switch (mAcquisition->separatorType())
    {
    case 1: ui->radioSepButton1->setChecked(true);
      break;
    case 2: ui->radioSepButton2->setChecked(true);
      break;
    case 3: ui->radioSepButton3->setChecked(true);
      break;
    case 4: ui->radioSepButton4->setChecked(true);
      break;
    case 5: ui->radioSepButton5->setChecked(true);
      ui->otherSeparator->setEnabled(true);
      break;
    default:qDebug("void CYAcquisitionSetup::showEvent(QShowEvent *e)");
    }
  }
}

void CYAcquisitionSetup::informations()
{
  if (!mAcquisition)
    return;

  if (!ui->acqPretendTime)
    linkDatas();

  double time;
  if (!mAcquisition->postMortem() && ui->acqIntervalTime->getTmpValue()!=0.0)
    time = (ui->acqPretendTime->getTmpValue()*(ui->acqWindowTime->getTmpValue()/(ui->acqWindowTime->getTmpValue()+ui->acqIntervalTime->getTmpValue())));
  else if (mAcquisition->postMortem() && !ui->pmContinusBackup->value())
    time = ui->acqTimerInput->getTmpValue()/1000.0; // conversion en secondes
  else
    time = ui->acqPretendTime->getTmpValue();

  if (mAcquisition->postMortem())
  {
    uint minTime = ui->pmMinTime->getTmpValue();
    uint timer = ui->acqTimerInput->getTmpValue();
    if (timer)
    {
      double tmp1 = minTime/timer;
      uint tmp2 = (uint)minTime/timer;
      uint nbLines;
      if (tmp1/tmp2>1.0)
        nbLines = tmp2+1;
      else
        nbLines = tmp2;

      uint max = ui->pmMaxLines->maxValue();
      if (nbLines > ui->pmMaxLines->maxValue())
      {
        CYMessageBox::sorry( this, tr( "The number of lines calculated %1 is higher than maximum allowed (%2)!\n"
                                       "Please reduce the acquisition period or its length time." ).arg(nbLines).arg(max) );
        return;
      }
    }
    mAcquisition->informations(ui->acquisitionDataViewHeader->datas, ui->acquisitionDataView->datas, time, (uint)ui->acqTimerInput->getTmpValue(), (uint)ui->pmMaxLines->value(), (uint)ui->acqMaxFiles->value(), (uint)ui->pmMinTime->getTmpValue());
  }
  else
    mAcquisition->informations(ui->acquisitionDataViewHeader->datas, ui->acquisitionDataView->datas, time, (uint)ui->acqTimerInput->getTmpValue(), (uint)ui->acqMaxLines->value(), (uint)ui->acqMaxFiles->value(), (uint)ui->pmMinTime->getTmpValue());

  ui->acqNbFiles->refresh();
  ui->maxFileSize->linkData();
  ui->maxSize->linkData();
}

void CYAcquisitionSetup::designer()
{
  CYWidget::designer();
  informations();
}

void CYAcquisitionSetup::setEnabled(bool val)
{
  linkDatas();
  CYWidget::setEnabled(val);
}


/*!
    \fn CYAcquisitionSetup::setDirPath()
 */
void CYAcquisitionSetup::setDirPath()
{
  ui->dirPathLine->setText(QFileDialog::getExistingDirectory(this, 0, mAcquisition->dirPath->val()));
}
