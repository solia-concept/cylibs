/***************************************************************************
                          cyacquisitiondataview.cpp  -  description
                             -------------------
    begin                : ven avr 30 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisitiondataview.h"
#include "ui_cyacquisitiondataview.h"

// QT
#include <QInputDialog>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QList>
#include <QMimeData>
// CYLIBS
#include "cycore.h"
#include "cydata.h"
#include "cyacquisitiondata.h"
#include "cydatasbrowser.h"
#include "cynethost.h"
#include "cynetlink.h"
#include "cydataslistview.h"

CYAcquisitionDataView::CYAcquisitionDataView(QWidget *parent, const QString &name)
: CYWidget(parent,name), ui(new Ui::CYAcquisitionDataView)
{
  ui->setupUi(this);
  mList = ui->list;
  connect(mList, SIGNAL(modifie()), this, SIGNAL(modifie()));
}

CYAcquisitionDataView::~CYAcquisitionDataView()
{
  delete ui;
}

void CYAcquisitionDataView::remove()
{ 
  if (!mList->currentItem())
    return;
  int pos = mList->currentItem()->text(mList->numColumn()).toInt()-1;
  datas.removeAt(pos);
  mList->deleteData();
  if (isVisible())
    emit modifie();
//  refresh();
}

void CYAcquisitionDataView::refresh()
{
  mList->clear();

  for (int i = 0; i < datas.size(); ++i)
  {
    CYAcquisitionData *d=datas[i];
    mList->addData(d->dataName());
  }
}

void CYAcquisitionDataView::addData(QString dataName)
{
  CYData *data = core->findData(dataName);
  if (!data)
    return;

  CYAcquisitionData *d = new CYAcquisitionData(data, this, dataName, data->label());
  addData(d);
  mList->addData(dataName);
}


void CYAcquisitionDataView::addGroup(QString txt)
{
  if (core)
    core->addGroup(txt, this);
}

void CYAcquisitionDataView::addData(CYAcquisitionData *data)
{
  mList->addData(data->dataName());
  datas.append(data);
}

void CYAcquisitionDataView::clear()
{
  if (mList->topLevelItemCount()>0)
    mList->clear();
  if (datas.count()>0)
  datas.clear();
}

void CYAcquisitionDataView::edit()
{
  if (!mList->currentItem())
    return;
  bool ok;
  QString text = QInputDialog::getText(this, tr("Data acquisition label"), tr("Enter the data acquisition label: "), QLineEdit::Normal, mList->currentItem()->text(1), &ok);
  if (ok && !text.isEmpty())
  {
    mList->currentItem()->setText(mList->labelColumn(), text);
    if (isVisible())
      emit modifie();
  }
}

CYDatasListView *CYAcquisitionDataView::list()
{
  return mList;
}

