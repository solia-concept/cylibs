/***************************************************************************
                          cyaquisitionwidget.h  -  description
                             -------------------
    begin                : jeu mai 6 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYAQUISITIONWIDGET_H
#define CYAQUISITIONWIDGET_H

// QT
#include <qtabwidget.h>

class CYAcquisitionView;
class CYAcquisitionSetup;

/** @short Widget d'acquisition.
  * QTabWidget constitué d'onglets de visualisation et de configuration
  * de l'acquisition.
  * @author LE CLÉACH Gérald
  */

class CYAquisitionWidget : public QTabWidget
{
  Q_OBJECT

public:
  CYAquisitionWidget(QWidget *parent=0, const QString &name=0);
  ~CYAquisitionWidget();

protected: // Protected attributes
  /** Widget de visualisation. */
  CYAcquisitionView *mView;
  /** Widget de configuration. */
  CYAcquisitionSetup *mSetup;
};

#endif
