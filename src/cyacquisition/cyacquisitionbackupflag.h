/***************************************************************************
                          cyacquisitionbackupflag.h  -  description
                             -------------------
    begin                : ven jun 18 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITIONBACKUPFLAG_H
#define CYACQUISITIONBACKUPFLAG_H

// CYLIBS
#include "cyflag.h"

class CYString;
class CYTime;
class CYS16;
class CYAcquisition;

/** @short Flag d'archivage d'acquisition.
  * @author LE CLÉACH Gérald
  */

class CYAcquisitionBackupFlag : public CYFlag
{
   Q_OBJECT
   Q_ENUMS(Type)
   Q_ENUMS(Enable)

public:
  enum Enable { None=0, Rising=1, Falling=2, Changing=3 };

  /** Création d'un flag d'archivage d'acquisition.
    * @param db     Base de données parent.
    * @param name   Nom de la donnée.
    * @param custom Type d'activation sur front(s) personnalisable sinon sur front montant.
    * @param val    Valeur de la donnée.
    * @param def    Valeur par défaut. */
  CYAcquisitionBackupFlag(CYDB *db, const QString &name, bool custom, flg *val, const flg def=1);
  ~CYAcquisitionBackupFlag();
  
  /** Saisie le préfixe utilisé pour créer le nom du fichier d'archive. */
  void setPrefixFileName(QString prefix);
  /** @return le préfixe utilisé pour créer le nom du fichier d'archive. */
  QString prefixfileName();

  /** @return l'en-tête de l'archive. */
  QString header();
  /** Saisie l'en-tête de l'archive. */
  void setHeader(QString val);
  /** @return l'en-tête de l'archive avec la date et l'heure. */
  QString headerWithDateTime();

  /** Autorise la gestion d'archivage si \a val vaut \a true. */
  void setEnable(int val);
  /** @return \a true si la gestion d'archivage est autorisée. */
  bool isEnable();

  /** @return la donnée du préfixe utilisé pour créer le nom du fichier d'archive. */
  CYString *dataPrefixfileName() { return mPrefixFileName; }
  /** @return la donnée d'en-tête de l'archive. */
  CYString *dataHeader() { return mHeader; }
  /** @return la donnée d'autorisation la gestion d'archivage. */
  CYS16 *dataEnable() { return mEnable; }
  /** @return la donnée d'écrasement d'archivage. */
  CYFlag *dataOverwrite() { return mOverwrite; }
  /** @return la donnée de retard d'archivage. */
  CYTime *dataDelay() { return mDelay; }
  void init(CYAcquisition *acq, QString prefix, QString header, int enable, bool overwrite);

public slots: // Public slots
  /** Traîtement à chaque changement d'état. Lorsque \a rising vaut \a true alors
    * le changement d'état correspond à un front montant. Dans ce cas on positionne
    * @see mBackupToDo à vrai pour indiquer qu'il faut faire un archivage de l'acquisition. */
  void stateChanged(bool rising);

  /** Demande l'ordre d'émettre le signal d'archivage du post-mortem. */
  void emitBackupToDo();

signals: // Signals
  /** Emis lorsque le flag doit activer l'archivage du post-mortem. */
  void backupToDo(CYAcquisitionBackupFlag *flag);

protected: // Protected attributes
  /** Type d'activation sur front(s) personnalisable sinon sur front montant. */
  bool mCustom;
  /** Préfixe utilisé pour créer le nom du fichier d'archive. */
  CYString *mPrefixFileName;
  /** En-tête de l'archive. */
  CYString *mHeader;
  /** Autorise la gestion d'archivage. */
  CYS16 *mEnable;
  /** Écrase l'archive précédente ou incrémente les archives. */
  CYFlag *mOverwrite;
  /** Acquisition rattachée. */
  CYAcquisition * mAcquisition;
  /** Retard sur évènement. */
  CYTime *mDelay;
};

#endif
