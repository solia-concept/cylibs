/***************************************************************************
                          cyacquisition.cpp  -  description
                             -------------------
    begin                : jeu avr 29 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisition.h"

// QT
#include <QTextStream>
// CYLIBS
#include "cycore.h"
#include "cydb.h"
#include "cynethost.h"
#include "cyf64.h"
#include "cyu32.h"
#include "cys32.h"
#include "cytime.h"
#include "cyflag.h"
#include "cystring.h"
#include "cyeventreport.h"
#include "cyeventsmanager.h"
#include "cyacquisitiondata.h"
#include "cyacquisitionbackupflag.h"
#include "cydblibs.h"
#include "cycore.h"

CYAcquisition::CYAcquisition(QObject *parent, const QString &name, QString label, CYFlag *enable, CYNetLink *link, int poste)
: QObject(parent),
  enableFlag(enable),
  pauseFlag(0),
  mLink(link),
  mLabel(label),
  mPoste(poste)
{
  setObjectName(name);
  mDB = 0;
}

CYAcquisition::CYAcquisition(QObject *parent, const QString &name, QString label, CYFlag *enable, CYFlag *pause, CYNetLink *link, int poste)
: QObject(parent),
  enableFlag(enable),
  pauseFlag(pause),
  mLink(link),
  mLabel(label),
  mPoste(poste)
{
  setObjectName(name);
  mDB = 0;
}

void CYAcquisition::init()
{
  mFile = 0;
  mNbLinesFile   = 0;
  mCurrentBuffer = 2;
  mRunning       = false;
  mUseNewFile    = true;
  mIsOnFlag      = 0;
  mFileNum       = 0;
  mNoMaxLinesFile= false;
  mSaveSetup     = true;

  mTotalTime    = 0.0;
  mWindowTime   = 0.0;
  mIntervalTime = 0.0;
  mPostMortem   = false;

  if (!mDB)
  {
    // rend globales les données de la première acquisition rapide pour utiliser les aides de ses données dans cydoc
    bool global = false;
    static bool first_fast = true;
    bool fast = inherits("CYAcquisitionFast") ? true : false;
    if (first_fast && fast)
    {
      global = true;
      first_fast = false;
    }
    if (core)
      mDB = new CYDB(core->localHost(), QString("DB_%1").arg(objectName()), -1, 0, global);
    else
      mDB = new CYDB(new CYNetHost(this, QString("Host_%1").arg(objectName())), QString("DB_%1").arg(objectName()), -1, 0, global);
    mDB->setGroup(mLabel);
    mDB->setAlwaysOk(true);
    if (core)
    {
      // sauvegarde par défaut (peut être écrasé si sauvegarde par projet)
      setSetupFileName(QString(core->appSettingDir()+"%1.cyacq").arg(objectName()));
    }
  }

  mStarted = new CYFlag(mDB, "STARTED", new flg, 0);
  mStarted->setLabel(tr("Acquisition started"));
  mStarted->setVolatile(true);

  mPausing  = new CYFlag(mDB, "PAUSE" , new flg, 0);
  mPausing->setLabel(tr("Acquisition pausing"));
  mPausing->setVolatile(true);

  mWindow = new CYFlag(mDB, "WINDOW", new flg, 0);
  mWindow->setLabel(tr("Acquisition window in progress"));
  mWindow->setVolatile(true);

  mSaving = new CYFlag(mDB, "SAVING", new flg, 0);
  mSaving->setLabel(tr("Acquisition saving"));
  mSaving->setVolatile(true);

  saveTimer = new CYTime(mDB, "SAVE_TIMER", new u32, "0m5s", "0m1s", "10m0s");
  saveTimer->setLabel(tr("Save acquisition file timer"));
  saveTimer->setVolatile(true);

  postMortemBackupTimer = new CYTime(mDB, "POSTMORTEM_BACKUP_TIMER", new u32, "1h0m0s", "0h0m0s", "500h0m0s", 500.0);
  postMortemBackupTimer->setLabel(tr("Post-mortem periodical backup timer"));

  postMortemBackupTimerFlag = new CYAcquisitionBackupFlag(mDB, "POSTMORTEM_BACKUP_TIMER_FLAG", false, new flg);
  postMortemBackupTimerFlag->setLabel(tr("Periodical backup"));
  addPostMortemFlags(postMortemBackupTimerFlag, "periodic");

  dirName = new CYString(mDB, "DIR_NAME", new QString(tr("Acquisition")));
  dirName->setLabel(tr("Acquisition files directory"));

  useTestDirectory = new CYFlag(mDB, "USE_TEST_DIRECTORY", new flg, true);
  useTestDirectory->setLabel(tr("Use test directory"));

  dirPath = new CYString(mDB, "DIR_PATH", new QString(core ? core->baseDirectory() : ""));
  dirPath->setLabel(tr("Directory path"));
  dirPath->setHelp(tr("This path is used to save acquisition files somewhere else than in the test directory."));
  dirPath->setHelpDesignerValue(false);

  prefixFileName = new CYString(mDB, "PREFIX_FILE");
  prefixFileName->setLabel(tr("Acquisition files prefix"));

  extensionFileName = new CYString(mDB, "EXTENSION_FILE", new QString("csv"));
  extensionFileName->setLabel(tr("Acquisition files extension"));

  maxLinesFile = new CYU32(mDB, "MAX_LINES_FILE", new u32, 1048576, 1, 1048576);
  maxLinesFile->setLabel(tr("Maximum of lines per acquisition file"));
  maxLinesFile->addNote(tr("The maximum is set at 1,048,576 for use with acquisition files from Excel 2007 and LibreOffice Calc 4.2."));

  maxNbFiles = new CYS32(mDB, "MAX_NB_FILES", new s32, 1000, 10, 1000000);
  maxNbFiles->setLabel(tr("Maximum of acquisition files"));
  maxNbFiles->setHelp(tr("The acquisition saving will restart from the first file if this acquisition file number is reached."));
  maxNbFiles->addNote(tr("Choose sensibly this number if you don't want to overwrite the oldest acquisition files."));

  nbFiles = new CYU32(mDB, "NB_FILES", new u32);
  nbFiles->setLabel(tr("Number of acquisition files"));
  nbFiles->setVolatile(true);
  nbFiles->setHelpDesignerValue(false);

  formatSoliaGraph = new CYFlag(mDB, "FORMAT_SOLIAGRAPH", new flg, false); // \a false par défaut pour garder la compatibilité des projets existants
  formatSoliaGraph->setLabel(tr("SoliaGraph compatibility"));
  formatSoliaGraph->setHelpFalse(tr("Preserves import compatibility with existing post-processing files (e.g. .ods or .xls spreadsheets) by using the old Cylix acquisition file format. These include a time counter (in ms), initialized to 0 at the start of acquisition."));
  formatSoliaGraph->setHelpTrue(tr("Makes Cylix CSV acquisition files compatibled with SoliaGraph Web software for easy visualization them in the form of time curves. For this, generate files has a defined format and a timestamp (in µs)."));
  if (core && !core->isBenchType("_SOLIAGRAPH"))
    formatSoliaGraph->addNote(tr("The integration of SoliaGraph in Cylix provides direct access and this generation of compatible CSV files. If you are interested in this feature, please contact us to available it."));

	PMContinusBackup = new CYFlag(mDB, "PM_CONTNUOUS_BACKUP", new flg, false);
	PMContinusBackup->setLabel(tr("Continuous backup")); // Sauvegarde continue
	PMContinusBackup->setHelpTrue(tr("In addition to saving post-mortem recordings by configurable events, you can also save all post-mortem recordings to multiple files.")+"<br>"+
                                 tr("Generates a time-stamped acquisition file at the end of each acquisition time in sub-directories following the 'year/month/day/hour/minute' tree structure.")+"<br>"+
                                 tr("SoliaGraph allows multiple selection of files or sub-directories to easily perform a temporal analysis of the acquisition by scrolling through the resulting curves.")+"<br>"+
                                 tr ("Please note that this mode of operation can consume a lot of storage space, and should therefore be used sparingly. You can simulate the space required in the 'Files' tab.")
                             );
	PMContinusBackup->setHelpFalse(tr("Generates an acquisition files based only on trigger events configured in the 'Events' tab.")+"<br>"+
                            tr("Storage size is limited by the maximum number of acquisition files configurable in the  'Files' tab."));

	fileSizeOptimization = new CYFlag(mDB, "FILE_SIZE_OPTIMIZATION", new flg, true);
	fileSizeOptimization->setLabel(tr("CSV file size optimization"));
	fileSizeOptimization->setHelp(tr("Optimizes CSV file size by using empty fields for unchanged values."));
	fileSizeOptimization->addNote(tr("Compatible with Solia Graph."));
	fileSizeOptimization->addNote(tr("Thanks to this function, you can, without greatly increasing the size of CSV files, add to the acquisition of data with highly variable values other, much less variable values, such as states."));
	fileSizeOptimization->addNote(tr("With this optimization, storage space simulation can give a much higher value than reality."));

  pretendTime = new CYTime(mDB, "PRETEND_TIME", new u32, "2000h0m0s", "0h0m0s", "10000h0m0s", 1000.0);
  pretendTime->setLabel(tr("Acquisition time to pretend"));

  maxFileSize = new CYF64(mDB, "MAX_FILE_SIZE", new f64);
  maxFileSize->setLabel(tr("Maximum file size"));
  maxFileSize->setMax(2000000000.0);
  maxFileSize->setHelpDesignerValue(false);

  maxSize = new CYF64(mDB, "MAX_SIZE", new f64);
  maxSize->setLabel(tr("Maximum size acquisition"));
  maxSize->setMax(1000000000000.0);
  maxSize->setHelpDesignerValue(false);

  startDateTimeString = new CYString(mDB, "START_DATETIME");
  startDateTimeString->setLabel(tr("Start acquisition date time"));

  separator = new CYString(mDB, "SEPARATOR", new QString("\t"));
  separator->setLabel(tr("Acquisition file column separator"));

  otherSeparator = new CYString(mDB, "OTHER_SEPARATOR", new QString("\t"));
  otherSeparator->setLabel(tr("Other acquisition file column separator"));

  setSeparatorType(1);

  if (!inherits("CYScopeAcquisition"))
  {
    core->cydblibs()->setUnderGroup(label());
    backupEvent = new CYFlag( core->cydblibs(), QString("%1_BACKUP_EVENT").arg(objectName()), new flg, false);
    backupEvent->setLabel( tr( "Acquisition backup" ) );

    backupFileName = new CYString( core->cydblibs(), QString("%1_BACKUP_FILENAME").arg(objectName()));
    backupFileName->setLabel( tr( "Backup file" ) );

    backup2Error = new CYFlag( core->cydblibs(), QString("%1_BACKUP2_ERROR").arg(objectName()), new flg, false);
    backup2Error->setLabel( tr( "Post-mortem continuous acquisition archive error" ) );

    backup2FileName = new CYString( core->cydblibs(), QString("%1_BACKUP2_FILENAME").arg(objectName()));
    backup2FileName->setLabel( tr( "Continuous acquisition archive file by post-mortem" ) );

    core->cydblibs()->setUnderGroup(0);
  }

  mSaverTimer = new QTimer(this);
  connect(mSaverTimer, SIGNAL(timeout()), SLOT(save()));

  mPostMortemBackupTimer = new QTimer(this);
  connect(mPostMortemBackupTimer, SIGNAL(timeout()), postMortemBackupTimerFlag, SLOT(emitBackupToDo()));

  mDir = new QDir();

  if (core)
    mPostMortmemDir = new QDir(QString("%1/%2").arg(core->appDataDir()).arg(objectName()));
}

CYAcquisition::~CYAcquisition()
{
}

bool CYAcquisition::addData(CYData *data, QString label)
{
  if (!data)
  {
    qWarning("bool CYAcquisition::addData(CYData *data, QString label)");
    return false;
  }

  if (label.isEmpty())
    label = data->label();

  datas.append(new CYAcquisitionData(data, this, data->objectName(), label));

  return true;
}

bool CYAcquisition::addData(QString dataName, QString label)
{
  return addData(core->findData(dataName), label);
}

bool CYAcquisition::addHeaderData(CYData *data, QString label)
{
  if (!data)
  {
    qWarning("bool CYAcquisition::addHeaderData(CYData *data, QString label)");
    return false;
  }

  if (label.isEmpty())
  {
    label = data->label();
  }

  headerDatas.append(new CYAcquisitionData(data, this, data->objectName(), label));

  return true;
}

bool CYAcquisition::addHeaderData(QString dataName, QString label)
{
  return addHeaderData(core->findData(dataName), label);
}

void CYAcquisition::save()
{
  save(false);
}

void CYAcquisition::save(bool ok)
{
  if (mSaving->val() && !ok)
    return;

  if (!ok)
    mSaving->setVal(true);

  currentFile();
  if (mFile)
  {
    if (mFile->open(QIODevice::Unbuffered|QIODevice::WriteOnly|QIODevice::Append))
    {
      QTextStream stream(mFile);
      stream.setCodec("UTF-8");
      if (mCurrentBuffer==2)
      {
        while (!mBuffer1.isEmpty())
        {
          if (mNoMaxLinesFile || (mNbLinesFile < maxLinesFile->val()))
          {
            // ajout d'une nouvelle ligne
            stream << *mBuffer1.first();
            // TOCHECK QT5
//            mBuffer1.removeFirst();
            delete mBuffer1.takeFirst();
            mNbLinesFile++;
          }
          else // nombre de lignes maximum atteint
            break;
        }
      }
      else
      {
        while (!mBuffer2.isEmpty())
        {
          if (mNoMaxLinesFile || (mNbLinesFile < maxLinesFile->val()))
          {
            // ajout d'une nouvelle ligne
            stream << *mBuffer2.first();
            // TOCHECK QT5
//            mBuffer2.removeFirst();
            delete mBuffer2.takeFirst();
            mNbLinesFile++;
          }
          else // nombre de lignes maximum atteint
            break;
        }
      }
      mFile->close();
      core->backupFile(mFile->objectName());
    }

    // changement de buffer
    if ((mCurrentBuffer==2) && (mBuffer1.isEmpty()))
      mCurrentBuffer = 1;
    else if ((mCurrentBuffer==1) && (mBuffer2.isEmpty()))
      mCurrentBuffer = 2;
    else
    {
      save(true);
      return;
    }
  }

//  emit saved();

  mSaving->setVal(false);
}

void CYAcquisition::currentFile()
{
  QString path;
  QString prefix;
  uint max;
  if (postMortem())
  {
    path = mPostMortmemDir->absolutePath();
    max = 2;
  }
  else
  {
    path = mDir->absolutePath();
    max = maxNbFiles->val();
  }

  core->mkdir(path);
  if (max==1)
  {
    mFileName = QString("%1/%2.%3").arg(path).arg(prefixFileName->val()).arg(extensionFileName->val());
    newFile();
    return;
  }
  else if (mUseNewFile)
  {
    mNbLinesFile = 0;
    mUseNewFile  = false;
    mFileNum++;
    if (mFileNum>max)
      mFileNum = 1;
    mFileName = QString("%1/%2_%3.%4").arg(path).arg(prefixFileName->val()).arg(mFileNum).arg(extensionFileName->val());
    newFile();
    return;
  }
  else if (mNbLinesFile >= maxLinesFile->val())
  {
    mNbLinesFile = 0;
    mUseNewFile  = false;
    mFileNum++;
    if (mFileNum>max)
      mFileNum = 1;
    mFileName = QString("%1/%2_%3.%4").arg(path).arg(prefixFileName->val()).arg(mFileNum).arg(extensionFileName->val());
    newFile();
    return;
  }
  if (mFile)
    delete mFile;
  mFile = new QFile(mFileName);
}

void CYAcquisition::newFile()
{
  if (mFile)
  {
    postMortemBackupContinu();
    delete mFile;
  }
  mFile = new QFile(mFileName);
  mFile->remove();

  if (mFile->open(QIODevice::Unbuffered|QIODevice::WriteOnly))
  {
    header(mFile, mStartTime, headerDatas, datas, mPoste);
    mFile->close();
//    saveSetup();
    return;
  }
  else
  {
    if (mFile)
      delete mFile;
    mFile = 0;
  }
}

void CYAcquisition::header(QFile *file, QDateTime startTime, QList<CYAcquisitionData*> headerList, QList<CYAcquisitionData*> acquisitionList, int poste)
{
  QDate date = startTime.date();
  QTime time = startTime.time();
  startDateTimeString->setVal(QString("%1 %2").arg(date.toString(Qt::ISODate)).arg(time.toString(Qt::ISODate)));
  QString sep;

  QTextStream stream(file);
  stream.setCodec("UTF-8");
  if (formatSoliaGraph->val())
  {
    sep=";";
    stream << core->designerRef()        << sep
           << core->customerName()       << sep
           << date.toString(Qt::ISODate) << sep
           << time.toString(Qt::ISODate) << sep
           << core->testDirPath(poste)   << "\n";
  }
  else
  {
    sep=separator->val();
    stream << QString("%1:%2\n").arg(core->customerName()).arg(core->designerRef());
    stream << core->testDirPath(poste) << "\n";
    stream << QString(tr("Acquisition since:")+"\t%1\n\n").arg(startDateTimeString->val());
  }

  if (formatSoliaGraph->val())
  {
    // libellés
    stream << tr("Timestamp");
    QListIterator<CYAcquisitionData *> i(acquisitionList);
    i.toFront();
    while (i.hasNext())
    {
      CYAcquisitionData *data =i.next();
      stream << sep << data->label();
    }
    // unités
    stream << "\n" << tr("us"); // La base de temps SoliaGraph est en µs pour le type de fichier SoliaTouch - Cylix.
    i.toFront();
    while (i.hasNext())
    {
      CYAcquisitionData *data =i.next();
      stream << sep << data->data()->unit();
    }
    // types
    stream << "\n" << "0";
    i.toFront();
    while (i.hasNext())
    {
      i.next();
      stream  << sep << "1";
    }

    if (headerList.count()>0)
    {
      stream << "\n";
      QListIterator<CYAcquisitionData *> it2(headerList);
      it2.toFront();
      while (it2.hasNext())
      {
        CYAcquisitionData *data =it2.next();
        stream << QString("%1:\t%2\n").arg(data->label()).arg(data->data()->toString());
      }
    }
    file->flush();
  }
  else
  {
    if (headerList.count()>0)
    {
      stream << "\n";
      QListIterator<CYAcquisitionData *> i(headerList);
      i.toFront();
      while (i.hasNext())
      {
        CYAcquisitionData *data =i.next();
        stream << QString("%1:\t%2\n").arg(data->label()).arg(data->data()->toString());
      }
    }

    stream << tr("TIME (ms)");

    QListIterator<CYAcquisitionData *> i(acquisitionList);
    i.toFront();
    while (i.hasNext())
    {
      CYAcquisitionData *data =i.next();
      stream << QString("%1%2 (%3)").arg(sep).arg(data->label()).arg(data->data()->unit());
    }
  }

  stream << "\n";
}

void CYAcquisition::clearSetup()
{
  db()->designer();
  datas.clear();
  headerDatas.clear();
}

bool CYAcquisition::loadSetup()
{
  return loadSetup(mSetupFileName);
}

bool CYAcquisition::loadSetup(const QString &filename)
{
  if (!mSaveSetup)
    return true;

  clearSetup();
  QFile file(filename);
  if (!file.open(QIODevice::ReadOnly))
  {
/*    saveSetup();
    CYMESSAGETEXT(tr("Can't open the file %1").arg(filename));*/
    return (false);
  }

  QString errorMsg;
  int errorLine;
  int errorColumn;

  QDomDocument doc;
  // Lit un fichier et vérifie l'en-tête XML.
  if (!doc.setContent(&file, &errorMsg, &errorLine, &errorColumn))
  {
    CYWARNINGTEXT(tr("The file %1 does not contain valid XML !\n"
                       "%2: %3 %4").arg(filename).arg(errorMsg).arg(errorLine).arg(errorColumn));
    return (false);
  }
  // Vérifie le type propre du document.
  if (doc.doctype().name() != "CYLIXAcquisitionSetup")
  {
    CYWARNINGTEXT(tr("The file %1 does not contain a valid definition, which must have a document type").arg(filename)+" 'CYLIXAcquisitionSetup'");
    return (false);
  }

  // Charge le contenu du fichier de configuration d'acquisition.
  QDomElement el = doc.documentElement();
  loadSetup(el);

  mStarted->setVal(false);
  mPausing->setVal(false);
  mWindow->setVal(false);
  mSaving->setVal(false);

  return (true);
}

void CYAcquisition::loadSetup(QDomElement &el)
{
  mFileNum = el.attribute("FileNum").toUInt();
  setSeparatorType(el.attribute("SeparatorType").toUInt());

  QDomNodeList list;

  // Connection des différents données à afficher en en-tête.
  list = el.elementsByTagName("HeaderData");
  for (int i = 0; i < list.count(); ++i)
  {
    QDomElement el = list.item(i).toElement();
    CYData *d = core->db.value(el.attribute("dataName"));
    if (!d)
    {
      CYWARNING;
      return;
    }
    else
      addHeaderData(d, el.attribute("dataLabel"));
  }

  // Connection des différents données à acquérir.
  list = el.elementsByTagName("Item");
  for (int i = 0; i < list.count(); ++i)
  {
    QDomElement el = list.item(i).toElement();
    CYData *d = core->db.value(el.attribute("dataName"));
    if (!d)
    {
      qWarning("void CYAcquisition::loadSetup(QDomDocument &doc)");
      return;
    }
    else
      addData(d, el.attribute("dataLabel"));
  }

  mDB->loadFromDOM(el);

  if (core && !core->isBenchType("_SOLIAGRAPH") && formatSoliaGraph->val())
    formatSoliaGraph->setVal(false);

  setNameDir(dirName->val());
}

bool CYAcquisition::saveSetup()
{
  return saveSetup(mSetupFileName);
}

bool CYAcquisition::saveSetup(const QString &filename)
{
  if (!mSaveSetup)
    return true;

  if (filename.isEmpty())
    return false;

  QDomDocument doc("CYLIXAcquisitionSetup");
  doc.appendChild(doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));

  // Enregistre la configuration.
  QDomElement el = doc.createElement("Acquisition");
  doc.appendChild(el);
  saveSetup(doc, el);

  QFile file(filename);
  if (!file.open(QIODevice::WriteOnly))
  {
    CYWARNINGTEXT(tr("Can't save file %1!").arg(filename));
    return (false);
  }
  QTextStream s(&file);
  s.setCodec("UTF-8");
  s << doc;
  file.close();

  return (true);
}

void CYAcquisition::saveSetup(QDomDocument &doc, QDomElement &el)
{
  el.setAttribute("FileNum", mFileNum);
  el.setAttribute("SeparatorType", mSeparatorType);

  QListIterator<CYAcquisitionData *> i(headerDatas);
  i.toFront();
  while (i.hasNext())
  {
    CYAcquisitionData *d =i.next();
    QDomElement item = doc.createElement("HeaderData");
    el.appendChild(item);
    item.setAttribute("dataName", d->objectName());
    item.setAttribute("dataLabel", d->label());
  }

  QListIterator<CYAcquisitionData *> j(datas);
  j.toFront();
  while (j.hasNext())
  {
    CYAcquisitionData *d =j.next();
    QDomElement item = doc.createElement("Item");
    el.appendChild(item);
    item.setAttribute("dataName", d->objectName());
    item.setAttribute("dataLabel", d->label());
  }

  mDB->saveToDOM(doc, el);
}

void CYAcquisition::startSaving()
{
  mSaverTimer->start(saveTimer->val());
}

void CYAcquisition::stopSaving()
{
  mSaverTimer->stop();
  while (mSaving->val());
  if (mFile)
    mFile->close();
  mStarted->setVal(false);
  mPausing->setVal(false);
  mWindow->setVal(false);
  mSaving->setVal(false);
}

void CYAcquisition::setNameDir(QString path)
{
  bool newDir = false;
  QString absolutePath;
  if (useTestDirectory->val())
    absolutePath = core->testDirPath(mPoste);
  else
    absolutePath = dirPath->val();

  if (absolutePath.isEmpty())
    return;

  while (path.contains('/'))
  {
    QString s = "/"+path.section('/', 0, 0);
    absolutePath.append(s);
    path.remove(0, path.indexOf('/')+1);
    mDir->setPath(absolutePath);
    if (!mDir->exists())
    {
      newDir = true;
      core->mkdir(absolutePath);
    }
  }
  if (!path.isEmpty())
  {
    absolutePath.append('/'+path);
  }
  mDir->setPath(absolutePath);

  if (!mDir->exists())
  {
    newDir = true;
    core->mkdir(absolutePath);
  }

  if (newDir)
  {
    mFileNum = 0;
    saveSetup();
  }
}

void CYAcquisition::informations(QList<CYAcquisitionData*> headerList, QList<CYAcquisitionData*> acquisitionList, double pretendTime, uint timer, uint nbLines, uint maxFiles, uint minTime)
{
  double pretendTimeMSEC = (double)(pretendTime*1000);
  double maxFileSizeTmp=0, maxSizeTmp;

  QString fileName = QString("%1/cylix.%2").arg(core->appDataDir()).arg(extensionFileName->val());

  QFile *file = new QFile(fileName);
  file->remove();
  if (file->open(QIODevice::WriteOnly))
  {
    header(file, QDateTime::currentDateTime(), headerList, acquisitionList, mPoste);
    file->close();
  }
  QFileInfo fi(fileName);
  uint headSize = fi.size();

  file->remove();
  QString sep = (formatSoliaGraph->tmp()) ? ";" : separator->val();
  if (file->open(QIODevice::WriteOnly))
  {
    QTextStream stream(file);
    stream.setCodec("UTF-8");
    double maxPretendTimeMSEC = pretendTimeMSEC*maxFiles; // permet d'ajuster la taille maximale du fichier
    uint maxNbCharTime = QString("%1").arg(maxPretendTimeMSEC).length();
    stream << QString("%1").arg(maxPretendTimeMSEC, maxNbCharTime, 'f', 0);
    QListIterator<CYAcquisitionData *> i(acquisitionList);
    i.toFront();
    while (i.hasNext())
    {
      CYAcquisitionData *data =i.next();
      stream << QString("%1%2").arg(sep).arg(data->data()->stringDef());
    }
    stream << QString("\n");
    file->close();
  }
  fi.setFile(*file);
  uint lineSize = fi.size();

  maxSizeTmp = 0.0;
  if (mPostMortem)
  {
    if (timer)
    {
      double tmp1 = minTime/timer;
      uint tmp2 = (uint)minTime/timer;
      if (tmp1/tmp2>1.0)
        nbLines = tmp2+1;
      else
        nbLines = tmp2;
      maxLinesFile->setVal(nbLines);
      maxFileSizeTmp = headSize + lineSize*nbLines;
			if (!PMContinusBackup->val())
        nbFiles->setVal(maxFiles);
      maxSizeTmp = maxFileSizeTmp*maxFiles;
    }
  }
	if (!mPostMortem || PMContinusBackup->tmp()) // si acquisition continue ou post-morten avec stockage continu
  {
    uint cptFiles = 0;
    if ((timer!=0) && (nbLines!=0))
      cptFiles = (uint)(pretendTimeMSEC/(timer*nbLines));
    double fullFilesTime = 0;
    double fullFilesSize = 0;
    if (cptFiles)
    {
      // temps total des fichiers remplis
      fullFilesTime = cptFiles*timer*nbLines;
      maxFileSizeTmp = headSize + lineSize*nbLines;
			if (!PMContinusBackup->tmp() && cptFiles>maxFiles)
        cptFiles=maxFiles;
      fullFilesSize = cptFiles*(maxFileSizeTmp);
    }

    double lastFileTime = 0;
    double lastFileSize = 0;
    if ((fullFilesTime<pretendTimeMSEC) && (cptFiles<maxFiles))
    {
      // temps du dernier fichier incomplet
      lastFileTime = (double)(pretendTimeMSEC - fullFilesTime);
      uint lines = (uint)(lastFileTime/timer);
      lastFileSize = headSize + lineSize*(1 + lines);
      if (cptFiles==0)
        maxFileSizeTmp = lastFileSize;
      cptFiles++;
    }

    nbFiles->setVal(cptFiles);
    maxSizeTmp += fullFilesSize + lastFileSize;
  }

  if (maxFileSizeTmp<1000.0)
  {
    maxFileSize->format()->setUnit(tr("bytes"));
    maxFileSize->format()->setNbDec(0);
    maxFileSize->setVal(maxFileSizeTmp);
  }
  else if (maxFileSizeTmp<1000000.0)
  {
    maxFileSize->format()->setUnit(tr("Kb"));
    maxFileSize->format()->setNbDec(3);
    maxFileSize->setVal(maxFileSizeTmp/1000.0);
  }
  else if (maxFileSizeTmp<1000000000.0)
  {
    maxFileSize->format()->setUnit(tr("Mb"));
    maxFileSize->format()->setNbDec(3);
    maxFileSize->setVal(maxFileSizeTmp/1000000.0);
  }
  else if (maxFileSizeTmp>=1000000000.0)
  {
    maxFileSize->format()->setUnit(tr("Gb"));
    maxFileSize->format()->setNbDec(3);
    maxFileSize->setVal(maxFileSizeTmp/1000000000.0);
  }
  if (maxSizeTmp<1000.0)
  {
    maxSize->format()->setUnit(tr("bytes"));
    maxSize->format()->setNbDec(0);
    maxSize->setVal(maxSizeTmp);
  }
  else if (maxSizeTmp<1000000.0)
  {
    maxSize->format()->setUnit(tr("Kb"));
    maxSize->format()->setNbDec(3);
    maxSize->setVal(maxSizeTmp/1000.0);
  }
  else if (maxSizeTmp<1000000000.0)
  {
    maxSize->format()->setUnit(tr("Mb"));
    maxSize->format()->setNbDec(3);
    maxSize->setVal(maxSizeTmp/1000000.0);
  }
  else if (maxSizeTmp>=1000000000.0)
  {
    maxSize->format()->setUnit(tr("Gb"));
    maxSize->format()->setNbDec(3);
    maxSize->setVal(maxSizeTmp/1000000000.0);
  }
}

void CYAcquisition::useNewFile()
{
  mUseNewFile = true;
}

void CYAcquisition::postMortemBackupContinu()
{
	if (postMortem() && PMContinusBackup->val())
  {
    QDateTime dt = QDateTime::currentDateTime();
    QString path = mDir->absolutePath()
        +"/"+dt.toString("yyyy")
        +"/"+dt.toString("MM")
        +"/"+dt.toString("dd")
        ;

		QString suffix = dt.toString("yyMMdd_hhmmss");		
		// Vérifie que le fichier n'a pas déjà été créé par appels successifs.
		if (backup2FileName->val().contains(suffix))
			return;

    if (prefixFileName->val().isEmpty())
			backup2FileName->setVal(QString(path+"/"+suffix+".csv"));
    else
			backup2FileName->setVal(QString(path+"/"+prefixFileName->val()+"-"+suffix+".csv"));

		/** Afin d'éviter une erreur @see backup2Error impliquant une alerte sous Cylix, vérifie que le fichier n'a pas déjà été créé par appels successifs. */
		if (QFile::exists(backup2FileName->val()))
			if (!QFile::remove(backup2FileName->val()))
					CYWARNING

		save();

		/** Vérifie que le fichier n'est pas vide. */
		if (!mFile->size())
			return;

		core->disableCYWARNING = true;
		core->mkdir(path);
		core->disableCYWARNING = false;

		if (!mFile->copy(backup2FileName->val()))
			backup2Error->setVal(true);
	}
}

void CYAcquisition::postMortemBackup(CYAcquisitionBackupFlag *flag)
{
  if (!isOn())
    return;

  if (!mPostMortem || !flag->isEnable())
    return;

  while (!mBuffer1.isEmpty() || !mBuffer2.isEmpty())
    save();

  if (!maxNbFiles->val()) // aucun fichier de stockahe autorisé
    return;

  QString txt = core->postMortemBackupFileName(flag);

  QStringList listFiles;
  QString string;

  QString absolutePath = mDir->absolutePath();
  QDir dir = QDir(absolutePath);

  core->mkdir(absolutePath);
  QString fileName;

  // liste des fichiers dans le répertoire
  listFiles = dir.entryList(QDir::Files, QDir::Time | QDir::Reversed );

  if (listFiles.count()>=maxNbFiles->val()) // trop de fichier dans le répertoire
  {
    int nb=listFiles.count();
    // suppression des fichiers les plus anciens
    for (QStringList::Iterator it = listFiles.begin(); it != listFiles.end(), nb>=maxNbFiles->val(); ++it)
    {
      string = *it;
      QFile::remove(dir.absolutePath()+"/"+(*it));
      nb--;
    }
  }

  // nouvelle liste des fichiers dans le répertoire
  listFiles = dir.entryList(QDir::Files, QDir::Time);

  if (flag->dataOverwrite()->val())
  {
    fileName = QString("%1/%2.%3").arg(dir.absolutePath()).arg(txt).arg(extensionFileName->val());
  }
  else
  {
    // recherche du plus récent
    int num=0;
    for (QStringList::Iterator it = listFiles.begin(); it != listFiles.end(); ++it)
    {
      string = *it;
      if (string.contains(txt))
      {
        string = string.remove(txt+"_");
        num = string.remove(QString(".%1").arg(extensionFileName->val())).toUInt();
        break;
      }
    }

    if (num<maxNbFiles->val())
      num++;
    else
      num=1;

    fileName = QString("%1/%2_%3.%4").arg(dir.absolutePath()).arg(txt).arg(num).arg(extensionFileName->val());
  }

  // Copie du répertoire des fichiers Post-Mortem pour ne pas gêner la mise à jour de ceux-ci et donc
  // des pertes de points d'acquisition sur les archivages suivants.
  QString tmpPath = QString("%1_%2").arg(mPostMortmemDir->absolutePath()).arg(QDateTime::currentMSecsSinceEpoch());
  if (!core->copyDirectory(mPostMortmemDir->absolutePath(), tmpPath)) {
    CYWARNINGTEXT(QString("Failed to copy directory %1 to %2").arg(mPostMortmemDir->absolutePath()).arg(tmpPath));
    return;
  }

  QDir tmpDir(tmpPath);
  QFile file(fileName);
  if (file.exists())
    file.remove();
  if (file.open(QIODevice::WriteOnly))
  {
    QTextStream stream(&file);
    stream.setCodec("UTF-8");

    bool firstFile = true;          // premier fichier lu
    QStringList headerLines;       // lignes d'en-tête
    QString sep;
    QStringList acqLines; // lignes d'acquisition
    listFiles = tmpDir.entryList(QDir::Files);
    for (QStringList::Iterator it = listFiles.begin(); it != listFiles.end(); ++it)
    {
      string = *it;
      QFile f(tmpPath+"/"+string);
      if (!f.open(QIODevice::ReadOnly))
        return;
      QTextStream tline(&f);
      tline.setCodec("UTF-8");

      bool header   = true;
      bool dtok      = false; // bonne date et heure

      int cptLine = 0;
      int headerLastLineMax = 0;
      while (!tline.atEnd())
      {
        QString line = tline.readLine()+"\n";
        cptLine++;

        bool headerLastLine;
        if (header) // recherche dernière ligne de l'en-tête
        {
          sep = ";";
          if (line.startsWith(tr("Timestamp")))
            headerLastLineMax=cptLine+2; // 3 lignes après (unités et types)
          headerLastLine = (headerLastLineMax && cptLine==headerLastLineMax) ? true : false;
        }
        else
        {
          sep = separator->val();
          headerLastLine = line.startsWith(tr("TIME"));
        }

        if (header && headerLastLine) // derniere ligne de l'en-tête
        {
          if (firstFile)
          {
            headerLines << line;
            headerLines << flag->headerWithDateTime() << "\n\n";
          }
          header = false;
        }
        else if (header) // nouvelle ligne de l'en-tête
        {
          if (formatSoliaGraph->val() || line.contains(startDateTimeString->val()))
            dtok = true;

          if (firstFile)
            headerLines << line;
        }
        else if (dtok) // nouvelle ligne d'acquisition
          acqLines << line;
        else // mauvaise date d'acquisition
          break;
      }

      if (firstFile && dtok) // validation du premier fichier lu
      {
        for (QStringList::Iterator itline = headerLines.begin(); itline != headerLines.end(); ++itline)
          stream << *itline;
        headerLines.clear();
        firstFile = false;
      }
      else if (firstFile)
        headerLines.clear();

      f.close();
    }
    acqLines.sort();
    int firstLine = acqLines.count() - maxLinesFile->val();
    if (firstLine<0)
      firstLine = 0;
    for (QStringList::Iterator itline = acqLines.begin(); itline != acqLines.end(); ++itline)
    {
      if (firstLine>0)
        firstLine--;
      if (firstLine==0)
        stream << *itline;
    }
    backupFileName->setVal(fileName);
    backupEvent->setVal(true);
    file.close();

    if (!core->removeDirectory(tmpPath))
      return;
  }
}

void CYAcquisition::addPostMortemFlags(CYAcquisitionBackupFlag *flag, QString prefix, QString header, int enable, bool overwrite)
{
  if (flag == postMortemBackupTimerFlag)
    flag->init(this, prefix, flag->group(true)+": "+flag->label(), enable, overwrite);
  else if (header.isEmpty())
    flag->init(this, prefix, flag->group(false)+": "+flag->label(), enable, overwrite);
  else
    flag->init(this, prefix, header, enable, overwrite);

  mPostMortemFlags.append(flag);
  connect(flag, SIGNAL(backupToDo(CYAcquisitionBackupFlag *)), SLOT(postMortemBackup(CYAcquisitionBackupFlag *)));
}

void CYAcquisition::setSeparatorType(uint val)
{
  mSeparatorType = val;
  switch (val)
  {
    case 1: separator->setVal("\t");
            break;
    case 2: separator->setVal(";");
            break;
    case 3: separator->setVal(",");
            break;
    case 4: separator->setVal(" ");
            break;
    case 5: separator->setVal(otherSeparator->val());
            break;
    default:qWarning("void CYAcquisition::setSeparatorType(uint val)");
  }
}

void CYAcquisition::removeFiles()
{
  QDir *dir;
  if (postMortem())
    dir = mPostMortmemDir;
  else
    dir = mDir;
  QStringList listFiles = dir->entryList(QDir::Files);
  for (QStringList::Iterator fileName = listFiles.begin(); fileName != listFiles.end(); ++fileName)
  {
    QFile f(dir->absolutePath()+"/"+(*fileName));
    f.remove();
  }
}

bool CYAcquisition::postMortem()
{
  return mPostMortem;
}

void CYAcquisition::setPostMortem(bool mode)
{
  mPostMortem = mode;
  if (mPostMortem)
  {
    maxNbFiles->setMin(0);
    maxNbFiles->setLabel(tr("Maximum of acquisition files on events"));
    maxNbFiles->addNote(tr("In continuous storage, this maximum is not used, as the number of files per hour is naturally low."));
    maxNbFiles->addNote(tr("To simulate only the memory space required for continuous storage with simulation time, you can set this value to 0."));
    maxNbFiles->addNote(tr("You can also enter 0 to disable backup on event."));
  }
}

bool CYAcquisition::started()
{
  if ( mStarted )
    return mStarted->val();
  return false;
}

bool CYAcquisition::isOn()
{
  if (mIsOnFlag)
    return mIsOnFlag->val();
  return false;
}

CYFlag *CYAcquisition::isOnFlag()
{
  return mIsOnFlag;
}

void CYAcquisition::setIsOnFlag(CYFlag *flag)
{
  mIsOnFlag = flag;
}

void CYAcquisition::setIsOnFlag(QString flagName)
{
  setIsOnFlag((CYFlag *)core->findData(flagName));
}


/*! Ajoute une ligne d'acquisition
    \fn CYAcquisition::addLine(QString line, bool append)
 */
void CYAcquisition::addLine(QString line, bool append)
{
  addLine(new QString(line), append);
}


/*! Ajoute une ligne d'acquisition
    \fn CYAcquisition::addLine(QString *line, bool append)
 */
void CYAcquisition::addLine(QString *line, bool append)
{
	if (append)
	{
	  if (mCurrentBuffer==1)
	    mBuffer1.append(line);
	  else
	    mBuffer2.append(line);
	}
	else
	{
	  if (mCurrentBuffer==1)
	    mBuffer1.prepend(line);
	  else
	    mBuffer2.prepend(line);
	}
}


/*! @return le numéro du fichier courant.
    Le nom du fichier courant est constitué de ce numéro précédé du
    préfixe du nom de fichier (@see prefixFileName).
    \fn CYAcquisition::fileNum()
 */
uint CYAcquisition::fileNum()
{
  return mFileNum;
}

/*! Saisie le numéro du fichier courant.
    Le nom du fichier courant est constitué de ce numéro précédé du
    préfixe du nom de fichier (@see prefixFileName).
    \fn CYAcquisition::setFileNum(uint num)
 */
void CYAcquisition::setFileNum(uint num)
{
  mFileNum = num;
}


/*! @return le nombre de flags d'archivage disponible
    \fn CYAcquisition::postMortemFlags()
 */
int CYAcquisition::postMortemFlags()
{
  return mPostMortemFlags.count();
}


/*! @return le flag d'archivage à l'index \a index
    \fn CYAcquisition::postMortemFlag(int index)
 */
CYAcquisitionBackupFlag *CYAcquisition::postMortemFlag(int index)
{
  return mPostMortemFlags.at(index);
}
