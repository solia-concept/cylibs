/***************************************************************************
                          cyacquisition.h  -  description
                             -------------------
    begin                : jeu avr 29 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITION_H
#define CYACQUISITION_H

// QT
#include <QObject>
#include <QList>
#include <QFile>
#include <QDateTime>
#include <QDomElement>
#include <QDir>
#include <QTimer>
// CYLIBS
#include "cyacquisitiondata.h"

class CYDB;
class CYData;
class CYF64;
class CYF32;
class CYU32;
class CYS32;
class CYTime;
class CYFlag;
class CYString;
class CYAcquisitionBackupFlag;
class CYNetLink;

/** @short Acquisition sur disque dur.
  * @author LE CLÉACH Gérald
  */

class CYAcquisition : public QObject
{
  Q_OBJECT
  Q_ENUMS(Separator)

public:
  /** Constructeur d'acquisition sans gestion de mise en pause.
    * @param parent Parent.
    * @param name   Nom.
    * @param label  Etiquette.
    * @param enable Flag d'activation.
    * @param link   Connexion réseau.
    * @param idPrj  Index de projet (0 si un seul project actif à la fois).*/
  CYAcquisition(QObject *parent, const QString &name, QString label, CYFlag *enable, CYNetLink *link=0, int idPrj=0);
  /** Constructeur d'acquisition avec gestion de mise en pause.
    * @param parent Parent.
    * @param name   Nom.
    * @param label  Etiquette.
    * @param enable Flag d'activation.
    * @param pause  Flag de mise en pause.
    * @param link   Connexion réseau.
    * @param idPrj  Index de projet (0 si un seul project actif à la fois).*/
  CYAcquisition(QObject *parent, const QString &name, QString label, CYFlag *enable, CYFlag *pause, CYNetLink *link=0, int idPrj=0);
  ~CYAcquisition();

  //  enum SeparatorType
  //  {
  //    /** Tabulation. */
  //    Tabulation = 1,
  //    /** Point virgule. */
  //    SemiColon = 2,
  //    /** Virgule. */
  //    Comma = 3,
  //    /** Espace. */
  //    Space = 4,
  //    /** Autres. */
  //    Others = 5,
  //  };

  /** @return la base de temps en ms. */
  uint timeBase() { return mTimeBase; }

  /** @return la  base de données locale à cet acquisition. */
  CYDB *db() { return mDB; }

  /** @return le nom du fichier de configuration d'acquisition. */
  QString setupFileName() { return mSetupFileName; }
  /** Saisie le nom du fichier de configuration d'acquisition. */
  void setSetupFileName(QString fileName) { mSetupFileName = fileName; }

  /** Ajoute une donnée à acquérir. */
  bool addData(CYData *data, QString label=0);
  /** Ajoute la donnée, ayant pour nom \a dataName, à acquérir. */
  bool addData(QString dataName, QString label=0);

  /** Ajoute une donnée à afficher dans l'en-tête du fichier d'acquisition. */
  bool addHeaderData(CYData *data, QString label=0);
  /** Ajoute la donnée à afficher dans l'en-tête du fichier d'acquisition, ayant pour nom \a dataName, à acquérir. */
  bool addHeaderData(QString dataName, QString label=0);

  /** @return l'étiquette de l'acquisition. */
  QString label() { return mLabel; }
  void addLine(QString *line, bool append=true);
  virtual uint fileNum();
  virtual void setFileNum(uint num);
  int postMortemFlags();
  CYAcquisitionBackupFlag *postMortemFlag(int index);

  /** Auorise au non l'enregistrement du paramètrage. Par défaut ceci est autorisé. */
  virtual void setSaveSetup(bool enable)
  {
    mSaveSetup = enable;
  }

  /** @return le nom du fichier courant. */
  QString fileName() { return mFileName; }

signals:
  /*! Emis à chaque fin d'enregistrement sur disque dur.*/
  void saved();

public slots: // Public slots
  /** Saisie l'étiquette de l'acquisition. */
  void setLabel(QString label) { mLabel = label; }

  /** Saisie la base de temps en ms. */
  void setTimeBase(uint base) { mTimeBase = base; }
  /** Enregistre sur disque dur. */
  virtual void save();
  /** Cherche le fichier courrant d'acquisition. */
  virtual void currentFile();
  /** Création d'un nouveau fichier d'acquisition. */
  void newFile();
  /** Création de l'en-tête du fichier d'acqusition. */
  virtual void header(QFile *file, QDateTime startTime, QList<CYAcquisitionData*> headerList, QList<CYAcquisitionData*> acquisitionList, int poste);


  /** Démarrer l'enregistrement de l'acquisition. */
  void startSaving();
  /** Arrêter l'enregistrement de l'acquisition. */
  void stopSaving();

  /** Charge la configuration d'acquisition. */
  virtual bool loadSetup();
  /** Charge la configuration d'acquisition à partir du fichier \a filename. */
  virtual bool loadSetup(const QString &filename);
  /** Charge le contenu du fichier de configuration d'acquisition. */
  virtual void loadSetup(QDomElement &el);
  /** Sauvegarde la configuration d'acquisition . */
  virtual bool saveSetup();
  /** Sauvegarde la configuration d'acquisition dans le fichier \a filename. */
  virtual bool saveSetup(const QString &filename);
  /** Sauvegarde le contenu de la configuration d'acquisition . */
  virtual void saveSetup(QDomDocument &doc, QDomElement &el);
  /** Saisie le chemin relatif dans le répertoire de l'essai courrant. */
  virtual void setNameDir(QString path);

  /** Saisie le type de séparateur. */
  void setSeparatorType(uint val);
  /** @return le type de séparateur. */
  //  CYAcquisition::SeparatorType separatorType() { return mSeparatorType; }
  uint separatorType() { return mSeparatorType; }

  /** @return la connexion. */
  CYNetLink *link() { return mLink; }
  /** Calcule les differentes informations utiles à la préparation du support des fichiers d'acquisition.
    * @param headerList      Liste des données à afficher dans l'en-tête du fichier d'acquisition.
    * @param acquisitionList  Liste des données à acquérir.
    * @param maxTime          Temps maximum de l'acquisition en secondes.
    * @param timer            Période d'acquisition.
    * @param nbLines          Nombre de lignes par fichier.
    * @param maxFiles         Nombre maximum de fichiers.
    * @param timer            Durée minimum d'acquisition utile en post-mortem. */
  void informations(QList<CYAcquisitionData*> headerList, QList<CYAcquisitionData*> acquisitionList, double maxTime, uint timer, uint nbLines, uint maxFiles, uint minTime);
  /** Donne l'ordre d'utiliser un nouveau fichier d'acquisition. */
  void useNewFile();

  /** Efface la configuration. */
  virtual void clearSetup();
  /** @return \a true si cette acquisition fonctionne en Post-Mortem. */
  bool postMortem();
  /** Saisir \a true si cette acquisition doit fonctionner en Post-Mortem. */
  void setPostMortem(bool mode);
  /** Ajoute un flag d'archivage en post-mortem.
    * @param prefix     Préfixe utilisé pour créer le nom du fichier d'archive.
    * @param header     En-tête utilisée pour le fichier d'archive.
    * @param enable     Evènement d'archivage.
    * @param overwrite  Archivage par écrasement des anciennes archive. */
  void addPostMortemFlags(CYAcquisitionBackupFlag *flag, QString prefix, QString header=0, int enable=1, bool overwrite=false);
  /** Archivage du post-mortem en mode continnu. */
  void postMortemBackupContinu();
  /** Archivage du post-mortem en un seul fichier suite à un évènement. */
  void postMortemBackup(CYAcquisitionBackupFlag *flag);
  /** Supprime les fichiers d'acquisition. */
  void removeFiles();
  /** Arrête l'acquisition. */
  virtual void stopAcquisition() {}
  /** Démarrre l'acquisition. */
  virtual bool startAcquisition() { return false; }

  /** @return \a true si l'acquisition a démarrée. */
  virtual bool started();

  /** @return \a true si l'acquisition est en service. */
  virtual bool isOn();
  /** @return le flag de mise en service de l'acquisition. */
  virtual CYFlag *isOnFlag();
  /** Saisie le flag de mise en service de l'acquisition. */
  virtual void setIsOnFlag(CYFlag *flag);
  /** Saisie le flag de mise en service de l'acquisition.
    * @param flagName Nom de ce flag. */
  virtual void setIsOnFlag(QString flagName);
  virtual void addLine(QString line, bool append=true);

protected: // Protected methods
  /** Traîtement de l'acquisition en fonctionnement. */
  virtual void runAcquisition() {}
  /** Enregistre sur disque dur. */
  virtual void save(bool ok);
  /** Initialisation de l'acquisition. */
  virtual void init();

public: // Public attributes
  /** Période d'acquisition. */
  CYTime *acqTimer;
  /** Période d'acquisition par rafales. */
  CYF32 *acqBurstsTimer;
  /** Durée d'acquisition. */
  CYTime *acqLengthTime;
  /** Durée de la fenêtre d'acquisition. */
  CYTime *acqWindowTime;
  /** Durée l'intervale entre 2 fenêtres d'acquisition. */
  CYTime *acqIntervalTime;
  /** Période d'enregistrement. */
  CYTime *saveTimer;
  /** Période d'archivage du post-mortem. */
  CYTime *postMortemBackupTimer;
  /** Flag d'archivage périodique du post-mortem. */
  CYAcquisitionBackupFlag *postMortemBackupTimerFlag;
  /** Enregistre les fichiers d'acquisition dans le répertoire d'essai si ce flag vaut \a true. */
  CYFlag *useTestDirectory;
  /** Nom du répertoire des fichiers d'acquisition dans le répertoire d'essai. */
  CYString *dirName;
  /** Chemin du répertoire des fichiers d'acquisition lorsque celui-ci ne se trouve pas dans le répertoire d'essai. */
  CYString *dirPath;
  /** Préfixe des noms de fichiers d'acquisition (@see mFileNum). */
  CYString *prefixFileName;
  /** Extension des noms de fichiers d'acquisition. */
  CYString *extensionFileName;
  /** Flag d'activation process de l'acquisition. */
  CYFlag *enableFlag;
  /** Flag de mise en pause de l'acquisition. */
  CYFlag *pauseFlag;
  /** Nombre de lignes par fichier d'acquisition. */
  CYU32 *maxLinesFile;
  /** Nombre maximum de fichiers d'acquisition. */
  CYS32 *maxNbFiles;
  /** Nombre de fichiers d'acquisition générés. */
  CYU32 *nbFiles;
  /** Temps d'acquisition à simuler. */
  CYTime *pretendTime;
  /** Taille maximum d'un fichier d'acquisition. */
  CYF64 *maxFileSize;
  /** Taille maximum de l'acquisition. */
  CYF64 *maxSize;
  /** Date et heure de début d'acquisition. */
  CYString *startDateTimeString;
  /** Séparateur de colonne du fichier d'acquisition. */
  CYString *separator;
  /** Séparateur de colonne du fichier d'acquisition autre que ceux proposés. */
  CYString *otherSeparator;
  /** Évènements d'archivage */
  CYFlag *backupEvent;
  /** Nom du fichier d'archive. */
  CYString *backupFileName;
  /** Nom du fichier d'archive d'acquisition continue par post-mortem. */
  CYString *backup2FileName;
  /** Erreur d'archive d'acquisition continue par post-mortem. */
  CYFlag *backup2Error;
  /** Format CSV compatible SoliaGraph */
  CYFlag *formatSoliaGraph;
	/** Archivage uniquement sur événement, sinon également en continue. */
	CYFlag *PMContinusBackup;
	/** Optimisation taille des fichiers CSV par champs vides en cas de valeur inchangée */
	CYFlag *fileSizeOptimization;

  /** Liste des données à acquérir. */
  QList<CYAcquisitionData*> datas;
  /** Liste des données à afficher dans l'en-tête du fichier d'acquisition. */
  QList<CYAcquisitionData*> headerDatas;

protected: // Protected attributes
  /** Base de données locale à cet acquisition. */
  CYDB *mDB;
  /** Nom du fichier de configuration d'acquisition. */
  QString mSetupFileName;
  /** Répertoire de stockage. */
  QDir *mDir;
  /** Répertoire oÃ¹ sont enregistrées les 2 fichiers d'acquisition permettant l'archive du post-mortem. */
  QDir *mPostMortmemDir;

  /** Temps total écoulé depuis le début de l'acquisition. */
  double mTotalTime;
  /** Temps écoulé depuis le début de la pause d'acquisition. */
  double mPauseTime;
  /** Temps écoulé dans la fenêtre d'acquisition. */
  double mWindowTime;
  /** Temps écoulé depuis la dernière fenêtre d'acquisition. */
  double mIntervalTime;
  /** Base de temps en ms. */
  uint mTimeBase;

  /** Nom du fichier courant.
    * Le nom du fichier courant est constitué de ce numéro précédé du
    * préfixe du nom de fichier (@see prefixFileName). */
  QString mFileName;
  /** Numéro du fichier courant.
    * Le nom du fichier courant est constitué de ce numéro précédé du
    * préfixe du nom de fichier (@see prefixFileName). */
  uint mFileNum;
  /** Nombre de lignes du fichier courrant. */
  uint mNbLinesFile;
  /** Type de séparateur. */
  uint mSeparatorType;

  /** Vaut \a true si cette acquisition fonctionne en Post-Mortem. */
  bool mPostMortem;

  /** Flag de mise en service de l'acquisition. */
  CYFlag *mIsOnFlag;
  /** Acquisition démarrée. */
  CYFlag *mStarted;
  /** Acquisition en pause. */
  CYFlag *mPausing;
  /** Fenêtre d'acquisition en cours. */
  CYFlag *mWindow;
  /** Temps du début de l'acquisition. */
  QDateTime mStartTime;

  /** 2 buffers sont utilisés pour l'acquisition. Lorsque l'un s'occupe de buffériser
    * les nouvelles valeurs l'autre est disponible pour un enregistrement sur disque. */
  int mCurrentBuffer;
  /** Buffer 1 d'acquisition. */
  QList<QString*> mBuffer1;
  /** Buffer 2 d'acquisition. */
  QList<QString*> mBuffer2;

  /** Timer d'enregistrement. */
  QTimer *mSaverTimer;
  /** Timer d'archivage du post-mortem. */
  QTimer *mPostMortemBackupTimer;
  /** Indique si un enregistrement est en cours. */
  CYFlag *mSaving;
  /** Connexion. */
  CYNetLink *mLink;

  /** Indique s'il faut utiliser un nouveau fichier d'acquisition. */
  bool mUseNewFile;


  /** Liste des flags d'archivage en post-mortem. */
  QList<CYAcquisitionBackupFlag*> mPostMortemFlags;
  /** Fichier d'acquisition. */
  QFile *mFile;
  /** Nombre de caractères maximum pour afficher le temps. */
  uint mMaxNbCharTime;
  /** Indique que l'acquisition travaille. */
  bool mRunning;
  /** Etiquette de l'acquisition. */
  QString mLabel;
  /** Pas de contrôle de nombre de lignes par fichier d'acquisition. */
  bool mNoMaxLinesFile;
  bool mSaveSetup;

  int mPoste;
};

#endif
