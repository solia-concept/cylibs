/***************************************************************************
                          cyacquisitionwin.cpp  -  description
                             -------------------
    begin                : jeu mai 6 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisitionwin.h"

// CYLIBS
#include "cy.h"
#include "cycore.h"
#include "cyacquisitionbrowser.h"
#include "cyaquisitionwidget.h"

CYAcquisitionWin::CYAcquisitionWin(QWidget *parent, const QString &name, int index, const QString &label)
: CYWin(parent,name,index,label)
{
  setWindowTitle(tr("CYLIX: Acquisitions tool"));
  mSplitter = new QSplitter(this);
  Q_CHECK_PTR(mSplitter);
  mSplitter->setOrientation(Qt::Horizontal);
  mSplitter->setOpaqueResize(true);
  setCentralWidget(mSplitter);

  mBrowser = new CYAcquisitionBrowser(mSplitter, "CYAcquisitionBrowser");
  Q_CHECK_PTR(mBrowser);

  mWidget = new CYAquisitionWidget(mSplitter, "CYAquisitionWidget");
  Q_CHECK_PTR(mWidget);
}

CYAcquisitionWin::~CYAcquisitionWin()
{
}

