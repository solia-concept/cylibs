/***************************************************************************
                          cyacquisitiondataview.h  -  description
                             -------------------
    begin                : ven avr 30 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITIONDATAVIEW_H
#define CYACQUISITIONDATAVIEW_H

// QT
#include <QList>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QTreeWidget>
// CYLIBS
#include "cywidget.h"

class CYAcquisitionData;
class CYDatasListView;

namespace Ui {
    class CYAcquisitionDataView;
}

/** @short Visualisation des données acquises.
  * @author LE CLÉACH Gérald
  */

class CYAcquisitionDataView : public CYWidget
{
   Q_OBJECT
public:
  CYAcquisitionDataView(QWidget *parent=0, const QString &name=0);
  ~CYAcquisitionDataView();
  /** Ajoute une donnée d'acquisition a partir d'un nom de donnée. */
  void addData(QString dataName);
  /** Ajoute une donnée d'acquisition. */
  void addData(CYAcquisitionData *data);
  /** Ajoute un groupe de données d'acquisition a partir d'un nom de groupe de données. */
  void addGroup(QString groupName);

  CYDatasListView *list();

protected: // Protected methods

public slots: // Public slots
  /** Supprime la donnée sélectionnée de la liste. */
  void remove();
  /** Rafraîchit la vue. */
  void refresh();
  /** Effacer la liste de données. */
  void clear();
  /** Edition de l'étiquette de la donnée sélectionnée. */
  void edit();

public: // Public attributes
  /** Liste des données. */
  QList<CYAcquisitionData*> datas;


private:
    Ui::CYAcquisitionDataView *ui;
    CYDatasListView *mList;
};

#endif
