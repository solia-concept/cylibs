/***************************************************************************
                          cyacquisitionview.h  -  description
                             -------------------
    begin                : jeu mai 6 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITIONVIEW_H
#define CYACQUISITIONVIEW_H

// QT
#include <qwidget.h>

/** @short Widget de visualisation d'acquisition.
  * @author LE CLÉACH Gérald
  */

class CYAcquisitionView : public QWidget
{
   Q_OBJECT
public:
  CYAcquisitionView(QWidget *parent=0, const QString &name=0);
  ~CYAcquisitionView();
};

#endif
