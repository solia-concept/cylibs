/***************************************************************************
                          cyacquisitionfast.h  -  description
                             -------------------
    begin                : jeu avr 29 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITIONFAST_H
#define CYACQUISITIONFAST_H

// CYLIBS
#include "cyacquisition.h"

class CYU8;

/** @short Acquisition rapide.
  * @author LE CLÉACH Gérald
  */

class CYAcquisitionFast : public CYAcquisition
{
   Q_OBJECT
public:

  /** Constructeur d'acquisition rapide sans gestion de mise en pause.
    * @param parent       Parent.
    * @param name         Nom.
    * @param label        Etiquette.
    * @param enable       Flag d'activation.
    * @param link         Connexion réseau.
    * @param sampleSize   Taille d'un échantillon.
    * @param sampleTimer  Durée d'un échantillon. */
  CYAcquisitionFast(QObject *parent, const QString &name, QString label, CYFlag *enable, CYNetLink *link, int sampleSize, float sampleTimer);
  /** Constructeur d'acquisition rapide avec gestion de mise en pause.
    * @param parent       Parent.
    * @param name         Nom.
    * @param label        Etiquette.
    * @param enable       Flag d'activation.
    * @param pause        Flag de mise en pause.
    * @param link         Connexion réseau.
    * @param sampleSize   Taille d'un échantillon.
    * @param sampleTimer  Durée d'un échantillon. */
  CYAcquisitionFast(QObject *parent, const QString &name, QString label, CYFlag *enable, CYFlag *pause, CYNetLink *link, int sampleSize, float sampleTimer);

  ~CYAcquisitionFast();
  
  /** Ajoute dans le buffer les valeurs de la structure \a str dans le tableau de structures
    * en fonction de la période d'acquisition. */
  virtual void buffering(int str);

  /** @short Mode d'échantillonnage. */
  enum SamplingMode
  {
    /** En continu. */
    Continuous = 0,
    /** Par rafales. */
    Bursts
  };

  /** Initialise les données utiles pour fonctionner par rafales
    * @param flag     Nom du flag d'acquisition par rafales.
    * @param enable   Nom du flag de gestion par rafales. Prise en compte ou non du flag d'acquisition.
    * @param ratio    Rapport période d'acquisition rapide / période d'acquisition par rafales. */
  void initBurstDatas(QByteArray flag, QByteArray enable, short ratio);

  /** Initialise les données utiles pour fonctionner par rafales
    * @param flag     Flag d'acquisition par rafales.
    * @param enable   Gestion par rafales. Prise en compte ou non du flag d'acquisition.
    * @param ratio    Rapport période d'acquisition rapide / période d'acquisition par rafales. */
  void initBurstDatas(CYFlag *flag, CYFlag *enable, short ratio);

public slots: // Public slots
  /** Démarre l'acquisition. */
  virtual bool startAcquisition();
  /** Arrête l'acquisition. */
  virtual void stopAcquisition();

protected: // Protected methods
  /** Traîtement de l'acquisition en fonctionnement. */
  virtual void runAcquisition();

public: // Public methods
  /** Flag d'acquisition par rafales. */
  CYFlag * burstFlag;
  /** Gestion par rafales. */
  CYFlag * burstEnable;

private: // Private methods
  /** Ajoute une ligne d'acquisition.
    * @param time Temps de l'échantillon en ms.
    * @param sample Echantillon à prendre en compte. */
  void addLine(double time, int sample);

private: // Private methods
  /** Initialisation de l'acquisition. */
  void init();

protected: // Protected attributes
  /** Taille d'un échantillon. */
  int mSampleSize;
  /** Période d'échantillonnage en ms. */
  float mSampleTimer;
  /** Compteur d'échantillons. Ce compteur est incrémenté à chaque fois
    * qu'un échantillon est traîté. Lorsque ce compteur multiplié par
    * la période d'échantillonnage donne une valeur au moins égale à la 
    * période d'acquisition, celui-ci est remis à zéro et l'échantillon traîté est 
    * bufférisé. */
  int mCptSamples;
  /** Index de la structure ne cours dans le tableau de structures. */
  int mStr;

  /** Rapport période d'acquisition rapide / période d'acquisition par rafales. */
  uint mBurstsRatio;
};

#endif
