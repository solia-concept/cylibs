/***************************************************************************
                          cyacquisitiondata.h  -  description
                             -------------------
    begin                : mar mai 11 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITIONDATA_H
#define CYACQUISITIONDATA_H

// QT
#include <qobject.h>

class CYData;

/** @short Donnée d'acquisition.
  * @author LE CLÉACH Gérald
  */

class CYAcquisitionData : public QObject
{
   Q_OBJECT
public:
  CYAcquisitionData(CYData *data, QObject *parent, const QString &name, QString label);
  ~CYAcquisitionData();

  /** @return le nom de la donnée en dans un tableau d'octet QByteArray suivant l'encodage UTF-8. */
  QByteArray dataName();

  /** @return l'échantillon d'index \a i dans la structure \a str.
    * Uniquement pour les donnée rapides. */
  double sample(int str, int i);
  /** @return la valeur de la donnée.
    * Fonction nécessaire pour les données lentes. */
  double val();
  /** @return l'étiquette. */
  QString label();
  /** @return la donnée acquise. */
  CYData *data();

	/**
	 * @brief fieldCSV Affecte un nouveau champ texte pour fichier d'acquisition CSV.
	 * Permet d'optimiser la taille des fichiers CSV d'acquisition en utilisant des champs vides tant que la valeur reste inchangée.
	 * @param optimized Activation de l'optimisation de taille des fichiers CSV.
	 * @param fast      Activation pour acquisition rapide.
	 * @param str				Structure réseau de la donnée rapide.
	 * @param sample		Index de l'échantillon de la donnée rapide
	 * @return le texte du champ CSV.
	 */
	QString fieldCSV(bool optimized, bool fast=false, int str=-1, int i=-1);

protected: // Protected attributes
  /** Donnée acquises. */
  CYData *mData;
  /** Etiquette. */
  QString mLabel;
	/** Champ CSV. */
	QString mFieldCSV;
};

#endif
