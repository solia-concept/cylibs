/***************************************************************************
                          cyacquisitionbrowser.h  -  description
                             -------------------
    begin                : jeu mai 6 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITIONBROWSER_H
#define CYACQUISITIONBROWSER_H

// QT
#include <QWidget>

/** @short Explorateur d'acquisitions.
  * KDirOperator permettant de sélectionner les fichiers de l'acquisition ou
  * son fichier de configuration.
  * En fonction du type de fichier selectionné le CYAcquisitionWidget affiche
  * soit une vue des valeurs acquises soit une vue de configuration de l'acquisition.
  * @author LE CLÉACH Gérald
  */

class CYAcquisitionBrowser : public QWidget
{
  Q_OBJECT
public:
  CYAcquisitionBrowser(QWidget *parent=0, const QString &name=0);
  ~CYAcquisitionBrowser();
};

#endif
