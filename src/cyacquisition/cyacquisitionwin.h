/***************************************************************************
                          cyacquisitionwin.h  -  description
                             -------------------
    begin                : jeu mai 6 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITIONWIN_H
#define CYACQUISITIONWIN_H

// QT
#include <qsplitter.h>
// CYLIBS
#include <cywin.h>

class CYAcquisitionBrowser;
class CYAquisitionWidget;

/** @short Fenêtre d'acquisitions.
  * Dans cette fenêtre il est possible de paramètrer et visualiser les
  * différentes acquisitions (acquisitions sur demandes, post-mortem...).
  * @author LE CLÉACH Gérald
  */

class CYAcquisitionWin : public CYWin
{
  Q_OBJECT
public:
  CYAcquisitionWin(QWidget *parent, const QString &name, int index, const QString &label);
  ~CYAcquisitionWin();

protected: // Protected attributes
  /** Splitter entre le CYAcquisitionBrowser et le CYAcquisitionWidget. */
  QSplitter *mSplitter;
  /** Explorateur d'acquisitions. */
  CYAcquisitionBrowser *mBrowser;
  /** Widget d'acquisition. */
  CYAquisitionWidget *mWidget;
};

#endif
