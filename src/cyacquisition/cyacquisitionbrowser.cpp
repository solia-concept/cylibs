/***************************************************************************
                          cyacquisitionbrowser.cpp  -  description
                             -------------------
    begin                : jeu mai 6 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisitionbrowser.h"

// CYLIBS
#include "cycore.h"
#include "cyacquisition.h"
#include "cystring.h"

CYAcquisitionBrowser::CYAcquisitionBrowser(QWidget *parent, const QString &name)
  : QWidget( parent)
//: KDirOperator( core->baseDirectory(), parent,name)
{
  setObjectName(name);
//  setView(KFile::PreviewContents);
}

CYAcquisitionBrowser::~CYAcquisitionBrowser()
{
}
