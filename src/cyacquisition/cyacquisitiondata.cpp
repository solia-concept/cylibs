/***************************************************************************
                          cyacquisitiondata.cpp  -  description
                             -------------------
    begin                : mar mai 11 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisitiondata.h"

// CYLIBS
#include "cys8.h"
#include "cys16.h"
#include "cys32.h"
#include "cys64.h"
#include "cyu8.h"
#include "cyu16.h"
#include "cyu32.h"
#include "cyu64.h"
#include "cyf32.h"
#include "cyf64.h"
#include "cybool.h"
#include "cyflag.h"
#include "cytime.h"
#include "cytsec.h"

CYAcquisitionData::CYAcquisitionData(CYData *data, QObject *parent, const QString &name, QString label)
: QObject(parent),
  mData(data),
  mLabel(label)
{
  setObjectName(name);
}

CYAcquisitionData::~CYAcquisitionData()
{
}

QByteArray CYAcquisitionData::dataName()
{
  return objectName().toUtf8();
}

QString CYAcquisitionData::fieldCSV(bool optimized, bool fast, int str, int i)
{
	QString txt;
	if (fast && data()->isReadFast())
		txt = QString("%1").arg(sample(str, i), 0, 'f', data()->nbDec());
	else
		txt = QString("%1").arg(val(), 0, 'f', data()->nbDec());
	if (optimized && txt == mFieldCSV)
	{
		mFieldCSV = txt;
		return "";
	}
	else
	{
		mFieldCSV = txt;
		return txt;
	}
}

double CYAcquisitionData::sample(int str, int i)
{
  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      return data->sample(str, i);
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      return data->sample(str, i);
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      return data->sample(str, i);
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      return data->sample(str, i);
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      return 0.0;
  }
  return 0.0;
}

double CYAcquisitionData::val()
{
  switch (mData->type())
  {
    case Cy::VFL    :
                    {
                      CYFlag *data = (CYFlag *)mData;
                      return data->val();
                    }
    case Cy::VS8    :
                    {
                      CYS8 *data = (CYS8 *)mData;
                      return data->val();
                    }
    case Cy::VS16   :
                    {
                      CYS16 *data = (CYS16 *)mData;
                      return data->val();
                    }
    case Cy::VS32   :
                    {
                      CYS32 *data = (CYS32 *)mData;
                      return data->val();
                    }
    case Cy::VS64   :
                    {
                      CYS64 *data = (CYS64 *)mData;
                      return data->val();
                    }
    case Cy::VU8    :
                    {
                      CYU8  *data = (CYU8  *)mData;
                      return data->val();
                    }
    case Cy::VU16   :
                    {
                      CYU16 *data = (CYU16 *)mData;
                      return data->val();
                    }
    case Cy::VU32   :
                    {
                      CYU32 *data = (CYU32 *)mData;
                      return data->val();
                    }
    case Cy::VU64   :
                    {
                      CYU64 *data = (CYU64 *)mData;
                      return data->val();
                    }
    case Cy::VF32   :
                    {
                      CYF32 *data = (CYF32 *)mData;
                      return data->val();
                    }
    case Cy::VF64   :
                    {
                      CYF64 *data = (CYF64 *)mData;
                      return data->val();
                    }
    case Cy::Time   :
                    {
                      CYTime *data = (CYTime *)mData;
                      return data->val();
                    }
    case Cy::Sec    :
                    {
                      CYTSec *data = (CYTSec *)mData;
                      return data->val();
                    }
    default         : CYWARNINGTEXT(objectName()+", "+parent()->objectName()+", "+mData->objectName());
                      return 0.0;
  }
  return 0.0;
}

QString CYAcquisitionData::label()
{
  return mLabel;
}

CYData *CYAcquisitionData::data()
{
  return mData;
}
