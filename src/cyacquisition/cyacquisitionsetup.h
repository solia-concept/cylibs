/***************************************************************************
                          cyacquisitionsetup.h  -  description
                             -------------------
    begin                : ven avr 30 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYACQUISITIONSETUP_H
#define CYACQUISITIONSETUP_H

// CYLIBS
#include <cywidget.h>
//Added by qt3to4:
#include <QString>
#include <QShowEvent>

class CYAcquisition;

namespace Ui {
    class CYAcquisitionSetup;
}

/** @short Widget de configuration d'une acquisition.
  * @author LE CLÉACH Gérald
  */

class CYAcquisitionSetup : public CYWidget
{
  Q_OBJECT
  Q_PROPERTY(QByteArray acquisitionName READ acquisitionName WRITE setAcquisitionName)

public:
  CYAcquisitionSetup(QWidget *parent=0, const QString &name=0);
  ~CYAcquisitionSetup();

  /** @return l'acquisition configuré par cet objet. */
  virtual CYAcquisition *acquisition() { return mAcquisition; }
  /** Saisie l'acquisition configuré par cet objet. */
  virtual void setAcquisition(CYAcquisition *acquisition);

  /** @return le nom de l'acquisition configuré par cet objet. */
  virtual QByteArray acquisitionName() const { return mAcquisitionName; }
  /** Saisie le nom de l'acquisition configuré par cet objet. */
  virtual void setAcquisitionName(const QByteArray &name) { mAcquisition=0; mAcquisitionName = name; }
  /** Initialise suivant l'acquisition. */
  virtual void initAcquisition();

public slots: // Public slots
  /** Applique les données. */
  void apply();
  /** Calcule les differentes informations utiles à la préparation du support des fichiers d'acquisition. */
  void informations();
  /** Charge les valeurs par défaut des données. */
  void designer();
  /** Désactive le widget. */
  virtual void setEnabled(bool);

protected: // Protected methods
  /** Fonction appelée à l'affichage du widget. */
  virtual void showEvent(QShowEvent *e);

protected: // Protected attributes
  /** Acquisition configuré par cet objet. */
  CYAcquisition *mAcquisition;
  /**Nom de l'acquisition configuré par cet objet. */
  QByteArray mAcquisitionName;
protected slots:
    virtual void setDirPath();

private:
    Ui::CYAcquisitionSetup *ui;
};

#endif
