/***************************************************************************
                          cyacquisitionview.cpp  -  description
                             -------------------
    begin                : jeu mai 6 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cyacquisitionview.h"

CYAcquisitionView::CYAcquisitionView(QWidget *parent, const QString &name)
: QWidget(parent)
{
  setObjectName(name);
}

CYAcquisitionView::~CYAcquisitionView()
{
}
