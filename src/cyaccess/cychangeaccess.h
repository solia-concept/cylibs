/***************************************************************************
                          cychangeaccess.h  -  description
                             -------------------
    begin                : jeu mar 25 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CYCHANGEACCESS_H
#define CYCHANGEACCESS_H

// CYLIBS
#include "cydialog.h"

namespace Ui {
    class CYChangeAccess;
}

/** @short Boîte de changement d'accès.
  * @author Gérald LE CLEACH
  */

class CYChangeAccess : public CYDialog
{
  Q_OBJECT
public:
  CYChangeAccess(QWidget *parent=0, const QString &name=0);
  ~CYChangeAccess();

protected slots: // Protected slots
  /** Applique les modifications et ferme la boîte de dialogue. */
  void accept();

private:
    Ui::CYChangeAccess *ui;
};

#endif
