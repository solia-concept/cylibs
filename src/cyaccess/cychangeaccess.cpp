/***************************************************************************
                          cychangeaccess.cpp  -  description
                             -------------------
    begin                : jeu mar 25 2004
    copyright            : (C) 2004 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cychangeaccess.h"
#include "ui_cychangeaccess.h"

// QT
#include <QComboBox>
// CYLIBS
#include "cycore.h"

CYChangeAccess::CYChangeAccess(QWidget *parent, const QString &name)
  : CYDialog(parent,name), ui(new Ui::CYChangeAccess)
{
  ui->setupUi(this);
  ui->listUsers->addItems(core->userList());
}

CYChangeAccess::~CYChangeAccess()
{
  delete ui;
}

void CYChangeAccess::accept()
{
  if (!core->changeUserAccess(ui->listUsers->currentText()))
  {
    ui->listUsers->clear();
    ui->listUsers->addItems(core->userList());
    return;
  }
  CYDialog::accept();
}
