################################################################
# Cy Widget Library
# Copyright (C) 2015   Gérald Le Cléach
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the Qwt License, Version 1.0
################################################################

# qmake project file for building the cy libraries

CY_ROOT = $${PWD}/..
include( $${CY_ROOT}/cyconfig.pri )
include( $${CY_ROOT}/cybuild.pri )
include( $${CY_ROOT}/cyfunctions.pri )

CY_OUT_ROOT = $${OUT_PWD}/..

TEMPLATE          = lib
TARGET            = $$cyLibraryTarget(cy)

DESTDIR           = $${CY_OUT_ROOT}/lib

contains(CY_CONFIG, CyDll) {

    CONFIG += dll
    windows|symbian: DEFINES += QT_DLL CY_DLL CY_MAKEDLL
}
else {
    CONFIG += staticlib
}

contains(CY_CONFIG, CyFramework) {

    CONFIG += lib_bundle
}

include ( $${PWD}/src.pri )

# Install directives

target.path    = $${CY_INSTALL_LIBS}
INSTALLS       = target

#-----------------------------------------------------------------

CONFIG(lib_bundle) {

    FRAMEWORK_HEADERS.version = Versions
    FRAMEWORK_HEADERS.files = $${HEADERS}
    FRAMEWORK_HEADERS.path = Headers
    QMAKE_BUNDLE_DATA += FRAMEWORK_HEADERS
}
else {
CONFIG(debug, release|debug) {
    headers.files  = $${HEADERS}
    headers.path   = $${CY_INSTALL_HEADERS}
    INSTALLS += headers
    }
}

contains(CY_CONFIG, CyPkgConfig) {

    CONFIG     += create_pc create_prl no_install_prl

    QMAKE_PKGCONFIG_NAME = Cy$${CY_VERSION}
    QMAKE_PKGCONFIG_DESCRIPTION = Qt Widgets for Technical Applications

    QMAKE_PKGCONFIG_LIBDIR = $${CY_INSTALL_LIBS}
    QMAKE_PKGCONFIG_INCDIR = $${CY_INSTALL_HEADERS}

    QMAKE_PKGCONFIG_DESTDIR = pkgconfig

    greaterThan(QT_MAJOR_VERSION, 4) {

        QMAKE_PKGCONFIG_FILE = Qt$${QT_MAJOR_VERSION}$${QMAKE_PKGCONFIG_NAME}
        QMAKE_PKGCONFIG_REQUIRES = Qt5Widgets Qt5Concurrent Qt5PrintSupport

        contains(CY_CONFIG, CySvg) {
            QMAKE_PKGCONFIG_REQUIRES += Qt5Svg
        }

        contains(CY_CONFIG, CyOpenGL) {
            QMAKE_PKGCONFIG_REQUIRES += Qt5OpenGL
        }

        QMAKE_DISTCLEAN += $${DESTDIR}/$${QMAKE_PKGCONFIG_DESTDIR}/$${QMAKE_PKGCONFIG_FILE}.pc
    }
    else {

        # there is no QMAKE_PKGCONFIG_FILE fo Qt4
        QMAKE_PKGCONFIG_REQUIRES = QtGui

        contains(CY_CONFIG, CySvg) {
            QMAKE_PKGCONFIG_REQUIRES += QtSvg
        }

        contains(CY_CONFIG, CyOpenGL) {
            QMAKE_PKGCONFIG_REQUIRES += QtOpenGL
        }

        QMAKE_DISTCLEAN += $${DESTDIR}/$${QMAKE_PKGCONFIG_DESTDIR}/$${TARGET}.pc
    }

    QMAKE_DISTCLEAN += $${DESTDIR}/libcy.prl
}
RESOURCES += \
             $${CY_ROOT}/designer/cy_designer_plugin.qrc

