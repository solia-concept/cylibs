#ifndef CYLIBS_GLOBAL_H
#define CYLIBS_GLOBAL_H

#include <QtCore/qglobal.h>

#define CY_DEBUG_SAVE_CURVES 0

#if defined(_MSC_VER) /* MSVC Compiler */
/* template-class specialization 'identifier' is already instantiated */
#pragma warning(disable: 4660)
/* inherits via dominance */
#pragma warning(disable: 4250)
#endif // _MSC_VER

#ifdef CY_DLL

#if defined(CY_MAKEDLL)     // create a Cy DLL library
#define CY_EXPORT Q_DECL_EXPORT
#else                        // use a Cy DLL library
#define CY_EXPORT Q_DECL_IMPORT
#endif

#endif // QWT_DLL


#ifndef CY_EXPORT
#define CY_EXPORT
#endif

#endif // CYLIBS_GLOBAL_H
