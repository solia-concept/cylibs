#include "core.h"
#include "cymainwin.h"
#include "cydblibs.h"
#include "cyacquisitionslow.h"
#include "cyacquisitionfast.h"
#include "cyacquisitionbackupflag.h"
#include "cynetmodule.h"
#include "cynethost.h"
#include "cynetlink.h"
#include "cydb.h"
#include "cytime.h"
#include "cytsec.h"
#include "cystring.h"
#include "cypidp.h"
#include "cypidramp.h"
#include "cys16.h"
#include "cyu16.h"
#include "cys32.h"
#include "cyf64.h"
#include "cyai.h"
#include "cyao.h"
#include "cytime.h"
#include "cyword.h"
#include "cycs.h"
#include "cydatetime.h"
#include "cyapplication.h"
#include "cyaboutdata.h"

#include "QDesktopServices"

#define RNR_BTMS               10 // base de temps pour courbe rapide en ms
#define RNR_BUFFER_SIZE         5 // nombre de mesure par structure (FIFO)

// SLOW_XXX = Taille du Buffer d'acquisition,Péiode d'acquisition en ms (pas du buffer)
#define SLOW_10MIN     6000 ,  100      // Buffer 10min,   0.1 sec    =>       10(min)*60(sec)* 1000(ms)/  100(ms)
// #define SLOW_30MIN     3600,   500      // Buffer 30min,   5   sec    =>       30(min)*60(sec)* 1000(ms)/  500(ms)
#define SLOW_30MIN     1000,   100      // Buffer 30min,   5   sec    =>       30(min)*60(sec)* 1000(ms)/  500(ms)
#define SLOW_2H        7200 , 1000      // Buffer  2h  ,   1   sec    =>  2(h)*60(min)*60(sec)* 1000(ms)/ 1000(ms)
#define SLOW_5H        3600 , 5000      // Buffer  5h  ,   5   sec    =>  5(h)*60(min)*60(sec)* 1000(ms)/ 5000(ms)
#define SLOW_24H       8640 ,10000      // Buffer  24h ,  10   sec    => 24(h)*60(min)*60(sec)* 1000(ms)/10000(ms)

// FAST_XXX = Taille du Buffer d'acquisition,Péiode d'acquisition en ms (pas du buffer),Taille de l'éhantillon d'acquisition.
#define FAST_1MIN     6000 , RNR_BTMS, RNR_BUFFER_SIZE   // Buffer 1 min a 100Hz (10ms)

#define FMT_DEG     1
#define FMT_TRIGO   2
#define FMT_EXP_DEG 3
#define FMTE_FUITE  4
#define FMT_BAR     5
#define FMT_P_C     6

Core::Core(QObject *parent, QString name)
  : CYCore(parent, name)
{
  setOffsetFontSize(-3);
  mBenchType.append("_APM"); // si on veut utiliser les Acquisitions Post-Mortem
//mBenchType.append("_ACO"); // si on veut utiliser les Acquisitions COntinues
}

void Core::init()
{
  CYCore::init();

  QElapsedTimer timer;
  //   timer.start(5000, true); // temps maxi de blocage en ms
  timer.start(); // temps maxi de blocage en ms

  bool end =false;
  while (!end)
  {
    if (timer.elapsed()>2000)
    {
      end = true;
    }
  }
  //   new CYSMTP("stpclima02@behrgroup.com", "bereitschaft.pruefstand@behrgroup.com", subject, body);
  //   new CYSMTP("mh1.st.behr.de", 25, "Oel-IDW6@behrgroup.com", "bereitschaft.pruefstand@behrgroup.com", subject, body);


  /*  new CYSMTP("cy.quiperchine@wanadoo.fr", "g.lecleach@yahoo.fr", subject, body);*/
  //   new CYSMTP("cy.quiperchine@wanadoo.fr", "g.lecleach1@free.fr", subject, body);

  setMaxCurvesByScope(32);
  module = new CYNetModule(this, "MODULE");
  Q_CHECK_PTR(module);
  setDesignerRef("CYLIBS");

  // HÔTE(S)
  mSPV = new CYNetHost(this, QString(tr("SUPERVISOR")), 0, tr("Supervisor"));
  Q_CHECK_PTR(mSPV);

  mRN  = new CYNetHost(this, QString(tr("Machine")), 1, tr("Regulator"));
  Q_CHECK_PTR(mRN);

  initBaseDirectory();

  // CONNEXIONS RESEAUX
  mLocalLink = new CYNetLink(this, tr("Local datas"), module, mSPV);
  Q_CHECK_PTR(mLocalLink);
  mSPV->addLink(mLocalLink);
  addLink(mLocalLink);

  mSlowLink1 = new CYNetLink(this, tr("Non-synchronous connection"), module, mRN);
  Q_CHECK_PTR(mSlowLink1);
  mSlowLink1->setCOM(0, 0);
  //   mSPV->addLink(mSlowLink1);
  mRN->addLink(mSlowLink1);
  addLink(mSlowLink1);

  mFastLink1 = new CYNetLink(this, tr("Synchronous connection"), module, mRN);
  Q_CHECK_PTR(mFastLink1);
  mFastLink1->setCOM(0, 0);
  mFastLink1->setBTMS(RNR_BTMS);
  mFastLink1->setObjectName( QString("%1 %2").arg(mFastLink1->objectName()).arg(mFastLink1->frequency()) );
  mRN->addLink(mFastLink1);
  core->addLink(mFastLink1);

  addHost(mSPV);
  addHost(mRN);

  initFormat();
  core->initDB();

  DBSPV = new CYDB(mSPV, "DBSPV", 0, 1000, true);
  Q_CHECK_PTR(DBSPV);
  DBSPV->setFileName(core->appDataDir()+"/DBSPV.cydata");
  mLocalLink->addDB(DBSPV);

  DBRN = new CYDB(mRN, "DBRN", 0, 200, true);
  Q_CHECK_PTR(DBRN);
  DBRN->setFileName(core->appDataDir()+"/DBRN.cydata");
  mSlowLink1->addDB(DBRN);

  DBRNR = new CYDB(mRN, "DBRNR", 0, 200, true);
  Q_CHECK_PTR(DBRNR);
  DBRNR->setFileName(core->appDataDir()+"/DBRNR.cydata");
  mFastLink1->addDB(DBRNR);

  //   core->setEventsPanel(new EventsPanel(0, "EventsPanel"));

  // TEST TEMPO
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, Cy::HOUR_MIN_SEC, 1000, Cy::Sys);
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "4s0ms", "0s500ms", "20s0ms");
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "4m0s", "0m50s", "20m0s", BASE_MSEC);
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "20 min", "1 min", "300 min");
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "0h0m0s", "0h0m0s", "100h0m0s", BASE_MSEC);
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "2h51m43s", "1h5m2s", "300h10m50s", BASE_MSEC, Cy::Sys);
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "2h", "1h", "100h", BASE_MSEC, Cy::Sys);
  //  CYTSec *time = new CYTSec(DBRN, "DATA_TIME", new u32, 0.2, 0.1, 5.0);
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "15 min", "1 min", "99 min");
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "15 hour", "1 hour", "99 hour");
  //  CYTSec *time = new CYTSec(DBRN, "DATA_TIME", new u32, 10.0, 5.0, 300.0);
    CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "2m3s", "1m1s", "54m0s");
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "2000h0m0s", "0h0m0s", "2000h0m0s", BASE_SEC);
  //  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "200ms", "40ms", "3600000ms");
  //   CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "1s0ms", "0s200ms", "1000s0ms");
  //   CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "0h30m", "0h01m", "10h20m");
  //   CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "0h30m00s", "0h00m01s", "10h00m00s");
  //   CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "0h30m00s005ms", "0h00m01s000ms", "10h00m00s000ms");
  //   CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "0h00m02s500ms", "0h0m00s200ms", "500h00m00s000ms", 5);
  //   CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "1h0m0s", "0h0m0s", "500h0m0s", 1000.0);
  //   CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "2h0m5s", "1h0m0s", "2h10m0s", 1000.0);
  //   CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "2h0m0s", "1h0m0s", "2h10m0s", 1000.0);
//    CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "1h0m5s", "0h0m0s", "10h10m0s", 1.0);
//  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "1h0m5s0ms", "0h0m0s0ms", "10h10m0s0ms", 1.0);
//  CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "0m5s0ms", "0m0s0ms", "10m0s0ms", 1.0);
  //   CYTime *time = new CYTime(DBRN, "DATA_TIME", new u32, "10 sec", "1 sec", "100 sec", 100.0);
  time->setLabel(QString("Save acquisition file timer"));
  Q_CHECK_PTR(time);
  //  time->setHelp("sdok");
  //  time->addNote("sdsqdjml;sddddddddddddddddddmmlmfsd");
  // TEST STRING
  //  char **val = new char *;
  //  QString msg = QString("Rééence");500.0500.0
  //  char *txt = (char*)((const char*)msg);
  //  char *txt = (char*)((const char*)(QString("Rééence")));
  //  char *txt = (char*)((const char*)(QString("Rééence")));
  //  char *txt = "Rééence";
  //  *val = txt;
  CYString *data = new CYString(DBRN, "CYString", new QString("Référence"));
  data->setGroup(QString("Info"));
  Q_CHECK_PTR(data);


#define ICS_MST              0  // compteur horaire sous tension
#define ICS_MES              1  // compteur horaire en service
#define ICS_TEST             2  // compteur horaire en test (au moins un poste)
#define NB_CS_G              3

//  QString nomG=tr("Bench");
//  CYCS::Attr1 attr[]=
//  {
//    // ------ NOM --------|------- INDEX -------|- MODE -|Groupe|------ ETIQUETTE ---------|
//    {"CS_MST"             , ICS_MST             , Cy::Sys, nomG , TRFR("Banc sous-tension")                   },
//    {"CS_MES"             , ICS_MES             , Cy::Sys, nomG , TRFR("Banc en service")                     },
//    {"CS_TEST"            , ICS_TEST            , Cy::Sys, nomG , TRFR("Banc en test (au moins un poste)")    },
//  };
//  if ( (sizeof(attr[0])*NB_CS_G) != sizeof(attr) )
//    CYFATAL;
//  for (int i=0; i<NB_CS_G; i++)
//  {
//    CYCS *cs = new CYCS(DBRN, attr[i].name, new t_cu32, &attr[i]);
//    cs->initResetData(DBRN, new flg);
//  }

  //  CYPIDP *data = new CYPIDP(DBRN, "DATA", new f32, 10.0, 5.0, 300.0);
  //  data->setGroup("Pressure PID");
  //  Q_CHECK_PTR(data);

  //  CYPIDRamp *data = new CYPIDRamp(DBRN, "DATA", new flg, 0);
  //  Q_CHECK_PTR(data);

  //  f32 *val = new f32;
  //  *val = 100.0;
  //  CYF32 *data = new CYF32(DBRN, "DATA", val);
  //  Q_CHECK_PTR(data);
  //  data->setGroup("Pressure PID");
  //  *val = 150.0;
  //  data->readData();

  CYWord *word = new CYWord(DBRN, "WORD", new u16);
  Q_CHECK_PTR(word);

  new CYFlag(DBRN, "CYFLAG", new flg);

  new CYFlag(DBRN, "SENS", new flg);
  new CYS32(DBRN, "POSX", new s32, -1, 1);
  new CYS32(DBRN, "POSY", new s32, -1, 1);

  /*  new CYF32(DBRN, "CYF32", new f32, 1.0e1, -1.0e-5, 1.0e6, FMTE_FUITE, Cy::User);*/
  CYF32 *df32 = new CYF32(DBRN, "CYF32", new f32, 1.1e1, 1.0e-7, 1.0e7, FMTE_FUITE, Cy::Sys);

  df32->setNbDec(2);

  //  new CYF32(DBRN, "CYF32", new f32, 2, 0, 360, Cy::Sys);
  //  new CYF64(DBRN, "CYF32", new f64, -3.5e1, -0.5e2, 2e5, 3, Cy::Sys);
  //  new CYF64(DBRN, "CYF32", new f64, 3, Cy::Sys);

  //   f32 *angle1 = new f32;
  //   *angle1 = 5.5;
  //   CYF32 *dangle1 = new CYF32(DBRN, "ANGLE1", angle1, 2, 0, 360, FMT_TRIGO, Cy::Sys);
  //   dangle1->setGroup(QString("Setpoint"));
  //   dangle1->setLabel(tr("Angle 1"));
  //   dangle1->setHelp(QString("Angle1 mfgmlshml;hgml;ml \t v;ml;smdf;sgfl \n ;gmll;f;fsd"));
  //   dangle1->addNote(QString("Angle1 mfgmlshml;hgml;ml \t v;ml;smdf;sgfl \n ;gmll;f;fsd"));
  //
  //   f32 *angle2 = new f32;
  //   *angle2 = 5.5;
  //   CYF32 *dangle2 = new CYF32(DBRN, "ANGLE2", angle2, 90, 0, 180, FMT_TRIGO, Cy::Sys);
  //   dangle2->setGroup(QString("Setpoint"));
  //   dangle2->setLabel(QString("Angle 2"));
  //   dangle2->setHelp(QString("Angle2 mfgmlshml;hgml;ml \t v;ml;smdf;sgfl \n ;gmll;f;fsd"));
  //   dangle2->addNote(QString("Angle2 mfgmlshml;hgml;ml \t v;ml;smdf;sgfl \n ;gmll;f;fsd"));

  for (int i=1; i<=34; i++)
  {
    CYF32 *dangle = new CYF32(DBRN, QString("ANGLE%1").arg(i), new f32, 0, 0, 360/i, FMT_TRIGO, Cy::User);
    dangle->setGroup(QString("Setpoint"));
    dangle->setLabel(QString("Angle %1").arg(i));

    CYF32 *dexp = new CYF32(DBRN, QString("EXP%1").arg(i), new f32, 0.0e0, 0.0e-5, 1.0e5, FMT_EXP_DEG, Cy::User);
    dexp->setGroup(QString("Setpoint"));
    dexp->setLabel(QString("Exp %1").arg(i));
  }

  f32 *course1 = new f32;
  *course1 = 50.0;
  CYF32 *dcourse1 = new CYF32(DBRN, "COURSE1", course1, 100, 0, 100, FMT_P_C, Cy::Sys);
  dcourse1->setGroup(QString("Setpoint"));
  dcourse1->setLabel(QString("Course 1"));
  dcourse1->setHelp(QString("Course1 mfgmlshml;hgml;ml \t v;ml;smdf;sgfl \n ;gmll;f;fsd"));
  dcourse1->addNote(QString("Course1 mfgmlshml;hgml;ml \t v;ml;smdf;sgfl \n ;gmll;f;fsd"));

  f32 *course2 = new f32;
  *course2 = 5.5;
  CYF32 *dcourse2 = new CYF32(DBRN, "COURSE2", course2, 2, 0, 360, Cy::Sys);
  dcourse2->setGroup(QString("Setpoint"));
  dcourse2->setLabel(QString("Course 2"));
  dcourse2->setHelp(QString("Course2 mfgmlshml;hgml;ml \t v;ml;smdf;sgfl \n ;gmll;f;fsd"));
  dcourse2->addNote(QString("Course2 mfgmlshml;hgml;ml \t v;ml;smdf;sgfl \n ;gmll;f;fsd"));

  // The QT_VERSION define has the numeric value in the form: 0xmmiibb (m = major, i = minor, b = bugfix). For example, Qt 3.0.5's QT_VERSION is 0x030005.
  char buf[100];
  sprintf(buf,"%X",QT_VERSION);
  CYMESSAGETEXT(QString("QT_VERSION=0x%1").arg(buf));

  //  sprintf(buf,"%X",KDE_VERSION);
  //  CYMESSAGETEXT(QString("KDE_VERSION=0x%1").arg(buf));

  f32 *val = new f32;
  *val = 3.2;
  CYF32 *df1 = new CYF32(DBRN, "DATA", val, 1, Cy::Sys);
  CYF32 *df2 = new CYF32(DBRN, "DATA2", new f32, 1, Cy::Sys);

  QList<CYPrintableInfo*> list;
  list.append(new CYPrintableInfo(core, "list", "DATA", "DATA qslk sdm sdk:"));
  list.append(new CYPrintableInfo(core, "list", "DATA2", "DATA2:"));
  list.append(new CYPrintableInfo(core, "list", "ANGLE1", "ANGLE1:"));
  list.append(new CYPrintableInfo(core, "list", "DATA", "DATA:"));
  list.append(new CYPrintableInfo(core, "list", "DATA2", "DATA2:"));
  list.append(new CYPrintableInfo(core, "list", "ANGLE2", "ANGLE2:"));
  list.append(new CYPrintableInfo(core, "list", "DATA", "DATA:"));
  list.append(new CYPrintableInfo(core, "list", "DATA2", "DATA2:"));
  list.append(new CYPrintableInfo(core, "list", "ANGLE1", "ANGLE1:"));
  core->setPrintableInfoList(list);

  CYU32 *du32 = new CYU32(DBRN, "DU32", new u32, 0, 0, 1000);
  du32->setNumBase( Cy::Binary );

  CYS32 *ds32 = new CYS32(DBRN, "DS32", new s32, 0, 0, 100, FMT_DEG);
  ds32->setLabel("DS32");
  df1->setGroup("Setpoint");
  df1->setLabel("Inlet temperature");
  df1->setHelp("sdok");
  df1->addNote("sdsqdjml;sdddddddddhhhhhmlmfsd");
  df1->addNote("sdfsdf");

  df2->setGroup("msdld");
  df2->setLabel("dfsdddddddddddj jjjjjjjjjjjjj jjjjjjjjjjjjjjj jjjjjjdddddddddddd");

#define TSGM_RIEN             0   // Rien
#define TSGM_REMP_ROD         1   // Remplissage, Rodage
#define TSGM_ROD_BP           2   // Rodage gymnastiquage B.P.
#define TSGM_ROD_HP           3   // Rodage gymnastiquage H.P.
#define TSGM_PERF_REG         4   // Perf.max.,regul P,Q ctrl,rinage
#define TSGM_CLAPET           5   // Clapet de secu.
#define TSGM_STAB             6   // Stabilite pression
#define TSGM_TRANSPAR         7   // Transparence
#define TSGM_VID_SYNC         8   // Synchro Vidange  (stabilisation pression)
#define TSGM_VIDANGE          9   // Vidange
#define TSGM_VID_ATM         10   // Vidange a l'atmosphere
#define TSGM_ATT_DCY         11   // Attente DCY (Depart CYcle)
#define TSGM_START_STOP_DEB  12   // Test Start/Stop debut
#define TSGM_START_STOP_FIN  13   // Test Start/Stop fin
#define TSGM_REG_RINCE_BP    14   // Perf.max.,regul P, et rincage  (mode rincage BP)
#define TSGM_REG_PRELEV_BP   15   // Perf.max.,regul P, et prelevement (mode prelevement HP)
#define TSGM_REG_RINCE_HP    16   // Perf.max.,regul P, et rincage  (mode rincage HP)
#define TSGM_REG_PRELEV_HP   17   // Perf.max.,regul P, et prelevement (mode prelevement HP)
#define NB_TSGM              18   // Nombre de type de segment

  CYS16 *ds16 = new CYS16(DBSPV, QString("CHOICE_LABEL"), new s16);
  ds16->setLabel(tr("Type"));
  ds16->setMax(NB_TSGM+1);
  ds16->setChoiceLabel(TSGM_RIEN           , "TSGM_RIEN          ");
  ds16->setChoiceLabel(TSGM_REMP_ROD       , "TSGM_REMP_ROD      ");
  ds16->setChoiceLabel(TSGM_ROD_BP         , "TSGM_ROD_BP        ");
  ds16->setChoiceLabel(TSGM_ROD_HP         , "TSGM_ROD_HP        ");
  ds16->setChoiceLabel(TSGM_PERF_REG       , "TSGM_PERF_REG      ");
  ds16->setChoiceLabel(TSGM_CLAPET         , "TSGM_CLAPET        ");
  ds16->setChoiceLabel(TSGM_STAB           , "TSGM_STAB          ");
  ds16->setChoiceLabel(TSGM_TRANSPAR       , "TSGM_TRANSPAR      ");
  ds16->setChoiceLabel(TSGM_VID_SYNC       , "TSGM_VID_SYNC      ");
  ds16->setChoiceLabel(TSGM_VIDANGE        , "TSGM_VIDANGE       ");
  ds16->setChoiceLabel(TSGM_VID_ATM        , "TSGM_VID_ATM       ");
  ds16->setChoiceLabel(TSGM_ATT_DCY        , "TSGM_ATT_DCY       ");
  ds16->setChoiceLabel(TSGM_START_STOP_DEB , "TSGM_START_STOP_DEB");
  ds16->setChoiceLabel(TSGM_START_STOP_FIN , "TSGM_START_STOP_FIN");
  ds16->setChoiceLabel(TSGM_REG_RINCE_BP   , "TSGM_REG_RINCE_BP  ");
  ds16->setChoiceLabel(TSGM_REG_PRELEV_BP  , "TSGM_REG_PRELEV_BP ");
  ds16->setChoiceLabel(TSGM_REG_RINCE_HP   , "TSGM_REG_RINCE_HP  ");
  ds16->setChoiceLabel(TSGM_REG_PRELEV_HP  , "TSGM_REG_PRELEV_HP ");

  data = new CYString(DBSPV, "FONC_DTMX");
  data->setNbChar(32);

  data = new CYString(DBSPV, "TEXT");
  data->setNbChar(4);
  Q_CHECK_PTR(data);

  CYF32 *dlevel = new CYF32(DBSPV, "LEVEL", new f32, 50.0, 0.0, 100.0, Cy::Sys);
  dlevel->setLabel(QString("Level"));

  mSummary = new CYString(DBSPV, "SUMMARY3", new QString("aqscs \n qqsa\n asqsqcfreg f gs gf  dd ddlkdfl,dfq ,,d,d,dsf,,dcf,q ,d, ,d,, d,s,ds, ndvn d dd *d d   "
                                                         "  d,ddf d,d,df,, ds"
                                                         " ddsdsdk,sndlk, ds,ds,fd"
                                                         " ,k,dkk,ql,fq "
                                                         "ds qqsfk,lk,fdslk,dsml;ds,f,kql,f k,fdk,ffd"
                                                         "\na\na\na\na\na\na\na\na\na\na\na\na\na\na\na\n"));
  mSummary->setVal("sqsq dsdsql,d ,ls ,s, S, ds,ds,,cs,sq,,sdlsq,sd,,lds,,s");
  mSummary->setLabel("SUMMARY3");
  QTimer *timer2 = new QTimer( this );
  connect( timer2, SIGNAL(timeout()), this, SLOT( changeSummary() ) );
  timer2->start( 500 );

  new CYDateTime(DBSPV, "DATE", new QString(QDate::currentDate().addMonths(6).toString(Qt::ISODate)));

  init_EA();
  init_SA();
  DBSPV->load();
}

void Core::initFormat()
{
  // CLE      | FORMAT  => PARENT|       NOM     |   UNITE     | DEFINITION PHYSYQUE |   MIN   |   MAX   |DEC|PREC| ACQ LENTE | ACQ RAPIDE |
  core->formats.insert(FMT_DEG    , new CYFormat(core, "FMT_DEG"     ,         "°C", tr("Temperature") ,      0.0,    150.0,  2, 0.0, SLOW_30MIN, FAST_1MIN));
  core->formats.insert(FMT_TRIGO  , new CYFormat(core, "FMT_TRIGO"   ,          "°", tr("Trigonometry"),   -180.0,    180.0,  2, 0.0, SLOW_30MIN, FAST_1MIN));
  core->formats.insert(FMT_EXP_DEG, new CYFormat(core, "FMT_EXP_DEG" ,         "°C", tr("Temperature") ,  "-1e-4",    "1e4",  2, 0.0, SLOW_30MIN, FAST_1MIN));
  core->formats.insert(FMTE_FUITE , new CYFormat(core, "FMTE_FUITE"  , "mBar.l/sec", tr("Leak rate")   ,  "1e-12",   "1e12",  1, 0.0, SLOW_30MIN, FAST_1MIN));
  core->formats.insert(FMT_BAR    , new CYFormat(core, "FMT_BAR"     ,        "Bar", tr("Pressure")    ,     -1.0,    200.0,  2, 0.0, SLOW_30MIN, FAST_1MIN));
  core->formats.insert(FMT_P_C    , new CYFormat(core, "FMT_P_C"     , "%"         , tr("Percentage")  ,   -200.0,    200.0,  1, 0.0, SLOW_30MIN, FAST_1MIN));
}

void Core::init_EA()
{
  DBRN->setUnderGroup(tr("Analog input"));
  // ENTREES ANA
  CYAna::Attr16 attr[]= // Attention l'ordre est important (comme index IEA_)
  {
      //     NOM      |POINTEUR|PTR. ADC|   FL  |  ELEC | PHYS |  MODE       |   FORMAT   |  LOW  |  TOP   |-------------- ETIQUETTE ------------|
  {"EA_PFE"     , new f32, new s16, new flg, "0EA1", "SP1", Cy::User    , FMT_BAR    ,   -1.0,    65.0, tr("Stations inlet pressure")           },
  {"EA_POS"     , new f32, new s16, new flg, "0EA2", "SD1", Cy::User    , FMT_P_C    ,    0.0,   100.0, tr("Cylinder position")                 },
  {"EA_TFE"     , new f32, new s16, new flg, "0EA3", "ST1", Cy::User    , FMT_DEG    ,    0.0,   180.0, tr("Stations inlet fluid temperature")  },
  {"EA_TFS"     , new f32, new s16, new flg, "0EA4", "ST2", Cy::User    , FMT_DEG    ,    0.0,   180.0, tr("Stations outlet fluid temperature") },
  {"EA_TRC1"    , new f32, new s16, new flg, "0EA5", "TC1", Cy::User    , FMT_DEG    ,    0.0,   200.0, tr("Ramp 1 heater temperature")         },
  {"EA_TRC2"    , new f32, new s16, new flg, "0EA6", "TC2", Cy::User    , FMT_DEG    ,    0.0,   200.0, tr("Ramp 2 heater temperature")         },

  {"EA_PFS"     , new f32, new s16, new flg, "1EA1", "SP2", Cy::User    , FMT_BAR    ,   -1.0,    65.0, tr("Stations outlet pressure")          },
  {"EA_PFB"     , new f32, new s16, new flg, "1EA2", "SP3", Cy::User    , FMT_BAR    ,    0.0,   160.0, tr("Bursting pressure")                 },
  {"EA_TAA"     , new f32, new s16, new flg, "1EA3", "ST3", Cy::User    , FMT_DEG    ,    0.0,   180.0, tr("Ambient air temperature")           },
  {"EA_TWE"     , new f32, new s16, new flg, "1EA4", "TC3", Cy::User    , FMT_DEG    ,    0.0,    70.0, tr("Water inlet temperature")           },
  {"EA_TWS"     , new f32, new s16, new flg, "1EA5", "TC4", Cy::User    , FMT_DEG    ,    0.0,    70.0, tr("Water outlet temperature")          },
};
  int nb = sizeof(attr)/sizeof(attr[0]);

  core->setInitBufferVal(0.0); // Permet l'utilisation de l'échelle automatique dès le début (impossible avec NAN)
  for (int i=0; i<nb; i++)
  {
    CYAI *data = new CYAI(DBRN, attr[i].name, &attr[i]);
    data->setSimulType(CYData::SimulType::Sinus);
    data->setDisableColor(Qt::gray);
    data->setMax(attr[i].tdef);
    //     data->setCali(DBSPV, &cal_ea[i], CYAna::CALU_487);
  }
  core->setInitBufferVal(); // Force la bufferisation par défaut par NAN

  DBRN->setUnderGroup(0);
}

void Core::init_SA()
{
  DBRN->setUnderGroup(tr("Analog output"));
  CYAna::Attr16 attr[]=
  {
    //     NOM      |POINTEUR|PTR. ADC|   FL  |  ELEC | PHYS  |  MODE       |   FORMAT   |  LOW  |  TOP   |-------------- ETIQUETTE ------------|
    {"SA_VAR"     , new f32, new s16, new flg, "0SA0", "VA1" , Cy::User    , FMT_P_C    ,    0.0,   100.0, tr("Frequency converter VA1 (circulation pump)")},
    {"SA_SERVO"   , new f32, new s16, new flg, "0SA1", "CAV1", Cy::User    , FMT_P_C    , -100.0,   100.0, tr("Servo cylinder control"                    )},
  };
  int nb = sizeof(attr)/sizeof(attr[0]);
  for (int i=0; i<nb; i++)
  {
    CYAO *data = new CYAO(DBRN, attr[i].name, &attr[i]);
    data->setDisableColor(Qt::gray);
  }
  DBRN->setUnderGroup(0);
}

void Core::changeSummary()
{
  static int cpt=0;
  mSummary->setVal(QString("aqscs \n qqsa\n asqsqcfreg f gs gf  dd ddlkdfl,dfq ,,d,d,dsf,,dcf,q ,d, ,d,, d,s,ds, ndvn d dd *d d   "
                           "  d,ddf d,d,df,, ds"
                           " ddsdsdk,sndlk, ds,ds,fd"
                           " ,k,dkk,ql,fq "
                           "ds qqsfk,lk,fdslk,dsml;ds,f,kql,f k,fdk,ffd"
                           "\na\na\na\na\na\na\na\na\na\na\na\na\na\na\na\n , S, ds,ds,,cs,sq,,sdlsq,sd,,lds,,s => %1").arg(cpt));
  cpt++;
}

void Core::customHelpMenu(QMenu *helpMenu)
{
  QIcon icon=QIcon(":/icons/cyapplication-pdf");
  helpMenu->addAction( icon, tr("Operator's manual"), this, SLOT(openCydocOperator()));
  helpMenu->addAction( icon, tr("Cylix manual"), this, SLOT(openCydoc()));
  helpMenu->addAction( icon, tr("Release notes"),this, SLOT(openReleaseNotes()));
}

void Core::openCydoc()
{
  QUrl url = docUrl("cydoc", "", "pdf", false);
  QDesktopServices::openUrl(url);
}

void Core::openCydocOperator()
{
  QUrl url = docUrl("cydoc_oper", "", "pdf", false);
  QDesktopServices::openUrl(url);
}

void Core::openReleaseNotes()
{
  // Ouverture non protégée des notes version
  QUrl url = docUrl("releasenotes", "", "pdf", false);
  QDesktopServices::openUrl(url);
}

//===================================================================================================
QString Core::label_ACO(int i)
{
  if (i==IACO_L)
    return tr("Slow continuous acquisition");
  if (i==IACO_R)
    return tr("Fast continuous acquisition");
  CYFATALTEXT(QString("iACO=%1").arg(i));
  return 0;
}

QString Core::name_ACO(int i)
{
  if (i==IACO_L)
    return QString("ACO_SLOW");
  if (i==IACO_R)
    return QString("ACO_FAST");
  CYFATALTEXT(QString("iACO=%1").arg(i));
  return 0;
}

QString Core::label_APM(int i)
{
  if (i==IAPM_L)
    return tr("Slow acquisition on event");
  if (i==IAPM_R)
    return tr("Fast acquisition on event");
  CYFATALTEXT(QString("iAPM=%1").arg(i));
  return 0;
}

QString Core::name_APM(int i)
{
  if (i==IAPM_L)
    return QString("APM_SLOW");
  if (i==IAPM_R)
    return QString("APM_FAST");
  CYFATALTEXT(QString("iAPM=%1").arg(i));
  return 0;
}

// "POST-MORTEM_LENT";
CYAcquisitionSlow * Core::initAcquisition_APM_Slow(QString acq_name,QString acq_label)
{
  CYFlag *flag = new CYFlag(core->cydblibs(), QString("FLR_ENABLE_%1").arg(acq_name), new flg);
  CYFlag *fl_on= new CYFlag(core->cydblibs(), QString("G_FL_%1").arg(acq_name), new flg);
  CYAcquisitionSlow *ptr = new CYAcquisitionSlow(this, acq_name, acq_label, flag, core->localLink());
  ptr->setIsOnFlag(fl_on);
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_DEFAUT_%1"   ).arg(acq_name), true, new flg) , QString("fault-%1"   ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_ALE_TOL_%1"  ).arg(acq_name), true, new flg) , QString("tol-%1"     ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_FUITE_%1"    ).arg(acq_name), true, new flg) , QString("leak-%1"    ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_FIN_%1"      ).arg(acq_name), true, new flg) , QString("end-%1"     ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_DEBUT_%1"    ).arg(acq_name), true, new flg) , QString("start-%1"     ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_OPERATEUR_%1").arg(acq_name), true, new flg) , QString("operator-%1").arg(acq_name));
//  ptr->maxNbFiles->setDef(2);
//  ptr->maxNbFiles->designer();
  ptr->setPostMortem(true);
  ptr->dirName->setVal("acquisition");
  ptr->prefixFileName->setVal(acq_name);
  ptr->postMortemBackupTimerFlag->setPrefixFileName(QString("periodic-%1").arg(acq_name));
  ptr->postMortemBackupTimerFlag->setEnable(false);
  acquisitions.insert(acq_name, ptr);
  return ptr;
}

// "POST-MORTEM_RAPIDE";
CYAcquisitionFast * Core::initAcquisition_APM_Fast(QString acq_name,QString acq_label)
{
  CYFlag *flag = new CYFlag(core->cydblibs(), QString("FLR_ENABLE_%1").arg(acq_name), new flg);
  CYFlag *fl_on= new CYFlag(core->cydblibs(), QString("G_FL_%1").arg(acq_name), new flg);
  CYAcquisitionFast *ptr = new CYAcquisitionFast(this, acq_name, acq_label, flag, core->localLink(), RNR_BUFFER_SIZE, RNR_BTMS);
  ptr->setIsOnFlag(fl_on);
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_DEFAUT_%1"   ).arg(acq_name), true, new flg) , QString("fault-%1"   ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_ALE_TOL_%1"  ).arg(acq_name), true, new flg) , QString("tol-%1"     ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_FUITE_%1"    ).arg(acq_name), true, new flg) , QString("leak-%1"    ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_FIN_%1"      ).arg(acq_name), true, new flg) , QString("end-%1"     ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_DEBUT_%1"    ).arg(acq_name), true, new flg) , QString("start-%1"   ).arg(acq_name));
  ptr->addPostMortemFlags(new CYAcquisitionBackupFlag(core->cydblibs(), QString("FLR_OPERATEUR_%1").arg(acq_name), true, new flg) , QString("operator-%1").arg(acq_name));
//  ptr->maxNbFiles->setDef(2);
//  ptr->maxNbFiles->designer();
  ptr->setPostMortem(true);
  ptr->dirName->setVal("acquisition");
  ptr->prefixFileName->setVal(acq_name);
  CYDB *db = dbList.value("DBRNR");
  db->addAcquisition(ptr);
  ptr->postMortemBackupTimerFlag->setPrefixFileName(QString("periodic-%1").arg(acq_name));
  ptr->postMortemBackupTimerFlag->setEnable(false);
  acquisitions.insert(acq_name, ptr);
  return ptr;
}

// "ACQUISITION_LENTE"
CYAcquisitionSlow * Core::initAcquisition_ACO_Slow(QString acq_name,QString acq_label)
{
  CYFlag *flag = new CYFlag(core->cydblibs(), QString("FLR_ENABLE_%1").arg(acq_name), new flg);
  CYFlag *pause= new CYFlag(core->cydblibs(), QString("FLR_PAUSE_%1").arg(acq_name), new flg);
  CYFlag *fl_on= new CYFlag(core->cydblibs(), QString("G_FL_%1").arg(acq_name), new flg);
  CYAcquisitionSlow *ptr = new CYAcquisitionSlow(this, acq_name, acq_label, flag, pause, core->localLink());
  ptr->setIsOnFlag(fl_on);
  ptr->dirName->setVal("acquisition");
  ptr->prefixFileName->setVal(acq_name);
  acquisitions.insert(acq_name, ptr);
  return ptr;
}

// "ACQUISITION_RAPIDE"
CYAcquisitionFast * Core::initAcquisition_ACO_Fast(QString acq_name,QString acq_label)
{
  CYFlag *flag = new CYFlag(core->cydblibs(), QString("FLR_ENABLE_%1").arg(acq_name), new flg);
  CYFlag *pause= new CYFlag(core->cydblibs(), QString("FLR_PAUSE_%1").arg(acq_name), new flg);
  CYFlag *fl_on= new CYFlag(core->cydblibs(), QString("G_FL_%1").arg(acq_name), new flg);
  CYAcquisitionFast *ptr = new CYAcquisitionFast(this, acq_name, acq_label, flag, pause, core->localLink(), RNR_BUFFER_SIZE, RNR_BTMS);
  ptr->setIsOnFlag(fl_on);
  ptr->dirName->setVal("acquisition");
  ptr->prefixFileName->setVal(acq_name);
  CYDB *db = dbList.value("DBRNR");
  db->addAcquisition(ptr);
  acquisitions.insert(acq_name, ptr);
  return ptr;
}

void Core::initAcquisition_APM(int i)
{
  if (i==IAPM_L)
  {
    initAcquisition_APM_Slow(name_APM(i),label_APM(i));
    return;
  }
  if (i==IAPM_R)
  {
    initAcquisition_APM_Fast(name_APM(i),label_APM(i));
    return;
  }
  CYFATALTEXT(QString("iAPM=%1").arg(i));
}

void Core::initAcquisition_ACO(int i)
{
  if (i==IACO_L)
  {
    initAcquisition_ACO_Slow(name_ACO(i),label_ACO(i));
    return;
  }
  if (i==IACO_R)
  {
    initAcquisition_ACO_Fast(name_ACO(i),label_ACO(i));
    return;
  }
  CYFATALTEXT(QString("iACO=%1").arg(i));
}

void Core::initAcquisitions()
{
  if (isBenchType("_APM"))
    for (int i=0; i<NB_APM; i++)
      initAcquisition_APM(i);
  if (isBenchType("_ACO"))
    for (int i=0; i<NB_ACO; i++)
      initAcquisition_ACO(i);
}
