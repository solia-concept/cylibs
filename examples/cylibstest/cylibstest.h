/***************************************************************************
                          cylibstest.h  -  description
                             -------------------
    begin                : jeu déc 18 11:43:59 CET 2003
    copyright            : (C) 2003 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

#ifndef CYLIBSTEST_H
#define CYLIBSTEST_H

//#ifdef HAVE_CONFIG_H
//#include <config.h>
//#endif

// CYLIBS
#include <cywidget.h>

namespace Ui {
    class CylibsTest;
}

/** Cylibs is the base class of the project */
class CylibsTest : public CYWidget
{
  Q_OBJECT
public:
  /** construtor */
  CylibsTest(QWidget* parent=0, const char *name=0);
  /** destructor */
  ~CylibsTest();
  /** Initialisation. */
  void init();
  void initMenu();

public slots: // Public slots
  /** Saisie un index qui une fois transformé en QString deviendra le repère
    * de généricité utile aux widgets génériques @see setGenericMark(QString txt). */
  virtual void setGenericMark(int index);
    void analyseWin();
    void testAutoCali();
    void testAutoCali(QByteArray dataName);
    void testManuCali();
    void testManuCali(QByteArray dataName);
    void changeXL( double val );
    void changeXR( double val );
    void changeYB( double val );
    void changeYT( double val );

    void close();

protected slots:
    void invokeHelp();

private:
    Ui::CylibsTest *ui;
};

#endif
