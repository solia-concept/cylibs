/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : jeu déc 18 11:43:59 CET 2003
    copyright            : (C) 2003 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèques
                 utiles au développement d'un superviseur CYLIX

 ***************************************************************************/

// QT
#include <QTranslator>
// CYLIBS
#include "cyaboutdata.h"
#include "cyglobal.h"
#include "cylibstest.h"
//#include "cyuseradmineditdlg2.h"
#include "cyapplication.h"
#include "cymainwin.h"

static const char *description =
	"Cylibs";
// INSERT A DESCRIPTION FOR YOUR APPLICATION HERE


//static KCmdLineOptions options[] =
//{
//  { 0, 0, 0 }
//  // INSERT YOUR COMMANDLINE OPTIONS HERE
//};

int main(int argc, char *argv[])
{
  CYAboutData aboutData( "cylibstest", "Cylibs",
    DEF_CY_VERSION, description, CYAboutData::License_GPL,
    "(c) 2003, LE CLÉACH Gérald", nullptr, "0", "gerald.lecleach@solia-concept.fr");
  aboutData.addAuthor("LE CLÉACH Gérald",nullptr, "gerald.lecleach@solia-concept.fr");
//  KCmdLineArgs::init( argc, argv, &aboutData );
//  KCmdLineArgs::addCmdLineOptions( options ); // Add our own options.

  CYApplication a(argc, argv, &aboutData, true);

  CylibsTest *cylibsTest = new CylibsTest();
  CYMainWin *mw = new CYMainWin(0, "MAINWIN", 1, QString("1: ")+QString("Main window"));
  Q_CHECK_PTR(mw);

  mw->setCentralWidget(cylibsTest);
  cylibsTest->initMenu();
  mw->createGUI("cylibsui.rc");
  mw->initHelpMenu();

//  cylibsTest->setCaption(VERSION_STR);
//   cylibsTest->setCaption(VERSION);
//   CYUserAdminEditDlg2 *cylibs = new CYUserAdminEditDlg2();
//   QListViewItem *group, *parent;
//   group = new QListViewItem( cylibs->userList, "Administrateur", "3" );
//   group->insertItem(new QListViewItem( group, "Administrateur", "3" ));

//   parent = group;
//   group = new QListViewItem( cylibs->userList, "Chef d'équipe", "4" );
//   group->insertItem(parent);
//   group->insertItem(new QListViewItem( group, "Michel", "4" ));

/*  parent = group;
  group = new QListViewItem( parent, "Opérateurs", "5" );
  group->insertItem(group);
  group->insertItem(new QListViewItem( group, "David", "5" ));
  group->insertItem(new QListViewItem( group, "Xavier", "6" ));
  group->insertItem(new QListViewItem( group, "Thomas", "7" ))*/;

//   cylibs->setCaption(VERSION);

//  a.setMainWidget(cylibsTest);
  mw->show();

  return a.exec();
}
