/***************************************************************************
                          cylibs.cpp  -  description
                             -------------------
    begin                : jeu dé 18 11:43:59 CET 2003
    copyright            : (C) 2003 by LE CLÉACH Gérald
    email                : gerald.lecleach@solia-concept.fr
 ***************************************************************************/

/***************************************************************************

                    Ce fichier fait partie des bibliothèues
                 utiles au déeloppement d'un superviseur CYLIX

 ***************************************************************************/

#include "cylibstest.h"
#include "ui_cylibstest.h"

// QT
#include <qtoolbox.h>
//Added by qt3to4:
#include <QString>
#include <QList>
#include <QElapsedTimer>
// CYLIBS
#include "cycore.h"
#include "cyai.h"
#include "cypen.h"
#include "cyuser.h"
#include "cynumdisplay.h"
#include "cymultimeter.h"
#include "cybargraph.h"
#include "cyscope.h"
#include "cyprintableinfo.h"
#include "cybargraph.h"
#include "cysmtp.h"
#include "cyeventspreferences.h"
#include "cypeninput.h"
#include "cypushbutton.h"
#include "cyanalysewin.h"
#include "cyanalysesheet.h"
#include "cyautocali.h"
#include "cymanucali.h"
//#include "eventspanel.h"
#include "cyscopeplotter.h"
#include "cyscopeaxis.h"
#include "cyslider.h"
#include "cyapplication.h"
#include "cyaboutdata.h"
#include "cymainwin.h"
#include "cyactionmenu.h"
#include "cydblibs.h"
#include "core.h"

CylibsTest::CylibsTest(QWidget *parent, const char *name)
  : CYWidget(parent,name), ui(new Ui::CylibsTest)
{
  core = new Core(this, "CORE");
  Q_CHECK_PTR(core);
  core->init();

  ui->setupUi(this);

  init();
}

CylibsTest::~CylibsTest()
{
}

void CylibsTest::init()
{
  QString subject = "TEST 2 CYSMTP";
  QString body = "Premier test";

  QElapsedTimer timer;
  //   timer.start(5000, true); // temps maxi de blocage en ms
  timer.start(); // temps maxi de blocage en ms

  bool end =false;
  while (!end)
  {
    if (timer.elapsed()>2000)
    {
      end = true;
    }
  }

  ui->sliderXL->setValue(100.0);
  ui->sliderYB->setValue(100.0);
  connect( ui->sliderXR, SIGNAL( sliderMoved(double) ), this, SLOT( changeXR(double) ) );
  connect( ui->sliderYB, SIGNAL( sliderMoved(double) ), this, SLOT( changeYB(double) ) );
  connect( ui->sliderYT, SIGNAL( sliderMoved(double) ), this, SLOT( changeYT(double) ) );
  connect( ui->sliderXL, SIGNAL( sliderMoved(double) ), this, SLOT( changeXL(double) ) );

  ui->bargraph->CYDisplay::addData(QString("COURSE1"));
  ui->bargraph->CYDisplay::addData(QString("COURSE2"));


  ui->scope->addData(core->findData("EA_PFE"));
  ui->scope->addData(core->findData("EA_POS"));
  ui->scope->setSettingsFile(core->appDataDir()+"/scope.cywdg");
  ui->scope->loadSettings();

  //  ui->bargraph->addData(df1);
  CYAI *ea_pfe = (CYAI *)core->findData("EA_PFE");
  ui->multimeter->addData(ea_pfe);
  ui->multimeter->setShowAna(true);
  ui->multimeter->setShowLCD(false);

  ui->bargraph->setSettingsFile(core->appDataDir()+"/bargraph.cywdg");
  ui->bargraph->loadSettings();

  //   displaySimple1->setSettingsFile(core->appDataDir()+"/displaySimple1.cywdg");

  ui->displaySimple2->setSettingsFile(core->appDataDir()+"/displaySimple2.cywdg");

  //   datasTable->setSettingsFile(core->appDataDir()+"/datasTable.cywdg");
  //   DBRN->load();
  //     CYEventsPreferences dlg;
  //     if (dlg.exec())
  //     {
  //         // redo your settings
  //     }

  mTimer->start(500);
  //  setGenericMark(1);
  //   cYDial1->setGeometry(400, cYDial1->y(),  cYDial1->width(), cYDial1->height() );
  ui->cYFrame1->moveInsideParent( 558, 282 );
  ui->cYFrame1_2->moveInsideParent( 558, 281 );

  CYPen *pen = new CYPen(core->cydblibs(), "PEN", "Pen");
  ui->penInput->setPen( pen );

  //   setEditPositions( false );
  setEditPositions( true );

  ui->analyseSheet->setFileName( QString(core->appDataDir()+"as.cyas") );
  if (!QFile::exists( ui->analyseSheet->fileName() ))
    ui->analyseSheet->createGrid(1, 1, Qt::Horizontal);
  else
  {
    ui->analyseSheet->setDisplayTitle("1/1", tr("Digital inputs"));   // colonne 1 ligne 1
    ui->analyseSheet->setDisplayTitle("2/1", tr("Analog inputs"));    // colonne 1 ligne 2
  }

  ui->toolBox->setCurrentIndex(8);

  core->readOSUserProperties(core->OSUserConfig());
  core->initAccess();
  core->initAcquisitions();
  core->startRefreshDB();

  QString title = QString("%1-%2 (Access: %3)").arg(cyapp->aboutData()->programName()).arg(cyapp->aboutData()->version()).arg(core->user()->title());
  setWindowTitle(title);

  core->setStarted();
  core->createDocBook();
  core->deleteSplash();
}

void CylibsTest::setGenericMark(int index)
{
  CYTemplate::setGenericMark(index);
  linkDatas();
}

/*!
    \fn CylibsTest::invokeHelp()
 */
void CylibsTest::invokeHelp()
{
  core->invokeHelp("cydoc");
}

void CylibsTest::analyseWin()
{
  CYAnalyseWin *mAnalyseWin = new CYAnalyseWin("ANALYSEWIN", 2, tr("Graphic analyse window"));
  Q_CHECK_PTR(mAnalyseWin);

  //  mAnalyseWin->createGUI(locate("appdata", "cyanalyseui_cylibs.rc"));
  mAnalyseWin->createGUI("cylixanalyseui.rc");
  mAnalyseWin->initHelpMenu();
  //  CYAnalyseSheetTemplate *ast = mAnalyseWin->addTemplate("cydoc", locate("appdata", "cyas/cydoc.cyas"));

  //  ast->setDisplayTitle("1/1", tr("Temporal example"));   // ligne 1 colonne 1
  //  ast->setDisplayTitle("1/2", tr("XY example"));         // ligne 1 colonne 2
  //  ast = mAnalyseWin->addTemplate("scope", locate("appdata", "cyas/scope.cyas"));
  //  ast->setDisplayTitle("1", tr("Temporal example"));   // ligne 1 colonne 1
  //  ast->setDisplayTitle("2", tr("XY example"));         // ligne 1 colonne 2
  mAnalyseWin->show();
  mAnalyseWin->raise();
}

/*!
    \fn CylibsTest::testAutoCali()
 */
void CylibsTest::testAutoCali()
{
  testAutoCali("EA_PFE");
}

/*!
    \fn CylibsTest::testAutoCali(QByteArray dataName)
 */
void CylibsTest::testAutoCali(QByteArray dataName)
{
  CYAna *data = (CYAna *)core->findData(dataName);
  CYAutoCali *dlg = new CYAutoCali(this, "CYAutoCali");
  Q_CHECK_PTR(dlg);
  dlg->init(data);
  dlg->exec();
}

/*!
    \fn CylibsTest::testManuCali()
 */
void CylibsTest::testManuCali()
{
  testManuCali("EA_PFE");
}

/*!
    \fn CylibsTest::testManuCali(QByteArray dataName)
 */
void CylibsTest::testManuCali(QByteArray dataName)
{
  CYAna *data = (CYAna *)core->findData(dataName);
  CYManuCali *dlg = new CYManuCali(this, "CYManuCali");
  Q_CHECK_PTR(dlg);
  dlg->init(data);
  dlg->exec();
}

void CylibsTest::changeXL( double val )
{
  double left  = ui->scope->plot()->timeScopeAxis()->leftData()->max()*val/100.0;
  double right = ui->scope->plot()->timeScopeAxis()->rightData()->val();
  ui->scope->plot()->changeRangeX(QwtPlot::xBottom, left, right);
}

void CylibsTest::changeXR( double val )
{
  double left  = ui->scope->plot()->timeScopeAxis()->leftData()->val();
  double right = ui->scope->plot()->timeScopeAxis()->rightData()->max()*val/100.0;
  ui->scope->plot()->changeRangeX(QwtPlot::xBottom, left, right);
}

void CylibsTest::changeYB( double val )
{
  double bottom  = ui->scope->plot()->scopeAxis[QwtPlot::yLeft]->minData()->def()*val/100.0;
  double top     = ui->scope->plot()->scopeAxis[QwtPlot::yLeft]->maxData()->val();
  ui->scope->plot()->changeRangeY(QwtPlot::yLeft, bottom, top);
}

void CylibsTest::changeYT( double val )
{
  double bottom  = ui->scope->plot()->scopeAxis[QwtPlot::yLeft]->minData()->val();
  double top     = ui->scope->plot()->scopeAxis[QwtPlot::yLeft]->maxData()->def()*val/100.0;
  ui->scope->plot()->changeRangeY(QwtPlot::yLeft, bottom, top);
}

void CylibsTest::initMenu()
{
  CYMainWin *mw=core->mainWin();
  CYActionCollection *actionCollection = core->mainWin()->actionCollection();

  // Menu Cylix
  new CYActionMenu(tr("C&ylix"), actionCollection, "Cylix");
  new CYAction(tr("&Protected access"), "cypreferences-desktop-cryptography" , Qt::CTRL+Qt::Key_P        , mw, SLOT(protectedAccess()), actionCollection, "cylix_protected_access");
  new CYAction(tr("&Change access")   , "cyman"                              , 0                         , mw, SLOT(changeAccess())   , actionCollection, "cylix_change_access"   );
  new CYAction(tr("Change &password") , "cypassword"                         , 0                         , mw, SLOT(changePassword()) , actionCollection, "cylix_change_password" );

  new CYAction(tr("Machine status backup"), "cydb-save"                       ,                         0,core, SLOT(machineStatusBackup())    , actionCollection, "cydb-save"    );
  new CYAction(tr("&Quit")            , "cyapplication-exit"                 , QKeySequence::Quit        , this, SLOT(close())                   , actionCollection, "cylix_quit"   );

  // Menu d'outils
  new CYActionMenu(tr("&Tools")      , actionCollection, "tools");
  new CYAction(tr("&Graphic analyse")   , "cygraphicanalyse"  ,      0, this , SLOT(analyseWin())    , actionCollection, "tools_analyse"         );

  // Menu de configuration
  new CYActionMenu(tr("&Settings"), actionCollection, "settings");
  new CYAction(tr("&User administration"),"cyuser-group-properties", 0, mw, SLOT(userAdmin()), actionCollection, "user_admin");
}

void CylibsTest::close()
{
  ui->analyseSheet->save();
  core->mainWin()->close();
}
