#ifndef CORE_H
#define CORE_H

// CYLIBS
#include <cycore.h>

class CYNetHost;
class CYNetLink;
class CYString;

//===============================================================
// index / NB_ACO   index Acquisition en COntinu (par fenetrage)
// FLR_ACO(a,i)
enum {
  IACO_L =0, // acquisition continue lente  (sur rn)
  IACO_R   , // acquisition continue rapide (sur rnr)
  NB_ACO     // nombre de ACO
};

// index / NB_APM   index Acquisition Post-Mortem (sur evenement)
// FLR_APM(a,i)
enum {
  IAPM_L =0, // Post-mortem lent  (sur rn)
  IAPM_R   , // Post-mortem rapide (sur rnr)
  NB_APM     // nombre de APM
};

class Core : public CYCore
{
  Q_OBJECT
public:
  /** Construit le noyau de l'application. */
  Core(QObject *parent=0, QString name=0);

  /** Saisie le projet d'essai en cours.
   * param tmp     Chargement temporaire (inutile de garder une trace de ce chargement).
   * @param index  Index de projet (0 si un seul project actif à la fois).*/
  void setProject(CYProject *project, bool tmp = false, int index =0)
  {
    CYCore::setProject(project, tmp, index);
  }

  /** Charge le projet ayant pour nom de fichier \a filename
   * @param filename nom du fichier projet.
   * @param tmp      temporaire (pas de nouveau répertoire).
   * @param index    Index de projet (0 si un seul project actif à la fois). */
  CYProject *loadProject(const QString &filename, bool tmp = false, int index=0)
  {
    Q_UNUSED(filename)
    Q_UNUSED(tmp)
    Q_UNUSED(index)
    return 0;
  }

  /** Initialisation. */
  void init();

  /** Initialise les acquisitions. */
  void initAcquisitions();
  void initAcquisition_APM(int i);
  CYAcquisitionFast * initAcquisition_APM_Fast(QString acq_name,QString acq_label);
  CYAcquisitionSlow * initAcquisition_APM_Slow(QString acq_name,QString acq_label);
  void initAcquisition_ACO(int i);
  CYAcquisitionFast * initAcquisition_ACO_Fast(QString acq_name,QString acq_label);
  CYAcquisitionSlow * initAcquisition_ACO_Slow(QString acq_name,QString acq_label);
  void initAcquisition_Project_Fast(CYAcquisitionFast *ptr, bool fl_init);
  void initAcquisition_Project_Slow(CYAcquisitionSlow *ptr, bool fl_init);
  QString label_ACO(int i);
  QString name_ACO(int i);
  QString label_APM(int i);
  QString name_APM(int i);

public slots:
  /** Ouvre le manuel de Cylix */
  void openCydoc();
  /** Ouvre le manuel opérateur de Cylix */
  void openCydocOperator();
  /** Ouvre les notes de version */
  void openReleaseNotes();
  void changeSummary();

private:
  /** Permet une personnalisation du menu d'aide des fenêtre principale de l'application. */
  void customHelpMenu(QMenu *helpMenu);

  void initFormat();
  void init_EA();
  void init_SA();

  CYDB *DBSPV;
  CYDB *DBRN;
  CYDB *DBRNR;
  CYNetHost *mSPV;
  CYNetHost *mRN;
  CYNetLink *mLocalLink;
  CYNetLink *mSlowLink1;
  CYNetLink *mFastLink1;

  CYString *mSummary;
};

#endif // CORE_H
