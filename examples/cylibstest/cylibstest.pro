################################################################
# Cylix Library
# Copyright (C) 2015   Gérald Le Cléach
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the Qwt License, Version 1.0
################################################################

include( $${PWD}/../examples.pri )

##-----------------------------------------------------------------
## Fichiers pouvant être recopiés dans l'arborescence de chaque
## Cylix pour en faire facilement un fichier "setup".

#DISTFILES+= \
#$${PWD}/../../languages/cylibs_fr.ts \
#$${PWD}/../../languages/cylibs_de.ts \
#$${PWD}/../../languages/cylibs_zh_CN.ts \
#$${PWD}/../../languages/cylibs_cs.ts \
#$${PWD}/../../languages/cylibs_pl.ts


#translations.files = $${DISTFILES}
#translations.files ~= s,\\.ts$,.qm,g
#translations.path = $${CY_INSTALL_LANG}
#INSTALLS += translations

#cyspec.files  = cyconfig.pri cyfunctions.pri cy.prf ChangeLog HOWTO cylibs.spec
#cyspec.path  = $${CY_INSTALL_FEATURES}
#INSTALLS += cyspec

##-----------------------------------------------------------------

SOURCES += \
    main.cpp \
    cylibstest.cpp \
    core.cpp

HEADERS  += \
    cylibstest.h \
    core.h

FORMS += \
    cylibstest.ui

target.path += $${CY_INSTALL_BIN}
INSTALLS += target

RESOURCES += \
      guirc.qrc

