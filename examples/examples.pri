################################################################
# Cylix Library
# Copyright (C) 2015   Gérald Le Cléach
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the Qwt License, Version 1.0
###################################################################

CY_ROOT = $${PWD}/..
include( $${CY_ROOT}/cyconfig.pri )
include( $${CY_ROOT}/cyfunctions.pri )

CY_OUT_ROOT = $${OUT_PWD}/../..

TEMPLATE     = app

DEPENDPATH += . \
              $${CY_ROOT}/src \
              $${CY_ROOT}/src/cyaccess \
              $${CY_ROOT}/src/cyacquisition \
              $${CY_ROOT}/src/cyanalyse \
              $${CY_ROOT}/src/cycali \
              $${CY_ROOT}/src/cycore \
              $${CY_ROOT}/src/cydata \
              $${CY_ROOT}/src/cydisplay \
              $${CY_ROOT}/src/cyevent \
              $${CY_ROOT}/src/cynet \
              $${CY_ROOT}/src/cyproject \
              $${CY_ROOT}/src/cyscript \
              $${CY_ROOT}/src/cyui \
              $${CY_ROOT}/src/cyuser \
              $${CY_ROOT}/src/cywidgets \
              $${CY_ROOT}/src/qwt \

INCLUDEPATH += . \
               $${CY_ROOT}/src \
               $${CY_ROOT}/src/cyui \
               $${CY_ROOT}/src/cycore \
               $${CY_ROOT}/src/cydisplay \
               $${CY_ROOT}/src/qwt \
               $${CY_ROOT}/src/cydata \
               $${CY_ROOT}/src/cyacquisition \
               $${CY_ROOT}/src/cyevent \
               $${CY_ROOT}/src/cynet \
               $${CY_ROOT}/src/cyuser \
               $${CY_ROOT}/src/cyanalyse \
               $${CY_ROOT}/src/cycali \
               $${CY_ROOT}/src/cyaccess \
               $${CY_ROOT}/src/cyproject \
               $${CY_ROOT}/src/cywidgets

!debug_and_release {

    DESTDIR      = $${CY_OUT_ROOT}/examples/bin
}
else {
    CONFIG(debug, debug|release) {

        DESTDIR      = $${CY_OUT_ROOT}/examples/bin_debug
    }
    else {

        DESTDIR      = $${CY_OUT_ROOT}/examples/bin
    }
}

QMAKE_RPATHDIR *= $${CY_OUT_ROOT}/lib
cyAddLibrary($${CY_OUT_ROOT}/lib, cy)

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += network
    QT += xml
    QT += widgets
    QT += printsupport
    QT += concurrent
}

contains(CY_CONFIG, CyOpenGL ) {

    QT += opengl
}
else {

    DEFINES += CY_NO_OPENGL
}

contains(CY_CONFIG, CySvg) {

    QT += svg
}
else {

    DEFINES += CY_NO_SVG
}


windows {
    contains(CY_CONFIG, CyDll) {
        DEFINES    += QT_DLL CY_DLL
    }
}
